Impulse durch Endnutzer / Freie Software
========================================

<!-- toc -->

- [Hersteller von Smartphone-Apps](#hersteller-von-smartphone-apps)
- [Antworten auf Fragen](#antworten-auf-fragen)
  * ["Warum verwenden Sie keine freie SW, obwohl es das BSI empfiehlt?"](#warum-verwenden-sie-keine-freie-sw-obwohl-es-das-bsi-empfiehlt)
- [Beispiele](#beispiele)
- [Häufige Reaktionen, allgemein](#haufige-reaktionen-allgemein)
  * ["nur Google, wegen Updates"](#nur-google-wegen-updates)
  * ["Wenn Freie SW, dann tauchen unkontrollierte Klone auf"](#wenn-freie-sw-dann-tauchen-unkontrollierte-klone-auf)
  * ["Wir sind an Verträge gebunden"](#wir-sind-an-vertrage-gebunden)
  * ["Es macht sonst niemand."](#es-macht-sonst-niemand)
  * ["Wir sind abhängig von Google-Diensten"](#wir-sind-abhangig-von-google-diensten)
  * ["Man kann ja Apple verwenden"](#man-kann-ja-apple-verwenden)
  * ["Aber Android wird ja von Google entwickelt"](#aber-android-wird-ja-von-google-entwickelt)
  * ["Die Leute wollen das so"](#die-leute-wollen-das-so)
- [Sonstige Aussagen](#sonstige-aussagen)
  * ["Code-Qualität ist schlechter"](#code-qualitat-ist-schlechter)
- [Hintergrund: Warum ist Privatsphäre wichtig?](#hintergrund-warum-ist-privatsphare-wichtig)
- [Beispiel: Katastrophen-Warn-Apps](#beispiel-katastrophen-warn-apps)
  * [Allgemein](#allgemein)
  * [KATWARN](#katwarn)
  * [NINA](#nina)
  * [Fazit: SMS-Verfahren verwenden](#fazit-sms-verfahren-verwenden)
- [Alternativen zu Google-Play-Diensten](#alternativen-zu-google-play-diensten)
- [Positive Beispiele](#positive-beispiele)

<!-- tocstop -->

Hersteller von Smartphone-Apps
------------------------------
Frage an App-Hersteller, die prinzipiell (laut Satzung oder Auftrag) auch dem Gemeinwohl dienen wollen, warum sie ihre App nicht nach Kritieren der freie Software zur Verfügung gestellt werden.

Hintergrund / Infomaterial für Anfragen:

* [Warum ist freie Software wichtig?](../FreieSoftware)
* siehe f-droid-project.md

Bisheriges Fazit: Viele der Reaktionen aus den verantwortlichen Abteilungen zeigen, dass es Vorbehalte gegenüber freier Software gibt. Diese basieren oft auf Vorurteilen und Spekulationen. Einiges lässt sich auch auf die Unterbewertung des Grundrechts auf Privatsphäre zurückführen. Details siehe unten.

Antworten auf Fragen
--------------------
### "Warum verwenden Sie keine freie SW, obwohl es das BSI empfiehlt?"
* siehe https://www.bsi.bund.de/DE/Themen/DigitaleGesellschaft/FreieSoftware/freiesoftware_node.html
* Das BSI ist eine Autorität, die erst einmal gut begründet widerlegt werden muss.
* Oder eben nicht, wenn kein Bewusstsein da ist.

Beispiele
---------
* Diskussion: ["DB Navigator für Android als APK Download"](https://community.bahn.de/questions/1420108-db-navigator-fur-android-apk-download)

Häufige Reaktionen, allgemein
-----------------------------
### "nur Google, wegen Updates"
* Wir machen kein APK, denn sonst müsste man eine eigene Update-Funktion einbauen
    * technisch wäre das einfach
    * muss ja nur _zusätzlich_ zum Google-Store sein

### "Wenn Freie SW, dann tauchen unkontrollierte Klone auf"
* Dafür gibt es kein praktisches Beispiel.
* Außerdem könnten Klone auch auftauchen, wenn die Software nicht frei ist.

### "Wir sind an Verträge gebunden"
* Es gibt viele Software-Häuser, die nach Auftrag freie Software entwickeln.
    * z. B. todo
* Wenn die App-Beauftrager solche Dienste stärker nachfragen würden, gäbe es auch mehr zur Auswahl.

### "Es macht sonst niemand."
* Gegenbeispiel: https://www.secuso.informatik.tu-darmstadt.de/de/secuso/forschung/ergebnisse/privacy-friendly-apps

### "Wir sind abhängig von Google-Diensten"
* Wurde die Notwendigkeit von freier Software erkannt und wird dem mit Wohlwollen begegnet? Das würde man daran merken:
    * Wird auf der Webseite vor den Gefahren der Google-Nutzung aufmerksam gemacht?
        * (meistens nicht)
    * Wird plausibel dargelegt, dass man an einer Alternative arbeitet
        * (meistens nicht)
    * (Das heißt also meistens: man ist nicht willig, die Privatsphäre des Users zu schützen.)

### "Man kann ja Apple verwenden"
* ist leider auch keine allgemeingültige Lösung
    * siehe BSI-Empfehlung zu freier Software
    * siehe z. B. ["Interview: Wie sicher sind iOS und macOS vor Angriffen?", User-Comment](https://www.heise.de/forum/Mac-i/Kommentare/Interview-Wie-sicher-sind-iOS-und-macOS-vor-Angriffen/Das-eigentliche-Problem-sind-die-Anwender/posting-30769985/show/), 2017

* 2019
    * ["Apple doesn't actually care about your privacy"](https://www.youtube.com/watch?v=shxTTon5lfs), 2019, 12 min
        * ...
    * ["Apple-Datenschutzrichtlinie ist größtenteils rechtswidrig"](https://www.heise.de/mac-and-i/meldung/Apple-Datenschutzrichtlinie-ist-groesstenteils-rechtswidrig-4316486.html), 2019
        * "Derzeit sei aber nicht auszuschließen, dass die Beklagte im Zuge einer erneuten Umstrukturierung auch wieder den Online-Store übernehme und wie die "jetzige Betreiberin die Datenschutzrichtlinie in ihrer früheren Fassung weiterverwendet". Der Store wird mittlerweile von einer anderen Apple-Tochter betrieben."
            * Apples Produkte sind so teuer, weil da Datenschutz schon mit drin ist?
                * User-Comment: https://www.heise.de/forum/Mac-i/News-Kommentare/Apple-Datenschutzrichtlinie-ist-groesstenteils-rechtswidrig/Re-Wie-war-das-noch/posting-34008411/show/
                    * "dass sie ihr iAd wegen Erfolglosigkeit 2016 begraben haben. Aus diesem Scheitern haben sie bei manchen den Eindruck erweckt, es ginge ihnen nicht um die Daten."
                        * https://www.heise.de/mac-and-i/meldung/Apple-plant-neues-App-Werbenetzwerk-4063900.html

### "Aber Android wird ja von Google entwickelt"
* Na und? Solange der Code frei ist, kann denen jeder in die Karten schauen und schlechtes entfernen
    * Beispiel: LineageOS und Replicant

### "Die Leute wollen das so"
* Wenn man die Leute nicht aufklärt, wollen die das natürlich so. Wer nicht weiß, was in der Waagschale liegt, kann keine informierte Entscheidung treffen. Die Leute _vertrauen_ Ihnen!

Sonstige Aussagen
-----------------
### "Code-Qualität ist schlechter"
* Im Gegenteil: Wenn der Code öffentlich ist, wird weniger schlampig programmiert, weil es sonst peinlich ist.
* siehe auch https://www.bsi.bund.de/DE/Themen/DigitaleGesellschaft/FreieSoftware/freiesoftware_node.html

Hintergrund: Warum ist Privatsphäre wichtig?
--------------------------------------------
* [Warum Privatsphäre wichtig ist](../Hintergrund/privatsphaere.md)

Beispiel: Katastrophen-Warn-Apps
--------------------------------

### Allgemein
Für Katastrophenwarn-Apps braucht man technisch und organisatorisch:

* einen Karten-Dienst (derzeit oft Google, könnte auch OpenStreetMap sein)
* einen Push-Dienst (derzeit oft Google)
* Anwender: die Behörden, die Meldungen rausschicken
    * (Alternative: per SMS, aber dann kostet es explizit Geld)
    * (Google ist halt schön kostenlos. Die Frage ist, wer bezahlt deren Server?)
* Endnutzer: die Bürger, die die App installiert haben
* Spannungsfeld: Interessen der Anwender (günstig), Endnutzer (günstig; und wenn aufgeklärt auch Privatsphäre)

### KATWARN
* https://www.fokus.fraunhofer.de/go/katwarn
* https://www.katwarn.de/hilfe.php
* https://www.katwarn.de/partner.php - "Beitrag zum Gemeinwohl"
    * "Das Bevölkerungswarnsystem KATWARN wurde vom Fraunhofer FOKUS im Auftrag der öffentlichen Versicherer Deutschlands und der CombiRisk GmbH als Beitrag zum Gemeinwohl entwickelt."
        * Bewertung: Wenn privatsphären-bewusste Bürger die App nicht verwenden können, ist das schade und trägt zumindest derzeit nicht zum Gemeinwohl bei.
* Negativ:
    * noch nicht mal als APK-Download verfügbar

### NINA
* vom Bundesamt für Bevölkerungsschutz und Katastrophenhilfe
* Downloadseite: http://www.bbk.bund.de/DE/NINA/Warn-App_NINA_node.html
* Positiv: als APK-Download verfügbar:
    * http://www.bbk.bund.de/SharedDocs/Downloads/BBK/DE/Downloads/Sonstiges/NINA_App_Android_alternativ.html
    * benötigt leider trotzdem Google-Play-Dienste
* HTML5-App als Alternative: http://warnung.bund.de/
* SMS als Alternative?

### Fazit: SMS-Verfahren verwenden
* Solange die Google-Abhängigkeit besteht und somit eine App auf Kosten der Privatsphäre von sich und anderen geht (das ist an sich ja ein Katastrophenfall, der bereits eingetreten ist), kann man weiterhin das **SMS-Verfahren** verwenden.

Alternativen zu Google-Play-Diensten
------------------------------------
* microG: https://microg.org
    * Was fehlt, sind die Push-Dienste
* Push-Dienst-Alternativen
    * http://app-kantine.de/alternative-push-dienste-fuer-android/
    * http://stackoverflow.com/questions/15297246/are-there-alternatives-to-google-cloud-messaging-for-custom-android-builds

Positive Beispiele
------------------
* Renommiert:
    * https://www.secuso.informatik.tu-darmstadt.de/de/secuso/forschung/ergebnisse/privacy-friendly-apps
    * ...
* Sonstige:
    * c:geo
