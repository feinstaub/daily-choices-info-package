IT-Sicherheit
=============

Lose Informationssammlung.

<!-- toc -->

- [Einsteiger: Anschauliche Videos mit Beispielen](#einsteiger-anschauliche-videos-mit-beispielen)
  * [Schwachstellen von KNX-Systemen](#schwachstellen-von-knx-systemen)
  * [Tool: USB Rubber Ducky](#tool-usb-rubber-ducky)
  * [Weiteres](#weiteres)
- [Einsteiger: Sicher online & Antivirensoftware](#einsteiger-sicher-online--antivirensoftware)
- [Amtliche Seite: BSI für Bürger](#amtliche-seite-bsi-fur-burger)
  * [Freie Software](#freie-software)
  * [Basisschutz / Virenscanner unter Linux?](#basisschutz--virenscanner-unter-linux)
  * [IT-Grundschutz](#it-grundschutz)
- [Professionelle IT](#professionelle-it)
  * [2018 User-Comment aus Admin-Sicht unter Zeitdruck](#2018-user-comment-aus-admin-sicht-unter-zeitdruck)
- [Meinungen](#meinungen)
  * [Windows 10](#windows-10)

<!-- tocstop -->

Einsteiger: Anschauliche Videos mit Beispielen
----------------------------------------------
### Schwachstellen von KNX-Systemen
* [Schwachstellen von KNX-Systemen](https://www.antago.info/index.php?id=74)
    * vernetzte Gebäude
    * Industrie 4.0

### Tool: USB Rubber Ducky
* Ein Tool der Gruppe [Hak5](http://www.pentestwithhak5.com), die professionelle Penetrationstests und Schulungen anbietet.
* Für einen Angriff ist physikalischer Zugang zum Ziel-Rechner nötig:
    * Video: [zu USB Rubber Ducky](https://www.youtube.com/watch?v=sbKN8FhGnqg), englisch, 30 Sekunden
    * [heise-Video](https://www.heise.de/video/artikel/USB-Angriffe-verstehen-Pentesting-Stick-USB-Rubber-Ducky-2545315.html), 2015
    * [Detaillierte Vorführung](https://www.youtube.com/watch?v=6Ay9tpvnuJ), 2013
* **Schutzmaßnahmen:** beim eigenen Rechner den Bildschirm sperren, wenn er unbeaufsichtigt ist.

### Weiteres
* Logitech Presenter: ist wie eine Tastatur, die man per Funk mit entsprechenden Tools übernehmen kann. Dann: siehe USB Rubber Ducky


Einsteiger: Sicher online & Antivirensoftware
---------------------------------------------
Antivirensoftware - die von Nicht-Experten als wichtigste Maßnahme bei der Online-Sicherheit eingeschätzt wird - ist von Experten eher umstritten, siehe z. B. [heise-Artikel vom 30.01.2017](https://www.heise.de/security/artikel/Ex-Firefox-Entwickler-raet-zur-De-Installation-von-AV-Software-3609009.html)

![Bild](https://1.f.ix.de/security/imgs/07/2/1/3/1/4/2/8/Beutler_Google_Security-practices-v6-599e1c7f079b7a7d.png)

(Bildquelle: Google, siehe Link)


Amtliche Seite: BSI für Bürger
------------------------------
Das [Bundesamt für Sicherheit in der Informationstechnik](https://de.wikipedia.org/wiki/Bundesamt_f%C3%BCr_Sicherheit_in_der_Informationstechnik), kurz BSI, ist eine Bundesbehörde, die für Fragen der IT-Sicherheit zuständig ist.

"600 Mitarbeiterinnen und Mitarbeiter, größtenteils mit einem abgeschlossenen Hochschul- bzw. Fachhochschulstudium der Ingenieurwissenschaften, Mathematik, Informatik sowie Physik, schützen unabhängig und neutral die deutsche Informationstechnik." ([Über das BSI](https://www.bsi-fuer-buerger.de/BSIFB/DE/Service/Das_BSI/dasBSI_node.html))

Die Webseite https://www.bsi-fuer-buerger.de klärt den Bürger über IT-Risiken auf, gibt herstellerunabhängige Empfehlungen und macht sich Gedanken über das Leben in der Digitalen Gesellschaft. Bei Fragen zur IT-Sicherheit kann der Bürger sich auch telefonisch an das BSI wenden.

### Freie Software
Das BSI setzt sich auch [für den Einsatz und die Fortentwicklung von Freier Software ein](https://www.bsi.bund.de/DE/Themen/DigitaleGesellschaft/FreieSoftware/freiesoftware_node.html).

### Basisschutz / Virenscanner unter Linux?
Empfehlungen des BSI: [Wie Sie Ihren Computer sicher einrichten](https://www.bsi-fuer-buerger.de/BSIFB/DE/Empfehlungen/BasisschutzGeraet/EinrichtungComputer/EinrichtungComputer_node.html)

In einem verlinkten PDF-Dokument steht zum Linux-Betriebssystem: "Die Installation eines Virenschutzprogramms ist, basierend auf dem aktuellen Stand der Bedrohungslage in
Bezug auf Schadsoftware für Linux, [...] nicht notwendig." [PDF, 2013](https://www.bsi-fuer-buerger.de/SharedDocs/Downloads/DE/BSIFB/Publikationen/BSIe009_Ubuntu.pdf?__blob=publicationFile&v=1)

Wer dennoch einen Virenscanner einsetzten möchte, der kann unter Linux z. B. [ClamAV](https://de.wikipedia.org/wiki/ClamAV) nutzen.

### IT-Grundschutz
Für Unternehmen: [IT-Grundschutz](https://www.bsi.bund.de/DE/Themen/ITGrundschutz/itgrundschutz_node.html)

Professionelle IT
-----------------
### 2018 User-Comment aus Admin-Sicht unter Zeitdruck
* ["Milliarden vertraulicher Dokumente frei im Netz auffindbar"](https://www.heise.de/meldung/Milliarden-vertraulicher-Dokumente-frei-im-Netz-auffindbar-4012048.html), 2018
    * ["wundert mich nicht; Anspruchsdenken ./. Praxis"](https://www.heise.de/forum/heise-online/News-Kommentare/Milliarden-vertraulicher-Dokumente-frei-im-Netz-auffindbar/wundert-mich-nicht-Anspruchsdenken-Praxis/posting-32164340/show/)
    * ["Hauptsache Billig"](https://www.heise.de/forum/heise-online/News-Kommentare/Milliarden-vertraulicher-Dokumente-frei-im-Netz-auffindbar/Hauptsache-Billig/posting-32163806/show/)

Meinungen
---------
### Windows 10
* siehe z. B. https://stallman.org/microsoft.html (englisch)
