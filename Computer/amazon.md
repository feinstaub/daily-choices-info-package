Amazon
======

Unsortiert
----------
Amazon bietet ein effizientes Logistik-Sytem und Cloud-Dienstleistungen an. Fallsammlung von Dingen, die passieren können, wenn ein einzelnes Unternehmen sehr groß wird und die Geschäftsbereiche vieler kleiner Unternehmen eindringt.

* 2018: ["Amazon ermöglicht Live-Gesichtserkennung für ganze Städte"](https://www.heise.de/newsticker/meldung/Amazon-ermoeglicht-Live-Gesichtserkennung-fuer-ganze-Staedte-4055143.html)

* Video: ["Scott Galloway - The Four - What To Do"](https://www.youtube.com/watch?v=GWBjUsmO-Lw), 2017, 40 min

* 2018:
    * Smile: They https://www.gladizon.com/ueberuns.html give 3 - 6 % instead of only 0.5 %
        * see also other articles
* 2017: ["Amazon-Spendenplattform führt Foodwatch ungefragt"](https://www.golem.de/news/foodwatch-amazon-spendenplattform-fuehrt-foodwatch-ungefragt-1706-128354.html), golem.de
    * ["foodwatch warnt vor Amazon-Spendenplattform „Smile“"](http://www.foodwatch.org/de/ueber-foodwatch/aktuelles-ueber-foodwatch/foodwatch-warnt-vor-amazon-spendenplattform-smile/)
    * [Pressemitteilung Foodwatch](http://www.foodwatch.org/de/presse/pressemitteilungen/foodwatch-warnt-vor-amazon-spendenplattform-smile-kunden-werden-in-die-irre-gefuehrt-angebliche-unterstuetzung-kam-bei-foodwatch-nicht-an/)
        * "Amazon führt Menschen in die Irre, die eine gute Sache unterstützen wollen, und generiert Umsatz mit falschen Versprechen"
        * "dass das Vorgehen von Amazon auch zum Schaden von Organisationen sei, die aus guten Gründen entschieden haben, kein Geld von Amazon annehmen zu wollen"
        * Abläufe und Briefe als PDF
    * Amazon regt damit zu (oft) sinnlosem Konsum an und da die Preise dort laut einer Untersuchung der Verbraucherzentralen oft höher sind, könnte man das Geld wesentlich effizienter anders spenden
        * siehe ["Foodwatch boykottiert Amazons Spenden-Kampagne"](https://www.welt.de/wirtschaft/article165459712/Foodwatch-boykottiert-Amazons-Spenden-Kampagne.html) auf welt.de, 2017

* https://ohneamazon.wordpress.com, ca. 2014, Single-Purpose-Webseite
    * zitiert Weizenbaum über die vermeintliche Ohnmacht des Einzelnen, siehe wirksamkeit-des-einzelnen.md
    * Umfangreiche Link-Sammlung auf der rechten Seite
        * ["Versandjustiz - Souverän Amazon"](http://www.faz.net/aktuell/feuilleton/versandjustiz-souveraen-amazon-12823776.html), FAZ, 2014
            * "Kafkaesk nennt man ein Verfahren, bei dem man verurteilt wird, ohne je zu erfahren, gegen welches Gesetz man verstoßen hat. So ist die Welt auch heute, jedenfalls wenn man es mit Monopolisten zu tun bekommt."

* 2017: ["Amazon entfernt Verkäuferlogos in Angeboten – Schlecht für Händler & Kunden!"](https://www.ecomparo.de/amazon-entfernt-verkaeuferlogos-in-angeboten-schlecht-fuer-haendler-kunden/) ohne Ankündigung, 2017

* 2017: ["Streit um Produktfälschungen - Birkenstock verlässt Amazon"](http://www.tagesschau.de/wirtschaft/birkenstock-amazon-101.html)

* Überblick auf Englisch: https://stallman.org/amazon.html

IMDB
----
* Alternative: https://www.themoviedb.org/
