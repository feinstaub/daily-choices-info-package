Digitaler Konsum / daily-choices-info-package
=============================================

[zurück](../../..)

<!-- toc -->

- [Inbox](#inbox)
  * [2020: Psychotricks bei Apps](#2020-psychotricks-bei-apps)
  * [2020: Digitale Souveränität](#2020-digitale-souveranitat)
  * [2019: Digitale Souveränität vs. zentrale Kontrolle von Daten](#2019-digitale-souveranitat-vs-zentrale-kontrolle-von-daten)
  * [2017: Datenschutz](#2017-datenschutz)
- [Hintergrund](#hintergrund)
  * [Kuketz-Blog - Empfehlungsecke für Einsteiger](#kuketz-blog---empfehlungsecke-fur-einsteiger)
  * [Probleme durch Werbung](#probleme-durch-werbung)
  * [Facebook](#facebook)
  * [Cambridge Analytica: Wie Facebook-Daten verwendet werden können](#cambridge-analytica-wie-facebook-daten-verwendet-werden-konnen)
  * [Google](#google)
  * [Privatsphäre](#privatsphare)
  * [Netzneutralität](#netzneutralitat)
  * [Führende Organisationen als Vorbilder](#fuhrende-organisationen-als-vorbilder)
  * [IT-Sicherheit](#it-sicherheit)
- [Digitalisierung](#digitalisierung)
  * [Tele-Akademie - SWR Fernsehen](#tele-akademie---swr-fernsehen)
- [Lösungswege](#losungswege)
  * [Beispiel: Tagesschau-App mit F-Droid-Repo](#beispiel-tagesschau-app-mit-f-droid-repo)
  * [Freie-Software-Lizenzen verstehen](#freie-software-lizenzen-verstehen)
  * [Disconnect Werbung](#disconnect-werbung)
  * [Selber Impulse geben](#selber-impulse-geben)
  * [F-Droid auf dem Smartphone](#f-droid-auf-dem-smartphone)
  * [Bewusstsein für Computer-Sicherheit entwickeln](#bewusstsein-fur-computer-sicherheit-entwickeln)
  * [Entwicklungen 2017](#entwicklungen-2017)
  * [Chaos macht Schule](#chaos-macht-schule)
- [Gemeinnützige Vereine](#gemeinnutzige-vereine)
- [Nachhaltige IT / Green IT](#nachhaltige-it--green-it)
- [Sonstiges](#sonstiges)
  * [Briar-Messenger](#briar-messenger)

<!-- tocstop -->

Inbox
-----
### 2020: Psychotricks bei Apps
* https://www.heise.de/newsticker/meldung/Klebrige-Apps-Bundesstagsstudie-kritisiert-Psycho-Tricks-von-Entwicklern-4630680.html, 2020
    * ""Klebrige" Apps: Bundestagsstudie kritisiert Psycho-Tricks von Entwicklern"
* https://linus-neumann.de/2020/01/hirne-hacken-36c3/

### 2020: Digitale Souveränität
* "Umfrage: IT-Profis vermissen digitale Souveränität", 2020, https://www.heise.de/ix/meldung/Umfrage-IT-Profis-vermissen-digitale-Souveraenitaet-4700656.html

### 2019: Digitale Souveränität vs. zentrale Kontrolle von Daten
* ["Digitale Souveränität: Kommunale IT-Dienstleister rebellieren gegen Microsoft"](https://www.heise.de/newsticker/meldung/Digitale-Souveraenitaet-Kommunale-IT-Dienstleister-rebellieren-gegen-Microsoft-4324178.html), 2019

* ["Alexa: Wie mächtig ist Amazon? | WDR Doku"](https://www.youtube.com/watch?v=KhdEOVairJY), 2018, 45 min
    * ... Amazon kennt uns besser als wir selbst
    * "Daten sind dann gefährlich, wenn sie zentral von einer Macht kontrolliert werden"
    * ... TODO

### 2017: Datenschutz
* ["Schleswig-Holsteins Chef-Datenschützerin rät zu Vorsicht bei Smartphone-Nutzung"](https://www.heise.de/newsticker/meldung/Schleswig-Holsteins-Chef-Datenschuetzerin-raet-zu-Vorsicht-bei-Smartphone-Nutzung-3762955.html), 2017

Hintergrund
-----------
### Kuketz-Blog - Empfehlungsecke für Einsteiger
* https://www.kuketz-blog.de/empfehlungsecke/
* https://www.kuketz-blog.de/umgang-mit-daten-im-privatleben-datensouveraenitaet-teil3/, 2018
* DuckDuckGo: https://www.kuketz-blog.de/duckduckgo-datenschutz-nur-auf-dem-papier/
* https://www.kuketz-blog.de/datenschutz-nicht-labern-handeln/

### Probleme durch Werbung
* Digitale Konsumenten jeden Alters sind einer manipulierenden Werbeflut ausgesetzt.
    * Was ist daran ein Problem?
        * siehe werbung.md
        * Die Fähigkeit selbstbestimmte Entscheidungen zu treffen wird reduziert.
        * Vor allem bei verdeckter Werbung, siehe z. B. ["Get your loved ones off Facebook."](http://www.salimvirani.com/facebook/)
* IT-Sicherheit: ...

### Facebook
* Zu Facebook gehören außerdem:
    * WhatsApp
    * Instagram
* Gute Gründe, warum sich Bürger entscheiden, Facebook nicht zu verwenden:
    * Facebook ist nicht vertrauenswürdig. Die Firma hat in der Vergangenheit offensichtlich die Unwahrheit gesagt.
        * siehe z. B. Versprechen Daten nicht von WhatsApp an Facebook weiterzugeben
        * https://stallman.org/facebook.html
            * "Facebook bought WhatsApp and committed not to combine that data with Facebook's other data. Now it is going to do just that."
            * "For Facebook, any commitment is meant to be broken, after a delay for people to forget about it."
    * Zentrales Datensilo; hohes Manipulationsrisiko ganzer Bevölkerungsgruppen; siehe Cambridge Analytica
    * Abhängigkeit
    * Facebook ist nicht vertrauenswürdig, weil intransparent
        * siehe https://de.wikipedia.org/wiki/Kritik_an_Facebook (verschlossene Auster)
        * Welche Daten von einem Nutzer existieren? Wie werden diese ausgewertet? Wer bekommt sie zu sehen? Welche Algorithmen entscheiden, was er zu sehen bekommt und was nicht?
        * Wenn Facebook es erst meint, würde es für Transparenz sorgen.
    * nicht vertrauenswürdig, weil es über die Daten unbeteiligter Dritter entscheidet
        * https://stallman.org/facebook.html
            * "The Facebook app obtained useds' whole contact lists, either directly grabbing them or by tricking useds into agreeing without knowing it."
    * nicht vertrauenswürdig, weil auch über Unbeteiligte Daten gesammelt werden
        * z. B. mittels Like-Button auf Webseiten
        * z. B. mittels Auslesen von Addressbüchern
    * Diskrimimierung
        * ["Diskriminierende Wohnungsannoncen: US-Regierung geht gegen Facebook vor"](https://www.heise.de/newsticker/meldung/Diskriminierende-Wohnungsannoncen-US-Regierung-geht-gegen-Facebook-vor-4141578.html), 2018
            * "Immobilieneigentümer können Facebook-Werbung schalten, die bestimmte Zielgruppen nicht sehen."
    * Siehe https://stallman.org/facebook.html
        * ["Get your loved ones off Facebook."](http://www.salimvirani.com/facebook/)
        * Echte Namen: "Facebook is not your friend. Its 'real name' policy is enough reason to refuse to let it use you"
            * Facebook stiftet "Freunde" zum Bespitzeln an, siehe ["Facebook wants you to snitch on friends that aren’t using real names"](https://www.digitaltrends.com/social-media/facebook-snitch-on-friends-that-arent-using-real-names/), 2012
            * Wenn es Facebook gut mit den Nutzern meinte, dann sollte es mehrere Accounts pro Person zulassen: "... it will be unacceptable because companies and the state will be able to connect the account with your real identity. In order for the site not to mistreat people, it must let you have one account to show your boss and your parents, another for your friends, and others for various kinds of political activism."
        * Zensur
            * Blockage von Links
        * Unbeteiligte Dritte
            * "Facebook predicts who new useds* know, based on their phone lists and email address lists. Along with the phone and email lists of all the other useds.
                This is a measure of how complete and dangerous Facebook surveillance is.
                It implies that giving your email or phone list to a company is mistreatment of everyone in that list!"
        * Andere Personen
            * "Facebook invites useds* to nag other useds to fill in their profiles with all sorts of personal information."
                * 2014: https://arstechnica.com/information-technology/2014/05/facebook-introduces-naggy-ask-function-into-profile-pages/, peer pressure
            * "Facebook asks its useds to provide their entire list of other people's email addresses.
                This by itself is surveillance of those other people, but Facebook uses it to go further and try to guess the relationships of people who are not Facebook useds* (along with collecting their phone numbers, and email and postal addresses)."
        * Kinder
            * "Facebook has a new trick to get people to identify their spouses and babies in photos."
                * 2015: https://techcrunch.com/2015/03/31/step-1-identify-baby-photo-step-2-hide-baby-photos/
        * Werbung
            * 2012: "Facebook sends political messages as coming from people who have clicked Like buttons."
        * Psychological Harm
            * ...
        * Steuern
            * "Facebook is a tax dodger. Of course, it's not the only one, but that is no excuse."
        * Rassismus
            * "Facebook guesses the race of each used, and companies use this to show people different ads."
                * ["Facebook’s ad platform now guesses at your race based on your behavior"](https://arstechnica.com/information-technology/2016/03/facebooks-ad-platform-now-guesses-at-your-race-based-on-your-behavior/), 2016
            * "Facebook is effectively racially profiling its useds, in an indirect and deniable way."
        * Medienkontrolle
            * "Facebook advertises based on its capacity to swing the results of elections. By not letting it use us, we reduce that capacity."
            * "Facebook's corporate-only news feed both directs useds away from independent journalism and tracks their reading."
        * Verschiedenes
            * ...
            * ["Facebook: the most congenitally dishonest company in America."](https://scholarsandrogues.com/2012/06/26/facebook-the-most-congenitally-dishonest-company-in-america/), 2012
    * Steht im Konflikt mit deutschem und europäischem Recht
        * Zulassung von Holocaustleugnung Sammlung, siehe https://de.wikipedia.org/wiki/Kritik_an_Facebook
            * siehe auch https://de.wikipedia.org/wiki/Holocaustleugnung
        * [Probleme mit politischer Werbung auf Facebook / Demokratieproblem](http://faktenfinder.tagesschau.de/inland/datenschutz-facebook-101.html), 2017
            * "Hintergrund ist, dass hierbei auch Informationen über die politischen Interessen und Neigungen der Nutzer verwendet werden." Dabei handle es sich um "sogenannte besondere personenbezogene Daten, deren Verarbeitung und Nutzung nur in einem engen rechtlichen Rahmen zulässig" sei."
            * "Allerdings weigere sich Facebook "systematisch und dauerhaft, das bisher geltende deutsche Datenschutzrecht umzusetzen und die Kontrollzuständigkeit" deutscher Datenschützer anzuerkennen."
            * "Mit anderen Worten: Bisher gibt es offenbar keinerlei Handhabe gegen einen vermuteten permanenten Rechtsbruch durch Facebook."
            * "Zudem sieht Caspar noch ein anderes Problem: Dass auf Facebook "auch Parteien gezielt auf die politische Meinung Einfluss nehmen, wird der breiten Öffentlichkeit weder bewusst sein, noch werden sie in der Lage sein zu erkennen, welche Informationen verwendet werden, um Einfluss zu nehmen. Aus rechtsstaatlicher und demokratietheoretischer Sicht stellt dies ein massives Problem dar. Denn weder die Parteien noch die Öffentlichkeit werden in der Lage sein zu überprüfen, ob sich Facebook tatsächlich neutral verhält und dadurch nicht mit Eigeninteresse Einfluss auf die politische Meinungsbildung nimmt.""
            * "Zum Beispiel wäre denkbar, dass Facebook denjenigen zu mehr Reichweite verhilft, die mehr bezahlen. Bezogen auf einzelne Posts handhabt Facebook dies nämlich genauso: Je mehr man in einen Werbe-Post investiert, desto mehr Personen wird er angezeigt. Die technischen Details und Hintergründe der Facebook-Algorithmen liegen weitgehend im Dunkeln, der Konzern betrachtet sie wie alle IT-Unternehmen als Geschäftsgeheimnis. Und auch die meisten Parteien zeigten sich auf Nachfrage des ARD-Faktenfinders nicht gerade gesprächig."
    * weitere siehe https://de.wikipedia.org/wiki/Kritik_an_Facebook
    * 2018: Datenskandal
        * ...
        * ["Facebook-Chef Mark Zuckerberg hat die Einladung des Europaparlaments angenommen"](https://www.heise.de/newsticker/meldung/Facebook-Datenskandal-Zuckerberg-will-ins-Europaparlament-kommen-4051021.html), 2018
            * "und will mit Vertretern über den Umgang mit Nutzerdaten sprechen. Das wird aber nicht öffentlich zu sehen sein, denn das Gespräch findet hinter verschlossenen Türen statt."
    * 2018: ["Facebook gewährt Handy-Herstellern tiefen Einblick in Nutzerdaten"](https://www.heise.de/newsticker/meldung/Facebook-gewaehrt-Handy-Herstellern-tiefen-Einblick-in-Nutzerdaten-4063962.html)

* ["Surveillance is the business model of the internet"](https://www.opendemocracy.net/digitaliberties/agne-pix-bruce-schneier/surveillance-is-business-model-of-internet), Agne Pix and Bruce Schneier 18 July 2017

* ["98 Daten, die Facebook über dich weiß und nutzt, um Werbung auf dich zuzuschneiden"](https://netzpolitik.org/2016/98-daten-die-facebook-ueber-dich-weiss-und-nutzt-um-werbung-auf-dich-zuzuschneiden/), 2016
    * "Aus 98 unterschiedlichen Datenpunkten versucht Facebook, Zielgruppen-gerechte Werbung auszuliefern. Vielen ist nicht bekannt, was das Unternehmen alles über einen gesammelt haben könnte. Wir haben eine lange Liste."
    * "Nutzer wissen schon länger, dass unter anderem „gelikte“ Seiten zu Werbezwecken analysiert werden. Überraschend sind jedoch die Online-Tracking-Bemühungen und die Zusammenarbeit mit großen Datendealern der Social-Media-Plattform. Jedes Mal, wenn eine Internetseite besucht wird, die mit einem „gefällt mir“- oder „teilen“-Button versehen ist, weiß Facebook hierüber Bescheid. Desweiteren arbeitet das Unternehmen mit Firmen wie Epsilon und Acxiom zusammen, welche Daten aus behördlichen Akten und Umfragen sammeln, und nutzt gewerbliche Quellen (wie zum Beispiel Listen von Magazinabonnenten), um mehr über Facebook-Nutzer zu erfahren."
    * "Die Washington Post hat auf Grundlage des Werbeportals analysiert, welche Daten Facebook in den USA sammelt beziehungsweise Werbern als Zielgruppe anbieten kann."
    * Zitat: "Es ist Facebooks Geschäftsmodell, so viele direkte und indirekte Daten wie möglich über Nutzer anzusammeln und dann zu bestimmen, wem es zu welchem Preis Zugang zu diesen gewährt. Wenn man Facebook benutzt, vertraut man dem Unternehmen Aufzeichnungen über alles an, was man tut. Ich denke, die Leute haben Gründe, darüber besorgt zu sein."
    * User-Comments:
        * "Und dass persönliche Daten nicht an Werbepartner weitergegeben werden, bedeutet noch lange nicht, dass sie nicht verfügbar sind, für wen auch immer."
    * Ist das überhaupt plausibel?
        * siehe Portal für Werbetreibende
        * siehe Buch: "Yvonne Hofstetter - Sie wissen alles - Wie intelligente Maschinen in unser Leben eindringen und warum wir für unsere Freiheit kämpfen müssen", 2014
            * "Yvonne Hofstetter hat aufsehenerregende Artikel in Medien wie der FAZ publiziert"
             * siehe auch Buch_: "Das Ende der Demokratie - Wie die künstliche Intelligenz die Politik übernimmt und uns entmündigt", 2018
                * "Yvonne Hofstetter warnt: Die Rückkehr in eine selbst verschuldete Unmündigkeit hat begonnen, auch wenn sie in smartem selbstoptimierendem Gewand daherkommt."
                * "dass „Wenn der Computer Geld verdient“ klassische, demokratische Regulations- und Kontrollmechanismen außer Kraft gesetzt werden. Gerade was die gedankliche „Freiheit“ des Handels mit Nutzer-Daten in einem radikal liberalen Wirtschaftsverständnis angeht."

* Folgefrage: Wertet Facebook auch Postings in privaten Gruppen aus?
    * siehe User-Comment oben
    * Like-Buttons auf Webseiten

* Prekäre Situation von "Content Moderators"
    * https://www.reuters.com/article/us-facebook-crime/facebook-tries-to-fix-violent-video-problem-with-3000-new-workers-idUSKBN17Z1N4, 2017
    * http://www.dailymail.co.uk/news/article-4548898/Facebook-young-Filipino-terror-related-material-Manchester.html, 2017
    * siehe Buch_: Digitale Drecksarbeit, ...
        * ...
        * Philippinen, siehe auch demokratie.md
        * ...

* https://www.heise.de/newsticker/meldung/VDZ-Praesident-fordert-Schranken-fuer-Netzaktivitaeten-von-ARD-und-ZDF-3914842.html, 2017
    * "Wenn Sender oder Verlage außerdem ihre Inhalte bei Facebook einstellen, bekommen sie eine größere Reichweite, und Facebook wird gleichzeitig bedeutender. Das ist ein sich selbst nährendes System, eine Hydra."
* 2011: ["Facebook: Deine Daten gehören(!) uns. (Wirklich.)"](https://netzpolitik.org/2011/facebook-deine-daten-gehoren-uns-wirklich/)

### Cambridge Analytica: Wie Facebook-Daten verwendet werden können
* ["Cambridge Analytica - The Power of Big Data and Psychographics"](https://www.youtube.com/watch?v=n8Dd5aVXLCc), "Concordia", 2016
    * Speaker: Mr. Alexander Nix, CEO, Cambridge Analytica
    * ["Cambridge Analytica whistleblower: 'We spent $1m harvesting millions of Facebook profiles'"](https://www.youtube.com/watch?v=FXdYSQ6nu-M), The Guardian, 2018
    * Meldung: ["Nach Datenskandal: Facebook erwartet weitere Fälle von Datenmissbrauch"](https://www.heise.de/newsticker/meldung/Nach-Datenskandal-Facebook-erwartet-weitere-Faelle-von-Datenmissbrauch-4037351.html), 2018
* ["Mit Zugriff auf Nutzerdaten angegeben: Facebook entlässt Mitarbeiter"](https://www.heise.de/newsticker/meldung/Mit-Zugriff-auf-Nutzerdaten-angegeben-Facebook-entlaesst-Mitarbeiter-4041047.html), heise.de, 2018
    * "Nachdem er offenbar auf Tinder mit seinem Zugang zu persönlichen Informationen von Nutzern geprahlt hatte, hat Facebook Berichten zufolge einen seiner Mitarbeiter entlassen."
* "Doku „The Great Hack“": https://www.deutschlandfunkkultur.de/netflix-doku-ueber-cambridge-analytica-wenn-daten-die-welt.1013.de.html?dram:article_id=454606, 2019
* Was war der Skandal?
    * https://de.wikipedia.org/wiki/Cambridge_Analytica, 2019
        * "gegründetes Datenanalyse-Unternehmen, das im Mai 2018 Insolvenz anmeldete. Es hat seinen Hauptsitz in New York City und sammelte und analysierte in großem Stil Daten über potentielle Wähler mit dem Ziel, durch individuell zugeschnittene Botschaften das Wählerverhalten zu beeinflussen (Mikrotargeting)"
        * "Datenbeschaffung - Im März 2018 wurde durch den Whistleblower Christopher Wylie bekannt, dass Cambridge Analytica seine Aktivitäten auf Datensätze stützte, die die Muttergesellschaft SCL 2014 von einem Unternehmen namens Global Science Research (GSR) erworben hatte. GSR wurde von Aleksandr Kogan[11] betrieben, einem Psychologen an der Universität Cambridge. Kogan hatte mittels einer App – angeblich zu wissenschaftlichen Zwecken – und mit einem kleinen finanziellen Anreiz Persönlichkeitstests mit amerikanischen Facebooknutzern durchgeführt, deren Teilnehmer am Ende jedes Tests einem Zugriff auf ihre Profile und die ihrer Kontakte zustimmten. So erlangte Kogan mit 320.000 solcher Tests im Schnitt jeweils etwa 160 weitere Datensätze von Facebookprofilen, deren Inhaber davon keine Kenntnis hatten. Die insgesamt über 50 Millionen Datensätze, für deren Erstellung SCL etwa eine Million Dollar zur Verfügung gestellt hatte, bildeten die Grundlage für die Arbeit von Cambridge Analytica in US-Wahlkämpfen."
    * Wahlbeeinflussung: "Cambridge Analytica Uncovered: Secret filming reveals election tricks" (https://www.youtube.com/watch?v=mpbeOCKZFfQ): Es geht darum, möglichst viel über die Menschen zu wissen, insbesondere über Ängste und Sorgen, um darauf aufbauend Wahlkampagnen aufzubauen, die diese Emotionen bedienen (Fakten spielen bei Wahlkämpfen keine Rolle).
        * Liste von Ländern, wo CA aktiv war: https://en.wikipedia.org/wiki/Cambridge_Analytica#Elections
* https://en.wikipedia.org/wiki/Cambridge_Analytica#Genocide
    * "There is proof of human rights abuse and propaganda related to genocide planned within the company trialed and guilty of heinous crime. According to whistleblowers previously employed at Cambridge Analytica and many news sources there was promotion of hate speech with the usage of social engineering and methods of psychological warfare via social media platform Facebook in the Myanmar. The company has been repeatedly found guilty of implementing methods of psychological warfare for profit and the puppeteering of political campaigns.[155] These violations in Myanmar lead to racial hatred and fueled a genocide within the country. [156] After hacking millions of accounts and collecting user data the misuse of social media resulted in hundreds of thousands of Rohingya Muslims fleeing violence by individuals manipulated psychologically via social media in the country. [157] A Reuters investigation found more than 1,000 incidences of posts in Burmese attacking Rohingyas and Muslims generally. With some posts not removed for years. [158]"
        * Quellen:
            * https://www.trtworld.com/magazine/a-recent-history-of-facebook-scandals-26157
            * https://www.theguardian.com/world/2018/apr/03/revealed-facebook-hate-speech-exploded-in-myanmar-during-rohingya-crisis

### Google
* Übersicht: https://de.wikipedia.org/wiki/Kritik_an_der_Google_LLC
* Standortdaten
    * ["Google Maps sammelt unerlaubt ihre Standort-Daten: Jetzt droht dem Konzern die erste Sammelklage"](https://www.chip.de/news/Google-Maps-sammelt-Standort-Daten-Konzern-droht-die-erste-Sammelklage_146558983.html), 2018
    * ["Google weiß, wo Sie sind"](https://www.sueddeutsche.de/digital/digitale-privatsphaere-google-weiss-wo-sie-sind-1.3859023), 2018

### Privatsphäre
* Speziell Software: bei den meisten beworbenen Softwareprodukten wird die Auswirkung auf das Recht der Privatsphäre grob missachtet.
    * Das zeugt von mangelndem Problembewusstsein oder vorsätzlicher Täuschung oder einer Kombination beidem.
    * Beispiele:
        * Android-Apps, die nur mit Google zusammen funktionieren, ohne z. B. eine F-Droid-Alternative bereitzustellen
            * Warn-Apps
            * Gesundheitsapps
            * Apps für den öffentlichen Nahverkehr
            * Banking-Apps
* Mehr: [Warum Privatsphäre wichtig ist](../Hintergrund/privatsphaere.md)
    * Wenn die Verkäuferin eine App wäre

### Netzneutralität
* Video: ["Burger King erklärt dir, warum Netzneutralität wichtig ist"](https://www.youtube.com/watch?v=ltzy5vRmN8Q), 2018, 3 min
    * http://www.business-punk.com/2018/01/burgerkingnetzneutralitaet
    * "Whopper neutrality was repealed"
* 2017: http://www.tagesschau.de/ausland/internet-daten-netzneutralitaet-101.html
    - Streamingdienste bestimmter ausgewählter Anbieter sollen bevorzugt werden, indem sie nicht auf das monatliche Datenvolumen angerechte werden
    - "Denn zu Beginn sind zahlreiche große, namhafte Partner mit an Bord: Apple, Amazon, funk, Napster, Netflix, Sky, YouTube und das ZDF zum Beispiel. Wer die Dienste dieser Partner nutzt, schont sein Datenvolumen. Das bedeutet aber auch: Wer andere Dienste unterwegs nutzt, muss sein teures Datenvolumen verbrauchen - eine indirekte Maut sozusagen. Das birgt die Gefahr, dass Nutzer nur noch die Dienste verwenden, die kein Datenvolumen verbrauchen."
    - "Doch zahlreiche Kritiker fürchten, dass ein Ende der Netzneutralität letztlich auch die Demokratie und die freie Meinungsäußerung gefährden könnte. Maut auf der Datenautobahn - das Netz scheint sich weiterzuentwickeln: zu einem "Zwei-Klassen-Internet"."
    - Das kann der Konsument tun: diesen Tarif nicht verwenden und ggf. zu einem anderen Mobilfunk-Anbieter wechseln.
* Lösungswege:
    * Konsument kann auf FreieSW bestehen, denn diese Community setzt sich unter anderem für Netzneutralität ein.

### Führende Organisationen als Vorbilder
* Derzeit problematische Zustände:
    * z. B. [Microsoft](microsoft.md)
    * z. B. [Amazon](amazon.md)
    * z. B. Apple: Lobby gegen Recht für Reparatur, Walled garden, Zentralismus
* Verbesserungspotential:
    * Software-Angebot von öffentlich-rechtlichen Stellen
    * App-Angebot von gesetzlichen Krankenkassen
    * App-Angebot von Banken
* in D.:
    * Tagesschau-App, siehe unten
    * nextCloud
    * SUSE

### IT-Sicherheit
* Banking
    * 2017: "Kriminelle Hacker haben Konten von deutschen Bankkunden über Sicherheitslücken im Mobilfunknetz ausgeräumt, die seit Jahren bekannt sind.", [heise.de](https://www.heise.de/newsticker/meldung/Deutsche-Bankkonten-ueber-UMTS-Sicherheitsluecken-ausgeraeumt-3702194.html), 03.05.2017

Digitalisierung
---------------
### Tele-Akademie - SWR Fernsehen
* 2019
    * Prof. Miriam Meckel: Mein Kopf gehört mir - Die schöne neue Welt des Brainhacking
    * Prof. Dr. Heinz Bude - Soziale Ungleichheiten in der Zukunft
        * "Eine tiefe Spaltung zieht sich durch unsere Gesellschaft. Müssen wir uns vom Traum einer gerechten Gesellschaft verabschieden? Immer mehr Menschen sind von den Segnungen des Wohlstands ausgeschlossen und haben keine Hoffnung mehr, dass sich daran etwas ändert."
    * Prof. Dr. Rainer Mausfeld: Elitedemokratie und Meinungsmanagement
    * Prof. Dr. Stefan Schmidt: Dem Zeitdruck entkommen - Muße und Achtsamkeit als Basis ärztlichen Handelns
    * Prof. Dr. Niko Paech: Befreiung vom Überfluss - Auf dem Weg in die Postwachstumsökonomie
    * Prof. Dr. Harald Lesch: Die digitale Diktatur
        * todo

Lösungswege
-----------
### Beispiel: Tagesschau-App mit F-Droid-Repo
* siehe f-droid-project.md

### Freie-Software-Lizenzen verstehen
* Was sind freie Software-Lizenzen?
    * ...
* Welche Vorteile haben sie im Vergleich zu proprietären Lizenzen; gerade im öffentlichen Sektor?
    * ...
* http://www.thelins.se/johan/blog/2020/04/what-a-license-track/, 2020, todo
    * "Why the GPL is great for business"
    * Reuse
    * Recht
    * ...

### Disconnect Werbung
* Für wichtige Dienste (wie E-Mail) dafür bezahlen, dass es werbefrei ist.
* Schrittweise auf [Freie Software](../FreieSoftware) umstellen.

### Selber Impulse geben
* [Impulse durch Endnutzer](impulse-durch-endnutzer.md)

### F-Droid auf dem Smartphone
* F-Droid ist [Freie Software](../FreieSoftware)
* siehe f-droid-project.md

### Bewusstsein für Computer-Sicherheit entwickeln
* [Alternativen zu Anti-Viren-Software – Snakeoil Teil3](https://www.kuketz-blog.de/alternativen-zu-anti-viren-software-snakeoil-teil3/), kuketz 2017
* ...

### Entwicklungen 2017
* DNS-Server ohne Protokollierung: https://www.kuketz-blog.de/dns-unzensierte-und-freie-dns-server-ohne-protokollierung/
    * Digitalcourage
    * Chaos Computer Club
* Google Web-Services: https://www.kuketz-blog.de/das-kranke-www-stop-using-google-web-services, 2017
    * https://www.kuketz-blog.de/firefox-send-nachtrag-zur-verschluesselung/
        * Webbkoll: ["Webbkoll: Wie datenschutzfreundlich ist deine Webseite?"](https://www.kuketz-blog.de/webbkoll-wie-datenschutzfreundlich-ist-deine-webseite/)
            * https://webbkoll.dataskydd.net
                * Code: https://github.com/andersju/webbkoll
                    * built on Elixir: https://elixir-lang.org/
                    * https://de.wikipedia.org/wiki/Erlang_(Programmiersprache)
        * https://netzpolitik.org/2017/wie-die-tech-konzerne-die-struktur-des-web-veraendern/
            * siehe [Amazon][amazon.md]
        * Framasoft
            *  https://www.konradlischka.info/2017/02/blog/how-a-french-association-with-6-employees-offers-mainstream-users-free-and-libre-alternatives-to-facebook-groups-slack-skype-and-the-like/
            * Framadrop, https://framadrop.org/
                * " Da sind wir wieder beim beliebten Thema »Krypto im Browser«. Kann man mögen, muss man aber nicht."
                    * ["What’s wrong with in-browser cryptography?"](https://tonyarcieri.com/whats-wrong-with-webcrypto), 2013
                        * ["Javascript Cryptography Considered Harmful"](https://www.nccgroup.trust/us/about-us/newsroom-and-events/blog/2011/august/javascript-cryptography-considered-harmful/), 2011

### Chaos macht Schule
* https://www.ccc.de/en/schule
    * "Ziel des Projekts ist es, Schüler, Eltern und Lehrer in den Bereichen Medienkompetenz und Technikverständnis zu stärken."
    * Mailingliste
    * CodiMD: https://md.darmstadt.ccc.de/

Gemeinnützige Vereine
---------------------
* https://digitalcourage.de
* https://fsfe.org
* [Digitale Gesellschaft e. V.](https://digitalegesellschaft.de)

Nachhaltige IT / Green IT
-------------------------
* Unsortiert
    * http://blog.faire-computer.de/
        * http://blog.faire-computer.de/was-kaufen/ - "Ich möchte ein möglichst faires Gerät haben. Was soll ich kaufen?"
        * http://blog.faire-computer.de/aktiv-werden/ - Aktiv werden
    * https://green-cyborg.org/
    * Gebrauchte Business-Notebooks
        * https://www.notebookgalerie.de

Sonstiges
---------
### Briar-Messenger
* https://briarproject.org/about-us/
