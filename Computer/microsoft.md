Microsoft
=========

<!-- toc -->

- [Einleitung](#einleitung)
- [Weitere Medienartikel](#weitere-medienartikel)
  * ["Microsoft will Militär und Geheimdienste beliefern"](#microsoft-will-militar-und-geheimdienste-beliefern)
  * [Keine Treuhänder-Cloud mehr, alles zentral](#keine-treuhander-cloud-mehr-alles-zentral)
  * [Mitarbeiter-Spionage](#mitarbeiter-spionage)

<!-- tocstop -->

Einleitung
----------
...produziert unter großem Ressourcen-Einsatz brauchbare Softwareprodukte.
Die genaue Funktionsweise darf zwar aufgrund von fehlenden freien Lizenzen meist nicht eingesehen werden,
aber der für den Benutzer sichtbare Funktionsumfang bei einigen Flagschiff-Produkten spricht für sich.
Die teilweise subtile Inkompatibilität mit freien Software-Lösungen auch.

Bei der Geschäftspolitik gibt es allerdings einige Stellen, die noch verbessert werden sollten, bevor der Einsatz von Microsoft-Produkten im Privatumfeld und in öffentlichen Einrichtungen zu empfehlen ist.

Im Folgenden werden Medienberichte gesammelt, die zeigen, wo Microsoft noch an sich arbeiten muss, um zu zeigen, dass ihm die Freiheit der Benutzer am Herzen liegen. Bis es soweit ist, sollte jeder engagierte Bürger und Politiker jetzt auf freie Software setzen.

* Inbox
    * ARD-Doku_: Das Microsoft-Dilemma

* 2019: "Windows 10 Home 1909: US-Nutzer brauchen Microsoft-Konto zur Installation"
    * https://www.heise.de/newsticker/meldung/Windows-10-Home-1909-gibt-fuer-US-Nutzern-Microsoft-Konto-bei-Installation-vor-4594207.html

* 2019: Wie Microsoft (als Beispiel für Ausnutzung von Marktmacht) den Wettbewerb untergrub/gräbt und mehr
    * https://valdyas.org/fading/hardware/why-we-shouldnt-blame-ourselves-for-the-linux-desktops-microscopic-marketshare/, 2019
        * ...
        * "crime has directly harmed us, the people working on free software, on the Linux Desktop"
        * ...
        * "Bill Gates made it so that his company would tax every computer sold no matter whether it ran Windows or not."
            * https://en.wikipedia.org/wiki/Bundling_of_Microsoft_Windows
        * "Hijacking the World: The Dark Side of Microsoft" - http://www.dicosmo.org/HoldUp/English/hijacking_the_world/
            * ...

* 2019: "Microsoft loves Linux"
    * https://itsfoss.com/windows-linux-kernel-wsl-2/, 2019
        * "The so-called ‘love for Linux’ seems more like ‘lust for Linux’ to me"
        * "The WSL has the capacity of shrinking (desktop) Linux to a mere desktop app in this partnership"
        * "Microsoft loves Linux. Microsoft loves Open Source. It’s love is so deep that it open sourced the magnificent Windows calculator so that all of us Linux users could run this marvel of a technology."
        * "Microsoft is one of the biggest contributors to open source project on GitHub (platform now owned by Microsoft). But if you look at their projects, you’ll notice that almost all of the Microsoft’s open source products are aimed at programmers and software developers."
        * "This is not love, Microsoft and Linux. This is merely a relationship of convenience."

* 2018: "Microsoft loves Linux"
    * "As Long as Software Patents Are Granted and Microsoft Equips Trolls With Them, **“Azure IP Advantage” is an Attack on Free/Libre Software** ", 2017
        * http://techrights.org/2017/02/27/microsoft-novell-v2-via-azure/, via https://stallman.org/microsoft.html

* 2017: ["Windows 10 S: Nur Apps, aber kein Linux"](https://www.heise.de/newsticker/meldung/Windows-10-S-Nur-Apps-aber-kein-Linux-3719236.html), heise.de, 21.05.2017
    * "Die Spezialausgabe für Schüler und Studenten, Windows 10 S, soll die Installation von Apps nur aus dem Microsoft Store erlauben. Das gilt allerdings nicht für alle Apps: Die für den Store versprochenen Linux-Distributionen bleiben ausgenommen."
    * Bewertung:
        * Schülern sollte nicht beigebracht werden, dass es nur einen Store gibt, wo man Software beziehen kann. Offenheit und Softwarevielfalt ist ein wichtiges Bildungsziel, siehe BSI zu freier Software.
        * GNU/Linux sollte nicht ausgegeschlossen, sondern gerade im Bildungsbereich explizit gefördert werden. Transparenz und Erlaubnis zur Analyse der eigenen Software sind Grundlage für eine fundierte digitale Bildung.

* 2017: ["Mehr Datenschutz für Windows 10"](https://www.heise.de/newsticker/meldung/Mehr-Datenschutz-fuer-Windows-10-3732348.html)
    * "Windows 10 lässt sich durchaus so konfigurieren, dass es nicht mehr nach Hause telefoniert – Microsoft erläutert sogar, wie."
    * z. B. für unsere Post würde das so lauten, wenn sie wie Microsoft vorgehen würde: "Wir haben einen tollen Dienst, um Ihre Briefe zu transportieren. Wenn Sie interessiert sind und eine Anleitung von uns im Internet befolgen, dann hören wir sogar auf, Ihre Post mitzulesen, sondern nur noch ein ganz kleines bisschen. Sie sehen, wir strengen uns richtig an, denn Ihre Privatsphäre liegt uns sehr am Herzen."

* permanent: EU-Lobbyismus
    * siehe [Lobbypedia](https://lobbypedia.de/wiki/Microsoft) von Lobby-Control
    * fehlende Bereitschaft, für eine Vielfalt statt Monokultur in der Softwarelandschaft einzutreten. Dies ist aber eine wichtige Voraussetzung für breite IT-Sicherheit (siehe z. B. Ransomware-Würmer wie Wannacry 2017 auf Windows7-Rechnern)
    * Sonstiger Lobbyismus mit fragwürdigen Methoden
        * https://lobbypedia.de/wiki/Microsoft#Astroturf-Kampagne_C4C
        * https://en.wikipedia.org/wiki/Campaign_for_Creativity

* permanent: Lobbyismus an Schulen
    * siehe Komplettausstattung deutscher Schulen mit Windows, obwohl für Bildungszwecke GNU/Linux viel besser geeignet ist
    * siehe neues Windows 10 S 2017
    * siehe [Lobbypedia](https://lobbypedia.de/wiki/Microsoft)

* Lobbyismus bei Städten
    * 2017: z. B. beauftragte Studie, die zur Absetzung des gut funktionierenden Limux in München führen könnte
        * https://www.mehr-freiheit.org/limux
            * "Öffentliche Infrastruktur muss unabhängig von kommerziellen Einzelinteressen, die bekanntermaßen Innovationen ausbremsen, bleiben. Freie Software bietet die einmalige Möglichkeit in gemeinsame Werte zu investieren und von den Beiträgen aller zu profitieren, während die Kontrolle, was wann eingesetzt wird, erhalten bleibt. Lokale Dienstleister, die in gesundem Wettbewerb miteinander stehen, stärken die lokale Wirtschaft und sichern die optimale Nutzung von Steuergeldern."
        * 2019: https://www.heise.de/tp/features/Fuer-die-Zukunft-4422201.html
            * "Christine Prayon erhielt den Dieter-Hildebrandt-Preis der Stadt München, prangert den Ausstieg der Stadt aus dem Open-Source-Projekt LiMux an und spendet das Preisgeld an die Free Software Foundation Europe"
        * 2019: "Südkoreas Regierung migriert von Windows 7 nach Linux" - https://www.heise.de/newsticker/meldung/Suedkoreas-Regierung-migriert-von-Windows-7-nach-Linux-4425455.html

* 2015: ["Microsoft Loves Linux"](https://blogs.technet.microsoft.com/windowsserver/2015/05/06/microsoft-loves-linux/)
    * 2017: Sie lieben es so sehr,
        * dass 2017 im Windows-10-S-Store die Linux-VMs gesperrt sind
        * dass die Microsoft-Cloud sich nicht richtig einen Linux-Desktop integrieren lässt
    * Schade, dass die Liebe so schnell wieder verblasst.

* 2017: Entwicklung von Überwachungswerkzeugen mit "Demokratisierung" als Rechtfertigung
    * und Bereitstellung von Diensten dazu
    * todo: link zum heise-Artikel

* Übersicht in Englisch:
    * https://stallman.org/microsoft.html

Weitere Medienartikel
---------------------
### "Microsoft will Militär und Geheimdienste beliefern"
* 2018: https://www.heise.de/newsticker/meldung/Microsoft-will-Militaer-und-Geheimdienste-beliefern-4205383.html
    * "Microsoft ist trotz Protesten von Mitarbeitern bereit, dem Militär und den Geheimdiensten des Landes KI-Systeme und sonstige Technologien zu verkaufen."
* siehe demokratie.md / http://autonomousweapons.org

### Keine Treuhänder-Cloud mehr, alles zentral
* ["Auslaufmodell: Microsoft Cloud Deutschland"](https://www.heise.de/newsticker/meldung/Auslaufmodell-Microsoft-Cloud-Deutschland-4152650.html), 2018
    * "Ein Statement, wie sich Microsoft selbst im Fall eines Datenabflusses –etwa an das Department of Homeland Security – verhalten wird, haben wir bislang nicht in der Veröffentlichung entdeckt." => keine Eigen-Kontrolle möglich
    * siehe User-Comments
        * Es geht in Richtung Zentralisierung.
        * "Das ist also wenn Microsoft von Treuhandverträgen spricht, Herr Reiter"
        * "Damit müßte eigentlich die Rückmigration in München Geschichte sein - Denn hochsensible Bürger-Daten in einer weltweiten Cloud, die nicht mehr unter deutscher Kontrolle steht, das wissen hoffentlich unsere Datenschützer zu verhindern."

### Mitarbeiter-Spionage
* ["Microsoft Office 365 nimmt Mitarbeiter unter die Lupe"](https://www.heise.de/ix/meldung/Microsoft-Office-365-nimmt-Mitarbeiter-unter-die-Lupe-3765394.html), 2017
    * [User-Comment zu Google-Mail](https://www.heise.de/forum/iX/News-Kommentare/Microsoft-Office-365-nimmt-Mitarbeiter-unter-die-Lupe/Re-Big-Brother-Award-Winner/posting-30657799/show/)
