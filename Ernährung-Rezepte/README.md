Rezepte
=======

Vegane Rezepte. [Gute Gründe für Zutaten aus ökologischer Landwirtschaft](../Bio).

Inhalt
------
* [Herzhaft - basis](herzhaft-basis.md)
* [Herzhaft - mehr](herzhaft-mehr.md)
* [Salat und Zubehör](salat.md)
* [Kuchen](kuchen.md)
* [Nachtisch und Süßes](nachtisch.md)
* [Brot](brot.md)
* Fortgeschritten: [Zutaten-Watch](zutatenwatch.md)
* Alte Sammlung bis 2016
    * Hinweis: Diese Sammlung wurde ohne Berücksichtigung der [Zutaten-Watch-Liste](zutatenwatch.md) erstellt.
    * [Rezepte, Einstieg, Bilder](foodstruct/index.md)
    * [Drinks](foodstruct/foodfluid.md)
    * [Auswärts](foodstruct/plantstruct.md)
    * [Basics](foodstruct/basics.md)
    * [Tmp](foodstruct/tmp.md)

<!-- toc -->

- [Rezepte](#rezepte)
  * [Kochbücher](#kochbucher)
  * [Heil-Rezepte bei Krankheit](#heil-rezepte-bei-krankheit)
  * [Blogs](#blogs)
  * [Bücher](#bucher)
  * [Tipps](#tipps)
- [Zutaten-Benefits](#zutaten-benefits)
  * [Hafer](#hafer)
- [Ideen](#ideen)

<!-- tocstop -->

Rezepte
-------
### Kochbücher
* Vegan Lecker Lecker
* [The Vegan Zombie](http://www.ventil-verlag.de/titel/1588/the-vegan-zombie-deutsche-ausgabe)
* Raw Soul Food

### Heil-Rezepte bei Krankheit
* Befreiung der Atemwege mit Inhalation / Entspannung
    * Wasser in einem Kopf kochen + Thymian(tee)
    * mit Handtuch über dem Kopf atmen
* Plantago bei Juckreiz und Entzündungen

### Blogs
* https://www.eat-this.org
    * Tipp von O.
    * https://www.eat-this.org/ueber-uns/
        * ...
        * https://de.wikipedia.org/wiki/Sriracha-Sauce

* https://veganheaven.de/Rezepte/green-monster-veggie-burger/
    * Green Monster Veggie Burger

### Bücher
* Buch: Kochbuch: Peace Food Vegano Italiano, inkl. Ethik und Kultur, ::frieden

### Tipps
* Allgemeine Tipps zum Veganmachen von Rezepten / Ei-Ersatz, Eiersatz
    * http://www.v-heft.de/index.htm?http://www.vheft.de/inhalte/themen/eier.htm
        * rechts in der Mitte, Unterscheidung zwischen Binden und Treiben

Zutaten-Benefits
----------------
### Hafer
* laut Haferflockenpackung: "hoher Vitamin B1-, Magnesium- und Zinkgehalt; enthält Eisen; enthält Beta-Gluacane"
    * B1: Energiestoffwechsel, Nervensystem, Psyche

Ideen
-----
* Ideen für Aktionen
    * **Veganer Döner**
        * Beispiel Mössinger Firma Topaz, Video: [Beitrag vom SWR über Seitan](http://www.ardmediathek.de/tv/Landesschau-Baden-W%C3%BCrttemberg/Seitan-Wurst-f%C3%BCr-Vegetarier/SWR-Baden-W%C3%BCrttemberg/Video?bcastId=250286&documentId=23994028), 4 min, 2014, (Seitan - Wurst für Vegetarier-23994028.mp4), tag:class2018
            * lange Tradion in Fernost
            * Gründer ist besondere Persönlichkeit
            * Wheaty
            * seit 20 Jahren
            * auch Metzger arbeiten mit, weil die Verwurstungsprozesse gleich sind
            * und lecker Geschnetzeltes
        * Rezept?
