Herzhaft - Basis
================

Vegane Rezepte; am besten mit Zutaten aus [pestizid-freier Landwirtschaft](../Bio) zubereitet.

<!-- toc -->

- [Reis, Sojasauce, Birnen](#reis-sojasauce-birnen)
- [Hirse-Grühnkohl mit schwarzen Bohnen](#hirse-gruhnkohl-mit-schwarzen-bohnen)
- [Mehlsuppe](#mehlsuppe)
- [Zwiebel-Pilz-X](#zwiebel-pilz-x)
- [Tsampa-Brei 2](#tsampa-brei-2)
- [Tsampa-Brei](#tsampa-brei)
- [Peperoni-Leinöl-Dip](#peperoni-leinol-dip)
- [Müsli Kaffee-Birne](#musli-kaffee-birne)
- [Hirsegries mit Sojasauce](#hirsegries-mit-sojasauce)
- [Kürbis Spezial](#kurbis-spezial)
- [Barbecue-Sauce](#barbecue-sauce)
- [Vollkorn-Dinkel-Couscous mit Pflaumen und Bohnen](#vollkorn-dinkel-couscous-mit-pflaumen-und-bohnen)
- [Tsampa als Verdicker - schneller Tofu-Zwiebel-Topf](#tsampa-als-verdicker---schneller-tofu-zwiebel-topf)
- [Currypfanne mit Süßkartoffeln](#currypfanne-mit-susskartoffeln)
- [Sellerie-Würfel-Curry mit Erdnussoße](#sellerie-wurfel-curry-mit-erdnussosse)
- [4P](#4p)
- [Kräuter-Quark](#krauter-quark)
- [Salat-Aufstrich](#salat-aufstrich)
- [Eine Soße](#eine-sosse)
- [Tofu - knusprig angebraten](#tofu---knusprig-angebraten)
- [Schnellmahlzeit: Soja-Schnetzel + Gemüsepüree](#schnellmahlzeit-soja-schnetzel--gemusepuree)
- [Gemüsepfanne mit Sojaschnetzel, mit Reis](#gemusepfanne-mit-sojaschnetzel-mit-reis)
- [Haferbrei Krankenmahlzeit](#haferbrei-krankenmahlzeit)
- [Verschiedenes](#verschiedenes)
- [Gewürz-Sammlung](#gewurz-sammlung)

<!-- tocstop -->

Reis, Sojasauce, Birnen
-----------------------
Oktober 2021, von selbst

* Oktober ist Birnenzeit
* 1x Natur-Risotto-Reis
* Soja-Sauce drüber zum Würzen
* dazu süße Birnen

Hirse-Grühnkohl mit schwarzen Bohnen
------------------------------------
Januar 2021, von selbst, warm + schnell

* Hirseflocken nach Rezept zu einem Brei kochen
    * Wasser erhitzen, dann Hirseflocken dazu
* Grünkohl aus dem Glas dazu
* ggf. Salz oder Sojasauce
* separat:
    * 1 Dose schwarze Bohnen
    * Sesam
    * Apfelessig
    * Lein/Olivenöl
    * Gemüsegewürz


Mehlsuppe
---------
April 2020, Juli 2020

* https://de.wikipedia.org/wiki/Mehlsuppe
* Basisrezept: https://www.chefkoch.de/rezepte/2747201426791268/Mehlsuppe.html
* Mehl sollte nich roh gegessen werden
    * im Gegensatz zu Tsampa: "Tsampa ist kein Mehl in herkömmlichem Sinn (auch wenn es so aussieht).
        Da die Körner beziehungsweise Erbsen bereits geröstet wurden, kann Tsampa ohne weiteres ungekocht genossen werden."
        (https://www.tsampa.ch/rezepte)

* 2 T Wasser mit Salz aufkochen
* 1/2 T Wasser + ca. 75 g Vollkorn-Mehl (2 gehäufte Esslöffel), mit Schneebesen vermischen
* Dann den Mehlbrei in das Kochwasser geben und aufkochen
* ggf. dazugeben
    * Olivenöl
    * Balsamico
    * Senf für Senfsoße
    * Mildes Currygewürz für Curry-Suppe

* Verwendung: Gemüse und Salat rein; ggf. plus "Lima Coriander & wasabi Tamari soya sauce"


Zwiebel-Pilz-X
--------------
April 2019, von selbst

* Zwiebeln mit Kreuzkümmel und Öl in einem Topf kochen
* plus halbierte Cherrytomaten
* plus Pilze, geschnitten
* Paprika Edelsüß (oder alternativ Kurkuma)
* salzen

In einen Salat mischen und zu Linsen, Reis etc.


Tsampa-Brei 2
-------------
Januar 2020, von selbst

Tsampa ist geröstetes Gerstenmehl. Daher muss es nicht gekocht werden.
Dieses Rezept ergibt einen kalten, aber nahrhaften und leckeren Brei.
Der Brei kann nach Belieben mit bereits vorhandenem
gekochten Gemüse oder Sauerkraut aus dem Glas ergänzt werden.

* 300 (oder nur 100???) ml **Tsampa** in einen Topf schütten
* 100 ml **Apfel-Aprikosen-Mark** dazu
* mit **Wasser** (ca. 200 ml) auffüllen
* ein Schuss **Olivenöl** dazu
* **Paprika Edelsüß**
* Salz
-> Mit Kochlöffel umrühren, bis alles verrührt ist
(keine Hitze notwendig)


Tsampa-Brei
-----------
Februar 2019, Tsampa-Verpackung, angepasst

("für 2 Personen")

* 60 g Fett schmelzen
* Muskat
* 1/2 TL Salz
* 120 g Tsampa zugeben
* 0,5 dl Apfelessig (2 dl Weißwein)
* 2,5 dl Wasser     (1 dl Wasser) dazugeben
* Auf kleiner Flamme solange rühren, bis dicklicher Brei entsteht.


Peperoni-Leinöl-Dip
-------------------
Januar 2019, von selbst

* Peperoni klein gehackt
* Leinöl
* Senf
* Sojasauce
* Salz


Müsli Kaffee-Birne
------------------
Dezember 2018, von selbst

* Müsli
* Birne
* Getreide-Kaffee (Dinkel)-Pulver drüberstreuen


Hirsegries mit Sojasauce
------------------------
Dezember 2018, von selbst

* Schnelle Mahlzeit für zwischendurch


Kürbis Spezial
--------------
2018, von selbst

1) Kartoffeln klein schnippeln (& kochen?) + Kümmel
2) Kürbis (wenn Sorte passend) komplett kleinschneiden (inkl. Kerne) und in die Pfanne (oder Topf)

* 2 Zwiebeln
* 1 Tofu 200 g
* einige Oliven
* Gemüsebrühe (Pulver), Sojasauce, Kreuzkümmel


Barbecue-Sauce
--------------
2018, von selbst

* Tomatenmark
* (Tofuwurst in Stücke)
* Wasser
* Ahornsirup
* Dinkelkleie
* Sojasauce
* Zwiebeln
* Essig
* Senfkörner, ganz
* Ingwer, Salz, Pfeffer

(siehe auch Sanchon Grillsauce Louisiana)


Vollkorn-Dinkel-Couscous mit Pflaumen und Bohnen
------------------------------------------------
September 2018, von selbst

* 1 Tasse **Vollkorn-Dinkel-Couscous** vorbereiten (5 min)
* Einige **Pflaumen** schnippeln
* Anleitung: Pflaumen mit einigem von dem Cousous bedecken, dazu **Kidney-Bohnen**. Fertig ist die Schnellmahlzeit. Mahlzeit.


Tsampa als Verdicker - schneller Tofu-Zwiebel-Topf
--------------------------------------------------
Mai 2018, von selbst

* **Tofu natur**, gewürfelt
* 1 **Zwiebel**, geschnitten
* Reste von **Spinatblättern**
* Bratöl, **Balsamico**-Essig, Soja-Sauce
* **Paprika edelsüß**, Kurkuma, Curry, **Fenchelsamen**, Salz
* Apfelsaft
* **Tsampa** zum Nachdicken


Currypfanne mit Süßkartoffeln
-----------------------------
April 2018, von selbst

* Zuerst mit Bratöl Senfkörner und Kreuzkümmel anbraten.
-> Die Zutaten in dieser Reihenfolge nach und nach in die Pfanne geben:
* 1 Karotte, gewürfelt
* 1 Zwiebel und 3 Lauch, gehackt
* Wirsing, gehackt
* **Süßkartoffeln**, gewürfelt
* Apfelessig, Kurkuma, Salz
* Erdnussmus, passierte Tomaten
-> So lange dünsten bis die Zutaten bissfest weich geworden sind.


Sellerie-Würfel-Curry mit Erdnussoße
------------------------------------
April 2018, von selbst

* Highlights
    * Bratöl
    * eine **große Knollen-Sellerie**, gewürfelt
    * Paprika, edelsüß
    * Senfkörner, Kreuzkümmel
    * ...
    * **Erdnusssoße**
    * Apfelessig, Sojasauce
    * Curry-Gewürz
    * ...
    * dazu: Dinkel wie Reis und ggf. passierte Tomaten


4P
--
April 2018, von selbst

4P = **P**rot, **P**esto, **P**ulled **P**ork

(Das Pulled Pork ist ein Fertigprodukt, bio und vegan von Edeka.)

Zubereitung: Auf das Prot zuerst das Pesto und dann das Pulled Pork drauflegen.


Kräuter-Quark
-------------
Sommer 2017, von selbst, nach Rezept von R / Kräuterquark

* 400 g **Soja-Quark** (oder ein anderer pflanzlicher Quark)
* 1 **Zwiebel**, kleingehackt
* 5 **Gewürzgurken**, gewürfelt
* **"Quark-Gewürz"**-Mischung von Lebensbaum oder Sonnentor
* **Paprika edelsüß**
* Salz
-> Alles in eine Schüssel und mischen.
-> Alternative/ergänzende Zutaten: Pfeffer, Thymian, Bockshornkleesamen


Salat-Aufstrich
---------------
Juni 2017, von Bio-S, Besonderheit: überaus salat-haltig

* Sonnenblumenkerne - 1 - 2 Hand voll
* Bratöl (neutrales Pflanzenöl) - 3 - 4 EL
* viel Salat - 1/2 - 3/4 - ganzer Kopf
* 1 saure Gurke
* Tomatenmark - 1 EL
* 1 kleine Zwiebel
* 1 frische Knoblauchzehe
* Paprika edelsüß
* Salz
* Erdnussmus - 1 EL

Mit dem Pürierstab klein pürieren und aufs Brot essen oder direkt aus dem Behälter.


Eine Soße
---------
Juni 2017, von selbst

Verwendung: After-the-fact-Flüssigmachung des Inhalts einer Gemüsepfanne auf dem Teller

* Hafermilch
* Tomatenmark
* Saure Gurken, klein geschnitten
* Leinöl
* Frühstücksbrei zum Andicken
* Dattelsirup
* Balsmico-Essig
* Salz, Pfeffer

Alles verrühren und kalt über das Essen gießen.


Tofu - knusprig angebraten
--------------------------
* Anfang 2017: http://www.seriouseats.com/2014/02/vegan-experience-crispy-tofu-worth-eating-recipe.html
* noch nicht ausprobiert, sieht aber lecker aus


Schnellmahlzeit: Soja-Schnetzel + Gemüsepüree
---------------------------------------------
2017

* Kleine Soja-Schnetzel mit heißem Wasser übergießen
* 10 Minuten warten
* darüber (vorher vorbereitetes) frische püriertes Gemüse mit Gewürzen, Sonnenblumenkernen, Öl etc.
* z. B. Haselnüsse
* Rosinen, damit es eine süße Note bekommt
* ggf. noch etwas Öl


Gemüsepfanne mit Sojaschnetzel, mit Reis
----------------------------------------
Dez 2017

* Wirsing, geschnitten
* Gewürze en masse inkl. Knoblauchgranulat, Koriander, Curry, Paprika edelsüß, Salz
* Heißes Wasser + **Sojaschnetzel**
    * (in anderen Gerichten kann man die Sojaschnetzel auch einfach dazutun, wenn genug Wasser drin ist)
* Passierte Tomaten
* dazu Reis


Haferbrei Krankenmahlzeit
-------------------------
Dez 2017

* Wasser kochen
* 1 Teelöffel **Gemüsebrühe** (ohne Palmöl)
* **Apfel**stücke
* **Rosinen**
* Sonnenblumenkerne
* Haselnusskerne
* **Haferflocken** dazugeben
* Rühren und warten

Lecker.


Verschiedenes
-------------
* 2016: Tofuwürfel + Cherrytomaten + Yokos Tofugewürz + Kurkuma + Pfeffer/Salz + Sojasauce + **Balsmico-Essig**
* 2016: Monis Salat (?) mit **Chinakohl**, Nudeln, **Tomatenmark/Mandelmus**, Paprika


Gewürz-Sammlung
---------------
Gute Gewürze:

* Lebensbaum Quarkgewürz: Schabzigerklee, Dill, Kerbel, Lauch, Knoblauch, Liebstöckel, Basikikum
* Yoku Tofu-Gewürz
* Lebensbaum Mykonos
* Sonnentor Sonnenkuss
