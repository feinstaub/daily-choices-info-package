2008-03 pre-conscious era
-------------------------

* Champignons mit Balsamico-Marinade

* Weißbrot + Ananas + herzhaften Käse (Dänisch?) --> Grillen + Curryketchup

* Würstchen + Rotkraut aus dem Glas --> warm machen. + Brot mit Butter

* Jogurt mit Erdnüssen und Schoko (M&Ms)

* Mexikanische Reismilch (süß)

* Ajvar: Paprikazubereitung: Paprika 88%, Auberginen, Sonnenblumenöl, Zucker, Salz, Knoblauch,
       Säuerungsmittel Branntweinessig

* Parmesankäse am Stück

* Toast + Margarine + Pilze aus der Dose + Scheibe Geflügelsalami + Scheibe Frankendamer --> Grill

* Tortillas + Rotkraut

* Geheimtipp: Jogurt mit Ahorsirup (ein kleiner Becher, ca. 2 Esslöffel?)

* Veg. Würste

