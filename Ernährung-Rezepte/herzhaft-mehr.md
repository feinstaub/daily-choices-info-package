Herzhaft - Mehr
================

[Vegane](..) Rezepte, die am besten mit Zutaten aus [pestizid-freier Landwirtschaft](../../Bio) zubereitet werden sollten.

<!-- toc -->

- [Rotkohl + Sojasauce](#rotkohl--sojasauce)
- [Auberginen auf Matjes Art](#auberginen-auf-matjes-art)
- [Vegane Lasange](#vegane-lasange)
- [Jackfrucht + Paprika + Salz](#jackfrucht--paprika--salz)
- [Dattel im Mangold-Mantel](#dattel-im-mangold-mantel)
- [Mangold-Pesto (wohin mit dem vielen Mangold?)](#mangold-pesto-wohin-mit-dem-vielen-mangold)
- [Mandelmus + Semmelbrösel in Gemüsebrühe](#mandelmus--semmelbrosel-in-gemusebruhe)
- [Bratkartoffeln, Dattelsirup, Worcestershiresauce](#bratkartoffeln-dattelsirup-worcestershiresauce)
- [Walnussfrikadellen (vegan)](#walnussfrikadellen-vegan)
  * [Variante: Gemüse-Frikadellen](#variante-gemuse-frikadellen)
- [Herzhafte Muffins (vegan)](#herzhafte-muffins-vegan)
- [Gyros + Tsatziki](#gyros--tsatziki)
- [Gemüse-Curry-Gericht](#gemuse-curry-gericht)

<!-- tocstop -->

Rotkohl + Sojasauce
-------------------
2022

* beim Rotkohl kochen Sojasauce hinzugeben = lecker

Auberginen auf Matjes Art
-------------------------
2019, todo, von Zapp, ARD

* http://www.vegane-naschkatzen.de/vegane-rezepte/vegan-vleisch-visch/186-auberginen-auf-matjes-art
* http://vegan-fitnessguide.de/recipe-auberginen-matjes.html#

Vegane Lasange
--------------
2019

* https://kaffeeundcupcakes.de/die-weltbeste-gemuese-lasagne-vegan/

Jackfrucht + Paprika + Salz
---------------------------
Juni 2018, selbst

* Jackfruchtstücke aus der Dose
* Paprikapulver und Salz drüberstreuen

Dattel im Mangold-Mantel
------------------------
Juni 2018, selbst

* Dattel + Mangoldblatt drumrumwickeln
* in Pfanne
* etwas Öl und Sojasauce
* warm machen / Endprodukt ist stabiler als gedacht (im kalten Zustand denkt man, ein Spieß wäre nötig)

Mangold-Pesto (wohin mit dem vielen Mangold?)
---------------------------------------------
November 2017, von CK-Koch/solawi

* Alles roh in den Mixer und dann pürieren
* **Mangold** (etwas schneiden) auf 2/3 Mixer-Füllhöhe
* **Walnüsse** oder Cashew-Kerne
* Rapunzel **getrocknete Tomaten** (100 g schmeckt etwas vor, nä. Mal weniger)
* Zitronensaft oder Weißweinessig oder Apfelessig
* Salz (ggf. nicht zuviel, wenn das schon in den getr. Tomaten drin ist)
* Pfeffer?
* **Knoblauch**
* **Neutrales Bratöl** (kein Olive) nach Gefühl auffüllen / **kein Wasser**

Mandelmus + Semmelbrösel in Gemüsebrühe
---------------------------------------
2017?

* Was schnelles, wenn Semmelbrösel da sind

Bratkartoffeln, Dattelsirup, Worcestershiresauce
------------------------------------------------
2017?

* gute Mischung

Walnussfrikadellen (vegan)
--------------------------
2018-Dez, von M., schmecken original

* 500 g gemahlene Walnüsse
* 2 EL Leinmehl (Beispiel für Ei-Ersatz)
* 3 - 5 EL Haferflocken (in Wasser angedickt)
* 1/2 Zwiebel in Würfel
* Salz, Pfeffer, Muskatnuss, etwas Senf

Gut durchmischen, Klopse formen, in Fett oder Öl knusprig braten.

### Variante: Gemüse-Frikadellen
2021-Juni, von M.

**anstelle** der 500 g gemahlene Walnüsse:

* 2 EL gehackte Nüsse oder/und Samen
* 2 geraffelte Möhren oder/und anderes Gemüse
* wahlweise Grünkernschrot (= Grünkernbratlinge)
* mehr Haferflocken, je nach Konsistenz

Herzhafte Muffins (vegan)
-------------------------
Oktober 2017, von R, Rezept schon ein paar Jahre alt und so gut, dass es immer alle haben wollen

**Zutaten** für 12 Muffins:

* **Teig**
    * 75 g geschmolzene Alsan
        * (= Margarine mit Butterkonsistenz), (Achtung Palmölproblem)
    * 2 EL Sojamehl
    * 250 g Mehl
    * 2 TL Backpulver
    * 2 TL Salz
    * 125 ml Sojamilch
    * 4 EL Tomatenmark
    * Pfeffer, Knoblauch, Chili, Oregano
* **Füllung**
    * 130 g Räuchertofu
    * 1 Zwiebel
    * 3 EL Pinienkerne
    * 100 g Wilmersburger Pizzaschmelz (= veganer Käse)
    * Olivenöl oder anderes pflanzliches Öl
    * Variante 1 (empfohlen): 10 getrocknete Tomaten, 200 g Kirschtomaten, 120 g Oliven
        * (ODER Variante 2: 1 rote Paprika, 1/2 Zucchini, 2 EL Mais)

**Zubereitung**

* **Backofen** auf 180 °C Umluft vorheizen.
* Den **Räuchertofu** in kleine Würfel schneiden und in einer Pfanne mit Öl knusprig anbraten. Die **Zwiebel** schälen und in feine Würfel schneiden. Die **Pinienkerne** in einer Pfanne ohne Öl rösten.
* Füllung: **getrocknete Tomaten** in feine Streifen schneiden und **Oliven** in Scheiben schneiden. **Kirschtomaten** vierteln.
    * (ODER Variante 2: Paprika und Zucchini waschen und in kleine Würfel schneiden. Mit dem Mais vermischen.)
* Alle Zutaten für den **Teig** verrühren. Mit Räuchertofu, Zwiebeln, Pinienkernen, **Wilmersburger** und der **Füllung** verkneten.
* Teig portionsweise in die geölte Muffinform oder Silikonförmchen drücken. Die Muffins gehen kaum auf, also könnt ihr die Form ruhig voll machen.
    * Für **ca. 20 Minuten backen**.
    * Mit der Stäbchenprobe könnt ihr feststellen, ob die Muffins durch sind.


Gyros + Tsatziki
----------------
* ...

Gemüse-Curry-Gericht
--------------------
2017, von O. für R.

    für die Soße: Zwiebeln, Knoblauch, Kokosmilch, Currypulver, Senfsamenkörner, Korianderpulver, Currypaste
    für die Gesundheit: Gemüse nach Wunsch (z. B. Lauch, Tomaten, Brokoli, Auberginen)
    Soja-Geschnetzeltes
    zum Stopfen der Mägen: Reis
    als Topping: firscher Koriander
