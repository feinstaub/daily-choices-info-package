Zutaten-Watch
=============

Wer so weit gekommen ist, alles vorwiegend vegan und pestizid-frei einzukaufen, der kann sein Augenmerk auf die nächste Stufe legen.

Liste von problematischen Zutaten, die mit etwas Übung problemlos miminiert werden können:

* Allgemein
    * Obst und Gemüse aus Übersee ist schwierig zu kontrollieren. Bei konventionellem Landbau leidet oft die lokale Bevölkerung und die Umwelt.
    * Allgemeines Maßhalten ist sinnvoll.
    * Wenn kaufen, dann nur selten und immer mit Bio-Siegel.

* **Ananas**
    * "Geringer Lohn und massiver Pestizideinsatz - Billige Ananas auf Kosten der Plantagen-Arbeiter", 2019
        * https://www.swrfernsehen.de/marktcheck/billige-ananas,ananas-100.html
        * „Der Preis ist unserer Auffassung nach viel zu niedrig. Er spiegelt nicht die sozialen und ökologischen Kosten wider, wir genießen die günstige Ananas, das geht aber zu Kosten der Arbeiter in Costa Rica.“, Franziska Humbert, Oxfam
    * "Die Ananas – süß, billig und unheimlich giftig", 2017
        * https://e-politik.de/?p=257281
            * "Zweck des Vereins ist die Förderung der politischen und journalistischen Bildung und Ausbildung junger Journalisten, Politiker und Wissenschaftler sowie die Förderung der Politikwissenschaft und eines unabhängigen und überparteilichen Journalismus."

* **Avocado**
    * Wenn kaufen, dann nur selten und immer mit Bio-Siegel.
    * Hoher Wasserverbrauch in den Anbauländern, siehe z. B. https://utopia.de/ratgeber/avocado/

* **Banane**
    * Wenn kaufen, dann nur selten und immer mit Bio-Siegel.
    * 2017: ["Da sterben Arbeiter an Pestiziden, Kinder kommen behindert zur Welt"](http://schrotundkorn.de/ernaehrung/lesen/kochprofi-ole-plogstedt.html)
        * Interview mit Ole Plogstedt
    * 2004: "Gift nebelt die Siedlungen ein": http://schrotundkorn.de/lebenumwelt/lesen/200410b6.html
        * beim konventionellen Bananen-Anbau
    * Chiquita-Hinweise
        * meist nicht-bio
        * 2006: http://schrotundkorn.de/lebenumwelt/lesen/200603a06.html

* **Palmöl**
    * siehe [Palmöl-Problematik](../../Konsum/palmöl.md)
