Zutaten
=======

<!-- toc -->

- [Roh essen?](#roh-essen)
  * [Zucchini](#zucchini)
  * [Erbsen / Zuckerschoten, auch Zuckererbsen oder Kaiserschoten genannt](#erbsen--zuckerschoten-auch-zuckererbsen-oder-kaiserschoten-genannt)
  * [Lauch](#lauch)
  * [Rosenkohl](#rosenkohl)
  * [Mohn](#mohn)
- [Nicht roh essen](#nicht-roh-essen)
  * [Kichererbsen, Kichererbsenmehl](#kichererbsen-kichererbsenmehl)
  * [Stangenbohnen, Buschbohnen](#stangenbohnen-buschbohnen)
  * [Auberginen](#auberginen)
  * [Spinat](#spinat)
- [Essbar?](#essbar)
  * [Kürbiskerne](#kurbiskerne)
  * [Schale vom Kürbis, Hokkaido, Zaphito](#schale-vom-kurbis-hokkaido-zaphito)
  * [Rosmarin](#rosmarin)
  * [Kohlrabi: Auch Blätter sind essbar](#kohlrabi-auch-blatter-sind-essbar)
- [Zubereiten](#zubereiten)
  * [Bohnen kochen - allgemein](#bohnen-kochen---allgemein)
  * [Sojabohnen](#sojabohnen)
  * [Dicke Bohnen zubreiten](#dicke-bohnen-zubreiten)
  * [Hirschhornwegerich verwenden](#hirschhornwegerich-verwenden)
- [Lagern](#lagern)
  * [Pilze](#pilze)
- [Ungewöhnliches](#ungewohnliches)
  * [Schimmliges Kerngehäuse beim Apfel](#schimmliges-kerngehause-beim-apfel)
  * [Schimmel allgemein](#schimmel-allgemein)

<!-- tocstop -->

Roh essen?
----------
### Zucchini
2018: JA, außer wenn bitter schmeckt, siehe https://utopia.de/ratgeber/zucchini-roh-essen-vorteile-und-moegliche-gefahren/

### Erbsen / Zuckerschoten, auch Zuckererbsen oder Kaiserschoten genannt
* https://www.edeka.de/ernaehrung/expertenwissen/1000-fragen-1000-antworten/sind-auch-rohe-zuckerschoten-zum-verzehr-geeignet.jsp
2018: JA, obwohl eine Hülsenfrucht (Ausnahme), siehe https://www.t-online.de/leben/essen-und-trinken/id_73486438/gesundheit-darf-man-erbsen-roh-essen-.html
    * "Sowohl frische Erbsen als auch unreif geerntete Zuckerschoten kommen für den Roh-Verzehr infrage."

### Lauch
* JA, https://www.chefkoch.de/forum/2,52,406552/Lauch-roh-essen.html

### Rosenkohl
* 2020: JA
    * https://www.wir-essen-gesund.de/rosenkohl-roh-essen/
        * bei Bedarf: "Damit der Rosenkohl seinen leicht bitteren Geschmack verliert, empfiehlt es sich ihn vor dem Verzehr
            in Salzwasser einzulegen. Nach etwa 30 Minuten sind die Bitterstoffe weitestgehend neutralisiert und der Kohl schmeckt milder und süßer als zuvor."
            * Bitterstoffe sind aber gesund.

### Mohn
todo

Nicht roh essen
---------------
### Kichererbsen, Kichererbsenmehl
* Kichererbsenmehl
    * 2014: https://www.bzfe.de/forum/index.php/forum/showExpMessage/id/46261/page1/5/searchstring/+/forumId/16
        * "Sie können auch in der Schwangerschaft ohne Bedenken Pfannkuchen aus Kichererbsenmehl herstellen. Wenn die Pfannkuchen gar sind, wurde auch das Kichererbsenmehl ausreichend erhitzt."
        * "Die in rohen Kichererbsen enthaltenen Giftstoffe zählen zur Gruppe der Lektine. Dies sind Eiweißverbindungen, die durch Erhitzen rasch zerstört werden. Dieser Vorgang erfolgt beim Mehl aufgrund der starken Zerkleinerung sehr viel schneller als bei ganzen Kichererbsen. Zudem ist die Temperatur beim Braten höher als beim Kochen."

### Stangenbohnen, Buschbohnen
2018: nicht roh essen / 10 Min über 70 Grad kochen
    * https://www.verbraucherzentrale-bayern.de/wissen/haetten-sies-gewusst/warum-sind-rohe-gruene-bohnen-giftig-17665
        * "((Die meisten Gemüsearten lassen sich roh verzehren. Zu den Ausnahmen zählen grüne Bohnen.))
            Ungekochte grüne Bohnen wie Busch- oder Feuerbohnen enthalten eine giftige Eiweißverbindung, das sogenannte Phasin"
        * "Werden die Bohnen mindestens zehn Minuten gekocht, wird das Protein weitgehend zerstört. Gegarte grüne Bohnen stellen daher keine Gefahr für den Menschen dar"
        * "Doch auch im Blanchierwasser grüner Bohnen findet sich eine erhebliche Mengen Phasin."
    * https://de.wikipedia.org/wiki/Gartenbohne
        * "Die Gartenbohne (Phaseolus vulgaris), auch Grüne Bohne oder österreichisch Fisole"
        * "Je nach Wuchsform wird die Art auch als Buschbohne oder Stangenbohne bezeichnet"
        * 'Gartenbohnen enthalten für den Menschen giftige Lektine (Phaseolin), die durch Kochen zerstört werden.'
        * "Alle Bohnen der Gruppe Phaseolus sind roh giftig. Erst das Erhitzen (in Kochwasser) auf mehr als 70 °C zersetzt das Gift Lectin"

Buschbohnen zubereiten: http://rezepte.anleiter.de/wie-kocht-man-buschbohnen

* "1 kg Buschbohnen, 500 ml Wasser, ein Bund Bohnenkraut, ein halber Teelöffel Salz"
* ...
* "Als erstes müssen die Bohnen gründlich gewaschen und geputzt werden, die beiden Enden werden an beiden Seiten mit einem scharfen Messer entfernt."
* "Die Bohnen in etwa 15 Minuten gar kochen lassen. Nach einer viertel Stunde sollte man eine Bohne probieren und testen, ob sie schon die gewünschte Konsistenz erreicht hat"
* Wasser abgießen

### Auberginen
* besser nicht roh essen
    * https://eatsmarter.de/ernaehrung/ernaehrungsmythen/auberginen-roh-essen
    * https://utopia.de/ratgeber/aubergine-roh-essen-warum-es-nicht-empfehlenswert-ist/

### Spinat
* ja, wenn jung
* nicht so viel, wenn Blätter schon gewellt und die Stiele
* Gesunde Kombination mit Kalzium und Vitamin C
    * https://utopia.de/ratgeber/spinat-roh-essen-wann-es-gesund-ist-und-wann-bedenklich/
    * https://www.iglo.de/gemuese/spinat/roh-essen

Essbar?
-------
### Kürbiskerne
* JA
    * siehe z. B. https://www.hausfrauenseite.de/kuechentipps/kuerbisse_mit_kernen_kochen.html
    * https://www.chefkoch.de/forum/2,9,43161/Wie-sind-frische-Kuerbiskerne-zu-verwenden.html

### Schale vom Kürbis, Hokkaido, Zaphito
* JA: "Theoretisch kann man jeden Speisekürbis mitsamt der Schale essen."
    * https://www.smarticular.net/kuerbis-schale-essbar-mitessen-schaelen/
        * "Da aber manche Schalen sehr hart sind und eine viel längere Garzeit als das Kürbisinnere haben, ist es empfehlenswert, sie bei diesen Sorten zu entfernen oder lediglich zum Füllen zu verwenden."
* JA: "Zaphito ist zwar botanisch ein Kürbis, wird aber als junge Frucht wie eine Zucchini geerntet. Entsprechend wird die Schale mitgegessen."
    (https://www.bingenheimersaatgut.de/de/bio-saatgut/gemuese/kuerbis/zaphito-g254)

### Rosmarin
* https://www.gartenjournal.net/rosmarin-essbar
    * mitgaren

### Kohlrabi: Auch Blätter sind essbar
* https://www.heilpraxisnet.de/naturheilpraxis/gesunder-kohlrabi-auch-blaetter-sind-essbar-2011042612684/

Zubereiten
----------
### Bohnen kochen - allgemein
* 2020: https://www.mutternatur.at/aktuelles/wie-koche-ich-bohnen/
    * "Schon seit Jahrtausenden werden überall auf der Erde unterschiedlichste Sorten von Bohnen angebaut, ernähren ganze Völker und halten sie gesund."
    * "Früher oft als „Arme-Leute-Essen“ verpönt, mausern sich Hülsenfrüchte – und somit auch Bohnen – heute langsam jenen Ruf, den sie als wertvolle Eiweiß- und Ballast­stoff­lieferanten absolut verdient haben."
    * ! "rohe und getrocknete Bohnen vor dem Verzehr immer waschen, einweichen und kochen, um Phasin und Co. unschädlich zu machen."
    * 1. "mit kaltem Wasser abspülen bzw. waschen und das Wasser anschließend gleich wegschütten."
    * 2. "in eine Schüssel geben und mit frischem Wasser auffüllen.
        Wichtig: das Wasser muss zirka doppelt so hoch in der Schüssel stehen, wie die Bohnen eingefüllt sind."
    * 3. "Zwischen 6-12 Stunden (je nach Bohnensorte unterschiedlich) aufquellen lassen, am besten über Nacht." (Kochzeit, Verdaulichkeit) (fertig, "wenn keine Fältchen mehr")
    * 4. "Wasser abgießen und die Bohnen nochmals kalt abspülen"
    * 5. "mit reichlich Wasser, in einen Topf geben, der groß genug ist.", noch nicht salzen; bei hartem Wasser ggf. Natron/Backpulver
    * 6. Aufsieden lassen; dann niedrige Hitze 40 - 90 Min (je nach Sorte) garen lassen (Deckel drauf; immer mit Wasser bedeckt)
    * 7. Gegen Ende immer wieder prüfen, ob fertig: weich sind, aber noch eine angenehme Bissfestigkeit
    * 8. Salzen und würzen (gegen Ende der Kochzeit, in den letzten 10 Min.)
        * z. B. Salz, Zitronensaft/Essig
        * z. B. Bohnenkraut, Fenchel, Kümmel, Majoran, Lorbeer- und Salbeiblätter (=> leichter verdaulich und "feine Extraportion Geschmack")
    * Was ist mit dem Kochwasser? - https://www.eat-this.org/richtig-huelsenfruechte-kochen/
        * scheint wohl essbar zu sein: "fülle ich die garen, heißen Böhnchen & Co. zusammen mit dem Kochwasser in sterilisierte Einmachgläser,
        verschraube sie gut und stelle sie sofort auf den Kopf, damit sich ein Vakuum bildet, und lasse sie so komplett abkühlen"
        * ansonsten auch gute Tipps

### Sojabohnen
* 1 Tasse trockene Sojabohnen ergibt ca. 3 T im gekochten Zustand

### Dicke Bohnen zubreiten
* https://eatsmarter.de/lexikon/warenkunde/huelsenfruechte-0/dicke-bohnen, 2019
    * Roh essen?
        * NEIN: "Roh essen sollte man sie nicht."
    * "Heute baut man Dicke Bohnen vor allem in Italien, Spanien, Frankreich und in kleinen Mengen auch bei uns in Deutschland an."
    * "frisch von Juni bis September auf den Markt"
    * Dose: "Sie können Dicke Bohnen bzw. die bereits heraus gelösten Kerne aber auch tiefgekühlt oder in der Konserve bekommen. Sie schmecken nicht ganz so gut wie frische, aber dafür sparen Sie damit Arbeit und sind von der Saison unabhängig."
    * Lagerung: "Frische Dicke Bohnen am besten nicht länger als 3-4 Tage im Gemüsefach des Kühlschranks aufbewahren."
    * Vorbereitung
        * ...
        * " Theoretisch wären sie nun fertig zum Zubereiten ? in der Praxis empfiehlt es sich aber, die sie umhüllende Haut ebenfalls zu entfernen. Dazu geben Sie die Kerne am besten in reichlich kochendes Salzwasser und blanchieren sie darin 1- 2 Minuten. Dann abgießen, mit eiskaltem Wasser  abschrecken und gut abtropfen lassen. Nun lässt sich die Schale leicht aufritzen und das Innere vorsichtig herauslösen."
    * Zubereitung
        * "die Kerne brauchen nur ganz wenig Zeit, um sich in einen Hochgenuss zu verwandeln. Es genügt, sie mit Butter in einer Kasserolle zwei, drei Minuten zu schwenken. Mit Salz, Pfeffer und frisch gehacktem Bohnenkraut oder auch Petersilie abschmecken ? fertig."
        * ...

### Hirschhornwegerich verwenden
* https://www.nebelung.de/wissenswertes/kulturanleitungen/gemuese/hirschhornwegerich/
    * "Verglichen mit Kopfsalat und Feldsalat enthalten die Blätter vom Hirschhornwegerich mehr Calcium, Vitamin A und Vitamin B2. Der Gehalt an Vitamin C entspricht etwa dem von Feldsalat. Die jungen Blätter verwendet man wie andere Blattgemüse in gemischten Salaten, eignen sich aber auch für die Zubereitung grüner Smoothies. Die älteren Blätter kann man dünsten oder 2 Minuten in siedendem Salzwasser blanchieren und als Gemüsebeilage verwenden."

Lagern
------
### Pilze
* https://www.topagrar.com/landleben/kochen-und-backen/familie-kochen-backen-kuechentipps-wie-man-pilze-richtig-lagert-9385212.html
    * Pilze mögen es kühl und luftig
    * Nicht quetschen
    * Kühlschrank
    * "Die Aufbewahrung in Papiertüten. Allerdings sollten die Pilze auch so nicht länger als ein bis zwei Tage im Kühlschrank aufbewahrt werden."

Ungewöhnliches
--------------
### Schimmliges Kerngehäuse beim Apfel
* Großzügig wegschneiden ist ok (solange es um das Gehäuse nicht schon angefangen hat zu faulen). Der Apfel sollte auch noch schmecken
    * https://www.swr.de/blog/1000antworten/antwort/8627/darf-man-einen-apfel-mit-schimmeligem-kerngehaeuse-ohne-bedenken-essen/
    * https://www.bzfe.de/forum/index.php/forum/showExpMessage/id/30417/page1/8/searchstring/+/forumId/17
    * https://projekte.meine-verbraucherzentrale.de/DE-BY/kernhausfaeule-bei-aepfeln

### Schimmel allgemein
* Rote Beete
    * https://www.chefkoch.de/forum/2,15,260466/weisser-schimmel-an-vor-14-tagen-geernteter-roter-bete-im-keller.html
* "Wegschneiden oder -werfen?" - https://derstandard.at/1254311400535/Schimmel-auf-Lebensmitteln-Wegschneiden-oder-werfen, 2009
* https://www.openscience.or.at/hungryforscienceblog/schimmel-wegschneiden-oder-wegwerfen/
    * Details über Gifte

