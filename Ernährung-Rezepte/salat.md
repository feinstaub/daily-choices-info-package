Salat und Zubehör
=================

[Vegane](..) Rezepte, die am besten mit Zutaten aus [pestizid-freier Landwirtschaft](../../Bio) zubereitet werden sollten.

<!-- toc -->

- [Sauerkraut-Bohnen-Erbsen-Salat](#sauerkraut-bohnen-erbsen-salat)
- [Kohlrabi-Sauerkraut-Salat](#kohlrabi-sauerkraut-salat)
- [Rotkohl-Zwiebel-Sojabohnen-Salat](#rotkohl-zwiebel-sojabohnen-salat)
- [Zwiebel-Bohnen-Salat](#zwiebel-bohnen-salat)
- [Apfel-Kohlrabi-Salat mit Zwiebeln und Bohnen](#apfel-kohlrabi-salat-mit-zwiebeln-und-bohnen)
- [Kurkuma-Zwiebelsalat mit schwarzem Sesam (und Linsen)](#kurkuma-zwiebelsalat-mit-schwarzem-sesam-und-linsen)
- [Fruchtig-herzhafter Tofusalat!](#fruchtig-herzhafter-tofusalat)
- [Fruchtig-herzhafter Gurkensalat!](#fruchtig-herzhafter-gurkensalat)
- [Winter-Asia-Salat Mango Deluxe](#winter-asia-salat-mango-deluxe)
- [Weißer Bohnensalat mit Kapern (Party)](#weisser-bohnensalat-mit-kapern-party)
- [Reis-Salat, Reissalat](#reis-salat-reissalat)
- [Roher Kohlrabi-Salat](#roher-kohlrabi-salat)
- [Salat-Rezept/Konzept mit Blattsalat und Kohlrabi](#salat-rezeptkonzept-mit-blattsalat-und-kohlrabi)
- [Tahin-Salat-Dressing](#tahin-salat-dressing)
- [Helle Salatsauce](#helle-salatsauce)
- [Salatsauce auf Nussmusbasis](#salatsauce-auf-nussmusbasis)
- [Omas Krautsalat](#omas-krautsalat)
  * [Bausteine](#bausteine)
  * [Anleitung](#anleitung)
- [Kartoffelsalat](#kartoffelsalat)
- [Tzatziki (Veganitzki)](#tzatziki-veganitzki)

<!-- tocstop -->

Sauerkraut-Bohnen-Erbsen-Salat
------------------------------
siehe Kohlrabi-Sauerkraut-Salat


Kohlrabi-Sauerkraut-Salat
-------------------------
Juni 2020, von selbst

* 1 großer Kohlrabi in Würfeln
* Sauerkraut aus dem Glas
* 1 Dose Borlotti-Bohnen (Gesamtfüllgewicht: 400 g, Abtropfgewicht: 250 g)
* Leinsamen, **Tsampa**
* Senf, Balsamico, Lein/Olivenöl, Salz
* ggf. Rest passierte Tomaten (mit Wasser?)


Rotkohl-Zwiebel-Sojabohnen-Salat
--------------------------------
Dezember 2019, selbst

* 1/2 Rotkohl, geschnitten, roh
* 1 Zwiebel, gehackt
* 1 Dose Edemame-Sojabohnen (schön grüner Kontrast zum roten Kohl)
* 1 paar Maulbeeren (oder Rosinen)
* Olivenöl
* Balsamico-Essig
* Tomatenmark
* Gomasio (Sesam mit Salz)
-> Alles in eine Schüssel und mischen.


Zwiebel-Bohnen-Salat
--------------------
Oktober 2019, von selbst

* 5 Zwiebeln hacken
* 1 Glas Kidney-Bohnen
* Passierte Tomaten
* Rapsöl
* Balsamico-Essig
* Frische Petersilie hacken
* einiges an Sesam
* 1 Peperoni fein hacken
* Bohnenkraut
* Salz


Apfel-Kohlrabi-Salat mit Zwiebeln und Bohnen
--------------------------------------------
März 2019, selbst

* 1 Apfel, gewürfelt
* 1 Kohlrabi, gewürfelt
* 1 Zwiebel
* 1 Dose weiße Bohnen
* 1 Handvoll Sonnenblumenkerne
* 1 Handvoll Leinsamen
* Olivenöl, Balsamico
* Sesam, Curry mild
* Muskat, Salz
-> Alles in eine Schüssel und mischen.


Kurkuma-Zwiebelsalat mit schwarzem Sesam (und Linsen)
-----------------------------------------------------
Januar 2019, von selbst

* Zwiebeln
* Leinöl
* Balsamico?
* Paprika-Gewürz?
* Kurkuma
* schwarzer Sesam

Dazu Linsen und Winterposteleiensalat (mit Sonnenblumenkernen und Cashew-Granulat etc.)


Fruchtig-herzhafter Tofusalat!
------------------------------
August 2018, selbst, (the hunger stiller)

* 400 g Tofu, würfeln
* viel gehackte Zwiebeln
* 1 Glas eingemachte Pfirsche
* Tomatenmark, Senf, Olivenöl, Balsamico-Essig, Soja-Sauce
* Sonnenblumenkerne, Kürbiskerne
-> Alles in eine Schüssel und mischen.
-> Dazu: Dinkel-Vollkorn-Cousous.


Fruchtig-herzhafter Gurkensalat!
--------------------------------
Juli 2018, selbst, (sehr lecker)

* **Salatgurke**, kleingeschnitten
* **Räuchertofu**, Würfel
* **Apfelmark**
* Öl
* Mykonos-Gewürz
* Ingwer, geraspelt
* Balsamico-Essig
* Salz


Winter-Asia-Salat Mango Deluxe
------------------------------
05.12.2017, selbst, Bio-S

Reihenfolge grob mengenmäßig:

* Asia-Salat
* Kohlrabi in Stücken
* Rotkohl roh, in dünne Streifen (Döner-Style)
* 1 Zwiebel, gehackt
* Curry-Tempeh-Würfel, angebraten
* Soße aus: Mandelmus + Sojaquark + Mangosauce
* Leinöl, Curry-Pulver
* Salz, Pfeffer, Muskat


Weißer Bohnensalat mit Kapern (Party)
-------------------------------------
23.03.2015, selbst, partytauglich, Update 16.04.2016

Reihenfolge wie es in die Schüssel kommt:

* Oliven**öl** (mit Zitrone oder Zitrone separat oder ohne Zitrone), Balsamico-**Essig**, Soja-Sauce, Hanföl
* 1 EL Tomatenmark (oder entsprechend mehr passierte Tomaten)
* [Soyana Sauerrahm](http://www.alles-vegetarisch.de/LEBENSMITTEL-Sahne---Ei-Ersatz-Soyana-SOYANANDA-Sauerrahm--200g-Fermentierte-Soja-Alternative-zu-Sauerrahm,art-4685) (oder [Soyanada](http://www.alles-vegetarisch.de/frischkaese-vegan-bio-soja-streichkaese-wie-rahm-pflanzlich,art-4688) oder Jogurth-Produkt)
* Ahornsirup
* Pfeffer, Muskat, (Koriander)
* 1 kleines Gläschen **Kapern** (optional: Saure Gurkenstücke)
* gehackte **Zwiebeln**
* Kau-Elemente:
    - 1 [Taifun Japanisches Bratfilet](https://www.taifun-tofu.de/de/japanische-bratfilets) (gewürfelt) und/oder 1/2 (ca. 100 g) [Vegiebelle](http://www.tofunagel.de/produkte/sojakaese/pur/) (gewürfelt)
    - oder alternativ: 1 [Grafitti-Tofu](https://taifun-tofu.de/de/tofu-terrine-graffiti) in Würfeln
* 1 Glas **weiße Bohnen** (gewaschen)

Alles in eine Schüssel geben und mischen.


Reis-Salat, Reissalat
---------------------
2008, T. Br.

* Reis, Curry
* in Stücken: Äpfel, Paprika, Tomate, Gurken, Banane (wenig)
* Was für Soße/Dressing?


Roher Kohlrabi-Salat
--------------------
Winter 2018

* 1 große **Kohlrabi** in Würfeln
* 1 **Apfel** in Würfeln
* **Sonnenblumenkerne**
* Rapsöl
* **Senf**
* Balsamico-Essig
* Kräuter der Provence, Salz, Pfeffer, Curry (klassisch)
* dazu: **Goldhirse** mit Sojasauce und Dattelsirup


Salat-Rezept/Konzept mit Blattsalat und Kohlrabi
------------------------------------------------
Herbst 2017

* 1 **Kopfsalat**
* 1 **Apfel**
* 1 **Kohlrabi**
* 5 Radischen
* Basilikumöl, Leinöl, Olivenöl
* Balsamico (wenig)
* Salz, Pfeffer
* Bockshornkleesamen
* Koriander
* Kräuter der Provence
* Dattelsirup


Tahin-Salat-Dressing
--------------------
19.04.2015, aus "Rohkost macht glücklich" (J. Lechner, A. Teichmann), kam 2017 gut an

* 2 EL Tahin
* 1 EL Tamari(Soja)-Soße
* 1 EL Agaven-Dicksaft (oder Dattelsirup)
* 2 TL Zironensaft


Helle Salatsauce
----------------
Herbst 2017, von selbst

* **Apfel-Balsamico**
* Distelöl/Leinöl
* **Sonnenblumenkerne**
* Dattelsirup, Sojasauce?
* Salz
* Senf
* Zwiebel, Gewürzgurke
* Gewürze (welche?), Curry/Kurkuma
* Haferdrink?


Salatsauce auf Nussmusbasis
---------------------------
07.11.2015, von T-Mo

Dieses Sauce passt mit Erdnussmus zu Karottensalat. Geht auch mit Mandelmus. Passt auch zu Kidneybohnen.

* 1 Löffel Nussmus
* Sojasauce
* Balsamico-Essig
* Olivenöl (und Leinöl)
* Pfeffer
* (im Originaltipp stand noch: Sherry)


Omas Krautsalat
---------------
07.01.2015, von Oma (Rezept deutlich älter)

### Bausteine

* 1 kleiner Kopf **Weißkraut** (Weißkohl)
* 3 EL Essig
* 3 EL Öl
* Salz, Pfeffer

### Anleitung

Weißkohl hobeln. Salz, Pfeffer, Essig, Öl darübergeben und mit dem Kartoffelstampfer gut unterstampfen.
Guten Appetit.
Was nicht aufgegessen wird, in ein Glasgefäß drücken und verschließen. Hält ca. 1 Woche.


Kartoffelsalat
--------------
08.04.2015, aus The Vegan Zombie

Konzept:

* Kartoffeln kleingeschnitten gar kochen, aber nicht weich
* mit Paprika
* mit Remoulade mit Dill und Gurken (todo: Marke?)
* Details siehe Kochbuch


Tzatziki (Veganitzki)
---------------------
30.03.2015, von StSt, partytauglich

* 1 Menge **Soja-Jogurth**
* 1/6 Menge **saure Gurken** (kleingeschnitten)
* **Knoblauch** kleingeschnitten (so klein wie es geht)
* bei Bedarf Zwiebeln
* Salz und Pfeffer, bei Bedarf Cayenne-Pfeffer, sonstige Kräuter
