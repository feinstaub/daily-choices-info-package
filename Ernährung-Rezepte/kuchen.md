Kuchen
======

[Vegane](../) Rezepte, die am besten mit Zutaten aus [pestizid-freier Landwirtschaft](../../Bio) zubereitet werden sollten. Das heißt in der Regel aus kontrolliert biologischer Landwirtschaft.

<!-- toc -->

- [Schoko-Bananen-Muffins](#schoko-bananen-muffins)
- [Veganer Schokokuchen](#veganer-schokokuchen)
- [Veganer Nusskuchen](#veganer-nusskuchen)
- [Veganer Tortenboden => Erdbeerkuchen](#veganer-tortenboden--erdbeerkuchen)
- [Vom eat-this-Blog](#vom-eat-this-blog)

<!-- tocstop -->

Schoko-Bananen-Muffins
----------------------
April 2018, von Mo

Grundrezept: https://www.chefkoch.de/rezepte/2701651423047733/Vegane-Schoko-Bananen-Muffins.html
Das wurde ein wenig abgewandelt: zum Beispiel noch etwas geriebene Schokolade rein.

Hier das Grundrezept:

* 200 g Mehl
* 50 g Kakaopulver (bio und fair)
* 150 g Zucker
* 1/2 Pck. Backpulver
* 7 EL Öl (Speiseöl)
* 300 ml Wasser
* 1 Pck. Vanillezucker
* 2  Bananen (bio und fair), reif
* 150 ml Kokosmilch

(Arbeitszeit: ca. 15 Min.; Koch-/Backzeit: ca. 30 Min.; Schwierigkeitsgrad: normal)

Mehl sieben und mit Kakao vermengen.
Zucker und Vanillezucker dazugeben. Eine Mulde machen.
Bananen pürieren und mit Kokosmilch vermengen. In die Mulde geben und Öl hinzufügen.
Vorsichtig rühren, damit keine Klumpen entstehen. Nach und nach Wasser dazu mischen. Man braucht eventuell weniger!
Zum Schluss Backpulver (gesiebt) untermischen. Nicht zu viel rühren, sonst werden die Muffins hart.

Ca. 2 EL Teig pro Muffin in die Form geben und bei 180° C Ober-Unterhitze ca. 30 Minuten backen.

Tipp: Je reifer die Bananen desto besser!

Ergibt 12 Muffins.


Veganer Schokokuchen
--------------------
2017, von Bio-S, Besonderheiten: vom Rezept her ohne Ei, ohne Ei-Ersatz, ohne Milch

"Supereinfach, lecker und ist jedes Mal gelungen"

**Zutaten:**

1 Tasse entspricht 250 ml

* 0,75 Tassen neutrales Pflanzenöl (z. B. Rapsöl)
* 2 Tassen Rohrohrzucker
* 2 Tassen kaltes Wasser
* 3 Tassen Weizenmehl
* 6-8 EL Kakaopulver (fair)
* 3 EL Apfelessig
* 20 g Backpulver
* 1 TL Salz
* 1 Päckchen Vanillezucker

Für die optionale Glausur:

* Dunkle Kuvertüre oder Zartbitterschokolade, garniert mit Himbeeren und Mandelstückchen.

**Zubereitung:**

1. Öl und Zucker gut mischen.

2. Essig, Wasser und Vanillezucker dazugeben.

3. Alle "trockenen" Zutaten in einer separaten Schüssel zuerst gut mischen (Mehl, Backpulver, Kakaopulver, Salz). Dann mit den flüssigen Zutaten gut verrühren.

4. Den Teig in eine gefettete und mehlierte Backform geben (ich nehme eine normale Springform; außerdem unten Backpapier mit einspannen) und bei 165 Grad (Umluft) ca. 70 - 75 Minuten backen. Am Besten ihr macht die Stäbchenprobe. Dabei ist es normal, wenn noch ein bisschen Teig am Stäbchen klebt. Der Teig muss nicht schon so fest und krümelig sein, wie bei anderen Rezepten.

5. Nachdem der Kuchen abgekühlt ist, mit einer dunklen Kuchenglasur übergiesen. Dafür eignet sich Kuvertüre, aber auch eine Zartbitterschokolade. Das kann man machen, wie man möchte, und was man gerade da hat.

Gelingt nur mit Obstessig, also keinen Balsamico etc. verwenden. Der Kuchen geht super toll auf und ist fünf Tage lang saftig. :-)

"Für ein optimales Ergebnis verwende ich ausschließlich Zutaten aus kontrolliert biologischem Anbau ;-)"

(abgeändert von [Original-Quelle](http://www.kochbar.de/rezept/378809/Veganer-Schokokuchen.html))


Veganer Nusskuchen
------------------
2017, von K-J, schmeckt laut Urheber am besten mit Bio-Zutaten; noch nicht selber gebacken, aber schon probiert

* 150 g gemahlene Haselnüsse
* 100 g gemahlene Mandeln
* 250 g Grieß
* 200 g Rohrzucker
* 1,5 Packete Vanillezucker
* ein wenig frische Vanilleschote
* 1/2 Liter Sojamilch
* 1 EL Mehl
* 1 Schuss Öl (z. B. Rapsöl)
* 1 Paket Backpulver

Alle Zutaten zusammenmixen, in eine Springform gießen und bei 200 Grad 40 bis 50 Minuten backen.

Klingt einfach.


Veganer Tortenboden => Erdbeerkuchen
------------------------------------
2017, von M..a, Orig-Quelle: Internet

* 225 g Mehl und 1-2 TL Lupinenmehl (für dunklen Boden: 2 EL Kakaopulver)
* 100 g Zucker
* 4 TL Backpulver
* 250 ml Sprudelwasser
* 5 EL Öl
* Öl und Paniermehl für die Form

Alle Zutaten verrühren, so dass ein zähflüssiger Teig entsteht.

Eine Tortenbodenform sehr gründlich einfetten, mit Paniermehl ausstreuen. Teig in die Form gießen und leicht verstreichen.

Bei 180 °C Ober-/Unterhitze 20 - 25 Minuten backen.

Nach dem Auskühlen mit Apfelbrei bestreichen (damit die Erdbeeren mangels Glasur nicht umfallen), Erdbeeren aufsetzen :)

Lecker.

Vom eat-this-Blog
-----------------
empfohlen von O.:

* Rotweinkuchen
* Apfel-Streußel-Kuchen
* Pflaumen-Blechkuchen
