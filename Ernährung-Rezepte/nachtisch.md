Nachtisch und Süßes
===================

[Vegane](..) Rezepte; mit Zutaten aus [pestizid-freier Landwirtschaft](../../Bio) zubereiten.

<!-- toc -->

- [Herzhaft-Fruchtiger Nachtisch mit Milchreis, Fruchtmus und Kakao](#herzhaft-fruchtiger-nachtisch-mit-milchreis-fruchtmus-und-kakao)
- [Kirsch-Eis, zuckerfrei](#kirsch-eis-zuckerfrei)
- [Apfel + Fenchel](#apfel--fenchel)
- [3-Schichtnachtisch mit Apfel](#3-schichtnachtisch-mit-apfel)
- [Amaranth-Kugeln](#amaranth-kugeln)
- [Chiasamenpudding](#chiasamenpudding)
- [Schokomousse auf Cashew-Basis](#schokomousse-auf-cashew-basis)
- [Marzipanquader mit Walnuss](#marzipanquader-mit-walnuss)
- [Schoko selbst gemacht - Blutwurst-Art](#schoko-selbst-gemacht---blutwurst-art)
- [Verschiedenes](#verschiedenes)

<!-- tocstop -->

Herzhaft-Fruchtiger Nachtisch mit Milchreis, Fruchtmus und Kakao
----------------------------------------------------------------
April 2019, von selbst, zuckerfrei

* Milchreis (nicht jedermanns Sache; oder Linsen) kochen und als Schicht in eine Schale geben
* Darauf Fruchtmus verteilen (z. B. Apfel-Birne oder Apfel-Pfirsisch etc.); je dicker, desto fruchdicker
* In einer kleinen Schale Kakaopulver mit Sesam und grauen Weizenkeimen (ggf. stattdessen Haferflocken oder was anders) mischen und drüberstreuen

Kirsch-Eis, zuckerfrei
----------------------
Anfang 2018, selbst

Zuviele Kirschen aus dem eigenen Garten? --> Entkernen und nebeneinander (z. B. in eine Plastikbox) einfrieren.

Im Hochsommer als eiskalten und leckeren Eis-Snack zur Kühlung verspeisen.

Apfel + Fenchel
---------------
ca. 2017

* gute Mischung: Apfel und Fenchelsamen (gemixt?)

3-Schichtnachtisch mit Apfel
----------------------------
Februar 2017, selbst

Grobaufbau: der Schichtnachtisch hat 3 Schichten:

* Oben: Sojaquarkmischung mit Kakaopulvertopping und Heidelbeeren
* Mitte Apfelbrei mit Ingwer (plus Leinsamen und Rosinen)
* Boden: Mischung aus Ahornsirup und Haferflocken und Mandelstückchen

Angerichtet wird das ganze in kleinen Gläschen. Jede Schicht ist ungefähr gleich hoch.

Details:

* Oben:
    * Die Sojaquarkmischung besteht aus
        * Sojaquark (z. B. von Provamel)
        * etwas Süßem, aber nicht zu süß, z. B. Agavendicksaft
        * ggf. Mandelmus (nicht zu viel?)
        * bei Bedarf Heidelbeeren
    * Kakaopulver mit einem Löffel drüberstreichen
    * Abschmecken, dass es schmeckt

* Mitte:
    * Äpfel kleinschneiden und zusammen mit Ingwer in einem Topf kochen
    * Dazu Leinsamen und Rosinen geben
    * Bis ein Brei entsteht

* Boden:
    * Am besten direkt ins Gläschen füllen (Sirup, Flocken und Stückchen)
    * Alternativen
        * Agavendicksaft statt Ahornsirup
        * Cashew-Kerne statt Mandelstücken


Amaranth-Kugeln
---------------
Oktober 2016, von W-J, (noch nicht selber gemacht)

* Ernuss-Mus und Cashew-Mus zu gleichen Anteilen
* Agavensirup/-dicksaft
* Zimt (ich nehme da ziemlich viel...)
* Optional etwas Kakao
* Amaranth, gepoppt

Einfach alles vermischen. Mit nassen Händen kleine Kugeln rollen.
Da die Masse recht klebrig ist, muss man immer wieder die Hände waschen zwischendurch, sonst rollt es sich nimmer gut.

Pro-Tipp: Statt Kugeln zu rollen, kann man auch eine Banane mit einer Gabel auf einem Teller zerdrücken und die Masse als Topping oben drauf streichen. Superlecker!


Chiasamenpudding
----------------
16.04.2016, selbst

Chiasamenpudding mit Bananen, Kakao, Cranberries, Weizenkleie und Äpfel


Schokomousse auf Cashew-Basis
-----------------------------
08.10.2015, Schauspielerin aus einer Fernsehshow, Update 16.04.2016, 2017

* 3/4 Tasse Cashewnüsse (Alternative: Mandeln)
* 3/4 Tasse Wasser
* 1 Banane
* 4 EL Kakaopulver
* 1 TL Zimt
* 1 Prise Salz
* 1 Spritzer Zitrone
* 2 TL Ahornsirup
* ggf. Reissirup zum weiteren Süßen

Alles in den Mixer geben und mixen. Etwas warten, weil (angeblich) noch nachdickt.


Marzipanquader mit Walnuss
--------------------------
21.06.2015, selbst, High Cuisine

* 1 ca. 2x2x1 cm Marzipan-Quader auf einen Teller legen und oben 1/2 Walnuss hochkant reindrücken. Voila.


Schoko selbst gemacht - Blutwurst-Art
-------------------------------------
30.05.2015

Referenzen: [1](http://www.chefkoch.de/rezepte/2309261368477887/Schokolade-selbst-gemacht.html) und [2](http://smort.de/2012/01/19/gesunde-schokolade-selbst-herstellen-rezept/)

Mengen:

90 g Kakaobutter
4 EL Kakaopulver
1 EL Agavendicksaft
2 EL Xucker
Mandelstifte
Zimt, Macis, Piment (oder eine andere Kombination)

Durchführung:

1. Alles in Schüssel außer Kakaobutter
2. geschmolzene (max 45 °C) Kakaobutter dazu
3. mit Schneebesen gut durchrühren
4. In flache Schale füllen so hoch wie die Schoko dick sein soll und in den Kühlschrank zum kühlen

Das obige Originalrezept in einer runden Schüssel zum Kühlen gebracht wirkt wegen der Mandelstifte beim Durchscheiden wie eine Blutwurst.


Verschiedenes
-------------
* 2017: Apfel + Ingwer = lecker
* 2017: Pudding + restliche Semmelbrösel
