Brot
====

<!-- toc -->

- [Dünnes Fladenbrot](#dunnes-fladenbrot)
- [Unsortiert](#unsortiert)

<!-- tocstop -->

Dünnes Fladenbrot
-----------------
Mai 2018, aus: ["Dünnes Fladenbrot"](https://vegan-taste-week.de/vegane-rezepte/duennes-fladenbrot), 30 min, mit Abwandlungen (kein Backpulver und Wasser statt Pflanzenmilchanteil)

* 250 g Mehl (ggf. später mehr, wenn Teig zu flüssig), Weizenmehl oder Dinkelmehl (am besten vollkorn)
* 1/4 TL Salz
* 200 ml Wasser
* 2 EL Olivenöl

Schritte:

1. Mehl, Salz, Wasser und Olivenöl in eine Rührschüssel geben
2. Mit einem Handrührgerät rühren bis ein glatter Teig entsteht
3. Eine Oberfläche bemehlen
4. Ofen mit Backpapier vorbereiten. Zieltemaratur 170 Grad Umluft oder 200 Grad normal.
5. Nach der Reihe aus dem Teig ca. 8 Kugeln formen
6. Pro Kugel:
    * Kugel platt machen (mit der Hand oder mit einem Nudelholz)
    * Wichtig: alles schön einmehlen, damit nichts klebt
    * Flachen Fladen in den Ofen legen
    * Weiter mit der nächsten Kugel
    * Die Backzeit eines Fladens beträgt 10 - 15 Minuten (er wird dann stellenweise bräunlich)
    * Das heißt, sobald das Blech voll ist, kann der erste Fladen rausgenommen werden und auf einen Teller gelegt werden.

Ideen: Zwiebeln rein

Unsortiert
----------
* ["Veganes Brot – unsere besten Rezepte"](https://vegan-taste-week.de/veganes-brot-unsere-besten-rezepte)
    * ["Dünnes Fladenbrot"](https://vegan-taste-week.de/vegane-rezepte/duennes-fladenbrot), 30 min
    * https://vegan-taste-week.de/vegane-rezepte/pestobrot-mit-basilikum
    * https://vegan-taste-week.de/vegane-rezepte/bauernbrot
        * mit Trockenhefe
* https://www.eat-this.org/rezepte/zeit-fuer-brot/
    * https://www.eat-this.org/vollkorn-sauerteigknoten-mit-kuerbis-kraeutern/, 2018
* https://www.veganblatt.com/veganes-gluecksbrot-hermann
    * https://www.veganblatt.com/blitzschnelle-zimt-schnecken
    * https://www.veganblatt.com/bananenbrot
