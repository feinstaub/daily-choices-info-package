foodstruct bis 2016
===================

Im foodstruct finden sich Essenskonstruktionen aus pflanzlichen Bausteinen.
Siehe auch **[basics](basics.md)**. Die folgenden foodnotes zeigen einige Rezeptentwürfe.

Mix-Drinks sind im **[foodfluid](foodfluid.md)** zu finden.

**[Läden und Restaurants](plantstruct.md)**

Kochbücher:

* Vegan Lecker Lecker
* [The Vegan Zombie](http://www.ventil-verlag.de/titel/1588/the-vegan-zombie-deutsche-ausgabe)
* Raw Soul Food

<!-- toc -->

- [Quick-Bohnen mit Senf, Hanföl, Ahornsirup](#quick-bohnen-mit-senf-hanfol-ahornsirup)
- [Tortillas mit Kidneybohnencreme](#tortillas-mit-kidneybohnencreme)
- [Raw: Zucchini-Avocado-Püree (Green Harmony)](#raw-zucchini-avocado-puree-green-harmony)
- [Blätterteig: Pizzarollen mit Wurst](#blatterteig-pizzarollen-mit-wurst)
- [Blätterteig: Mohnschnecken](#blatterteig-mohnschnecken)
- [Ofen: Gemüseblech mit Zitronen](#ofen-gemuseblech-mit-zitronen)
- [Ofen: Süßkartoffelscheiben](#ofen-susskartoffelscheiben)
- [Salbei-Vegiebelle-Knoblauch-"Soße"](#salbei-vegiebelle-knoblauch-sosse)
- [Avocado in Scheiben mit Vinaigrette](#avocado-in-scheiben-mit-vinaigrette)
- [Gemüsenudeln mit Cashew-Pesto](#gemusenudeln-mit-cashew-pesto)
  * [Bausteine](#bausteine)
  * [Anleitung](#anleitung)
- [Helle Mehlschwitze](#helle-mehlschwitze)
- [Humus](#humus)
  * [Bausteine](#bausteine-1)
  * [Anleitung](#anleitung-1)
- [Avocadocreme / Guacamole](#avocadocreme--guacamole)
  * [Bausteine](#bausteine-2)
- [Bilder 2015](#bilder-2015)
- [Tortilla Wrap 2014 in Bildern](#tortilla-wrap-2014-in-bildern)
- [Schwarzer Reis 2014 in Bildern](#schwarzer-reis-2014-in-bildern)
- [Bilder 2014](#bilder-2014)
- [Weihnachten 2014](#weihnachten-2014)

<!-- tocstop -->

---

Quick-Bohnen mit Senf, Hanföl, Ahornsirup
-----------------------------------------
19.04.2015, selbst, ROH

* 1 Schale mit Kidneybohnen (50 - 100 g)
* 1 Teelöffel Senf
* 1 Schuss Hanföl
* 1 Schuss Ahornsirup
* 1 Schuss weißer Balsamico
* 1 bisschen Sesam
* saure Gurken in Würfeln
* Pfeffer
* Muskat

Fertig. Ausbaufähig zum partytauglichen Bohnensalat.

---

Tortillas mit Kidneybohnencreme
-------------------------------
07.04.2015, von StSt

Konzept:

* (Weizen?)Tortillas mit Tomatenmark, Gewürzen, veg. Streukäse
* Bohnencreme mit Stabmixer

---

Raw: Zucchini-Avocado-Püree (Green Harmony)
-------------------------------------------
08.03.2015, aus Raw Soul Food, S. 56

In den Mixer:

* 2 **Zucchinis**
* 1 **Pastinake**
* 1 **Avocado** (ohne Kern und Schale, versteht sich)
* ein paar frische **Basilikumblätter**
* **Gewürze:** Paprika edelsüß, Pfeffer, Curry scharf, Muskat, etwas Salz

Der Mixer ist nun recht voll. Bis zur Hälfte mit Wasser auffüllen.
Mixen (nicht zu lang). Fertig.

2 - 3 EL geröstete Pinienkerne und frische Basilikumblätter zum Garnieren.

---

Blätterteig: Pizzarollen mit Wurst
----------------------------------
07.03.2015, abgewandelt aus The Vegan Zombie, S. 125, (10+15min)

* **Ofen** auf 180 Grad Umluft vorheizen
* **Blätterteig** antauen lassen und vierteln (ca. 15x15 cm große Stücke, mancher Blätterteig kommt auch schon in der Größe)
* **Mexican Salsa Sauce** (oder Tomatenmark) auf die Viertel verteilen, aber min 1 cm Rand lassen
* **Oregano**, **Bohnenkraut** und Pfeffer drauf und frischer **Basilikum**
* je 1 kleine **Scheibe Käse** drauf (z. B. Wilmersburger Cheddar Style)
* je 1 **Würstchen** (z. B. Wheaty Bauernknacker) passend zurechtschneiden und drauflegen
* Den Blätterteig vorsichtig um das Würstchen rollen und die Enden zusammenpressen.
* 15 Minuten backen.

---

Blätterteig: Mohnschnecken
--------------------------
07.03.2015, aus [1](http://www.vegan-news.de/)

* **Ofen** auf 180 Grad Umluft vorheizen
* **Blätterteig** antauen lassen (geht auch ähnlich mit vorgevierteltem Teig)
* In 1 **Topf** 100 ml **Hafermilch** erwärmen. Wenn die Milch zu dampfen beginnt, dann vom Herd nehmen
* 30 g **Dinkelgries**, 70 g **Mohn**, 2 TL **Vanille-Xucker** einrühren und daraus einen Griesbrei rühren
* **Granberries** dazu
* Mischung auf dem Blätterteig verteilen, min. 1 cm Rand
* Blätterteig aufrollen und von dieser Rolle ca. 3 cm breite Scheiben abschneiden.
* Auf ein Backblech setzen und ca. 15 Minuten gold-braun backen.


---

Ofen: Gemüseblech mit Zitronen
------------------------------
28.02.2015, von T..a/Ö

Teil 1:

* 1 **Sellerie**
* 1 **Fenchel**
* 1 - 2 **Paprika**
* 1 - 2 **Möhren**
* **Tomaten** (wer mag)
* 1 **Zitronen** --> Zitronenscheiben mit Schale (plus Saft, aber ohne Kerne)
* **Süßkartoffelscheiben**
* 1/2 **Tempeh** in Scheiben (wer mag)
* 1 **Zwiebel**
* Olivenöl (zum Drübergießen)

Das alles auf ein Backblech in den Ofen bei 175 Grad Umluft ca. 20 Minuten (bis die ersten Sachen schwarz werden und die harten Sachen weich).

Teil 2:

Reis und Salat oder Nudeln als Beilage.
Plus Pfeffer.
Plus bei Bedarf Arche Curry Sate Sauce, damit das ganze etwas flüssiger wird.


Ofen: Süßkartoffelscheiben
--------------------------
08.02.2015, von: K-K

* Eine dicke **Süßkartoffel** in ca. 1 cm dicke Scheiben schneiden und auf ein Backpapier legen.
* (z. B. mit einem Esslöffel) oben mit **Olivenöl** bestreichen.
* (Cayenne)**Pfeffer**, **Salz**, ggf. **Bohnenkraut** draufstreuen.
* Bei 150 Grad Umluft ca. 10 bis 15 Minuten **im Ofen** backen bis die Scheiben sich mit der Gabel durchstechen lassen.
* (Bei Bedarf zusammen mit [Soyanada](http://www.alles-vegetarisch.de/frischkaese-vegan-bio-soja-streichkaese-wie-rahm-pflanzlich,art-4688) (Frischkäse) servieren.)

---

Salbei-Vegiebelle-Knoblauch-"Soße"
-----------------------------------
20.11.2014, von R.

**Knoblauch** (2 Zehen) und
geschnittene eingelegte **getrocknete Tomaten** (1/2 Glas, 10 Stücke)
(bei Bedarf auch gerne **frische Cherry-Tomaten** klein geschnitten)
in **Olivenöl** andünsten.
Dann **Veggiebelle** (200 g)
und **Salbei**blätter (einige, gerupft) hinzugeben.
Ggf. mit Pfeffer abschmecken und auf **Nudeln** oder Linsen servieren.

---

Avocado in Scheiben mit Vinaigrette
-----------------------------------
17.11.2014, von K-K

* 1 **Avocado**
* **Essig, Öl, Pfeffer**

Avocado in handliche Scheiben/Streifen schneiden und eine Mischung aus Essig, Öl und Pfeffer drübergeben.
Zusammen mit **Weißbrot** servieren - ggf. mit Frischkäse (Soyanada). Oder ohne zum späteren Auftunken der Soße.

---

Gemüsenudeln mit Cashew-Pesto
-----------------------------
24.08.2014, selbst

### Bausteine

* 1/3 **Zwiebel**
* 2 **Tomaten**
* 1 kleine **Karotte**
* Bratöl
* **Pfeffer**
* 1 mittlere **Zuchini**
* **GEPA Salsa Curry Cashew** (Cashewkerne (60%), Sonnenblumenöl, Salz, Curry (1,5%), Curcuma, Knoblauch, Schwarzer Pfeffer)
* **Nudeln**

### Anleitung

Die klein geschnittenen Zwiebeln, die grob gestückten Tomaten und die in kleine Scheiben geschnittene Karotte zusammen mit etwas Bratöl und Pfeffer so lange in einer Pfanne dünsten, bis die Karotten weich geworden sind und die Tomaten am Zerfallen sind (ca. 10 Minuten).
Die Zuchini würfeln und in die Pfanne dazugeben. Solange weiterdünsten, bis die Zuchini-Stücke bissfest weich sind.
Zusammen mit einer gewünschten Menge an Gepa Salsa Curry (muss nicht extra warm gemacht werden) auf Nudeln servieren.

---

Helle Mehlschwitze
------------------
15.08.2014, von M..a

**Alsana** in einem Topf erhitzen. **Lupinenmehl** unterrühren bis eine Paste entsteht. Dann etwas X-Drink dazugeben.

TODO...

---

Humus
-----
16.06.2014, Biomarkt-Tipp / s. a. https://de.wikipedia.org/wiki/Hummus

### Bausteine

* 200 g **Kichererbsen** (aus dem Glas)
* 2 stark gehäufte Teelöffel **Tahin** (Sesam)
* etwas Limettensaft (oder ähnliche **Säure**, um Sesam aufzuspalten, damit er nicht so bitter ist)
* **Olivenöl**, Leinöl
* ggf. etwas Wasser
* **Kreuzkümmel**
* (frischer) **Basilikum**
* Pfeffer

### Anleitung

Alles mit dem Stabmixer **mixen**. Kalt, z. B. zu warmen Nudeln oder Linsen servieren, zusammen mit passierten Tomaten als Soße und sonstigen Dingen. Rest in Glasdosen  zur späteren Verwendung kühlstellen.

---

Avocadocreme / Guacamole
------------------------
26.11.2013, selbst

### Bausteine

* 1 **Avocado**
* 3 Cherry**tomaten**
* Tomatenmark
* **Olivenöl**
* **Pfeffer**, ggf. **Zimt, Muskat**
* bei Bedarf: wenig Zwiebel, etwas Soja-Jogurth, Leinöl
* 03.03.2015: Geheimtipp: **Hanföl** und ein wenig **Ingwer**

---

Bilder 2015
-----------

![](img/2015-SAM_1010.JPG)
![](img/2015-SAM_1090.JPG)
![](img/2015-SAM_1092.JPG)
![](img/2015-SAM_1094.JPG)


Tortilla Wrap 2014 in Bildern
-----------------------------

![](img/2014-CIMG3683.JPG)
![](img/2014-CIMG3684.JPG)
![](img/2014-CIMG3685.JPG)
![](img/2014-CIMG3686.JPG)
![](img/2014-CIMG3687.JPG)
![](img/2014-CIMG3688.JPG)


Schwarzer Reis 2014 in Bildern
-----------------------------

![](img/2014-CIMG3690.JPG)
![](img/2014-CIMG3691.JPG)
![](img/2014-CIMG3695.JPG)


Bilder 2014
-----------

![](img/2014-CIMG3698.JPG)
![](img/2014-CIMG3699.JPG)
![](img/2014-CIMG3700.JPG)
![](img/2014-SAM_0988.JPG)


Weihnachten 2014
----------------

![](img/2014-SAM_1005.JPG)
