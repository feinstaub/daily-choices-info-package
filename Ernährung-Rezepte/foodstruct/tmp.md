tmp bis 2016
============

2015-10-30: Mittagessen: Gemüsepfanne
-------------------------------------
Gemüse:

* **Fenchel** (B ok)
* Aubergine, Zuchini, Tomaten (B ok)
* **Zwiebeln** (B ?)
* (**Kohlrabi** (B Bl))
* (**Paprika** (B Bl))
* Knoblauch, wenn gewünscht

Soße (zum Binden des ganzen Gemüses):

* 100 g weißes **Mandelmuß** mit 240 ml Wasser anrühren
    * alternativ: **Hafersahne** (z. B. "Hafer Cuisine")
    * alternativ: Lupinenmehl oder Johannisbrotkernmehl
* dazu 2 EL Olivenöl unterheben

Gewürze:

* **Pfeffer**, **Kurkuma** (oder Curry), **Kümmel**, **Ingwer**
* Muskat, Paprika, Salz
* dazu **Zitrone**(nsaft)

Eiweiß:

* **Linsen** (fertig aus dem Glas) oder Erbsen oder Kichererbsen oder **Räuchertofu**

Kohlenhydratbasis:

* **Quinoa** oder Reis
