Smoothies aus dem Mixer
=======================

---

Papaya, Orange, Lucuma, Minze
-----------------------------
21.06.2015, von S.

2 Papaya (schälen und den "Kaviar" vorher rausmachen)
2 kleine Orangen
2 EL Lucumapulver
5 Blätter frische Minze
Auf 1000 ml mit Wasser auffüllen.
Mixen.

---

Grüner Kobold (Melone & Spinat)
-------------------------------
2016, abgewandelt aus [1](Für mich vegan, Heft 1, 2014).

* 1/2 kleine **Wassermelone** (laut Rezept Honigmelone)
* 180 g **Spinat** (ca., tiefgefroren)
* 1 Banane
* Saft einer 1/2 Zitrone
* Ein wenig **Lucuma**-Fruchtpulver (sorgt für sehr-leicht-karamelligen Geschmack und etwas Schaum)

(Auf das erste Probieren hat mich das ganze an einen Salatgurkendrink erinnert.
=> Merke: Additive Gemüsemischung: Wassermelone + Spinat = Salatgurke; oder so ähnlich)

---

Neue Zutaten: Paranüsse und Seidentofu
--------------------------------------
29.03.2015

* 1 Orange
* 1 Banane
* 2 mittlere Äpfel
* ein paar **Paranüsse**
* 250 g **Seidentofu** => damit es lassiartig wird
* Wasser

---

Neue Zutaten: Fenchelsamen etc.
-------------------------------
22.03.2015

* 1 Orange
* 2 mittlere Äpfel
* 5 Elemente Spinat
* 1 Handvoll Kokosflocken
* 3 getrocknete **Datteln** (kleingeschnitten)
* 3 TL weißes Mandelmuß
* 1 TL **Fenchelsamen** (lecker!)
* 1/2 TL **Macis (Muskatblüte)**
* Auffüllen auf ca. 1000 ml mit Wasser

---

Wenn der Mixmann zweimal klingelt (dunkel-lila)
-----------------------------------------------
11.03.2015

(Erster Drink: Pina-Colada mit Kokosmilch, Hafersahne und viel Ananas und frische Minze und Wasser)

Zweiter Drink:

Übrigens: **50:50** ist ein gutes Verhältnis von Frucht und Grün (zumindest laut [Grüne Smoothies von GU](http://www.gu.de/buecher/kochbuecher/gesunde-leichte-kueche/441986-gruene-smoothies/)).

* 3 mittlere Äpfel
* 1 kleine Banane
* 1 große Möhre
* 1 Daumenspitze Ingwer
* 6 Spinatelemente
* eine Handvoll tiefgefrorene **Heidelbeeren**
* ein bisschen **frische Minze**
* Auffüllen mit Wasser auf 1300 ml

---

Orange County (Orangen & Gojibeeren)
------------------------------------
07.03.2015

Abgewandelt aus [1](Für mich vegan, Heft 1, 2014).

* 4 **Orangen**
* 1 **Banane**
* 4 TL **Gojibeeren**
* 3 TL **Mandelmuß** (für die Cremigkeit)
* 1/2 Daumen **Ingwer**
* Gewürze: Muskat (am meisten), Zimt, Kardamom
* ca 200 ml **Haferdrink**
* auf ca. 1000 ml mit Wasser auffüllen

Gut durchmixen.

---

Spinat-Matcha-Mix
-----------------
09.02.2015

Frei nach der Green-Smoothie-Formel [1](www.weltveganmagazin.de, Ausgabe 01, 2015).

1) Basis:       **Wasser** (auf 1250 ml auffüllen)
2) Das Grün:    6 **Spinat**-Elemente
3) Das Obst:    3 **Äpfel**, 1 Karotte, 1 Banane
4) Die Booster: 1 kleiner Teelöffel **Matcha**, 1 Scheibe **Ingwer**, ein paar Walnüsse

Mindestens 2 Minuten gut durchmixen.

---

Mango-Lassi-X
-------------
27.01.2015

* 1 Mango (nur Fruchtfleisch)
* Soja-Jogurth
* Hafer-Drink oder Wasser
* etwas Xucker zur Süßung
* Zimt
* Muskat

Alles mixen.

---

Salbeigrüner Salbei-Drink
-------------------------
18.01.2015

* 3-4 mittlere Äpfel
* 1 Element **Tiefkühlspinat** (für die Farbe)
* ein paar Wahlnüsse
* ein paar frische **Salbei-Blätter**
* auf 1200 ml mit Wasser auffüllen

---

Indisch-Gelber Banane-Orange-Mandarine-Apfel-Kokos-Drink
--------------------------------------------------------
17.01.2015

* 1 Banane
* 1 Orange
* 1 Mandarine
* ca. 3 mittlere Äpfel
* eine Handvoll **Kokosflocken**
* auf 1200 ml mit Wasser auffüllen

---

Leichtgrüner Birnen-Bananen-Ingwer-Drink
----------------------------------------
04.01.2015

* 3 Birnen
* 1 mittelgroße Banane
* 1/2 Daumen Ingwer
* auf 1200 ml mit Wasser auffüllen.
* Gut durchmixen.

---

Hellgrüner Apfel-Spinat-Ingwer-Drink
------------------------------------
22.12.2014

* 6 mittelgroße Äpfel
* 3 Spinat-Elemente
* 1/3 Daumen Ingwer
* Auf 1200 ml mit Wasser auffüllen.
* Gut durchmixen.

---

Milder Rote-Beete-Smoothie
--------------------------
26.11.2014

* 1/2 **Rote Beete**
* 1 **Banane**
+ 3 "Bällchen" tiefgefrorener **Spinat**
* 1 **Apfel**
* 1 kleine Tomate (wenn gerade zur Hand)
* 1 Daumen **Ingwer**
* Auffüllen auf 900ml mit **Hafer-Drink**

Alles fein mixen.
Das Ergebnis ist hell-rosa, durchsetzt von weißen Drink-Partikeln, mit einer schaumigen Oberfläche.

---

Hellminzgrüner Minz-Kokos-Vanille-Cocktail
------------------------------------------
18.08.2014

### Bausteine

* 3 Teile **Kokosmilch**
* 2 Teile Soja-Vanille-Drink
* ca. 4 Stangen **Minze** (ohne Stiel) => Minzblätter
* 1 gepresste **Zitrone** inkl. Fruchtfleisch
* ein wenig Vanillezucker
* Reste von Weintrauben, falls vorhanden

Alles fein mixen und bei Bedarf mit einem Schuss Amaretto und Rum servieren.

---

Mintgrüner Banane-Apfel-Spinat-Ingwer-Smoothie
----------------------------------------------
08.08.2014

### Bausteine

* 1 **Banane**
* 3 mittlere **Äpfel**
* 2 “Bällchen” tiefgefrorener **Spinat**
* 1/2 Daumenspitze **Ingwer**
* 1 Schuss **Leinöl**
* 1 Schuss **Ahornsirup**
* ca. 400 ml Flüssigkeit (Dinkeldrink und Wasser)
* (2 saure Pfirsische)

Das alles fein mixen.

Weitere Idee: Fenchel oder Sellerie

---
