Läden und Restaurants
=====================

<!-- toc -->

- [Nach Ort](#nach-ort)
- [Gießen](#giessen)
- [Bad Homburg](#bad-homburg)
- [FFM](#ffm)
- [Mainz](#mainz)
- [DA](#da)
- [HP, Odenwald, HD](#hp-odenwald-hd)
- [Viernheim](#viernheim)
- [HD](#hd)
- [Mannheim](#mannheim)
- [Karlsruhe, Raum Baden-Baden](#karlsruhe-raum-baden-baden)
- [Baiersbronn](#baiersbronn)
- [Offenburg, Freiburg](#offenburg-freiburg)
- [Friedberg, Augsburg, Nürnberg](#friedberg-augsburg-nurnberg)
- [Bad Waldsee, Singen](#bad-waldsee-singen)
- [Related](#related)

<!-- tocstop -->

Vegan/bio/freundlich Ausgehen und Einkaufen.

Angaben zu Öffnungszeiten sind konservativ. V: rein vegan, (V): vegetarisch

todo/gut?: http://www.vegman.org/ (für Reisen)

Nach Ort
--------
* Oldenburg/Hostein
    * Bistro [Biomarkt&Bistro](http://www.biomarkt-bistro.de), Schauenburgerstrasse 5 | 23758 Oldenburg i.H., MO - FR 11.00 bis 16.00, SA 9.00 bis 13.00
        * Bio / etwas vegan
* Eisenach, Leipzig, Dresden:
    * [Zucker+Zimt](http://www.zucker-zimt-eisenach.de/)
        * kein Bio?
* Zeltingen-Rachtig/Mosel
    * [Die Weinstube](https://www.facebook.com/Weinstubevegan)
        * "Die Weinstube - 100 % vegan - Das vegane Restaurant an der Mosel"
        * Bio?
* ...
* Groß-Gerau
    * V-Restaurant [Vegano](https://www.facebook.com/Vegano-1566103680291547/), Alte Straße 1, Groß-Gerau, Tel.: 06152 52217
        * kein Bio?
* ...
* Stuttgart
    - Coox & Candy V
    - kleinundfein V
    - Körle und Adam V
    - superjami V


Gießen
------
* V-Restaurant [Vollwert-S.](http://www.vollwert-s.de)
    * alles vegan?
    * bio?
* **!!!** Biomarkt&Bistro, V-Restaurant [Veganatural](http://veganatural.de/)
    * "Euer veganes Bio-Bistro in Gießen"

Bad Homburg
-----------
* [Apfelkern & Kolibri](http://www.apfelkern-und-kolibri.de/)
    * "Wir kochen und backen ganz ohne tierische Produkte. Unsere Zutaten sind größtenteils biologischer oder regionaler Herkunft."
* [La Piazza Toscana](http://www.lapiazzatoscana.de)
    * kein Bio?

FFM
---
- V-Restaurant [Chimichurri](http://www.chimichurri-frankfurt.de/), Im Prüfling 63, 60389, 18:00-23:00
- V-Restaurant [Wondergood](http://www.wondergood.de/), Preungesheimer Str. 1, 60389
- Restaurant [ginkgo](http://www.ginkgo-frankfurt.de/), Bergerstrasse 81, 60316
    "Seit 2012 interessieren wir uns persönlich mehr und mehr für die vegane und vegetarische Ernährung – ein Einfluss, der sich natürlich auch auf der Karte zeigt."
- Bio-Burgerrestaurant [Wiesenlust](http://www.wiesenlust.de), Bergerstrasse 77, 60316, Montag bis Donnerstag und Sonntag 11:00 - 23:00 Uhr - Küchen bis 22:00 Uhr, Freitag und Samstag 11:00 - 23:30
- V-Imbiss direkt vom Metzger [bioSpahn vegan](http://biospahn-vegan.de/), [Vegan-Info](Info: http://biospahn-vegan.de/de/vegan-info?coID=11), Berger Strasse 222, Bornheim, Mo-Sa, Uhrzeit?
- Bio-(V)-Restaurant [VEVAY](http://www.vevay.net/), Neue Mainzer Straße 20, 60311 FFM, Mo-Fr 9:00-21:00, Sa 10:00-21:00, So zu
- TODO: (V)-Restaurant [savory](http://www.savory-thevegtory.de),  Burgfriedenstraße 2 / Ecke Alt-Rödelheim, Di-Fr: 11:30-20:00 Uhr, Sa: 11:00-16:00 Uhr
    - [Anfahrt](https://www.google.de/maps/dir/Hauptbahnhof,+Frankfurt/Burgfriedenstra%C3%9Fe+2,+60489+Frankfurt/@50.114939,8.6257427,14z/data=!4m14!4m13!1m5!1m1!1s0x47bd0955616e2395:0x651064cdb7f56566!2m2!1d8.664613!2d50.107733!1m5!1m1!1s0x47bd099b88ff3e33:0xa1def54c9951cdc8!2m2!1d8.6121931!2d50.1251901!3e3)
    - Rödelheim - Bockenheim - Westend-Süd/Nord - Innenstadt/Nordend-West - Nordend-Ost/Ostend - Bornheim
- TODO: V-Cafe [Extravegant](http://www.extravegant-ffm.de), Berger Straße 154,  Montags bis Freitags von 10:00 Uhr bis 18:00 Uhr und Samstags und Sonntags von 10:00 Uhr bis 19:00 Uhr
- (V)-Restaurant [Naturbar](http://www.naturbarfrankfurt.de/), Oederweg 26, 60318, Mo-Fr: 11:30-15:30, 18:00-23:00, Sa: 18:00-23:00, So/Ft zu
- V-Cafe [http://www.edelkiosk.de/](http://www.edelkiosk.de/index.html), Rhönstraße 119, 60385, Mo zu
- Restaurant [Im Herzen Afrikas](http://im-herzen-afrikas.de/index.html), Gutleutstrasse 13, Dienstag bis Samstag 18 bis 1 Uhr, Sonntag 16 bis 22 Uhr, Montag geschlossen
- [burittobande](https://burritobande.de/), derzeit Hauptbahnhof
- [my indigo], Frankfurter Markthalle, gegenüber Gleis 6, Hauptbahnhof
- Le Crobag (belegtes Brötchen), Hauptbahnhof
- wursthelden (gute Curry-Saucen für Pommes), Hauptbahnhof
- Öko-Restaurant [Arche Nova](http://restaurant-archenova.de), Montag - Samstag 11:30 bis 24:00 Uhr, Sonntag 11:30 bis 18:00 Uhr, Küche bis 16.30 Uhr
- TODO: [rag bar](https://www.facebook.com/rawandgrateful) (Café · Live & raw food restaurant), Zeil 112-114, Zeilgalerie, Mon: 13:00–18:00, Tues - Thurs: 11:00–18:00, Fri - Sat: 11:00–20:00
- (todo?) Hotel, (V?)-Restaurant [Saravana Bhavan](http://www.saravanabhavan.com/restaurants.php?cn=Germany&cy=Frankfurt&rid=68), Kaiserstrasse 66, Open on all 7 days from 11 a.m. to 10.30 p.m.
- (todo/Nur Lieferservice?): [Veggie House](http://www.veggiehouse-frankfurt.de/), Schlossstr. 125

Mainz
-----
* siehe [vegan-o-mania blog](http://vegan-o-mania.blogspot.de/p/uber-mich.html)
* TODO: am "Kardinal Volk Platz": Thai Express mit Vegan-Ente
* todo: [MoschMosch](http://vegan-o-mania.blogspot.de/2014/05/moschmosch-der-veganer-der-ersten.html), Mailandsgasse 3, Mo-Sa: 11:00-23:00, So: 13.00-22.00
    "Edamame" - frische, gekochte Sojabohnen zum Auszuzeln
    [Abstand zur Rheingoldhalle: 400 m, 5 Fußminuten](https://www.google.de/maps/dir/Rheingoldhalle,+Rheinstra%C3%9Fe+66,+55116+Mainz/Mailandsgasse+3,+55116+Mainz/@50.0009213,8.2730414,17z/data=!3m1!4b1!4m14!4m13!1m5!1m1!1s0x47bd97052a5458f3:0x43db4c07d0911440!2m2!1d8.27561!2d50.0019!1m5!1m1!1s0x47bd971b1bdbab99:0xcd86b73dc733010c!2m2!1d8.27439!2d50!3e2)
* todo: "Le cedre du Liban (libanesischer Imbiss) kann man auch den Falafel-Sandwich sowie den Makali-Sandwich vegan bestellen"
    [yelp](http://www.yelp.de/biz/le-c%C3%A8dre-du-liban-mainz), Emmeransstr. 35, Mo-Sa: 11:00-21:00, So: zu
    [Abstand zur Rheingoldhalle: 650 m, 8 Fußminuten](https://www.google.de/maps/dir/Rheingoldhalle,+Rheinstra%C3%9Fe+66,+55116+Mainz/Emmeransstra%C3%9Fe+35,+55116+Mainz/@50.0017028,8.2713794,18z/data=!3m1!4b1!4m14!4m13!1m5!1m1!1s0x47bd97052a5458f3:0x43db4c07d0911440!2m2!1d8.27561!2d50.0019!1m5!1m1!1s0x47bd97032d2aa57f:0xa04a1f382923b90e!2m2!1d8.26959!2d50.00199!3e2)
* todo: wo ist das? "Die besten Falafel gibt es mit ABSTAND beim Libanesen an der Ecke hinterm Kaufhof, wo früher das Konservatorium war!"
* todo: "Hier gibt es vegetarischen und veganen Kuchen. Ausprobiert habe ich die Bäckerei aber noch nicht."
    [kornland-baeckerei](http://www.kornland-baeckerei.de/), Weißliliengasse 23, Montag - Fr: 07:00 - 18:00 Uhr, Sa: 08:00 - 14:00 Uhr
    [Abstand zur Rheingoldhalle: 1 km, 12 Fußminuten](https://www.google.de/maps/dir/Rheingoldhalle,+Rheinstra%C3%9Fe+66,+55116+Mainz/Wei%C3%9Fliliengasse+23,+55116+Mainz/@49.999092,8.2722321,17z/data=!4m14!4m13!1m5!1m1!1s0x47bd97052a5458f3:0x43db4c07d0911440!2m2!1d8.27561!2d50.0019!1m5!1m1!1s0x47bd971955cd4bc5:0x16a3ec90ad53f40a!2m2!1d8.271!2d49.99656!3e2)
* todo: V-Eco-Cafe [Möhren Milieu-Eco Café](http://moehren-milieu.de), Adam-Karrillon Straße 5, 55118 Mainz,
    Mo: zu, Di-Sa: 10:00-20:00, So:10:00-18:00
    [Abstand zur Rheingoldhalle: 1.3 km, 17 Fußminuten](https://www.google.de/maps/dir/Rheingoldhalle,+Rheinstra%C3%9Fe+66,+55116+Mainz/Adam-Karrillon-Stra%C3%9Fe+5,+55118+Mainz/@50.0038241,8.2615702,16z/data=!4m14!4m13!1m5!1m1!1s0x47bd97052a5458f3:0x43db4c07d0911440!2m2!1d8.27561!2d50.0019!1m5!1m1!1s0x47bd96fc8cf68f83:0x1b8c9679b803cedf!2m2!1d8.261494!2d50.004616!3e2)
* todo: ["Big Döner" am Mainzer Hauptbahnhof](http://vegan-o-mania.blogspot.de/2013/05/die-besten-falafel-der-stadt-bei-big.html), Bahnhofplatz 1, "Falafelrolle vegan, bitte",
    So,Mo,Di: 9:00-03:00, Mi,Do: 9:00-06:00, Fr,Sa: 9:00-07:00,
    [Abstand zur Rheingoldhalle: 1.5 km, 18 Fußminuten](https://www.google.de/maps/dir/Rheingoldhalle,+Rheinstra%C3%9Fe+66,+55116+Mainz/Bahnhofplatz+1,+55116+Mainz/@50.0017124,8.2585773,15z/data=!3m1!4b1!4m14!4m13!1m5!1m1!1s0x47bd97052a5458f3:0x43db4c07d0911440!2m2!1d8.27561!2d50.0019!1m5!1m1!1s0x47bd96e4a43036c7:0x369c1263c0ef3ae5!2m2!1d8.25866!2d50.00193!3e2)
* todo: "Es gibt noch ein veganes Frühstück bei der AnnaBatterie in der Neustadt
* (todo:) [Sichtbar Vegan - Mainz](https://www.facebook.com/SichtbarVeganMainz?fref=ts), [vegan-o-mania-Kommentar](http://vegan-o-mania.blogspot.de/2013/07/vegan-schlemmen-in-der-sichtbar-in-mainz_21.html), Hintere Bleiche 29
    Mo: zu?, Di-So: 18.00-22:00, Sa: 19:00-22:00
    [Abstand zur Rheingoldhalle: 1 km, 13 Fußminuten](https://www.google.de/maps/dir/Rheingoldhalle,+Rheinstra%C3%9Fe+66,+55116+Mainz/Hintere+Bleiche+29,+55116+Mainz/@50.0019249,8.2675237,17z/data=!3m1!4b1!4m14!4m13!1m5!1m1!1s0x47bd97052a5458f3:0x43db4c07d0911440!2m2!1d8.27561!2d50.0019!1m5!1m1!1s0x47bd96fd352c77f9:0x78e4e6a56cd7e8c0!2m2!1d8.264!2d50.0024!3e2)
* todo: (V)-Restaurant [Schrebergarten](https://www.facebook.com/schrebergartenmainz/info?tab=page_info), Kurfürstenstr. 9, Mo-Fr: 11:30–20:30, Sat-Sun: 12:00–20:00
    [Abstand zur Rheingoldhalle: 1,5 km, 20 Fußminuten](https://www.google.de/maps/dir/Rheingoldhalle,+Rheinstra%C3%9Fe+66,+55116+Mainz/Kurf%C3%BCrstenstra%C3%9Fe+9,+55118+Mainz/@50.0032232,8.2624922,16z/data=!3m1!4b1!4m14!4m13!1m5!1m1!1s0x47bd97052a5458f3:0x43db4c07d0911440!2m2!1d8.27561!2d50.0019!1m5!1m1!1s0x47bd96fb9a18f615:0x939151b2669d28ec!2m2!1d8.2591032!2d50.0058923!3e2)
* todo: "Im [Restaurant Kupferberg Terrassen](http://www.restaurant-kupferberg.de/) gibt es ein veganes 4-Gang-Menü", Kupferbergterrasse 17-19
    Warme Küche: von 11:30 Uhr bis 14 Uhr und 17:30 Uhr bis 22 Uhr
    [Abstand zur Rheingoldhalle: 1,4 km, 19 Fußminuten](https://www.google.de/maps/dir/Rheingoldhalle,+Rheinstra%C3%9Fe+66,+55116+Mainz/Kupferbergterrasse+19,+55116+Mainz/@49.9993923,8.2671204,17z/data=!3m1!4b1!4m14!4m13!1m5!1m1!1s0x47bd97052a5458f3:0x43db4c07d0911440!2m2!1d8.27561!2d50.0019!1m5!1m1!1s0x47bd96e21c075155:0x9f3fb1886a224c24!2m2!1d8.2632!2d49.9976!3e2)
* todo, siehe Liste [veganice.eu/restaurants/mainz](https://veganice.eu/restaurants/mainz)

DA
---
* (V)-Bistro www.cafe-habibi.de, Landwehrstraße 13, Darmstadt
    * Bio? - "Wie euch vielleicht aufgefallen ist, ist das Wort Bio von unserer Homepage verschwunden. An unserem Anspruch, unserer Einkaufsphilosophie und unserer Qualität hat sich nichts geändert, nur bedarf es nun eines Zertifikats, um Bioware als solche bezeichnen zu dürfen."
* Bistro www.mondo-daily.de, Grafenstraße 31, Darmstadt
    * todo
* V-Cafe [Veelgood](https://www.facebook.com/www.veelgood.de), Mühlstrasse 76, DA, 06151 6011450, Mo-Do 9:00-19:30, Fr-Sa: 11:00-23:00
    * geschlossen

HP, Odenwald, HD
----------------
* Restaurant [Indian Palace](http://www.indian-palace-heppenheim.com), Kalterer Straße 7, 64646 (am Bahnhof)
    * kein Bio
* Restaurant [Taj Tandoori](http://www.golocal.de/heppenheim/indische-restaurants/taj-tandoori-MJ4fe/), Ludwigstr.B3 32, 64646 (B3)
    * kein Bio
* (Eher) Imbiss Thai (Parkhof)
    * kein Bio
* Imbiss [Patel Drive In](http://www.patel-drive-in.de), Lorscher Straße 53, 64646 (beim Kreisel)
    * kein Bio
* Restaurant [Jaipur Indische Tandoori Restaurant](http://www.yelp.de/biz/jaipur-tandoori-bensheim), Lammertsgasse 15, 64625 Bensheim
    * kein Bio?
* [Landgasthof Dorflinde](http://www.landgasthof-dorflinde.de/restaurant-und-erlebniswelt/), Siegfriedstraße 14, 64689 Gras-Ellenbach
    * (todo?)
    * kein Bio?
* [Kunst-Kiosk Erlenbach](https://www.facebook.com/KunstKioskErlenbach/info?tab=page_info), kunstverein erlenbach, 64658 Fürth-Erlenbach, So: 11:00-18:30 (06253/22663)
    * kein Bio?
* [Clubrestaurant La Pineta](www.tc-schriesheim.de/clubrestaurant/), Schriesheim
    * (todo?)
    * kein Bio?

Viernheim
---------
* Restaurant/Italiener [Galicia/Donnici](http://www.donnici-viernheim.de/kontakt-und-impressum.php), Bürgerhaus, Kreuzstr. 2, 68519 Viernheim

HD
---
* http://heidelberg-vegan.org/
* [Volcano Handschuhsheim](https://www.facebook.com/volkano.heidelberg/info/?tab=page_info)
* (V)-Selbstbedienungsrestaurant **red**, HD, Mo-Fr 11:00-22:00
* V-friendly [Malaysisches Restaurant Serai](http://www.serai.de/de/about/lage), Schillerstrasse 28-30, Weststadt, Vegan-Büffet am ersten Montag im Monat 18:00-22:00, Di-Fr: 12:00-14:00, 18:00-22:00, Sa: 18:00-23:00, So+Mo zu
* V-Cafe: holy kitchen, ca. 11-19 Uhr
* todo: Schmidt's (vegane Kuchen, Sojamilch, leckere Kakaos und Kaffee)
- Die Kuh, die lacht (komplett vegane Burger im Angebot)

Mannheim
--------
* https://de-de.facebook.com/VeganInMannheim
* V-Restaurant [LöVenzahn](http://www.loevenzahn.de/), Eichelsheimer Str.42, 68163 Mannheim-Lindenhof, Telefon: MA (06 21) 43 95 40 00, [Anfahrt, am Hbf](https://www.google.de/maps/place/Eichelsheimer+Str.+42,+68163+Mannheim/@49.4738982,8.467845,16z/data=!4m2!3m1!1s0x4797cc1ce4db0e95:0x7e630152aeebbea1) (noch geöffnet?)
* (V)-[Cafe Vogelfrei](https://de-de.facebook.com/cafevogelfrei) (Essen vegan, aber es gibt auch Kuhmilch), C3-20 Mannheim, Mo: zu, Di-Sa: 11:00–19:00, So: 14:00–22:00
* (V) SB-Restaurant Heller's
* todo: (V) Kombüse (Restaurant / Cafe), Jungbuschstraße 23, 68159 Mannheim
* TODO: Cafe Rost, Pflügersgrundstr. 16 (nördlich des Neckars)
* todo: (V) Sonnenblume (Restaurant), T 3,7, 68161 Mannheim
* todo: (V) Supan's Küche (thailändischer Imbiss), P 2,6, 68161 Mannheim
* todo: (V) Restaurant Lindbergh, Seckenheimer Landstraße 170, 68163 Mannheim

Karlsruhe, Raum Baden-Baden
---------------------------
* (todo) Imbiss Falafelhaus, Amalienstr. 51, 76133 KA, 01577 6017307, Fr 11-23Uhr, ...
* Naturkost-Cafe (sehr begrenzte Auswahl), Büttenstr. 8, 76530 Baden-Baden, 07221-31448, Mo-Fr 11:30-17:00
* [mañana fair trade shop & café](http://www.fairmanana.de/), Büttenstrasse 4, Baden-Baden, Mo: zu, Di-Fr: 10:00-18:30, Sa: 10:00-17:30
* (todo?) [Landcafe zur Sonne](https://de-de.facebook.com/LandcafeZurSonne), Bammental

Baiersbronn
-----------
* (todo?) [Holzschuh's Schwarzwaldhotel](http://www.schwarzwaldhotel.de/), [V-Biozimmer](http://www.schwarzwaldhotel.de/?lang_id=DE&page_id=78#A_904)

Offenburg, Freiburg
-------------------
* (todo?) Gälriwli, veget. Vollwert, Offenburg
* TODO (V)-Restaurant [El Haso](http://el-haso.de/), Leopoldring 1, 79098 Freiburg, Di - Sa ab 18 Uhr

Friedberg, Augsburg, Nürnberg
-----------------------------
* todo: V-Cafe [COSMOSCAFE e.V.](http://cosmoscafe.jimdo.com), Jungbräustrasse 8, 86316 FRIEDBERG
* [Mount Lavinia](http://www.mountlavinia.de/), ceylonesischen Restaurant, Jakobsplatz 22, 90402 **Nürnberg**
* todo: V-Biorestaurant [The Tasty Leaf](http://www.tasty-leaf.de), **Nürnberg**, Di-Do 11:30-22:00, Fr 11:30-23:00, Sa 15:00-23:00, So 10:00-15:30

Bad Waldsee, Singen
-------------------
* todo: (V)-Restaurant [Die Möhre](http://moehrerestaurant.de), Bad Waldsee (nördl. v. Bodensee)
* (todo?): [Vegan Tempel Restaurant](https://www.facebook.com/pages/Vegan-Tempel-Restaurant-mit-Teehaus-seit-1982/190223997680082?sk=timeline&ref=page_internal), Singen

Related
-------
* [Mehr: nach Städten sortiert](http://www.peta.de/restaurants)
* [Europa](http://www.topvegetarianrestaurants.net/)
* http://visionvegan.blogspot.de/p/in-rhein-main.html
