Basis-Bausteine
===============

Bausteine
---------
Sammlung von Zutaten aus denen sich leckere Dinge machen lassen:

### 2021
* Spielberger **Hirseflocken** Vollkorn; gut für Brei
* **Rote Beete**: "eignet sich auch sehr gut für Rohkost, z. B. gerieben 2:1 mit Apfel"
* **Rosenkohlköpfe** "den obersten Teil der Rosenkohlpflanze"
    "lässt sich ähnlich wie Spinat oder Spitzkohl zubereiten"

### 2019
* https://www.peta.de/eisen
* https://www.peta.de/zink
* https://www.peta.de/b12, schönes Video
* Sesam, (gut für Eisen)
* Kürbiskerne, (gut für Zink und Eisen)
* Hirse
* Linsen

### bis 2016
* Süßkartoffeln
* Sprossen ([Trip to India](http://www.healthypowerfood.de/) aus Worms)
* Nüsse (Hasel, Wal, Para, Erd)
* Kichererbsen
* Avocado (weniger, siehe zutatenwatch.md)
* Rote Beete, Spinat
* **Soyananda** (frisch fermentierter Frischkäse) von Soyana - aus der Schweiz: http://www.soyana.ch/
* Soyana Sauerrahm
* **VegiBelle** - "the first real vegan cheese" - http://www.vegibelle.com/ - aus Hamburg: http://www.tofunagel.de/ueber-uns/
  * Empfehlung die Variante "pur" nach Hirtenart
* Apfelkraut (als Aufstrich etc.)
* Fire Roasted Pasta: http://www.ppura.ch/de/la-pasta/fireroasted-pasta
* Blumenkohl, Zuccini, Paprika
* "Dinkel wie Reis"
* Olivenöl mit Zitrone
* Muskat, Pfeffer
* Kidneybohnen
* Ahornsirup
* Rohe Kakaobohnen (von z. B. [Kallari-Futuro GmbH](http://kallari-eg.de/))
* weiße Bohnen
* Weiße Bohnen in Tomatensoße
* [Veggie Life Räuchertofu Premium](http://www.veggielife.de/tofus/)
* lifefood Bio Möhrenkräcker (veganz)

* 2017:
    * TODO: https://de.wikipedia.org/wiki/Aquafaba - "Kochflüssigkeit oder das Einweichwasser von Kichererbsen, Bohnen und anderen Hülsenfrüchten"

Quick-Stuff
-----------
* "Trek"-Riegel (Veganz FFM)
* "Raw-Organic-Foodbar"-Riegel "Active Greens Chocolate", "chocolate coconut" (Veganz FFM)
* Raw Bite - Vanilla Berries

X-Drinks
--------
Hintergrund: "Im Handel in der Europäischen Union darf mit „Milch“ nur Milch von Kühen bezeichnet werden."
(16.06.2014, https://de.wikipedia.org/wiki/Milch)

Bei den X-Drinks steht das X für

* Hafer
* Dinkel
* Soja
* Reis
* Mandel
* Dinkel/Mandel
* Reis/Kokos
* und weitere Kombinationen
* jeweils mit oder ohne Zusatz von
  * Kalzium
  * Zucker (besser ohne)
  * Schoko oder Vanille

etc.

![](img/Natumi-Dinkeldrink-Natur-plus-Calcium-Pflanzendrink-Milchalternative-h200.jpg)
![](img/Natumi-Haferdrink-Natur-Pflanzendrink-Milchalternative-h200.jpg)


Pudding
-------
Siehe bekanntes Pudding-Rezept, aber mit X-Drink statt Milch, wobei X nach Geschmack zu wählen ist.


Heißer Kakao mit X-Drink
------------------------
Analog zum Pudding.


Nährstofftabelle 1
------------------
von [roots of compassion](http://www.rootsofcompassion.org/de/vegane-ernaehrungstabelle)

![](img/roots-of-comp-tabelle.jpg)


Nährstofftabelle 2
-------------------
Quelle: extravegant.de, abgerufen 08.03.2014

![](img/extravegant-tabelle.png)


Süßes/Schoko ohne Kristall-Zucker
---------------------------------
Ziel: leckere Süßigkeiten, rein pflanzlich, ohne Kristallzucker (z. B. wegen der Haut), ohne Sklavenarbeit, möglichst naturnah.

* [Positivliste ohne Sklavenarbeit](http://www.foodispower.org/schokoladenliste/)

Schokostufen:

- Rohe Kakaobohnen, Kakaonibs
- Müsli mit Kakaopulver (ohne Zucker)
- Fertige Schoko, siehe Beispiele

Beispiele:

* **Schoko-Light 120g** von Govinda
  * Link: http://www.govinda-natur.de/shop/Bio-Naturkost/Suesses/Schoko-Light-120g::1114.html
  * Zutaten (2014-12): Datteln, Kakaopulver, Vanille
  * Unternehmen: Govinda Natur GmbH / Dieselstr. 13A / D-67141 Neuhofen
  * Bezugsquellen: Bio-Laden
* **lovechock Rocks, Mandel/Zimt** von lovechock
  * Link: http://www.lovechock.com/de/producten/rocks/mandel-zimt.html / http://www.lovechock.com/de/producten/rocks.html?view=default
  * Zutaten (2014-12): Rohe Kakaomasse, Mandeln (23%), Kakaobutter, getrockneter Kokosblütennektar, getrockneter gekeimte Quinoa, Zimtpulver (1%), Reishi Extrakt (0,5%), Vanillepulver, Meersalz
  * Unternehmen: LOVECHOCK / Asterweg 20 B2 / 1031 HN Amsterdam
  * Bezugsquellen: Bio-Laden
* **Raw 70% Cacao with Spirulina Chocolate Bar** von Pacari
    * Zutaten (2015): Kakaobohnen ungeröstet, Kokosblütenzucker, Kakaobutter, Spirulina 1%, Emulgator: Sonnenblumenlecithine
    * http://www.pacari.com/products/raw-70-chocolate-with-spirulina
    * http://www.pacari.com/pages/about-us

Links
-----
* Benjamin Lupton's vegan page: https://github.com/balupton/plant-vs-animal-products/blob/master/README.md
    * "A definitive comparison of plant vs animal products"
    * Links to introductory films and other ressources
    * [gary yourofsky speech](http://balupton.tumblr.com/post/64768966299/best-speech-you-will-ever-hear-gary-yourofsky)
    * Ethical FAQ: https://github.com/balupton/plant-vs-animal-products#faq
* Kochen lassen: http://www.veganmania.at
* http://albert-schweitzer-stiftung.de

### Schwedische Apfeltorte
eingestellt am 12.01.2014

aus: Vegan backen - mit Liebe, aber ohne Ei (von Nicole Just)
