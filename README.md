daily-choices-info-package
==========================

**Themenauswahl für die täglichen Entscheidungen:**

* [Essen kaufen - Bio](Ernährung/bio-README.md)
* [Essen kaufen - Vegan](Vegan)
* [Essen kaufen - Zuckerprodukthersteller meiden](Ernährung/zucker-README.md)
* [Essen kaufen - Bäckerei-Produkte](Ernährung/bäckerei-README.md)
* [Genussmittel - Kaffee](Ernährung/kaffee-README.md)
* [Genussmittel - Kakao](Ernährung/kakao-README.md)

**Themenauswahl für regelmäßige Entscheidungen:**

* [Computer - Digitaler Konsum](Computer) und [Freie Software](Computer-FreieSoftware)
* [Kleidung kaufen](Kleidung) (inkl. Pelz vermeiden)
* [Urlaub - Flugreisen minimieren](Freizeit/urlaub-README.md)
* [Kosmetik](Konsum/kosmetik-README.md)

**Themenauswahl für spezielle Entscheidungen:**

* [Unterhaltung - Blumen aus Übersee](Konsum/blumen-README.md)
* [Statussymbol - Edelmetall-Schmuck](Konsum/schmuck-README.md)
* [Freizeit - Garten ohne Torf](Freizeit/garten-README.md)
* [Unterhaltung - Haustiere](Freizeit/haustiere-README.md)
* [Unterhaltung - Zoo und Tier-Zirkus?](Freizeit/zoo-README.md)

**Themenauswahl für seltene Entscheidungen:**

* [Finanzen - Geld](Finanzen)
* [Wohnen - Stromanbieter](Energie-Strom)
* [Wohnen - Fläche](Hintergrund/bauen.md)
* [Mobilität - Auto](Mobilität)
* [Haushalt - Waschmaschine](Hintergrund/langlebigkeit.md) (Langlebigkeit)

* Informationssammlung für den [persönlichen Gebrauch](LIZENZ).
