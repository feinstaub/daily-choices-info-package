Tiere - artgerecht
==================

<!-- toc -->

- [Tierrechte für Einsteiger](#tierrechte-fur-einsteiger)
  * [2019: Rechte der Tiere im ZDF](#2019-rechte-der-tiere-im-zdf)
  * [2016: Tierrechte von trufe](#2016-tierrechte-von-trufe)
  * ["Warum lieben wir Hunde und warum essen wir Schweine?"](#warum-lieben-wir-hunde-und-warum-essen-wir-schweine)
  * [Veganismus gleich Humanismus - Lakmustest für Aufklärung](#veganismus-gleich-humanismus---lakmustest-fur-aufklarung)
  * [30 Tage Vegan Selbstexperiment, 2018](#30-tage-vegan-selbstexperiment-2018)
  * [Milchkühe: Das Geschäft mit kranken Kühen, 2019](#milchkuhe-das-geschaft-mit-kranken-kuhen-2019)
- [Was ist artgerecht?](#was-ist-artgerecht)
- [Allgemein](#allgemein)
  * [Videos](#videos)
  * [Tierfutter, Tiernahrung](#tierfutter-tiernahrung)
  * [Bio-Anteil](#bio-anteil)
  * [Aktuelle Zustände 2017](#aktuelle-zustande-2017)
- [Huhn](#huhn)
  * [Positiv-Beipiel](#positiv-beipiel)
  * [Gängige Praxis der Hühnermast](#gangige-praxis-der-huhnermast)
  * [Schnabelkürzen (Amputation)](#schnabelkurzen-amputation)
  * [Schlachten, Rupfen, Flügel abschneiden](#schlachten-rupfen-flugel-abschneiden)
  * [Lebenserwartung](#lebenserwartung)
- [Ente](#ente)
  * [Bio-Enten](#bio-enten)
- [Pute / Haustruthuhn / Truthahn](#pute--haustruthuhn--truthahn)
- [Gans](#gans)
  * [Braten](#braten)
  * [Stopfleber](#stopfleber)
  * [Daunen](#daunen)
- [Schwein](#schwein)
  * [Gängige Praxis: Kastration von Ferkeln (Amputation)](#gangige-praxis-kastration-von-ferkeln-amputation)
  * [Gängige Praxis: Ferkel: Schwanz kupieren / abschneiden (Amputation)](#gangige-praxis-ferkel-schwanz-kupieren--abschneiden-amputation)
  * [Gängige Praxis: Unrentable Ferkel](#gangige-praxis-unrentable-ferkel)
  * [Gängige Praxis: Ohren einklipsen](#gangige-praxis-ohren-einklipsen)
  * [Gängige Praxis: Schlachtung](#gangige-praxis-schlachtung)
- [Rind](#rind)
  * [Gängige Praxis: Milchproduktion: allgemein / Turbozüchtung](#gangige-praxis-milchproduktion-allgemein--turbozuchtung)
  * [Gängige Praxis: Milchproduktion: Trennung Kuh / Kalb](#gangige-praxis-milchproduktion-trennung-kuh--kalb)
  * [Gängige Praxis: Milchproduktion: Kälber / unrentable männlichen Kälber](#gangige-praxis-milchproduktion-kalber--unrentable-mannlichen-kalber)
  * [Gängige Praxis: Weißmast bei Kälbern](#gangige-praxis-weissmast-bei-kalbern)
  * [Gängige Praxis: Tiertransporte ab Deutschland, per LKW und Schiff](#gangige-praxis-tiertransporte-ab-deutschland-per-lkw-und-schiff)
  * [Gängige Praxis: Lebenslange Stallhaltung](#gangige-praxis-lebenslange-stallhaltung)
  * [Gängige Praxis: Enthornung](#gangige-praxis-enthornung)
  * [Gängige Praxis: Weiteres](#gangige-praxis-weiteres)
  * ["Glückliche Kühe - Zufriedener Landwirt"](#gluckliche-kuhe---zufriedener-landwirt)
  * [Forschung: Kuh mit Loch - Würde?](#forschung-kuh-mit-loch---wurde)
  * [Indien: "Heilige" Kühe?](#indien-heilige-kuhe)
- [Meeresbewohner / Fische](#meeresbewohner--fische)
  * [Gängige Praxis: Wale, Delfine etc.](#gangige-praxis-wale-delfine-etc)
  * [im Kommen: Elektrofischen](#im-kommen-elektrofischen)
  * [Austern](#austern)
- [Schaf](#schaf)
  * [Lammfleisch und regionale Schäfer](#lammfleisch-und-regionale-schafer)
  * [Wolle aus Australien](#wolle-aus-australien)
  * [Sonstiges](#sonstiges)
- [Strauß](#strauss)
- [Kaninchen](#kaninchen)
- [Pferd](#pferd)
  * [Betrug mit Hobby-Reit-Pferden](#betrug-mit-hobby-reit-pferden)
  * [Europa](#europa)
  * [Pferderennen, Springreiten](#pferderennen-springreiten)
  * [Dressurreiten, Dressurpferde](#dressurreiten-dressurpferde)
  * [Pferdegestüt](#pferdegestut)
  * [Anderswo: Pferderennen, Rodeos etc.](#anderswo-pferderennen-rodeos-etc)
- [Tiere im Urwald](#tiere-im-urwald)
- [Tiere in Afrika](#tiere-in-afrika)
  * [Nashorn](#nashorn)
  * [Artgerecht zum Vergleich: Löwen 2018](#artgerecht-zum-vergleich-lowen-2018)
- [Frosch](#frosch)
- [Wachtel](#wachtel)
- [Känguruh](#kanguruh)
- [Hund](#hund)
- [Fuchs, Waschbär, Robbe](#fuchs-waschbar-robbe)
- [Ratte, Maus](#ratte-maus)
- [Lebensdauer vs. Lebenserwartung](#lebensdauer-vs-lebenserwartung)
  * [Allgemein](#allgemein-1)
  * [Huhn](#huhn-1)
  * [Schwein](#schwein-1)
- [Unsortiert](#unsortiert)
  * [2018](#2018)
  * [Industrie 2016](#industrie-2016)

<!-- tocstop -->

Tierrechte für Einsteiger
-------------------------
### 2019: Rechte der Tiere im ZDF
* Video: Rechte der Tiere im TV: "Zahlreiche Studien belegen, dass **viele Tiere ein Bewusstsein haben**,
    dass sie leidens- und schmerzfähig sind.
    **Tiere treibt mehr an, als der bloße Fortpflanzungs- und Überlebenstrieb**.", 45 min,
    https://www.zdf.de/dokumentation/3sat-wissenschaftdoku/rechte-der-tiere-102.html
    * ...
    * Unterschied zwischen artgerecht (Winterschlaf in der Natur)
        und tiergerecht (Menschen halten Tiere und hier geht es um das Wohl jedes einzelnen Tieres)
    * ...
    * ...todo...
    * ...

### 2016: Tierrechte von trufe
* Video_: ["Tierrechte einfach erklärt"](https://www.youtube.com/watch?v=tN2ohNST7Eo), trufe, 2016, 9 min, tag:offline
    * ...
    * ganz nett, aber spricht unnötige Tabuthemen an => Alternative benötigt


### "Warum lieben wir Hunde und warum essen wir Schweine?"
* Doku: ["Wie Tiere fühlen"](https://www.youtube.com/watch?v=QAW3b0e7TgY), arte, 2016, 50 min
    * YT-User DrSelbstdenker
    * "Ein beleidigter Hund, eine hilfsbereite Ratte, ein mitfühlender Schimpanse, ein gerechter Wolf, ein verständnisvoller Elefant: Sind Tiere fähig, sich in andere hineinzuversetzen? Haben sie Mitgefühl? Sind sie empathisch, altruistisch, fair und gar gerecht? Neue Erkenntnisse aus der Verhaltensforschung scheinen zu revolutionieren, was man bisher über die Gefühlswelt der Tiere wusste."
    * ...
    * "Warum lieben wir Hunde und warum essen wir Schweine?"
    * ...

* Video: [Hund freut sich über ersten Schnee](https://www.youtube.com/watch?v=hR1JQ5BRtFQ), YT, 2015, 6 sec, tag:offline
* Video: [Krähe fährt Schlitten](https://www.youtube.com/watch?v=ggM1hWrW_Lw), YT, 2012, 1 min, tag:offline

### Veganismus gleich Humanismus - Lakmustest für Aufklärung
* YT: "Veganismus gleich Humanismus - Warum das Thema so wichtig ist", 2016, Cyberphilosoph
    * Ein Gymnasiallehrer, selber irgendwann vegan geworden, reflektiert über seine - oft erschreckend negativen - Erfahrungen, das Thema Tierrechte in seinem hochgebildeten Umfeld zu thematisieren. Er sieht die starken Zusammenhänge zu anderen Diskriminierungssituationen und unterstreicht die Verbindung von Aufklärung und Humanismus.
    * ...
    * Tierrechte gut geeignet, um "auf den Busch klopfen", ob das Gegenüber die Fähigkeit / den Willen zur kritischen Selbstreflektion hat
        * "interessiert mich nicht"? - ...
    * ...
    * Schülern soll ja auch eine Botschaft, **eine Haltung** mitgegeben werden
    * tolles Angebot, Positionen zu beziehen
    * richtige Fragen stellen
    * wogegen soll man eigentlich auf die Straße gehen? / Demos
        * wofür stehe ich ein; seine Stimme zu erheben
    * ...
    * macht sich in der Öffentlichkeit oft zum Narren (wird verspottet),
        weil er aufsteht, zwei Sätze zu Tierrechten sagt und Angebote macht (z. B. einen Film schauen mit Kollegen)
    * die Welt so laufen lassen und hoffen, dass ich es irgendwie noch mit heiler Haut noch bis ins Grab schaffe
    * ...
    * **im Kern geht es nicht um Tierrechte, sondern um Aufklärung**
        * schön auf den Busch klopfen, ist jemand bereit sich auf ein radikales Thema einzulassen

* siehe weitere "Veganismus gleich Humanismus"
* siehe auch "Deeyah Khan"

### 30 Tage Vegan Selbstexperiment, 2018
* ["Die Wahrheit über Veganismus - 30 Tage Vegan Selbstexperiment"](https://www.youtube.com/watch?v=2Qk75XhDFJk), tomatolix, Dez 2018, 50 min
    * "Welches Video ich auch sehr interessant fand (und welches mich überhaupt zum Veganismus gebracht hat" (e-fell, 2021)
    * 1/2 Jahr Recherche (das merkt man)
        * hohe Qualität und "13.238 Kommentare" stand Jan. 2019
    * sehr ausgewogen
        * wenn typische Scheinargumente vorkommen, werden diese kritisch hinterfragt
    * Milch
    * Niko Rittenau
        * sehr gute B12-Erklärung inkl. Zufütterung (auch von Selen) und Jod
            * (siehe auch UGB 2005?: https://www.ugb.de/lebensmittel-im-test/jodprophylaxe-ueberholt-neue-zahlen-alte-empfehlungen/)
    * Rügenwalder
    * Bio-Bauer
        * inkl. Bilder vom Schlachten und Zerlegen
        * 5 % Bio allgemein, 2 % Bio-Fleisch
    * Animal Equality
        * man kann sich einfach nicht vorstellen, dass diese Dinge hier bei uns geschehen, in diesem geregelten Land etc.
    * Kosten
    * Umweltauswirkungen mit gut ausgewählten Details
        * auch differenziert: ab und zu ein Biohuhn kann besser sein als jeden Tag Avocados von Übersee
    * Ethische Aspekte wichtig oder egal, persönliche Entscheidung
    * Schönes Fazit
        * kauft nun quasi fast ganz vegan ein und nur noch unterwegs aus Bequemlichkeit nicht ganz, wegen des mangelnden Angebots

### Milchkühe: Das Geschäft mit kranken Kühen, 2019
    * ["Das Geschäft mit kranken Kühen"](https://www.daserste.de/information/wirtschaft-boerse/plusminus/sendung/kranke-kuehe-100.html), ARD, 2019
        * von https://www.soko-tierschutz.org/
        * ["Erneut Tierschutzfall mit Downer-Kühen"](https://www.elite-magazin.de/news/newsticker/erneut-tierschutzfall-mit-downer-kuehen-11519262.html)
    * siehe auch ["Landwirtschaft einst und heute (1970)"](https://www.youtube.com/watch?v=GZZFCuQUg9M)
        * Kühe mit Metall um den Kopf
        * stehen in Reih und Glied, ohne Stroh
    * ["Verdeckte Aufnahmen: So grausam werden ausgediente Milchkühe behandelt"](https://utopia.de/kuehe-fleisch-milch-soko-tierschutz-video-108275/)
    * Aus Sicht eines Tierheims: "Was die Milch anrichtet – SOKO Tierschutz deckt auf" (siehe dort)


Was ist artgerecht?
-------------------
* siehe Wikipedia: https://de.wikipedia.org/wiki/Artgerechte_Haltung

* So sieht **Artgerechtigkeit** aus (siehe positive Beispiele). Unsere geschichtlich überlieferte Tiernutzung - konventionell und bio - sieht da etwas anders aus (bei der traditionellen Betrachtung von "Artgerechtigkeit bei Nutztieren" werden essentielle Bedürfnisse von Lebewesen ausgeblendet, z. B. **das Bedürfnis weiterzuleben**).
    * Anmerkung zum traditionellen Tiernutzungsverständnis, falls jemand Probleme hat, sich die Negativ-Beispiel-Videos anzusehen: "Wenn es nicht gut ist für Augen und Seele, wie kann es dann gut für den Magen sein?"
    * Die Krone der Schöpfung.

* User-Comments von zeit.de:
    * "Es gibt in Deutschland ausschließlich Läden, die von sich behaupten, Fleischwaren aus artengerechter Haltung anzubieten. Oder haben Sie schon mal einen Laden gesehen, der mit dem Slogan wirbt 'Preiswert, weil nicht artengerecht gehalten'?"
    * "Eines ist schon sehr seltsam: Es treffe heute eigentlich nur noch Menschen, die mir erzählen, dass sie nur gutes Fleisch kaufen. Trotzdem stammen rund 99% Prozent des Fleisches aus industrieller Massentierhaltung. Wo kommen nur angesichts der vielen verantwortungsvollen Konsumenten die Käufer dafür her?"

Allgemein
---------
### Videos
* Video: [Hühner, Fließbänder, Züchtung, Sojaimport Tierfutter, Pestizide, Resteexport Afrika, Schredder, 3sat nano, 2014](https://www.youtube.com/watch?v=wdAP7qHy0xA), 5 min, tag:offline, tag:class2019
    * (etwas sarkastischer Unterton beim Sprecher; aber angesichts der Situation sehr passend)
    * Fließbänder mit vielen Hühnern am Anfang
    * Filet-Wahn
    * "Wie kann das funktionieren? - Kurz... muss das Hühnerleben sein"
    * Züchtung
    * Sojaimport Tierfutter
    * Pestizide
    * Resteexport Afrika
    * Schredder am Ende

* Video_: [Schlachten für Fleisch (Schweine, Hühner), Zahlen, Billiglöhner, Fließbänder](https://www.youtube.com/watch?v=ET-qVB2t1uQ), WDR, 2016, 3 min, tag:class2019?, nicht so klasse
    * ...
    * Fließbänder
    * Schlachten für Fleisch (Schweine, Hühner)
    * Zahlen
    * Billiglöhner
    * ...

### Tierfutter, Tiernahrung
* Video: ["Tierfutter - Gefahr für die Umwelt und die Welternährung?"](http://www.daserste.de/information/wissen-kultur/w-wie-wissen/videos/tierfutter-gefahr-fuer-die-umwelt-und-die-welternaehrung-100.html), ARD, 2014, 6 min, (welternaehrung-100), tag:offline, Viehfutter
    * ...
    * Nicht vergessen: wir Menschen essen dieses Futter indirekt mit
    * ...
    * Tierfutter früher, mit Namen
    * Tierfutter heute
        * kein Name mehr, fettgezüchtet
        * Zahlen: 30 Tage lebt ein Masthuhn
        * Turbofutter für Turbowachstum
        * Zahlen: Tierfutter macht bei der Geflügelmast mehr als die Hälfte der Produktionskosten aus
        * Zusatzstoffe wie aus dem Chemiebaukasten
        * für Dotterfarbe
        * Zahlen: Rinder, Schweine und Hühner brauchen 200.000 Tonnen pro Tag in Deutschland. Weltweit 1.000.000.000 Tonnen pro Jahr.
        * Rezept
            * Abfälle aus der Lebensmittelproduktion, billiger, aber auch problematisch
        * Skandale
    * Zahlen: "Fast die Hälfte der weltweiten Getreideernte landet im Futtertrog."
        * Unterschied, ob wir Menschen Getreide direkt verzehren; oder indirekt als Milch oder Steak
        * Zahlen: Aus 10 Kalorien, die in den Futtermitteln stecken, wird 1 Fleischkalorien.
        * Zahlen: "Es gibt weltweit dreimal so viele Nutztiere wie Menschen."
    * Graphik Treibhausgase Huhn, Schwein, Rind
    * Rinder könnten keine Nahrungskonkurrenten sein; Schweine und Hühner sind immer welche
    * Zahlen: In deutsche Ställe fließen jährlich 4 Millionen Tonnen Soja; meist gentechnisch verändert.
    * Ursache: weltweiter Eier-, Milch- und Fleischhunger

* Hintergründe zu Tierfutter
    * "Womit werden Bio-Tiere gefüttert?"
        * https://www.boelw.de/themen/bio-argumente/biofrage-12/, todo
        * http://www.garant.co.at/index.php?id=258

### Bio-Anteil
* https://www.foodwatch.org/de/informieren/bio-lebensmittel/mehr-zum-thema/zahlen-daten-fakten/
    * Zahlen: "In Deutschland gab es 2015 insgesamt 26.855 Bio-Höfe. Das sind 9,7 Prozent aller Landwirtschaftsbetriebe."
    * Zahlen: "Bio-Produkte machen lediglich 4,8 Prozent des gesamten Lebensmittelumsatzes in Deutschland aus (2015). Den höchsten Bio-Anteil gab es in Dänemark mit 8,4 Prozent Umsatzanteil, gefolgt von der Schweiz mit 7,7 Prozent."
    * Zahlen: "Bei Fleisch ist der Bio-Anteil noch geringer: 2016 lag er bei Geflügel bei 1,4 Prozent, bei Rotfleisch (Schwein, Rind, Lamm, Schaf und Kalb) bei 1,8 Prozent und bei Fleisch- und Wurstwaren sogar nur bei 1,2 Prozent."

### Aktuelle Zustände 2017
* ["Massentierhaltung in Deutschland - "Beim Tierschutz geht es zu langsam voran""](http://www.spiegel.de/wissenschaft/mensch/massentierhaltung-in-deutschland-zwischen-wunsch-und-wirklichkeit-a-1161829.html), 2017
    * "Die Tiermedizinerin Nicole Kemper ist Leiterin des Instituts für Tierhygiene, Tierschutz und Nutztierethologie an der Tierärztlichen Hochschule Hannover"
    * ...
    * Politik
    * "Wenn Sie sich etwas für die Nutztiere in Deutschland wünschen dürften - was wäre das?"
        * "Schön wäre es, wenn endlich alle Betriebe bestehende Standards konsequent einhalten. Und ich wünsche mir, dass Nutztiere mit mehr Respekt betrachtet werden und nicht nur als Fleischproduzenten. Auch die Verbraucher sollten sich klarmachen, dass ihre Wurst mal ein Tier war. Sie sollten nicht nur nach dem Preis einkaufen."

Huhn
----
### Positiv-Beipiel
* https://www.youtube.com/watch?v=9McJxR5nFEY, Gary Yourofsky, 5 min

### Gängige Praxis der Hühnermast
* Video: ["Frontal 21 - Elterntierhaltung bei Masthühnern"](https://www.zdf.de/politik/frontal-21/elterntierhaltung-bei-masthuehnern-100.html), ZDF, 2017, 8 min, tag:offline
    * "Hühner - Elterntierhaltung bei Masthühnern, ZDF frontal21, 2017, 8min (elterntierhaltung-100).mp4"
    * "Masthühner werden noch vor ihrer Geschlechtsreife geschlachtet. Deshalb sorgt ein ausgeklügeltes Zuchtsystem mit sogenannter Eltern- und Großelterntieren für den Nachwuchs. Doch diese Zuchttiere sind gesetzlich schlechter geschützt als ihre Nachkommen."
    * ...
    * Wimex-Gruppe
    * ...

* Video: ["Die moderne Hühner-Landwirtschaft"](https://www.youtube.com/watch?v=0t7i05ji4kk), 3sat, 2014, 6 min
    * Lohmann-Tierzucht
    * ...
    * Zuchtlinien
    * ...
    * Labor
    * ...
    * Demeter-Huhn
    * "Das Huhn, das mit seinen Artgenossen in den Tag hinein lebt und alle paar Tage ein Ei legt, ist eine romantische Vorstellung, die mit der modernen Landwirtschaft nichts mehr zu tun hat."

* Video: ["Zweinutzungshuhn: Viele Eier und trotzdem Fleisch | Unser Land | BR Fernsehen"](https://www.youtube.com/watch?v=YSA3Fm0lo7k), BR, 2017, 7min
    * Kükenschreddern auch oft bei Bio
    * ...
    * (Alternative auch bei Lohmann)
    * Zahlen: "Mindestens 320 Eier legt eine Bio-Sandy-Henne pro Jahr"
    * Hohe Legeleistung
        * Brüder gnadenhalber aufgezogen
    * Alternative: alte Rassen
    * ...
    * Man muss sich an höhere Preise gewöhnen

* Haltungsformen Legehennen
    * Video: ["Legehennen-Haltung in verschiedenen Stallsystemen"](https://www.youtube.com/watch?v=CDxF2jY0RBc), www.stallbesuch.de, 2014, 7 min
        * ...
    * siehe bio-eier.md / "utopia-Zusammenstellung Kaufberatung"

* Video: ["Hähnchenmast: Undercover-Recherche"](https://www.youtube.com/watch?v=jj0scwvzWT0), 2017, aktuelle traurige Aufnahmen, 5 min, tag:class?
    * von AnimalEquality Germany, ruhige und sachliche Sprecherin
    * "99 % aller Masthühner in Deutschland leben in Betrieben mit mehr als 10.000 Tieren"
    * riesige Flächen mit Küken
    * lebendige Entsorgung von kranken und halbtoten Tieren; mit der Schaufel im Schubkarren totschlagen
    * Es werden die Firmennamen gezeigt, zu denen die Anlagen gehören
    * Überzüchtung
    * brutales Umgehen zum Einsammeln vor der Schlachtung
    * siehe auch YT-Comments

* Doku: ["37Grad - Unser täglich Tier"](https://www.youtube.com/watch?v=Z8ytr3Tm6eU), 2014, ZDF, 45 min, tag:class?
    * ...
    * Hühner, Lebendtransport von Elterntieren
    * ...
    * Kundin: "Ich achte eigentich auf gar nichts, außer den Preis"
        * "Die meisten Menschen kaufen so"
    * ...
    * todo

* Schnabelkürzen vielleicht bald ganz verboten?
    * [Niedersachsen - 2017: Schnabelkürzen ab sofort verboten](https://www.agrarheute.com/tier/2017-schnabelkuerzen-ab-sofort-verboten-530274)
    * https://albert-schweitzer-stiftung.de/kampagnen/schnabelkuerzen-beenden
        * "Auch bei einem Ausstieg aus dem Schnabelkürzen würden wir nicht von glücklichen Legehennen sprechen, denn wesentliche Kritikpunkte bleiben u. a. die Überzüchtung der Hennen, das frühe Töten nach nur rund einem Legejahr und das Töten der männlichen Küken. Trotzdem wird der Ausstieg aus dem Schnabelkürzen, richtig umgesetzt, viele der schlimmsten Leiden in der Eierindustrie lindern."
        * "In der Bio-Haltung wird das Schnabelkürzen nicht durchgeführt. Da aber auch dort die Bedingungen oft völlig unzureichend sind und es in der Bio-Haltung besonders schwer ist, den hohen Nähstoffbedarf der überzüchteten Hennen zu decken, sehen Bio-Legehennen nach einem Legejahr häufig besonders schlimm aus (mehrere kahle Stellen oder gar ein weitestgehend fehlendes Gefieder). Dies gilt weitgehend unabhängig von den verschiedenen Bio-Siegeln."
    * https://de.wikipedia.org/wiki/Schnabelk%C3%BCrzen

### Schnabelkürzen (Amputation)
* Video: ["Schnabelkürzen auf quarks und Co"](https://www1.wdr.de/mediathek/video/sendungen/quarks-und-co/video-schnabelkuerzen-100.html), 2013 (bis 2018), 1 min, (Schnabelkürzen-mdb-247572.mp4), tag:class2018
    * "Was passiert wenn viele auf engem Raum zusammen sind. Man wird aggressiv. Das wäre bei uns Menschen nicht anders"
    * "Der Schnabel wird gekürzt. Das ist nicht so wie Fingernägelschneiden. Der Schnabel des Huhns ist durchsetzt von vielen Nerven, ist hochempfindlich."
    * "Brutale Prozedur"
    * Bilder vom Kürzen und von einer Maschine
* ["Protestaktion gegen das qualvolle Schnabelkürzen"](https://www.youtube.com/watch?v=i_uimBe81V0), albert-schweitzer-stiftung, 2010, 4 min

### Schlachten, Rupfen, Flügel abschneiden
* Hühner rupfen
* Beim Anblick der Bilder wie Hühner gerupft werden, stellt sich die Frage nach der Würde des Huhns.
* ["Rupfmaschine"](https://www.youtube.com/watch?v=f0s_KS132yo), 2016, 1 min, Deutschland, tag:offline
    * Trommel mit Plastikstumpen
    * Zwei tote Hühner rein und dann innerhalb von 30 sek fertig. Wirkt auch ein bisschen wie eine Fleischmassage.
* ["Rupfen von Wachteln mit Nassrupfmaschine"](https://www.youtube.com/watch?v=Cbt3MwuJNuw), 2017, D, 3 min, von "Selbstversorger 2.0"
    * Wachtel erst in siedend heißes Wasser und dann in die Maschine
* ["Wildenten rupfen mit der Rupfmaschine und küchenfertig machen"](https://www.youtube.com/watch?v=oPN-xObn2JM), 2016, 11 min, von "Die Selbstversorger Familie"
    * Enten jagen gehen
    * "Das ist meine Rupfapparatur. Da spart man sich unheimlich viel Zeit mit.", 3 - 4 min pro Ente, ansonsten mit der Hand 20 - 30 Minuten.
        * im Internet bestellt (Anmerkung: Selbstversorger mit Internet?!)
    * Ente 45 sek in heißes Wasser tauchen (Anmerkung: als Wildente hatten die vorher auf jeden Fall ein gutes Leben)
    * Die Federn fliegen, der Kopf bauelt schlapp herunter
    * Kopf abschneiden, Flügel abschneiden
    * Der Kater sucht sich die Reste
    * 1 gutes Kilo kommt raus
    * Ente ausnehmen (eklig)
* ["DIY cordless Chicken plucker in action"](https://www.youtube.com/watch?v=J2LjMdJPbHo), 3 min, 2016
* ["Hühner und Enten schlachten, oder von der simplen, ungeschönten Realität. (Video)"](http://www.neulichimgarten.de/blog/dies-und-das/huehner-und-enten-schlachten), 2013
    * "Unser Ziel ist, eine Familie mit Lebensmitteln komplett zu versorgen (jedenfalls in der Theorie). Dazu zaehlt fuer mich auch Fleisch. Nicht taeglich, aber in einer gesunden Menge. Da uns die Moeglichkeiten fehlen, groessere Tiere zu halten, bleiben uns letztendlich nur Kleintiere. Huehner, Enten und Kaninchen. Wir halten diese, um sie zu nutzen. Es sind Nutztiere, die am Ende im Topf oder in der Pfanne landen. Dazu stehe ich."
    * "Mit einer Maschine geht das Rupfen in Sekundenschnelle."
    * ... siehe User-Comments
    * [eingebettetes Video](https://www.youtube.com/watch?v=yoJkKQjLN4A) altersbeschränkt (Abhilfe youtube-dl), 2013, 16 min, von "Der Selbstversorgerkanal", tag:offline
        * "zeigt die simple Realität"
        * Hühner im Dunkeln holen, weil es dann kein Geschrei gibt
        * "im Auto stinkt das bestialisch"
        * Erst die Ente: mit einem Knüppel betäuben. Kehle aufschneiden. Zappelt noch (aber das sei normal sagt man). Kopf ganz ab.
        * In heißes Wasser
        * 6:30: große Rupfmaschine
        * Flügel ab. Füße ab.
        * Speiseröhre rausreißen.
        * ...
        * (Die gute Nachricht: in Zusammenarbeit mit anderen Menschen kann sich der Mensch eine Gesellschaft bauen, in der man so etwas nicht braucht)
* Frage: werden Hühner immer so unansehnlich gerupft?
    * https://www.markt.de/ratgeber/nutztiere/huehner-richtig-schlachten-und-rupfen
        * 1. heißes Wasser, 2. Herausziehen der Federn. Anscheinend also ja.

### Lebenserwartung
* siehe unten

Ente
----
### Bio-Enten
* ["Bioenten aus Oberösterreich ORF Oberösterreich heute"](https://www.youtube.com/watch?v=9PX6sxO5v_Q), 2018, ORF, 3 min, tag:class2019?
    * Bilder von der Wiese
    * Schlachtalter: 7 Wochen, vergleiche Lebensdauer
    * Brüderküken werden auch verwertet: Bilder von Schenkeln
    * Bilder von der Zerlegung

Pute / Haustruthuhn / Truthahn
------------------------------
* Was ist überhaupt eine Pute? --> https://de.wikipedia.org/wiki/Pute
* Positiv-Beipiel: https://www.youtube.com/watch?v=pdjKGTfwZMU (6 min, engl., die ersten 2:30 Minuten reichen für den Truthahn-Eindruck)
    * Belly-Rub-Reaktion und dem Turkey-Talk
* 2017: [Putenklage in Vorbereitung](https://albert-schweitzer-stiftung.de/helfen/spenden-2), 2017
* Das traditionelle Tiernutzungsverständnis: siehe z. B. Vogelgrippe
* Gängige Praxis:
    * massenhaftes Schlachten, um schön zu feiern
    * YT: "IT'S TURKEY KILLING DAY! | Giving Thanks", 2019, people having respect and then kill and mock the animal

Gans
----
* Positiv-Beipiel: todo

### Braten
* Das traditionelle Tiernutzungsverständnis:
    * ["Weihnachtsgans: Genuss aus artgerechter Haltung"](https://www.verbraucher.de/weihnachtsgans-genuss-aus-artgerechter-haltung), Verbraucherzentrale Hessen, 19.12.2016
        * "Im Dezember hat Gänsebraten Hochsaison. Wer zu Weihnachten eine Gans aus artgerechter Tierhaltung servieren möchte, sollte auf das Bio-Siegel oder die Kennzeichnung "aus Freilandhaltung" achten."
        * Der persönliche Genuss steht im Vordergrund. Das Wort "artgerecht" wird traditionell verwendet.
        * "Zwangsmast und Lebendrupf von Gänsen zur Produktion von Stopfleber und Daunenprodukten ist nicht kennzeichnungspflichtig.", siehe Stopfleber

### Stopfleber
* Was ist das? --> siehe http://www.stopfleber.org/
    * Intro-Video: [So wird Stopfleber hergestellt](https://www.youtube.com/watch?v=zkeN6nBuvCI), 3 min, Animal Equality Recherche 2015, tag:class?
        * Aufnahmen der Zwangsfütterung relativ am Anfang
    * interessant auch der Tagebucheintrag der Aktivisten
    * Liste von Ländern, wo das erlaubt ist und wo verboten. Bild des Rohres.
    * Was sagen solche Bilder über die Konsumenten aus, die davon wissen, aber trotzdem weiterkaufen?
    * https://de.wikipedia.org/wiki/Nudeln_(Mast)
        * "Nudeln oder Stopfen ist eine Form der Geflügelmast, bei der Gänse oder Enten „gestopft“ werden, indem ihnen das Futter per Zwangsernährung verabreicht wird."
        * ![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Gavage-cages-collectives-France.jpg/640px-Gavage-cages-collectives-France.jpg) (So etwas wird heute noch ohne Strafen durchgeführt / Würde des Tieres? / Menschlichkeit?)
* https://de.wikipedia.org/wiki/Foie_gras
    * "ist eine kulinarische Spezialität"
    * "2005 wurde Foie gras von der französischen Nationalversammlung in einem Zusatz zum Landwirtschaftsgesetz zum **nationalen und gastronomischen Kulturerbe** erklärt und ist dadurch von französischen Tierschutzgesetzen ausgenommen."
    * "Hauptabnehmer außerhalb Frankreichs ist Spanien mit jährlich 801 Tonnen, an fünfter Stelle steht Deutschland mit 121 Tonnen (2004)"
    * "Rund drei bis viermal pro Tag wird den Tieren mittels eines Rohres ein Futterbrei aus 95 Prozent Mais und 5 Prozent Schweineschmalz in den Magen gepumpt."
    * "Die Produktion ist in vielen Ländern verboten, Import und Verkauf sind aber beispielsweise in der EU zugelassen."
* Weiteres:
    * Stopfleber auf ARTE:
        * https://future.arte.tv/de/stopfleber (Übersicht)
        * http://future.arte.tv/de/stopfleber/zwischen-respekt-und-folter-videos-zur-stopfleberproduktion (Videos)
    * [Artikel zur Stopfleberproduktion](https://albert-schweitzer-stiftung.de/aktuell/stopfleberproduktion), 2017
* Was tun? Aufregen? Ob der sinnlosen Gewalt im eigenen Europa resignieren? Petition unterschreiben?
    * alles gut und zusätzlich **Die Oster-Chance**:
        1. Feststellen - siehe z. B. [jahres-kalender](../Hintergrund/jahres-kalender.md)/Ostern - was wir hierzulande so mit der Begründung "Kulturbewahrung" durchführen.
        2. Sich von den dort vorgeschlagenen Lösungsmöglichkeiten inspirieren lassen. Damit das eigene vegane Osterfest schrittweise (auch über Jahre hinweg) selber umsetzen.
        3. Damit zeigen, dass der Mensch fähig ist, die Teile seiner Kultur und Tradition zu bewahren, auf die er stolz sein kann. Und die Teile auszuphasen, die unmenschlich sind.

### Daunen
* Video: ["Lebendig gerupft: Das Geschäft mit den Gänsedaunen"](https://www.spiegel.de/video/lebendig-gerupft-das-geschaeft-mit-den-gaensedaunen-video-1536369.html), 10 min, Spiegel Magazin 2014, tag:class2018, Mülln
    * "Wissen Sie woher Ihre Daunen kommen?"
        * ...
        * "Warum nicht?" - "Weil ich mir dachte, wenn es was teures ist, dann ist es was ordentliches."
        * "Wollen Sie mal etwas sehen?" - "Nein ich weiß. ... Ich weine jetzt."
        * "Ich kann das nicht kontrollieren."
        * "Ich weiß wie es produziert wurde, aber da möchte ich mir keine Gedanken drüber machen."
    * Bilder vom Lebendrupf
        * Verboten in Deutschland, aber viel kommt aus Polen und Asien
        * Mehrfachrupfung möglich. Dabei wird die Qualität immer besser. Die Deutschen lieben Qualität.
    * Friedrich Mülln von der Soko Tierschutz: Angriff durch Farmer
    * Nur ein deutscher Vertrieb/Hersteller antwortete auf Interviewanfragen. Dieser betreibt den vorgeschriebenen Schlachtrupf.
* Artikel: ["Gänsedaunen: Tierquälerei für kuschelige Winterjacken?"](https://www.swr.de/swr2/wissen/daunenjacken-lebendgerupfte-gaense-und-alternativen/-/id=661224/did=20673850/nid=661224/ck88o1/index.html), SWR2, 2017
    * inkl. **4 min-Audio**
        * wieviele Gänse müssen für eine Jacke geerntet werden?
        * warum werden die Gänse lebend gerupft?
        * in unkontrollierten Umgebungen kommen auch Maschinen zum Einsatz, die eigentlich zum Entfedern von toten Tieren gedacht sind.
            * siehe auch "Hühner rupfen"
    * Bilder
        * "Gänse nach dem Rupfen auf einer Gansfarm in Ungarn"
        * vom Stopflebermästen (Schlauch im Rachen)
    * "Daunenjacken sind im Trend. Darunter leiden Tiere. Gänsen werden die Federn oft lebendig vom Leib gerissen. Was kann man als Verbraucher tun, um den Tieren zu helfen?"
    * "Nur etwa 150 Tonnen Daunen werden jährlich in Deutschland produziert. Viel mehr wird importiert – rund 10.000 Tonnen pro Jahr."
    * "Nach Angaben des Verbandes der Deutschen Daunen- und Federnindustrie kommen rund 70 Prozent davon auf Ostasien, wie etwa auch China. Die restlichen 30 Prozent stammen aus Osteuropa."
    * "In den Exportländern gehört „Lebendrupfen“ oft zum Standard. Zur Herstellung einer Daunenjacke benötigt man etwa die Daunenmenge von zwanzig Tieren."
    * ...
    * Alternativen
        * "verschiedene Kunstfasern wie beispielsweise „Primaloft“"
        * "Alternativen aus der Natur: Kapok, die sogenannte Pflanzendaune, die aus dem Wollbaum gewonnen wird.
        * "Milkweed. Ein Kraut, das hauptsächlich aus Nordamerika kommt."
* Hintergrund:
    * Grundsätzlich gibt es zwei Rupfarten:
        * (1) Totrupfung von Tieren nach der Schlachtung (Schlachtrupf)
            * siehe http://www.tierschutzbund.de/verbrauchertipps-federn-daunen.html
                * "über 90 % des Weltaufkommens von Daunen und Federn durch Schlachtrupf gewonnen", und das heißt wahrscheinlich zum größten Teil aus vorheriger Massentierhaltung
            * "bei vorsichtiger Handhabung kann solch ein Rupfen für die Tiere weitgehend schmerzfrei erfolgen." Allerdings gilt für heute eher das hier: "In der gewerblich bis industriell betriebenen Vogelzucht wird jedoch – u. a. auf Grund teils enormer Bestandsgrößen von hunderttausenden Tieren" darauf keine Rücksicht genommen und die Situation für die Tiere ist entsprechend qualvoll
                * siehe http://www.sueddeutsche.de/panorama/gaense-daunen-und-lebendrupf-das-arme-federvieh-1.1028568
                * http://www.peta.de/daunen (fiese Sache: im eingebetteten Video wird gezeigt, dass die Gänse auch wieder zugenäht werden (ohne Betäubung), wenn bei der Rupfung zuviel Haut abging)
                * http://hugsforhikers.com/blog/2012/01/23/daunen-outdoorbranche/
        * (2) Lebend-Rupfung (ggf. mehrmals pro Leben)
            * siehe https://de.wikipedia.org/wiki/Daune
    * Beispiele von Herstellern, die nicht auf Daunen verzichten wollen, aber sich anstrengen die Situation zu verbessern:
        * Firma "Fjällräven": "Aufgrund unserer vollständig kontrollierten Lieferkette, strikter Kontrollen und Audits haben wir in unserer Branche beim Ranking der Tierschutzorganisation Vier Pfoten den ersten Platz erreicht" (http://www.fjallraven.de/explore-fjallraven/unsere-verantwortung/natur-und-umweltschutz/unser-daunen-versprechen)
        * Bei bergsteiger.de gibt es eine [Übersicht, wie verschiedene bekannte Hersteller mit dem Daunen-Thema umgehen](http://bergsteiger.de/ausruestung/produktpflege-nutzung/gans-im-glueck-daune-und-ihre-herkunft)
        * https://utopia.de/ratgeber/daunen
* Wo sind Daunen üblicherweise drin?
    * Bettwaren wie Kopfkissen und Bettdecke
    * Schlafsäcke
    * Jacken
* Alternativen zu Daunen:
    * Kunstfasern: [Polyamide](https://de.wikipedia.org/wiki/Polyamide)
    *   z. B. ["PrimaLoft®: Die clevere Alternative zu Daune"](https://www.bergzeit.de/magazin/primaloft-die-clevere-alternative-zu-daune/)
    * Kapok, eine "Pflanzendaune", siehe oben
    * Milkweed, siehe oben

Schwein
-------
* Positiv-Beipiele:
    * Deutschland:
        * ["Neues Zuhause für Ferkel Viktoria"](https://www.ndr.de/fernsehen/sendungen/hamburg_journal/Neues-Zuhause-fuer-Ferkel-Viktoria,hamj55170.html), 2017, ndr.de
        * ["Schweine sind schlau"](https://www.ardmediathek.de/ard/player/Y3JpZDovL2Rhc2Vyc3RlL3d3aWV3aXNzZW4vMDUwOTU2MzU5Nw/), 2015, 7 min
            * "Schweine besitzen eine hohe Intelligenz, einen ausgeprägten Spieltrieb und sind reinlich."
            * ähnlich intelligent wie Hunde
            * so wie wir die Tiere als Nutztiere halten, ist demnach ein Skandal
    * Amerika: https://www.youtube.com/watch?v=awNjy7OMG8I, Gary Yourofsky, 6 min
        * unter anderem ein Schwein auf dem Feld
        * happy in mud but NOT in shit
    * siehe Doku: [arte: "Das Tier und wir - Das Schwein"](https://www.arte.tv/de/videos/070357-003-A/animalisch-das-tier-und-wir/), 2018, 26 min
        * todo

### Gängige Praxis: Kastration von Ferkeln (Amputation)
* Video: ["Kastration männlicher Ferkel: Ohne Betäubung - wie lange noch? | Unser Land | BR Fernsehen"](https://www.youtube.com/watch?v=HpTi5z2qrs0), 2016, 5 min, tag:class?
    * erste Minute
        * Bilder vom Kastrationsprozess an kleinen Ferkel-Jungs
        * Grund: Fleisch riecht sonst nicht so angehem (Ebergeruch)
        * "seit 2016 nur noch in den ersten 7 Tagen nach der Geburt erlaubt"
        * "Niemand darf einem Tier ohne vernünftigen Grund Schmerzen, Leiden oder Schaden zufügen“, Tierschutzgesetz §1
        * Studie zeigt, auch in den ersten 7 Tagen fühlen die Ferkel schmerzen (was man auch schon fast an den lauten Schreien der Ferkel ablesen kann)
    * Lösung: Ebermast => stinkendes Fleisch
    * Schlachtbilder
    * Geruch nicht technisch feststellbar. An manchen Schlachthöfen stehen Leute am Band, die "Schnüffler"
        * entsorgen oder mit anderem Fleisch zu Wurst verarbeiten
    * Ebermast: die Schweine gehen sich gegenseitig an die Schniedel
    * Beste Methode erfordert Maschine, Gas, Tierarzt, mehr Arbeitsaufwand; in der Schweiz teilweise eingesetzt, hierzulande als zu teuer empfunden
* Artikel: ["Ferkelkastration – warum der 4te Weg ein Irrweg ist"](http://www.tfvl.de/ferkelkastration-warum-der-4te-weg-ein-irrweg-ist/), 2017
    * "Auf der Suche nach weiteren Alternativen zur Verhinderung des „Schweinegeruchs“ bei Eberfleisch kam zum wiederholten Male der Vorschlag auf, Schweine allein unter örtlicher Betäubung/lokaler Anästhesie zu kastrieren, analog zum Vorgehen bei z. B. Rindern. Sogar in der Humanmedizin finden Sterilisationen, (die Durchtrennung der Samenstränge) unter lokaler Anästhesie statt, warum also nicht auch bei Schweinen, könnte man denken?"
    * ...
    * "Daraus resultiert ein zwangsweise falsches Vorgehen, indem die örtliche Betäubung in den Hoden selbst anstatt in den Samenstrang injiziert wird."
    * "Dadurch ist dieses Vorgehen sehr schmerzhaft und nach guter veterinärmedizinischer Praxis nur unter Vollnarkose durchführbar."
    * "Die richtige Methode [...] kann [...] nur unter guten technischen Bedingungen erbracht werden, nicht jedoch unter Praxisbedingungen im Stall."
    * "Aus unserer Sicht ist die Kastration männlicher Ferkel unter alleiniger, unvollständiger und deshalb schmerzhafter Lokalanästhesie ein Verstoß gegen das Tierschutzgesetz."
* ["Ihr fragt, wir antworten, heute Kastration"](https://www.youtube.com/watch?v=_XSEwusotrg), 2017, 8 min
    * ...

### Gängige Praxis: Ferkel: Schwanz kupieren / abschneiden (Amputation)
* todo
* Video: ["Ihr fragt, wir antworten, heute Schwänze kupieren"](https://www.youtube.com/watch?v=AzRkS6FRLes), Bocholter Landschwein, 2017, 5 min, tag:class?
    * ...
    * eigentlich nicht erlaubt, aber es gibt immer Ausnahmegenehmigungen; ohne Betäubung
    * Fokus darauf, was erlaubt ist. Frage: ist alles, was erlabut ist, ok?
    * ...
    * Erklärungen zum Schwänzebeißen
    * ...
    * insbesondere moderne Zuchtlinien
    * ...
    * ab 4 min: 4 Tage altes Ferkel und **heißer Elektroklinge**, nur ein kurzer Schrei

### Gängige Praxis: Unrentable Ferkel
* Allgemein
    * Täglich tolerierte Gewalt. Siehe "Was ist Toleranz?"
    * Zahlen: "In Deutschland sterben jährlich etwa 13,6 Mio. (13.000.000) Schweine _vor_ ihrer Schlachtung – das entspricht 21 % der lebend geborenen Tiere."
        * https://albert-schweitzer-stiftung.de/aktuell/kontrolle-toter-schweine-belegt-verstoesse, 2017
        * Grundlage: Studie der Tierärztlichen Hochschule (TiHo) Hannover (wird auch in der Doku unten zitiert)
        * "Die verantwortliche in den Betrieben sehen weg"
        * siehe auch Doku unten "Millionen Schweine sterben für den Müll"
    * todo: wieviele Schlachtungen?

* Video: ["Millionen Schweine sterben für den Müll"](https://www.youtube.com/watch?v=iONKeuMZ08E), NDR, 2018, 8 min, tag:offline
    * "Rund 60 Millionen Schweine werden in Deutschland geschlachtet. 13,6 Millionen Tiere aber überleben die Mast erst gar nicht, bzw. müssen vorher notgetötet werden. Das sind rund ein Fünftel aller Schweine, die in Deutschland geboren werde"
    * Sehr schöne Anmoderation
        * schon morgen wieder die Frage: Schweineschnitzel ja oder nein
    * Aktuelle ARIWA-Aufnahmen von Jürgen Foß
    * Politik auf Landesebene wird langsam wach?
    * Ein Betreiber einer **Tierbeseitungsanlage** lässt die Kamera rein
        * Bilder von angelieferten Tieren (Rind), die auf Tierschutzverletzungen untersucht werden
        * Per LKW werden Schweine angeliefert und in eine Stahlwanne gekippt (10 Tonnen pro Ladung)
        * Zahlen: Pro Jahr 50.000 Schweine nur in dieser einen Anlage
        * Schweine werden aus organisatorischen Gründen nicht untersucht
    * Studie der Tierärztlichen Hochschule (TiHo) Hannover mit Bildmaterial in Buchform
        * Autorin vom Ausmaß überrascht
        * Zahlen: Pro Jahr sind rund 300.000 Schweine erheblichen Leiden ausgesetzt, bevor sie entsorgt werden.
        * Zahlen: Pro Jahr werden rund 60 % (730.000) der Schweine werden fehlerhaft notgetötet.
    * Derzeit sind die Kontrollen in den Tierkörperbeseitungsanlagen freiwillig
    * Politik: Bestrafungen müssen möglich sein, damit sich der Zustand verbessern kann
        * Bund verweist auf Länder
    * Fazit: das Leiden findet systematisch statt; es sind keine Ausnahmen
    * ["Das Leiden muss gestoppt werden! - Bundestierärztekammer fordert Kennzeichnung von Falltieren beim Schwein"](http://www.bundestieraerztekammer.de/index_btk_presse_details.php?X=20180321102623), 2018
* Video: [Schweine - Ferkel werden weiterhin qualvoll getötet](https://www.youtube.com/watch?v=42QjjohqjKE), ARD Report Mainz, 2016, 8 min, wenig Änderungen seit 2014, tag:offline
    * "So sieht in Deutschland der derzeit branchenübliche Umgang mit Ferkeln aus, deren medizinische Behandlung und Aufzucht sich finanziell nicht lohnen würde"
    * ...
    * "Es wird intensiv an der Weiterentwicklung der Verfahren gearbeitet" (wie schon seit Jahren)
    * ...

* Video: ["MDR Exakt über das Töten unrentabler Ferkel in Deutschland"](https://www.youtube.com/watch?v=yMhIN2C0qqs), 2017, 7 min, tag:offline
    * ...
    * Schuld wird vom Geschäftsführer auf den einzelnen Mitarbeiter abgewälzt
    * ...
    * ehemalige Mitarbeiterin berichtet
    * Amtstierarzt
    * ...
    * Es müsste eine Video-Überwachung der Mitarbeiter eingeführt werden (in anderen sensiblen Bereichen Gang und Gäbe)
    * ...

### Gängige Praxis: Ohren einklipsen
* todo?

### Gängige Praxis: Schlachtung
* beste Schlachtmethoden (sehr selten):
    * ["Schwein gehabt: Hausschlachtung in Kirchlauter | Zwischen Spessart und Karwendel | BR"](https://www.youtube.com/watch?v=KE8dLocsKwI) (10 min), 22. April 2016
        - Auf dem Frühstückstisch steht "JA"-Milch
        - Schwein: kein schlechtes Leben, bis zum Schluss gibts keinen Stress
        - Besuch aus München
            - Frau sagt: kein schöner Anblick
            - **Bolzenschuss**: interessant die starke emotionale Reaktion zu sehen, obwohl klar ist, was kommt (ihr Herz rase noch)
        - Man merke, das Schwein sei nervös und wisse, dass irgendwas passieren wird
        - Hier sieht man das Ausbluten und Pumpen mit dem Vorderbein.
        - Schwein in der Wanne
        - Frau: lacht und "das wars mit dem Schweinefleisch" (lacht)
        - Q: Problem beim Schlachten?
            - A: Am Anfang schon ein bisschen Gewissen, aber mittlerweile Routine.
            "Und die Menschen woll was zu essen und deswegen brauchen ma e bissel a Fleisch, ne."
        - ab 4 min: Schweinehälftentrennung etc.
* [weniger, aber dafür besseres Fleisch](https://www.arte.tv/de/videos/078329-000-A/frankreich-gesuendere-schweinezucht/), 2017, 3 min
    * Schweinezucht in Frankreich ohne Gentechnik und Antibiotika
    * 10 % teurer
    * süße Schweine
    * Schlachtung nicht gezeigt
* siehe [schlachten.md](schlachten.md)

Rind
----
* Positiv-Beipiel: todo

### Gängige Praxis: Milchproduktion: allgemein / Turbozüchtung
* Was die Konsumenten auch nicht vergessen sollten: auch wenn die Kühe von außen glücklich wirken...
    * Es sind hochgezüchtete Turbokühe, siehe stetiger Anstieg der Milchleistung
        * https://de.wikipedia.org/wiki/Milchleistung
            * Laktationsperiode: ca. 305 Tage
            * Zur Ernährung eines Kalbes reichen 8 kg Milch pro Tag aus (ca. 8 Liter) => 8 * 305 = 2440 Liter / Jahr
            * 2001:
                * USA: fast 10.000 kg / Jahr
                * Indien: 2000 - 5500 kg / Jahr
                * Neuseeland: 4000 kg / Jahr
        * http://www.meine-milch.de/milkipedia/milchleistung
            * 1984: 4600 kg / Jahr
            * 2018: 8000 kg / Jahr
    * Das ständige Trächtigsein und Milchgeben belastet die Kuh so sehr, dass sie viel früher sterben als ohne Nutzung
        * siehe z. B. "Lebenserwartung" unten

* 2016: Doku: ["ARD Reportage: Verheizt für billige Milch - Das Leiden der deutschen Turbokühe"](https://www.youtube.com/watch?v=3opHoo5osII), 30 min, 2016, tag:class?
    * Bauer sagt, er füttert seit der Turbokuhumstellung sogar Palmfett.
    * Tierarzt sagt kritische Worte
    * ...
    * TODO: Leistung der Biokuh?
        - https://albert-schweitzer-stiftung.de/massentierhaltung/milchkuehe/2
            - unnötiges Töten
        - Frage: Wo kaufen Sie Ihre ethisch korrekte Milch bzw. Milchprodukte?

* Video: ["Das Rind als Nutztier"](https://www.br.de/fernsehen/ard-alpha/programmkalender/sendung-1923836.html), BR alpha, 2018, 15 min
    * Zahlen: "In einer leistungs- und gewinnorientierten Landwirtschaft hat ein Rind die Aufgabe, möglichst viel Milch und/oder Fleisch zu liefern. Durch Zucht konnte im Lauf der Jahre die Milchleistung der Kühe um ein Vielfaches gesteigert werden. Eine Hochleistungskuh gibt heute über 10 000 Liter Milch im Jahr. Dabei wird oft vergessen, dass Rinder Lebewesen sind, mit einem Anspruch auf eine artgerechte Haltung und dass eine Kuh nur dann Milch gibt, wenn sie auch jedes Jahr ein Kalb zur Welt bringt. Beträgt das natürliche Alter einer Kuh etwa 20 Jahre, so wird eine moderne Hochleistungskuh heute oft nicht einmal mehr 5 Jahre alt, bis sie „verbraucht“ ist. Glück haben da die Kühe auf einer Alm, die den Sommer über gemütlich auf saftigen Almwiesen grasen."
        * Milchleistung
        * Lebenserwartung
    * 2x weiterführendes Material
        * https://www.br.de/alphalernen/faecher/biologie/rind-nutztier-landwirtschaft-112.html
            * "Fell, Fleisch, Milch: Rinder haben in der Leder- und Nahrungsindustrie eine große Bedeutung. Dabei wird oft vergessen, dass Kühe vor allem eins sind: Lebewesen mit Anspruch auf artgerechte Haltung. Wir porträtieren ein nur scheinbar unscheinbares Nutztier."
            * "Dass Kühe eigentlich nur deshalb Milch geben, um ihre Kälber zu ernähren, interessiert dort, wo Rinder wie Maschinen gehalten werden, kaum jemanden."
            * "**Hornlose Rinder** - ... Dabei werden die Hornknospen verödet, bis zu einem Alter von sechs Wochen findet der schmerzhafte Eingriff oft ohne Betäubung statt."
            * "Einblick in die Besamungstechnik"
            * "Fakten über Abstammung, Züchtung und Rinderrassen"
                * ...
            * "Diese Fragen/Arbeitsaufträge solltest du beantworten/lösen können"
                * ...
                * "Warum ist Milchtrinken für den Menschen gesund?" (argh)
                    * was ist mit den min. 15 % laktoseunverträglichen Menschen?
                * ...
    * Zahlen: 4 Millionen Kühe in Deutschland, die Milch geben müssen
        * z. B. zusammen mit 500 weiteren Kühen in einem modernen Kuhstall in Bayern
    * Almkuh Allgäu
        * "mit 13 Kolleginnen"
        * Wieviele?
            * https://www.welt.de/regionales/bayern/article155314776/Warum-Kuehe-in-die-Sommerfrische-gehen.html
                * "50.000 Jungrinder und Kühe verbringen in Bayern den Sommer auf den Almen", 2016
            * https://de.wikipedia.org/wiki/Alm_(Bergweide)
                * Österreich 1997: "500.000 Stück Almvieh"
                * Schweiz Jahr?: "380.000 Stück Rinder"
            * Summe Deutschland: 50.000 / 4 Mio. => **1,3 %** !!!
            * Summe D+Öst+Schweiz: 930.000 / 4 Mio. => **23 %**
    * Melkkarussell
    * Melken
    * 1:50: Spritze zur Entspannung
        * "wenn i des spritz, dann läuft's"
    * Wellnesscenter im Stall
    * 2:45: "Das Rind als Landschaftspfleger"
        * ...
    * 3:30: Anatomie einer Kuh
        * ...
    * 5:10: "Die Kuh als Milchmaschine"
        * nur einwandfreie Milch darf in die Tanks gelangen
        * "regelmäßig schwanger werden", 1x / Jahr ein Kalb
        * Trennung Kalb/Kuh bereits in den ersten Stunden
        * "Kälberiglus"
        * Mast und Verkauf als Kalbfleisch
            * siehe auch Video zur Weißmast!
        * ...
        * auf der Alm: Kühe geben weniger als 25% der Milchmenge einer Hochleistungskuh
        * 7:55: Wie entsteht eigentlich die Milch im Euter? / Anatomieanimation
        * Alm: Verwendung als Frischmilch und Käse
            * ...
    * 10:15: "Das Rind als Fleischlieferant"
        * liefern uns Menschen auch Fleisch
        * Zahlen: "über 3 Millionen Rinder werden dafür jährlich geschlachtet"
        * 10:30: "So ein Nutztier wird noch weiter ausgeschlachtet"
            * Knochen
                * Kämme und Schmuck
                * Gelatine für Kuchen
                * Rohstoffe für Klebstoffe und Knöpfe oder Brieföffner
                * Keratin (in Schampoos und Cremes enthalten) => veganes Label
            * Haut der Rinder
                * gegerbt und als Leder verarbeitet
                * Sohle: Abschluss feine Lederschuhe
                * Gürtel
    * 11:15: "Rinderzucht und Rassen"
        * unterschiedliche Rassen
        * "Hightech"
        * Zuchtkatalog
        * künstliche Zeugung
        * Samen aus den USA
        * künstliche Besamung mit Plastikhandschuh und langer Metallspritze
    * 13:14: "Rinderhaltung und Umwelt"
        * immer mehr Rinder weltweit
        * Nachteile für die Umwelt
        * Wasserverbrauch
        * Methan = klimaschädlich
        * Gülle
            * Grundwasserverseuchung

* Doku_: ["ORF | Milch um jeden Preis"](https://www.youtube.com/watch?v=4dCe4rq35C4), 2015, 45 min
    * ...
    * Bergbauer gibt der Kuh nur Gras. Vor allem aber nichts, was auch der Mensch essen könnte, um Konkurrenz zu vermeiden.
        * Warum bekommen so viele Kühe heutzutage Getreide? -> "Weil man es sich leisten kann"
    * ...
    * ...?

* 2011: https://www.n-tv.de/panorama/Kuhgreisin-macht-Bauer-stolz-article2547486.html
    * "Milchkühe werden normalerweise nicht sehr alt."

### Gängige Praxis: Milchproduktion: Trennung Kuh / Kalb
* Video: [Kühe - Trennung von Kuh und Kalb](https://www.youtube.com/watch?v=EBvfrbClJXo), Albert Schweitzer Stiftung für unsere Mitwelt, 2015, 2 min, tag:class?

* Video: [Kühe - Milch - Vom Kalb zur Kuh (Trennung, enge Boxen, Verkauf männl. Kälber)](https://www.youtube.com/watch?v=dBQ5WZ1bhu8), Landesbetrieb Landwirtschaft Hessen, 2013, 5 min, tag:class?
    * ...

* ["Trennungsschmerz bei Kuh und Kalb"](http://www.stallbesuch.de/trennungsschmerz-bei-kuh-und-kalb/), 2014, inkl. Video

* Video: ["Alternative Haltung - Kuh und Kalb vereint im Stall"](https://www.br.de/mediathek/video/alternative-haltung-kuh-und-kalb-vereint-im-stall-av:584f8ce23b46790011a22858), BR, 2016, 5 min, tag:offline
    * "Normalerweise wird ein Kalb gleich nach der Geburt von der Mutter getrennt und kommt in ein Einzel-Iglu. Ein Betrieb in Baden-Württemberg stellte diese Art der Haltung nach hohen Kälberverlusten um. Die Kälber bleiben nach der Geburt für drei Tage mit der Mutter zusammen und werden erst danach schrittweise getrennt."
    * Bio-Stall mit 40 Kälbern pro Jahr
        * bis vor 10 Jahren Trennung kurz nach der Geburt
        * große Zweifel, ob das richtig ist
        * Kälberseuche, ziemlich viele Kälber an Durchfall gestorben
        * Lösung: "wir lassen die wieder wie in der Natur bei den Müttern trinken"
        * erste Mahlzeit Milch, aber nicht aus dem Plastikeimer
        * Mutterkühe wissen genau, welches ihr eigenes Kalb ist
        * nach 3 Tagen schrittweise getrennt
    * Zahlen: In Bayern und Baden-Württemberg sterben mehr als 10 % der Kälber.
    * Wissenschaftliche Studien: positive Auswirkungen auf Tierwohl
    * 3:00: Am Anfang haben die Kälber so viel Milch getrunken, dass der Hof fast keine Milch mehr verkaufen konnte.
        * Lösung: phasenweise Trennung; 2x am Tag ca. 1 h Kuh und Kalb zusammen
    * Wie schafft man es nach 1 h wieder die Trennung durchzuführen?
        * kein Problem, weil Instinkt ausgelebt wurde
    * Problem: pro Kuh fehlen 400 Liter Milch
        * wirtschaftlich am Anfang schwierig; mittlerweile gut zu schaffen
    * einige Höfe in Bayern und Baden-Württemberg machen das nach

### Gängige Praxis: Milchproduktion: Kälber / unrentable männlichen Kälber
* Doku: ["Armes Kalb: Abfallprodukt der Milchindustrie?"](http://www.ardmediathek.de/tv/45-Min/Armes-Kalb-Abfallprodukt-der-Milchindus/NDR-Fernsehen/Video?bcastId=12772246&documentId=46804230), NDR 45min, 2017, 44 min
    * (nicht vergessen: der Mensch braucht nach der Stillzeit keine Milch mehr und auch kein Kalbfleisch, um gut zu leben)
    * ...
    * "Was passiert mit den Kälbern in Deutschland?"
    * ...
    * "Wenn ich kein Kalb habe, habe ich keine Milch"
    * ...
    * Kälber bringen beim Verkauf immer weniger Geld ein
    * Wohin die männlichen Kälber nach dem Verkauf kommen und wie es ihnen dort geht, weiß der Milchviehwirt leider nicht
    * ...
    * Zahlen: Schätzung: 4 Mio Kälbergeburten pro Jahr
    * Kälber sind anfällig; Kälber bringen weniger Geld als die Anfahrt des Tierarztes => weniger Tierarztbesuche
    * ...
    * ehemalige Mitarbeiterin berichtet
        * Chef ließ Bullenkälber gezielt eingehen, weil sie keine Milch bringen und im Verkauf kein Geld
        * Stöhnen der Kälber
    * Bullenkälber verdursten
    * ...
    * Insider: keine Einzelfälle
    * ...
    * Wiener Schnitzel = bekanntestes Kalbfleischgericht
    * Es gibt helles und dunkles Kalbfleisch
        * helles = stärker nachgefragt
            * Die Kälber bekommen lebenslang Milch; von der Geburt bis zur Schlachtung.
            * Diese Fütterung ist als nicht artgerecht umstritten.
            * 80 % der deutschen Kälber kommen in die sogenannte **Weißmast**.
        * dunkles = Rosefleisch = Verbraucher skeptisch
            * mehr Grünfutter
            * viel Eisen => rote Farbe
    * 17:45: einer der größten Produzenten von Kalbfleisch lehnt Interviewanfragen ab
        * andere große Hersteller auch
    * 18:00: unabhängiger Rosefleischzüchter, entwöhnt die Tiere schrittweise von der Milch, wie in der Natur
        * die Nichtentwöhnung grenze an Tierquälerei
        * sehr unnatürlich dem Kalb die ganze Zeit Milch geben zu wollen; das könne die Mutterkuh auch gar nicht leisten
        * natürliche Aufzucht; muss aber auch davon leben
    * 20:00: Werbevideo eines Verbandes mit lebenslanger Milchfütterung
        * Man sieht Tränken voller Milch.
    * ...
    * 26:00...: Rauhfutter: Eisen wichtig für Tiergesundheit
    * ...
    * 29:00: Weißgemästete Kälber bekommen **Milchaustauscher** (keine echte Kuhmilch):
        * wenig Eisen, Palmölfett, Eiweiß, Magermilch oder Molkepulver
        * 30:25: Bilder von Eimern mit Milch, wo das Kalb draus trinkt
    * 30:45: Schweizer machen das nicht so
        * Tiere gesünder
        * 31:40: Sezierung von Kälbermägen
        * **Geschwüre in Mägen**, eine Qual für das Tier, Schmerzen, Brennen
        * Gründe: Stressfaktoren
            * Geburt/Trennung von der Mutter
            * Haltungsbedingungen
        * 90 % der Kälber haben solche Geschwüre in Europa
            * Schweiz: 10-20%
        * Lösung: gesünderes, eisenhaltiges Futter
    * 34:45: muss das sein? - "wir müssen ja davon leben"
        * kleine Portion Rauhfutter, weil gesetzlich vorgeschrieben
    * 35:25: 6 Monate alte Kälber; in die Futterrinne wird Milch eingelassen
        * alles gesetzlich ok
        * kein Himmel, keine Weide
        * "nein, die dürfen nicht raus; die bleiben hier" (was eine Frage)
            * man versucht es ihnen angenehm zu machen
            * wenn man es anders machen würde, wäre es teurer; das Fleisch würde teurer und ließe sich nicht verkaufen

* todo: siehe offline-videos

* Video: ["Die Ramschkälber | Panorama - die Reporter | NDR"](https://www.youtube.com/watch?v=Zi3p9jjtWhc), 2016, 30 min, tag:offline
    * "Über die Geburt weiblicher Kälber freuen sich Landwirte, denn ihre Milch soll später das Einkommen sichern. Doch was passiert mit den männlichen Kälbern, die quasi wertlos sind? Denn als Milchrassetiere setzen sie kaum Fleisch an und so werden sie nach wenigen Wochen meist mit Verlust verkauft. Panorama - die Reporter hat sich auf die Spur der ungeliebten Kälber begeben."
    * ...
    * Job: Kälber im Umkreis einsammeln
    * 7:00 Bilder einer "Luxusunterkunft" für die Mast
    * ...
    * ...
    * [NDR-Artikel dazu](https://www.ndr.de/fernsehen/sendungen/panorama_die_reporter/Die-Ramschkaelber,sendung514264.html), 2016
        * "Männliche Kälber rechnen sich nicht"
        * "Schweigen aus Furcht vor Imageschaden?"
        * "Die ersten sieben Tage müssen nicht nachvollziehbar sein"
        * ["Stellungnahme zur Versorgung von Bullenkälbern der Milchviehrassen"](https://www.bundestieraerztekammer.de/presse/archiv/5/2015/stellungnahme-zur-versorgung-von-bullenkaelbern-der-milchviehras/1187??pid=0), **Bundestierärztekammer e.V. 2015**
            * "Die Bundestierärztekammer weist darauf hin, dass sowohl die systematische Vernachlässigung als auch das Töten ohne vernünftigen Grund - **und wirtschaftliche Ineffizienz ist kein vernünftiger Grund** - nicht nur unmoralisch ist, sondern auch einen Straftatbestand darstellt."
    * siehe auch
        * ["Leidvoll: Das Schlachten trächtiger Kühe"](https://www.ndr.de/nachrichten/Leidvoll-Das-Schlachten-traechtiger-Kuehe,kaelber113.html), 2014
            * "Auch trächtige Milchkühe werden geschlachtet, wenn sie ihre Leistung nicht mehr bringen. Die ungeborenen Kälber sterben qualvoll mit - im Abfall des Schlachthofes."
            * Zahlen: "In Deutschland werden einer Schätzung der Bundestierärztekammer zufolge jährlich bis zu 180.000 trächtige Kühe in Schlachthöfen geschlachtet. Dabei sterben auch die Kälber im Mutterleib."
            * ...

### Gängige Praxis: Weißmast bei Kälbern
* siehe auch anderes Video hier
* ["Die dunkle Seite von weißem Kalbfleisch"](https://www.ndr.de/ratgeber/verbraucher/Was-ist-schlimm-an-weissem-Kalbfleisch,kalbfleisch110.html), 2017
    * "Gesetzlich ist die Weiß- oder auch Milch-Mast erlaubt. Aber unter vielen Experten gilt sie als nicht artgerecht. Das sieht auch die Schweizer Tierärztin Corinne Bähler so: "Ich bin der Meinung, dass eine tier- oder kälbergerechte Ernährung Eisen enthalten muss. Denn Eisen ist eine sehr wichtige Substanz, um das Immunsystem zu optimieren. Dann können die Kälber möglichst selbst  gegen Krankheiten kämpfen und müssen nicht mit mehreren Medikamenten behandelt werden.""
    * "Folgt man dem Ergebnis der österreichischen Studie, ist davon auszugehen, dass die Kälber in der Weißmast leiden - ganz legal. Eine größere Rolle scheinen aber das Wohlergehen und die kulinarischen Vorlieben der Verbraucher zu spielen."
* ["NDR hält Milch-Mast von Kälbern für Tierquälerei"](https://www.topagrar.com/news/Home-top-News-NDR-haelt-Milch-Mast-von-Kaelbern-fuer-Tierquaelerei-8762097.html), 2017
    * User-Comment: "Wenn Konzerne schon mauern wegen Drehgenehmigung, leichter fader Beigeschmack"
    * Interessant: Top-Agrar berichtete im Jahr 2011 noch im Sinne von NDR: ["Vollmilch – nur mit Ergänzer?"](https://www.topagrar.com/archiv/Vollmilch-nur-mit-Ergaenzer-535896.html)
        * "Fazit: Wer Vollmilch füttert, muss auch einen Aufwerter einsetzen, denn die Eisen- und Vitamingehalte in der Milch sind zu gering. Ohne deren Ergänzung sind die Kälber krankheitsanfälliger und haben ein verzögertes Wachstum."

### Gängige Praxis: Tiertransporte ab Deutschland, per LKW und Schiff
* Video: ["Rindertransport per Schiff: Tierschutz über Bord?"](http://www.ndr.de/fernsehen/sendungen/panorama3/Rinderschiffstransporte-Tierschutz-ueber-Bord,schiffstransport108.html), ndr.de, 18.04.2017, 7 min, (drei2476), tag:offline
    * "Rinder aus Niedersachsen werden nicht nur mit dem Lkw, sondern auch per Schiff exportiert. NDR und SZ liegen Informationen über Tierschutzprobleme von Seetransporten vor."
    * "Hier werden Rinder von Lastwagen auf ein Schiff umgeladen, unter ihnen sind auch Kühe aus Niedersachsen. Was vermutlich kaum bekannt ist, Rinder werden auch per Schiff exportiert wie in diesem Fall nach Nordafrika in die ägyptische Hafenstadt Alexandria."
    * "Einige der rund 1.700 Rinder sind krank, atmen schwer. Putnik bemängelt, dass an Bord die passenden Medikamente gefehlt hätten, um sie zu behandeln. Neun Tiere sterben."
    * deutsche Zuchtrinderexporte
    * Milch-Kühe werden auf der Auktion teilweise grob präsentiert
    * ...
    * "Wir fahren ja auch nach Italien mit dem Bus." / Den Kühen gehts schon gut.
    * ...
    * Kroatische Hafenstadt: Tiere werden an Bord getrieben
    * ...
    * Tote Tiere werden direkt ins Meer entsorgt!
    * ...
    * Schwimmen im eigenen Kot
        * Atemwegserkrankungen
        * keine Medikamente parat
        * Neue Tiere sterben an Bord
    * Beispiel von langen Routen: Wittmund -> Aurich -> Budapest(Ungarn), Besamung -> Kroatien -> Alexandria(Ägypten)
    * ...

* Video: ["Gequält und eingepfercht mit amtlicher Genehmigung"](https://www.rbb-online.de/kontraste/archiv/kontraste-vom-24-05-2018/tiertransporte-ins-ausland-gequaelt-und-eingepfercht-mit-amtlicher-genehmigung.html), ARD Kontraste, 2018, 10 min
    * Tiere werden gequält; verdursten etc.
    * rechtlich unhaltbare Zustände, wenn es außerhalb der EU geht
        * Qual und Schächtung
    * ...
    * Kameras sind nicht erwünscht
    * ...
    * Veterinärämter können ihrer Pflicht nicht nachkommen
        * anonyme Mitarbeiterin berichtet
    * ...

* Video: ["Geheimsache Tiertransporte, ZDF 37grad"](https://www.zdf.de/dokumentation/37-grad/37-geheimsache-tiertransporte-100.html), 2018, 44min (gttz37g-100), Achtung Bilder von europ. Tieren außerhalb von Europa.mp4", tag:offline
    * ...
    * Beispiele von langen LKW-Schlangen in großer Sommerhitze (40 Grad) an türkischer Grenze, langsame Abfertigung
    * einige Fuhrunternehmer setzen gute Technik ein, aber selbst das bringt nicht viel
    * ...
    * Video-Aufnahmen von Tieren (z. B. Kühe, Schafe), die zusammengepfercht im Laderaum stehen, die an "concentration camp trucks" erinnern
    * ...
    * Export von trächtigen Kühen aus Deutschland zur Milchproduktion in der Türkei
    * Es gibt sogar eine EU-Broschüre als Hilfestellung für Leute, die Tiere in die Türkei transportieren möchten
        * Man muss mit allem rechnen und daher extra Futter und Wasser mitnehmen
        * Situation seit Jahren unverändert schlecht
    * ...
    * "Kein Gesetz kann sie dort noch schützen" (selbst mit Gesetz gibt es untragbare Zustände)
    * ...
    * Amtstierärztin aus Kempten gibt ihre Unterschrift zu einem überlangen Transport nicht und es kommt zum Gerichtsverfahren
        * Urteil: die EU-Vorschriften zum Tierschutz gelten für den gesamten Transport, auch wenn große Teile und das Ziel in Nicht-EU-Gebieten liegen. Entscheidend ist, wo der Transport beginnt.
    * ...
    * Schlimme Bilder von 1994, 1996, 2005 über das Entladen von Tieren von Schiffen in Drittländern (an einem Bein aufgehängt, gebrochene Beine, solange lebendig noch verkaufsfähig
        * damals Export von Rindern gefördert von der EU
        * jedesmal wird gesagt: Einzelfälle
        * auch 2017 wieder dokumentiert
    * ...
    * Libanon auch gerne mal: Ausstechen der Augen (mit Video)
    * ...
    * ...

### Gängige Praxis: Lebenslange Stallhaltung
* Video_: ["Die Wahrheit hinter Parmesan und Grana Padano"](https://www.youtube.com/watch?v=aVD3u-5IBlA), Albert Schweitzer Stiftung für unsere Mitwelt, 2017, 3 min
    * ...

### Gängige Praxis: Enthornung
* https://de.wikipedia.org/wiki/Enthornung
    * "Unter Enthornung versteht man das Zerstören von Hornanlagen bei horntragenden Tieren, z. B. Rindern, Schafen und Ziegen, in der Vieh haltenden Landwirtschaft."
    * "Die Enthornung wird im Wesentlichen bei Rindern durchgeführt."
    * Zahlen: "Auch in ca. 70 % der Bio-Betriebe wird die Enthornung praktiziert." (gelesen 2018)
        * "Der Bio-Verband Demeter verbietet die Enthornung."
    * Gründe: wirtschaftliche
    * "Laut deutschem Tierschutzgesetz (TierSchG) sind Eingriffe an Tieren ohne Betäubung nicht zulässig. Abweichend davon dürfen Kälber bis zum Alter von sechs Wochen ohne Betäubung enthornt werden." (2018)
* Video: ["Kälber enthornen | landwirt.com"](https://www.youtube.com/watch?v=CxtMzmzX_og), 2016, 4 min, zeigt fachgerechte Enthornung
    * "Rinder ohne Hörner brauchen weniger Platz und sind eine geringere Gefahr für den Landwirt. Trotzdem ist das Enthornen ein schmerzhafter Eingriff. Wir zeigen euch, wie man die Hornanlagen des Kalbes schonend verödet."
    * Aufnahmen vom Veröden mit Heizstab; nur drehen, nicht rühren!
    * Homöopathische Behandlung (siehe dort)
    * Bessere Alternative (wurde nicht erwähnt): das Horn bleibt dran und das Tier bekommt mehr Platz; oder wird gar nicht erst gehalten
* Video: ["agriKULTUR: David - Pebbles, Enthornung! Wie und Warum?"](https://www.youtube.com/watch?v=qJWN7tad-dQ), 2015, 4 min
    * "Ein wichtiges Ereignis findet statt, Pebbles Hornanlagen werden heute verödet. Wie und warum wir das machen seht ihr im Video."
    * Man sieht einen typischen Stall mit Spaltenböden
    * Die Prozedur wird schneller durchgeführt als beim vorigen Video. Es wird mindestens eine Spritze gegeben. Gleicher Heizstab
    * Genannter Grund: "um uns selber zu schützen, werden die Hörner abgenommen"
    * Bewertung:
        * Vorbildlich, so ein Video zu drehen
        * Man sieht, die beteiligten Personen fehlt das Wissen und/oder die Vermarktungsfägigkeit, das ganze auch ohne Enthornung zu gestalten
        * Also: der Konsument ist gefragt (Demeter-Milch oder Pflanzenmilch wählen), damit sich die Situation verbessern kann.
            * Erst danach wird Politik und Wirtschaft reagieren können
* Video: ["Damit es nicht weh tut! Betäubung bei der Enthornung | Unser Land | BR"](https://www.youtube.com/watch?v=36ZPD6q9LQg), 2015
    * siehe user-comments
    * Bilder von typischen Einzelkälberboxen
    * ohne Kritik
* https://de.wikipedia.org/wiki/Hornlosigkeit
    * "die wegen einer genetischen Veranlagung keine Hörner besitzen"
    * "Das absichtliche Entfernen von Hornanlagen wird Enthornung genannt."

### Gängige Praxis: Weiteres
* siehe "Auch Bio-Milch ist nicht toll fürs Tier" auf der vorigen Seite
* Aussage: "Toll, diese Melk-Roboter. Die machen das so gut. Super für die Kuh"
    * Vergleiche: Erfahrung eigenes Milch-Abpumpen. Ist das toll? Warum soll es bei der Kuh anders sein?

### "Glückliche Kühe - Zufriedener Landwirt"
* 2017: ["Landwirt des Jahres 2016"](https://www.agrarheute.com/management/landwirt-jahres-2016-hat-fuer-michael-doerr-veraendert-533942)
    * Ein Vorbild auf konventioneller Erzeugerseite.
    * Jetzt müssten nur noch die Konsumenten nachziehen und nichts mehr kaufen, was nicht mindestens so gut ist (was zu einer drastischen Nachfragereduktion führen würde). Das hilft den Landwirten und den Kühen.

### Forschung: Kuh mit Loch - Würde?
- Video: ["Kühe für die Forschung - fistulierte Kühe an der Uni Hohenheim"](https://www.youtube.com/watch?v=nS3jLXTe2jI), Produktion der Uni Hohenheim, 2012, 2 min, tag:class?
    - "Kühe für die Forschung. Der HFT (Hohenheimer Futtermitteltest) ist vor allem in Entwicklungsländern
    ein wichtiger Faktor um die Milchleistung von Kühen zu erhöhen. Für die Kühe schmerzfrei
    kann dieser Test durchgeführt werden. Die Futterprobenm werden einfach durch eine Fistel entnommen
    (einem operierten Loch im Pansenmagen)"
        - Warum brauchen Entwicklungsländer mehr Milchleistung? Sollen unsere Turbokühe ein Vorbild sein?
    - User-Comment: "Das ist abartig. Und nennt sich Forschung. Im Sinne der Profitmaximierung."
    - Ein User-Kommentar unter einem Uni-Hohenheim-Video ging so:
        "Was regen sich die Leute so auf, das sind doch nur ein paar wenige Tiere, die man so behandelt".
    - "Es gibt Schlimmeres". Ja, gibt es immer.
    - Würde der Kuh?
        * Artikel: ["Die Würde des Tieres ist unantastbar, oder?"](https://www.heise.de/tp/features/Die-Wuerde-des-Tieres-ist-unantastbar-oder-3362246.html), 2013
            * "Heute gilt es als übertrieben, die stete Rücksichtnahme auf alles Lebendige bis zu seinen niedersten Erscheinungen herab als Forderung einer vernunftgemäßen Ethik auszugeben. Es kommt aber die Zeit, wo man staunen wird, dass die Menschheit so lange brauchte, um gedankenlose Schädigung von Leben als mit Ethik unvereinbar einzusehen. Ethik ist ins Grenzenlose erweiterte Verantwortung gegen alles, was lebt." - Albert Schweitzer
            * ... todo ...
        * Buch: ["Die Würde des Tieres ist unantastbar: Eine neue christliche Tierethik"](https://www.amazon.de/Die-W%C3%BCrde-Tieres-ist-unantastbar/dp/3766622331), 2016
            * [Christen für Tiere](http://www.christen-fuer-tiere.de/die-wuerde-des-tieres-ist-unantastbar-interview-verlosung)
            * Video: ["Neue Tierethik: „Die Würde des Tieres ist unantastbar“"](https://www.youtube.com/watch?v=G_FCYrkABNM), 2016, 9 min, ORF Orientierung
                * zeigt anschaulich, dass Tiere Gefühle haben
                * todo
        * Artikel: ["Die Würde des Tieres bleibt antastbar"](http://www.spiegel.de/politik/deutschland/grundgesetz-die-wuerde-des-tieres-bleibt-antastbar-a-72806.html), 2000
            * "Die Aufnahme des Tierschutzes ins Grundgesetz ist erneut im Bundestag gescheitert. Für die notwendige Zwei-Drittel-Mehrheit fehlten die Stimmen der Union. Die Begründung der Union für ihren Widerstand: Eine Verfassungsänderung brächte doch sowieso nichts."
        * Wert und Würde
- [Teil einer SWR-Doku 2018](http://www.ardmediathek.de/tv/nat%C3%BCrlich/Nat%C3%BCrlich-vom-16-Januar-2018/SWR-Fernsehen/Video?bcastId=1026394&documentId=49191918), (Natürlich vom 16. Januar 2018-49191918.mp4), 30 min, tag:class?
    - ab 16:45 min (Ställe etc.) und ab 21:35 min (das Loch beginnt), 24:00 min ("Ist der Versuch gerechtfertigt?")
    - Gute Fragen von der Moderatorin (24:20 min "Warum versucht man da immer noch mehr aus der Kuh rauszupressen?")
        - Antwort... "ökonomische Rahmenbedingungen", "Nachfrageverhalten auf Verbraucherseite", "Leistungsniveau", findet die Entwicklung selber nicht richtig
        - bis 25:10min
- ["Uni Hohenheim bekennt sich zu Tierversuchen: 6000 Versuchstiere für Forschung und Lehre"](https://www.stuttgarter-nachrichten.de/inhalt.uni-hohenheim-bekennt-sich-zu-tierversuchen-6000-versuchstiere-fuer-forschung-und-lehre.d1de8ed9-2bde-480e-aee8-b8f4cee83433.html), 2017
    - "„Wir wollen Tierversuche nur dann machen, wenn sie unerlässlich sind“, so Dabbert
        also nur dann, wenn ihre Wissenschaftler den Versuch für den Erkenntnisgewinn für unerlässlich halten."
        - Ist Steigerung der Milchleistung unerlässlich?
    - eigene Transparenz-Initiative: https://www.uni-hohenheim.de/tierversuche
- ["Kuh mit Schraubverschluss: Uni Hohenheim legt Details zu Tierversuchen offen"](https://www.rnz.de/politik/suedwest_artikel,-Suedwest-Kuh-mit-Schraubverschluss-Uni-Hohenheim-legt-Details-zu-Tierversuchen-offen-_arid,285865.html), 2017
- ["Uni Hohenheim: Aufschraubbare Kuh dient der Forschung"](https://www.stuttgarter-zeitung.de/inhalt.uni-hohenheim-aufschraubbare-kuh-dient-der-forschung.b3a01d5c-1d58-4940-aa23-d4a4437bed5a.html), 2013

### Indien: "Heilige" Kühe?
* Frage: Ist die heilige Kuh in Indien nur noch ein moderner Mythos?

* ["Indiens Kühe - heilig und gequält"](https://www.netap.ch/de/aktivitaeten/nutztiere/indien-kuehe/282-indiens-kuehe-heilig-und-gequaelt), Jahr?
    * ...
    * "Plastik ist allgegenwärtig. Futter ist rar. Die Tiere fressen das ungeniessbare Zeug oft unbewusst und gehen irgendwann qualvoll daran ein."
    * ...

* https://www.animalequality.de/neuigkeiten/verehrt-und-gequaelt-kuehe-in-indien, 2018
    * Ausbeutung für Milch und Leder
    * "Verehrt und gequält: Eine Undercover-Recherche von Animal Equality in mehr als 100 Betrieben der indischen Milchindustrie bringt unvorstellbares Tierleid ans Tageslicht."
    * mit Video, 12 min
        * Indien größter Konsument von Milchprodukten
        * Künstliche Befruchtung oft ohne sterile Handschuhe mit bloßen Händen
        * schlimme Zustände
        * Bullenkälber: sich selbst überlassen oder geschlachtet
        * Oxitozin-Spritzen beim Beschleunigen des Melkvorgangs
            * eigentlich von der Regierung verboten
        * Unfachgerechte Bedienung der Melkmaschinen: werden teilweise laufen gelassen, obwohl das Euter leer ist
        * auch bewusste Quälerei und Brutalität
        * Tierexperimente mit Loch
        * krude, veraltete Geburtshelfermethoden mit einer Holzlatte
        * keine medizische Versorgung, weil zu teuer
        * freilaufende Kühe ernähren sich auch von Müll
        * Milchkühe werden hier 4 - 5 Jahre alt (statt über 15)
        * "Man kann sich keine Gefühlsduselei leisten, wenn man einen Milchbetrieb leitet"
        * "Die Leute denken, Milch sei ein vegetarisches Produkt"
        * Tiertransporte, brutal(st)
        * krude Betäubungsmethoden vor der Schlachtung, z. B. mit Hammerschlägen
        * Kehle aufschneiden bei vollem Bewusstsein bei Halal-Schlachtung
        * indisches Tierschutzgesetz wird regelmäßig verletzt
        * "Rund 90 % des Leders aus Indien wird in die EU verkauft. Der zweitwichtigste Absatzmarkt ist Deutschland"
        * Schlechte Bedingungen für Arbeiter; oft auch illegal minderjährig
        * Umweltverschmutzung
    * todo: Yoga-Leute fragen, was dazu zu sagen ist
        * https://www.justlovefestival.org/taste
            * "On a global scale, becoming vegan is better for the environment, since it saves water and protects the rainforest and wildlife too. So, no matter how you look at it, it just makes so much sense and offers a win-win-win solution for all."

* ["Die heiligen Kühe Indiens – Mythos und Realität"](http://indienheute.de/die-heiligen-kuhe-indiens-mythos-und-realitat/), 2014, Franz Zang
    * "Wie heilig ist also die heilige Kuh in Indien?"
    * "Die heiligen Kühe Indiens: Sind in Indien Kühe wirklich heilig?"
    * "Die heiligen Kühe Indiens: Opfer des Hungers nach Milchprodukten und Protein"
    * "In der Tat, 4 Mio. Tonnen Rindfleisch, die jedes Jahr in Indien erzeugt werden, müssen ja irgendwo herkommen (Zum Vergleich: Die Rindfleischproduktion der gesamten EU ist Im Vergleich dazu etwa doppelt so groß.)"
    * "Der Anstieg der Rindfleischproduktion Indiens in den letzten Jahren hat einen einfachen Grund: Die Nachfrage nach Milchprodukten steigt in Indien stark an – in den letzten Jahren jährlich um etwa 6 Mio. Tonnen -, und mit der Zahl der Milchkühe steigt auch die Zahl der männlichen Kälber, die für die Schlachtung zur Verfügung stehen."

* https://de.wikipedia.org/w/index.php?title=Heilige_Kuh

Meeresbewohner / Fische
-----------------------
* Positiv-Beipiel: im Meer schwimmen lassen

### Gängige Praxis: Wale, Delfine etc.
* An der Art wie die Menschen mit Walen, Delfinen und sonstigen Meeresbewohnern umgehen, kann man schon einiges ablesen:
    * Video: ["ARD Brisant - Sea Shepherd Extended"](https://www.youtube.com/watch?v=A7As2Ehh35c), 2014, 11 min, tag:class2018
        * Grindwalschlachten
        * Tradition vs. Tierschutz?
        * 5 min: bereit sein Leben...
        * Redakteur Mare-Zeitschrift
    * Video: ["Deutsche Welle: Färöer Inseln: Blutige Tradition | Fokus Europa"](https://www.youtube.com/watch?v=-VEIyPCv6-4), 2015, 5 min, etwas zu kurz, tag:class2018
        * Grindwalschlachten
        * Sea Sheperd
        * ...
        * https://de.wikipedia.org/wiki/F%C3%A4r%C3%B6er#Walfang
            * "Der umstrittene Grindwal­fang wird von den Färingern nicht kommerziell, sondern als reine Subsistenzwirtschaft betrieben. Zwischen 2001 und 2005 wurden 41 Grindwalschulen aufgebracht und dabei insgesamt 3.359 Tiere getötet, was einer durchschnittlichen Fangmenge von 672 Grindwalen jährlich entspricht. Die Art und Weise der Treibjagden, die mit motorisierten Booten und mittlerweile unter dem Schutz der dänischen Marine abgehalten wird, wird international verurteilt, da sie als grausam gilt. Bewohner der Färöer und Abgeordnete des dänischen Parlaments bezeichnen die Jagden als „traditionell“. Seit Ende 2008 raten die Gesundheitsbehörden, kein Fleisch von Grindwalen mehr zu verzehren, da es aufgrund der hohen Konzentration an Giftstoffen für den menschlichen Verzehr nicht geeignet ist."
    * Film: ["Paul Watson - Bekenntnisse eines Ökoterroristen"](https://www.youtube.com/watch?v=Sh7ZtggGQeM), 2011, 1h 25min, tag:class?
        * [ZDF aspekte](https://www.zdf.de/kultur/aspekte/bekenntnisse-eines-oeko-terroristen-102.html)
            * "Ein witziger und wichtiger Umweltschutz-Film"
            * "Paul Watson ist der wohl berühmteste Umweltpirat der Welt. Seit über 30 Jahren kämpft er mit seiner Armada von derzeit vier Schiffen unter schwarzer Totenkopf-Flagge für den strikten Schutz aller Geschöpfe des Ozeans."
        * Schiffe versenken
        * Vorstellung Paul Watson und Robert Hunter
        * Greenpeace, Sea Sheperd
        * Kameramann der NBC
        * 6:30 "Unsere Freiwilligen sind fast alle Veganer. Zumindest die guten. Bei Veganern handelt es sich um [...]"
        * viele Frauen
        * kein Romantikurlaub
        * Verhaftung
        * 14:15 Robben
            * 17:10 zensiert
            * 18:30 brutale Jagdbilder
            * Besprühen
            * 19:30 Kanadisches Robbenschutzgesetz: entweder 800 Meter entfernt bleiben oder Tötungsabsicht
        * 23:20 Zwischen Kanada und USA
            * Grauwale etc.
        * ...
    * Doku: ["Delfinschlachten auf den Färöer Inseln"](https://www.youtube.com/watch?v=fPNXYWWeam8), 30 min WDR-Doku, 2016, todo: link dead
        * [Zusammenfassung](http://www1.wdr.de/fernsehen/tiere-suchen-ein-zuhause/delfinjaeger-faeroerer-inseln-102.html), 7 min, todo: link dead
        * Doku: [Hintergrund: Menschen, die was dagegen tun](https://www.youtube.com/watch?v=Sh7ZtggGQeM), 1:30 min
            * über die Sea Shepherd Conservation Society und den Gründer Paul Watson
    * Film: Die Bucht - The Cove (2009)
        * Recherche über Delfinschlachten in Japan
        * [Trailer](https://www.youtube.com/watch?v=2dLpA9zpGPM), 3 min
            * u. a. auch Delfinarien (vergleiche [Zoos](../Zoo))
    * Film: Blackfish
* Im Vergleich harmlos: der Begriff "Meeresfrüchte"
* Sonstige Verschmutzung: siehe Mikroplastik

### im Kommen: Elektrofischen
* siehe [fisch-fischer-überfischung.md](fisch-fischer-überfischung.md)

### Austern
* Lebendverzehr: https://de.wikipedia.org/w/index.php?title=Austern_(Lebensmittel)#Verzehr
* PETA: "Es gibt bestimmte Lebewesen – wie Jakobsmuscheln, Austern und Shrimps – von denen wir einfach nicht viel wissen. Wir sind unsicher, wie viel Schmerz und Leiden sie empfinden können. Hier bei PETA fordern wir Menschen auf, „sich auf der Seite des Mitgefühls zu irren“. Da wir nicht sicher wissen, dass diese Geschöpfe nicht leiden können, entscheiden wir uns dafür, davon auszugehen, dass sie Schmerz empfinden können und handeln dementsprechend." (https://www.peta.de/vegetarismus-veganismus)
* ["Möchten Sie lebendig gekocht werden?"](http://leaflet.blogsport.de/2015/05/14/moechten-sie-lebendig-gekocht-werden/), 2015

Schaf
-----
### Lammfleisch und regionale Schäfer
* ARTE-Doku über Schäfer und Schafe, 2019, "Schäfer vor dem Aus - Ein Beruf im Wandel"
    * die Hälfte des Einkommens kommt von Lammfleisch
        * wirtschaftliche Grundlage
    * alle würden diese Form der Tierhaltung wollen (inkl. der Staat wegen Kulturerhalt), aber es gibt keine Subventionen und die Verbraucher sind auch nicht hinterher mehr zu zahlen
    * Videoaufnahmen vom Scheren von Schafen (Hilfskräfte aus Polen), Tierwohl vor maximaler Schergeschwindigkeit
        * gibt kaum Geld für die Wolle
    * Berufsbild stirbt aus. Was nun?
* siehe auch Ostern

### Wolle aus Australien
* Doku: ["Merino-Boom"](https://www.zdf.de/dokumentation/zdfzoom/zdfzoom-350.html), ZDF, 2019, 30 min, "Um möglichst viel Wolle zu produzieren, sind die Schafe überzüchtet"
    * "Wolle von Merinoschafen wird immer beliebter. Mit 88 Prozent ist Australien der weltweit größte Produzent. Doch das Tierwohl spielt auf den allermeisten australischen Farmen keine Rolle."
    * "Je mehr Hautfalten, desto mehr Wolle. Das Problem: Hautfalten sind Angriffsziel für Fliegenmaden. Um das zu verhindern, werden Lämmern Falten um den After weggeschnitten – meist ohne Betäubung." = Mulesing

### Sonstiges
* "Lämmer werden geschlachtet, bevor sie zwölf Monate alt sind", also Lammfleisch stammt von maximal 1 Jahr alten Schäfchen"
    (https://de.wikipedia.org/wiki/Lammfleisch)
* https://vimeo.com/62486583, 5 min, Das Leben der Lämmer
    - Doku über Schafhaltung in Italien
    - keine Ahnung, ob das repräsentativ ist
    - Massenschafhaltung
    - viel Gemähe von großen und kleinen Schafen
    - Lämmer werden an den Beinen ähnlich wie Bananenbündel zusammengepackt, hochgezogen und gewogen
    - Große Zange zum Anbringen der Ohr-Marke
    - Warten der Menge auf die Schlachtung
    - Betäubung wie man es von der Schweineschlachtung kennt mit einer Elektrozange, danach das Messer
* http://graslutscher.de/der-wolf-das-lamm-hurz/
* Lammfleisch für Döner
* Wolle (schmerzhafte Scherung; Züchtung auf viel zu viel Wolle) für Kleidung
    * todo: Akkordscherung? Rasse mit zuviel Wolle?

Strauß
------
* z. B. [Straußenfarm Tannenhof bei Schaafheim](https://www.op-online.de/region/babenhausen/saisoneroeffnung-straussenfarm-tannenhof-schaafheim-9711689.html), 2018, und St.Echo
    * "So richtig zahm werden sie nicht. „Sie tolerieren uns, weil wir ihnen etwas zum Fressen mitbringen“, sagt Roth, aber so richtig Freundschaft schließen kann man mit ihnen nicht."
        * (schlaue Tiere)
    * "die Nachzucht ist überwiegend zur Fleischgewinnung gedacht"
    * über einen Züchter: "Er hat bei seinem ersten Schlachtversuch nicht richtig gearbeitet. Da hat sich der Strauß gewehrt und mit seinem Schnabel den Schädel des Züchters attackiert"
        * (ein eindrückliches Zeichen, dass der Vogel nicht sterben wollte)

Kaninchen
---------
* siehe https://de.wikipedia.org/wiki/Kaninchenproduktion, Stand 2017
    * **Kaninchenfleisch** als "Haupterzeugnis"
        * Deutschland im Jahr 2011: 14 Mio. Kaninchen
        * d. h. um auf die 14 Mio. zu kommen, muss rein rechnerisch jeder der 80 Mio Deutschen alle 6 Jahre ca. 1 ganzes Kaninchen verspeisen, wenn man das so rechnen kann
    * **Pelz** und Angorawolle
        * Achtung potentielle Tierquälerei, unter anderem beim Scheren, siehe https://de.wikipedia.org/wiki/Angora
    * **Tierversuche**
        * z. B. in Österreich 18.439 Kaninchen im Jahr 2005 laut https://de.wikipedia.org/wiki/Tierversuch)
    * **Haustiere / Heimtiere**
        * "Hauskaninchen werden im Regelfall 7 bis 11 Jahre alt, unter idealen Umständen auch älter.", https://de.wikipedia.org/wiki/Hauskaninchen

Pferd
-----
### Betrug mit Hobby-Reit-Pferden
* "Pferde: Schlachthof statt Gnadenhof | NDR | Doku | 45 Min", 2020, https://www.youtube.com/watch?v=6f1gJgMddEw
    * ...
    * Die Menschen sind sehr besorgt um ihre Tiere. Enge Bindungen. Tränen fließen.
        * Können diese Emotionen auch für andere Tierarten aufgebracht werden (Schwein, Rind, Huhn)?
    * Leute, die Pferde wollen und von anderen Leuten betrogen werden
    * Interessante Mentalität: "Da hab ich ein Pony im Stall stehen und kann es nicht reiten"
        * Der Zweck der Tiere ist das Reiten.
    * ...

### Europa
* [Pferdefleischskandal in Europa 2013](https://de.wikipedia.org/wiki/Pferdefleischskandal_in_Europa_2013)
    * "bis zu 100 % nicht deklariertes Pferdefleisch"
    * "auch nicht deklarierte Anteile von anderen Fleischsorten wie Schweinefleisch und Medikamente wie Phenylbutazon"
    * "Betroffen waren insbesondere Tiefkühlkost und Soßen mit Hackfleisch wie Lasagne, Sauce Bolognese und ähnliche Produkte"
    * Frage dazu: Warum fällt überhaupt Pferdefleisch an, das dann illegal, vorsätzlich und fahrlässig verarbeitet wurde?

Pferdefleisch:

* https://de.wikipedia.org/wiki/Pferdefleisch
    * https://de.wikipedia.org/wiki/Pferdemetzgerei
        * Schweiz
            * Zahlen: "In der Schweiz selbst wird allerdings vergleichsweise wenig Pferdefleisch produziert: 411 Tonnen (2011). Jährlich werden rund 5000 Tonnen importiert, größtenteils aus Amerika (Kanada, Mexiko und Argentinien)
                * siehe dazu Problematik der Pferdehaltung in den Exportländern in Amerika
* "Die Schlachtteile und ihre Verwendung" -> Schematische Darstellung
* "Pferdefleisch wird in der Regel wie Rindfleisch zubereitet, auch wenn die Garzeiten meist kürzer sind, da Pferdefleisch grundsätzlich zarter ist"
* Italien
    * "Italien liegt europaweit mit 900 Gramm jährlichem Pro-Kopf-Verbrauch noch vor anderen traditionellen Pferdefleischländern wie Belgien und Frankreich."

### Pferderennen, Springreiten
* FAQ
    * Die Pferde rennen doch gerne / das liegt in deren Natur
        * Ausnutzen von Instinkten (Peitsche in der Flanke => Angriff) => Angst
            * ...bestätigen
        * Das Tier rennt über seine Grenzen und macht sich dabei selbst kaputt (Rennen auf Verschleiß)
            * so was macht man nicht aus Spaß, sondern nur wenn Angst im Spiel ist
    * Pferde sind Fluchttiere
        * ["Das Pferd als Beutetier: So funktioniert sein Alarmsystem | Quarks"](https://www.youtube.com/watch?v=f4f-kbBAUU8), 2013
            * "Soziale und neugierige Natur"
    * Wie Kaninchen leiden Pferde, ohne einen Ton von sich zu geben (erst kurz vorm Tod)
        * ...bestätigen
        * Sehnenschäden, Magengeschwüre (von außen nicht sichtbar) (s. Doku)
        * Angst

* YT: "Skandale, Doping und Quälerei: Das läuft falsch beim Springreiten | Quarks", 2019, 7 min
    * Salbe, die die Fesseln wund machen, damit die Pferde höher springen, weil sie die Stange vermeiden wollen, weil es wehtut

* Video: ["Warum Pferderennen Todesopfer fordert | Quarks"](https://www.youtube.com/watch?v=lR1JEUes6m0), ARD, 2013, 7 min
    * "Einige Pferde galoppieren jährlich eine halbe Millionen an Preisgeldern zusammen und bei den Pferdewetten geht es um gewaltige Summen, denn: 35 Milliarden Euro setzen die Europäer jedes Jahr beim Pferderennen ein"
    * "kaum einer der Besucher von Pferderennen ahnt, was dieser knallharte Leistungssport vielen Pferden antut."
    * Follow up:
        * ["Du armes Pferd - Geliebt. Gequält. Gedemütigt."](https://www.youtube.com/watch?v=_vpGHp1PItc), 2011, ARD, 45 min
            * inkl. Doping von Olympia-Reitern
        * ["Pferde – Warum wir sie lieben und trotzdem quälen"](https://www1.wdr.de/mediathek/video/sendungen/quarks-und-co/video-pferde--warum-wir-sie-lieben-und-trotzdem-quaelen-100.html), 2019
            * "Quarks . 09.07.2019. 44:12 Min.. UT. Verfügbar bis 09.07.2024. WDR"
            * "Wer früher ein Pferd besaß, konnte seinen Acker effektiver bestellen, neue Länder erschließen und Schlachten gewinnen. Heute ist das Pferd Sehnsuchtsobjekt kleiner Mädchen, Hobby für Millionen von Reitern und Basis eines Milliardengeschäfts im Spitzensport. Quarks zeigt, warum das Pferdeleben häufig alles andere als ein Ponyhof ist."

* Doku: ["Das kurze Leben der Rennpferde - Eine Doku des NDR"](https://www.youtube.com/watch?v=epwVOCEdd8o), NRD, 2017, 45 min
    * fast 2 Jahre in der deutschen Galopprennszene recherchiert
    * bis zu 1400 Rennen pro Jahr
    * nach Fußball am meisten besucht / rund 1 Million Gäste pro Jahr
    * Wettumsatz 2016: 26 Mio EUR
    * Video von Pferd mit gebrochenem Bein, das trotzdem versucht weiterzurennen
    * vergangene Rennsaison: 12 Pferde noch auf der Bahn eingeschläfert
        * keine Zahlen für tödliche Unfälle beim Training; vermutlich hunderte
    * viele Pferde scheiden wegen Verletzungen aus, "und verschwinden oft ohne Spuren zu hinterlassen"
    * ehemaliger Tierarzt, der auf der Rennbahn gearbeitet hat, Fachtierarzt für Pferde
        * Sehenschäden
        * Magengeschwüre (nicht von außen sichtbar)
    * ehemaliges Rennpferd mit 8 Jahren ausgemustert / werden normal 30 Jahre und älter
    * 6:45 min
    * Jährlingsauktion für einjährige Vollblüter
        * Erfolgreichster Trainer Deutschlands sucht für Auftraggeber neue Rassepferde
        * Pferd wird äußerlich begutachtet, ob alles korrekt ist, ob die Winkel und die Bemuskelung stimmt / wie Ware eben
        * erste Rennen schon mit 2 Jahren (gerade mal dem Folenalter entwachsen)
            * Dachverband sagt ein Vollblüter sei nicht vergleichbar von der Alterstruktur wie ein Warmblüter
    * Rennstall Recke
        * Beispiel Aspantau: günstig aus England eingekauft, einige Rennen gewonnen => Gewinn
    * Zuschauer finden ab und zu Gewalt an Pferden normal (es gäbe schlimmeres)
        * Galoppverband: die Pferde seien in ihren Element; sind so gezüchtet; in Deutschland geht alles mit rechten Dingen zu
        * einige Video-Bilder dazu. Wo fängt Tierquälerei an?
        * Rennzuchtverband: man könne alles in Frage stellen, aber dem Pferd ist der Galopprennsport am angemessensten
        * Arzt: Pferde laufen eben nicht gerne; wollen nicht in die Start-Boxen und haben Angst vor dem, was dann folgt
        * Rennbahngalopp ist ein Angstgalopp
            * nicht wissenschaftlich belegt, aber viele Tiere haben schon früh Sehnenschäden etc. und können nicht mehr Rennen laufen (für eine Sache, die das Pferd angeblich natürlich und gern macht, passt das nicht zusammen)
    * Pferde seien außerhalb der Rennbahn in Einzelboxen
    * Training
        * monotones Trotten in einer Führanlage (automatische karusellartige Maschine)
        * 23 h / Tag alleine in der Box, 1 h draußen
        * z. B. 10 Min. Galopp pro Tag zum Muskelaufbau
        * seelische Schäden aufgrund der langen Boxenhaltung; natürliche Umgebung - der Weidegang - wird verwehrt
            * in der freien Natur bewegen sich Pferde 16 h / Tag in der Herde
    * Nötige Beruhigungsmethoden ("Hilfsmittel")
        * Ohrenschützer
        * Zungenband => wird an die Zunge geknotet, damit die Zunge vor Stress nicht hochgezogen wird, weil sonst keine Luft mehr während des Rennens
        * Pfeitsche immer noch erlaubt, obwohl sogar das Tierschutzgesetz dagegen spricht ("ohne vernünftigen Grund")
    * Galopperverband: keine Verantwortung für ausgediente Pferde (Sie sind eben Sportgeräte.)
        * als Freizeitpferd oft ungeeignet

### Dressurreiten, Dressurpferde
* ["Tierschutz: Mangelhafte Kontrollen beim Reitturnier CHIO? | Quarks"](https://www.youtube.com/watch?v=bbE3wqXxmmk), 2018, 12 min
    * Beschreibung
        * "Der CHIO ist eines der größten Pferdesportturniere weltweit. Hier messen sich Reiter aus aller Welt in verschiedenen Disziplinen des Reitsports. Immer wieder aber überschatten Skandale das Turnier. 2018 sorgte ein Facebook-Post für einen Shitstorm: Bilder von Pferden in zu enger Kopf-Hals-Haltung mit aufgesperrten Mäulern wurden innerhalb kurzer Zeit über 18.000 Mal geteilt. Wir wollten wissen, wie die Situation für die Dressurpferde dort wirklich ist. Darum haben wir mit mehreren Kameras Aufnahmen am Trainingsplatz gemacht. Das Drehmaterial haben wir Tierärzten, Wissenschaftlern und anderen Sachkundigen gezeigt. Das Ergebnis: In mehreren Fällen haben die Sportler eine Reitweise gezeigt, die aus wissenschaftlicher Sicht den Tieren Schmerzen und Gesundheitsschäden zufügen kann. Aus Sicht der Experten hätte das Aufsichtspersonal am Trainingsplatz eingreifen müssen, was aber nicht geschehen ist."
    * ...
    * Pferd vor Stress auf die Zunge gebissen
    * ...
    * "aufgesperrte Mäuler mit sichtbaren Zähnen => Abwehrreaktion", Facebook-Shitstorm
    * ...

### Pferdegestüt
* ["Fragwürdige Zustände auf Pferdegestüt | Exakt | MDR"](https://www.youtube.com/watch?v=kdBHVs0bIIg), 2018, 7 min
    * "mit Sonderlackierung"
    * unhaltbare Zustände; Behörden greifen nicht ein
        * leichte Verbesserungen durch PETA und MDR-Recherchen ausgelöst

### Anderswo: Pferderennen, Rodeos etc.
* Video: ["Pferdefleisch aus Südamerika und anderes schlimmes"](https://www.youtube.com/watch?v=4uk_nJUKxD0), 2017, 14 min, Tierschutzbund Zürich
    * Auszehrende Pferderennen, vollgepumpt mit Medikamenten
    * Verletzte Pferde bei Rodeos
    * alles nur zur Unterhaltung
    * ...
* Blutskandal für Medikamentenwirkstoffe
    * ...

Tiere im Urwald
---------------
* Positiv-Beipiel: den Urwald stehen lassen
* Aktuelle Situation:
    * Artikel: ["ZDF-Doku über Orang-Utans - Der Mensch ist dem Affen ein Wolf](http://www.spiegel.de/kultur/gesellschaft/zdf-doku-ueber-orang-utans-der-mensch-ist-dem-affen-ein-wolf-a-571458.html)
        * (Exkurs: "wenn ein Tier vorher ein gutes Leben hatte, dann ist es ok..."; siehe hier: todo)
    * Arte-Doku (welche?) zu Borneo und Indonesien, siehe palmöl.md (über Elefanten)
        * für Elefanten ist nur noch ein dünner Streifen Urwald übrig
        * Elefanten werden sogar vergiftet, wegen Palmölplantagen

Tiere in Afrika
---------------
### Nashorn
* 2017: Video: ["Rhino-Horn entfernen, Wilderei verhindern"](http://www.tagesschau.de/videoblog/afrika_afrika/nashorn-wilderei-suedafrika-101.html), 6 min
    * Wilderei wird dadurch leider nicht verhindert, siehe unten.
    * Farm mit Nashörnern, 1500 Tiere
    * Regelmäßig betäubt und das Horn mit einer Säge abgenommen.
        * Danach gibt es ein Aufputschmittel.
        * (Hört sich ja richtig toll an für das Tier.)
    * 10 cm müssen stehengelassen werden, damit nachwächst und es nicht blutet.
    * Horn besteht aus Keratin (wie Fingernägel), Schwarzmarkt viele 10.000de EUR
    * Besitzer der Farm hat erfolgreich gegen ein nationales Nashorn-Horn-Handelsverbot geklagt (international weiterhin verboten).
        * Jetzt darf er es national verkaufen. Bestände im Wert von 60.000.000 EUR.
        * Begründung: so würde die Wilderei durch legalen Handel trockengelegt.
        * Umweltschützer: das Gegenteil ist der Fall. Weil es verhältnismäßig einfach ist, ein illegales Horn als legal zu deklarieren.
        * Nur auf der Farm wird Wilderrei verhindert, weil alle Nashörner ohne Horn rumlaufen. :(
    * Auch Muttertiere werden nicht verschont.
        * Die Milch solle man mal probieren, schmecke wie Zuckerwasser.
    * 9 Hörner in 3 h (Wert: min. 150.000 EUR)
    * Besitzer der Farm gibt an, wenn er auch den ausländischen Markt mit legalem Horn überschwemmt, nähme die Wilderei ab. (fragwürdig)
    * Warum überhaupt?
        * "In Asien werden sie in der traditionellen Medizin verwendet, wo man ihnen Wunderkräfte zuschreibt, die nie nachgewiesen werden konnten"
            * Das erinnert an Homöopathie, siehe dort
        * Die Farmleute sehen sich dazu gezwungen, weil der Markt hauptsächlich von Kriminellen bedient wird.
        * FRAGE: Macht der Farmer auch Aufklärungskampagnen, um aufzuzeigen, dass sich der Kauf von Hörnern (legal oder illegal) nicht lohnt?

### Artgerecht zum Vergleich: Löwen 2018
* https://www.tagesschau.de/ausland/suedafrika-loewen-101.html
    * YT-Link: https://www.youtube.com/watch?v=l8YcV7n-2VE
    * "Ein Ende für die Löwenfarmen-suedafrika-loewen-101 - ARD - 2018 - 3min.mp4", tag:offline
    * Quellen:
        * http://www.bloodlions.org
            * ["Blood Lions Trailer"](https://www.youtube.com/watch?v=-T86GCjCpus), 2015, 2 min
    * Inhalt
        * Löwenfarm
        * Löwenzüchter
        * 0:40 - 1:05: Achtung, brutale Bilder
        * 1:50: "geschlachtet wie am Fließband"
        * 2:30: "Wenn wir die Zucht einstellen, dann werden die sich die wilden Löwen holen"
            * andere lassen das nicht gelten und sagen: Das Züchten der Löwen fürs Töten ist "grausam und unfair"
                * (aber es wird doch alles vom Tier genutzt!?)
    * Fragen zur Diskussion:
        * Zu welchem Zweck werden die Löwen gezüchtet? (0:05 min)
            * getötet werden für Spaß; um sich stark zu fühlen
            * Vergleiche mit:
                * ["Milch Werbung", Milch macht stark, "Entfessel deine Kraft"](https://www.youtube.com/watch?v=olGhA1j8yGs), 2014, 1 min, tag:offline
                    * Kraftversprechen
                * ["Cafe Latte Ad featuring Winona Ryder"](https://www.youtube.com/watch?v=B3Co1ZScDGA), 2006, 15 sek
                    * Luxus
                * ["Weihenstephan TV Spot H-Milch"](https://www.youtube.com/watch?v=dBr3tUOOt7o), 2015, 30 sek
                    * "Geschenk des Himmels"
                    * siehe Kritik an Weihenstephan, wegen Milchviehhaltung, Billigmarke
                        * z. B. https://de.wikipedia.org/wiki/Unternehmensgruppe_Theo_M%C3%BCller#Molkerei_Weihenstephan
                        * ["Milchprodukte: Der Schwindel mit der Almidylle"](https://www.tz.de/tv/zdfzoom-doku-deckt-auf-schwindel-almidylle-zr-3008817.html), ZDF zoom, 2013
                            * Bärenmarke
                * Milchschokolade: ["Milka Nussini Crunch Challenge"](https://www.youtube.com/watch?v=Pwv2DsWuw3Y), 2017, 15 sek
                    * Spaß
                * ["Edeka Eier Werbung 2.0"](https://www.youtube.com/watch?v=RakQJCBxBvU), 2018, 20 sek
                    * "vielleicht ist da Kacke drin"
            * advanced:
                * ["Wenn Werbung für Kuhmilch ehrlich wäre"](https://www.youtube.com/watch?v=fUjYdLyzNu4)
                    * nur teilweise gut
                * ["SwissMilk - Lustige Werbung - Schweizer Milch Werbung"](https://www.youtube.com/watch?v=J3nMlTVCpaA), 2018, 30 sek
                    * Kuh fährt Ski
        * Wieviel Geld kostet es, Löwen zu schießen? (ca. 0:45)
        * Was verwendet der Löwenzüchter alles vom Löwen? (ca. 1:15)
            * Ist es nicht gut, wenn das ganze Tier verwendet wird?
            * An wen gehen die Knochen? An wen das Fleisch?
                * Vietnam, Wein (zur Potenzsteigerung)
            * Welche Rolle spielt Geld?
        * Welche Möglichkeiten gibt es, die Situation zu verbessern?
            * Woher kommt das Geld für die Farmen?
                * von Leuten, die Spaß haben wollen, oder von Leuten, die auf Marketingtricks reinfallen
        * Züchter: seine Tiere seien artgerecht. Meinungen dazu?
        * Warum genau macht das international Schlagzeilen? (2:00)
            * Was ist der Unterschied von Löwe im Vergleich zu Kuh, Schwein, Huhn und Fisch?
            * Sind das nicht einfach **Nutzlöwen**, die gutes Geld bringen?
        * Warum fordert der Politiker ein Ende dieser Praxis? (2:15)
            * Imageschaden
        * Parallelen?
            * Nachfrage treibt die Produktion
            * angeblich artgerecht
            * "geschlachtet wie am Fließband"
            * Ein Dilemma wird gezeichnet: wenn wir nicht, dann...
            * Wo ist das Problem, wenn doch alles vom Tier genutzt wird?

Frosch
------
* Problem: Froschschenkel
    * Video: ["Tierschützer contra Froschschenkel"](http://www.ardmediathek.de/tv/nat%C3%BCrlich/Tiersch%C3%BCtzer-contra-Froschschenkel/SWR-Fernsehen/Video?bcastId=1026394&documentId=43799080), ARD-Sendung "natürlich!", SWR, 24.06.2017, 4 min, (Tierschützer contra Froschschenkel-43799080.mp4), tag:class?
        * Bilder von Fröschen in der Natur, in Froschfarmen (Asien für den europäischen Markt), in der Bratpfanne

Wachtel
-------
* 2016: https://albert-schweitzer-stiftung.de/aktuell/urgasa-beendet-kaefighaltung-wachteln

Känguruh
--------
* für Fleisch
    * Doku über fiese Methoden
        * ...

Hund
----
* in anderen Ländern: für Fleisch
    * enge Käfige
    * etc.
    * ...

Fuchs, Waschbär, Robbe
----------------------
* für Pelz, siehe pelz.md

Ratte, Maus
-----------
* für Tierversuche
* siehe Ärzte gegen Tierversuche

Lebensdauer vs. Lebenserwartung
-------------------------------
### Allgemein
* Bilder aus [Video von Mic the vegan, 10 Quick responses](https://invidio.us/watch?v=A3crW4y9MBc)
    * Lebenserwartung: ![](img/lifespan-cc.jpeg)
    * Animals are killed during harvest: ![](img/animals-harvest-slaughter.jpeg)
    * ![](img/island-vs-abundance.jpg)
* "Lebenserwartung vs. Lebensdauer" - http://www.swissveg.ch/lebenserwartung
    * "Kein einziges Tier, welches für den Fleischkonsum geschlachtet wird, erreicht auch nur einen Sechstel seiner natürlichen Lebenserwartung. Fast alle werden getötet, noch bevor sie überhaupt ausgewachsen sind."
    * Graphik: ![](http://www.swissveg.ch/sites/swissveg.ch/files/bilder/Diagramme/lebenserwartung4.png)
        * img/lebenserwartung4.png
    * und Tabelle für verschiedene Tierarten
    * => Die Tiere müssten in ihrem kurzen Leben mindestens 10x glücklicher sein, um die künstlich verkürzte Lebensspanne aufzuwiegen.
        * FRAGE: Können wir das den Tieren bieten?
* ["Lebenserwartung von Schlachttieren"](http://www.schlachthof-transparent.org/pages/schlachttiere/lebenserwartung.php)
    * (die natürlichen Zahlen scheinen etwas zu hoch)

### Huhn
* http://www.rettet-das-huhn.de/h%C3%BChnerhaltung/besonderheiten-der-ex-hochleistungshennen/
    * "„Normale“ Hühner können über 10 Jahre alt werden -unsere Legehybriden aus der Massentierhaltung erreichen ein solches Alter wegen der extremen Überzüchtung  aber leider nicht. Die Lebenserwartung dieser Hochleistungshennen liegt bei durchschnittlich 3-5 Jahren, weil die körperlichen Ressourcen durch die angezüchtete ständige Eierproduktion schneller verbraucht werden und es durch die unnatürlich hohe Eierproduktion oftmals zu Problemen und Krankheiten im Legeapparat kommt."
    * ...
* https://de.wikipedia.org/wiki/Legehenne
    * Zahlen
        * "Im Jahr 1950 legte ein Huhn 120 Eier pro Jahr, 2015 waren es etwa 300."
        * "In Deutschland gab es in Betrieben mit mindestens 3.000 Hennenhaltungsplätzen am 1. Dezember 2011 gut 34,0 Millionen Legehennen, eine Steigerung von 13,8 Prozent gegenüber Dezember 2010. Die dominierende Haltungsform in deutschen Legehennenbetrieben ist die Bodenhaltung. Von den in Deutschland verfügbaren Hennenhaltungsplätzen waren 25,7 Millionen Plätze (64,2 %) in Bodenhaltung, 5,8 Millionen Plätze (14,4 %) in Freilandhaltung und 5,6 Millionen Plätze (14,0 %) in Kleingruppenhaltung und ausgestalteten Käfigen. Weitere 3,0 Millionen Plätze (7,4 %) gab es in ökologischer Erzeugung."
            * todo
        * "Die weiblichen Küken werden in der kommerziellen Eierproduktion über einen Zeitraum von ca. 18 Wochen aufgezogen, anschließend werden sie eingestallt, um ihrer Funktion als Legehuhn nachzukommen. Diese dauert ca. 15 Monate an, bis die Legeleistung nachlässt und sie geschlachtet und als Suppenhühner verkauft werden. Die männlichen Küken werden am ersten Lebenstag getötet. Legehennen bei privaten Hobbyhaltern, die ähnlich wie Haustiere gehalten werden, erreichen ein Alter von etwa acht Jahren, die Legeleistung lässt nach etwa vier Jahren deutlich nach."
* https://www.huehner-haltung.de/haltung/produktive-huhn/
    * Schlachtalter (auch für Brüderküken): Industrie: 4 Wochen; Hobby-Zücher: 6 - 9 Monate; Lebenserwartung: 5 - 9 Jahre
        * => Überschlagsrechnung:
            * Annahme: Schlachtalter 9 Monate = 3/4 Jahr; Lebenserwartung: 5 Jahre => Faktor 75/500
            * Umrechnung bei Lebenserwartung von 80 Jahren: 80 * 75/500 = **12 Jahre**

### Schwein
* https://de.wikipedia.org/wiki/Hausschwein
    * Zahlen: "Schweine können etwa zehn Jahre alt werden."

Unsortiert
----------
### 2018
* Video: [Schweine - Was passiert, wenn ein Riesenstall wegen Tierschutzverstößen zugemacht wird](https://www.youtube.com/watch?v=F6qKrS73otY), MDR, 2016, 13 min, tag:offline
    * ...
    * Anwohner, Geruchsbelästigung
    * Schöne Worte auf der Webseite
    * ...
    * Ariwa
    * ...
    * keine Transparenz

* Video: [Tierfilmer freigesprochen 1](https://www.youtube.com/watch?v=NNvE-Fyv4ng), ZDF, 2018, 2 min, Änderung nur durch Bilder, Gericht lobt, Schmitz, tag:offline
    * ...

* Video: [Tierfilmer freigesprochen 2](https://www.youtube.com/watch?v=AE_8h1NuLE0), 3sat Mediathek, 2018, 3 min, tag:offline
    * ...

* Video: ["Tierhaltung der Bauern-Chefs zeigt das alltägliche Leiden"](https://www.youtube.com/watch?v=8sbd8pENc6s), ARD panorama, 2016, 9 min, tag:offline
    * ...

* Doku_: ["Planet der Menschen - Das Aussterben der Wildtiere"](https://www.youtube.com/watch?v=BVvyQgj8psY), 3sat, 2016, 44 min, tag:offline
    * ...
    * Artensterben
    * ...

### Industrie 2016
* [Besuch der EuroTier 2016](https://www.youtube.com/watch?v=rMLe7FlbR70), 12 min
    * Käfigkonstruktionen mit Matten für Legehennen, die als "artgerecht" verkauft werden.
    * guter Eindruck über die Zustände, aber Sprecher ist biased
    * interessante Fragen
    * Firma, die mit High-Tech-Kameras und -Software bei Schweinen im Schlachthaus, die am Schlachthaken vorbeirauschen, Auswertungen zu Tierwohlkritieren macht, damit man zur Qualitätskontrolle weiß, ob die lieferende Mastanlage die Tierschutzkriterien einhält
        * z. B. wird auf abgebissene Schwänze getestet
