Vegan / More
============

<!-- toc -->

- [Wortspiele](#wortspiele)
- [Verschiedenes](#verschiedenes)
  * [Insekten](#insekten)
  * [Fleischsteuer?](#fleischsteuer)
- [Lobbyismus gegen Tierschutzarbeit](#lobbyismus-gegen-tierschutzarbeit)
  * [gegen PETA](#gegen-peta)
  * [Geld](#geld)
- [Gegenpositionen](#gegenpositionen)
  * ["hochverarbeitete Lebensmittel"](#hochverarbeitete-lebensmittel)
  * [Tierhaltung ohne Flächenkonkurrenz für menschliche Nahrung](#tierhaltung-ohne-flachenkonkurrenz-fur-menschliche-nahrung)
  * ["jede Art von Landwirtschaft ist schlecht"](#jede-art-von-landwirtschaft-ist-schlecht)
- [Zitate](#zitate)
- [Bedenken](#bedenken)
- [Hintergrund: Vegan im geschichtlichen Verlauf](#hintergrund-vegan-im-geschichtlichen-verlauf)

<!-- tocstop -->

Wortspiele
----------
* Vleischsalat vom Tofutier
* Veine Vahne Vischfilet
    * Visch
* Vutengeschnetzeltes
* Jäger-Vitzel
* oder doch lieber
    * Vicken Curry
        * https://www.dunzo.com/pune/chicken-vicken-shivajinagar
    * Vozzerella

Verschiedenes
-------------
### Insekten
* ["Insekten essen - Nicht die Nahrung der Zukunft"](https://taz.de/Insekten-essen/!5612482/), 2019
    * "Sechs Beine, schmackhaft und gesund: Die UN findet, Insekten sind ein gutes und klimafreundliches Mittel gegen den Welthunger. Aber stimmt das?"

### Fleischsteuer?
* Leute möchten nicht bevormundet werden
    * besser die Tier-Agrar-Subventionen runterfahren; damit die Preise mehr die realen Kosten widerspiegeln

Lobbyismus gegen Tierschutzarbeit
---------------------------------
### gegen PETA
* Planet Wissen, 2015, ZDF, Interview Karremann zum Vorwurf "PETA tötet Tiere"
    * [Teil 1](https://www.youtube.com/watch?v=y667mjNI82Y), 6 min
    * [Teil 2](https://www.youtube.com/watch?v=rom15j89ssY), 3 min
    * https://de.wikipedia.org/wiki/Manfred_Karremann - Autor und Journalist
    * Vorwürfe werden glaubwürdig entkräftet
    * Lobby-Organisation: http://www.petatotet.de
        * http://www.petatotet.de/about
        * Urheber: https://de.wikipedia.org/wiki/Center_for_Organizational_Research_and_Education
        * finanziert von der Tier-Industrie
        * siehe auch Lobbypedia: https://lobbypedia.de/wiki/Consumer_Choice_Center
        * https://de.wikipedia.org/wiki/People_for_the_Ethical_Treatment_of_Animals#Einschl%C3%A4ferung_von_Tieren
            * "In einer Stellungnahme schreibt PETA unter anderem, dass ca. 6 bis 8 Millionen Katzen und Hunde jährlich in US-amerikanische Tierheime gebracht werden, und dass der Anteil unvermittelbarer Tiere in den eigenen Einrichtungen bedeutend höher sei."

### Geld
* siehe User-Comments hier: https://sustainablefoodtrust.org/articles/is-eating-no-meat-actually-doing-more-harm-than-good/, 2017
    * "I love when people like you talk about ''anti-meat propaganda'', which come from vegan association for example, this big powerful lobby that represent 2 or 3 % of the population, in face of this minuscule Agribusiness Products Group, which has no money for fight back..."

Gegenpositionen
---------------
### "hochverarbeitete Lebensmittel"
* ["Vegane Ernährung: Kein Fleisch macht auch nicht glücklich"](http://www.zeit.de/wissen/gesundheit/2014-05/vegane-ernaehrung-essen-fleischersatz), zeit.de, 2014
    * Der Artikel argumentiert, dass eine vegane Ernährung ungesund ist, weil es ungesunde vegane Fleisch-Ersatzprodukte gibt.
    * Wichtig: sich selber informieren und feststellen, dass hochverarbeitete Industrie-Lebensmittel im Allgemeinen ungesund sind (egal ob vegan oder nicht vegan)

* siehe auch
    * Aussage: Vegan ist alles voller Zusatzstoffe!
        * Lösung: http://graslutscher.de/vegan-ist-ja-altindianisch-und-bedeutet-synthetische-chemiepampe/

    * Aussage: Vegane Fertigprodukte sind voll ungesund!
        * Lösung: Richtig, genauso wie alle anderen Fertigprodukte. Besser unverarbeitet und BIO und vegan.

### Tierhaltung ohne Flächenkonkurrenz für menschliche Nahrung
* ["Standpunkt vegan und Landwirtschaft: Fleisch gehört dazu"](http://www.taz.de/!120197/), 2013
    * "Veganer retten nicht die Welt, sagt Ulrike Gonder. Die Ökotrophologin meint, sie wissen zu wenig über die Natur."
        * (Veganer wollen nicht die Welt retten, sondern Unrecht gegenüber Tieren mindern)
    * "Dieser Logik folgte auch die amerikanische Umweltaktivistin Lierre Keith" (Einzelmeinung)
    * "Einer der Denkfehler vieler Vegetarier ist die Annahme, die heute übliche agrarindustrielle Intensivmast sei die einzige Möglichkeit, Tiere zu halten."
        * (Tiere werden auch bei anderen Methoden geschlachtet)
        * (Wo bekommt man denn heute konkrekt Tierprodukte aus solchen idealen Quellen her?)
    * "Anstelle der Tierhaltung mehr Getreide oder Soja für die wachsende Menschheit anzubauen, löst weder das Welthungerproblem noch schont es die Umwelt"
        * (doch, denn weniger Tiere bedeutet weniger Energieverlust bei der Umwandlung des Getreides)
    * ...
    * "Alles, was lebt, hatte eine Mutter (und vieles einen Vater), auch Pflanzen."
        * (was ist mit Hund vs. Schwein?)
    * "Sind sie weniger schützenswert? Wer zieht hier wo die Grenzen?"
        * Thesen:
            * "Was ist mit den millionenfach im Boden lebenden Einzellern, Würmern und Bakterien, die durch den Anbau von Getreide- und Sojamonokulturen getötet werden? Zählen die nicht?"
            * "Warum nicht? Weil man auch als Veganer irgendetwas essen muss? Genau hier wird die Willkürlichkeit der Grenzziehung deutlich:"
            * "Wer jegliches Leben schützen will, wird verhungern.
            * "es gibt kein Essen, kein Leben ohne den Tod – es muss immer jemand sterben, damit ein anderer essen kann."
        * Antworten:
            * Allgemein: vegan heißt soweit praktisch möglich und nicht jegliches Leben
            * zur Grenzziehung: siehe philosophie.md / "Veganismus als neue Aufklärungsbewegung" -> User-Comments
    * "Wer jegliches Leben schützen will, wird verhungern"
        * (vegan bedeutet im Einklang mit der Moralvorstellung zu leben, dass es falsch ist, Tieren ohne Grund Leid zuzufügen)
    * "Man kann sich ohne Fleisch sehr gesund ernähren."
        * (achso)
    * "Kürzlich erklärten zwei prominente US-Veganerinnen, dass sie trotz bester Pflanzenkost krank wurden und nun – wie auch Lierre Keith – wieder tierisches essen."
        * ...

### "jede Art von Landwirtschaft ist schlecht"
* Lierre Keith
    * https://en.wikipedia.org/wiki/Lierre_Keith (eher weniger plausible Argumente)
        * https://matadornetwork.com/change/5-simple-things-can-save-world-right-now/
        * https://skepticalvegan.com/2010/03/19/myths-of-the-vegetarian-myth/

Zitate
------
* "If a man aspires towards a righteous life, his first act of abstinence is from injury to animals.", LEO TOLSTOY, The First Step, http://www.notable-quotes.com/t/tolstoy_leo.html, oder Albert Einstein? (https://www.goodreads.com/quotes/893324-if-a-man-aspires-towards-a-righteous-life-his-first)
* "A man can live and be healthy without killing animals for food; therefore, if he eats meat, he participates in taking animal life merely for the sake of his appetite. And to act so is immoral.", LEO TOLSTOY, On Civil Disobedience, http://www.notable-quotes.com/t/tolstoy_leo.html

Bedenken
--------
* Fingernägel (Calcium) wachsen nicht mehr richtig?
* öfter krank?
* falls krank, nicht mehr richtig gesund werden ohne Tierprodukte
* häufiger Pupsen?

Hintergrund: Vegan im geschichtlichen Verlauf
---------------------------------------------
* Vergangenheit:
    * ca. 500 v. Chr.
        * https://vebu.de/veggie-fakten/geschichte-des-vegetarismus-und-veganismus/
            * "Alles, was der Mensch den Tieren antut, kommt auf den Menschen wieder zurück. -- Pythagoras", ca. 500 v. Chr.
            * (irgendwo stand auch, dass der früh-geschichtliche Begriff "vegetarisch" damals oft im Wortsinn, also als "pflanzlich", verwendet wurde; also das, was heute "vegan" ist. TODO)
            * "Das Wort “vegan” wurde um 1944 von Donald Watson, Gründer der Vegan Society, kreiert."

    * 1890er Jahre
        * " Fleischlose Kost - Wie die Welt veggie wurde - Veganismus ist der Trend? Kann schon sein. Aber wirklich neu ist er deshalb nicht."
            * http://www.faz.net/aktuell/gesellschaft/schriftstellerin-fritzen-ueber-fleischlose-kost-14472414.html
            * Artikel über einen Herrn Riedel, der in den 1890er Jahren vegan lebte, bevor es den Begriff gab.
            * Interessant auch die **damaligen Gesundheitsbedenken**. "Gemüseheilige".

    * ab 1944
        * http://www.ukveggie.com/vegan_news/ (QUARTERLY MAGAZINE OF THE NON-DAIRY VEGETARIANS.) 1944
        * Donald Watson - https://de.wikipedia.org/wiki/Donald_Watson

    * jüngere Vergangenheit:
        * z. B. kommerziell [Lebe Gesund-Vertrieb](https://www.lebegesund.de/content/vom-anbau-bis-zum-kunden-4frg8xjd1dz-c.htm) seit 1983

* Gegenwart:
    * The Vegan Society’s formal definition is: "veganism is a way of living which seeks to exclude, as far as is possible and practicable, all forms of exploitation of, and cruelty to, animals for food, clothing or any other purpose." - https://www.vegansociety.com/go-vegan/definition-veganism", "and by extension, promotes the development and use of animal-free alternatives for the benefit of humans, animals and the environment. In dietary terms it denotes the practice of dispensing with all products derived wholly or partly from animals."
        * "Ripened by Human Determination: Seventy Years of The Vegan Society" - http://www.vegansociety.com/sites/default/files/uploads/Ripened%20by%20human%20determination.pdf

    * Rezeption bei Umweltschutzverbänden: siehe diskussion.md

    * Veganz-Insolvenz, weil nun bereits im Großhandel
        * 17.01.2017 "Veganz meldet Insolvenz für Filialen an" - http://www.manager-magazin.de/unternehmen/handel/veganz-planinsolvenz-fuer-tochtergesellschaft-angemeldet-a-1130144.html

    * manager magazin, 2016
        * 1. Teil: Entdeckung der Besserverdiener - http://www.manager-magazin.de/magazin/artikel/ernaehrung-vegan-ist-der-megatrend-des-jahrzehnts-a-1089183.html
        * 7. Teil: Vleischsalat vom Tofutier - http://www.manager-magazin.de/magazin/artikel/ernaehrung-vegan-ist-der-megatrend-des-jahrzehnts-a-1089183-7.html
            * "Bis 1990 war der Sojaquark, das Grundnahrungsmittel Asiens, in Deutschland verboten."

* Zukunft / Science Fiction:
    * 2030 - NASA mission to Mars
        * hauptsächlich tierproduktfrei, aus logistischen Gründen, siehe z. B. http://inhabitat.com/nasa-astronauts-on-mars-mission-will-eat-100-vegan-menu/ (Quelle???)

    * StarTrek
        * spielt im 24. Jahrhundert (siehe https://en.wikipedia.org/wiki/Star_Trek:_The_Next_Generation_(season_1))
        * Ausschnitt aus Episode "Lonely among us": https://en.wikipedia.org/wiki/Lonely_Among_Us:
            * Video: ["We no longer enslave animals for food purposes."](https://www.youtube.com/watch?v=k8N7XuxThdQ), StarTrek (engl. mit Untertiteln), (gedreht 1987), 1 min

    * The Hitchhiker's Guide To The Galaxy - What's Eating You?
        * https://www.youtube.com/watch?v=bAF35dekiAY
        * An animal that actually _wants_ to be eaten
        * It says it will shoot itself in a humane way
        * Ein anderer Gast am Ende zum verstörten Protagonist: And what's eating you?
