Beispiele
=========

<!-- toc -->

- [Menschen](#menschen)
  * [Deutschland AV 2020, für Einsteiger](#deutschland-av-2020-fur-einsteiger)
  * [Deutschland](#deutschland)
  * [Schweiz - Vegane Bauern](#schweiz---vegane-bauern)
  * [Schweiz - von Menschen und anderen Tieren](#schweiz---von-menschen-und-anderen-tieren)
  * [Österreich](#osterreich)
  * [Vegan auf den Punkt, D](#vegan-auf-den-punkt-d)
  * [Simon West, D](#simon-west-d)
  * [Der Artgenosse, D](#der-artgenosse-d)
  * [Niko Rittenau](#niko-rittenau)
  * [Keanu - Wenn ein Junge vegan wird](#keanu---wenn-ein-junge-vegan-wird)
  * [Dennis Mi, D](#dennis-mi-d)
  * [Vegan ist ungesund, D](#vegan-ist-ungesund-d)
  * ["Magdeburg Vegan"](#magdeburg-vegan)
  * [Friederike Schmitz, D](#friederike-schmitz-d)
  * [The Vegan Strategist - Tobias Leenaert](#the-vegan-strategist---tobias-leenaert)
  * [Melanie Joy](#melanie-joy)
  * [Enjoy Plants, D.](#enjoy-plants-d)
  * [Mit 51 Jahren vegan](#mit-51-jahren-vegan)
  * [That Vegan Couple](#that-vegan-couple)
  * [OFF: James Aspey, Australien](#off-james-aspey-australien)
  * [Matt Cama](#matt-cama)
  * [Hudson Tarlow](#hudson-tarlow)
  * [Paul Bashir und Asal Alamdari](#paul-bashir-und-asal-alamdari)
  * [Ryuji - Peace by Vegan](#ryuji---peace-by-vegan)
  * [Benny the Vegan](#benny-the-vegan)
  * [Humane Hancock](#humane-hancock)
  * [Nikki Tee, engl.](#nikki-tee-engl)
  * [Chris Delforce, Australien](#chris-delforce-australien)
  * [für Sportler](#fur-sportler)
  * [Earthling Ed, UK](#earthling-ed-uk)
  * [Nathan Runkle, UK](#nathan-runkle-uk)
  * [Mic the Vegan, UK](#mic-the-vegan-uk)
  * [George Martin, UK](#george-martin-uk)
  * [Pia Kraftfutter](#pia-kraftfutter)
  * [Gary Yourofsky](#gary-yourofsky)
  * [Tal Gilboa, Israel](#tal-gilboa-israel)
  * [Queen guitarist Brian May](#queen-guitarist-brian-may)
  * [George Monbiot](#george-monbiot)
- [Länder](#lander)
  * [Kanada](#kanada)
- [Organisationen](#organisationen)
  * [Sea Sheperd](#sea-sheperd)
  * [GLS-Bank](#gls-bank)
- [In Kantinen](#in-kantinen)
- [An Schulen](#an-schulen)
- [Juristen](#juristen)
  * [Jens Bülte](#jens-bulte)
  * [Loeper](#loeper)
  * [Goetschel](#goetschel)
- [Bekannte Persönlichkeiten, die vegan leben](#bekannte-personlichkeiten-die-vegan-leben)
- [Graslutscher-Blog](#graslutscher-blog)
  * [Jagdverband](#jagdverband)
  * [Bundes-Politik](#bundes-politik)
  * [Unzureichend recherchierte Darstellung der veganen Ernährung in Medien](#unzureichend-recherchierte-darstellung-der-veganen-ernahrung-in-medien)
- [Kulturen](#kulturen)
  * [Maya](#maya)
- [Verfahrensweisen](#verfahrensweisen)
  * [Biogemüse düngen](#biogemuse-dungen)
- [Etwas zu aggressiv und angreifbar](#etwas-zu-aggressiv-und-angreifbar)
  * [Joey Carbstrong, Australien](#joey-carbstrong-australien)
- [Change of mind away from vegan](#change-of-mind-away-from-vegan)
  * [Bonnyrebecca](#bonnyrebecca)

<!-- tocstop -->

Menschen
--------
### Deutschland AV 2020, für Einsteiger
* YT: "Cube of Truth - Hast du so etwas schonmal gesehen? | 2020", 10 min, sehr gut

### Deutschland
* Aktiv: https://www.activistsforthevictims.de/unsere-termine/

* Video: ["Vegane Ernährung: Leben ohne tierische Produkte | Unser Land | BR Fernsehen"](https://www.youtube.com/watch?v=NjWfXRkjwAI), 2016, 6 min, tag:offline
    * ...
    * was ist vegan
    * vegan und Mangel?
    * vegane Kinder
    * vegan in der Schwangerschaft
    * ...
* Video: [Vegane Studenten-WG in Regensburg](http://www.br.de/fernsehen/ard-alpha/sendungen/campusmagazin/wg-geschichten-vegane-wg-100.html), BR alpha, 2017, 6 min, (Hauptsache vegan-6ca2eb0a-906f-46cc-ac8f-72aad6ad5703), tag:offline, tag:class?
    * Einkaufen ohne Verpackung, Müllvermeidung
    * Vit. D im Winter, Vit. B12 ganzjährig, voll gesund
    * Freundeskreis wichtig
    * positive Lebenseinstellung
* Blog: Fast vegane Familie
    * Netter Artikel http://www.aussteiger-familie.de/wie-gesund-ist-vegan-fuer-die-familie/
        * siehe auch User-Comments zu Kuh-Milch
    * Kontrovers: die Impfpraxis: http://www.aussteiger-familie.de/die-aussteiger-familie/

### Schweiz - Vegane Bauern
* YT: "Vegane Bauern – Landwirtschaft ohne Nutztiere | Reportage | SRF DOK", 30 min, 2020
    * "Zuerst verliebte sich Veganerin Claudia Troxler in Mastschwein Felix und rettete es vor dem Metzger, danach in Beat, Felix’ Besitzer. Jetzt steigen Beat und Claudia Troxler aus der Nutztierhaltung aus, und aus dem Bauernhof Eichenmoos im luzernischen Büron wird der «Lebenshof Aurelio»."
    * Ausstieg aus der Milchviehhaltung aus ethischen Gründen
    * "Reporterin Helen Arnet begleitet Claudia und Beat Troxler auf dem Weg vom Milch- zum Hafermilchbauern: Denn neben den Patenschaften, mit denen der Unterhalt der Eichenmoos-Tiere finanziert wird, wollen sie ihr Auskommen mit regionalem Pflanzendrink erwirtschaften."
    * https://www.hof-narr.ch/aurelio
        * schöne Geschichte über den Weg des Hofes

### Schweiz - von Menschen und anderen Tieren
* Doku: "Vegan – von Menschen und anderen Tieren" (http://www.srf.ch/sendungen/dok/vegan-von-menschen-und-anderen-tieren), 18.12.2014, Schweizer Fernsehen, 50 min, unaufgeregt und positive Grundhaltung zum Thema, (N-1qsUCO4nA), tag:class2018
    * Untertitel einschalten wegen teilweise Schweizer-Deutsch :-)
    * Ein Thema: Was gehört zur **Männlichkeit**? Die Schwachen schützen? Oder sie ausbeuten?
    * [YT-Variante 1](https://www.youtube.com/watch?v=N-1qsUCO4nA), mit Übersetzungssprecher für Hochdeutsch
    * [YT-Variante 2](https://www.youtube.com/watch?v=WJ0slTeA_6g), ohne Untertitel und sonstige Hilfen
    * Srf DOK: Rockkonzert: "Ein richtiger Mann isst ein Stück Fleisch"?
        * "Was ist ein richtiger Mann?"
        * "Sich für die Schwachen stark machen"
        * "Ein richtiger Mann steht ein für die Schwachen und beutet sie nicht aus" ("Der Veganer ist der richtige Mann, weil er sich dafür stark macht")
    * Nutztier ist ein Unwort für sie.
    * "Das hat der Mensch doch schon immer gemacht..." --> Es gibt durchaus Dinge, die der Mensch in seiner Geschichte auch "schon immer gemach", aber dann nicht mehr.
    * "Der Mensch ist doch dafür gebaut..." --> nicht soviel wie er heutzutage isst
    * 6:00 Tochter auch vegan
        * vegan in der Schwangerschaft
    * "Wir finden zuviel Alkohol, Rauchen und Fleisch keine gute Sache. Und das alles tun wir unserem Kind vorenthalten."
    * Küken: das CO2 strömt ein und die Küken ersticken.
    * Kinderarzt: Vorsicht bei Erziehung, sonst kann das Kind bei fleischessenden Freunden innere Widersprüche erfahren.
    * Denn: Fleisch essen ist an sich nicht schlecht. Schlecht ist die Herstellungsweise und schlecht ist die Menge, die verzehrt wird.
    * 21:50 min: **Interview mit A.W.Dänzer, Gründer von Soyana**, mit abgelehnter Dissertation über tierfreien Biolandbau, weil zu radikal
        * Fazit: Tiere zu unserem Genuss massenhaft zu halten ist einfach out.
        * Soyana komplett Kundenfinanziert
    * Veganer provozieren durch ihre bloße Anwesenheit, da sie bestehende Produktionssystem und Umgang mit Tieren hinterfragen.
    * siehe auch [Artikel zum Film](milch.md)

### Österreich
* Video: ["Vegan ORF Schmatzo - Der Koch-Kids-Club"](https://www.youtube.com/watch?v=3UzvtAzTpco), 15 min, 2016, tag:offline
    * die kleine Marie ist vegan
    * "ein Burger mit Tofu ist trotzdem ein Burger"

### Vegan auf den Punkt, D
* Video: [Vegan: "40 Argumente in 15 Minuten"](https://www.youtube.com/watch?v=fFRKa4yagHE), 2017, 15 min
    * Einige gute Argumentationsweisen und Schaubilder
* Video: ["Fakten vs. Gefühle": Probleme sollten nicht durch eine beschönigende Sprache verharmlost werden](https://www.youtube.com/watch?v=8s08xf6tvPk), 2018, 4 min
    * ...
    * Menschen ehrlich informieren, ohne sie wie Kinder behandeln zu müssen
* Video: [Zustände in einem vorbildlichen Schlachtbetrieb: Zeitungsartikel vs. Situation für die Tiere vor Ort](https://www.youtube.com/watch?v=uCfgdUy9N2c), 2018, 9 min
    * 120 Sekunden CO2: Hinweis auf SWR-Artikel über Todeskampf bei dieser Standard-Methode
    * Nummern statt Namen
    * Interview mit Metzger: Tiere töten ist nichts schönes (hat sich das aber zum Beruf gemacht), Soja-Argument, Tiere in Tiertransporten hätten angeblich mehr Platz als Kinder im Schulbus
    * Artikel: https://www.volksfreund.de/nachrichten/themen-des-tages/simon-fleisch-wie-das-schwein-zum-schinken-wird-_aid-7826699
    * siehe Odysso-Video "Besser schlachten?"
    * Tipps für souveräne Gespräche für einen YT-Commentor: "Für mich wurde es einfacher zu diskutieren, als ich angefangen hab mir vor Augen zu halten, dass mein Gegenüber selbst ein Opfer des Systems ist. Stell dir vor, die Leute stehen unter nem Imperiums-Fluch und je länger sie mit dir reden, desto schwächer wird er.﻿"
* Straßeninterviews
    * Interviews nach Schlachtvideos: https://www.youtube.com/watch?v=-mOVvS7JfG0, 12 min, 2017
        * "dann wünsche ich mir doch wieder, dass..."
        * siehe auch README.md
    * https://www.youtube.com/channel/UCHwZu-zfRpib8LhgqeCaOEw/about

### Simon West, D
* [Tiere lieben und essen, geht das?](https://www.youtube.com/watch?v=OwGIQQYbAFQ), 2018, 12 min
    * Straßeninterviews
    * Hund vs. Schwein
    * "Bist du Tierliebhaber", "Isst du Tiere?"

### Der Artgenosse, D
* ...

### Niko Rittenau
* 2018 - Interview privat: https://www.youtube.com/watch?v=M09cQg12UjE

### Keanu - Wenn ein Junge vegan wird
* 2019, 9 Jahre alt: https://vimeo.com/320791659
    * Lob vom Fußballtrainer

### Dennis Mi, D
* ["Interview mit Attila Hildmann](https://youtu.be/Fb5yraCmErk), 2017
    * (vegan-Challenge - wenn ich verliere, esse ich ein Kalbsteak (und du musst es vor unseren Augen töten))
    * wir leben nicht in einer Gesellschaft von Fleischessern, sondern von Menschen mit Unrechtsbewusstsein
    * (vegan - eigene Geschichte erzählen)
* ["FLEISCHESSERIN KANN KEINER FLIEGE WAS ZULEIDE TUN?!"](https://www.youtube.com/watch?v=NyzKISk4Bls), 2017, Cube of Truth Interview, 12 min
    * Bio-Käuferin und nachhaltig
    * "Was ist bei Bio anders als bei konventionell?"
    * ["The Cubes are so Important"](https://www.youtube.com/watch?v=NaxSNN31dNc)
        * Bilder von Menschen
* ["FLEISCH VOM BAUERN NEBENAN?"](https://www.youtube.com/watch?v=hJuCIf4a3J8), 2017, Cube of Truth Interview, 6 min
    * "Du kannst niemand human töten. Das geht nicht."
* ["FLEISCHESSER SIND VEGETARIER ZWEITER KLASSE?!"](https://www.youtube.com/watch?v=hUgvvaDiwKQ), 2017, Cube of Truth Interview, 10 min
    * ...
    * "Mit Betäubung wäre das in Ordnung?"
* ["FLEISCH FÜR KALZIUM UND VITAMINE?!"](https://www.youtube.com/watch?v=KQKo6zWUWCY), 2017, Cube of Truth Interview, 9 min
    * ...

### Vegan ist ungesund, D
Youtube-Channel

* 2019
    * Interview Radio Energy, "Vegan ist ungesund über soziale Ausgrenzung und den besten veganen Burger in Hamburg!", 2017, 16 min
        * sehr sympathisch
        * Doku-Tipp: H.O.P.E. What You Eat Matters (2018), https://www.youtube.com/watch?v=FprMvJYnD44, 1 h 30 min
            * ...
            * Dr. Shiva
            * most grain is not grown for humans
            * ...
            * Campbell
                * Pickel bei Jugendlichen; lass Milch weg (allergieauslösend) und das geht weg
            * ...
        * Doku-Tipp: Cowspiracy
    * Besuch bei Rapunzel, 2019
    * Besuch bei Sonnentor, 2018
    * [Dominion-Filmpräsentation](https://www.youtube.com/watch?v=ws2vm43EXBk)
    * [Zoos](https://www.youtube.com/watch?v=G8cmgjTTb1M)
    * "VEGANER NEHMEN BAUERN IHRE JOBS WEG"
    * https://www.youtube.com/watch?v=Zj4hosTGJek - sympathische vegane Ärztin
        * "Vor langer Zeit hat Natalie (Ärztin) hat ein 1-monatiges veganes Selbstexperiment gestartet. Nach 6 Monaten treffen wir sie wieder und was sich seit dem getan hat": sie ist vegan

* Video: [Interviews auf der Veggie-World Düsseldorf](https://www.youtube.com/watch?v=zp82SXDyUqc), 2017, Vegan ist ungesund, 6 min, tag:offline, tag:class?
    * "WIR ÄRGERN VEGANER | Öko-Messe in #Düsseldorf"
    * vegane Schwangerschaft
    * ...

### "Magdeburg Vegan"
* https://www.magdeburg-vegan.de/ueber-uns
    * https://venus-webdesign.de

### Friederike Schmitz, D
siehe diskriminierung-speziesismus.md

### The Vegan Strategist - Tobias Leenaert
* z. B. http://veganstrategist.org/2020/08/26/thats-disgusting-should-animal-activists-aim-for-the-stomach/
    * comment: "I’ve been grappling with using the ‘disgust’ angle recently, especially in the wake of COVID-19, and when talking about factory farming.
        I have found that these animals are sadly not high up on the empathy-register! People are far more inclined to respond emotionally to pigs or cows."

### Melanie Joy
* https://www.veganadvocacy.org/videos
    * by Dr. Melanie Joy and Tobias Leenaert
    * "E8: Be as Vegan as Possible"
    * ...TODO...

* Melanie Joy
    * "In Conversation with Dr. Melanie Joy", https://www.youtube.com/watch?v=I-vyFKvlus8, 2020, 35 min, Earthling Ed
        * ...
        * messages much much: advocate for peace must be peaceful
            * (some in the comments disagree)
        * ...
    * "Toward Rational, Authentic Food Choices | Melanie Joy | TEDxMünchen", 2015, https://www.youtube.com/watch?v=o0VrZPBskpg, 20 min
        * ...
    * YT: (Toxic) "Vegan Communication: The Problem and The Promise", 2019, 45 min, ?

### Enjoy Plants, D.
* https://enjoyplants.de/videos/
    * Video WDR Lokalzeit
    * Rezept Linsenschnitzelchen

### Mit 51 Jahren vegan
* https://www.ardmediathek.de/ard/player/Y3JpZDovL3N3ci5kZS9hZXgvbzEwOTQ2MTM/alles-hausgemacht-und-vegan, 5 min
    * Pferde, vegan und der Wunsch nach einer positiven Kehrtwende für die Welt

### That Vegan Couple
* "SELFISH" MEAT EATER vs VEGAN DEBATE, 20 min
    * self-proclaimed selfish person is not as selfish after all

* YT: VEGAN WOMAN ENDS UP IN MEAT EATER'S ARMS, 2018, 16 min
    * comment: "That young man is an absolute angel, he got it. You could see him making the connection and him working out the truth."

* MEAT EATER KISSES WOMAN IN VEGAN DEBATE
    * Italy

* HOW TO DEAL WITH CLIMATE CHANGE ANXIETY & DENIERS, 2019
    * various degrees of denial as a way to protect ourselves from the impact
        * deny climate change at all
        * accept that it is happening but deny that humans are causing it
        * accept that it is happening and that humans are causing it but deny that one can do something about it
        * "choice denial", ::wirksamkeit-des-einzelnen
    * Founded 2010: https://www.psychologyforasafeclimate.org/ - Psychology for a safe climate - ::bildung
        * "FOSTERING EMOTIONAL ENGAGEMENT WITH CLIMATE CHANGE"
        * "Our purpose is to contribute psychological understanding and support within the community, **helping people face the difficult climate reality.**"
        * "“Psychology for a Safe Climate ran some fantastic mindfulness workshops with Environment Victoria helping us look after ourselves so we can keep trying to look after the world.”"
        * https://www.psychologyforasafeclimate.org/about-us
            * "Helping people engage rather than withdraw"
        * https://www.psychologyforasafeclimate.org/workshops
            * "Running workshops is our forte! We offer a variety of workshops on developing self awareness, self care and communication. These are tailored to the context and needs of the audience. See current and recent workshops on our EVENTS page."

* PIERS MORGAN vs VEGAN | Pigs Raised In School & Fed To Children - https://www.youtube.com/watch?v=cIh-EwqXgws
    * ähnlich: Rent-a-Huhn
    * Isn't it better to educate how meat is "grown" than not to?
        * If you just show the nice aspects and omit all the cruel aspects (factory farms and slaughterhouses), is this real education?
    * ...

* ARDEN ROSE HUMANE GRILLED CHEESE Response - https://www.youtube.com/watch?v=vEzHlF2QYcY
    * Unterschied zwischen Versehen und Absicht

* EVERY VEGAN MUST WATCH THIS SPEECH! - https://www.youtube.com/watch?v=KIycj6R1ZTw, Israel 2019
    * Tips for activists: "worst thing to say is say nothing at all"
    * "negative peace"
    * https://www.directactioneverywhere.com
        * https://www.directactioneverywhere.com/open-rescue
            * https://tierretter.de/portfolio/tierleid-hobbyzuechter
        * tierbefreier.org
            * https://tierbefreier.org/jagdsabotage-rettet-leben-adbusting-aktion
                * "Der öffentliche Raum ist zugepflastert mit Werbung [...]. Der öffentliche Raum sollte und muss frei von dieser Indoktrinierung von Konsumgütern sein – solange dies nicht passiert, werden und müssen diese Werbeflächen auch für politische Botschaften frei sein"
        * "Direct Action Everywhere (DxE) is an animal liberation group that is notorious for its large-scale actions which sometimes involve illegal tactics such as civil disobedience and trespassing."
            * Documentary by VICE: https://www.youtube.com/watch?v=Vp8-rYqCAnM, 2019
            * Boycott? https://caroljadams.com/carol-adams-blog/why-i-am-boycotting-events-if-dxe-is-also-an-invited-speaker
                * http://www.liberationpledge.com/
        * "Veganism Is Not Enough" speech 8 min, 2017: https://www.youtube.com/watch?v=nqOcQtsur5I
    * DXE Belgien
        * https://www.facebook.com/directactioneverywherebelgium/
* new
    * https://www.youtube.com/watch?v=1xdKxLrm2nQ - vegetarian knows about environment, animals etc. but sometimes it is just convenient
        * What if this footage would play when you want to eat meat? - I would not eat it
        * I understand. You know all the information, it feels wrong but it is so convenient in certain situations.
            * --> "What would make it easier for you (to make the right choice)"?
        * "If I would give you some help, would you be open and maybe try veganismn?"
            * use happy cow when travelling
            * start with what you are eating now (e.g. pasta bolognese) but use the vegan version
    * I CAN'T BELIEVE WHAT MEAT EATERS SAID!
    * UNIVERSITY TALK + Q&A | PODCAST SAMPLE
    * SELF-CARE FOR VEGANS & ACTIVISTS

* YOGA TEACHERS DEFEND EATING ANIMALS - https://www.youtube.com/watch?v=xECtrSwfhTs
    * ...
* EAT EATING BUDDHIST DOESN'T LIKE VEGAN QUESTIONS - https://www.youtube.com/watch?v=SrkskSViGAY
    * ...
* VEGAN MAKES TEENAGE GIRLS FEEL SICK - https://www.youtube.com/watch?v=kk9vGfc1kPU
    * ...
* 3 CHRISTIAN GUYS MOCK VEGAN WOMAN - https://www.youtube.com/watch?v=9DJ28bDZCi0
    * I could need a hamburger, too. The great thing is, we can get a hamburger without animal cruelty
    * ...
* LOCAL WOMAN STOPS BY OUR DAIRY PROTEST - https://www.youtube.com/watch?v=0H8OJHCVnn4
    * ...
* MOST ANNOYING VEGAN DEBATE WITH MEAT EATER EVER! - https://www.youtube.com/watch?v=nNx4PkuMvxs
    * ...
* BEEF FARMER WANTS TO DEBATE VEGAN - https://www.youtube.com/watch?v=CV0b9x1Te40
    * ...
* MEAT EATERS SMASH VEGAN WITH OBJECTIONS - https://www.youtube.com/watch?v=AZ_9F1CNi5A
    * junges Päärchen, ältere Damen
    * ...
* TELL HER TO SHUT UP, I WANT TO EAT MEAT - https://www.youtube.com/watch?v=cK8z23Wq3Y8
    * cute couple
    * ...
* NEW YORK MEAT EATERS SMASH VEGAN WOMAN - https://www.youtube.com/watch?v=vu6PbTkVZ90
    * ...
* GROWN MAN CAUGHT OUT BREASTFEEDING - https://www.youtube.com/watch?v=_RPtOe5u3vw
    * ...
* LITTLE WOMAN TAKES ON BIG TEXAS! - https://www.youtube.com/watch?v=WWPzYN5z0lk
    * ...
* I HAVE THE MORAL HIGH GROUND - https://www.youtube.com/watch?v=283kPN8ygPo
    * ...
* Vegans Kill Plants & Fish are Stupid - https://www.youtube.com/watch?v=GvnVKzwv5T8
    * ...
* GOING VEGAN IS COMPLICATED - https://www.youtube.com/watch?v=srJwz8Tvlt8
    * It is as easy or as complicated as you want it to be
    * ...
* ANIMALS ARE THINGS | 3 ON 1 INTENSE DEBATE! - https://www.youtube.com/watch?v=PJgDJvzSEpA
    * How do those images make you feel? Angry, hungry, sad?
    * someone vs. something
    * "Hunde sind ein etwas, nicht ein jemand"
        * Warum nennen wir sie dann "ihn" oder "sie"?
    * ...
* MEAT EATER'S OUTRAGE - https://www.youtube.com/watch?v=s9N5wvfKKxE
    * this makes me feel bad / do you think if there was nothing wrong with it you still would feel bad?
    * ...

### OFF: James Aspey, Australien
* 2021: YT: "My Investment Just Boosted 24.6%!", 2 min, featuring Randy
    * comment: "Just two shirtless guys in a shed coked up to the eyeballs asking for your money, what could possibly go wrong"

* YT: ["A Christian man argues in a favour of killing animals because he believes God put them here for us to use."](https://www.youtube.com/watch?v=BoW5g2VbDK0), James Aspey, 13 min
    * ... gutes Gespräch mit schwierigem Diskussionspartner...
    *
    * "you have a choice"
    * ...
    * "they care for their families. ... and you say their only purpose is to be food for us when we can easily something else"
    * ...
* [Lies, Respect and Angry Vegans - James Aspey](https://www.youtube.com/watch?v=oCM5a0LAqV8), 2015, 15 Min.
    * (wie man Leuten am besten über Veganismus aufklärt)
    * ...
    * Ideal conversation with a friend drinking cow's milk
        * Ideale Welt: Du siehst deinen Freund Kuh-Milch trinken und fragst: weißt du eigentlich wo die Milch herkommt. Nein. Dann erzähle ich dir das mal .... Oh das wusste ich gar nicht, das ist ja schrecklich. Was kann man tun? Du kannst aufhören diese Produkte zu kaufen und die vegane Botschaft der Liebe verbreiten. Das macht total Sinn, das mache ich sofort.
    * We are told we need milk for Calcium for bones.
    * We are told we need meat for protein.
    * Gemeinsamkeiten: wir suchen nach Freude und versuchen Leid von uns fernzuhalten.
    * Uns wurde beigebracht, dass wir Tierprodukte brauchen um zu überleben. Ich bin froh, dass mir nach 26 Jahren jemand klar gemacht hat, dass das nicht so ist.
    * Jeden Mythos kennen und nach der Reihe richtigstellen.
    * Viele Menschen sehen Veganer als wütend, belehrend etc. Effektiverweise sollte man sich so _nicht_ präsentieren.
    * Es gibt zwar Veganer, die mit aggressivem Stil sehr viel Erfolg haben (er meint vermutlich Gary Y.) und das ist auch gut so. Bei vielen Menschen funktioniert das.
        Aber eben nicht bei allen. Und der aggressive Stil funktioniert nicht für alle, die sich für die Tiere einsetzen wollen
        [gerade in der persönlichen Kommunikation ist Gewaltfreiheit viel effektiver].
        * Am besten kommt man von einem Ort von Verständnis, Frieden und Respekt vor anderen Menschen.
        * Wir müssen nicht die _Handlungen_ der Gesprächsperson respektieren, um die Person zu respektieren.
            * Denn die Leute sind verwirrt. Sie sind gegen sinnlose Gewalt an Tieren, so wie wir das auch waren/sind. Aber sie wissen es im Moment nicht besser.
        * Man, I understand where you are coming from. I used to believe exactly what you believe. Aber hier sind die Fakten: ...
        * **We do not need to blame and shame. We can inform and explain.**
        * Wir müssen keine wütenden Veganer sein. Denn die Wahrheit ist stark genug.
        * **Fokussiere dich auf deine Stärke. Auf das, wo du gut drin bist. Und dann nutze das.**
        Weil wir sind alle in etwas gut und wir alle haben eine Stimme.
        Tiere können sich nicht ohne uns helfen.
            * siehe auch img/
* ["Am I Blowing your mind right now?"](https://www.youtube.com/watch?v=INmWjdIKILE) - James Aspey, 2017, 9 min
    * "We want to show them the killing and the violence. **Because we believe that people are good. And when they know the truth they don't want to contribute to what they are seeing.**"
    * Schönes Interview mit guten Punkten
    * ...
    * Wir sind keine schlechten Menschen. Nur haben wir die falschen Dinge beigebracht bekommen.
    * ...
    * ["Two New Yorkers Get Woke"](https://www.youtube.com/watch?v=tgEg0ZwdIlM), 2017, 3 min
        * ...
        * Wenn du weißt, dass du nicht zu diesen Gewalttaten beiträgst, dann wird eine Veränderung in der vorgehen.
        * ...
        * **Most people are good persons. When you know better, you do better.**
* ["Educating people in Norwich, England on why veganism is the only rational choice for a civilised society to make"](https://www.youtube.com/watch?v=r_G9e9qIGZ4), 18 min, 2017
    * ...
    * gutes Gespräch
    * ...
* ["Dear Vegetarians..."](https://www.youtube.com/watch?v=o_0eLA9vr_w), 2015, 5 min
    * ...
* [Fragen an Tourist nach Tierproduktionsvideos](https://www.youtube.com/watch?v=dwka3Q9F8mE&app=desktop), 2017, 14 min
    * Dog: beat every single day vs. beat once a week
        * thats different, because our body needs meat
    * "What if I tell you that our body does not need any meat"
    * ...
    * einfühlende Fragen
    * ...
* [Zwei Straßeninterviews](https://www.youtube.com/watch?v=JwH-jn5SXRA), 4 min
    * Koch
    * ...
    * Frau
    * ...
* [Hühner](https://www.youtube.com/watch?v=jxBS3coCpfc), 2018, 5 min
    * "Over 50 animal activists peacefully gathered outside of Case Farms slaughterhouse to bare witness and show compassion for the chickens before they were brutally slaughtered for human consumption. Many chickens arrived frozen to death from the extreme cold."
* [“Kids, Put Your Hands Up If..."](https://www.youtube.com/watch?v=fFDNOlpGpes), 2017, 23 min
    * ...
* [YT-Channel](https://m.youtube.com/channel/UCcKFPLxaWvKMwbjd5GjhzKg)
    * "Breaking My 365 Day Vow of Silence", 4 min, 2015
* [Instagram](https://www.instagram.com/jamesaspey/?hl=en)

### Matt Cama
* Cool fitness person
* Asking effective questions: [Street Debate](https://www.youtube.com/watch?v=fpJZCHpMTyk), 2018, 20 min
    * Follow-up: different street interview with different opinions: https://www.youtube.com/watch?v=HL_zvQDf_fo, 2017, 7 min

### Hudson Tarlow
* YT: "Vegan VS College Students", 2020, 7 min
    * ...
    * tried new techniques
        * e.g. expensive? -> depending lifestyle (cheap and good or expensive gourmet; like with steaks)
        * e.g. health? -> do you think not planning a diet is a good reason to let animals being tortured and killed?
    * best benefit? ...
    * remember: as long as you take baby steps...
        * which side of history
    * leaving people with moral imparative and hope people will pick it up
* YT: "Which Side Of History Will You Choose To Stand On?", 2021, 3 min
* YT: "Why Aggressive Outreach is GOOD for Veganism | Livestream with @Skinny Vegan Fitness", 2020, 38 min
    * ...
    * promote main benefit: no long being a hypocrite
    * people respect when you are truthful with them
    * Sympathisch? -> hmm, wenn es dem Aktivist wichtig ist, dass ein random stranger auf der Straße ihn mag, dann kann das mit den Tieren ja nicht so schlimm sein
    * Artikel: https://www.vox.com/world/2019/1/14/18151799/extremism-white-supremacy-jihadism-deeyah-khan / White Supremacy
        * "**Deeyah Khan**, a Muslim woman, met her enemies — and came away more hopeful than ever."
        * "This filmmaker spent months interviewing neo-Nazis and jihadists. Here’s what she learned."
        * mit Trailer
        * "Filmmaker Deeyah Khan sits down with white supremacist Ken Parker in White Right: Meeting the Enemy. **Parker has since left the neo-Nazi movement.**"
        * ...
        * "“I don’t think it’s the responsibility of persecuted people, or abused people, or oppressed people, to have to “reform” extremists”"
    * siehe auch "Veganismus gleich Humanismus"
* irgendwo:
    * siehe Abtreibung; two different issues; bei Veganismus geht es um nicht-menschliche Tiere;
        das heißt nicht, dass Veganer nicht auch Menschenrechtler sein können;
        wenn du auf eine gay rights-Veranstaltung gehst, dann beschwerst du dich (hoffentlich) ja auch nicht,
        dass dort nicht die Rechte von Frauen explizit zum Thema gemacht werden oder Rassismus. / whataboutism

### Paul Bashir und Asal Alamdari
* YT: "Best of AV Street Outreach | July 2019 - February 2020 | Cube of Truth", 2 h 50 min
    * todo
* YT: "Dairy Farmer Admits to Animal Abuse", 2020, 11 min
    * ...
    * learn to respect animals
    * learn that they are **on this planet _with_ us** and not _for_ us
    * ...
    * evil will not last forever... the question is... what will we you do in that
        **transition period** from the the time on you knew better
* YT channel: https://www.youtube.com/channel/UC7ajaARFH8ASLoh5gffmEkg
    * https://youtu.be/TZkrpCCKKWs - interview with girl
        * ...
        * "because you said **try**", whould you do that **with any other injustice** that you know of?
* YT: "You Aren't Against Oppression Unless You're Vegan", 2020, 10 min, Outreacher: Jacob Silva / **oppression, unterdrückung**
    * interview with black lives matter guy
    * ...
* YT: "You're Either an Animal Abuser, or You're Vegan", 2020, 4 min, Outreacher: Hudson Tarlow
    * ...
    * **"And the best benefit is..."**
    * ...
    * **the same mentality**
    * ...
* YT: "Frank Tufano VS. The Truth", 2019, 24 min
    * "this isn't some hippie-dippie bullshit",
        * "we are not f... woodstock attendees; we are adults; ...I swear when I find it appropriate, I am adult; if you are a baby then I guess this conversation won't work"
    * what is veganism about?
        * not about health or the environment but primary the animals
        * if there is one "vegan" parent who harmed their baby, there are at least a thousand other people who harmed their child with their lifestyle choices;
            * it is not about health; **this is an exploitation discussion; it is about whether we find it morally acceptable to exploit animals or not**
    * ...
    * ... camera man of "opponent" takes the message with him
* YT: "Vegan Activist VS Pescatarian", 2020, 12 min, Asal
    * "I still however consume **milk and dairy products and eggs**" and sea animals
    * **"for environmental reasons"**
    * **did you also think about the animals?** (mit Frage beginnen)
    * ...
    * 98 % factory farmed (supermarket, restaurant etc.)
        * 2 % treat animals a bit differently but they are **send to the same slaughterhouse**
        * not an issue with the treatment, but the fact that they are exploited
    * ...
    * kennt auch die B12-Sache
    * ...
    * "**the dairy industry** is just as bad as the meat industry"
    * ...
* Über die Gründer: https://www.anonymousforthevoiceless.org/our-founders
    * ...
    * ...
* Insta: https://www.instagram.com/paulbashir_av
* YT: Asal: "Mother & Daughter become Vegan Activists on the spot", 2019
    * in jedem Land heißt es: unser Land isst halt so gerne Fleisch
    * ...
    * **being vegan is a non-action**, active means help other people find the message
    * ...
* Video: ["Pharmacist vs Paul Bashir"](https://www.youtube.com/watch?v=8NHqcdOYsFA), 2017, 8 min, souveräner Umgang mit schwierigen Aussagen
    * (Pharmazie, Apotheker)
    * Tiere als Produkt sehen
* ["This Guy Was On His Way To McDonalds..."](https://www.youtube.com/watch?v=vVrmSXf-j9E), 2018, 2 min
    * Typ macht spontan beim Cube mit.

### Ryuji - Peace by Vegan
* https://www.youtube.com/c/PeaceByVegan/about - "Hi! I'm Ryuji and my vision is peace for all beings."
* https://www.youtube.com/watch?v=JagXxMQ8yO0 - "The Wrong Way To "Debate" Veganism"
    * make sure to be on the same page
    * start with the destination: **justice for animals** / **oppression, unterdrückung**
    * (ask if bullying a kid is wrong - is not ok or wrong because _I_ think it is - it is wrong because of the victim)
    * ...
    * ...
    * veg advocacy
        * **clear words**
        * **don't argue about (own) opinions** but let people **focus on the animals (= victims) point of view**
            * siehe "Veganismus gleich Humanismus"
        * hold people **accountable**
        * **make irresistable offer** (end massive cruelty; and stop hypocrisy; and easy)
* Das Mittel (vegan) kommt als zweites. **Zuerst über das Ziel im klaren werden**.
    * Erst über den Urlaubsort einig werden, dann über das Mittel

### Benny the Vegan
* ["Farmer Cries While Talking to Vegan"](https://www.youtube.com/watch?v=tmt8jUNlmOg), 2018, 10 min
    * Part 1: It is not about going back in time / You say we should not eat meat? / All I am saying there is no way to justify it nowadays
    * Part 2, 1:35 (72 y o woman): individual taste / no it does not piss me off at all
    * Part 3, 3:20 (couple from Colorado): Local, Free Range / 98 %, any type of restaurant etc.
    * Part 4, 5:00 (owns a farm): does not eat **someone** he knew
* "Should ALL Christians Be Vegan?" - https://www.youtube.com/watch?v=AyKQIAkeNBs
    * **two very open religious people**
* "Vegan vs Jehovah's Witness | NYC Subway" - anstrengend
* "Vegan Questions Dog Lovers | STREET INTERVIEWS"

* Human Hancock
    * "Vegan Ruins Christmas | Street Interviews"
    * "Being Mean to Meat Eaters in London"
        * three girls
    * ""YOU CAN'T HARASS PEOPLE" | Vegan Street Interviews" - https://www.youtube.com/watch?v=JByT9HdmG_k
        * Frage nach Ersatzfleisch

### Humane Hancock
* https://www.youtube.com/watch?v=JByT9HdmG_k - "YOU CAN'T HARASS PEOPLE" | Vegan Street Interviews, 2018
    * Frage: "Do you think not killing animals is more in line with your own values?"

### Nikki Tee, engl.
* ["Teens go vegan on the spot"](https://www.youtube.com/watch?v=cJmBck0Tkjc), 2017, 9 min, Titel stimmt nicht
    * weibliche Empathie

### Chris Delforce, Australien
* ["Chris Delforce Speech Adelaide 2017 Lucent Director"](https://www.youtube.com/watch?v=U-0_78vVWuQ), 2017, 15 min

### für Sportler
* Tim ..., [""Vegans Are Too Weak and Are Too Low Energy To Train With Me," says some guy"](https://www.youtube.com/watch?v=X7YH7usafZY), 2014, 4 min
    * tolle Körperbeherrschung

### Earthling Ed, UK
* "Vegan VS InfoWars" - https://www.youtube.com/watch?v=ScbeMdYkKDc, 42 min, 2018
    * tough questions
    * judeo-christian values justification
    * republican state fear
        * want to ban meat allegation
        * globalist government Vorwurf (allegation)
    * abortion topic
    * grass-fed vs. grains
    * unhealthy
    * Vorwurf: unsustainable (falsch)
    * Vorwurf: anderen Ländern das Essen wegnehmen wollen (falsch)
    * Luxus-Problem (ärmere Länder bräuchten Fleisch etc.) (-> die ärmsten Ländern essen am wenigsten Tierprodukte)
    * humane killing
        * benevolent (wohlwollend, gütig), compassionate (mitfühlend)
    * ...

* Surge: https://surgeactivism.org/aboutsurge, https://surgeactivism.org/whyvegan

* "30 Ausreden von Nicht-Veganern und passende Antworten", siehe dort

* Film_: Land of Hope and Glory

* YT: "This is my advice to vegans.", 2020, 16 min
    * ask questions, socratic method

Straßeninterviews

* YT: "American Public Confronted by Vegan Protest", 2018, 12 min

* "VEGAN TRIES TO CONVINCE MEAT EATERS TO EAT TOFU", 2019

* YT comment by some user
    * "Char Tha Vegan - 1 month ago - 150 years ago, they would have thought you were absurd if you advocated for the end of slavery. 100 years ago, they would have laughed at you for suggesting that women should have the right to vote. 50 years ago, they would object to the idea of African Americans receiving equal rights under the law. 25 years ago they would have called you a pervert if you advocated for gay rights. They laugh at us now for suggesting that animal slavery be ended. Some day they won't be laughing.﻿"
        * move to geschichte.md

* "Every Argument Against Veganism" | Ed Winters | TEDxBathUniversity, 2019
    * ...

* "The Ostrich Effect: The truth we hide from ourselves | Ed Winters | TEDxLundUniversity", 20 min, 2019
    * ...

* "You Will Never Look at Your Life in the Same Way Again | Eye-Opening Speech!", 2018, 30 min
    * sehr gut aufgebauter Vortrag
    * Weg vom Chicken(wings) lover hin zur Öffnung der Augen
    * UN says more plants
        * https://www.plantbasednews.org/news/people-must-eat-plant-based-save-planet-warns-un
        * https://www.unenvironment.org/news-and-stories/story/tackling-worlds-most-urgent-problem-meat

* "Harvard University Class on Veganism | Animals as Commodities", 2019, 28 min
    * ...
    * on how psychology works here
    * ...
    * "a world where sensory pleasure dictates what's right and what's wrong becomes a pretty scary world"
    * "Do the best you can until you know better. Then when you know better, do better.” -Maya Angelou
    * ...

* "Equality and moral consideration" at Harward, https://www.youtube.com/watch?v=M9aAP9RTT4M, 20 min, 2019

* https://www.youtube.com/watch?v=z7uN2ZrHcdI, 2019
    * "Cow's milk is for baby cows. Change my mind."
        * ...
        * heated discussion
            * ...
            * Climate Change, "show me the real link with Cow's Milk"
                * "I can't make it tangible - as with CO2 - but I can show you studies from international scientists"
    * (Fish)
    * (8 min: local farming)

* uestions at Food Festival (Debate): https://www.youtube.com/watch?v=5rpaSo_m1Ho

* DEBATE A VEGAN: Brown University Students vs Veganism
* Brown University Student Won't Accept Veganism: DEBATE A VEGAN, 2019
    * main argument: **lack of necessity** makes it immoral
        * e.g. cannicalism in context of plane crash is ok, but normally, it is not
        * the same is true for all actions that include a victim which are not necessary
    * student responds with weired theory about morality, very convoluted

* ["We Should All Be Vegan | Change My Mind"](https://www.youtube.com/watch?v=zs_UTC9f1YE), 50 min
    * Schwierige und herausfordernde Fragen vom tief religiösen Gesprächspartner

* Geschichte vom Mann mit Hund und drei Optionen

* [Vegans Forcing Their Views? Ft. Joey Carbstrong](https://www.youtube.com/watch?v=yKG1nAbjEX4), 2017, 19 min
    * ...
    * Es ist nicht gut, dauernd zu predigen. - Es ist nicht gut, wenn man Gewaltlosigkeit und Frieden gegenüber Tieren predigt?
    * ...

* ["Humane Bacon?"](https://www.youtube.com/watch?v=GtpgsdOXKB4), 2017, 12 min
    * "Talking to the public about equality, why they wouldn't eat dogs and if there's such a thing as humane bacon. "
    * ...
    * "That's how I was raised" - "Should be way we were raised dictate our morality?" - "No."
    * ...

* ["You're a Vegan? Are You Often Ill?"](https://www.youtube.com/watch?v=HDJCwx6gT2I), 2017, 13 min.
    * "Chatting to the public about why they don't eat dogs and cats - including doctor who is a self-proclaimed animal lover (who eats animals)"

* https://www.youtube.com/watch?v=xelkCwvX1Ys, 20 min, 2016
    * gewaltfreie Kommunikation

* Kontroverse Fragen an Landwirte, etwas hitzig
    * https://www.youtube.com/watch?v=VdPcvOM4vXk, 10 min, 2017
        * ...
        * what is the moral justification
        * ...
        * the world is changing
        * ...
        * all animals has the preference to live their lives
        * ...
        * but we don't have to
        * but it does have to happen
    * https://m.youtube.com/watch?v=yBE3KGqonew, 12 min, 2017
        * ...

* Video: ["How to Talk to Non-Vegans"](https://www.youtube.com/watch?v=PxCF_8sY54o), 2017, 9 min
    * ...
    * 1:15: Try to learn from your Gegenüber as well
        * use logic and reasoning when false arguments are presented
    * 2 min: Sokrates-Methode, the Socratic Method, ::komm
    * ...

### Nathan Runkle, UK
* [Exclusive Q&A With Mercy for Animals' President Nathan Runkle](https://www.youtube.com/watch?v=635mMmtD4UI), 2017, 3 min
    * ...
    * siehe auch [LIVEKINDLY](https://www.youtube.com/channel/UCjacRo8crmvJuQt6nyFXyBg/videos)

### Mic the Vegan, UK
* ["Grass-Fed Beef Debunked"](https://www.youtube.com/watch?v=_zADSiDr_TM), 2019
    * https://www.americangrassfed.org/the-facts-about-the-usdas-ams-grassfed-marketing-claim-recission/
    * ...
    * 8 min: guter Hinweis
        * (Irgendwo gibt es jetzt und in Zukunft perfektes Gras-fed beef) Macht es dann Sinn jetzt weiterhin das eigentlich ungewünschte zu kaufen?
    * ...
* ["Myths that Vegans Spread"](https://www.youtube.com/watch?v=6s_6iddUdJs), 2018
    * ...

### George Martin, UK
* **A practical solution**
    * ["George Martin & Paul Bashir: How to Win an Argument With a Vegan"](https://www.youtube.com/watch?v=oAtSvcNZGao), 7 min, 2016
        * ...
        * There are few things you can say to a person who says "I don't care". But gladly, most people do care.
        * But what about arguments from highly educated people who say that grass-fed beef is ok; good for the animals and better for the environment etc.
            * Really? Do they consume only grass-fed beef and no other animal exploitation products (like milk, leather etc.)? What about in the restaurant?
            * => This sounds not like a practical solution to reduce animal suffering and environmental issues.
            * Good news: Veganism is a **practical** solution.
        * Most arguments sound like excuses.
            * vegan vs. non-vegan = reason vs. excuse
        * Also eine vegane Lebenseinstellung ist eine praktische Lösung für tatsächliche Probleme
            * Diskutiert wird meist über Rand- und Ausnahmesituationen.

    * You are vegan but you do other stuff which is bad.
        * Vegan sein, heißt nicht, dass man ein Heiliger ist. Eine vegane Lebenseinstellung ist in erster Linie eine für alle Leute praktisch durchführbare und effektive Lösung bzw. Milderung einiger wichtiger Probleme.
            * vergleiche "100% vegetarisch" vs. "limes(99%) vegan"

* ["Your step-by-step guide to justifying killing animals and destroying the planet"](https://www.facebook.com/george.magicmartin/posts/10156258279625541), by George Martin, 2015
    * "Step 1: REMIND THEM THAT HUMANS HAVE BEEN EATING MEAT FOR THOUSANDS OF YEARS"
    * "Step 2: BECOME A PLANT RIGHTS ACTIVIST"
        * "Everybody knows that killing animals and killing plants is the same thing."
    * "Step 3: ASK THEM WHAT THEY'D DO IF THEY WERE STRANDED ON A DESERT ISLAND"
    * "Step 4: REMIND THEM THAT EATING MEAT ISN'T ILLEGAL"
    * "Step 6: POINT AT YOUR CANINE TEETH"
        * ..., "For consistency, be sure to sexually assault someone and then point at your dick when the police question you as to why you did it."
    * "Step 7: TELL THEM THAT WE NEED TO EAT ANIMAL PRODUCTS TO SURVIVE"
        * "Many vegans are unaware of the fact that they are actually dead, much like Bruce Willis' character in 'The Sixth Sense'."
        * (Was genau fehlt?)
    * "Step 10: REMIND THEM THAT THEY USE ELECTRICITY"
        * "Being involved in a justice movement is hypocritical if you use electricity. Be sure to also tell racial equality campaigners, gay rights activists and anti child abuse campaigners that their cause is pointless for the same reason."
    * "Step 12: REMIND THEM THAT YOUR SITUATION IS JUST LIKE A LION'S"
    * "Step 13: MAKE THEM AWARE OF HOW YOU EATING MEAT AND ANIMAL PRODUCTS ACTUALLY BENEFITS THINGS"
    * http://www.challenge22.com/challenge22

* ["Will the World Go vegan in Our Lifetime?"](https://www.youtube.com/watch?v=CJ00HDs2ciI), 8 min, George Martin
    * an injustice can't last forever
    * there will always be people that do bad stuff, that's how the world works
    * sooner or later meat and other animal cruelty will be considered amoral by the majority of people

### Pia Kraftfutter
* ["Warum ich keine Eier esse"](https://www.youtube.com/watch?v=48PbKIEfajI), 2017
* ["Warum ich keinen Käse esse"](https://www.youtube.com/watch?v=P9SqiX6htJk), 2017

### Gary Yourofsky
* YT: "An Unfair Race - Gary Yourofsky", 2020, 5 min
    * if we wait for all human rights fixed before going to animals we will wait forever
* Video: [Best speech mit deutschen Untertiteln](https://www.youtube.com/watch?v=ZCMAIMnI8iw), Georgia Tech, July 8, 2010, YT 2013, 1h 10 min
    * https://www.youtube.com/watch?v=U5hGQDLprA8, YT 2015, mit vielen Untertiteln
* Video: ["Gary Yourofsky - Brilliant Interview in Prime Time (HD)"](https://www.youtube.com/watch?v=p1W5RQOxgdU), 2014, Israel, 25 min
* Video: [Interview with Gary Yourofsky in Israel TV](https://www.youtube.com/watch?v=FE7Yf5fkTqc), 2015, 15 min
* Video: ["Killing ‘em with Kindness - Gary Yourofsky"](https://www.youtube.com/watch?v=Pdjje7s8Jgc), 2015, 3 min

* ["Live! Gary Yourofsky, Animal Activist, in Jerusalem"](https://www.youtube.com/watch?v=kmZ8mULWSDE), mit Intro von Steve Cutts (YT: "MAN", 2012, 4 min)
    * ...
    * victimized
    * hypocrite, speciesist
    * ...
    * YT: "MAN 2020" by Steve Cutts, 1 min, artwork.md

* 2018: "How Gary Yourofsky Went Vegan: The Octopus Story" - https://www.youtube.com/watch?v=Ky-VOFW51M8

### Tal Gilboa, Israel
* ALF Isreal, Total Liberation
* Civil Disobedience inside of an Egg Hatchery - https://www.youtube.com/watch?v=lg0-mB7d5I8
* Winner of Big Brother
* Interview with Vegan Couple: https://www.youtube.com/watch?v=CKYhMfB5xcE
* Glass walls- The lecture: https://www.youtube.com/watch?v=5-g9D9tFpLw

### Queen guitarist Brian May
* YT: "Queen guitarist Brian May and Yourofsky promote compassion"

### George Monbiot
* https://www.monbiot.com/2018/04/03/the-day-i-became-a-vegan/, 2018
    * coming from Film: Seaspiracy, 2021

Länder
------
### Kanada
* 2019: siehe "Kanada streicht Milch- und Milchprodukte fast komplett aus der offiziellen Ernährungsempfehlung"
* Kanada 2017: ["Canada’s New Draft Food Guide Favors Plant-Based Protein And Eliminates Dairy As A Food Group"](http://www.huffingtonpost.com/entry/progress-canadas-new-food-guide-will-favor-plant_us_5966eb4ce4b07b5e1d96ed5e?utm_hp_ref=vegan)
    * "taking back our eating recommendations from industry and promoting evidence-based eating patterns to benefit our health and planet."
    * wie Brasilien: [Food-based dietary guidelines - Brazil](http://www.fao.org/nutrition/education/food-based-dietary-guidelines/regions/countries/brazil/en/)
        * "Ten Steps to Healthy Diets: 1. Make natural or minimally processed foods the basis of your diet - Natural or minimally processed foods, in great variety, and mainly of plant origin, are the basis for diets that are nutritionally balanced, delicious, culturally appropriate, and supportive of socially and environmentally sustainable food systems."

Organisationen
--------------
### Sea Sheperd
* ["Peter Hammarstedt of Sea Shepherd"](https://www.youtube.com/watch?v=L3F9su3f5uk), 2012, 15 min, engl.
    * Rede
    * ["Sea Shepherd // Press Conference // Germany Frankfurt Airport"](https://www.youtube.com/watch?v=4-ZeNi5PdR4), 2012, 9 min

### GLS-Bank
* 2018: https://blog.gls.de/bankspiegel/lasst-uns-ueber-tiere-sprechen
* 2018: Graspapier, siehe dort

In Kantinen
-----------
* siehe [kantine](../Hintergrund/kantine.md): Veganes Essen in Kantinen

An Schulen
----------
* siehe [kantine](../Hintergrund/kantine.md): Veganes Essen an Schulen

Juristen
--------
### Jens Bülte
* Jens Bülte, Mannheim; siehe auch tierrechte.de (spenden-atlas.md)
    * https://de.wikipedia.org/wiki/Jens_B%C3%BClte
        * "deutscher Jurist und Hochschullehrer an der Universität Mannheim"
        * "Lehrstuhl für Strafrecht, Strafprozessrecht und Wirtschaftsstrafrecht"
    * Artikel: **"Warum wird Tierquälerei so selten bestraft?"**, ZEIT, 2018
        * https://www.zeit.de/arbeit/2018-06/tierquaelerei-betriebe-missstaende-wirtschaftsstrafrecht-interview
        * "Amtsveterinäre, die Bauernhöfe kontrollieren, werden oft blockiert.
            Der Strafrechtler Jens Bülte macht dafür auch die schlechte Arbeit von Staatsanwälten verantwortlich."
        * Tierschutzgesetz wird systematisch nicht durchgesetzt.
        * Essay: "Zur faktischen Straflosigkeit institutionalisierter Agrarkriminalität", 2018
            * "Tierschutzfälle aus den vergangenen Jahrzehnten untersucht"
            * https://madoc.bib.uni-mannheim.de/44143/
                * "Wer eine Tierquälerei begeht, wird bestraft, wer sie tausendfach begeht, bleibt straflos und kann sogar mit staatlicher Subventionierung rechnen."
        * "Gerichtsentscheidungen über Tierschutzfälle aus den vergangenen 40 Jahren ausgewertet. Viele davon sind handwerklich einfach schlecht und nicht haltbar. Darüber hinaus habe ich den Eindruck, dass Staatsanwaltschaften Strafverfahren oftmals ohne ernsthafte Ermittlungen mit der Begründung einstellen, der Agrarunternehmer hätte nicht gewusst, dass die Haltung der Tiere so nicht erlaubt ist."
        * "Strafbare Tierquälerei setzt voraus, dass der Täter vorsätzlich gehandelt hat. Wer also nicht weiß, was er tut, wird nicht wegen Vorsatz bestraft."
            * "Hier werden aber zwei Dinge verwechselt: Wer nicht weiß, was er tut, handelt ohne Vorsatz. Wer aber weiß, was er tut, und nur glaubt, es sei erlaubt, ist nur dann straflos, wenn er seinen Irrtum nicht vermeiden konnte."
            * "Es gilt: Nur wer sich umfassend erkundigt und neutral beraten lässt, erfüllt seine Pflichten. In allen anderen Rechtsgebieten setzt die Justiz dieses Prinzip sehr streng um, nur im Tierschutzstrafrecht wird sehr großzügig damit umgegangen. Eine juristische Begründung für die Ungleichbehandlung kann ich nicht erkennen."
        * "Studie des Braunschweiger Thünen-Instituts aus dem Jahr 2015"
        * "Bei Tierschutzverstößen müssen Sie als Staatsanwalt ermitteln. Sie müssen Veterinärämter befragen, die oft kein Interesse daran haben, den Missstand aufzudecken, weil sie sich durch Unterlassung selbst strafbar gemacht haben könnten."
        * "Die Tierschutzfälle aus den vergangenen Jahren zeigen, dass die Verteidiger Spezialprobleme auspacken, von denen der Staatsanwalt noch nie gehört hat."
        * "Was müsste man tun, um die Arbeit der Staatsanwaltschaft zu verbessern?"
            * ...
        * "Die Bundesregierung schreibt im Koalitionsvertrag, dass Stalleinbrüche von Tierschützern künftig härter bestraft werden sollen."
            * "Vielmehr lohnt da die Frage: Sollen hier wirklich Landwirte vor kriminellen Tierschützern geschützt werden, oder eher kriminelle Agrarunternehmer vor Tierschützern?"

### Loeper
* Loeper
    * https://de.wikipedia.org/wiki/Eisenhart_von_Loeper
    * gründete 1987 die Vereinigung „Juristen für Tierrechte“
    * nebenbei: Protest gegen Stuttgart 21
        * http://stuttgart21.strafvereitelung.de/
        * https://de.wikipedia.org/wiki/Protest_gegen_Stuttgart_21

### Goetschel
* Buch_: Tiere klagen an, von Antoine F. Goetschel, 2012

Bekannte Persönlichkeiten, die vegan leben
------------------------------------------
* [Benedict Cumberbatch](https://www.peta.org/blog/benedict-cumberbatch-says-he-eats-vegan/)
    * https://en.wikipedia.org/wiki/Benedict_Cumberbatch
* Listen
    * [vegane Promis 2014](http://www.huffingtonpost.de/alexandra-hildebrandt/was-nutzt-ein-intellektueller_b_8064732.html)
    * http://vegaliferocks.de/vegane-promis/
    * https://vebu.de/veggie-fakten/prominente-vegetarier-und-veganer/
* [Moby](https://de.wikipedia.org/wiki/Moby)
    * ["Why I'm A Vegan | Moby | TEDxVeniceBeach"](https://www.youtube.com/watch?v=6DgUb9w8mOY), 2018, 12 min
        * ...
        * "Dating musicians is always a terrible idea"
        * ...
        * vegan animal activist since 35 years!
        * ...
    * Musik-Video zu Moby - Disco Lies, Hühner, artwork.md
    * Video: [PETA-Interview mit Moby](https://www.youtube.com/watch?v=xaAxsv3R_Sk), 7 min, 2011, englisch mit deutschen Untertiteln
        * ... What's wrong with dairy and eggs? ... It's factory farming ... no one profits except for the shareholders ...
    * 2018: "MOBY CALLS FOR VEGAN UNITY (PBN Spotlight #3)" - https://www.youtube.com/watch?v=slQcuw8oMwQ, 14 min
* [Peter Dinklage](https://de.wikipedia.org/wiki/Peter_Dinklage)
    * bekannt aus Game of Thrones
    * Video: [PETA-Interview mit Peter Dinklage](https://www.youtube.com/watch?v=QKWKUU0XQ8U), 2014, 4 min, englisch, (mit schlimmen Bildern)
* [Bryan Adams](https://en.wikipedia.org/wiki/Bryan_Adams)
    * "Adams has been a vegan since 1989"
    * "If you love animals, don’t eat them." ([PETA-Interview](https://www.peta.org/features/bryan-adams-animal-rights-vegan/))
* [Leonardo di Caprio](todo)
* Hannes Jaenicke
    * über Plasikmüll: wie funktioniert Veränderung? Wer muss anfangen? -> "Es ist ein Dreiklang. Als erstes muss der Konsument etwas verändern und einfach keine Plastikflaschen mehr kaufen. Die Industrie und die Politik müssen natürlich auch umdenken. Es funktioniert nur, wenn alle drei etwas tun. Da ich die Politik und die Industrie nur schwerlich beeinflussen kann, kann ich nur vor der eigenen Tür kehren und meide Plastik wo ich nur kann." ([Internview](http://eatsmarter.de/blogs/star-watch/interview-hannes-jaenicke), 2014)
* Thomas D
    * [PETA-Video](http://www.peta.de/thomasdvegan), 1 min
    * siehe auch http://ianimal360.de/
    * Schweine und Charakter: "Thomas D. über Schweine": https://www.youtube.com/watch?v=KHNr4OacUEE, 2 min
* [Joaquin Phoenix](https://www.themoviedb.org/person/73421-joaquin-phoenix)
    * ["He became a vegan when he, at the age of 3, joined his siblings on a boat to catch fish. He saw how a fish was caught and tossed aside, writhing"](https://en.wikipedia.org/wiki/Joaquin_Phoenix#Animal_rights_activism)
    * [sozial aktiv](https://en.wikipedia.org/wiki/Joaquin_Phoenix#Social_activism)
    * [PETA-Video von Joaquin Phoenix über Fische](https://www.youtube.com/watch?v=U94p7XgpWM8), 2013, 1 min, englisch
    * [Interview zu Dominion](https://www.livekindly.co/joaquin-phoenix-rooney-mara-vegan-documentary-dominion/amp/)
        * Trailer: https://vimeo.com/230171301
* [James Cameron](https://en.wikipedia.org/wiki/James_Cameron)
    * [Regisseur](https://www.themoviedb.org/person/2710-james-cameron) (Avatar etc.)
    * "In 2012, Cameron, his wife and his children adopted a vegan diet.[96][97] Cameron explains that "By changing what you eat, you will change the entire contract between the human species and the natural world""
    * "When asked what's the best thing an individual can do to fight climate change, Cameron said, "Stop eating animals.""
    * ["Less Meat, Less Heat: Behind the Scenes with James Cameron & Arnold Schwarzenegger"](https://vimeo.com/169913909), 3 min
* [James Cromewell](https://en.wikipedia.org/wiki/James_Cromwell)
    * Schauspieler und Aktivist
* [Tobey Maguire](https://de.wikipedia.org/wiki/Tobey_Maguire)
    * Schauspieler

Graslutscher-Blog
-----------------
### Jagdverband
* [Extreme und seltene Einzelmeinungen mit Verbindung zur Tierrechtsthemtik präsentiert vom Deutschen Jagdverband](https://www.youtube.com/watch?v=gYKblw45mTY), 3 min
    * siehe YT-Comments und hier: https://graslutscher.de/ueber-das-durchgeknallte-video-vom-durchgeknallten-jagdverband
    * Thema komplett verfehlt, hier wird unzulässig verallgemeinert

### Bundes-Politik
* http://graslutscher.de/zuckerminister-rat-fur-eine-ausgewogene-kinderernahrung-mindestens-ein-snickers-zu-jeder-warmen-mahlzeit/
* Die Lösung für den Milchpreis
    * http://graslutscher.de/hilfe-der-milchpreis-habt-ihr-den-milchpreis-gesehen-warum-tut-denn-niemand-was/

### Unzureichend recherchierte Darstellung der veganen Ernährung in Medien
* FAZ: [„Vegan geht nicht ohne Pharma“](http://www.faz.net/aktuell/race-to-feed-the-world/veganismus-mediziner-warnen-vor-mangelernaehrung-15756772-p3.html)
    * todo: wieviel Pharma steckt in der Tierhaltung?
    * Überschrift "Mangelernährung in Deutschland" wegen Veganismus passt nicht zum Inhalt
        * user comments
            * "Das Sommerloch scheint dieses Jahr besonders groß zu sein... Mangelernährung - wie es auch in dem Artikel herausgestellt wird, trifft hauptsächlich sozial schwache und weniger informierte Menschen."
            * "Durch die Chemisierung der Landschaft haben alle Menschen ein B12-Problem, da das Bodenmilieu zerstört wurde."
                * siehe Bio

* [Spiegel Online, 2018](http://graslutscher.de/ich-habe-den-bericht-uber-den-veganen-selbstversuch-im-aktuellen-spiegel-gelesen-damit-ihr-es-nicht-tun-musst/)
    * Kommentar: http://www.spiegel.de/lebenundlernen/schule/vegane-kita-in-frankfurt-jetzt-kommen-die-extremisteneltern-a-1220807.html
        * Mit folgenden Begriffen (Thema: Gewalt in der Sprache) wird dort die Eröffnung einer veganen Kita in Frankfurt kommentiert und Eltern (die oft gar nichts selber vegan sind), die ihre Kinder in diese Kita geben, damit in Zusammenhang gebracht:
            * "Extremisteneltern", "dogmatischer Sündenfall", "übergriffige Behütung", "dogmatischen Grundhaltung", "radikaler Konsequenz", "ihr gesamtes Umfeld missionieren", "Salafisteneltern", "christlich-radikale", "evangelischen Sekte Zwölf Stämme", "Prügelstrafe und Demütigung", "unverantwortlich", "Zwang zum Veganertum", "könnten die Erzieherinnen dann ja eine Hexe verbrennen"
        * Im Graslutscher-Artikel finden sich wichtige Fakten zu einem runden Bild, die im Sp-Kommentar weggelassen wurden
            * ...
    * Artikel: http://www.spiegel.de/gesundheit/ernaehrung/vegane-ernaehrung-fuer-kinder-wie-gut-ist-sie-geeignet-a-1221323.html
* ["Wie der Deutschlandfunk 6 Absätze mit 6 fragwürdigen Falschbehauptungen veröffentlichte"](https://graslutscher.de/wie-der-deutschlandfunk-6-absatze-mit-6-fragwurdigen-falschbehauptungen-veroffentlichte), 2018
    * ...
* [Spiegel Online, 2016](http://graslutscher.de/ich-habe-den-bericht-uber-den-veganen-selbstversuch-im-aktuellen-spiegel-gelesen-damit-ihr-es-nicht-tun-musst/)
* [Motherbord, 2016](http://graslutscher.de/eine-welt-mit-motherboard-artikeln-ist-eine-dummere-welt/)
* zu Kindeswohl siehe gesundheit-ernährung.md

Kulturen
--------
### Maya
* Maya, https://de.wikipedia.org/wiki/Maya#Steinarchitektur
    * "Bemerkenswert ist, dass den Maya, ebenso wie den übrigen Völkern Amerikas (mit Ausnahme der Inkas, die in geringem Umfang Lamas nutzten), keine Lasttiere zur Verfügung standen, und dass das Rad (obwohl prinzipiell bekannt) als mechanisches Hilfsmittel nicht verwendet wurde. Die großen Mengen Baumaterial wurden daher ausschließlich durch Menschenkraft bewegt."

Verfahrensweisen
----------------
### Biogemüse düngen
* Video_: "Biogemüse düngen - Klee-Pellets statt Hornmehl", siehe biovegan.md

Etwas zu aggressiv und angreifbar
---------------------------------
### Joey Carbstrong, Australien
* YT: "This made him so angry he threatened to shoot me" -> Kentucky Fried Swan

Straßeninterviews

* YT: "Devout Christian & Vegan CLASH in heated exchange", 2020
    * everything in moderation... shooting people in the head in moderation?
    * man or animal? man, animal or vegan burger.
    * but now that you are aware, you can be held accountable
        * if I pay someone to hurt you I am as responsible as the guy that actually hurts you
    * what tells you the book that is writting in your heart? your conscience
    * would Jesus approve of slaugherhouses?
    * these animals are God's creations; they can feel pain and suffer because of us;
        why eat them when God also created plants for us; eating them is the work of the devil
    * if I believe in God, I trust what he has written in my heart
        * the book was written by humans; I don't trust those humans translating God correctly; humans can lie
    * we have different opinions on whether God exists; but we found common ground on animal cruelty; that these slaugherhouses are ungodly
    * god is most important; you must do other things than that -> how do you know that god hasn't put me on this mission? (why do you judge me?)
    * animals _are_ already in hell; if animals had a religion human beings would be the devil; no one else on this planet tortures them as we do
* YT: "It happened again, except this time...", 2020
    * vegan police man
    * often the regular, down-to-earth people are reacting most humanly (compared to more educated ones)
    * order vegan food for a poor guy
* JOEY CARBSTRONG VS 'ANTI-VEGAN' AVI YEMINI - https://www.youtube.com/watch?v=AYgaXoeB_II
* ["She Saw The Truth.. And Went Vegan On The Spot"](https://www.youtube.com/watch?v=K7sjiYCuZ-0), 2017, 15 min
    * 1200 Kommentare
* https://www.youtube.com/watch?v=dP6dlyuw2sY, 2017
    * Cruelty free is good / there are lots of alternatives today in our society
    * "I have seen places where animals have been treated very well" - "Did you also witness how those animals were killed?"
* https://www.youtube.com/watch?v=oenCot8hLeo, 2017
* "vegan vs. meateater"
* "Vegan vs. Cambridge students"
* "Buddhist and vegan activist clash"

Change of mind away from vegan
------------------------------
### Bonnyrebecca
* ["5 reasons you SHOULDN'T go vegan - DEBUNKED"](https://www.youtube.com/watch?v=QISizOO_x5E), 22 min
    * 0:45 - We are designed to eat meat
        * question: what is _ideal_ for humans to eat?
    * 7:29 - Deficiencies
    * 13:35 - My parents wont let me go vegan
    * 14:51 - I like the taste
    * 16:25 - Convenience, tradition and social acceptance
