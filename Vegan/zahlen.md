Zahlen
======

<!-- toc -->

- [Biomasse Nutztiere, Menschen, Wildtiere](#biomasse-nutztiere-menschen-wildtiere)
- [Killcounter](#killcounter)
- [Politik: Mehrwertsteuer](#politik-mehrwertsteuer)
- [Tierzahlen](#tierzahlen)
  * [Kaninchen](#kaninchen)
- [Tierversuche](#tierversuche)

<!-- tocstop -->

Biomasse Nutztiere, Menschen, Wildtiere
---------------------------------------
* Zahlen: Weltweite Biomasse Landwirbeltiere: 65% Nutztiere, 32% Menschen, 3% Wildtiere
    * https://utopia.de/0/gruppen/veganer-innen-167/diskussion/weltweite-biomasse-wirbeltiere-65-nutztiere-32-menschen-3-199549
* Zahlen: 2018: "Jedes zweite Säugetier ist ein »Nutztier«"
    * https://albert-schweitzer-stiftung.de/aktuell/jedes-zweite-saeugetier-ist-ein-nutztier
* Zahlen: "Der Studie zufolge sind 60% aller Säugetiere weltweit Nutztiere (0,1 Gt C), während es der Mensch auf 36% und Wildtiere gerade einmal 4% (0,007 Gt C) bringen. Auch bei Vögeln sieht es ähnlich aus: 70% der Biomasse entfällt heute auf domestiziertes Geflügel (0,005 Gt C), hauptsächlich Hühner, während Wildvögel gerade einmal 30% ausmachen.", 2018
    * https://www.weltagrarbericht.de/aktuelles/nachrichten/en/33212.html

![](https://utopia.de/0/uploads/assets/user/45211/Gloabl_terrestrial_vertebrate_biomass.jpg)

Killcounter
-----------
* http://adaptt.org/about/the-kill-counter.html
    * "These are the numbers of animals killed worldwide by the meat, egg, and dairy industries since you opened this webpage."
    * inklusive Fisch

Politik: Mehrwertsteuer
-----------------------
* 2017:
    * Umweltbundesamt fordert höhere Mehrwertsteuer auf Tierprodukte
        * ["Umweltbundesamt will höhere Steuern für Milch und Fleisch"](http://www.sueddeutsche.de/wirtschaft/essen-und-trinken-umweltbundesamt-will-hoehere-steuern-fuer-milch-und-fleisch-1.3321976)
            * "Wer tierische Produkte im Supermarkt einkauft, muss dafür bislang nur eine reduzierte Mehrwertsteuer zahlen. Genau wie für andere Lebensmittel werden sieben statt 19 Prozent fällig"
            * "Publikation mit dem Titel "Umweltschädliche Subventionen in Deutschland""
            * "Auch die Wissenschaftler wiesen darauf hin, dass eine Reduzierung des Fleischkonsums besonders wichtig für den Klimaschutz sei"
        * ["Umweltbundesamt für Mehrwertsteuererhöhung bei Fleisch und Milch"](https://www.topagrar.com/news/Home-top-News-Umweltbundesamt-fuer-Mehrwertsteuererhoehung-fuer-Fleisch-und-Milch-6849094.html), 2017, topagrar
        * http://www.tagesschau.de/inland/mehrwertsteuer-milch-fleisch-101.html
            * "Dem Bericht zufolge liegen die umweltschädlichen Subventionen in Deutschland bei 57 Milliarden Euro. Krautzberger sagte, beim Subventionsabbau leiste sich Deutschland "seit Jahren riesige blinde Flecken". Einerseits verpflichte sich das Land auf internationaler Ebene zu mehr Klimaschutz, gleichzeitig werde klimaschädliches Verhalten im eigenen Land mit Steuergeldern subventioniert."
        * Was ist die https://de.wikipedia.org/wiki/Mehrwertsteuer?
        * [FAQ Tierische Produkte höher besteuern](https://www.umweltbundesamt.de/fuer-klima-umwelt-tierische-produkte-hoeher)

Tierzahlen
----------
### Kaninchen
* Zahlen: 2017: "340 Mio Kaninchen werden jährlich in Europa in engsten Käfigen gezüchtet", Quelle?

Tierversuche
------------
* todo
