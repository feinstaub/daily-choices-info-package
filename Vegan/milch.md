Milch
=====

<!-- toc -->

- [Milch-Infos 2021](#milch-infos-2021)
  * [2020: ARD: Eine Welt ohne Fleisch (S01/E02) - Biobauer seit 20 Jahren keine Milchviehhaltung](#2020-ard-eine-welt-ohne-fleisch-s01e02---biobauer-seit-20-jahren-keine-milchviehhaltung)
  * [2020: Bio-veganer Landbau mit Daniel Hausmann](#2020-bio-veganer-landbau-mit-daniel-hausmann)
  * [2019: Bilderserie: Wie entsteht Kuhmilch? (inkl. Besamung)](#2019-bilderserie-wie-entsteht-kuhmilch-inkl-besamung)
  * [2020: 73 Cows - from beef to organic vegan farming](#2020-73-cows---from-beef-to-organic-vegan-farming)
  * [2020: YT: Alle essen vegan? Was dann? | mal angenommen – tagesschau-Podcast](#2020-yt-alle-essen-vegan-was-dann--mal-angenommen-%E2%80%93-tagesschau-podcast)
- [Milch-Dokus](#milch-dokus)
  * [NDR: Die Welt der Milch: Von Robotern, Melkkarussel und Biomilch, 2018](#ndr-die-welt-der-milch-von-robotern-melkkarussel-und-biomilch-2018)
  * [ARD: Das System Milch, 2017](#ard-das-system-milch-2017)
  * [ARD: Milch und Gesundheit, 2017](#ard-milch-und-gesundheit-2017)
  * [Bildungsangebote](#bildungsangebote)
  * [Werbung - Oatly, 2021 plus Hintergrund 2019 - CEO: put climate change in the center of everything](#werbung---oatly-2021-plus-hintergrund-2019---ceo-put-climate-change-in-the-center-of-everything)
- [Gängige Praxis](#gangige-praxis)
  * [2019: Milchviehskandal in Bayern](#2019-milchviehskandal-in-bayern)
  * [Export deutscher Kuh-Milch](#export-deutscher-kuh-milch)
  * [These: ein erwachsener Mensch braucht nach der Stillzeit keine weitere artfremde Milch](#these-ein-erwachsener-mensch-braucht-nach-der-stillzeit-keine-weitere-artfremde-milch)
- ["Sag Nein zur Milch"](#sag-nein-zur-milch)
  * [Zentrale Infos von gegengeprüft](#zentrale-infos-von--gegengepruft)
  * [Kuh-Milch-Infographik](#kuh-milch-infographik)
- [Schweiz](#schweiz)

<!-- tocstop -->

Milch-Infos 2021
----------------
### 2020: ARD: Eine Welt ohne Fleisch (S01/E02) - Biobauer seit 20 Jahren keine Milchviehhaltung
* siehe "Eine Welt ohne Fleisch" biovegan.md

### 2020: Bio-veganer Landbau mit Daniel Hausmann
siehe biovegan.md

### 2019: Bilderserie: Wie entsteht Kuhmilch? (inkl. Besamung)
* http://www.kuhmilch.org

### 2020: 73 Cows - from beef to organic vegan farming
* https://lockwoodfilm.com/73-cows, 15 min, ca. 2019
    * "73 Cows tells the story of Jay Wilde, the first farmer in the UK to trade beef farming for sustainable organic vegan farming, giving up his entire herd of cattle in the process."

### 2020: YT: Alle essen vegan? Was dann? | mal angenommen – tagesschau-Podcast
* "In xyz ist heute die letzte Milchkuh gestorben"
* sei großer Yogurth-Fan
* ... TODO ...

Milch-Dokus
-----------
### NDR: Die Welt der Milch: Von Robotern, Melkkarussel und Biomilch, 2018
* ["Die Welt der Milch: Von Robotern, Melkkarussel und Biomilch | Wie geht das? | NDR"](https://www.youtube.com/watch?v=15mLITIep60), 30 min, 2018
    * Betrieb 1: viele Kühe, wenig Personal, 300.000-EUR-Melkroboter, Putzroboter, dicke Euter, jede Kuh hat eine Nummer und ist per Chip Computer kontrollierbar, jeden Tag eine Besamung, Auswahl nach Roboter-Gängigkeit, also z. B. hoch sitzendes Euter
    * Betrieb 2: Bio-Betrieb, stabile Bio-Milchpreise, auch dicke Euter, auch Kraftfutter, geben weniger Milch
    * Betrieb 3: Nur Gras- und Heu-fütterung => Heumilchkäse
    * Betrieb 4: mit Molkerei, also inklusive Vertrieb, 400 Kühe, Melkkarussel
    * Zahlen: 65.000 Milchviehbetriebe in Deutschland (2018)

### ARD: Das System Milch, 2017
* Doku: "Das System Milch", ARD, 2017, 45 min
    * https://de.wikipedia.org/wiki/Das_System_Milch
    * "Milch ist ein gefragter Rohstoff, mit dem knallhart gehandelt wird. Der Dokumentarfilm "Das System Milch" blickt hinter die Kulissen der Milchindustrie und zeigt die Konsequenzen für Mensch, Tier und Umwelt."
    * "Milch-Doku auf ARD - Ein Film wie eine Überdosis Laktose", 2019
        * "Mehr als einmal lässt „Das System Milch“ einem die Landliebe sauer werden. Immerhin zeigt die ARD-Doku, dass es auch anders geht."
        * ...
    * ...
    * Zucht-Praxis und Bullenschau
    * ...

### ARD: Milch und Gesundheit, 2017
* Doku: ["betrifft: Milch - wie gesund ist sie wirklich?"](http://www.ardmediathek.de/tv/betrifft-/betrifft-Milch-wie-gesund-ist-sie-wir/SWR-Fernsehen/Video?bcastId=1100786&documentId=47156190), "25.10.2017 | 45 Min. | UT | Verfügbar bis 25.10.2018 | Quelle: SWR"
    * Konventionelle Milchviehhaltung mit Karusell, Höchstleistung, Kraftfutter
    * Ernährungsexperte pro Milch
    * Ernährungsexperte Milch-skeptisch
    * Demeter-Milchkuhhaltung, nur Gras, mit Namen
    * 11 min TODO: weiterschauen

### Bildungsangebote
* Der gemeinnützige Verein Mensch Tier Bildung e.V, siehe dort

### Werbung - Oatly, 2021 plus Hintergrund 2019 - CEO: put climate change in the center of everything
* YT: "What have we here? (English version) | Help Dad | Oatly", 2021
    * "Need help talking to dad about milk?", https://help-dad.com
* YT: "John Schoolcraft of Oatly on How to Crack Consumer Marketing Without a Marketing Team", 18 min, 2019
    * Oatly gab es schon lange: für laktose-intolerante Menschen und Veganer; nur sahen die Packungen langweilig aus
        * (new CEO: main reason is to make the world better, see GS interview: https://www.youtube.com/watch?v=IFB1kPTTL0A "Toni Petersson, Chief Executive Officer of Oatly", 2020)
            * why success? -> Toni Petersson: put climate change in the center of everything; reason for existence; culture; every decision
                * companies are about people; there has to be a reason why to get up in the morning and go to work
                * running such a company is not easy but it is easy_er
                * knowing what to do seems more intuitive
    * The Cow-Way, the Oatly-Way
    * Made for Humans
    * Milch-Lobby-Klagen in Schweden
    * Only one strategic document... :-)
    * ...
    * Get rid of the marketing department
        * a way to build trust, be collaborative, because everyone is together in meetings
        * create ideas and execute them (instead of: what might the CEO think?)
    * Have an opinion (as a company) and use the packaging to convey it
    * become a voice; make stuff for real people; don't use KPIs just be oneself
    * be ourselves everywhere we go in the world (you don't change basically)
    * ca. 12:15: Bjölk, Pjölk... only real milk tastes like milk
        * they registered those names and put them on the package
    * food industry, show us your numbers: climate footprint on package
* YT: "Toni Petersson, Chief Executive Officer of Oatly", 2021

Gängige Praxis
--------------
### 2019: Milchviehskandal in Bayern
* ARD
    * "Mangelt es an Kontrollen?" -> Ja
    * Versammlung aufgebrachter Bürger, SOKO Tierschutz eingeladen (weil Videos aufgenommen)
    * "Massive Tierschutzverstöße in der Milchproduktion", ARD Report Mainz, https://www.ardmediathek.de/daserste/video/report-mainz/massive-tierschutzverstoe-e-in-der-milchproduktion/das-erste/Y3JpZDovL3N3ci5kZS9hZXgvbzExMzQ2NzI/, 2019, einer der größten Milchviehbetriebe in Bayern / Allgäu, 8 min, Mülln
        * mit Informant

### Export deutscher Kuh-Milch
* nach China, obwohl dort 94 % laktoseintolerant sind
    * http://www.spiegel.de/wirtschaft/milch-china-ist-deutschlands-wachsender-absatzmarkt-a-916711.html, 2013
        * ["Hochwald - H-Milch für China- Deyatür"](https://www.youtube.com/watch?v=oXIHn792xOI), engl. 2013
            * Es wird mit grünen Weiden geworben und sonstigem :-(

### These: ein erwachsener Mensch braucht nach der Stillzeit keine weitere artfremde Milch
* ["Gedankenspiel: Kein Erwachsener braucht Milch"](https://www.svz.de/deutschland-welt/panorama/kein-erwachsener-braucht-milch-id20144582.html), 2018
    * "Eigentlich trank unser Autor gern Milch – dann begann er zu recherchieren und las sich durch Berge von Studien. Was er herausfand, brachte ihn zum Nachdenken"

"Sag Nein zur Milch"
--------------------
### Zentrale Infos von  gegengeprüft
* Webseite: http://www.sagneinzumilch.de
* http://www.sagneinzumilch.de/produktion.php
    * "Kühe geben wie alle Säugetiere, nur dann Milch, wenn sie Nachkommen zur Welt gebracht haben. Sie sind wie Menschen neun Monate lang schwanger"
        * Das sagt auch das Lobbyportal der konventionellen Landwirtschaft zum Thema [Trächtigkeitsdauer](http://www.agrilexikon.de/index.php?id=1041).
    * "Milchkühe werden nicht als Lebewesen begriffen, deren Bedürfnisse zählen oder denen der Menschen ähnlich wären, sondern als Produktionseinheiten"
    * "lebenslangem Kreislauf von Trächtigkeit und Laktation"
    * "Ab dem zweiten Lebensjahr beginnt für die Kühe die Dauerschwangerschaft"
    * "Sie werden jedes Jahr künstlich befruchtet und sind fast ununterbrochen schwanger, damit sie unterbrochen Milch geben können"
        * Das sagt auch das Lobbyportal der konventionellen Landwirtschaft zum Thema [Kalbung](http://www.agrilexikon.de/index.php?id=763).
            * "Im Regelfalle wird jedes Jahr ein Kalb geboren" (d. h. min. 1x/Jahr wird befruchtet)
    * "Haben sie neun Monate Schwangerschaft hinter sich, gebären sie. Die Kälber werden ihnen kurz nach der Geburt weggenommen, was für die sozialen und sensiblen Tiere immer traumatisch ist"
    * "Würden die Mutterkühe nicht kurz nach der Geburt wieder befruchtet, wären sie nach Ende ihrer Laktationsperiode wirtschaftlich nicht mehr nutzbar."
    * "Mittlerweile konnte die Laktationszeit von etwa ursprünglich sechs Monaten auf 10 bis 11 Monate ausgedehnt werden. Daher kann davon ausgegangen werden, dass etwa die Hälfte der heute ermolkenen Milch von schwangeren Kühen stammt."

### Kuh-Milch-Infographik
![](http://www.sagneinzumilch.de/assets/infografik_milch.jpg)

Schweiz
-------
Artikel zum Film - 2014-12-28: http://www.srf.ch/sendungen/dok/warum-veganer-allesesser-provozieren inkl. User-Kommentare über Milchproduktion

* "Nach den Recherchen zu diesem Film und vielen Gesprächen mit Veganerinnen und Veganern bin ich persönlich zum Schluss gekommen: Veganer sind nicht extrem. Sie sind höchstens extrem konsequent. Sie machen auf die Missstände im Umgang mit unseren Nutztieren aufmerksam. Sie regen zum Denken an. Sie provozieren mit einer klaren Haltung. Sie sind der Stachel in unserem Fleisch – und der soll ruhig ein bisschen weh tun."

User-Comments:

* "@Stefan: Da muss ich dir als ehemaliger Milchproduzent leider widersprechen. Das Tierprodukt mit dem wenigsten Leid ist wohl Bio-Weide-Beef. Aber gerade Milch und Eier sind mit sehr viel Leid verbunden. Vegetarier essen also Tierprodukte, die sehr viel Leid verursachen. Nichtsdestotrotz ist der Schritt zum Vegetarier ein wichtiger Schritt, der später oft zum Veganismus führt. War bei mir genau so. Mehr Details zu den Gründen unter vegan.ch Stichwort "Hintergründe".

* "Der Dok-Film war für mich sehr beeindruckend! Ich bin zwar noch nicht Veganer, aber bin überzeugt, dass der Weg "richtig" ist - fühle mich motiviert, bestärkt und bin "unterwegs"!"

* "Seit fast 20 Jahren lebe ich in den USA, wo diese Kontroverse (Veganer missionieren... Fleischesser reagieren...etc.) schlichtwegs kein Thema ist. In meinem Umfeld werden Veganer und Vegetarier vollends akzeptiert, bei sogenannten "potlucks" gibt es jederzeit fleischlose Alternativen. Ich kann nicht verstehen, warum dies in der Schweiz nicht auch moeglich ist. Warum denn diese Zweispaltung, Beleidigungen und Rechtfertigungen?"

* "Als ehemaliger Milchproduzent muss ich leider sagen: Haltung von Milchvieh ohne Leid ist unmöglich. Allenfalls kann das bei einer reinen Fleischproduktion möglich sein. Aber der Umweg Tier ist eine massive Verschwendung (weit über 3x weniger Kalorien pro ha) und ohne Tötung geht es nicht. Die Gülle belastet je länger je mehr das Grundwasser. Da vegan gesund funktioniert, spricht alles dafür, keine Tiere für Nahrung mehr zu züchten. Die Tötung eines Tieres ist und bleibt ein unnötiger Gewaltakt."

* "@Rochus: Alle Nutztiere sind unnötige, und zum grossen Teil Qual-Zuchten. Diese Tiere würden lieber nicht geboren werden und sie belasten die Umwelt. Die wilden Tiere gehören zu dieser Erde, nicht aber menschengezüchteten "Nutztiere". Bei laufend geringerer Nachfrage werden diese immer weniger nachgezüchtet. Letztendlich wird man vielleicht ein paar robuste von ihnen frei leben lassen, damit sie nicht aussterben. Mir würden allerdings die wilden Vorfahren reichen (Wildschafe, Wildschweine usw.)."

* "@Mirjam: Dass die vegane Ernährung gesund ist, ist erwiesen. Egal welche Ernährungsform, sie muss einfach vielfältig und nicht zu Fett und Eiweisslastig sein, dann ist sie gesund. Die Ernährungsgewohnheiten von vor 100 Jahren waren wohl dem Veganismus näher als dem heutigen Wahnsinn."

* "Veganismus ist alles andere als ein Luxusproblem. Unser hoher Tierproduktproblem ist hingegen eine Luxuserscheinung - das kann doch nicht so schwer zu kapieren sein oder?. Was hat man in der Schweiz während der Kriegszeit in hohem Masse angebaut? Kartoffeln! z.B. in Brasilien sind schon viele Familien am verzweifeln, weil ihnen an sehr vielem fehlt. Schuld sind die riesigen Sojaplantagen für unsere Nutztiere. Da gelobe ich mir mein Sojagenuss aus D-A-CH. Veganismus wirkt GEGEN den Hunger."
