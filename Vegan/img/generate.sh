#!/bin/bash

echo "Bilderliste" > README.md
echo "-----------" >> README.md
echo "(autogen!)" >> README.md
echo "" >> README.md

for a in $(ls *.png *.jpg) ; do
    echo "![]($a)" >> README.md
    echo "" >> README.md
done
