Bioveganer Landbau
==================

<!-- toc -->

- [Funktionierende Beispiele](#funktionierende-beispiele)
  * [Susanne Heine, Weser Bergland, Bioveganes Gärtnern](#susanne-heine-weser-bergland-bioveganes-gartnern)
  * [PfalzBio, Bernd Kugelmann](#pfalzbio-bernd-kugelmann)
  * [Bio-Hof Franz bei Dresden](#bio-hof-franz-bei-dresden)
  * [2020: ARD: Eine Welt ohne Fleisch (S01/E02) - Biobauer seit 20 Jahren keine Milchviehhaltung](#2020-ard-eine-welt-ohne-fleisch-s01e02---biobauer-seit-20-jahren-keine-milchviehhaltung)
  * [2020: Bio-veganer Landbau mit Daniel Hausmann](#2020-bio-veganer-landbau-mit-daniel-hausmann)
  * [2020: Mutter Natur Österreich](#2020-mutter-natur-osterreich)
  * [Bio-vegane SoLaWi – Guter Grund e.V., Frankfurt](#bio-vegane-solawi-%E2%80%93-guter-grund-ev-frankfurt)
  * [Tolhurst Organic in England](#tolhurst-organic-in-england)
  * [Griechenland](#griechenland)
- [Inbox](#inbox)
  * [2020: neue Kultur, wo es keinem einfällt, dass Tiere Lebensmittel sind](#2020-neue-kultur-wo-es-keinem-einfallt-dass-tiere-lebensmittel-sind)
  * [2020: „Braucht der Ökolandbau noch Tiere?“ Prof. Dr. Bernhard Hörning, 2018](#2020-%E2%80%9Ebraucht-der-okolandbau-noch-tiere-prof-dr-bernhard-horning-2018)
  * [2020: Bio-Obst vom Bodensee biozyklisch-vegan](#2020-bio-obst-vom-bodensee-biozyklisch-vegan)
  * [2018: Erste biozyklisch-vegane Betriebe anerkannt](#2018-erste-biozyklisch-vegane-betriebe-anerkannt)
- [Hintergrund](#hintergrund)
  * [Informationsportal Biozyklisch-veganer Landbau](#informationsportal-biozyklisch-veganer-landbau)
  * [biovegan.org offline](#bioveganorg-offline)
- [Entwicklungen](#entwicklungen)
  * [2018](#2018)
  * [2017](#2017)
- [Fragen](#fragen)
  * [Wie soll man ohne Tiere richtig düngen?](#wie-soll-man-ohne-tiere-richtig-dungen)
  * [Terra Preta](#terra-preta)
  * [Was ist mit den Regenwürmern?](#was-ist-mit-den-regenwurmern)
  * [Sind Tiere für eine nachhaltige Landwirtschaft unbedingt notwendig?](#sind-tiere-fur-eine-nachhaltige-landwirtschaft-unbedingt-notwendig)
- [Sonstiges](#sonstiges)
  * [Lupine](#lupine)

<!-- tocstop -->

Funktionierende Beispiele
-------------------------
### Susanne Heine, Weser Bergland, Bioveganes Gärtnern
* https://www.hortus-aquaveganum.de/bio-vegan-g%C3%A4rtnern/, "Hortus Aquaveganum"
    * Video_: "Bioveganes Gärtnern - Was ist das? - Susanne Heine erklärt es perfekt; denn auch hier bevorzugen wir 100 % pflanzlich", 3 min
        * Bio plus ethischer Motivation, **keinem Tier bewusst zu schaden**
        * ohne tierisches düngen und besonders gut für die wilden Tiere, die im Garten vorbeikommen, sorgt
        * Dünger: **"Erde wird oft mit Schlachtnebenprodukten vorgedüngt"**, "Das muss aber nicht deklariert werden"
            * geschredderte Klauen etc.
        * ...
    * Weitere Videos

### PfalzBio, Bernd Kugelmann
* in der Pfalz: http://pfalzbio.de
    * eingebettetes Video: YT: "Bio ist nicht gleich Bio | RON TV |", 2018
        * **Bernd Kugelmann**: seit 30 Jahren Gemüse
        * Gründe für den Umstieg weg von tierischem Dünger
            * EHEC-Skandal
            * **im Zukauf-Dünger sind Haarmehle und Federmehle, die mit oft Keimen und Antibiotika belastet sind**
            * Pflanzenreste 2 - 3 Jahre lagern für guten Dünger bzw. nährstoffreiche Humuserde

### Bio-Hof Franz bei Dresden
* Klee-Pellets. Video: [Biogemüse düngen - Klee-Pellets statt Hornmehl](http://www.br.de/mediathek/video/sendungen/unser-land/klee-duenger-biogemuese-100.html), BR 2017, 5 min, (Klee-Pellets statt Hornmehl-av-594d447f3fb6780012f4e7ea.mp4), tag:offline, tag:class?
    * https://www.br.de/mediathek/video/biogemuese-duengen-klee-pellets-statt-hornmehl-av:594d447f3fb6780012f4e7ea
        * TODO: ... ...
    * [Klee-Pellets als Gemüsedünger](http://biooekonomie.de/nachrichten/klee-pellets-als-gemueseduenger)
    * 2020
        * "KleePura den ersten und einzigen Naturland zertifizierten, veganen Öko-Dünger für Verbraucher auf den Markt gebracht"
            * https://naturland.de/de/partner/3267-gruenerduengen-portraet-2021.html

### 2020: ARD: Eine Welt ohne Fleisch (S01/E02) - Biobauer seit 20 Jahren keine Milchviehhaltung
* https://www.ardmediathek.de/ard/video/eine-welt-ohne/eine-welt-ohne-fleisch/das-erste/Y3JpZDovL2Rhc2Vyc3RlLmRlL2VpbmVXZWx0T2huZS9iMWE0MGRkZi0wOWQ0LTRjZTQtOWVkMC00Zjk4MjQyODRkNGI/ "Eine Welt ohne Fleisch (S01/E02)", 15.11.2020, ARD, 30 min
    * dataDocuments/Aktiv/2020-11-16-ARD-Eine-Welt-ohne-Fleisch-Screenshots/
    * ca 5 min: Klimaschädliches Methan der Kühe
    * 5:30: wie wäre es, wenn wir uns alle rein pflanzlich ernähren würden?
        * Änderung der Anbauflächen; inkl. freiwerden von Flächen
    * ab ca. 7:30: junge, vegane FFF-Aktivistin im vevay in Frankfurt
    * 9:30: "vegan leben ist das eine, vegane Landwirtschaft das andere"
        * Düngung?
        * Hessische Staatsdomäne Frankenhausen, Wissenschaftler
            * "Mist und Gülle waren bisher Teil der **ökologischen Kreislaufwirtschaft**"
                * ((analog dazu: wie mache ich Landwirtschaft ohne Pestizide? **Frage der Ausbildung**))
            * "doch immer mehr Bio-Landwirte wollen auf tierischen Dünger verzichten und brauchen Alternativen"
            * Prof. Jürgen Heß, Ökologischer Land und Pflanzenbau Universität Kassel
                * "das heißt, dass sie weiterhin gute Erträge haben, obwohl sie keine Tierhaltung haben."
                * "Deshalb braucht es an anderes Kreislaufkonzept für die Düngemittel, die in einem solchen Betrieb erzeugt werden können."
            * Forschung, um fundierte Empfehlungen für die Bio-Landwirte geben zu können
    * 10:45: Beispiel eines **deutschen Bio-Bauern, der schon vor 20 Jahren wegen der Tiere die Milchviehwirtschaft aufgegeben hat**
        * screenshot_20201116_193731 - Hans Pfänder - früher Milchbauer - seit 20 Jahren nun nur noch Gemüse; weil Bedenken die Tiere, wenn Milchleistung abnimmt, zum Schlachter zu bringen; und männl. Kälber; Abgeben, Gefühl wurde immer schlimmer
        * screenshot_20201116_194054 - Mutiger Schritt ins Ungewisse; Idealismus; er und später seine Söhne experimentieren mit pflanzlichem Dünger - Legominosen; Fruchtfolge oder zwischen Gemüse und Getreide
        * screenshot_20201116_194258 - eigener Kompost, Pflanzendünger (auch etwas Pflanzenkohle); dafür ausgezeichnet bei einem **Bundeswettbewerb für Ökolandbau**
        * screenshot_20201116_194444 - 30 ha, 1000 t Gemüse pro Jahr; ein gutes Ergebnis im Biolandbau; regionale Vermarktung
        * screenshot_20201116_194551 - alle aus der Familie leben vegetarisch
        * screenshot_20201116_233329 - Pfänder bei Milchvieh konnte nur 1 Familie davon leben; nun mit Pflanzen sind es 3 Familien
        * screenshot_20201116_233415 - und sie versorgen ganz viele Menschen mit Bio-Gemüse
            * ((Thema: **Wirtschaftlichkeit: ja, ist möglich**))

### 2020: Bio-veganer Landbau mit Daniel Hausmann
Praktisches Beispiel:

* https://www.ariwa.org/biovegan/ -> Video Mitte rechts, 3 min
    * **Dünger mittels Kleegras**
        * herkömmlich: Kleegras -> Tierfutter -> Tier -> Dünger
        * bio-vegan: Kleegras -> **Kompost oder Biogasanlage** -> Dünger
    * Bio-Hof übernommen und umgestellt

### 2020: Mutter Natur Österreich
* https://www.mutternatur.at/aktuelles/wo-bohnen-und-ideen-wachsen/, kein Datum, 2020?
    * "Als Vordenker strebt Harald Strassner jetzt den ersten biozyklisch-vegan zertifizierten Landwirtschaftsbetrieb in Europa an. Das bedeutet, dass der Betrieb auf Produktionsebene entlang seiner kompletten Wertschöpfungskette ausschließlich **ohne tierische Dünger auskommt** und auch keine Futtermittelproduktion für die Tierhaltung (Stichwort Fleischproduktion) betreibt. Das alles geht also noch einen ganzen Schritt weiter, als das von vielen Produkten bekannte Vegan-Siegel. [...] Wenn es um Veganismus geht, dürfen vor allem Hülsenfrüchte nicht fehlen. So hat Strassner vor etwa fünf Jahren seinen Bohnenanbau reaktiviert und arbeitet seitdem mit rund zehn Sorten."

### Bio-vegane SoLaWi – Guter Grund e.V., Frankfurt
* https://www.solawi-gutergrund.de/biovegan/
* https://www.solawi-gutergrund.de/ueber-uns/
* https://www.solawi-gutergrund.de/selbstverstaendnis/

### Tolhurst Organic in England
* Stockfree Organic: "Why no animal manures?"
    * https://www.tolhurstorganic.co.uk/about-us/what-is-stockfree-organic/
    * Video: YT: "Living With The Land | Part 6 | Nutztierfreie Landwirtschaft (German Subtitles)", https://www.youtube.com/watch?v=eP9pUtVljP0, 2015, 4 min
        *"Als Pionier einer pflanzenbasierten Landwirtschaft ist Iain Tolhurst seit 1976 ein praktizierender Erzeuger für ökologisch angebautes Gemüse."
        * ... mehr ...

### Griechenland
* in Griechenland: http://www.biocyclic-network.net

Inbox
-----
### 2020: neue Kultur, wo es keinem einfällt, dass Tiere Lebensmittel sind
* Hunde- oder Katzenmilch ist auch kein Lebensmittel
* TODO: Joey Carbstrong zu weggeworfenem Subway-Sandwich mit Fleisch, ca. 2020
    * das Tier ist kein Lebensmittel, von daher ist es hierzulande keine Verschwendung es nicht zu essen
* Film: **Carnage (2017)**, Simon Amstell, 65 min

### 2020: „Braucht der Ökolandbau noch Tiere?“ Prof. Dr. Bernhard Hörning, 2018
* „Braucht der Ökolandbau noch Tiere?“ Prof. Dr. Bernhard Hörning 57. Fortbildungskurs SIGÖL, Bad Düben, 8.11.2018 . Fachgebiet Ökologische Tierhaltung
    * https://naturland.de/images/Erzeuger/Fachthemen/Fachveranstaltungen/Themenuebergreifende_Veranstaltungen/Sigoel_08.11.2018/SIGOEL_2018_11_08_Hoerning-net.pdf
        * offline-material/2020/SIGOEL_2018_11_08_Hoerning-net.pdf
    * 81 Folien
    * viele Zahlen
    * "bio ohne Tier" möglich, aber schwieriger
    * Tierwohlprobleme bei Bio
    * Grünlandnutzung
        * (siehe auch https://de.wikipedia.org/wiki/Graspapier#Reduzierung_von_Zellstoff-Importen)
    * ...

### 2020: Bio-Obst vom Bodensee biozyklisch-vegan
* https://vegconomist.de/food-and-beverage/bio-obst-vom-bodensee-jetzt-auch-biozyklisch-vegan-zertifiziert/
    * "Seit Ende der 90er Jahre gehören Bio-Äpfel fest ins Sortiment der BayWa Obst GmbH & Co. KG"
        * https://www.baywa.de/pflanzenbau_obst/obst/obsthandel/
    * ...check...

### 2018: Erste biozyklisch-vegane Betriebe anerkannt
* https://albert-schweitzer-stiftung.de/aktuell/erste-biozyklisch-vegane-betriebe-anerkannt
    * "Obstbaubetrieb »Biolandhof Hund« aus Meckenbeuren in Baden-Württemberg"
    * PfalzBio GbR, siehe oben

Hintergrund
-----------
* "Bio-veganer Landbau: eine forder- und förderungswürdige Alternative": https://albert-schweitzer-stiftung.de/aktuell/bio-veganer-landbau (2014 - 2017)
* Siehe hier https://albert-schweitzer-stiftung.de/themen/landwirtschaft
* "Rezension: Peaceful gardening" - https://albert-schweitzer-stiftung.de/aktuell/rezension-peaceful-gardening
* ["Gemüse ohne Tier: Wie vegane Landwirtschaft funktioniert"](https://ich-lebe-vegan.de/veganer-lifestyle-vegan-leben/vegane-landwirtschaft/)

### Informationsportal Biozyklisch-veganer Landbau
* https://biozyklisch-vegan.org
    * Das FAQ beinhaltet Fragen zu Kreislaufwirtschaft und Dünger: https://biozyklisch-vegan.org/faq

### biovegan.org offline
* http://biovegan.org/
* http://biovegan.org/forum/
* 2017: "Vegan zertifizierte Bio-Früchte ab 2017 erstmals im LEH" - http://biovegan.org/vegan-zertifzierte-bio-fruechte-ab-2017-erstmals-im-leh/
* Wie soll man ohne Tiere richtig düngen? - Lösungen allgemein
    * http://biovegan.org/infopool/nahrstoffe-und-bodenhilfsstoffe
    * Terra Preta, Schwarzerde
        * https://utopia.de/0/blog/veggie-tag/schwarze-erde-wiederentdeckung-einer
            * https://utopia.de/0/gutefragen/fragen/wie-viele-km-biolandbau-sind-noetig-um#15819%3C/a%3E
        * http://www.bund-bergstrasse.de/themen_und_projekte/terra_preta_schwarzerde/

Entwicklungen
-------------
### 2018
* ["Vegane Landwirtschaft als Standard anerkannt"](https://albert-schweitzer-stiftung.de/aktuell/vegane-landwirtschaft-standard-anerkannt)
    * http://biozyklisch-vegan.de/biozyklischvegan/2017/12/aufnahme-in-die-ifoam-familiy-of-standards-ein-meilenstein/
    * [IFOAM](https://de.wikipedia.org/wiki/Internationale_Vereinigung_der_%C3%B6kologischen_Landbaubewegungen)
        * seit 1972
        * "Die IFOAM hat, neben anderen Aktivitäten, einen Standard für ökologische Landwirtschaft und ein Rahmenwerk für Öko-Zertifizierungen entwickelt, welches richtungsweisend für z. B. die EG-Öko-Verordnung war."

### 2017
* ["Themenabend zur biozyklisch-veganen Landwirtschaft"](https://albert-schweitzer-stiftung.de/aktuell/themenabend-biozyklisch-vegane-landwirtschaft)
    * "Die Bedeutung des Begriffs „biozyklisch“" - http://biozyklisch-vegan.de/anbaurichtlinien/
    * http://biozyklisch-vegan.de
    * http://www.biocyclic-network.net
    * http://pfalzbio.de
* "Nachhaltige Landwirtschaft und die Zukunft tierischer Nahrungsmittel", 2-Tagesseminar der Evangelischen Akademie, Lutherstadt Wittenberg
    * [Programmheft](http://ev-akademie-wittenberg.de/sites/default/files/downloads/veganelandwirtschaft_web_0.pdf)
        * "Probleme der Produktion tierischer Nahrungsmittel für Klima, Biodiversität, Böden, Gewässer, Stickstoff- und Phosphorkreisläufe"
        * "Bedingungen des sozialen und individuellen Wandels beim Fleischkonsum"
        * "Welternährung mit Ökolandbau, tierische Lebensmittel weitgehend aus Weidewirtschaft - Chancen und Grenzen"
        * "Einstellungen veganer Verbraucher zur landwirtschaftlichen Tierhaltung"
    * http://ev-akademie-wittenberg.de/veranstaltung/vegane-landwirtschaft

Fragen
------
### Wie soll man ohne Tiere richtig düngen?
* Die Dimension des aktuellen Problems zeigt sich z. B. hier
    * ["Tiermehl als Phosphatquelle nutzen"](http://biooekonomie.de/node/6875), 2015
        * "Phosphat ist nicht nur für den Menschen ein lebenswichtiger Baustein. Das Salz ist neben Stickstoff der wichtigste Pflanzennährstoff. Da Deutschland über keine eigenen natürlichen Ressourcen verfügt, muss der Rohstoff in großen Mengen aus dem Ausland importiert werden, wo er in Bergwerken abgebaut wird. Der mit Abstand größte Teil des kostbaren Minerals landet hierzulande als Dünger auf den Äckern. Eine bisher vernachlässigte Phosphatquelle ist Tiermehl, das aus den Abfällen der Schlachthöfe gewonnen und anschließend wieder verfüttert oder verbrannt wird."
    * Warum Mist Mist ist: https://utopia.de/0/blog/fair-flechten/warum-mist-mist-ist-gedanken-und-zitate-zum-welt-boden-tag

### Terra Preta
* https://de.wikipedia.org/wiki/Terra_preta
* https://de.wikipedia.org/wiki/Terra_preta#Potential_zur_effizienten_Ressourcennutzung
* https://permaculturenews.org/2010/05/25/back-to-the-future-terra-preta-%E2%80%93-ancient-carbon-farming-system-for-earth-healing-in-the-21st-century/
* ["Schwarze Wundererde? - Was Pflanzenkohle im Garten tatsächlich leisten kann"](https://www.nabu.de/umwelt-und-ressourcen/oekologisch-leben/balkon-und-garten/gartentipps/20401.html)

### Was ist mit den Regenwürmern?
* Alles schön und gut, aber was ist mit den Regenwürmern, die durch den Spaten umkommen?
    * Wenn konventionell gewirtschaftet wird, dann sind die Regenwürmer sowieso tot, wegen der Chemie
        * Lösung: [Bio](../Bio)
    * Prinzipiell ist Leid nicht gegeneinander aufwägbar, aber man kann einen Schritt nach dem anderen machen

### Sind Tiere für eine nachhaltige Landwirtschaft unbedingt notwendig?
* "Huhn und Schwein. Muss das sein?"
    * "Huhn und Schwein - lass es sein"
    * Es gibt derzeit keinen sachlogischen Grund für die Haltung von Hühnern und Schweinen in größerer Menge
* 2017:
    * Die meisten Umweltschutzorganisationen und Bioverbände sagen, dass man zumindest Rinder in größerer Menge braucht.
        * z. B. Düngerkreislauf
        * Kann man daraus folgern: "Huhn und Schwein. Muss das sein?"
    * http://felix-ekardt.eu/
        * Forschungsstelle Nachhaltigkeit und Klimapolitik
        * spannendes Aufgabengebiet
            * "Die FNK widmet sich in Leipzig und Berlin Projekten für öffentliche oder gemeinnützige Auftraggeber [...] und einer intensiven Grundlagenforschung zur Nachhaltigkeit, dokumentiert in vielen Publikationen. Ebenso eine große Rolle spielen die Politikberatung auch ohne Projektbezug, eine breite Vortragstätigkeit [...] sowie unsere Medienarbeit."
        * Buch ["Jahrhunderaufgabe Energiewende - Ein Handbuch"](https://www.politische-bildung.nrw.de/imperia/md/content/e-books/jahrhundertaufgabe-energiewende.pdf), 2014
        * Taschenbuch "Wir können uns ändern" erschienen, 2017
            * [Autorenprofil](https://www.oekom.de/index.php?id=2250), unten
        * weitere Themen auf der Webseite
        * ["Terrorangst: Wieder über die anderen Katastrophen reden"](http://www.zeit.de/politik/2017-03/terrorangst-ablenkung-attentaeter-ordnung-untergrabung/komplettansicht), Gastbeitrag in der ZEIT, 2017
            * "Terroranschläge als Dauerthema begünstigen Angst, Illiberalität und neue Anschläge – und überlagern größere Gefahren. Für die wir dann keine Kapazität mehr haben."
* Welche Tiere in der Nutztierhaltung sind für eine nachhaltige Landwirtschaft notwendig?
    * Vermutlich nur Rinder (Schafe und Ziegen); und zwar für Weideflächen und als Düngelieferant; nicht als Milch- oder Fleischproduzenten
        * Frage: was ist mit dem Rinder-Methan und dem Klima?
        * todo: Wieviel Prozent des produzierten oder konsumierten Fleisches stammt aus "vorbildlicher Weidetierhaltung" (also auf richtigen Flächen und mit guten Management)?
            * konkrete Beispiele?
            * todo: Wieviel könnten es werden, wenn alle Flächen genutzt würden, die dafür infrage kämen?
        * Idee: Das Ergebnis ist vermutlich: ca. 5 %. Das heißt der Fleischkonsum müsste auf 5 % reduziert werden.
            * Fleisch käme dann nur noch Sonntags oder zu Festlichkeiten auf den Tisch.
            * Ein breit gefächertes, überall verfügbares, schmackhaftes und günstiges veganes Angebot ist zu entwickeln, um die Differenz auszugleichen.
                * Die Folge wäre, dass veganes Essen überall wie selbstverständliche verfügbar wäre und man dadurch als veganer Gesellschaftsteilnehmer überall voll akzeptiert würde.
        * ["Veganes Blutvergießen und die Weidefleisch-Fiktion"](https://friederikeschmitz.de/veganes-blutvergiessen-und-die-weidefleisch-fiktion/), 2014
            * ...
            * "Olschewski behauptet natürlich, solche Tierprodukte nicht zu meinen. Der Fleischesser, der zum Vergleich herangezogen werden soll, müsste also ausschließlich Weidefleisch, Weidemilch und Weideeier (?) verzehren. Es ist allerdings nicht besonders glaubwürdig, dass Olschewski selbst für eine solche Ernährung eintritt: Auf seinem Blog gibt es zum Beispiel zahlreiche Rezepte mit allerlei verschiedenen Tierprodukten, bei denen nie dazu gesagt wird, dass die aus derartiger Erzeugung stammen sollten."
            * "dass sie zwar ab und an solches Fleisch kaufen, faktisch aber – im Alltag, bei Freunden, im Restaurant – ständig auch Massentierhaltungsfleisch konsumieren."
            * "Nach Korrektur des Rechenfehlers in dem genannten Journalartikel kommt der Autor Matheny zu dem Schluss, dass pro Proteinmenge bei Getreide weniger Tiere zuschaden kommen als bei Weidefleisch – und das bei industrieller Getreideproduktion."
            * "Aus Umweltperspektive ist auch die Weidewirtschaft hoch problematisch und führt oft zur Zerstörung von Böden und Ökosystemen. Olschewski gesteht das ein, meint aber, ein gutes Management könnte das verhindern. Das mag sein, aber wie lässt sich wissen, wo es gutes Management gibt, und **wie kann man Weidefleisch allgemein loben, wenn sich das nicht sicher eruieren lässt?**"
            * "Zuletzt ist noch ein entscheidender Faktor, dass **vielerorts zahllose Wildtiere brutal getötet werden, weil sie die Weidewirtschaft stören** – in den USA tötet der sog. Wildlife Service u.a. zum Schutz der „Nutztier“-Haltung 1,5 Millionen Wildtiere pro Jahr."
            * ...
            * "An echtes Weidefleisch zu kommen, ist eine gewisse Herausforderung und kostet entsprechend. **Wenn man nur dieses Fleisch essen wollte, dann isst man schonmal mindestens im Restaurant, unterwegs und in der Kantine vegan.** Die Frage ist: Wenn man daneben Aufwand und Kosten investieren will, um sich für zuhause Weidefleisch zu besorgen – und dabei noch nachzuprüfen, ob es auch aus gutem Management stammt – dann sollte man den gleichen Aufwand und die gleichen Kosten doch besser in möglichst ökologisches Getreide und Gemüse stecken, das mit ziemlicher Sicherheit eine bessere Bilanz hat."
            * "dass wir es nicht beim Anders-Konsumieren bewenden lassen sollten: Eine grundlegende Wende in der Landwirtschaft wird sich nicht durch veränderten Konsum erreichen lassen, sondern nur durch **eine starke soziale Bewegung, die direkt Einfluss auf die Produktion nimmt**"
            * "Deshalb gilt es, sich u.a. innerhalb der Tierbefreiungs- und der Umweltbewegung für eine andere Landwirtschaft einzusetzen sowie daneben vor Ort bereits Alternativen z.B. in Form solidarischer Landwirtschaftsgemeinschaften aufzubauen."

Sonstiges
---------
### Lupine
* todo
