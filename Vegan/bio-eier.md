Bio-Eier
========

<!-- toc -->

- [Was ist das Problem bei Eiern, auch Bio-Eiern?](#was-ist-das-problem-bei-eiern-auch-bio-eiern)
  * [2020: Beispiel Biobetrieb, der Legenhennenhaltung einstellte](#2020-beispiel-biobetrieb-der-legenhennenhaltung-einstellte)
  * [Doku zu Massenware (auch Bio) aus NL und D für deutsche Ketten](#doku-zu-massenware-auch-bio-aus-nl-und-d-fur-deutsche-ketten)
  * [utopia-Zusammenstellung Kaufberatung](#utopia-zusammenstellung-kaufberatung)
  * [Ausgewählte Zahlen](#ausgewahlte-zahlen)
  * [Generell](#generell)
  * [Discounter etc.](#discounter-etc)
- [Kükenschredderei / Vergasen](#kukenschredderei--vergasen)
- [Brütereien für Produktion von Legehennen](#brutereien-fur-produktion-von-legehennen)
- [Schlachtungen nach Produktivitätsnachlass](#schlachtungen-nach-produktivitatsnachlass)
- [Elterntiere: Leiden und weniger strenge Gesetze](#elterntiere-leiden-und-weniger-strenge-gesetze)
- [Turbohennen](#turbohennen)
- [So sieht artgerechte Haltung aus](#so-sieht-artgerechte-haltung-aus)

<!-- tocstop -->

* Nicht verwechseln: [Bio-Landwirtschaft](../Bio) ist im Durchschnitt umweltschonender als konventionelle Landwirtschaft.
    * ABER: Tierhaltung mit dem primären Ziel des Erzeugens von Milch, Eiern und Fleisch, führt auch in Bio-Betrieben zu Zuständen, die nicht als artgerecht zu bezeichnen sind.
    * Ausweg/Übergangslösung: Tierhaltung ohne planmäßige Schlachtungen für Tierprodukte, sondern die Tierprodukte als netten Zugewinn ansehen (siehe Geisehof Südschwarzwald)

Was ist das Problem bei Eiern, auch Bio-Eiern?
----------------------------------------------
### 2020: Beispiel Biobetrieb, der Legenhennenhaltung einstellte
Artikel über einen Bio-Betrieb in Österreich, der aus ethischen Gründen die Legehennenhaltung / Eierproduktion einstellte:

* https://www.bio-stadthofladen.ch/var/m_5/57/575/106005/10651952-BZ_8.8.20_Aufgabe_Eierproduktion-c20ad.pdf?download
    * offline-material/2020/10651952-BZ_8.8.20_Aufgabe_Eierproduktion-c20ad.pdf
* Original-Kunden-Info: https://www.haenni-noflen.ch/var/m_5/57/575/106005/10621043-Info_Eier_2020-c9f0f.pdf?download, Hänni Hofladen
    * offline-material/2020/10621043-Info_Eier_2020-c9f0f.pdf
    * "Bestimmt sind auch Sie ein tierliebender Mensch, genau wie wir!"
    * "Mit unserer Legehennen-Haltung haben wir den höchstmöglichen Stand erreicht,der weit über die geltenden Bio-Richtlinien hinausgeht."
    * "Am Ende einer einjährigen Legeperiode steht aber leiderimmereine Schlachtung völlig gesunder und leistungsfähiger Tiere vor der Türe.
        Seit Jahren belastet uns dieser Umstand emotional enorm."
    * "Wir sind nicht mehr länger bereit, Tiere nach Nutz-und Haustier zu kategorisieren und somit über diese Lebewesen zu bestimmen."
    * "Lesen Sie auf der Rückseite, wie Eier bei vielen Gerichten einfach ersetztwerden können!"

### Doku zu Massenware (auch Bio) aus NL und D für deutsche Ketten
* Doku: "Gefälschte Eier - Wie uns die Industrie austrickst | SWR betrifft", https://www.youtube.com/watch?v=UW-OslczCf0, 2018, 45 min
    * ...
    * ... für die großen Handelsketten braucht es große Liefermengen
        * viel aus NL (und dann in D. verpackt; so steht es dann auf der Packung)
    * Bodenhaltung:     9 Hennen / qm
    * Freilandhaltung:  9 Hennen / qm + 4 qm / Tier Auslauf tagsüber draußen
    * Öko:              6 Hennen / qm, 18 cm Sitzstange, 4 qm Auslauf (https://www.bmel.de/DE/Tier/Nutztierhaltung/_texte/HaltungLegehennen-Bioeier_FAQ_Tierschutz.html;jsessionid=F063E82892A0CB5C973EEC9E6D9D48CA.1_cid385#doc3724080bodyText8)
    * Fiese Tricks bei Bio-Eiern
        * siehe auch http://biowahrheit.de/
    * Vorteil für Betrieb, wenn die Hühner nicht rausgehen
        * Fläche einfacher zu bewirtschaften
        * die Hühner koten nur im Stall
        * und die Eier bleiben im Stall
    * ca. 13 min: krasse Bilder zu "2: Bodenhaltung"
        * ...
        * ... TODO
        * ...
    * ...

* Aufschlussreich: die Startseite des ZDG (Zentralverband der Deutschen Geflügelwirtschaft e.V.)

* https://www.daserste.de/information/wirtschaft-boerse/plusminus/sendung/wdr/plusminus-legehenne-100.html, 2019
    * "Auf "Plusminus"-Bitte um eine Stellungnahme zum Tierwohl in der Bodenhaltung erklärt uns die Deutsche Geflügelwirtschaft:
        "Rechtlich basieren die Haltungsbedingungen auf den Vorgaben der deutschen Tierschutz-Nutztierhaltungsverordnung (…).
        Die Bodenhaltung gewährleistet somit ein Höchstmaß an Tierschutz."

### utopia-Zusammenstellung Kaufberatung
* ["Bio-Eier, Freilandeier, Bodenhaltung – welche Eier soll ich kaufen?"](https://utopia.de/ratgeber/kaufberatung-ei-bio-freiland-eier/)
    * "Hier die 5 wichtigsten Tipps: [...]" klare Ansagen
    * Code 1: Freiland: "Die Auslauffläche sollte größtenteils bewachsen sein, in der Praxis wächst dort aber wegen des Hühnerkotes nicht mehr viel."
    * "Code 2: Bodenhaltung klingt besser, als sie ist -
        „Bodenhaltung“ klingt halbwegs natürlich. Ist sie aber nicht.
        Neun Hennen teilen sich bei Bodenhaltung einen Quadratmeter.
        Nur innerhalb ihrer geschlossenen Ställe dürfen sie sich „frei“ bewegen."
    * ...
    * "Sind Discounter-Eier wirklich Bio?"
    * "Sind Bio-Eier mit Verbandslogo besser?"
    * "Kann ich Ei ersetzen?"
        * "Wegen all der Probleme rund ums Ei verzichten viele Menschen auf Eier.", Rezepte
    * "Eier aus Bodenhaltung oder Käfighaltung sollte man meiden." => Kein Ei mit der 3 oder 2

### Ausgewählte Zahlen
* ca. 30.000.000 Hühner (30 Millionen) werden in Deutschland jedes Jahr im Rahmen der Eierproduktion geschlachtet. (Quelle elsewhere)
* Verteilung (https://www.peta.de/eier-so-leiden-huehner-in-deutschen-haltungssystemen)
    * Käfig:        11,4 %
    * Boden:        64,5 %  ___ zusammen knapp 76 % aus Zuständen, die nur mit Sichtschutz und Täuschung akzeptiert werden
    * Freiland:     15,7 %
    * Bio:           8,4 %  (ca. 2,5 Millionen)


### Generell
* Zunächst muss die Frage geklärt werden, ob der Mensch auch ohne Hühner-Eier leben kann?
    * Interessanterweise findet man zu dieser Frage keine genauen Antworten
        * Hühner-Ei enthält viele Nährstoffe. Die Frage ist, sind diese auch von woanders her zu erlangen.
        * siehe aber [vegane-kost-leben-ohne-eiweiss](http://www.naturheilmagazin.de/natuerlich-leben/ernaehrung/vegane-kost/vegane-kost-leben-ohne-eiweiss.html); bezieht sich aber nicht auf Eiweiß
    * Die Praxis vegan lebender Menschen aus unterschiedlichen Ländern und Kulturkreisen scheint die These zu bestätigen, dass der Mensch auch ohne Hühner-Ei gesund leben kann.
* z. B. http://www.adaptt.org/veganism/chickens-eggs-and-the-free-range-scam.html (Achtung, zeigt auch die unschönen Bilder der Eiproduktion, die der Verbraucher lieber nicht mit seinem Bio-Frühstücksei verbindet)
    * Auch jedes freilaufende Huhn wird vor dem natürlichen Ende seines Lebens geschlachtet (= Gewalt). Es gibt keine Hühner-Lebenshöfe in der Eier-Wirtschaft.

### Discounter etc.
* Video: ["Horror-Eier auch bei Aldi, Edeka, Netto und Rewe"](https://www.animalequality.de/neuigkeiten/horror-eier-auch-bei-aldi-edeka-netto-und-rewe), 2018, 5 min, und Artikel, ::aldi
    * "Hennenqual fürs Horror-Ei"
* Video: ["Bioeier von ALDI Süd: Die Mär von glücklichen Hühnern"](https://www.youtube.com/watch?v=m_5NY15qvP0), SWR, 2017
    * "Marktcheck hat Betriebe besucht, die Bioeier auch für ALDI Süd produzieren. Von Elektrozäunen bis zu herumliegenden Tierkadavern: Ein Skandal wird sichtbar."
    * Alternative: im Tierbereich: Bioland, Naturland etc.

Kükenschredderei / Vergasen
---------------------------
* https://albert-schweitzer-stiftung.de/massentierhaltung/legehennen

Brütereien für Produktion von Legehennen
----------------------------------------
* ["Tierschutzverstöße in Brütereien?"](https://www.zdf.de/verbraucher/wiso/videos/tierschutzverstoesse-in-bruetereien-100.html), ZDF WISO, 2018, 6 min, (bruetereien-100), tag:offline, Mülln
    * "Männliche Küken dürfen offiziell massenweise getötet werden, weil sie für die Eierproduktion nutzlos sind. Weibliche Tiere ohne Grund zu töten ist illegal, doch Tierschützer haben jetzt Hinweise gefunden, dass sich auch daran nicht immer gehalten wird."
    * ...
    * SOKO Tierschutz schleust sich als Mitarbeiterin ein
    * Weiterhin Schnäbelkürzen für Küken, die in den Export gehen (freiwillige Selbstverpflichtung gilt nur für Legehennen für den deutschen Markt)
        * mit fiesem Laserkarusell
        * Akademie für Tierschutz
            * die Amputation ist eigentlich vom Gesetz her verboten, aber es gibt Ausnahmeerklärungen
            * Haltungsbedingungen sollten an Tiere angepasst werden und nicht umgekehrt
    * männliche Küken werden in einem CO2-Automaten vergast
    * **Küken (auch weibliche), die kurz vor Schichtende ausschlüpfen, gehen direkt in den Vergaser, weil halt gleich Feierabend**; das ist sogar ganz offiziell illegal!

Schlachtungen nach Produktivitätsnachlass
-----------------------------------------
* "Muss man auch den Konsum von Bio-Eiern problematisieren?"
    * ja, denn z. B. gilt in der Regel, dass "auch Bio-Legehennen nach ein bis anderthalb Jahren getötet werden, weil ihre Produktivität nachlässt"

Elterntiere: Leiden und weniger strenge Gesetze
-----------------------------------------------
* Artikel: [Das Leiden der Hühnereltern](http://www.spiegel.de/wirtschaft/gefluegelmast-so-leiden-die-huehnereltern-a-1140175.html), 28.03.2017, Spiegel Online
    * "Artgerechte Tierhaltung? Verspricht die Fleischbranche immer häufiger. Doch sie verschweigt, dass Millionen Elterntiere besonders qualvoll leben - mitunter ganz legal. SPIEGEL ONLINE liegen heimlich in Ställen gedrehte Videos vor."
* siehe tiere-artgerecht.md
    * "Frontal 21 - Elterntierhaltung bei Masthühnern" / Wimex
    * "Die moderne Hühner-Landwirtschaft" / Lohmann
    * "Zweinutzungshuhn: Viele Eier und trotzdem Fleisch | Unser Land | BR Fernsehen"
* siehe auch ariwa.org im spenden-atlas

Turbohennen
-----------
* Artikel: ["NABU: Der Fluch der Turbohennen"](https://www.nabu.de/umwelt-und-ressourcen/oekologisch-leben/essen-und-trinken/fleisch/16555.html)
* Artikel: ["Legehennen geht es mies - egal, wie sie gehalten werden"](http://www.sueddeutsche.de/wirtschaft/foodwatch-studie-stressiges-huehnerleben-1.2486724), 2015
    * Bilder:
        * Vergleich Platz pro Quadratmeter
            * 2015-Hühner-Legehennen-Belegungsdichte.png
        * Pro-Kopf-Verbrauch von Eiern "Schaleneier mit Kennzeichnungspflicht" vs. "Verarbeitete Eier ohne Kennzeichnungspflicht)
            * 2015-Hühner-Ei-Verbrauch-Schale-vs.-versteckt.png

So sieht artgerechte Haltung aus
--------------------------------
* Video: ["Chickens & Emus Are Decent People - Gary Yourofsky"](https://www.youtube.com/watch?v=9McJxR5nFEY), 2015, 5 min
* siehe auch [tiere-artgerecht](../Vegan/tiere-artgerecht.md)
