Vegan / Zusatzinfos
===================

<!-- toc -->

- [Aus Sicht eines Kindes: "animal people"](#aus-sicht-eines-kindes-animal-people)
- [Damals aus heutiger Sicht](#damals-aus-heutiger-sicht)
  * [Damalige Verhältnisse](#damalige-verhaltnisse)
  * [Moderne Informationstechnik](#moderne-informationstechnik)
  * [Berufe, die es heute nicht mehr gibt](#berufe-die-es-heute-nicht-mehr-gibt)
  * [Berufe, die heute Übergangslösungen bedürfen](#berufe-die-heute-ubergangslosungen-bedurfen)
  * [Berufe, die bald Übergangslösungen bedürfen](#berufe-die-bald-ubergangslosungen-bedurfen)
- [vegan vs. vegetarisch](#vegan-vs-vegetarisch)
- [2017: Warum essen Menschen anders, als sie sich ernähren sollten?](#2017-warum-essen-menschen-anders-als-sie-sich-ernahren-sollten)
- [2017: ifane.org](#2017-ifaneorg)
- [Palmöl vermeiden](#palmol-vermeiden)
- [Verschiedene Aussagen genauer betrachtet](#verschiedene-aussagen-genauer-betrachtet)
  * ["Bei einem Wurstkonzern sollte man keine vegetarische Wurst kaufen"?](#bei-einem-wurstkonzern-sollte-man-keine-vegetarische-wurst-kaufen)
  * [Vermenschlichung von Tieren?](#vermenschlichung-von-tieren)
  * ["Alles nur ein Trend"? / Psychologie](#alles-nur-ein-trend--psychologie)
  * [Kultur, Identität](#kultur-identitat)
  * [Erläuterung Widerspruch Hund/Kuh/Kultur](#erlauterung-widerspruch-hundkuhkultur)
  * [Was ist Toleranz?](#was-ist-toleranz)
  * ["Uh. Da ist ja Knorpel drin"](#uh-da-ist-ja-knorpel-drin)
  * ["Was kann man denn da noch essen?"](#was-kann-man-denn-da-noch-essen)
  * [Fehlinformationen, u. a. von Urgeschmack](#fehlinformationen-u-a-von-urgeschmack)
  * [Religion](#religion)
  * ["Ersatzreligion"](#ersatzreligion)
  * ["Das ist extrem"](#das-ist-extrem)
  * [Veggie-Day](#veggie-day)
  * [Diskussionsleitfaden](#diskussionsleitfaden)
- [More](#more)

<!-- tocstop -->

Aus Sicht eines Kindes: "animal people"
---------------------------------------
* ["Little Irish girl does not want to have animals chopped up anymore"](https://www.youtube.com/watch?v=Sl9lDtTOE_U), 2 min, engl. mit Untertitel
* ["Little Boy wants his Mommy to Leave the Chicken Alone"](https://www.youtube.com/watch?v=BS2JIPOIhoM), 3 min, YT, 2016
* ["3 year old explains why she doesn't eat fish - So cute..."](https://www.youtube.com/watch?v=qnGGQWfK2J0), 1 min, YT, 2015
    * "Children by nature are opposed to violence and murder. Please stop feeding it to them."

Damals aus heutiger Sicht
-------------------------
### Damalige Verhältnisse
* Wenn wir heute zurückblicken, was sehen wir?
    * Sklaverei, Rassismus schwarz/weiß, Ungleichbehandlung von Mann und Frau, Prügelstrafe
    * eine Gruppe von Menschen, die sich für etwas besseres hält und sich wie selbstverstandlich über eine andere Gruppe erhebt
    * Wie kam es damals jeweils zur Weiterentwicklung der gesellschaftlichen Moral? Politiker, Aktivisten, schrittweise Weiterentwicklung?
        * (Und danach erst Gesetze)
    * Der Kreis des Mitgefühls wurde schrittweise erweitert.

### Moderne Informationstechnik
* Durch moderne Informationstechnologie können sich die Bürger wesentlich schneller als zu irgendeiner Zeit austauschen.
    * Den Widersprüchen der vorherrschenden Moral kommt man viel schneller auf den Grund.
        * zwei künstliche Kategorien:
            * Haustiere (innerhalb des Kreises des Mitgefühls)
            * Nutztiere (nicht im Kreis des Mitgefühls)
        * logisch wäre: Akzeptanz, dass die Kategorien willkürlich sind, diese auflösen und entsprechend handeln
        * Kreis des Mitgefühls logisch erweitern.

### Berufe, die es heute nicht mehr gibt
* z. B. Kutscher

### Berufe, die heute Übergangslösungen bedürfen
* z. B. Arbeiter im Kohle-Tagebau
* Die Gesellschaft sollte diese Menschen nicht allein lassen, sondern beim Übergang in eine andere Tätigkeit helfen!

### Berufe, die bald Übergangslösungen bedürfen
* z. B. Schlachter und Metzger
* Die Gesellschaft sollte diese Menschen nicht allein lassen, sondern beim Übergang in eine andere Tätigkeit helfen!

vegan vs. vegetarisch
---------------------
* vegetarisch (nach akueller Definition; vergleiche vor 100 Jahren) = kein Fleisch essen
* Tieren leiden darüberhinaus - teilweise erheblich - durch folgende Handlungen:
    * Der Konsum von Milchprodukten und Eiern
    * das Kaufen von Leder, Wolle und Daunen (siehe [tiere-artgerecht.md](tiere-artgerecht.md).
    * Einfach mal hinter die Kulissen der Produktion schauen, siehe z. B. [tiere-artgerecht](../Vegan/tiere-artgerecht.md)
    * Siehe z. B. regelmäßige Unfälle mit Mutter-Kühen, denen man geschäftsmäßig die Kälber wegnimmt.
        * Kann man diese Tierkinder auch auf liebevolle Weise wegnehmen, so dass sich die Mutterkuh darüber freut?
    * Die Tierhaltung, die der Mensch praktiziert, ist immer mit Gewalt verbunden - spätestens bei der [Schlachtung](schlachtung.md).
* "vegan" = Tiere als Lebewesen respektieren und so gut wie praktisch möglich umsetzen (z. B. durch weitestgehende Verzicht auf oben genannte Konsumgüter)

2017: Warum essen Menschen anders, als sie sich ernähren sollten?
-----------------------------------------------------------------
"Es müssen Lebensräume geschaffen werden, die über aktive
Bewegungs- und schmackhaft-vollwertige Essensangebo
te den Lebensstil bereits in der Kindheit prägen."

"Mit Information über „richtige Ernährung“ allein wird sich das emotional
e Essverhalten nicht korrigieren lassen. Essen wird nicht über
Information gelernt, sondern durch Training."

"Auf einem Parteitag habe ich mal gelernt, dass Prävention das
teuerste ist, was wir tun können. [...] Ein Jahr Lebensverlängerung
kostet  bei  den  Deutschen  im  Durchschnitt  24  Milliarden  Euro"

Quelle: http://www.uni-kassel.de/upress/online/frei/978-3-89958-682-4.volltext.frei.pdf

von: http://www.upress.uni-kassel.de/katalog/abstract.php?978-3-89958-682-4
("Abenteuer Nahrung - weißt Du was Du isst?")
("Dokumentationsband 16. Witzenhäuser Konferenz 02. bis 06. Dezember 2008")

von: ifane.org: "Lehrveranstaltungen an Universitäten, Fachhochschulen und Schulen"
http://ifane.org/bildung-und-beratung/lehrveranstaltungen-an-universitaten-und-fachhochschulen/#veranstaltungen%202016

2017: ifane.org
---------------
* http://ifane.org/das-institut/team/
* http://ifane.org/das-institut/
* http://ifane.org/

* Seminar: "Vegan von Anfang an – geht das?" - https://www.ugb.de/seminare/fortbildungen/vegan-schwangere-kinder-stillende/ (toter Link)
    * weiteres siehe gesundheit-ernährung.md

* Kurs: "Vegane Bio-Lebensmittel - Trend oder nachhaltige Entwicklung?" - http://www.bioaktuell.ch/fileadmin/documents/ba/Agenda/Agenda_2017/Programm_Vegan.pdf
    * Lustig bei der Anmeldung die Mittagessens-Auswahlmöglichkeiten: [ ] Veganes Menü  oder [ ] kein Essen

Palmöl vermeiden
----------------
* Auch im Sinne der Tiere ist es gut Palmöl zu vermeiden, siehe [Palmöl-Problematik](../Konsum/palmöl.md).

Verschiedene Aussagen genauer betrachtet
----------------------------------------
### "Bei einem Wurstkonzern sollte man keine vegetarische Wurst kaufen"?
* siehe [FR-Interview mit Wursthersteller, der immer mehr fleischloses absetzt](http://www.fr.de/wirtschaft/ruegenwalder-muehle-damit-haben-wir-nicht-im-traum-gerechnet-a-1253054) (2017-03-30)
    * siehe User-Comments:
        * Forderung nach ultimativer Konsequenz ist nicht möglich: "Ich verstehe nicht, warum Sie hier Menschen, die auf Fleisch und Wurst verzichten, beschimpfen. Wieweit wollen Sie denn mit Ihrer Trennung in Gut und Böse gehen? - Darf der Veggie-Hersteller Teil eines Konzerns sein, der auch Fleischprodukte herstellt? - Darf es in der selben Ladentheke liegen? etc.)
        * "jedes nicht gegessene Schnitzel ein Gewinn für die Umwelt und die Tierethik."
    * generell gilt: vegan ist in allen Aspekten dem "nur" vegetarischen überlegen; was man draus macht, ist jedem selber überlassen
    * Praxis-Tipp: mal vom Wurstkonzern probieren ist vertretbar, solange es nicht zur Regel wird. Rechtzeitig nach ([bioveganen](biovegan.md)) Alternativen suchen

### Vermenschlichung von Tieren?
* nein, siehe z. B. [Hey Veganer, ihr vermenschlicht Tiere!](https://www.youtube.com/watch?v=uzgYtyMhujg), 2013, (2 min)
    * Rechte werden auf Basis von Bedürfnissen gewährt
    * Beispiele aus der Geschichte
        * "mehr Rechte für Frauen ist ja auch nicht deren Vermännlichung." :)

### "Alles nur ein Trend"? / Psychologie
* z. B. http://der-artgenosse.de/hey-veganer-das-ist-nur-so-ein-trend/ (4 Minuten)
    * "Fleischessen ist eben auch **identitätsstiftend**", weswegen man negative Aussagen dazu nicht gerne hört.
        * siehe auch [Psychologie](../Hintergrund/psychologie.md)
        * ggf. versuchen etwas zu suchen, das weniger mit Gewalt verbunden ist
    * vegan vs. gesund
    * "Weiterentwicklung unserer Ethik"
* Artikel: ["Du bist, was du isst? Psychologische Forschung zum Fleischkonsum"](http://www.bpb.de/apuz/262265/du-bist-was-du-isst?p=all), Bundeszentrale für politische Bildung, 2018
    * ...
    * "Neben Konservatismus gehört dazu auch eine soziale Dominanzorientierung – das heißt, dass eine Hierarchisierung gesellschaftlicher Gruppen unterstützt und begrüßt wird – und eine Tendenz zum Autoritarismus. Diese Einstellungen sind relevant und positiv mit der Häufigkeit des Fleischkonsums assoziiert."
    * "essen dabei nicht nur Fleisch, weil es ihnen gut schmeckt, sondern bringen damit ihren Glauben an soziale Hierarchien und die menschliche Überlegenheit über Tiere zum Ausdruck."
    * ...

### Kultur, Identität
* "Der Mensch (Europäer) hat schon immer Fleisch gegessen. Das (feierliche?) Essen von Tieren ab und an gehört dazu"
    * Frage: Definieren wir unsere kulturelle Identität darüber, (alleine oder in Gesellschaft) zumindest ein bisschen Fleisch zu essen?
        * Muss jeder Mensch (Europäer) das tun, um richtig dazuzugehören?
        * Könnte man auch etwas anderes im feierlichen Rahmen essen, ohne dass man seine kulturelle Identität verliert?
        * Wie stark gehört Respekt vor Schwächeren, die sich möglicherweise sogar nicht selber äußern können, zu unserer kulturellen Identität? / Mitgefühl?
            * zu unserem Gerechtigkeitsempfinden?
        * Könnte unsere soziale Gesellschaft auch ohne Fleisch gut funktionieren?
* Ist die aktuell dominierende Produktionsweise von Tierprodukten zu kritisieren?
    * Wenn ja: Warum ist Totalverzicht nicht ein probates Mittel, dem Missstand Ausdruck zu verleihen?
        * Wenn probates Mittel: sehr schön, unterstützenswert? Warum nicht nachahmenswert?
        * Wenn kein probates Mittel:
            * **Wieviel müsste man mindestens selber kaufen und wo und bei welchen Gelegenheiten sollte man wieviel mitessen, damit es noch akzeptable wäre,
                aber dennoch dem aktuellen Problem gerecht wird?**

### Erläuterung Widerspruch Hund/Kuh/Kultur
* established: Welche Tiere gegessen werden sollen/dürfen und welche im Kreis des Mitleids verweilen dürfen, ist kulturbedingt.
    * Wenn jemand sich für einen Hund (Haustier) einsetzt, warum tut er das?
    * Wenn jemand sich für einen Kuh (Nutztier) einsetzt, warum tut er das?
    * Wenn einen Hund isst (andere Kultur), warum fühlt er keine Veranlassung zu Mitleid?
    * Sagt dieses Verhalten nicht mehr über die menschliche Denkweise aus als über die Situation des jeweiligen Hundes?

### Was ist Toleranz?
Antwort auf die Frage, ob "der Veganer" nicht mehr Toleranz gegenüber der traditionellen Gewalt gegenüber Tieren haben sollte.

* https://www.youtube.com/watch?v=XwUiN4hwoqY, 3:30 min, Der Argenosse, 2014
    * Tolerismus (= Einfordern des Duldens unnötiger Gewaltausübung; Kritik daran, (unnötige) Gewalt nicht zu akzeptieren)
    * Beim gegenseitigen Respektieren sollte man Dritte (z. B. Opfer von Gewalt einer Handlung) nicht außer acht lassen.
        * Beispiel: Schläger-Typen, deren "Sozialverhalten" ja auch zu Recht angeprangert wird
    * Gewalt und Ablehnung von Gewalt steht nicht auf derselben Stufe
        * Beispiel: Es gibt Alternativen zu Fleisch; es gibt aber keine (sinnvolle) Alternative zu einem unversehrten Leben
* http://www.gutefrage.net/frage/ueber-tolerismus-und-toleristen
    * Toleranz nicht mit Ignoranz verwechseln

### "Uh. Da ist ja Knorpel drin"
* Ja, das gehört zum Package dazu.
    * Man kann sich nicht immer die Filet-Stücke raussuchen.
    * Man kann auch Demut zeigen und alles mitessen.

### "Was kann man denn da noch essen?"
* siehe Einsteiger-Tipps

### Fehlinformationen, u. a. von Urgeschmack
* http://der-artgenosse.de/weidehaltung-auf-der-autobahn-und-blutruenstige-pflanzenfresser/
* https://friederikeschmitz.de/veganes-blutvergiessen-und-die-weidefleisch-fiktion/, siehe auch allgemein-zielgruppen.md

### Religion
* Siehe "Die Würde des Tieres ist unantastbar: Eine neue christliche Tierethik"
* ["Why all Christians should go vegan"](https://www.washingtonpost.com/posteverything/wp/2017/01/05/why-all-christians-should-go-vegan), The Washington Post, 2017
    * "Franklin Graham, the son of pastor Billy Graham and heir to his evangelical empire, has gone vegan. That’s right: Perhaps the US’s best-known evangelical leader has stopped consuming animal products. If that statement seems too outrageous to be believed, see his Facebook announcement for yourself."
    * ...
* "Ich bin Gläubiger der Religion X und deswegen mache ich Y", z. B. Tiere konsumieren
    * Frage: Welcher Aspekt der Religion X zwingt zum Tiere konsumieren?
        * Aussagen aus der heiligen Schrift? ("kann" oder "muss"-Aussagen?)
        * Steht in der Bibel, dass man Tiere essen _muss_?
        * Was ist mit der Goldenen Regel?
        * Mit Mitleid mit Schwächeren?
    * Frage: Gibt es auch Menschen, die der Religion X angehören, die vegan leben? Stehen diese Menschen mit ihrer Religion im Widerspruch?
    * Kannst du dir vorstellen, dass es welche gibt oder kennst du vielleicht auch Christen, die vegan leben?
        * Hat der Glaube etwas damit zu tun wie man im Alltag andere Menschen beauftragt Tiere zu behandeln?
* Aussage einer Person: "Ich bin Christ und ich kaufe nur Fleisch der Weide von nebenan." / "In der Bibel steht genau, welche Tiere man essen darf."
    * Es gibt auch Christen, die Massentierhaltungsfleisch kaufen. Und es gibt Christen, die vegan leben. Siehe unten. An der Religion kann man tierproduktfreie/ethische Ernährung also nicht
    festmachen.
        * FRAGE: Was würdest du zu einem (ausgewachsenen) Mitchristen sagen (sozusagen von Christ zu Christ), wenn dieser Mensch regelmäßig Tierprodukte aus der Massentierhaltung konsumiert? Gibt es eine Schwelle, ab der du die Bewahrung der Schöpfung inkl. ihrer Lebewesen verletzt siehst und du deswegen aufbegehren würdest?
    * So wie die Indianer: viel Fläche und nur wenig Fleisch und sich beim Tier bedanken.
        * Der Indianer sieht sich mit der Natur verbunden. Als Teil der Nautr. Ist das vereinbar mit dem Bild Mensch auf der einen Seite und Tiere und Natur auf der anderen Seite zu dessen Nutzen?
        * Der Indianer lebt in einem harten Umfeld. Tiere sind notwendige Opfer. Was würde der Indianer machen, wenn er plötzlich die Möglichkeit hätte, die Tiere nicht mehr zu behelligen?
        * Umgekehrt: Was haben wir Bewohner eines Industrielandes in einer globalisierten Welt mit den Indianern gemeinsam?
            * Tauschen wollen? Oder einfach ein Nahrungsergänzungsmittel nehmen, so wie es viele Omnivore auch tun.
                * Es gibt sehr, sehr viele Nahrungsergänzungsmittel auf dem Markt. Das wenigste davon ist für Vegetarier und Veganer.
* Die Bibel
    * "in der Bibel steht, welche Tier der Mensch essen darf"
        * Steht da auch, dass er sie essen _muss_?
    * Ähnlich: Frauenrechte in der Bibel
        * ["Bibelzitate zur Herrschaftslegitimation des Mannes"](https://www.uni-muenster.de/FNZ-Online/sozialeOrdnung/haus_familie/quellen/bibel.htm), 2003
            * "Und zum Weibe sprach er: Ich will dir viel Mühsal schaffen, wenn du schwanger wirst; unter Mühen sollst du Kinder gebären. Und dein Verlangen soll nach deinem Manne sein, aber er soll dein Herr sein."
            * "Einer Frau gestatte ich nicht, dass sie lehre, auch nicht, dass sie über den Mann Herr sei, sondern sie sei still. Denn Adam wurde zuerst gemacht, danach Eva. Und Adam wurde nicht verführt, die Frau aber hat sich zur Übertretung verführen lassen."
        * [Blog 2013](https://giordanobrunostiftung.wordpress.com/2013/03/11/margot-kasmann-die-bibel-fordert-wie-selbstverstandlich-die-gleichberechtigung-von-mann-und-frau/)
            * User-Comment: "[...] So etwas findet man in vielen antiken Werken, unter anderem in der Ilias, den altisländischen Sagas, den Sagen der alten Syrer und alten Inschriften der Maya. Aber niemand verkauft die Ilias als Fundament unserer Ethik. Genau hier liegt das Problem. Die Bibel wird als Leitfaden für die Lebensführung angepriesen und gekauft."
        * ["Mann und Frau – und wie die Bibel sie sieht"](http://bibelarbeit.privat.t-online.de/testament/neues/1_Petrus2_18_3_7.pdf), Datum?
        * In der obigen Logik sollte ein Christ deswegen die Errungenschaften im Bereich Frauenrechte anzweifeln?
s* Was ist mit (katholischen) Christen, die vegan leben? Schließt sich das aus?
    * siehe z. B. http://www.christen-fuer-tiere.de/botschafter_innen (Christen, die vegan sind)
* Freikirchen
    * oft starke Fokussierung auf Bibeltext (strafender Gott vs. Mainstream: liebender Gott)
* https://de.wikipedia.org/wiki/Konziliarer_Prozess
    * "gemeinsamen Lernweg christlicher Kirchen zu Gerechtigkeit, Frieden und Bewahrung der Schöpfung"
    * 1983
    * 1990:
        * "Grundüberzeugung VII: „Wir bekräftigen, dass Gott die Schöpfung liebt. Gott, der Schöpfer, ist der Ursprung und der Erhalter des ganzen Kosmos. Gott liebt die Schöpfung […] Da die Schöpfung von Gott ist und seine Güte die ganze Schöpfung durchdringt, sollen wir alles Leben heilig halten […] Wir bekräftigen, dass die Welt als Gottes Werk eine eigene Ganzheit besitzt und dass Land, Wasser, Luft, Wälder, Berge und alle Geschöpfe, einschließlich der Menschen, in Gottes Augen ‚gut‘ sind […]“"
        * Umwelt: "Grundüberzeugung VIII: „Wir bekräftigen, dass die Erde Gott gehört. Das Land und die Gewässer bedeuten Leben für die Menschen [...] Wir verpflichten uns außerdem, den ökologisch notwendigen Lebensraum anderer Lebewesen zu achten."
    * KEINE Religion zwingt den Gläubigen Tierprodukte zu essen.
* Vorwürfe: "Religiöse Inbrunst. Ernährung als dominierender Lebensinhalt."
* https://www.jvs.org.uk/ - Jewish Vegetarian Society
    * "JVS is an international charity dedicated to promoting a kinder world, without killing animals for food."
    * https://en.wikipedia.org/wiki/Jewish_vegetarianism
    * ["Rabbis call on Jews to adopt a vegan diet"](https://www.thejc.com/news/uk-news/rabbis-call-on-jews-to-adopt-a-vegan-diet-1.445042), 2017
    * ["Vegan is the new kosher, rabbis worldwide declare"](https://www.jvs.org.uk/2018/05/23/vegan-is-the-new-kosher-rabbis-worldwide-declare/), 2018
    * ["online plant-based parenting course"](https://www.jvs.org.uk/2018/07/06/new-free-online-plant-based-parenting-course-launches/), 2018
* http://www.interfaithveganalliance.org/ - Interfaith Vegan Alliance

### "Ersatzreligion"
* ["Vegane Kost als Ersatzreligion - Radikalisierung der Ernährung schreitet munter voran"](https://www.aerztezeitung.de/medizin/krankheiten/neuro-psychiatrische_krankheiten/?sid=954963), 2018
    * ...
    * User-Comments
        * "Ungültige Verknüpfung, unklare Trennung Vegan / Vegetarisch"
            * "Ich frage mich allerdings was das ganze mit einer veganen Ernährung zu tun haben soll? Man gewinnt beim Lesen den Eindruck alle Veganer/-innen hätten ein großes Risiko eine Essstörung mit Untergewicht zu entwickeln und sozial zu Vereinsamen. Das halte ich für vollkommen verfehlt."
            * "[...] macht keinen Sinn wenn in der Überschrift doch die vegane Ernährung als "Radikalisierung" und "Ersatzreligion" ins Visier genommen wird. "
            * "Aus eigener Erfahrung kann ich berichten das Einladungen zu Partys und Feiern kein Problem bei veganer Ernährung darstellen - fast immer trifft man heutzutage auf Akzeptanz und immer häufiger wird mit Mühe, Liebe und Sorgfalt etwas entsprechend veganes zu Essen bereitgestellt (vegetarisches Essen dagegen ist ja generell sowieso schon Standard)."
        * "Wo ist die Ideologie?"
            * "Jenseits der ernährungsmedizinisch diskussionswürdigen Aspekte veganer Ernährung ist es argumentativ ein grobes Foul, vegane Ernährung als "Ideologie" zu diskreditieren"
            * "[...] Und damit ist die Auffassung, es wäre moralisch legitim, Tiere zu töten und zu essen ebenso eine "Ideologie" wie der Veganismus."
            * "Diese unlautere Verwendung des Begriffs stammt aus der (schäbigen) politischen Diskussion (Demokratie ist per definitionem genauso Ideologie wie Stalinismus oder Konservatismus oder sonst eine Weltanschauung) - in wissenschaftlich-medizinischem Kontext wie hier sollte man darauf doch verzichten und sich auf der Sachebene bewegen."
            * "Ernährungsmedizinisch ist die kategorische Gegenüberstellung von "Veganismus" vs. "Gesunder Ernährung" ebenfalls Unsinn und absurd. Eine vegane Ernährung kann genauso gesund oder ungesund sein wie eine nicht-vegane Ernährung."
            * "Grüße aus der Ernährungsmedizin, Martin Smollich (kein Veganer)"

### "Das ist extrem"
* Comic: http://der-artgenosse.de/wp-content/uploads/2017/04/extrem-1024x577.png
* Leseempfehlung
    * https://de.wikipedia.org/wiki/Extremismus
    * http://www.bpb.de/politik/extremismus/
    * Extremismus ist also in der landläufigen Definition meist eng mit Gewalt verbunden. Eine vegane Einstellung ist ungefähr das Gegenteil.

### Veggie-Day
* "Ich esse nur Fleisch von der Weide von nebenan. ... Und ich bin gegen den VeggieDay."
    * Warum gegen Veggie-Day? Er wäre doch gar nicht betroffen, denn in 99 % der Kantinen gibt es kein Fleisch von der Weide von nebenan.

### Diskussionsleitfaden
* [VEBU-Diskussionsleitfaden mit vielen spannenden und immer wiederkehrenden Argumenten](http://www.theologische-zoologie.de/uploads/media/M_16d_VEBU_LeitfadenDiskussionen.pdf)
    * Webversion: https://vebu.de/los-gehts/fleisch-oder-pflanze/diskussionsleitfaden/
* https://www.veganblog.de/verschiedenes/10-argumente-gegen-eine-vegane-ernaehrung/

More
----
[more](more.md)
