Vegan / daily-choices-info-package
==================================

[zurück](../../..)

Warum eher Richtung vegan und warum das mehr Vor- als Nachteile hat.

Die zugrundliegende Ethik ist hierbei: "Gehe mit anderen so um, wie du selber behandelt werden willst" (unnötige Gewalt vermeiden)

<!-- toc -->

- [Warum vegan?](#warum-vegan)
  * [Tierwohl / Ethik](#tierwohl--ethik)
  * [Welternährung](#welternahrung)
  * [Energieeffienz](#energieeffienz)
  * [Umweltschutz 2021 - Studien und Uni-Speisepläne](#umweltschutz-2021---studien-und-uni-speiseplane)
  * [Umweltschutz](#umweltschutz)
  * [Gesundheit](#gesundheit)
  * [Videos für Einsteiger](#videos-fur-einsteiger)
  * [Dokumentationen](#dokumentationen)
- [Wie vegan?](#wie-vegan)
  * [Einsteiger-Tipps](#einsteiger-tipps)
  * [Ernährungspyramide](#ernahrungspyramide)
  * [Für fast jeden geeignet: Vegan für Kraftsportler](#fur-fast-jeden-geeignet-vegan-fur-kraftsportler)
  * [Vegan Advocacy](#vegan-advocacy)
- [Illustrationen](#illustrationen)
  * [Der Mensch und die Tiere](#der-mensch-und-die-tiere)
  * [Ethik des Fleischessens](#ethik-des-fleischessens)
- [Positive Beispiele](#positive-beispiele)
- [Vegan vs. Freilandeier von Oma](#vegan-vs-freilandeier-von-oma)
  * [Situation: Kuchenbüffet mit Kuchenspenden und Kaffee mit Milch](#situation-kuchenbuffet-mit-kuchenspenden-und-kaffee-mit-milch)
- [Risikokommunikation](#risikokommunikation)
  * [Bundesinstitut für Risikobewertung (BfR), 2017](#bundesinstitut-fur-risikobewertung-bfr-2017)
  * [Vegan in der Schwangerschaft? - Keine Evidenz für eine Nichtempfehlung](#vegan-in-der-schwangerschaft---keine-evidenz-fur-eine-nichtempfehlung)
- [Vitamine / B12](#vitamine--b12)
- [Zusatzinfos](#zusatzinfos)
  * [Vegan society: Ripened by human determination](#vegan-society-ripened-by-human-determination)
  * [Selber aktiv werden](#selber-aktiv-werden)
  * [Utilitarismus](#utilitarismus)
  * [Rechtliche Situation](#rechtliche-situation)
  * [Mögliches Fazit für Einsteiger](#mogliches-fazit-fur-einsteiger)
  * [Antibiotika](#antibiotika)
  * [Soja-Import](#soja-import)
- [Bildungsangebote](#bildungsangebote)
  * [Verein Mensch Tier Bildung e.V.](#verein-mensch-tier-bildung-ev)
  * [Mensch-Tier-Beziehung von Frankfurt-Zirkus.org](#mensch-tier-beziehung-von-frankfurt-zirkusorg)
- [Inbox 2020](#inbox-2020)
  * [Naturland: Veganer und Tierhalter](#naturland-veganer-und-tierhalter)
  * [gemeinsam-gegen-die-tierindustrie](#gemeinsam-gegen-die-tierindustrie)
  * [Bio-Obst vom Bodensee biozyklisch-vegan](#bio-obst-vom-bodensee-biozyklisch-vegan)
  * [US-Milchviehbetriebe in Bio-Haferfarmen umwandeln](#us-milchviehbetriebe-in-bio-haferfarmen-umwandeln)
  * [Das Menschenrecht Tiere zu schützen](#das-menschenrecht-tiere-zu-schutzen)
  * [Jacy Reese](#jacy-reese)

<!-- tocstop -->

Warum vegan?
------------
Sammlung von aktuellen Zuständen, die durch unsere Tiernutzung ausgelöst oder verschärft werden.

### Tierwohl / Ethik
* Video: [Mitgefühl und Moral](https://www.youtube.com/watch?v=tEOJUw2Uq_Q), 3 min, Quarks&Co, WDR, 2016, tag:offline, tag:class2017
    * "Sie begegnen uns zuhause als Teil der Familie, auf dem Speiseplan als Nahrung, oder einfach nur als Rohstoff: Tiere"
    * Mitgefühl ist angeboren; mit wem wir fühlen, aber nicht
* Frage: Woher wissen wir, dass Tiere ähnlich wie wir fühlen?
    * Woher weiß man, dass andere Menschen so fühlen wie wir?
    * Beobachten wie Menschen mit Haustieren umgehen.
    * Beobachten wie Kinder mit allen Tieren umgehen.
* Opfersicht einnehmen
    * Insbesondere Fokus auf Leidensfähigkeit und Unschuldigkeit. Haben die das verdient (auch im Hinblick, was der Mensch sonst noch so mit Tieren macht)?
* Gewalt: Tierische Produkte sind immer mit Gewalt verbunden, die mit der https://de.wikipedia.org/wiki/Domestizierung der Tiere begann
    * "Die moderne Tierzucht, die die Haustiere unter völliger Kontrolle hält und jeden Kontakt mit Wildtieren zu vermeiden sucht, ist eine vergleichsweise junge Erfindung und wurde erst Jahrtausende nach der ersten Domestizierung üblich"
* Artgerechte Tötung?
* Die Zustände zu verbessern ist für den einzelnen Menschen vergleichweise einfach: er muss nur das ändern, was er täglich isst
* https://albert-schweitzer-stiftung.de/massentierhaltung
    * ["Warum vegan? Gründe und Gegenargumente"](https://albert-schweitzer-stiftung.de/aktuell/warum-vegan)
    * [Positiv und nachsichtig sein](https://albert-schweitzer-stiftung.de/aktuell/vegane-ernaehrung-ansteckend)
* Eier - Was spricht gegen Eier?
    * https://www.animalequality.de/essen/eier
        * Züchtung, Ertrag, Gewinn, Ausbeutung, soziale Tiere, männliche Küken
        * Backen ohne Eier
    * ["About Backyard Eggs"](https://www.youtube.com/watch?v=GxO2-WREo50), 2016, 3 min
    * [Bio-Eier](bio-eier.md)
* Auch Bio-Milch ist nicht toll fürs Tier: https://schrotundkorn.de/ernaehrung/lesen/tiergesundheit-artgerecht.html
    * siehe auch User-Comments: [biovegan](biovegan.md)
* Tiere in artgerechter Umgebung, siehe [tiere-artgerecht](tiere-artgerecht.md)
* Meeresbewohner: siehe [fisch-fischer-überfischung.md](fisch-fischer-überfischung.md)
* NABU
    * Artikel: ["Bio, vegetarisch, vegan – wichtige Schritte für mehr Tier- und Naturschutz"](https://blogs.nabu.de/bio-vegetarisch-vegan), 2015
    * ["Biomilch im Kaffee"](https://blogs.nabu.de/biomilch-im-kaffee), 2016
        * (oder Pflanzenmilch)
    * Ökologisch auch in der Gruppenarbeit: ["Ökologisch leben"](https://www.nabu.de/umwelt-und-ressourcen/oekologisch-leben/index.html)
        * [Video](https://www.youtube.com/watch?v=49LmPQa8vjI), 2018, 5 min
            * 4 Tipps, darunter Veganes Rezept für Gruppenverpflegung (anstelle viel Fleisch wie früher)
                * und wenn, dann Wild
* BUND
    * http://www.bund-berlin.de/bund_berlinde/hg_texte_startseite/bio_in_berlin/was_man_sonst_noch_tun_kann.html
* 2017: Greenpeace-Gutachten zu Schweinehaltung: "Konventionelle Schweinehaltung in Deutschland ist gesetzeswidrig"
    * http://www.tagesschau.de/inland/schweinezucht-greenpeace-101.html, 03.05.2017

### Welternährung
* "Die Fleischindustrie behauptet, die Ernährung zu sichern, doch in Wahrheit verschärft sie die Hungerproblematik."
    * https://albert-schweitzer-stiftung.de/themen/welternaehrung

### Energieeffienz
* 2020: Die Nahrungskette "Gras - Kuh - Mensch" ist ineffizient: todo

### Umweltschutz 2021 - Studien und Uni-Speisepläne
* Plant Based News (https://www.plantbasednews.org/), "vegan 2020": https://www.youtube.com/watch?v=ExOPTFRcR5I, 50 min
    * covid19-fokussiert
    * September: https://www.totallyveganbuzz.com/news/david-attenborough-eating-meat-middle-class-hypocrisy/
        (David Attenborough "wurde durch seine preisgekrönten Naturdokumentationen bekannt" https://de.wikipedia.org/wiki/David_Attenborough
        Aussage aus dem Radio-Interview - “I’m affluent enough to afford free-range, but it’s a middle-class hypocrisy"
    * Studie: https://www.sciencedirect.com/science/article/pii/S0048969720328709, Titel: "Food systems in a zero-deforestation world: Dietary change is more important than intensification for climate targets in 2050"
        * u. a. "Our results show that diets are the main determinant of GHG emissions, with highest GHG emissions found for scenarios including high meat demand, especially if focused on ruminant meat and milk, and **lowest emissions for scenarios with vegan diets**."
        * inkl. Graphik drin, die zeigt, dass vegan vor allen anderen untersuchten Ernährungsformen verortet.
    * Oktober: Adidas bringt eine vegane Schuh-Linie raus.
    * University of Cambridge hat 2016 erfolgreich Rindfleisch und Lammfleisch aus den Speiseplänen und dem Event-Catering gestrichen: https://www.cam.ac.uk/news/removing-beef-and-lamb-from-menu-dramatically-reduces-food-related-carbon-emissions-at-cambridge
    * November: "Tari Tari" kommt ins Fernsehen (https://www.youtube.com/watch?v=psdtyJ2aizI, 1 min)
    * Dezember: https://www.un.org/sg/en - "Making peace with nature is the defining task of the 21st century. It must be the top, top priority for everyone, everywhere — ANTÓNIO GUTERRES"

### Umweltschutz
* https://albert-schweitzer-stiftung.de/themen/landwirtschaft
    * siehe auch [bioveganer Landbau](biovegan.md)
* Umweltbundesamt zu Fleisch (2017): [Warum Fleisch zu billig ist](https://www.umweltbundesamt.de/themen/warum-fleisch-zu-billig-ist), Artikel, 5 min
* Meeresbewohner: "Is Your Diet Destroying the Ocean?" - https://www.youtube.com/watch?v=EO3knfFyRtA (13 Minuten) (2017)
    * Kritik am MSC-Siegel: siehe [MSC-Siegel](fisch-fischer-überfischung.md)
* ["Vegan und Umwelt – ein unschlagbares Team?!"](http://ich-lebe-vegan.de/gruende-fuer-den-veganismus/vegan-umwelt/)
* Video: ["Was würde passieren, wenn wir 80 Prozent weniger Fleisch essen?"](https://www.youtube.com/watch?v=wi8rMOiOQYY), 3sat nano, 2014, 4 min, tag: offline, tag:class?
* Video: ["Was würde passieren, wenn wir 80 Prozent weniger Fleisch essen? | SWR Odysso"](https://www.youtube.com/watch?v=XhHotmxQKnk), 2014, 3 min, tag:class2017
* Video: Insekten-TV-20171019-2246-2001.webxl.h264, ARD, 2017, 3 min, Insektensterben, tag:offline

### Gesundheit
* https://albert-schweitzer-stiftung.de/themen/gesund
* Generell gilt als unbestritten: Ausreichend Schlaf, körperliche Betätigung an der frischen Luft / in der Natur, viel Obst und Gemüse sind gut für die Gesundheit.
    * ABER: Vergleiche wie oft man _Werbung_ für diese Dinge sieht; Werbung für ausreichend Schlaf? Werbung für Spaziergänge draußen? Werbung für Spinat? Werbung für Pilze? Werbung für Äpfel und Karotten? Meistens: Werbung für ungesunde Sachen und Medikamente.
* ["Leben Veganer gesünder? Oder ist Veganismus ungesund?"](http://ich-lebe-vegan.de/gruende-fuer-den-veganismus/sind-veganer-gesuender/)
* https://www.anonymousforthevoiceless.org/mustsees/
    * ["How Not To Die: The Role of Diet in Preventing, Arresting, and Reversing Our Top 15 Killers"](https://www.youtube.com/watch?v=lXXXygDRyBU), 2016, 1h 20 min, Vortrag von Dr. Greger
        * https://nutritionfacts.org
        * Problem with Paleo: https://nutritionfacts.org/topics/paleolithic-diets/
* siehe zusatzinfos.md / User-Comment eines Ernährungsmediziners zu "Ersatzreligion" und "Ideologie"

### Videos für Einsteiger
* Häufig gestellte Fragen: [Der Artgenosse](http://der-artgenosse.de) (und Comics)
* Philosophischer Grundlagenvortrag von Gary Yourofsky: http://adaptt.org (engl.)
* Film, der aufzeigt, wie Menschen mit Tieren umgehen: Earthlings
* siehe beispiele.md
* Praktische Tipps https://utopia.de/galerien/10-tipps-ein-bisschen-veganer-zu-werden/

### Dokumentationen
* Doku: ["ZDF: Tierfabrik Deutschland"](https://www.youtube.com/watch?v=r4l8nrffQ1E), 2015, 45 min, tag:offline
    * ...
    * Hühner-Zuchtbetrieb Lohmann
    * ...
    * 21min: Christian Schmitt setzt Geldinteressen über Tierschutz
    * ...

Wie vegan?
----------
Hilfe für Personen, die sich gerne möglichst vegan verhalten wollen.

### Einsteiger-Tipps
* https://www.youtube.com/watch?v=bmnFDRj5mKM - "Was ich Gesprächspartnern nach Interviews erzähle", aus der Videobeschreibung:
    * 22 Tage vegane Ernährung testen: https://www.challenge22.com/challenge22/
    * Simple, schnelle, leckere und gesunde vegane Rezepte: http://www.5minuterecipes.de/
    * Vegane Restaurant- und Shoppingoptionen: https://www.happycow.net/
    * 10 allgemeine Gesundheitstipps für Veganer: ...
    * und noch mehr in der Videobeschreibung
* Tagesausflüge zu (Bio-)veganen Restaurants machen => sich von der Kreativität der Speisen inspirieren lassen
* https://utopia.de/galerien/10-tipps-ein-bisschen-veganer-zu-werden/
* [Vegan-Taste-Week](https://vegan-taste-week.de/)
* http://www.vheft.de/veganbuddy
* ["Die besten Eiweißquellen für Veganer"](http://ich-lebe-vegan.de/vegane-ernaehrung/die-besten-eiweissquellen-fuer-veganer/)
* Einen veganen Kochkurs besuchen (ggf. als Team-Event) => kreative Ideen für Rezepte
* ["Vitamin B12 und seine Supplementierung in der veganen Ernährung"](http://ich-lebe-vegan.de/vegane-ernaehrung/supplementierung-des-vitamins-b12/)
    * siehe auch:
        * ["Vitamin-B12-Bluttest: Wer muss zahlen?"](https://albert-schweitzer-stiftung.de/aktuell/vitamin-b12-bluttest-kosten), 2017
            * "Ein sinnvoller Bluttest beinhaltet mindestens den Parameter Holo-TC, wobei sich eine zusätzliche Erhebung von Methylmalonsäure (MMA) empfiehlt (siehe unser Merkblatt dazu)."
                * [Merkblatt](https://albert-schweitzer-stiftung.de/wp-content/uploads/Infoblatt-Vitamin-B12_Oktober16.pdf)
            * "Die BKK ProVita (…) übernimmt die Kosten für diese Untersuchung.« Zudem können über das 2017 eingeführte Bonusprogramm »Bonus Plus Ernährung« bis zu 200 Euro für spezielle Blutuntersuchungen (z. B. Vitamin B12), Gesundheitsvorträge und andere zweckgebundene Leistungen erstattet werden. Auch die Kosten für Nahrungsergänzungsmittel wie etwa Vitamin B12, Vitamin D oder Omega-3-Fettsäuren können übernommen werden."
        * ["Veganfreundliche Krankenkassen – gibt es das?"](https://albert-schweitzer-stiftung.de/aktuell/veganfreundliche-krankenkassen), 2017
            * Analyse der großen Kassen und drei kleiner Vorbilder
* http://veggietale.de/about-me/
    * Geschichte einer Frau, die in einem Schlachtumfeld aufwuchs, das aber nicht aushielt und nun auf ihrem Blog zeigt, was man alles leckeres essen kann, wenn man Tieren nicht absichtlich wehtun will.
* selber im Internet suchen
* [Rezeptsammlung](../Rezepte)

### Ernährungspyramide
* Oft wird gefragt, was man praktisch am besten isst, um eine ausgewogene Ernährung zu ermöglichen. Ernährungspyramiden sind ein mögliches praktisches Hilfsmittel:
    * https://www.rootsofcompassion.org/vegan-nutrition-chart-german
    * https://vebu.de/fitness-gesundheit/ernaehrungspyramide/vegane-ernaehrungspyramide/
    * http://www.netdoktor.de/ernaehrung/ernaehrungspyramide/vegane-ernaehrungspyramide/, 2017

### Für fast jeden geeignet: Vegan für Kraftsportler
* Patrick Baboumian: http://ondemand-ww.wdr.de/medp/fsk0/135/1353478/1353478_15738992.mp4 (WDR, 4 min)

### Vegan Advocacy
* Melanie Joy, siehe beispiele.md

Illustrationen
--------------
### Der Mensch und die Tiere
![Bild 1](http://www.adaptt.org/images/headers/kuczynski-speciesism-640x389.jpg "Der Mensch und die Tiere")

(Bildquelle siehe Link)

### Ethik des Fleischessens
* https://en.wikipedia.org/wiki/Ethics_of_eating_meat
* ["Is there a moral case for meat?"](http://grist.org/food/is-there-a-moral-case-for-meat), 2015

"I have the right to eat meat"

![](img/the-right-to-eat-meat.jpg)

Positive Beispiele
------------------
* siehe [beispiele.md](beispiele.md)

Vegan vs. Freilandeier von Oma
------------------------------
### Situation: Kuchenbüffet mit Kuchenspenden und Kaffee mit Milch
* Frage: Was ich bei vegan nicht verstehe, ist, was gegen Eier wie von Oma einzuwenden ist.
    * 99 % der Eier stammen ja gar nicht aus solchen idyllischen Zuständen
* Aber der Hof xy, da laufen die Hühner frei draußen herum, da hole ich meine Eier. Die Hühner legen doch sowieso Eier. Freilandeier.
    * Ja, die Situation sieht nicht verkehrt aus. Dazu folgende Fragen
        * Haben Sie einmal gefragt, **wo die Hühner herkommen**, die dort picken? (üblicherweise von einem Zuchtbetrieb: qualvolle Zuchtmethoden; Legehennen vs. Kükenschredder)
        * **Welche Rasse** wird eingesetzt? Turbo-Legehuhn tut weh.
        * Wieviele Eier / Jahr würden die Hühner legen, **wenn wir Menschen sie nicht wegnehmen würden** (so wie es normal wäre)?
        * Wo gehen die Hühner hin, wenn die Legeleistung aus Sicht des Betriebs zu gering geworden ist?
            * **Schlachten weit vor Ende es natürlichen Lebensendes**
        * Würden wir so mit anderen Tieren (wie z. B. Hunden und Katzen) umgehen? Würden wir das toll finden?
        * Warum?
            * Weil wir davon **überzeugt** sind, ohne Eier geht es nicht (obwohl es genug Gegenbeispiele gibt)
            * Weil wir Hühner als **Produktionsmaschinen** betrachten.
                * Diese Tier-Maschine wird nur soweit gepflegt und gewartet wie es sich positiv für die Produktivität auswirkt. Das **Leben des Tiers hat keinen Wert an sich**.
    * Schauen Sie sich mal die hier vorhandenen Kuchen und die Milch an:
        * **Wieviele der Kuchen hier enthalten wohl Eier und Milch aus guten Zuständen?**
        * **Was ist mit der "Ja"-Milch dort?** Wenn das keine Massentierhaltung ist, was ist es dann?
        * Einfache Lösung: für solche Kuchenbüffets vegane Rezepte nachfragen. Das ist viel einfacher, billiger und umweltschonender als tierfreundliche Milch und Eier zu finden.

Risikokommunikation
-------------------
### Bundesinstitut für Risikobewertung (BfR), 2017
* Artikel: [Pressemitteilung: "Vegane Ernährung als Lebensstil: Es besteht Risikokommunikationsbedarf"](http://www.bfr.bund.de/de/presseinformation/2017/42/vegane_ernaehrung_als_lebensstil__es_besteht_risikokommunikationsbedarf-202177.html)
    * "Tofu-Würstchen auf dem Grill, danach Kuchen mit Banane statt Ei: Die vegane Ernährung liegt im Trend. Doch neben nachgewiesenen positiven Einflüssen auf die Gesundheit werden auch Risiken beschrieben."
    * "Unter anderem wurde klar: Eine effektive Risikokommunikation sollte an bestehende Überzeugungen der Veganerinnen und Veganer anknüpfen. Das Ziel sind konkrete Tipps, die sich mit einer veganen Ernährung verbinden lassen."
        * (also keine pauschale Zurückweisung dieser Lebenweise)
    * "Ein wachsender Anteil der Bevölkerung entscheidet sich für die vegane Ernährung. Welche gesundheitlichen Vor- und Nachteile dieser Schritt mit sich bringt, ist wissenschaftlich jedoch noch nicht eindeutig geklärt."
    * "Mit Hilfe von Fokusgruppen-Interviews wurden im Rahmen eines Forschungsprojektes insgesamt 42 Veganerinnen und Veganer zu ihren Einstellungen befragt."
    * "Es ließen sich einheitliche Einstellungsmuster erkennen. So ist die Entscheidung für eine vegane Ernährung in der Regel ethisch begründet. Meist impliziert sie auch den Verzicht auf tierische Produkte in anderen Bereichen, wie beispielsweise der Bekleidung."
    * "Für die überwiegende Mehrheit der Befragten ist die Rückkehr zur omnivoren Ernährung, die tierische Produkte zulässt, nicht vorstellbar. Auch eine Schwangerschaft wird meist nicht als Grund dafür angesehen."
    * "Wer die vegane Ernährung als gefährlich oder abnormal darstellt, findet wenig Gehör bei der Zielgruppe."
* [Abschlussbericht: "Vegane Ernährung als Lebensstil: Motive und Praktizierung"](http://www.bfr.bund.de/cm/350/vegane-ernaehrung-als-lebensstil-motive-und-praktizierung.pdf) 85 Seiten
* [Vortrag 2016: "Vegan – Risiken durch einen neuen Ernährungsstil?"](http://www.bfr.bund.de/cm/343/vegan-risiken-durch-einen-neuen-ernaehrungsstil.pdf), Abteilung Risikokommunikation, 29 Seiten
    * "Voraussetzung für erfolgreiche Risikokommunikation"
        * "absolut neutrale oder sogar affirmative „pro-vegan“ Kommunikation"
        * "keine Stigmatisierung des Veganismus durch die explizite oder implizite Darstellung als anormale Ernährungsform"
        * "Nahrungsalternativen anbieten und konkrete Supplementierungsempfehlung zur Sicherstellung einer gesunden veganen Ernährung geben"

### Vegan in der Schwangerschaft? - Keine Evidenz für eine Nichtempfehlung
* siehe gesundheit-ernährung.md / Kinder

Vitamine / B12
--------------
* siehe gesundheit-ernährung.md

Zusatzinfos
-----------
* [Zusatzinfos](zusatzinfos.md)
* [tiere-artgerecht](tiere-artgerecht.md)
* [Rezeptsammlung](../Rezepte)

### Vegan society: Ripened by human determination
* "Ripened by human determination - 70 years of The Vegan Society - 2014: https://www.vegansociety.com/sites/default/files/uploads/Ripened%20by%20human%20determination.pdf (19 Seiten PDF)
* Gegründet 1944 von https://de.wikipedia.org/wiki/Donald_Watson
    * siehe Bauernhofbesuch seines Onkels

### Selber aktiv werden
* [Infoblatt mit Tipps von peta2 (engl.)](https://www.peta2.com/feature/vegan-college-ranking/wp-content/uploads/2014/10/veganize-your-dining-hall.pdf) was man tun kann, damit die eigene Mensa mehr Veganes anbietet.

### Utilitarismus
* https://de.wikipedia.org/wiki/Utilitarismus#Andere_Spezies
* u. a. entwickelt von Jeremy Bentham (1748–1832)
    * "„Der Tag mag kommen, an dem der Rest der belebten Schöpfung jene Rechte erwerben wird, die ihm nur von der Hand der Tyrannei vorenthalten werden konnten. Die Franzosen haben bereits entdeckt, dass die Schwärze der Haut kein Grund ist, ein menschliches Wesen hilflos der Laune eines Peinigers auszuliefern. Vielleicht wird eines Tages erkannt werden, dass die Anzahl der Beine, die Behaarung der Haut oder die Endung des Kreuzbeins ebenso wenig Gründe dafür sind, ein empfindendes Wesen diesem Schicksal zu überlassen."

### Rechtliche Situation
* Buch: Tiere klagen an, von Antoine F. Goetschel, 2012
    * siehe z. B. https://www.magdeburg-vegan.de/produkt/antoine-f-goetschel-tiere-klagen-an/

### Mögliches Fazit für Einsteiger
* Fleisch und Fisch (schrittweise) vermeiden.
* Tierprodukte (auch [Bio-Eier](bio-eier.md)) im Allgemeinen (soweit praktikabel schrittweise) minimieren.
* siehe wirksamkeit-des-einzelnen.md / Die Verantwortlichkeit der Intellektuellen

### Antibiotika
* siehe antibiotika.md

### Soja-Import
* ... das meiste (zudem genveränderte) Soja, das aus dem Ausland importiert wird, dient als Tiernahrung für Fleisch- und Milch-Tiere (d. h. auch Kuh-Milchschokolade) ...
* ... das verwendete Bio-Soja für vegane Bio-Gerichte kommt meist aus Europa ...

Bildungsangebote
----------------
::bildung

### Verein Mensch Tier Bildung e.V.
* siehe https://mensch-tier-bildung.de, spenden-atlas.md

### Mensch-Tier-Beziehung von Frankfurt-Zirkus.org
* http://www.frankfurt-zirkus.org/das-kannst-du-tun/bildungsangebot/

Inbox 2020
----------
### Naturland: Veganer und Tierhalter
* https://www.naturland.de/images/Verbraucher/tierwohl/pdf/KI_Vegan_und_%C3%96ko_2019_.pdf
    * offline-material/2020/KI_Vegan_und_Öko_2019_.pdf
* „Braucht der Ökolandbau noch Tiere?“ Prof. Dr. Bernhard Hörning, siehe biovegan.md

### gemeinsam-gegen-die-tierindustrie
* https://gemeinsam-gegen-die-tierindustrie.org/termine/
* https://tierfabriken-widerstand.org/aktuelles/termine/
* siehe auch https://friederikeschmitz.de/

### Bio-Obst vom Bodensee biozyklisch-vegan
* siehe biovegan.md

### US-Milchviehbetriebe in Bio-Haferfarmen umwandeln
* "Hälsa Foods wandelt US-Milchviehbetriebe in Bio-Haferfarmen um"
    * https://vegconomist.de/international/haelsa-foods-wandelt-us-milchviehbetriebe-in-bio-haferfarmen-um/
        * ...

### Das Menschenrecht Tiere zu schützen
Rechtsfragen, veganes Recht

* Video: „Tierleidfreie Rechtsräume gestalten. Das Menschenrecht Tiere zu schützen“, https://www.youtube.com/watch?v=pjtwq7altTo, 2017, 30 min
    * "Vortrag von Ralf Müller-Amenitsch auf der PETA Tierrechtskonferenz 2017"
    * ...
    * ... todo
    * ...
    * https://veganes-recht.de/vegan-in-oeffentlichen-institutionen
        * https://veganes-recht.de/qualitaetskriterien-fuer-veganes-essen-und-reisen

### Jacy Reese
* Video: "The End of Animal Farming | Jacy Reese | TEDxUniversityofMississippi", https://www.youtube.com/watch?v=PBmbVphZKYc, 13 min, 2018, Soziologe
    * "Many books and articles have documented how animal agriculture devastates public health, the environment, and animal welfare.
    Experts predict that the future of food is vegan. But readers and listeners are left with one burning question:
    How do we actually get from here to there?” I'm a social scientist currently working on my first book, [...]"
* https://en.wikipedia.org/wiki/Jacy_Reese
    * "His research focuses on effective altruism, anti-speciesism, and plant-based and cellular agriculture"
    * https://www.theguardian.com/food/2018/nov/16/theres-no-such-thing-as-humane-meat-or-eggs-stop-kidding-yourself
        * "There's no such thing as humane meat or eggs. Stop kidding yourself "
        * "Many people think they consume humane meat, but only a tiny fraction actually do.
            The majority of consumers are totally wrong about what they eat"
        * Buch: https://en.wikipedia.org/wiki/The_End_of_Animal_Farming
            * "The End of Animal Farming is written from the "effective altruist" point of view, and carries both that
                movement’s best and worst tendencies. At their best, the effective altruists help hone our moral reasoning,
                and focus on being useful rather than seeming virtuous. You can see that in Reese’s approach:
                He wants to convince you that ending animal farming is _possible_, and lay out a series of steps by which
                it might be achieved, not just show that it’s important. In fact, he spends little time making the moral case,
                which is quite simple, and the bulk of the book is dedicated to solutions.
                Unfortunately, the "effective altruists'" frustrating qualities are on display too. [...]"
