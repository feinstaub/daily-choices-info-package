Wiederkehrende Ereignisse
=========================

Alle Jahre wieder zu bestimmten Ereignissen hat jeder die Chance sich bewusst zu machen, was Sache ist.

<!-- toc -->

- [Jeden Tag im Jahr](#jeden-tag-im-jahr)
  * [Schweine](#schweine)
- [Immer wieder jährlich](#immer-wieder-jahrlich)
  * [Vögel / Vogelgrippe](#vogel--vogelgrippe)
  * [Stopfleber](#stopfleber)
  * [Genussmittel](#genussmittel)
- [Januar: Neujahr](#januar-neujahr)
  * [Feuerwerk](#feuerwerk)
- [Winter: Bekleidung](#winter-bekleidung)
  * [Pelz-Mode](#pelz-mode)
  * [Warme Winterjacken mit Daunen](#warme-winterjacken-mit-daunen)
- [Frühjahr](#fruhjahr)
  * [Ostern - IST-Zustand](#ostern---ist-zustand)
  * [Ostern - Lösungsmöglichkeiten](#ostern---losungsmoglichkeiten)
  * [24. April - Tag des Versuchstiers](#24-april---tag-des-versuchstiers)
- [Mai](#mai)
  * [Erdüberlastungstag](#erduberlastungstag)
- [Sommer](#sommer)
  * [1. Juni - Tag der Milch](#1-juni---tag-der-milch)
  * [Zoo](#zoo)
  * [Zirkus](#zirkus)
- [Herbst](#herbst)
  * [Steuern und Allmende](#steuern-und-allmende)
- [November](#november)
  * [Thanksgiving: Truthahn = Pute](#thanksgiving-truthahn--pute)
- [Dezember](#dezember)
  * [Weihnachten](#weihnachten)

<!-- tocstop -->

Jeden Tag im Jahr
-----------------
### Schweine
* siehe [Schweine](../Vegan/tiere-artgerecht.md)

Immer wieder jährlich
---------------------
### Vögel / Vogelgrippe
Alle Jahre wieder:

* ["Vogelgrippe in Hessen: Sind Putenbrust und Hähnchenschenkel in Gefahr?"](https://www.verbraucher.de/vogelgrippe--ist-der-gefluegelbraten-zu--weihnachten-in-gefahr-), Verbraucherzentrale Hessen, 04.04.2017
    * Die Überschrift suggeriert, dass die größte Gefahr der Vogelgrippe ist, dass der Verbraucher Angst um sein Lieblingsfleisch haben muss. Dass auch eine Gefahr für das Wohl der Tiere besteht, wird offensichtlich niederpriorisiert.
* todo: Stallpflicht, vorbeugende Notschlachtungen

### Stopfleber
* Die betroffenen Opfer: **Enten** und **Gänse**
    * siehe [Stopfleber](../Vegan/tiere-artgerecht.md)

### Genussmittel
* konventioneller Wein: siehe Pestizide

Januar: Neujahr
---------------
### Feuerwerk
* Betroffen: Menschen
    * daher: ["Augenärzte wollen Raketen und Böller verbieten lassen"](http://www.sueddeutsche.de/gesundheit/augenheilkunde-raketen-und-boeller-weg-1.3306174), 2016
        * "[in Deutschland] sind jährlich Hunderte Augenverletzungen zu beklagen, Dutzende verlieren ihr Sehvermögen, weil eine Rakete oder ein Böller ins Auge gegangen ist."
        * oder einfach: freiwillig darauf verzichten
    * 2017: dasselbe
* Betroffen: Umwelt
* Betroffen: Haustiere, Wildtiere, eingesperrte Zootiere
    * tödliche Gefahr, Stress, Panik
    * jedes Jahr wird erneut darauf hingewiesen
    * siehe auch: https://www.peta.de/feuerwerk, Was kann man selber tun
        * "Bitten Sie den/die Bürgermeister/in Ihrer Stadt, sich für ein friedliches und tierfreundliches Silvester einzusetzen und das Abbrennen jeglicher Feuerwerke zu verbieten."
        * "Setzen Sie sich in Ihrem Freundes- und Bekanntenkreis für ein Silvester ohne Böller und Raketen ein."

Winter: Bekleidung
------------------
### Pelz-Mode
* siehe [Pelz](../Kleidung/pelz.md)
    * Echt-Pelz-Mode und als Kunstpelz ausgegebenen Echt-Pelz vermeiden

### Warme Winterjacken mit Daunen
* siehe [Daunen](../Vegan/tiere-artgerecht.md)
    * inkl. Alternativen

Frühjahr
--------
### Ostern - IST-Zustand
Die derzeitigen Tier-Opfer und Leidtragenden:

* **Hühner** (in Form von Fleisch und Eiern)
    * Tiere: siehe [Bio-Eier](../Vegan/bio-eier.md)
    * Menschen: Antibiotika-Resistenzen, siehe [Antibiotika](antibiotika.md)
        * ["Keime in Gefügel - Es ist nicht besser geworden"](http://www.tagesschau.de/inland/pute-101.html), tagesschau 2017
            * "Hähnchen- und Putenfleisch ist enorm mit antibiotikaresistenten Keimen belastet. Behörden haben die Keime in fast jeder zweiten Probe gefunden."
            * Die Politik scheut sich zu sagen, was Sache ist: die Lösung ist eine drastische Reduzierung des Tierkonsums. Die Veganer machen vor wie es praktisch gehen könnte.
            * "als Verursacher von zum Teil schwerwiegenden Krankenhausinfektionen"
                * Die Krankenhäuser könnten von sich aus die Patienten mit veganer Kost in Bioqualität versorgen.
    * Siehe 5-Minuten-Video [99 % gängige Praxis / Hähnchenmast](../Vegan/tiere-artgerecht.md)
    * Demeter-Zweinutzungshuhn von Lohmann
        * https://de.wikipedia.org/wiki/Lohmann_Tierzucht#Tierqu%C3%A4lerei
        * http://www.spiegel.de/wirtschaft/gefluegelmast-so-leiden-die-huehnereltern-a-1140175.html
    * Tierfutter Nahrungskonkurrent Mensch
        * die moderne Hühnerlandwirtschaft: https://www.youtube.com/watch?v=0t7i05ji4kk
* **Schafe** (in Form von Lammfleisch)
    * siehe tiere-artgerecht.md
* **Kaninchen** (in Form von Kaninchenbraten)
    * ["Profit kontra Tierwohl - So grausam sind die Zustände in der Kaninchenmast"](http://www.mdr.de/brisant/so-grausam-sind-die-zustaende-in-der-kaninchenmast-100.html), mdr.de, 07.04.2017
        * Hinweis: der Profit entsteht durch die unreflektierte Nachfrage der Konsumenten
    * ["Kaninchenmast - Ein Sinnbild für die wahllose Einteilung in „Nutz- oder Haustier“"](http://tierretter.de/portfolio/grausam-kaninchenmast/), tierretter.de, 2016
    * siehe auch "Das Kaninchen" unter tiere-artgerecht.md.
* **Kühe** (in Form von Schokoladenhasen, siehe auch [Genussmittel - Kakao](../Kakao))
    * siehe das Grauen der Milchproduktion
* siehe auch: http://www.peta.de/ostern-auch-fuer-tiere-ein-feiertag
* Auswirkungen auf **Mitmenschen**:
    * Probleme mit kakaohaltigen Schokoladenhasen: siehe [Genussmittel - Kakao](../Kakao)
    * Probleme mit milchhaltigen Schokoladenhasen, weil das (unnatürliche) **Soja-Kraftfutter** für die Kühe aus Übersee kommt und dort für Probleme sorgt

Hintergrund?

* Tradition und Kulturbewahrung
* Das Ei steht für Fruchtbarkeit und neues Leben.

### Ostern - Lösungsmöglichkeiten
* Siehe Tierschutzverein Hamburg
* https://www.veganblog.de/verschiedenes/10-alternativen-fuer-ein-leidfreies-osternest/
    * Eier bemalen: Kunststoff-, Ton- oder Holzeier
    * "Oder ihr bastelt die kompletten Eier selbst und könnt darin sogar etwas verstecken"
    * Leidfreie Schokolade
    * "Kuchen backen ohne Ei und Milch"
    * Veganes Ostermenü
* https://blogs.nabu.de/ostern-ohne-muell/ - siehe Kommentare
* Vorbild sein / über den eigenen Schatten springen / die Veränderung sein, die man gerne sähe
    * Essen: schrittweise vegane Rezepte umsetzen (z. B. http://www.peta.de/eifreierezepte, [Rezeptsammlung](../Rezepte))
    * Eier anmalen und verstecken: zur Kulturbewahrung z. B. wiederverwendbare **Holzeier** verwenden
* siehe [Genussmittel - Kakao](../Kakao) (Problem und Lösung)
* Wer mit gutem Beispiel vorangeht, hat ein bessere Ausgangsposition zur Anmahnung einer Lösung des [Stopfleber-Problems](../Vegan/tiere-artgerecht.md)
    * "Die Oster-Chance"

### 24. April - Tag des Versuchstiers
* https://www.aerzte-gegen-tierversuche.de/de/ueber-uns/was-wir-tun/2051-internationaler-tag-zur-abschaffung-der-tierversuche
* http://www.tag-zur-abschaffung-der-tierversuche.de/
    * "Der Gedenk- und Aktionstag wurde 1979 in Großbritannien ins Leben gerufen und geht auf den Geburtstag von Lord Hugh Dowding zurück, der sich im Britischen Oberhaus für den Tierschutz einsetzte."
* http://dertagdes.de/jahrestag/tag-des-versuchstiers/
    * wird gefeiert seit 1962 (?)

Mai
---
### Erdüberlastungstag
Erd-Ressourcen für dieses Jahr verbraucht

* 2020: am 3. Mai
    * "Müller fordert Abkehr von traditionellem Kapitalismus", https://rp-online.de/politik/deutschland/mueller-fordert-zum-erdueberlastungstag-abkehr-von-kapitalismus_aid-50338885
        * "Entwicklungsminister Gerd Müller (CSU) hat die deutsche Wirtschaft aufgefordert, zur Rettung des Klimas vom traditionellen Kapitalismus abzukehren.
        „Der Immer-Weiter-Schneller-Mehr-Kapitalismus der letzten 30 Jahren muss aufhören“"
        * "Alle vier Sekunden werde weltweit die Fläche eines Fußballfeldes abgeholzt – vor allem für die riesige Soja- und Palmölplantagen. „Das müssen wir sofort stoppen. Wir betreiben Raubbau in den Regenwäldern.“"
        * Regenwald, Soja und Palmöl:
            * "„Wir benutzen Shampoo mit Palmöl und grillen häufig Fleisch vom Schwein, das mit Soja gefüttert wurde.“ Die EU solle nur noch nachhaltiges und klar zertifiziertes Soja und Palmöl importieren. „Wer künftig Soja oder Palmöl importieren will, der muss den Nachweis erbringen, dass der Anbau nicht auf gerodeten Waldflächen erfolgt ist.“ Man müsse endlich einmal ernstmachen mit den Nachhaltigkeitsvorgaben in den Handelsabkommen. „Im Hafen von Hamburg darf kein Schiff anlegen, das Soja oder Palmölprodukte aus nicht zertifizierter Produktion nach Deutschland bringen will. Uns muss auch in Deutschland bewusst sein: Immer nur billig – das geht auf Kosten der Natur und der Menschen in anderen Erdteilen.“"
        * Sinn:
            * "„Jedes einzelne Leben ist nur ein Flügelschlag in der Geschichte des Planeten. Wir tragen Verantwortung für die nächsten Generationen und Verantwortung vor Gott.“"
    * mehr siehe "Gerd Müller"

* 2019: am 29. Juli
    * https://www.germanwatch.org/de/overshoot "Globale Erdüberlastungstag 2019 ("Earth Overshoot Day")"
        * Graphik mit Vergleich USA, Deutschland, etc. und Rest der Welt
    * https://utopia.de/ratgeber/earth-overshoot-day/

* 2018: am 1. August
    * Von Regierungs-Seite: "Earth Overshoot Day 2018", https://www.bmz.de/de/presse/aktuelleMeldungen/2018/juli/180731_pm_034_Bundesentwicklungsminister-Mueller-zum-Earth-Overshoot-Day-2018/index.jsp
        * "Bundesentwicklungsminister Müller: Brauchen ein radikales Umdenken bei der Nutzung natürlicher Ressourcen"
        * "Bundesentwicklungsminister Dr. Gerd Müller: "Wir brauchen ein radikales Umdenken, um die Ausbeutung der Naturressourcen zu stoppen.
            Saubere Luft, sauberes Wasser, fruchtbare Böden, ein stabiles Klima, Artenvielfalt – davon hängt das Überleben der Menschheit ab.
            Weltweit sind bereits 90 Prozent aller Fischbestände bis an die Grenze der Nachhaltigkeit ausgebeutet oder überfischt.
            Und jedes Jahr werden auf der Erde 7,6 Millionen Hektar Wald zerstört, das entspricht der Fläche Bayerns.
            Der Erdüberlastungstag zeigt in aller Deutlichkeit: Wir verbrauchen von allem zu viel!
            Wir leben immer stärker auf Kosten unseres Planeten, unserer Kinder und Enkelkinder und der Menschen in Entwicklungsländern.
            Würden alle Menschen unsere Konsummuster übernehmen, bräuchten wir mehr als drei Erden.
            Deswegen müssen wir jetzt drei Trendwenden einleiten:
            Ein **neues Wachstumsverständnis**, eine **Wirtschaft, die in Kreisläufen denkt**, und ein konsequentes **Umsteuern auf erneuerbare Energie**.
            Das ist Voraussetzung für nachhaltige Entwicklung – und zugleich eine **Frage der globalen Gerechtigkeit**!""
    * ["Ressourcen für 2018 bereits verbraucht"](http://www.tagesschau.de/inland/ressourcen-verbrauch-101.html), 2018
        * "Deutschland hat laut Klimaschützern einen zu großen ökologischen Fußabdruck. Demnach haben wir die natürlichen Ressourcen, die uns für 2018 zur Verfügung stehen, bereits verbraucht - und leben ab morgen "auf Kredit" künftiger Generationen."

* Zahlen weichen ab: https://de.wikipedia.org/wiki/Earth_Overshoot_Day#Deutschland

Sommer
------
### 1. Juni - Tag der Milch
* http://www.tag-der-milch.de
    * wirkt recht uninformativ bei kritischen Themen: Tierwohl, Massenkuhhaltung, Milchpreisproblem
* https://de.wikipedia.org/wiki/Weltmilchtag
    * "Milch soll als natürliches und gesundes Getränk weltweit und für alle Altersstufen beworben werden. Der Internationale Tag der Milch fand 2007 zum 50. Mal statt."
* http://www.sagneinzumilch.de
    * Alternativen zur Tiermilch
* Gängige Praxis: siehe [vegan/milch](../Vegan/milch.md)

### Zoo
* siehe Zoo/

### Zirkus
* siehe Zoo/

Herbst
------
### Steuern und Allmende
* "Jeweils im Herbst veröffentlicht der **"Bund der Steuerzahler"** das Schwarzbuch 'Die öffentliche Verschwendung'" (https://de.wikipedia.org/wiki/Bund_der_Steuerzahler_Deutschland)
    * Achtung, bitte beachten, dass diese Organisation vermutlich nicht wie der Name suggeriert im Interesse des Gemeinwohls arbeitet: https://de.wikipedia.org/wiki/Bund_der_Steuerzahler_Deutschland#Kritik
    * Steuerverschwendung und Überregulierung sind nicht zielführend, **aber** die Errungenschaften eines Staats madig zu reden, führt nicht zu positivem Engagement.
        * Ein zu schlanker, ausgemergelter Staat schadet dem Gemeinwohl: siehe [Die Allmende-Herausforderung](nachhaltigkeit.md)
    * Beispiele von Steuerverschwendungen
        * z. B. im [Bundeslandwirtschaftsministerium](http://www.schwarzbuch.de/aufgedeckt/fall-details/news/935000-eur-fuer-markenfleisch-von-edeka/?tx_news_pi1%5Bcontroller%5D=News&tx_news_pi1%5Baction%5D=detail&cHash=19e5e4e21531570290a5784fd0fa6177)
    * siehe auch Schwarzbuch Markenfirmen

November
--------
### Thanksgiving: Truthahn = Pute
Die vergessenen, aber vermeidbaren Opfer: "feiern die Amerikaner Thanksgiving: 45 Millionen Truthähne werden verspeist."

* 45 Millionen Truthähne an **nur einem Tag!** und auch nur in einem Land.
    * siehe [tagesschau 23.11.2017](http://www.tagesschau.de/ausland/thanksgiving-105.html)
    * auf dem Bild lachen die Menschen, die gerade 2 der 45.000.000 "begnadigt" haben, als ob sie vorher schuldig gesprochen wurden. Warum lachen die?

Dezember
--------
### Weihnachten
Die vergessenen, aber vermeidbaren Opfer:

* **Kühe** (in Form von Kuhmilch-Schokoladen-Weihnachtsmännern)
    * siehe das Grauen der Milchproduktion
* Andere Tiere
    * wie **Gänse**, falls diese gebraten auf dem Festtagstisch stehen
* Behandlung von **Mitmenschen**:
    * Probleme mit kakaohaltigen Schokoladennikoläusen: siehe [Genussmittel - Kakao](../Kakao) (Problem und Lösung)
* Truthahn = Pute, siehe dort
* Konsum:
    * Lustgewinn dabei leider nur Strohfeuereffekt

Lösungsmöglichkeiten:

* Erinnerung: Weihnachten ist das Fest der Liebe und Nächstenliebe
* Besinnung auf den Kern von Weihnachten (statt gestresst durch [Markengeschäfte](marken-multinational.md) rennen)
* veganes Weihnachtsmenü
* Ideen gegen nutzlosen Konsum:
    * Verbrauchsgüter (wie Essen) schenken
    * Bar-Gutscheine oder Spenden (an gemeinwohlorientierte Organisationen - siehe [spenden-atlas.md](spenden-atlas.md)) schenken
    * z. B. Geschenkpapier wiederverwenden oder alte Zeitungen nehmen
    * "Stattdessen suchen wir jedes Jahr ein Hilfsprojekt aus und spenden den Betrag, der normalerweise in nicht besonders dringend benötigte Dinge investiert würde. Es gibt dann kleine 'Spendenquittungen' für Verwandte und Freunde" (Alnatura Magazin 12.2016, S. 17 Vegane Kolumne)
        * "beim ersten Mal war es ungewöhnlich, regte aber zum Nachdenken an und wird seitdem sehr geschätzt"
