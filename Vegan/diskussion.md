Vegan / Diskussion
==================

<!-- toc -->

- [2020](#2020)
  * [Fleischgenuss als Luxus](#fleischgenuss-als-luxus)
  * [Ausreden / Angriffe](#ausreden--angriffe)
  * [Earthing Ed - 30 Ausreden](#earthing-ed---30-ausreden)
  * [Vater Steak](#vater-steak)
  * [Milch: Kälber sterben illegal](#milch-kalber-sterben-illegal)
- [2019](#2019)
  * [Ethik](#ethik)
  * [Speciesism](#speciesism)
- [2018](#2018)
  * [Jagdfleisch, Wildfleisch](#jagdfleisch-wildfleisch)
  * [Umweltverbände](#umweltverbande)
  * ["Ich esse eigentlich nur Bio-Fleisch; und wenig"](#ich-esse-eigentlich-nur-bio-fleisch-und-wenig)
  * [Nicht alle vegan; aber vegan-freundlich sollte drin sein](#nicht-alle-vegan-aber-vegan-freundlich-sollte-drin-sein)
- [2017](#2017)
  * ["Puls 180: Metzgerin trifft PETA-Aktivistin und Tierschützerin", hr-fernsehen, 2017](#puls-180-metzgerin-trifft-peta-aktivistin-und-tierschutzerin-hr-fernsehen-2017)
  * ["Puls 180 - Wie tickst Du denn? AfD-Kreisvorsitzender trifft auf Friseur", hr-fernsehen, 2017](#puls-180---wie-tickst-du-denn-afd-kreisvorsitzender-trifft-auf-friseur-hr-fernsehen-2017)

<!-- tocstop -->

2020
----
### Fleischgenuss als Luxus
* ...den man sich nicht nehmen lassen will
    * "Keiner hat etwas gegen den Genuss des Fleisches an sich. Sondern mit den damit verbundenen gängigen Produktionsbedingungen."
        * es gibt auch Fleisch von Tieren, die alt werden durften
        * oder Laborfleisch
    * Es geht also nicht um den Genuss, sondern mit den Folgen für Lebewesen in der gängigen Praxis
        * Wie würdest du dir eine Tierhaltung vorstellen, die für dich OK ist?
        * Wann sollten die Tiere sterben? (Kriterien?)
        * Wollen Tiere sterben oder lieber weiterleben?
        * Wie genau sollte das Tier getötet werden? (Würdest du es selber machen?)
        * Wieviel ist ein Leben wert?

### Ausreden / Angriffe
* Die Bildschirme verbrauchen unnötigen Strom. Einer könnte ausgeschaltet werden.
    * Mit der selben Argumentation könnte jeder weitere ausgeschaltet werden.
    * Was sind die Tiere wert, die dort gezeigt werden? Nicht mal den Strom hier?
    * Was ist mit dem Strom, der täglich in den Massenschlachthäusern verbraucht wird?
* Alle vegan => Wirtschaft bricht zusammen
    * Ziel alle Kriege abzuschaffen => die Waffenwirtschaft bricht zusammen?
* Besser für Menschenrechte und gegen Kriegsgebiete auf die Straße gehen / Whataboutism
    * Es wird immer jemanden geben, der sich beschwert, dass es etwas Schlimmeres gibt. Was nun?
        * Man könnte alle Kriegsgebiete auf den vielen Bildschirmen zeigen.
            * ...
    * Wo zieht man die Grenze? Nach dieser Logik dürften man auch keine elektronische Werbung
        für alltägliche Dinge zeigen. Oder fernsehen.
* Wie gehst du damit um, wenn du jemanden zum Veganismus bekehrt hast und der dann krank wird?
    * Unterliegende Annahme/Angst: Veganismus macht krank
    * Was wäre, wenn du jemanden für Menschenrechte begeisterst und der dann in einem Hilfseinsatz in einem Krisengebiet umkommt?

### Earthing Ed - 30 Ausreden
* "30 Ausreden von Nicht-Veganern und passende Antworten" - Menschenrechte sind wichtiger (Earthling Ed) / Whataboutism
    ("30 Non-Vegan Excuses and How to Respond to Them by Earthling Ed GERMAN.pdf", https://earthlinged.org/ebook)
        * https://drive.google.com/file/d/1sTVSvCDHNd76f7NOyTcT-i-ew-B8n4zR/view, "30 Ausreden", Deutsch, 2018
    * Problem: kann man auf alles anwenden
        * Situation der Obdachlosen? -> „Aber sollten wir uns nicht darum kümmern, die Situation in Syrien zu verbessern?“
        * Mehr Unterstützung für Menschen mit Behinderung? -> "Was ist mit den Arbeitern in Bekleidungsfabriken in Bangladesch?"
        * "All diese Probleme müssen gelöst werden und es wird
            nichts erreicht, wenn man eine von ihnen als weniger wichtig einstuft."
    * Man kann Veganer sind und dennoch Menschen retten.
    * Positive Auswirkungen auf Menschen wird oft übersehen:
        Resourchenknappheit, Arbeiter in Schlachthäusern (hohe Depressionsrate), etc.
    * ...
    * Nahrungskette
        * "Wer die Nahrungskette als Ausrede nutzt, nimmt die **„Macht bedeutet Recht“**-
            Position ein, bei der Menschen glauben, es sei für uns moralisch vertretbar
            andere zu versklaven und auszubeuten, weil wir die physische Fähigkeit dazu
            haben."
        * "Aber in der Position der Macht zu sein, bedeutet, dass wir die
            **Verantwortung haben, uns um die Verletzlichen zu kümmern.**"
        * "Daher **müssen wir unsere Dominanz nicht nutzen, um andere zu töten**.
            Wir können unsere Macht nutzen, um uns um
            andere zu kümmern und eine bessere Welt zu erschaffen – genau genommen
            haben wir eine moralische Verpflichtung dazu."
* siehe "Veganismus gleich Humanismus"

### Vater Steak
* Ein liebenswerter Papa sagt, er genieße gerne ab und zu ein Steak. (Tochter Referat zur Massentierhaltung)
    * Geschmack vs. Leben: Was würde er seinen dazu Kindern vermitteln/vorleben wollen?

### Milch: Kälber sterben illegal
* https://animalequality.de/blog/fuer-milch-in-deutschland-sterben-jaehrlich-hunderttausende-kaelber/

2019
----
### Ethik
* "Gesellschaftspolitischer Stammtisch: Wie viel Tier darf´s denn sein?" - https://www.youtube.com/watch?v=AALdejbrb50, 2017, 1 h 40 min
    * "Wäre Jesus heute Vegetarier?", kath. Kirche
    * Kolpinghaus Dornbirn
    * Impulsvortrag: Prof. Dr. Martin Lintner
        * Buch_: "Der Mensch und das liebe Vieh - Ethische Fragen im Umgang mit Tieren", 2017
    * Auf dem Podium:
        * Frau Jäger, Coach?, lebt vegan
        * Ing. Tobias Giesinger, Verein gegen Tierfabriken
        * Manfred Jenny, Biobauer mit Muttertierhaltung und Hofmetzger
        * Prof. Dr. Martin Lintner, Prof. für Moraltheologie, Brixen / Impulsvortrag
    * Moderation: Thomas Matt
    * Inhalt
        * ...
        * Moraltheologe isst selber max. 1x/Woche Fleisch, aber nur gutes.
            * Rechtfertigung mit der Entstehung unserer Landschaften, ..., bestimmte Vorteile für das Tier (welche?)
        * ...
        * Moderator wundert sich: eigentlich alle wissen um die Probleme, aber die Konsequenz fehlt, dann auch wirklich veg/vegan zu handeln
            * (mögliche Erklärung: an anderen Stellen fehlt auch Problembewusstsein, wie z. B. historische Schuld des Kolonialismus, Ausbeutung
                von ärmeren Ländern um Rohstoffe für unseren nicht-nachhaltigen Lebensstil; die Probleme sind weit weg und komplex zu lösen,
                aber wengistens beim Punkt Essen kann man seinen Beitrag effektiv leisten)

### Speciesism
* ["Speciesism: The Movie - Official Trailer"](https://www.youtube.com/watch?v=tJYzia6KUbs)
    * ...

* Video: ["Professor Tom Regan: An Introduction to Animal Rights"](https://www.youtube.com/watch?v=jGyCxnMdpUg), University of Heidelberg in Germany on May 24, 2006, 50 min
    * Description:
        * "In particular, some need to be convinced; some need a logical argument."
        * "Professor Regan accepts this challenge and invites others to consider the main factual and moral questions whose answers inform the conviction that animals have rights."
    * Cat-Restaurant example
        * ...
        * "natürliches" Mitgefühl zu der Katze
        * Animal rights advocates dehnen dieses Mitgefühl auch auf andere Tiere aus
    * Wie kommt es zu der Situation, dass wir bei Tieren so große Unterschiede machen?
        * manche Menschen bringen eine große Empathiefähigkeit von Natur aus mit, so dass sie schon in jungen Jahren von sich aus auf Tierprodukte verzichten, wenn sie erfahren, wie sie hergestellt werden
        * life-altering experience
            * z. B. durch neues Wissen wie "Was, die Tiere können sich noch nicht mal umdrehen?" - "Ja, leider wahr."
    * ...
    * ...

2018
----
### Jagdfleisch, Wildfleisch
* Was ist mit professionllen Jägern oder Hobbyjägern, die im Wald und im Revier am Waldrand für Ordnung sorgen und dabei z. B. Wildschweinfleisch anfällt?
    * Vorschlag: wenn es dem Jäger hauptsächlich um die Ordnung im Revier geht, könnte er das anfallende Fleisch an solche Menschen weitergeben, die ansonsten Fleisch aus herkömmlicher Tierhaltung kaufen würden. So kann der Jäger selber vegan essen und dazu beitragen die Gesamtsumme der nötigen Schlachthäuser zu reduzieren.

* siehe allgemein-zielgruppen.md / Wildbret

### Umweltverbände
* BUNDjugend: "Auf unseren BUNDjugend-Veranstaltungen verpflegen wir uns ausschließlich mit veganen Biolebensmitteln." - http://www.welt-vegan-magazin.de/tiere-umwelt/umwelt/bund-100177.html
* Nabu-Blog: "Bio, vegetarisch, vegan – wichtige Schritte für mehr Tier- und Naturschutz" - https://blogs.nabu.de/bio-vegetarisch-vegan/
* Die Forderung "ausschließlich vegan" ist selbst bei Umweltverbänden im Allgemeinen derzeit utopisch
    * ABER: der Wunsch, dass sich auch vegan lebende Menschen rundum wohl fühlen sollten, sollte bei gutem Willen leicht erfüllbar sein; und für alle anderen entsteht kein Nachteil
    * Dieser Vorschlag sollte allgemein für Zustimmung sorgen; denn es geht ja um Menschen. Keiner möchte sich ja gerne vorschreiben lassen, was er essen soll.
    * Vorteil von rein pflanzlichem Angebot bei Veranstaltungen: die Menschen - von Grund auf schwach - kommen erst gar nicht in die Versuchung, eine Auswahl zu treffen, die eher zum Nachteil von Tieren und Umwelt ist

### "Ich esse eigentlich nur Bio-Fleisch; und wenig"
* ["Wenn Du tatsächlich nur ganz wenig Biofleisch isst, Dir das aber keiner glaubt"](http://graslutscher.de/wenn-du-tatsachlich-nur-ganz-wenig-biofleisch-isst-dir-das-aber-keiner-glaubt/), 2018
    * ["Hey Veganer, ich esse auch nur ganz wenig Fleisch! (vom Metzger meines Vertrauens)"](https://www.youtube.com/watch?v=Qym6u1IKXkw), 8 min, Der Artgenosse, 2018
        * Aufzählung von Fleisch zwischendurch
        * siehe auch "vegan-freundlich sollte drin sein"
* ["Hey Veganer, ich esse nur Bio-Fleisch!"](https://www.youtube.com/watch?v=d8ZSNtEUct0), 4 min, Der Artgenosse, 2014
    * Umweltaspekt
    * Die glücklichen Tiere aus der Werbung... leider Fehlinformation
    * Wenn man jemand vorher gut behandelt hat, dann darf man ihn töten. Oder?
    * Turbokuh
    * Tiere schnell verbraucht.
    * Oft scheint es auszureichen, theoretisch das Fleisch bei einem theoretischen Vertrauens-Metzger zu kaufen
    * Interessant ist die Ausnahmenliste, die zusammenkommt

### Nicht alle vegan; aber vegan-freundlich sollte drin sein
* siehe "Hey Veganer, ich esse auch nur ganz wenig Fleisch! (vom Metzger meines Vertrauens)"
    * Feststellung: Zu viel Fleisch ist verpönt; wenig Fleisch wird anerkannt; es gilt sogar: **je weniger, desto besser; aber: gar keins? Das geht ja gar nicht.**
    * siehe auch, sogar für die DGE ist der deutsche **Durchschnitt** zu hoch
        * siehe auch DGE-Analyse von Niko Rittenau
* Folgerung: Also warum nicht dafür sorgen, dass sich Menschen, die Tierprodukte (zumindest an diesem Abend) gerne vermeiden möchten, sich rund um wohl fühlen?
* siehe auch Umweltverbände oben
* ["Hey Veganer, für euch muss man viel mehr Pflanzen anbauen!"](https://youtu.be/fqUYsc4tc_k), ::flächenfraß
    * ...
    * Flächenverbrauch und Zahlen
    * Thema Mais, siehe auch woanders
    * Zahlen: (Quelle?) "Etwa die Hälfte der in Deutschland ausgebrachten 43.000 Tonnen Pestizide geht auf den Anbau von Futtermitteln zurück."
    * ...
* ["Hey Veganer, auf vielen Agrarflächen kann man nur Viehfutter anbauen."](https://youtu.be/MvUFVNhA_n4), 3 min
    * Was ist mit dem Argument, dass es Flächen gibt, auf denen nicht viel mehr als Gras wächst und die daher kaum für den Anbau menschlicher Nahrungsmittel geeignet sind und die deshalb nur mit mit der Produktion von Tierfutter nutzbar gemacht werden können?
    * Zahlen: (Quelle?) "90 % des deutschen Grünlandes ist nicht natürlichen Ursprungs; also anthropogen; bestand vorher aus artenreichen Biotopen wie Mooren und Wäldern"
    * Deutschland: Grünland ist besser als Ackerflächen, aber noch besser sind Wälder und Moore; also warum nicht _gar keine_ Nutzung? Außer als Kohlenstoffspeicher oder Hort der Biodiversität?
    * Ja: wenn keine Tierhaltung, dann würde mehr menschliche Nahrung vom Acker kommen.
        * ABER: durch den Wegfall des Kraftfutterbedarfs würde die Gesamtfläche an benötigtem Acker weltweit sinken.
        * (alles komplex und heterogen, aber in D. relativ eindeutig)
        * Frage: "Wäre eine tierproduktfreie oder zumindest -arme Ernährungsweise in Deutschland ein sinnvoller individueller Beitrag zum Moorschutz?"
    * Frage: was ist mit Dünger, den die Bio-Tiere produzieren? Wie soll man den ersetzen? (siehe biovegan?)

2017
----
### "Puls 180: Metzgerin trifft PETA-Aktivistin und Tierschützerin", hr-fernsehen, 2017
* Video: ["Puls 180: Metzgerin trifft PETA-Aktivistin und Tierschützerin"](https://www.youtube.com/watch?v=5nQDr6z1wfg), 2017, hr-fernsehen, 45 min
    * "Sie begegnen sich, ohne zu wissen, wer die andere ist. Sie wissen nur, dass der oder die andere komplett anders tickt als sie selbst"
    * spielt in Darmstadt Innenstadt
    * YT-Comment: "Beide haben sich angestrengt, sich mit Respekt und Wertschätzung zu begegnen. Auch unangenehme Situationen hielten beide aus. Das verdient Hochachtung! Danke für dieses Format.﻿"
    * YT-Comment: "Wenn da ein brennendes Haus ist und da is n Hund drin und ein Kind drin, was würdest du retten?"
        * zuerst das Kind, danach den Hund. Aber was genau hat das mit einer veganen Lebensweise im Alltag zu tun?
    * M: "Jeder, der Fleisch und Wurst isst, muss verstehen wie und wo das herkommt"
        * "Zangenbetäubung"
        * "dann wird gestochen"
        * **"ruhige Stimmung", "die anderen drei Schweine sind kein bisschen beunruhigt"**
        * "Wenn ich als Schwein wiedergeboren werden würde, würde ich hoffen, dass ich bei uns geschlachtet werde"
            * (oder halt gar nicht)
    * Treffen im asiatischen Restaurant am Marktplatz
        * M: "5 mal / Woche Fleisch oder Wurst"
        * V: "seit fast 5 Jahren vegan", 32 Jahre alt
    * Besuch in der Drogerie für vegane Produkte
        * [Zahnpasta kann Knochenmehl enthalten](https://www.geo.de/natur/nachhaltigkeit/16227-bstr-diese-produkte-sind-nicht-vegan/222687-img-zahnpasta), geo, Datum?
    * In den Supermarkt
        * Käse; warum steht da Salami drauf?; Theke des Todes; kompletter gehäuteter Hase für 20 EUR; das Leben nur 20 EUR wert?
    * Beide haben einen Hund
        * M: Hund ist schon ein Familienmitglied, aber nich so hoch priorisiert. Beim ersten Hund große Schwierigkeiten gehabt, ihn nach langer Krankheit einzuschläfern. Andere Hunde kommen da von der emotionalen Beziehung nicht mehr ran.
        * V: sucht sich bewusst schwer vermittelbare Hund raus; würde für sie alles tun
    * Abendessen bei V mit veganer Lasagne
        * Empathie etc.
        * Mutterkuh ihr Kälbchen wegnehmen
        * Hund retten?
        * M: würde das vielleicht anders sehen, wenn sie nicht schon seit Generationen Metzger wären
    * 1 Woche später Besuch beim Metzger
        * in die Wurstküche
        * Fleisch wird zerlegt
        * V: weiß ja was vor sich geht; nimmt sie mit
        * M: "Wenn dir schlecht wird, sag Bescheid"
        * eigene Rezepte für die Wurst
        * V: ist überrascht von der Passion zum Beruf
    * Besuch von einem von zwei Betrieben, wo das Schweinefleisch herkommt
        * Schwänze gekürzt, wenig Spielzeug, relativ eng
        * V: "Wo kommen die Ferkel her? Werden die hier gezüchtet?"
            * Ferkel kommen vom Ferkelerzeugerbetrieb (dort sind die Muttersauen). Hier ist nur die Mast.
        * V: "Wie lange lebt ein Schwein, bis es sterben muss?"
            * Die Ferkel sind 11 Wochen alt, wenn sie ankommen. 115 Tage später werden sie geschlachtet. **Im Alter von 6 - 7 Monaten.**
        * V: "Gibt es da manchmal Gewissensbisse?"
            * **"Naja, man gewöhnt sich schon an die Tiere. Sie gehn halt dann weg und es kommen wieder neue."**
        * V: "Warum macht man das, obwohl man sich ja auch anders ernähren kann? Es geht ja anders."
            * "90% der Bevölkerung fragt das nach. Man kann ja auch nicht von heut auf morgen sagen, das Fleischessen muss wegfallen. Wir erzeugen das Fleisch, weil es ja auch verlangt wird. Ist ja nicht so, dass wir die Leute dazu verführen müssen, ihr müsst Fleisch essen. Wir machen nix verkehrt, wir dienen einfach der Ernährung. Weil die Leute das nachfragen."
                * (vergleiche Werbung), (also es ist mal wieder die Nachfrage)
    * Beim Bauern auf der Weide
        * Die Tiere (Rinder) sind immer auf der Weide
        * 3 Kühe davon sind 15 Jahre alt.
        * **"Die älteste war dann 18. Die ist aber dann nicht mehr trächtig geworden. Also wird sie eigentlich für uns dann uninteressant. Die Kuh, die wird dann geschlachtet."**
        * V: "Schauen Sie den Kühen in die Augen"
            * Klar. Wir kennen uns. Kennt jede Kuh und ihre Eigenschaften. Wer die Leitkuh ist. Welche Kuh welches Kalb hat.
        * V: Was geht in Ihnen vor zu wissen, dass sie das Todesurteil für die Kuh unterzeichnet haben, die sie 15 Jahre lang gekannt haben?
            * Die Kuh ist für den Betrieb und den Lebensunterhalt da. "Das ist zwiespältig.". Aber wir leben von denen.
        * V: Wenn es einen anderen Weg (ohne Kuhsterben) geben würde, würden Sie den Weg wählen?
            * "Der Weg zeichnet sich für uns eigentlich nicht auf. Ich hab keine Alternativ dazu."
    * Gespräch an der Fleischtheke
    * Besichtigung der Elektrozange, Erklärung des Schlachtprozesses:
        * erst Zange
        * dann Stich in die Halsschlagader; (wie trifft man das richtig?) darf nur von geschultem Personal durchgeführt werden
        * und dann ein paar Mal vor und zurück reinstechen
        * Eine schmerzfreie Schlachtung ist möglich, wenn man alles richtig macht.
    * Abschluss: Kaffee-Trinken bei der Metzger-Familie
    * Was gelernt?
        * mehr auf Kosmetik ohne Tierversuche achten
        * Unterschiede zu hier und Intensivtierhaltung
        * Jeder hat den für sich richtigen Weg gewählt.

### "Puls 180 - Wie tickst Du denn? AfD-Kreisvorsitzender trifft auf Friseur", hr-fernsehen, 2017
* Video: ["Puls 180 - Wie tickst Du denn? AfD-Kreisvorsitzender trifft auf Friseur"](https://www.youtube.com/watch?v=hgi7M9FVYOc), 2017, hr-fernsehen, 45 min
    * Treffen am Hauptbahnhof FFM
    * "„Puls 180“ bringt Menschen zusammen, die im Alltag nie in direkten Kontakt miteinander kommen. In der ersten Folge treffen Florian (20) aus Bad Karlshafen, Kreisvorsitzender der AfD Kassel Land, und Bob (51), Friseurmeister mit afrikanischen Wurzeln aus dem Frankfurter Bahnhofsviertel, aufeinander. Sie lassen sich auf das Treffen ein, ohne zu wissen, wer der andere ist. Sie wissen nur, dass sie jeweils einen Tag in der Lebenswelt des Anderen verbringen werden – und dass diese Welt ganz anders ist als ihre eigene..."
    * Bob und Flo
    * B: unterschiedliche Wurzeln; hat einen guten Riecher, wer der Gesprächspartner wohl sein könnte
        * Friseurladen, "Free Trump Haircut"
    * F: kommt vom Land; was ist die AfD für dich?
    * B: "Wolf im Schafsfell"
    * ...
    * Gast: Die AfD sucht immer einen Schuldigen für Probleme.
        * F: Das stimmt nicht. Wir sehen nicht nur Ausländer als eine Ursache für die Probleme von Einheimischen, sondern kritisieren auch die Politik.
            * (nicht nur)
            * gegen zu viel politische Korrektheit
            * "zu einer Demokratie gehört Streit dazu"
    * F: Hat kein Problem mit Ausländern, solange sie die Gesellschaft voranbringen.
    * 11:18 min: ...
