Gesundheit / Ernährung
======================

<!-- toc -->

- [Die Gießener vegane Lebensmittelpyramide](#die-giessener-vegane-lebensmittelpyramide)
- [UGB / Gesundheit -> Umwelt](#ugb--gesundheit---umwelt)
  * [UGB - Veganer meist besser versorgt als Mischköstler / Lebensmittelpyramide, 2020](#ugb---veganer-meist-besser-versorgt-als-mischkostler--lebensmittelpyramide-2020)
  * [UGB - Fachberater Vegane Ernährung](#ugb---fachberater-vegane-ernahrung)
  * [UGB - Interview mit The Habit Rabbit, 2019, inkl. Umwelt](#ugb---interview-mit-the-habit-rabbit-2019-inkl-umwelt)
  * [UGB - Leitzmann - Ausstieg aus der Massentierhaltung mit BUND](#ugb---leitzmann---ausstieg-aus-der-massentierhaltung-mit-bund)
- [Vegane Ernährungspyramide / Bioverfügbarkeit / Eiweißquellen](#vegane-ernahrungspyramide--bioverfugbarkeit--eiweissquellen)
- [Niko Rittenau et al](#niko-rittenau-et-al)
  * [Vegan und DGE](#vegan-und-dge)
  * [Darum empfiehlt die DGE eine vegane Ernährung nicht / Rittenau](#darum-empfiehlt-die-dge-eine-vegane-ernahrung-nicht--rittenau)
  * [Meat is not essential - why risk lives?](#meat-is-not-essential---why-risk-lives)
  * [Buch - Vegan Klischee ade, 2018](#buch---vegan-klischee-ade-2018)
  * [Niko Rittenau bei der Bundeswehr](#niko-rittenau-bei-der-bundeswehr)
  * [Vorträge](#vortrage)
- [Kinder, Kinderernährung](#kinder-kinderernahrung)
  * [Mangelernährung in Deutschland, insbesondere bei Kindern](#mangelernahrung-in-deutschland-insbesondere-bei-kindern)
  * [Kinderernährung, ZDF, VeChi-Studi](#kinderernahrung-zdf-vechi-studi)
  * [Vegetarische und vegane Ernährung, Markus Keller, VeChi](#vegetarische-und-vegane-ernahrung-markus-keller-vechi)
  * [Vegan in der Schwangerschaft? - Keine Evidenz für eine Nichtempfehlung, Apotheker vs. DGE, 2016](#vegan-in-der-schwangerschaft---keine-evidenz-fur-eine-nichtempfehlung-apotheker-vs-dge-2016)
  * [UGB - Kinderernährung](#ugb---kinderernahrung)
  * [Niko Rittenau - Kindernährung Responsevideo 2020](#niko-rittenau---kindernahrung-responsevideo-2020)
  * [bzfe.de](#bzfede)
  * [Positive Beispiele](#positive-beispiele)
  * [Unverantwortliche Eltern - Vegan-Vorurteile in den Medien](#unverantwortliche-eltern---vegan-vorurteile-in-den-medien)
  * [Extremismus-Vorwürfe, bewusst oder unbewusst, Medien](#extremismus-vorwurfe-bewusst-oder-unbewusst-medien)
  * [Ungesund und Schädigung des Kindes durch Zucker, Fettleibigkeit, Diabetes Typ 2 etc.](#ungesund-und-schadigung-des-kindes-durch-zucker-fettleibigkeit-diabetes-typ-2-etc)
- [Business-Umfeld, Unternehmensumfeld mit Technik-Background](#business-umfeld-unternehmensumfeld-mit-technik-background)
  * [Gesundheit: Workshop-Ideen, 2020](#gesundheit-workshop-ideen-2020)
  * [Gesundheit: Metzger wird vegan, kein Fleisch, voller Genuss, 2020](#gesundheit-metzger-wird-vegan-kein-fleisch-voller-genuss-2020)
  * [Gesundheit: BKK Provita, 2020](#gesundheit-bkk-provita-2020)
  * [Gesundheit: Kanada, 2019](#gesundheit-kanada-2019)
  * [Gesundheit: Game Changers, 2019 / Sport](#gesundheit-game-changers-2019--sport)
  * [Gesundheit mit Metzgern, 2019](#gesundheit-mit-metzgern-2019)
  * [Gesundheit: Schlaganfallriskio niedriger, wenn fleischlos; aber..., 2019](#gesundheit-schlaganfallriskio-niedriger-wenn-fleischlos-aber-2019)
- [Inbox 2019](#inbox-2019)
  * [Lebensmittelskandale](#lebensmittelskandale)
  * [10 Regeln der DGE](#10-regeln-der-dge)
  * [Vitamin B12](#vitamin-b12)
- [Inbox 2018](#inbox-2018)
  * [Misc](#misc)
  * [hpd-Rezension: Grundlegende Informationen zum Veganismus](#hpd-rezension-grundlegende-informationen-zum-veganismus)
- [Inbox 2017](#inbox-2017)
- [Vitamine / B12 / Natürlichkeit](#vitamine--b12--naturlichkeit)
  * [Wo ist Zink drin?](#wo-ist-zink-drin)
  * [2020](#2020)
  * [2019](#2019)
  * [2017](#2017)
- [Allgemein](#allgemein)

<!-- tocstop -->

Die Gießener vegane Lebensmittelpyramide
----------------------------------------
* 2020: Schöner Überblick https://ecodemy.de/magazin/vegane-ernaehrungspyramide-neuester-stand/

* Vegane Lebensmittelpyramide - mit der DGE (wo steht das?)
    * https://www.ernaehrungs-umschau.de/news/14-08-2018-die-giessener-vegane-lebensmittelpyramide/
        * "Auch wenn genaue Daten nicht vorliegen, dürfte die Zahl der Veganer in Deutschland in den letzten Jahren angestiegen sein.
        **Für die Umsetzung einer bedarfsdeckenden veganen Ernährung sind wissenschaftlich fundierte Ernährungsempfehlungen notwendig.**
        Vor diesem Hintergrund wurde die Gießener vegane Lebensmittelpyramide von Stine Weder, Caroline Schaefer und Markus Keller entwickelt.
        Grundlage war die berechnete Nährstoffzufuhr über einen **14-tägigen veganen Speiseplan**. Dabei lag ein besonderer Fokus darauf,
        die Referenzwerte der potenziell kritischen Nährstoffe einer veganen Ernährung (Protein, langkettige n-3-Fettsäuren, Vitamin D,
        Riboflavin, Vitamin B12, Kalzium, Eisen, Zink, Jod, Selen) zu erreichen (ab S. M422)."

UGB / Gesundheit -> Umwelt
--------------------------
UGB: über jeden Verdacht, eine "vegane Agenda" zu vertreten, erhaben.

siehe TODO

### UGB - Veganer meist besser versorgt als Mischköstler / Lebensmittelpyramide, 2020
* https://www.ugb.de/ugb-medien/einzelhefte/klimawandel-clever-handeln/die-giessener-vegane-lebensmittelpyramide/, 2020
    * "Die Gießener vegane Lebensmittelpyramide"
    * "Die derzeitige Studienlage zeigt, dass Veganer viele Ernährungsempfehlungen der Fachgesellschaften besser umsetzen als die Allgemeinbevölkerung."
    * "Grundlage für die Entwicklung der veganen Lebensmittelpyramide war die Nährstoffzufuhr über einen optimierten 14-tägigen veganen Speiseplan"
        * TODO: wie sieht der Speiseplan aus?
    * ...
    * "Ohne Supplemente geht es nicht"
        * Jod (wie bei allen), B12, Vit. D
    * ...
    * weitere Artikel unten verlinkt
    * siehe auch Statement zu guter Versorgung der Veganer von Edith Gätjen (s. Habbit Rabbit Interview)

### UGB - Fachberater Vegane Ernährung
* https://www.ugb.de/seminare/fortbildungen/vegane-vollwert-ernaehrung/
    * "Um das Thema vegane Ernährung kommt heute kein Ernährungsberater mehr herum. Schätzungsweise eine Million Menschen in Deutschland leben mittlerweile vegan – Tendenz steigend! Zwar verzeichnen vegane Kochbücher, Food-Blogs und Social-Media-Netzwerke einen unglaublichen Boom, aber fachkundige Informationen, wie man sich ohne tierische Lebensmittel optimal versorgt, sind selten."
    * "Was fehlt, sind qualifizierte Ernährungsfachkräfte, die vegan essende Menschen auf wissenschaftlicher Grundlage, objektiv, unabhängig und spezifisch für die jeweilige Lebenssituation in Theorie und Praxis beraten.
        Diese Lücke schließt die UGB-Akademie mit einer zweistufigen Fortbildung unter der Leitung von
        Edith Gätjen, Prof. Dr. Markus Keller und Dr. Günther Schwarz. Die Dozenten verfügen über langjährige Erfahrungen in veganer Vollwert-Ernährung."
    * Link zu The Habit Rabbit Interview
        * "Thomas Schumacher von "The Habit Rabbit" fragt UGB-Dozentin **Edith Gätjen** und die stellvertretende
        Geschäftsführerin des UGB Elisabeth Klumpp, wie der UGB zur veganen Ernährung
        steht und warum wir qualifizierte Fachkräfte für die vegane Ernährung brauchen."

### UGB - Interview mit The Habit Rabbit, 2019, inkl. Umwelt
* "Das sagt der UGB zu VEGANER ERNÄHRUNG", https://www.youtube.com/watch?v=wKEXModd9Zk, 2019, 6 min, Interview
    * ...
    * Veganer halten die Empfehlungen für die Hauptnährstoffe der DGE am allerehesten ein.
        * Verteilung von Eiweiß, Fett und Kohlenhydrate; Gemüse und Obst; Getreide, Nüsse
        * "was beim Mischköstler schon eher schwieriger ist"
    * "Die, die es machen _wollen_, denen können wir sagen wie es gut gehen _kann_."
        * (todo: Ableitung für AK Ernährung BUND: Keine Angst vor der veg. Ernährung / ähnliche aufgeklärte Position einnehmen?)
    * nicht empfehlen, aber "Menschen mitnehmen"
    * "vorwiegend pflanzlich orientierte Ernährung ist die Ernährung, mit der man sich am besten gesund erhalten kann"
        * aber kein Urteil, was da von den veg*-Formen jetzt am besten ist
    * Mensch übt macht über Tiere aus... "das ist nicht fair"
    * Ernährung mit Tierprodukten muss auch nicht negiert werden.
    * UGB steht für Vollwert-Ernährung => "vegan geht" heißt dann "vollwertig vegan" (inkl. B12-Substitution)
    * 1981 gegründet
    * Weiterbildung auf zwei Schienen:
        1. für jedermann, um damit arbeiten zu können,
        2. für Fachleute: Küche, Ökotrophologen, Mediziner
    * "Meine Botschaft: Macht's vegan vollwertig"
        * Lebensmittel so naturbelassen wie möglich
        * so vielseitig wie möglich
        * saisonal
        * regional
    * todo: "Welche ist die BESTE Omega 3 QUELLE? [Prof. Keller klärt auf]"

### UGB - Leitzmann - Ausstieg aus der Massentierhaltung mit BUND
* https://www.ugb.de/vollwert-ernaehrung/massentierhaltung/, 2011
    * "Prof. Claus Leitzmann, Leiter des wissenschaftlichen Beirats des UGB, unterstützt einen Appell für den Ausstieg
    aus der Massentierhaltung. Der vom Bund für Umwelt und Naturschutz (BUND) organisierte Appell wird bislang von
    485 Professoren verschiedener wissenschaftlicher Fachrichtungen getragen."
    * Unten verlinktes PDF
        * Graphik zur Entwicklung des Fleischkonsums in D. von 1950 bis heute:
            1950: 26 kg / Jahr, dann Anstieg
            1985: 66 kg / Jahr
            1995: 60 kg / Jahr (leicht gesunken), aber dann bis 2010 ca. konstant

Vegane Ernährungspyramide / Bioverfügbarkeit / Eiweißquellen
------------------------------------------------------------
* "Verbesserung der Bioverfügbarkeit": https://ecodemy.de/magazin/vegane-ernaehrungspyramide-neuester-stand/#Verbesserung_der_Bioverfuegbarkeit
    * dort auch vegane Eiweißquellen
* Kulturen auf aller Welt: Getreide und Hülsenfrüchte kombinieren

* "Im zweiten Teil zeigt Sebastian seine vielfältigen Rezepte im
    Baukastensystem, mit dessen Hilfe man durch die Kombination der
    verschiedenen Komponenten wie Getreide, Gemüse, Hülsenfrüchte
    und Nüsse optimal mit Nährstoffen versorgt wird" (https://www.nikorittenau.com/kochbuch/)
    * "Mal wird das Blumenkohlsteak mit Hirsepolenta und Curry-
        Hülsenfrüchte-Salat kombiniert, mal mit Soja-Paneer, Vollkornreis
        und Koriander-Pesto serviert."

Niko Rittenau et al
-------------------
### Vegan und DGE
DGE = Deutschlands anerkannte Fachgesellschaft für Ernährung; auf deren Expertise und Empfehlungen werden Speisepläne erstellt

* https://albert-schweitzer-stiftung.de/aktuell/dge-position-vegane-ernaehrung

### Darum empfiehlt die DGE eine vegane Ernährung nicht / Rittenau
* ["Darum empfiehlt die DGE eine vegane Ernährung nicht"](https://www.youtube.com/watch?v=Qa-OaQYUkVs), 2018, 16 min, Niko Rittenau
    * (schriftlich siehe auch: https://www.nikorittenau.com/wp-content/uploads/2019/08/VKA-Darum-empfiehlt-die-DGE-eine-vegane-Ern%C3%A4hrung-noch-nicht.pdf)
        * ... Selen ...
    * Gesellschaften
        * Deutsche und Schweizer: nein
        * andere: ja
        * Vergleiche
    * ...
    * Eigentlich ist nichts Wesentliches gegen eine vegane Ernährung einzuwenden
    * Der Mensch sollte sich informieren; das gilt für jede Ernährungsform
    * **wird bei entsprechender Information sogar als besser als der übliche Durchschnitt anerkannt**
    * ...
    * User-Comments
        * "Warum sind es eigentlich immer nur die Veganer die ihre Ernährung akribisch planen müssen? In Zeiten von krankhafter Fettleibigkeit, McDonalds und Co sind es mMn doch eher die Mischköstler die ihre Ernährung nicht ausreichend "planen".﻿"
        * "Ich finds immer ziemlich lustig wenn Leute die morgens ein Nutella Brot, mittags Mcdonalds und abends Pizza essen, sagen das eine vegane Ernährung ungesund/mangelhaft ist, nur weil man eben Vitamin B12 zuführen muss.﻿"
        * "Ich glaube die DGE hält die meisten Menschen für zu dumm und nicht fähig eine vegane Ernährung gesund umzusetzen.﻿"
    * ["Nebelnieks "Warum ich kein Veganer mehr bin" • Antwort von Niko Rittenau"](https://www.youtube.com/watch?v=IlUb6dkMQJ8), 2018, 13 min
        * Hinweis zu Selen-Zugabe in der Tiermast vs. fehlende Zugabe auf deutschen Böden (in Finnland wird das seit 1984 gemacht)
        * Jod --> jodiertes Speisesalz
    * 2020: **Ergänzung der DGE zur ihrer Position der veganen Ernährung**: https://www.ernaehrungs-umschau.de/fileadmin/Ernaehrungs-Umschau/pdfs/pdf_2020/09_20/64_72_Sonderheft_2020_DGE_Vegane_Position.pdf. Ganz am Ende heißt es dort:
        "Es ist den Autorinnen und Autoren jedoch wichtig, **nicht nur eine auf Defizite ausgerichtete Sichtweise aufzuzeigen,**
        sondern auch darauf zu ver­weisen, dass Hinweise auf positive gesundheitliche Effekte vorliegen,
        diese jedoch einer systematischen Analyse unterzogen werden sollten."

* => eine vegetarische Ernährung ist gut möglich; eine vegane bei guter Information auch möglich
    * => z. B. **Fisch ist nicht essentiell**; z. B. Fleisch ist nicht essentiell

### Meat is not essential - why risk lives?
* "Meat is not essential. Why are we killing for it?", https://www.washingtonpost.com/opinions/2020/05/11/jonathan-safran-foer-meat-is-not-essential-why-are-we-killing-it/?arc404=true, 2020
    * ...
    * "Trump’s “order” was, in fact, the result of meat industry executives requesting his relief from legal liability for worker deaths. The number of slaughterhouse workers who have already died this year is on par with the number of U.S. servicemembers who have died annually fighting in Afghanistan over the last five years."
    * ...
    * "The industry has continued such cruel practices with relative impunity, because workers are too dependent on their jobs to effectively resist unscrupulous managers, and the public has continued to underwrite the abuse"

* "Meat Is Not 'Essential' - Yet We Are Risking Lives To Produce It", https://www.plantbasednews.org/opinion/meat-not-essential-risking-lives-produce-it, 2020
    * ...

### Buch - Vegan Klischee ade, 2018
* Buch: Vegan Klischee ade, 2018
    * Bestseller: https://de.wikipedia.org/wiki/Niko_Rittenau
    * positiv von der BILD rezipiert; Vorsicht, siehe "Die Zerstörung der Presse" (https://www.youtube.com/watch?v=hkncijUZGKA, 2020, 1 h)

### Niko Rittenau bei der Bundeswehr
* https://so-gesund.com/niko-rittenau-warum-vegan/ (ca. 2018)
    * "Was haben Sie bei ihrer eigenen Umstellung besonders vermisst?"
        * " **Käse**, vor allem Parmesan. Aber **unsere Geschmacksnerven ändern sich bei einer Umstellung innerhalb von vier bis sechs Wochen**, und danach hört das Verlangen in den allermeisten Fällen auf. Natürlich hatte ich anfangs auch mal Rückschläge. Einmal habe ich dann auch mal wieder Käse gegessen und gemerkt, dass es mir der Geschmack nicht wert ist, dass dafür Tiere und die Umwelt in Mitleidenschaft gezogen werden."

### Vorträge
* [""Vegan isst gesund" Vortrag von Rittenau | feelWell Festival Berlin 2017"](https://www.youtube.com/watch?v=H1XXuosJhkY), 45 min
    * ...
    * Vegan kann sehr gesund, aber auch aus Junk-Food bestehen
    * ...
    * 10:35: Häufigkeit eines Nährstoffmangels in verschiedenen Ernährungsformen
        * vegan:
            * Zink 50 %   --> Wo?
            * Jod  75 %   --> Salz
            * B2   25 %
    * ...
    * ... todo

Kinder, Kinderernährung
-----------------------
### Mangelernährung in Deutschland, insbesondere bei Kindern
* ["Immer mehr Kinder in Deutschland von Mangelernährung betroffen"](https://www.merkur.de/leben/gesundheit/immer-mehr-kinder-deutschland-mangelernaehrung-betroffen-zr-9945325.html), 2018
    * "Ein Chefarzt schlägt Alarm: Allein in Deutschland seien mehr als 1,5 Millionen Menschen von Mangelernährung betroffen. Auch Kinder trifft es immer häufiger."
    * ""Jeder vierte Patient, der stationär in einem deutschen Krankenhaus aufgenommen wird, hat Zeichen einer Mangelernährung", sagt der Mediziner, der den Kongress "Ernährung 2018" (21. bis 23. Juni, Kongress Palais in Kassel) als Kongresspräsident der Deutschen Gesellschaft für Ernährungsmedizin leitet."
    * "Immer häufiger zählten aber auch Kinder zu den Betroffenen - vor allem aus sozial schwachen Familien."
        * (auch eine "Normalkost" scheint also "gut geplant" sein zu müssen, um Mangel vorzubeugen)
        * (die Lösung ist also nicht, Veganer zu verunglimpfen, sondern für sozialen Ausgleich und Ernährungsbildung zu sorgen!)
    * https://www.hna.de/gesundheit/mangelernaehrung-jeder-vierte-krankenhauspatient-betroffen-9945192.html, 2018
        * !!! "„Im Gegensatz zum Übergewicht kann man frühzeitig diagnostizierte Mangelernährung mit einfachen, ernährungstherapeutischen Möglichkeiten effektiv und nachhaltig behandeln“, betont Löser."
* https://www.aerzteblatt.de/archiv/79795/Unter-und-Mangelernaehrung-im-Krankenhaus, 2010
    * "In kaum einem Teilgebiet der Medizin sind in den letzten Jahren so fundamentale Paradigmenwechsel vollzogen worden, wie auf dem Gebiet der Ernährungsmedizin. Gezielte Ernährung wird heute nicht mehr als Befriedigung eines Grundbedürfnisses betrachtet, sondern ist für den Arzt Teil seiner therapeutischen und präventiven Behandlungsoptionen."
    * Ursachen
        * (sehr viele verschiedene, Veganismus ist nicht darunter)
* https://www.faz.net/aktuell/wirtschaft/f-a-s-exklusiv-viele-krankenhaus-patienten-sind-mangelernaehrt-15159061.html, 2017
    * "Patienten in Deutschlands Krankenhäusern essen nicht genug. Jeder vierte kommt schon mangel- oder unterernährt in die Klinik."
    * "Selbst viele anfangs normal ernährte Patienten zeigen nach dem Aufenthalt im Krankenhaus Anzeichen von Mangel- oder Unterernährung. Das behindert ihre Genesung erheblich."
    * "Auf bis zu 5 Milliarden Euro veranschlagt die Fachgesellschaft die von Mangelernährung im Krankenhaus verursachten Mehrkosten in einem Konzeptpapier"
        * "Die Summe entspricht mehr als dem Doppelten der jährlichen Gesamtausgaben der deutschen Kliniken für die Verpflegung ihrer Patienten."
    * am Essen wird gespart
    * (ein Riesenproblem, das sehr viele Menschen betrifft. Warum wird das nicht öfter thematisiert?)
* !!! https://www.daserste.de/information/wirtschaft-boerse/plusminus/sendung/mangel-ernaehrung-110.html, 2019
    * "Mehr Komplikationen durch Mangelernährung"
    * Andere Länder: "In den Niederlanden ist das obligatorisch. Wer im Nachbarland ins Krankenhaus kommt, wird nicht nur wegen seiner eigentlichen Krankheit untersucht. In einem speziellen Ernährungsscreening wird er auch gefragt, ob er in den vergangenen Monaten ungewollte Gewicht verloren hat oder über verminderten Appetit klagt. Das ist Teil eines ernährungsmedizinischen Screenings."
    * ""Wenn man das richtig macht, liegt der Patient kürzere Zeit im Krankenhaus", erklärt Marian de van der Schueren. Die Wissenschaftlerin hat die niederländische Regierung davon überzeugt, dass die Ernährung ein wichtiger Baustein im Heilungsprozess ist."
    * "Investiere man pro Patient 75 Euro in die ernährungswissenschaftliche Betreuung eines Patienten, spare man einen ganzen Tag Aufenthalt im Krankenhaus"
    * Kostendruck bei den Kliniken: "Laut einer Studie gaben die Krankenhäuser in den vergangenen Jahren durchschnittlich 14 Prozent weniger pro Patient und Tag aus."
* Magersucht
    * https://www.merkur.de/leben/gesundheit/schock-haben-zwoelf-jahre-magersucht-junger-frau-gemacht-zr-9629021.html
    * http://www.magersucht.de/krankheit/zahlen.php
        * "Etwa 5 Millionen Frauen und Männer in Deutschland leiden an Essstörungen, davon haben 3,7 Millionen gefährliches Untergewicht.
        * 100 000 Menschen, insbesondere Frauen leiden demnach an Magersucht.
        * 600 000 Frauen und Männer haben Bulimie. Die Zahl der Magersüchtigen verdreifachte sich in den letzten zehn Jahren."
        * "Schönheitsideal oder Schlankheitswahn?"

### Kinderernährung, ZDF, VeChi-Studi
* "Vegane Ernährung bei Kindern" - https://www.zdf.de/verbraucher/volle-kanne/vegane-ernaehrung-bei-kindern-100.html - 2018, 4 min Video
    * "Kein Fleisch, keine Milchprodukte, keine Eier: Immer mehr Familien ernähren sich vegan. Bei vielen Nicht-Veganern stößt diese Ernährungsweise auf Unverständnis. Eine neue Studie stärkt den Eltern den Rücken."
    * Ergebnisse der VeChi-Studie

### Vegetarische und vegane Ernährung, Markus Keller, VeChi
* ["Vegetarische und vegane Ernährung funktioniert: FHM-Hochschullehrer Dr. Markus Keller stellt erste Ergebnisse der VeChi-Studie in Berlin vor"](https://www.fh-mittelstand.de/fhm/news-hochschulinformationen/artikel/vegetarische-und-vegane-ernaehrung-funktioniert-fhm-hochschullehrer-dr-markus-keller-stellt-erste-e/), 2018
    * ...
    * Kernaussagen: ...
* Artikel: https://www.essen-und-trinken.de/vegetarische-rezepte/78518-rtkl-ernaehrungswissenschaftler-dr-markus-keller-ueber-vegetarische

### Vegan in der Schwangerschaft? - Keine Evidenz für eine Nichtempfehlung, Apotheker vs. DGE, 2016
* Artikel: ["Vegan in der Schwangerschaft?"](https://www.deutsche-apotheker-zeitung.de/news/artikel/2016/11/09/vegan-in-der-schwangerschaft), Deutsche Apotheker Zeitung, 2016
    * "Für Schwangere wird eine vegane Ernährung nicht empfohlen. So steht es jedenfalls in einem Positionspapier der Deutschen Gesellschaft für Ernährung.
    Evidenz für diese Empfehlung gibt es allerdings keine – die amerikanische Fachgesellschaft kommt denn auch zu einem anderen Ergebnis."
    * "Die Deutsche Gesellschaft für Ernährung (DGE) und die amerikanischen Fachgesellschaften vertreten zu dieser Thematik unterschiedliche Auffassungen – obwohl die jeweiligen Positionen auf derselben Publikation basieren."
    * "Nicht mehr Komplikationen bei Veganerinnen"
        * "dass heterogene Daten die Aussage limitieren. Aber innerhalb dieser Grenzen könne man eine vegan-vegetarische Ernährungsweise in der Schwangerschaft als sicher betrachten – unter der Voraussetzung, dass der Bedarf an Vitaminen und Spurenelementen gedeckt wird"
            * [Vitaminpräpatate: Ökotest](https://www.deutsche-apotheker-zeitung.de/news/artikel/2015/04/24/Okotest-Fast-alle-Produkte-ungenugend) hält nur bei B12 zur Vorbeugung den Nutzen als belegt: "Ökotest rät daher, bei rein veganer oder vegetarischer Ernährung mit wenig Milchprodukten Vitamin B12 mit Monopräparaten [...] oder mit angereicherten Lebensmitteln zu supplementieren. Und zwar prophylaktisch, nicht erst wenn ein Mangel auftritt."
    * "„Vegane Ernährung ist für jede Phase des Lebens geeignet""
    * "Für eine komplette Ablehnung der veganen Ernährung in der Schwangerschaft fehlt zwar laut Zieglmeier die Evidenz. Die Haltung der DGE lässt sich aber mit angemessener Vorsicht begründen, weil die Datenlage nicht homogen ist."

### UGB - Kinderernährung
* https://www.ugb.de/presse/pressemitteilungen/stellungnahme-vegan-kinder/, Jahr?
    * "UGB-Stellungnahme: Wenn vegan – dann aber richtig!"
    * "Immer häufiger kommen Zweifel auf, ob mit veganer Ernährung eine ausreichende Zufuhr von Nährstoffen möglich ist."
        * -> Seminar nötig, dann passt das

### Niko Rittenau - Kindernährung Responsevideo 2020
* YT: ""Vegane Ernährung für Kinder gehört verboten!" - Dr. Stefan Eber • Reaktionsvideo", 2020, 25 min
    * ... gut widerlegt ...
    * Vorsicht Celebrities
        * Jon Venus doch wieder vegan
        * Cyrus
    * u. a. Eisenmythos aufgeklärt
    * inkl. Verweis auf Dr. Sven Armbrust, der auch Falschaussagen tätigt, siehe oben
    * inkl. Hinweis auf https://vegped.com/
        * Carolin Wiedmann, Fachärztin für Kinder- und Jugendmedizin, Ernährungsmedizin (DGKJ)
    * ...
    * **Beispiel eines zweifachen veganen Vaters, der auch Unternehmer ist (SIRPLUS)**
    * Beispiel eine veganen Mutter; ganz "normales" Kind

### bzfe.de
* "Vegane Ernährung für Kleinkinder ungeeignet
 - Nährstoffmangel gefährdet Gesundheit des Kindes"
    * https://www.bzfe.de/inhalt/vegane-ernaehrung-3664.html
    * Orig-Quelle nicht mehr erreichbar
    * "Kleinkind ab Beginn des 2. bis zum vollendeten 3. Lebensjahr"
    * => Für Kinder also geeignet?

### Positive Beispiele
* https://www.lilies-diary.com/kinder-vegan-ernaehren/, 2019
    * "Kinder vegan ernähren – 7 Tipps für die richtige vegane Kinderernährung"
* siehe Video "Vegane Ernährung: Leben ohne tierische Produkte | Unser Land | BR Fernsehen", 2016
* siehe Doku "Vegan – von Menschen und anderen Tieren"
* siehe eigenes Umfeld
* siehe Internet
* siehe Kinderärzte
* siehe Produkte zu "Vegane Säuglingsnahrung" im Internet (z. B. tofufamily), falls man nicht stillen kann
    * spezielle Rezeptur / auch Säuglingsnahrung auf Kuh-Milchbasis ist speziell industriell gefertigt
* siehe auch "Darum empfiehlt die DGE eine vegane Ernährung nicht"
* https://vegawatt.de/vegan-leben/ab-welchem-alter-kann-sich-ein-kind-vegan-ernaehren
    * "Als Veganer sind wir es eigentlich gewohnt uns immer mal wieder für unsere Ernährung rechtfertigen zu müssen. Schwierig wird es allerdings, wenn vegane Eltern der Vernachlässigung und bewussten Mangelernährung bezichtigt werden, die ihren Kindern eine vegane Ernährung „aufzwingen“ würden. Dann wird das Thema plötzlich hochemotional und geradezu ideologisch geführt."
    * ...
    * "Eine gesunde vegane Ernährung für Kinder hat viele Vorteile. Sie trägt zum Schutz vor chronischen Erkrankungen im späteren Leben bei. Sie beugt späterem Übergewicht vor. Und sie ist deutlich gesünder als die übliche Durchschnittskost der Nicht-Veganer. Die Gefahr der von vielen Kritikern veganer Ernährung heraufbeschworenen sogenannten Mangelernährung besteht nur, wenn sich die Kinder nicht ausgewogen ernähren."

### Unverantwortliche Eltern - Vegan-Vorurteile in den Medien
* 2020 - Ostsee-Arzt
    * https://graslutscher.de/wie-ein-kardiologe-der-ostsee-zeitung-erzaehlt-vegetarismus-sei-unsinnig-weil-soja-zu-wenig-protein-enthaelt/
    * "Vegane Ernährung bei Kindern ist Körperverletzung • Reaktionsvideo zu Dr. Sven Armbrust" - https://www.youtube.com/watch?v=HyiGAQ_se5M, 2020, Rittenau, 14 min

* https://www.sueddeutsche.de/panorama/vegan-baby-australien-1.4572314, 2019
    * "Gericht verurteilt Eltern von vegan ernährtem Baby"
    * "Ein Elternpaar wurde in Sydney zu 300 Stunden gemeinnütziger Arbeit verurteilt."
    * "Es hatte seine Tochter streng vegan ernährt. Mit 19 Monaten hatte das Mädchen noch keinen Zahn und wog nicht einmal fünf Kilogramm."
        * "streng vegan" und vor allem falsch
            * denn: Vegan kann sehr gesund, aber auch aus Junk-Food bestehen.
    * "Die Vegane Gesellschaft Deutschland wirft den Eltern Unwissenheit vor."
        * ""Reismilch ist definitiv die schlechteste Pflanzenmilch, die man einem Kind geben kann", sagte Vagedes. Daran sehe man, dass die Eltern nicht informiert waren."
        * "Wer sein Kind vegan ernähre, müsse sehr genau darauf achten, dass die Nahrung alle wichtigen Nährstoffe enthalte. Muttermilch, oder ein adäquater Ersatz mit entsprechend viel Vitamin B12, sei unerlässlich. Sonst schade man der Gesundheit des Kindes. Das werde dann mit Recht kritisch gesehen und bestraft."
        * "Der Veganer-Vorsitzende beklagte aber auch, dass der aktuelle Fall aus Australien dazu missbraucht werde, um gegen vegane Ernährung insgesamt vorzugehen. Grundsätzlich sei eine solche Ernährung von Kindern gut möglich."
    * "Sollten sich Eltern dennoch für Ernährung ohne tierische Produkte entscheiden, sei eine regelmäßige Kontrolle des Blutes notwendig. Nur so könnten eventuelle Mängel erkannt und die richtige Dosierung der Zusatzpräparate bestimmt werden."
    * User:
        * FB-Comment: "Was kommt beim Mainstream-Headlines-Leser denn an? Vegan = Mangel. Und nichts anderes wollen sie doch. Als vegane Mutter wird mir bei all diesen Artikeln schlecht."
        * laut Gr.l.: "Kind war schlicht massiv unterernährt und seit seiner Geburt nicht ein Mal beim Arzt. Die Vermutung liegt nahe, dass dieses Paar auch mit einer unveganen Ernährung irgendwann vor einer Richterin gelandet wäre. Passiert leider auch regelmäßig, wird aber medial kaum beachtet. Im Urteil kommt das Wort "vegan" dann auch nicht vor, Richterin Sarah Huggett kritisierte die Eltern wegen "putting her on a "completely inadequate" diet."

* In ähnlichen Nachhrichten: Verletzungen und Todesfälle durch Fahrradfahren
    * z. B. https://www.focus.de/gesundheit/familiengesundheit/warnung-einer-krankenschwester-krankenschwester-warnt-ich-sehe-taeglich-was-passiert-wenn-eltern-keinen-helm-tragen_id_9335531.html
    * https://www.haufe.de/recht/familien-erbrecht/aufsichtspflichtverletzung-der-eltern-bei-fahrradunfall-ihrer-fuen_220_78858.html
    * Sollten unverantwortliche Einzelfälle oder schlichte Unglücke hergenommen werden, um Fahrradfahren an sich zu diskreditieren?

### Extremismus-Vorwürfe, bewusst oder unbewusst, Medien
* https://www.augsburger-allgemeine.de/wissenschaft/Kinderarzt-warnt-Vegane-Ernaehrung-ist-gefaehrlich-fuer-Kleinkinder-id54271261.html, 2019
    * "Kinderarzt warnt: Vegane Ernährung ist gefährlich für Kleinkinder"
    * "Die meisten Mütter und Väter lenken ein, wenn ich ihnen von den Risiken erzähle. Nur wenige Eltern sind so extrem und halten an ihrer strikten Ernährung fest."
        * richtig gemacht geht es aber: "Dabei gibt es durchaus Präparate, die man begleitend einnehmen kann. Doch sie müssen richtig dosiert werden, das ist ganz wichtig."

* ["Unterernährte Kinder sind der Presse egal, es sei denn, die Eltern sind vegan lebende Hardcore-Esoteriker"](https://graslutscher.de/unterernaehrte-kinder-sind-der-presse-egal-es-sei-denn-die-eltern-sind-vegan-lebende-hardcore-esoteriker/), 2019
    * es taucht immer wieder der Begriff "streng vegan" auf

* ["Skandal, veganes Kind unterernährt! Unterernährung mit Hackfleisch ist viel verantwortungsvoller!"](http://graslutscher.de/skandal-veganes-kind-unterernahrt-unterernahrung-mit-hackfleisch-ist-viel-verantwortungsvoller/), 2017

* Vegan ungleich "gesund" (ausgewogen vollwertig, siehe Ernährungswissenschaft) oder "ungesund" (nur Bier, Chips und Süßigkeiten)
    * "Individuelle Mobilität ist tödlich, siehe Autounfälle"
    * "Umweltfreundliche Mobilität ist tödlich, siehe Fahrradunfälle"
    * "Bahnfahrer mitverantwortlich für Tode, siehe Suizide"

* ["Extremer Veganismus schädigt das Hirn"](https://www.heise.de/tp/features/Extremer-Veganismus-schaedigt-das-Hirn-3363225.html), 2014
    * Was genau bedeutet extrem? Vielleicht hat das eine mit dem anderen nichts zu tun?
    * ["Kinder vegan zu ernähren, ist unmoralisch"](https://www.tagesanzeiger.ch/schweiz/standard/Kinder-vegan-zu-ernaehren-ist-unmoralisch/story/28522474), 2011
        * "Eltern ernähren ihre Kinder vegan und verweigern ihnen zusätzliches Vitamin B 12"
            * das ist fahrlässig und nicht die Regel
        * "Eigentlich müssten die Eltern ihre Kinder gemäss den Werten erziehen, die in der Gesellschaft vorherrschen."
            * Echt? Ui. Und was ist mit den Werten, Tiere und die Natur zu achten? Darf man das jetzt auch nicht mehr vermitteln?

### Ungesund und Schädigung des Kindes durch Zucker, Fettleibigkeit, Diabetes Typ 2 etc.
* siehe oben: „Im Gegensatz zum Übergewicht kann man frühzeitig diagnostizierte Mangelernährung mit einfachen, ernährungstherapeutischen Möglichkeiten effektiv und nachhaltig behandeln“
    * das heißt, wer mit dem Kind regelmäßig zum Arzt geht, handelt verantwortungsvoll und im Sinne des Kindes (auch bei veganer Ernährung)
* Es gibt erstaulich wenige Urteile gegenüber Eltern, die ihr Kind zur Fettleibigkeit erziehen, obwohl das die Lebensqualität stark einschränkt.
    * Was tun?
* ["Warum Eltern oft Schuld an den Extrapfunden ihrer Kinder haben"](https://rp-online.de/leben/gesundheit/medizin/kinderkrankheiten/adipositas-bei-kindern-das-sind-die-ursachen_aid-20622861), 2017
    * "Doch je länger ein Mensch ein zu hohes Gewicht hat, desto mehr Gesundheitsprobleme entwickelt er."
* ["Woher kommt Über­ge­wicht?"](https://www.tk.de/techniker/magazin/ernaehrung/uebergewicht-und-diaet/uebergewicht-ursachen-2006778), 2017
    * "Andererseits prägen Eltern, Geschwister und andere Bezugspersonen Kinder maßgeblich in ihrer Ernährungsweise. So haben Kinder dicker Eltern ein sehr hohes Risiko, auch übergewichtig zu werden. Und das liegt nicht nur an den Genen, sondern auch an der Tatsache, dass die Kinder die meist zu kalorienreiche Ernährung ihrer Eltern nachahmen."
* https://www.diabetesinformationsdienst-muenchen.de/aktuelles/nachrichten/nachrichten-aus-der-diabetesforschung/news/article/gleichbleibend-hoch-uebergewicht-bei-kindern-und-jugendlichen-in-deutschland//index.html, 2018
    * "Gleichbleibend hoch: Übergewicht bei Kindern und Jugendlichen in Deutschland"
* "Diabetes mellitus und Übergewicht - Häufigkeit des Kinder-Diabetes verdoppelt", 2004
    * https://www.diabetes-deutschland.de/archiv/3805.htm
    * !!! "Übergewicht und Typ 2 Diabetes als Gesundheitsproblem in Deutschland [...] Wenn nicht deutliche Fortschritte in der Prävention und Therapie des metabolischen Syndroms gemacht werden, so steht zu befürchten, dass die jüngere Generation wegen ihrer Lebensgewohnheiten nicht mehr so alt werden kann wie ihre Eltern."
    * "Durch ein verändertes Ernährungs- und Bewegungsverhalten werden weltweit immer mehr Kinder und Jugendliche übergewichtig. Man geht davon aus, dass bereits rund 22 Millionen Kinder unter fünf Jahren an Übergewicht leiden."
    * "In Deutschland ist jedes fünfte Kind und jeder dritte Jugendliche übergewichtig. Vier bis acht Prozent aller Schulkinder sind sogar adipös [Anm.d.Red.: stark übergewichtig]."
    * "Neben der damit verbunden psychischen Belastung beobachtet man bei Kindern mit Adipositas eine Reihe von Folgeerkrankungen, die früher erst im Erwachsenenalter auftraten. So findet hier zum Beispiel Schäden an den Gelenken und der Wirbelsäule, Bluthochdruck, Fettstoffwechselstörungen sowie erhöhte Harnsäurewerte. Außerdem ist die steigende Zahl adipöser Kinder mit einem Auftreten von Diabetes mellitus Typ 2 („Altersdiabetes“) verbunden, der bislang nur bei erwachsenen Menschen mit Übergewicht gesehen wurde."
* ["Übergewichtige Eltern – übergewichtige Kinder?"](https://www.bzfe.de/inhalt/uebergewichtige-eltern-uebergewichtige-kinder-29777.html), 2017
    * "Studie bestätigt Zusammenhang"
* Zucker
    * siehe zucker-README.md / "Das Zucker-Problem / Kinder" / "Überzuckerte Getränke aktiv meiden"
* Hochverarbeitete Lebensmittel
    * siehe zucker-README.md
* zuwenig Bewegung, zuviel McDonalds

Business-Umfeld, Unternehmensumfeld mit Technik-Background
----------------------------------------------------------
siehe auch essen-2.md

### Gesundheit: Workshop-Ideen, 2020
- Gesundheit und Umwelt im Team
- Angebote und Erfahrungsaustausch (keine Beratung)
- **Zucker**-Vortrag mit Herzkrankheiten, Lobby (Cola, Nestle Kinder etc.) (wer verdient am falschen Ernährungswissen?), siehe Zucker-Doku
- **HR-Funkkolleg Ernährung**: https://www.hr-inforadio.de/podcast/funkkolleg-ernaehrung/index.html, 2019 - 2020
    - guter Input
- Vollwertige pflanzliche Ernährung
    - Niko Rittenau einladen
        - Vorbild Mercedes "Die Zukunft is(s)t nachhaltig - Mercedes-Benz EQ & Niko Rittenau" (Video)
    - ProVeg
    - Vegane Mythen
- Low Carb?
    * siehe HR-Funkkolleg Ernährung
    * https://veggieworld.de/015-high-carb-ist-das-neue-low-carb-niko-rittenau-bonus-vegablum/
    * Paelo/LowCarb-Lehre sagt oftmals: vegan = nicht gesund bzw. schleichend gefährlich, z. B.
        * https://www.cleaneatingkitchen.com/vegan-diet-dangers-health/, "extreme" etc., siehe auch Gesundheitsgeschichte des Autors im Artikel unten
        * vs. BKK ProVita
        * vs. Vegane Kita in FFM (alles approved)

* "Natürlichkeit?" - Diese Frage kann in einem Umfeld, das geprägt ist von Technik und
    sonstigen "Unnatürlichkeiten" wie Bürostühlen etc. als weniger relevant angesehen werden.
    * unnatürliche Tierhaltung, siehe Schweinehaltung fürs Schnitzel
        * die Mehrheit scheint damit kein Problem zu haben. Meinung?
    * B12? - Viele Menschen sehen die Vorteile einer veganen Ernährung, aber haben Probleme mit einer potenziellen B12-Ergänzung. Dazu...
        * Erinnerung an die Geschichte vom Jod-Salz   --> Deutschland kein Jod-Mangelgebiet mehr
        * Amerikanischer Raum: B12 überall zugesetzt  --> kein B12-Mangelgebiet mehr
        * vergleiche Mikronährstoffe (inkl. B12) plus Medikamente in unserer Tierhaltung
        * B12-Suppl. pauschal ablehnen? => Jodsalz ablehnen
            * das scheint die Mehrheit aber auch nicht zu tun. Meinung?
* (Der Verzicht auf Tierprodukte mit allen größeren Religionen kompaptibel und hat daher ein hohes Diversity-Potential.)
- Vegan: Umwelt, Gesundheit und Tierwohl mit Genuss
- weiteres
    - Nachhaltigkeitsselbsthilfegruppe - die Natur braucht uns, aber keinen interessiert es - treffe Gleichgesinnte

### Gesundheit: Metzger wird vegan, kein Fleisch, voller Genuss, 2020
* siehe Bio-Spahn => Fleisch in großen Mengen ist ungesund. Reduktion auf Null ist machbar, ohne Genussverzicht
    * https://www.youtube.com/watch?v=aoT23UkoZQ8, 2019, 10 min
        * "Als Metzger auf tierische Produkte verzichten? Klingt ziemlich kontrovers,
            doch Metzgermeister Michael Span lebt tatsächlich vegan! "Galileo" hat ihn einen Tag lang begleitet
            und herausgefunden, wie ihm der Spagat zwischen Arbeit und Lebensweise gelingt."
        * Alternative zum Beyond-Patty mit weniger Zusatzstoffen

### Gesundheit: BKK Provita, 2020
* siehe "BKK Provita + Gemeinwohlökonomie-Video"

### Gesundheit: Kanada, 2019
* siehe "Kanada streicht Milch- und Milchprodukte fast komplett aus der offiziellen Ernährungsempfehlung"
    * dort auch "Marketing can influence your food choices"

### Gesundheit: Game Changers, 2019 / Sport
* https://gamechangersmovie.com/the-film/
    * Resonanz bei englisch-sprachiger Fitness-Gruppe?
    * Lewis Hamilton als F1-Beispiel
    * Menschen vollbringen körperliche Höchstleistungen => wäre der Ausbau eines leckeren pflanzlichen Angebots nicht auch was für die Menschen, die für den Konzern körperliche Leistungen vollbringen, um sie bestmöglich zu unterstützen?
        * Pflanzenbasierte Nahrung für Gewinner!
    * Vorleben durch die Führung
    * "Human Ancestors Were Nearly All Vegetarians", https://blogs.scientificamerican.com/guest-blog/human-ancestors-were-nearly-all-vegetarians/, 2012
    * https://en.wikipedia.org/wiki/Exponent_(consulting_firm)

* Beispiele von Top-Athleten: https://www.greatveganathletes.com/

### Gesundheit mit Metzgern, 2019
* Bio Spahn vegan wegen Gesundheit
* http://der-vegetarische-metzger.de/en/homepage/

### Gesundheit: Schlaganfallriskio niedriger, wenn fleischlos; aber..., 2019
* https://healthcare-in-europe.com/de/news/fleischlose-ernaehrung-verhindert-schlaganfaelle.html, 2019
    * ""The BMJ" (ehemals British Medical Journal)"
    * "Noch immer sind in Deutschland Schlaganfälle die dritthäufigste Todesursache."
    * https://www.bmj.com/content/366/bmj.l4897
        * "Conclusions In this prospective cohort in the UK, fish eaters and vegetarians had lower rates of ischaemic heart disease than meat eaters, although vegetarians had higher rates of haemorrhagic and total stroke."
            * total stroke?
        * "Additional studies in other large scale cohorts with a high proportion of non-meat eaters are needed to confirm the generalisability of these results and assess their relevance for clinical practice and public health."
        * "This study showed that fish eaters and vegetarians (including vegans) had lower risks of ischaemic heart disease than meat eaters"
        * "Vegetarians (including vegans) had higher risks of haemorrhagic and total stroke than meat eaters"
    * https://www.gesundheit.gv.at/krankheiten/gehirn-nerven/schlaganfall/formen, 2017
        * "Ischämischer Schlaganfall – der „echte“ Schlaganfall"
            * "Der ischämische Schlaganfall kommt deutlich häufiger vor." (https://www.focus.de/gesundheit/lexikon/krankheiten/schlaganfall_aid_232979.html)
        * "Hämorrhagischer Schlaganfall – die Hirnblutung"
            * "Diese Form des Schlaganfalls kommt meistens bei Personen mit lange vorbestehendem Bluthochdruck vor."

Inbox 2019
----------
### Lebensmittelskandale
* z. B. 2 Tote durch Listerienvergiftung in Wurstwaren. Firma meldet Insolvenz an

### 10 Regeln der DGE
https://www.dge.de/ernaehrungspraxis/vollwertige-ernaehrung/10-regeln-der-dge/

    Punkt 1:
    "Nutzen Sie die Lebensmittelvielfalt und essen Sie abwechslungsreich. Wählen Sie überwiegend pflanzliche Lebensmittel."
    "Um die ausreichende Versorgung mit Nährstoffen zu erleichtern, ist es sinnvoll, die pflanzlichen Lebensmittel durch tierische Lebensmittel wie Milch, Milchprodukte, Fisch, Fleisch und Eier zu ergänzen."

    Punkte 4:
    "Nur tierische Lebensmittel enthalten in nennenswerten Mengen verfügbares Vitamin B12. Wer wenig oder gar keine tierischen Lebensmittel isst, muss darauf achten, Vitamin B12 zusätzlich einzunehmen."

    Punkt 5 (Gesundheitsfördernde Fette):
    "Bevorzugen Sie pflanzliche Öle wie beispielsweise Rapsöl und daraus hergestellte Streichfette."

### Vitamin B12
* https://www.aerzteblatt.de/nachrichten/100555/Referenzwert-fuer-Vitamin-B12-Zufuhr-um-ein-Drittel-angehoben
    * "Du-musst-aber-Vitamin-B12-Präparate-einnehmen"-Abschreckungsszenrio.
    * Artikel erwähnt nicht: dass unsere Tiere ihr B12 heute größtenteils auch über Präparate beziehen
    * siehe unten

Inbox 2018
----------
### Misc
* Vegane Lebensmittelpyramide mit der DGE, siehe oben
* Darum empfiehlt die DGE eine vegane Ernährung nicht, siehe oben
* Niko Rittenau: Vegan-Klischee ade, siehe dort

### hpd-Rezension: Grundlegende Informationen zum Veganismus
* "Der bekannte Ernährungswissenschaftler Claus Leitzmann [...]", siehe dort

* ["Deutschlands erster Vegan-Professor "Vegane Eltern stehen unter Beobachtung""](https://www.geo.de/natur/nachhaltigkeit/19473-rtkl-deutschlands-erster-vegan-professor-vegane-eltern-stehen-unter), 201x?
* Empfehlung: kein verarbeitetes Fleisch in Krankenhäusern
    * 2017: https://ama.com.au/ausmed/american-doctors-urge-hospitals-stop-serving-processed-meat-patients
        * "The American Medical Association’s House of Delegates recently adopted a resolution calling on hospitals to remove processed meats from their menus and instead provide plant-based meals."

Inbox 2017
----------
* Vegan und DGE, siehe oben
* siehe oben: ""Vegan isst gesund" Vortrag von Rittenau | feelWell Festival Berlin 2017

Vitamine / B12 / Natürlichkeit
------------------------------
### Wo ist Zink drin?
* ...

### 2020
* https://twitter.com/GeorgeMonbiot/status/1221689611556024320
    * "George Monbiot - One of the commonest charges against a #vegan diet is that it can’t be natural or healthy,
    because it requires “supplements” of vitamin B12. Here’s why this argument is nonsense."
    * ...
    * more vegan on twitter: https://twitter.com/hashtag/Vegan

### 2019
* siehe https://www.youtube.com/watch?v=2Qk75XhDFJk (elsewhere) / für Einsteiger
    * Niko Rittenau: sehr gute B12-Erklärung inkl. Zufütterung (auch von Selen) und Jod bei Tieren

### 2017
* http://www.lfl.bayern.de/mam/cms07/ite/dateien/grunds__tze_der_schweinef__tterung.pdf, Juni 2009
    * die Schweine bekommen u. a. Vitamin B12 ins Futter zugesetzt
        * FRAGE: Wieviel im Vergleich zum menschlichen B12?, siehe unten
    * siehe auch http://graslutscher.de/unnaturliches-vitamin-b12-from-hell
* ["Vitaminmangel: Jeder Vierte im Alter mit Vitamin B12 unterversorgt"](https://www.aerzteblatt.de/treffer?mode=s&wo=17&typ=1&nid=87142&s=vegan), Ärzteblatt 2017
* Video: [Interviews nach Schlachtvideos](https://www.youtube.com/watch?v=-mOVvS7JfG0), 12 min, 2017, von "Vegan auf den Punkt"
    * "Was geht dir durch den Kopf, wenn du diese Szenen siehst?"
    * "Isst du selber Fleisch?"
    * "Empfinden Sie einen Widerspruch?" - "Nein, weil es anderen Möglichkeiten gibt, Tiere zu töten"
    * "Das kann alles nicht gesund sein"
    * "Glauben Sie, dass wir Tiere essen _müssen_?" - "Wer sagt mir, dass Pflanzen nichts empfinden?"
    * "Was genau hält Sie davon ab?"
    * "Sie werten also Geschmack über Leben?"
    * "Was können wir aus Fleisch bekommen, was wir sonst nicht bekommen können?"
    * "Woher bekommen die Nutztiere B12?"
        * "90 % der weltweit hergestellten B12-Supplemente werden den Tieren ins Futter gemischt", TODO: Quelle?
    * "Fleisch gehört dazu und Veganer sind militant und religiös. Sekte?"
    * "Inwiefern ist Veganismus in ihrer Einschätzung religös zu bewerten?" - "Glaubensfrage, Freiheiten andere Menschen tun wollen, nicht zugestehen"
    * "Was unterscheidet Mensch vom Tier..."
    * "Tiere sind dem Menschen untertan. Muss zwar respektvollen Umgang pflegen. Mensch steht evolutionsmäßig über dem Tier."
    * "Wir sind ja auch Tiere im Prinzip."
    * "Ziel: unnötiges Leid vermeiden", "150 000 000 000 Tiere / Jahr"
    * "Ich will mal selber ausprobieren, welche Effekt das auf einen hat. Primär gesund, sekundär Ethik"
    * Milch: "Ich glaub nicht, dass die Kuh sich da gequält fühlt"
    * "Was denkst du, was mit männlichen Kälber passiert?" -> 10-s-Video, "ja ok das wusst ich jetzt auch noch nicht"
* https://www.youtube.com/watch?v=s3wbVpEctIw, "vegan ist ungesund"
    * 4:30min: "USA: 1% Veganer, 39% B12-Mangel". Quelle?
* Video: ["Hey Veganer, euch fehlt Vitmain B12!"](http://der-artgenosse.de/hey-veganer-euch-fehlt-vitmanin-b12), mit Transkript
    * "Praktisch alle Tiere in der „Intensivtierhaltung“, die keine Wiederkäuer sind, bekommen B12 im Futter supplementiert. Zudem hat auch das von uns produzierte Fleisch auch sonst kaum noch etwas mit Natürlichkeit zu tun. Das kommt fast ausschließlich aus Tierfabriken und industrieller Massentierhaltung."
* Blogs
    * http://graslutscher.de/ahnungsloser-ernahrungsminister-ist-ahnungslos/
        * "Diese bösen synthetischen Vitamine sind übrigens kein explizit nur für Veganer produziertes Ergänzungsmittel: Sollte auf dem Schwangerschaftstest ein Kreuz zu sehen sein, dann bekommt die Glückliche vom Arzt als erstes einen kleinen Cocktail verschrieben, bestehend aus Vitamin D, Folsäure und Vitamin B12, eben weil die typische Kost der deutschen nicht zwingend genug davon enthält."
        * Ähnliches Beispiel: jodiertes Speisesalz in Europa
    * 2018, Comment einer Userin: "Ich bin übrigens einer der Veganer mit B12 Mangel. Kann aber Tabletten schlucken wie ich will, es wird nicht besser. Jetzt spritze ich eben.
Kenne das aber auch genauso von Nicht-Veganern."
* Herstellung: "Vitamin B12 als Nahrungsergänzungsmittel für den Menschen und bei der Tierernährung (Geflügel und Schweine) wird mittels Mikroorganismen hergestellt.", https://de.wikipedia.org/wiki/Cobalamine#Bedarf
    * In der Tiermast wird B12 also Geflügel und Schweinen zugesetzt
    * Schweine: ["Vitamine in der Fütterung"](https://www.landwirt.com/schweineberichte/Mastschweinbraucht,11,Vitamine-in-der-Fuetterung.html)
    * ["Vitamine in der Tierernährung"](http://www.hl-futter.de/files/vitamine_awt.pdf), 2001, 79 Seiten
        * umfangreiches Dokument
        * ...
* Artikel: [Vitamin B12](https://www.geo.de/natur/nachhaltigkeit/2291-rtkl-vitamin-b12-fleischlos-gluecklich), Geo-Artikel 2013
* Artikel: [Fleischkonsum und Klima - "Wir müssen weg von der Tierhaltung"](https://www.geo.de/natur/nachhaltigkeit/2291-rtkl-vitamin-b12-fleischlos-gluecklich), Geo-Artikel 2012
* Artikel: [Tierrechte - "Wir dürfen Tiere nicht töten, nur weil sie uns schmecken"](https://www.geo.de/natur/nachhaltigkeit/2146-rtkl-tierrechte-wir-duerfen-tiere-nicht-toeten-nur-weil-sie-uns-schmecken), Geo-Artikel 2014

Allgemein
---------
* Hülsenfrüchte (https://de.wikipedia.org/wiki/H%C3%BClsenfrucht)
    * Bohnen, Erbsen, Erdnüsse, Kichererbsen, Linsen, Platterbsen („Wicken“), Sojabohnen, Lupinen
        * enthalten Eisen

* Psychologie:
    * Früher konnten sich viele Menschen nicht vorstellen, dass der Mensch je auf dem Mond landen könnte.
    * Vegane Ernährung ist da sogar vergleichweise einfach.
