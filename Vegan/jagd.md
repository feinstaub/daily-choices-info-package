Jagd
====

<!-- toc -->

- [Inbox 2021](#inbox-2021)
  * [Niemand (auch Jäger) bezeichnet sich als Tierquäler vs. Aborigines-Schlachten in Australien](#niemand-auch-jager-bezeichnet-sich-als-tierqualer-vs-aborigines-schlachten-in-australien)
- [Inbox 2020](#inbox-2020)
  * [Warum dieser Jäger keine Tiere mehr tötet](#warum-dieser-jager-keine-tiere-mehr-totet)
  * [Mentalität in der Szene: Taubenjagd im Winter](#mentalitat-in-der-szene-taubenjagd-im-winter)
  * [Jagd auf Turteltauben](#jagd-auf-turteltauben)
  * [Wildfleisch, Wildbretsuche](#wildfleisch-wildbretsuche)
  * [Jagdbare Arten](#jagdbare-arten)
  * [Wildbretlotterie und Schonzeitbraten](#wildbretlotterie-und-schonzeitbraten)
  * [Mehr: Interessenskarte Wald](#mehr-interessenskarte-wald)
  * [Sonstiges](#sonstiges)
- [Siehe](#siehe)
  * [zu Land](#zu-land)
  * [zu Wasser](#zu-wasser)

<!-- tocstop -->

Inbox 2021
----------
### Niemand (auch Jäger) bezeichnet sich als Tierquäler vs. Aborigines-Schlachten in Australien
* suche https://de.wikipedia.org/wiki/Australien nach "Aborigines"
* https://de.wikipedia.org/wiki/Liste_der_Massaker_an_Aborigines
    * https://de.wikipedia.org/wiki/Jardwadjali#Massaker
        * "Die Siedler der Küste erzählten über die Siedler des Innenlandes, dass diese die Aborigines vernichteten als wären sie Kühe.
            In der Tat hätten sie die Auffassung, dass Aborigines keine oder kaum Menschen seien,
            weswegen sie jedem einschärften, dass das Töten der Aborigines grundsätzlich kein Mord sei." (1841)
        => Human Supremacy
        = Damals wie heute: **Gedankliches Abwerten (ob Mensch, ob Tier) erleichert Töten mit ruhigem Gewissen.**
            **Gleichzeitig kann man gedanklich der größte Menschen- und Tierfreund sein.**.
            Alles, was man tun, hat seine guten Gründe.
            Wenn Recht und Gesetz die Schwachen (Eingeborene dort, Tiere hier) nicht schützt, ist Willkür an der Tagesordnung.

    * https://de.wikipedia.org/wiki/Gippsland-Massaker
        * "Die Schwarzen sind nun sehr ruhig hier, arme Kerle.
            **Kein Wild des Waldes wurde mit derart schonungsloser Ausdauer gejagt.**
            **Männer, Frauen und Kinder wurden erschossen, wo auch immer sie angetroffen wurden.** [...]
            Ich habe in jedem Ort, in dem ich in Gippsland gewesen bin, auf das leidenschaftlichste protestiert, aber diese Angelegenheit wird geheim gehalten, denn die Strafe wären Hängen.[...] **Falls ein Schwarzer meine Schafe schlachtete, würde ich selbst auf ihn schießen, ohne jegliche Reue, wie auf einen wilden Hund,**
            aber kein Grund auf dieser Erde würde mich dazu bringen,
            **in ihre Camps zu reiten und sie wahllos zu erschießen, so wie es derzeit üblich ist,** wenn irgendwo Rauch zu sehen ist.
            Die Aborigines werden bald ausgerottet sein. Es ist unmöglich, zu sagen, wie viele erschossen wurden, aber ich vermute, dass nicht weniger als 450 bereits ermordet wurden."

Inbox 2020
----------
### Warum dieser Jäger keine Tiere mehr tötet
* Video

### Mentalität in der Szene: Taubenjagd im Winter
* https://jung-jaeger.eu/taubenjagd-im-winter/, 2019
    * "Ringeltauben gibt es in fast jedem Revier. Doch nur in wenigen werden die Geringelten gezielt bejagt.
        Schade! Denn beachtet der Jäger im Winter einige Dinge, kann es bei den Grau-Blauen in den kalten Monaten mächtig rappeln."
    * ...
    * "Eine erlegte Taube mit Einschusslöchern in 4 mm Stärke ist meist nicht mehr zu verwerten."

* https://de.wikipedia.org/wiki/Ringeltaube
    * "Die Art ist trotz der starken Bejagung in vielen Ländern ein häufiger Brutvogel und in Europa nicht gefährdet."

### Jagd auf Turteltauben
* "Die Turteltauben sind gefährdet", 2019, https://www.br.de/nachrichten/wissen/die-turteltauben-sind-gefaehrdet,RI2ftc5
* https://www.nabu.de/tiere-und-pflanzen/voegel/zugvogelschutz/malta/22992.html

### Wildfleisch, Wildbretsuche
* https://ljv-hessen.de/jagd-in-hessen/wildfleisch-aus-hessen/
* https://www.wild-auf-wild.de/wildbretsuche

### Jagdbare Arten
* https://nrw.nabu.de/natur-und-landschaft/landnutzung/jagd/jagdbare-arten/
* Fasan: https://nrw.nabu.de/natur-und-landschaft/landnutzung/jagd/jagdbare-arten/weitere-vogelarten/05322.html

### Wildbretlotterie und Schonzeitbraten
* ...
* vergleiche Wildbretsuche

### Mehr: Interessenskarte Wald
* siehe dort

### Sonstiges
* https://www.nabu.de/tiere-und-pflanzen/voegel/gefaehrdungen/23480.html - "Vogeltod durch Klebepaste"

Siehe
-----
### zu Land
* geschichte.md -> Vogeljagd in Amerika
* siehe "April 2019: SWR: Patricia Kopietz kennt sich mit den unschönen (und versteckten) Details der Jagd aus"
* pelz.md
* siehe "Jagdverband"
* siehe "Jagdfleisch"

### zu Wasser
* siehe "Thunfischjagd"
* Trophäenjagd
* tiere-artgerecht.md -> Treibjagd auf Wale
