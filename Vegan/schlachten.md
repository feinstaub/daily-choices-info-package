Schlachten
==========

<!-- toc -->

- [Gängige Praxis](#gangige-praxis)
  * [Fische](#fische)
  * [2020: Mobile Schlachteinheit für Rinder, Schlachtung mit Achtung](#2020-mobile-schlachteinheit-fur-rinder-schlachtung-mit-achtung)
  * [2020: Entwicklung eines Geflügelschlachtmobils für Hessen](#2020-entwicklung-eines-geflugelschlachtmobils-fur-hessen)
  * [2020: Weideschuss in der Schweiz](#2020-weideschuss-in-der-schweiz)
  * [2020: Schlachten von Schweinen](#2020-schlachten-von-schweinen)
  * [2020: Schlachten von Ziegen, Bio, Frankreich](#2020-schlachten-von-ziegen-bio-frankreich)
  * [2019](#2019)
  * [2018](#2018)
  * [2017](#2017)
  * [2014](#2014)
- [Schonendes Schlachten](#schonendes-schlachten)
  * [2017](#2017-1)
  * ["Bio-Tiere haben es ihr Leben lang besser; also ist es in Ordnung sie zu schlachten"](#bio-tiere-haben-es-ihr-leben-lang-besser-also-ist-es-in-ordnung-sie-zu-schlachten)
- [Neue Kultur](#neue-kultur)
  * [2020: Rinderhaltung ohne Schlachtung](#2020-rinderhaltung-ohne-schlachtung)
  * [2020: Fleisch von alten Schafen](#2020-fleisch-von-alten-schafen)
- [Alte Kultur](#alte-kultur)
  * [Metzger-Ästhetik, Fleisch-Ästhetik](#metzger-asthetik-fleisch-asthetik)
- [Verschiedenes](#verschiedenes)
  * [Broschüre "Gesichter der Angst. Ein Appell wider die Gleichgültigkeit"](#broschure-gesichter-der-angst-ein-appell-wider-die-gleichgultigkeit)
  * [Bolzenschussgerät](#bolzenschussgerat)

<!-- tocstop -->

Sammlung zum Thema Schlachten von Tieren.

Gängige Praxis
--------------
### Fische
siehe fisch-fischer-überfischung.md

### 2020: Mobile Schlachteinheit für Rinder, Schlachtung mit Achtung
* im Rahmen von Ökomodellregionen Hessen als Illustration
    * (wobei diese Form der Schlachtung auch im Ökobereich noch die Minderheit darstellt, todo: check)
    * TODO: rausfinden, wieviele Ökobetriebe in Hessen noch mit Tiertransporten arbeiten müssen
        und die Potential für die mobile Schlachteinheit haben (todo: dort nachfragen)
    * todo: AG-E: als "Tierschlachtungsinnovation bzw. best-of-Schlachtung ins Bewusstsein"
    * todo: Warum ging das ganz früher eigentlich ohne? Wegen fehlender rechtlicher Vorgaben?
* "Kritik an Preisträgern - Mobile Schlachteinheit wird ausgezeichnet", 2019
    * https://www.swr.de/swraktuell/baden-wuerttemberg/av-o1178892-100.html, 2019, 3 min
        * "Der Transport von Schlachttieren ist unnötiger Stress für die Tiere. Bei "Schlachtung mit Achtung" kommt der Schlachthof zum Tier.
        Die Erfinder der mobilen Schlachteinheit erhalten den Preis für landwirtschaftliche unternehmerische Innovation.
        Sie werden aber auch angefeindet."
        * (das geht gar nicht; Anfeindungen sind völlig fehl am Platz;
            für den Tierwohl-Fußabdruck von Menschen, die Rinder-Produkte (Fleisch, Milch, Käse etc.) konsumieren,
            ist dies ein toller Fortschritt im Vergleich zu herkömmlichen Schlachthöfen)
* https://www.schlachtung-mit-achtung.de/home/mobile-schlachteinheit-mse/
    * "Vorteile der MSE-200A zur Durchführung der „Hofnahen Schlachtung“"
        * ...
    * todo: Nur für Rinder? Auch für Schweine? Was ist mit Hühnern?
    * https://www.schlachtung-mit-achtung.de/
        * "Gemeinsam für den Tierschutz bei der Fleischgewinnung"
        * "Wir lieben, was wir tun - Unsere Stärke ist unsere Schwäche für Rinder (Tiere)"
        * "Wir wollen den Dialog zwischen Tierhaltern, Handel und Verbrauchern fördern"
        * "Tierschonende Schlachtmethoden umsetzen"
        * "Achtung und Würde des Tieres schützen"
        * "Tod in vertrauter Umgebung"
        * "Die Zeit ist reif für besseren Tierschutz und alternative Schlachtmethoden."
            * (oder sogar reif für eine Abkehr davon)
            * "Wir setzen uns für einen fairen Umgang mit den Tieren ein!"
            * "Tierschutz ist auch Menschenschutz"
    * https://www.schlachtung-mit-achtung.de/home/hintergrund/
        * "Billig erzeugtes Fleisch hat einen hohen Preis."
        * Zitat von Christian Morgenstern:
            * "„Wir brauchen nicht so fortzuleben, wie wir gestern gelebt haben.
                Machen wir uns von dieser Anschauung los,
                und tausend Möglichkeiten laden uns zu neuem Leben ein.“"
            * siehe auch "Christian Morgenstern"
                * Auch passend: Zitat: "Weh dem Menschen, wenn nur ein einziges Tier im Weltgericht sitzt.", Christian Morgenstern
            * (Weitere Zitate: https://www.tierheim-duesseldorf.de/wissenswertes/zitate.html)

    * https://www.schlachtung-mit-achtung.de/home/bildergalerie/
        * Bilder von der Realität inkl. Statements von PETA
* https://www.topagrar.com/suedplus/news/mobile-schlacht-einheit-bekommt-tierschutzpreis-11885888.html, 2019
    * Preis
* "Extrawurst: Innovative Schlachtverfahren"
    * https://www.praxis-agrar.de/tier/rinder/innovative-schlachtverfahren/
    * ...
    * mit Video_: "stressfrei schlachten"

### 2020: Entwicklung eines Geflügelschlachtmobils für Hessen
* Landestierschutzbeauftragte: https://tierschutz.hessen.de/nutztiere/gefl%C3%BCgel/entwicklung-eines-gefl%C3%BCgelschlachtmobils-f%C3%BCr-direktvermarkter-hessen
    * "Entwicklung eines Geflügelschlachtmobils für Direktvermarkter in Hessen"
    * "Viele Hühnerhalter mit **artgerecht** gehaltenen Tieren suchen daher vergeblich eine nahe gelegene Schlachtstätte, die auch eine geringe Anzahl von Hühnern oder Hähnchen schlachtet. Die Hühner solcher Halter leben in der Regel ganzjährig in ihrem Hühnermobil mit täglichem Weidegang."

* https://tierschutz.hessen.de/nutztiere/gefl%C3%BCgel/millionenfache-t%C3%B6tung-von-m%C3%A4nnlichen-eintagsk%C3%BCken
    * "Damit ist in Hessen jetzt Schluss! Am 04.09.2014 untersagte Hessen die Tötung männlicher Eintagsküken.
        In Hessen steht eine der größten Brütereien Deutschlands, mit **15 Millionen getöteten Küken pro Jahr**.
        An dieses Unternehmen wurde nun eine Verfügung geschickt"
    * ...

### 2020: Weideschuss in der Schweiz
* https://www.e-journal.ch/region/meilenstein-fuer-den-tierschutz/, 2020
    * "Seit dem 27. Mai ist die Hof- und Weidetötung in der Schweiz erlaubt"
    * "Eric Meili vor seinem Haus mit seinem ersten Milchrassenochsen Fanjo, kurz vor dessen Schlachtung"
        * "Berater am Forschungsinstitut für biologischen Landbau (FiBL) in Frick. Der Agronom mit Schwerpunkt Nutztierwissenschaften
            ist seit den Anfängen für das Projekt «Hof- und Weidetötung» verantwortlich.
            Meili ist selbst stolzer Besitzer von Weiderindern, deren Fleisch er unter www.meilibeef.ch direkt vermarktet."
            * http://www.meilibeef.ch/
                * "Das Fleisch von Dreak ist verkauft.
                    Alle weiteren Bestellungen gehen auf das nächste Tier."
                * Fokus auf das Tier als Produkt. Lebewesen ist sekundär.
        * "Sie haben ja auch selbst Rinder. Ist das nicht ein schlimmes Gefühl,
            sein Tier auf dem Hof töten zu müssen? Da könnte einem das Delegieren
            an einen grossen Schlachthof doch auch entgegenkommen."
            * "Meine Tiere sind mir ans Herz gewachsen. Ich ziehe sie auf, ich nenne sie beim Namen,
                ich kenne ihren Charakter, ihr Wesen. Jedesmal, wenn ich sie für den Gang zum **Schlachthof** verlade,
                habe ich ein mulmiges Gefühl. **Das tut weh. Es fühlt sich einfach unnatürlich an.**
                Die Vorstellung, das Tier **in seinem vertrauten Umfeld töten zu können**,
                es von einem regionalen Metzger verarbeiten zu lassen und dann direkt ab Hof zu verkaufen,
                **ist einfach viel stimmiger.**"
                * (aber wohl auch nicht ganz stimmig?; Hände weg von unnatürlichem Fleisch?;
                    Wie können wir Landwirten dennoch ein gutes Auskommen sichern?)
                * (Gibt es eine Grenze, wo man sagen würde, das geht nicht? Wie sieht die Grenze aus?)
    * Echo-Artikel, 4. Juli 2020
        * "Bleibt ein weiteres Problem: Nicht jeder Bauer hat die Kraft, seine Tiere zu töten." (Quelle?)
            * Eric Meili hat dafür die "Fressgitter"-Alternative.
            * (Wie können wir den Landwirten helfen, wegzukommen von dieser Spirale des Todes?
                z. B. um in Harmonie mit sich selbst und der Natur zu leben)

### 2020: Schlachten von Schweinen
* https://www.hr-fernsehen.de/sendungen-a-z/hessenreporter/sendungen/schweine-schlachten,sendung-80274.html, 30 min
    * "Trotzdem: Den Prozess, der sich zwischen Schwein und Schnitzel abspielt, blenden viele doch am liebsten aus."
    * "fühlt sich ruhig an"
    * viele explizite Bilder

### 2020: Schlachten von Ziegen, Bio, Frankreich
* https://www.kinderworld.org/videos/meat-industry/baby-goats-slaughter-france/
    * https://youtu.be/FtiGNN97qWk, 3 min
    * Zange
    * Blut

### 2019
* https://albert-schweitzer-stiftung.de/aktuell/schlachthoefe-verstoesse-an-der-tagesordnung

### 2018
* Artikel: ["Schockierende Bilder aus einem Schlachthof erschüttern Burger-Fans"](https://www.stern.de/tv/warum-ein-mcdonald-s-schlachthof-diese-bilder-erklaeren-muss-7860438.html), sternTV, 2018
    * zwei eingebette Videos
        * Video: ["McDonald's-Fleischlieferant: Schockierende Bilder aus Schlachthof - die ganze Reportage | stern TV"](https://www.youtube.com/watch?v=qQarDrrgPIw), 2018, 10 min, tag:offline
        * Interview mit Friedrich Mülln, 7 min
    * so sollte es eigentlich sein: (ass-fb-2018-Lebenskühe Gilda und Felicitas – So sieht Mutterliebe aus.mp4), Albert-Schweitzer-Stiftung, 2018, 1 min, tag:class?
    * Video: ["Affenversuche am MPI Tübingen eingestellt - Reportage 6 von 6 | stern TV"](https://www.youtube.com/watch?v=gKyi81qio_c), 2017, 7 min, tag:offline, Mülln
    * Video: ["Einstellung der Tierversuche am Max-Planck-Institut? - Reportage 3 von 6 | stern TV (13.05.2015)"](https://www.youtube.com/watch?v=dKTGdc-c6s8), 12 min, tag:offline
        * Soko Tierschutz, Mülln
    * ergänzend Video_: ["Talk zu Tierversuchen am Max-Planck-Institut | Talk 1 von 2 | stern TV (10.09.2014)"](https://www.youtube.com/watch?v=xE1OEw94HNc), 14 min, tag:offline, Mülln
    * Artikel: ["Freibrief für Stalleinbrüche? Für Rukwied ein Skandal"](https://www.topagrar.com/news/Home-top-News-Einbruch-als-Nothilfe-im-Koalitionsvertrag-verankern-9057304.html), 23.02.2018, topagrar
        * "Das Oberlandesgericht Naumburg (Sachsen-Anhalt) hat in einer Revisionsverhandlung über einen Fall von Hausfriedensbruch im Zusammenhang mit einer Undercover-Recherche des Vereins Animal Rights Watch (ARIWA) die drei angeklagten Tierrechtler erneut frei gesprochen."
        * siehe auch "Menschen - Tierfilmer freigesprochen 1, Änderung nur durch Bilder, Gericht lobt, ZDF, 2018, 2min (NNvE-Fyv4ng), Schmitz.mp4"

### 2017
* https://albert-schweitzer-stiftung.de/aktuell/schlachtzahlen-2017
    * Zahlen: 745 Mio. geschlachtete Tiere in D. im Jahr 2017; 8,1 Mio. Tonnen; OHNE Fische, Kaninchen und Wirbellose (Statistisches Bundesamt)
        * https://www.destatis.de/DE/PresseService/Presse/Pressemitteilungen/2018/02/PD18_038_413.html
        * siehe auch [Graphik 2014](https://www.welt.de/politik/deutschland/article123700329/Deutsche-schlachten-pro-Jahr-750-Millionen-Tiere.html)
        * pro Sekunde? -> 745000000 / 365 / 24 / 3600 = 24

### 2014
* https://www.derwesten.de/panorama/60-milliarden-tiere-werden-getoetet-um-gegessen-zu-werden-id8853722.html
    * Zahlen: 2011: "Über 60 Milliarden Tiere würden weltweit jährlich geschlachtet und verzehrt. Davon 58 Milliarden Hühner."
        * pro Sekunde? -> 60000000000 / 365 / 24 / 3600 = 1900
        * Zahlen: Schlachten weltweit: plus geschätzt 90 Millarden Fische = 150 Millarden Tiere.
            * siehe fisch-fischer-....md

* Andere Länder - z. B. Mexiko
    * https://www.veganblog.de/ernahrung/mexiko-schlachthaus-10-bilder/
        * Video: ["Slaughterhouse. What the meat industry hides. // Documentary film."](https://www.youtube.com/watch?v=0VbTT5GUqBk), 2018, 40 min
            * "images obtained during an investigation carried out in fifty eight slaughterhouses in Mexico between 2015 and 2017"

Schonendes Schlachten
---------------------
* Humanes Schlachten, Tiergerechtes Schlachten, Tierfreundliches Schlachten, Schonendes Schlachten, Artgemäßes Schlachten
    * Gibt es das? Wie würde das aussehen?

### 2017
* Fernsehbeitrag: [Reporterin versucht ein Lamm mit state-of-the-art Bolzenschuss zu töten"](https://www.youtube.com/watch?v=muSfC8VwnGk), PULS ist das junge Programm des Bayerischen Rundfunks, 2017
    * kriegt es aber nicht hin
    * vor laufender Kamera
    * ca. 15.000 YT comments
        * "Das ist die harmlosests und beste Methode ein Tier zu töten!"
        * "Oh ja, DAS ist der Grund warum es nicht jugendfrei ist, nicht dass im Video ein Lebewesen vor laufender Kamera getötet wird.﻿"
        * "Ach komm! Das Tier ist wundervoll gestorben :) hatte vorher ein schönes Leben und musste keinen Transport überstehen!﻿"
            * "ja gell, da hat man gleich selber Lust zu sterben.﻿"
        * "Was haben meine Vorfahren mit dem Thema zu tun. Deine Vorfahren haben womöglich Hexen verbrannt. Willst du das noch immer zum? Es gibt knapp 100000 essbare Pflanzen - wozu auf friedliche Tiere losgehen? Denkst du, dass Veganer nur Bananen und Tofu essen?"
* Video: ["Schlachten auf der Weide: Tierwohl bis zum Schuss | Unser Land | BR Fernsehen"](https://www.youtube.com/watch?v=IUInbe0-pIU), 2016, 7 min, tag:class?
    * ...
* Video_: ["Schwein gehabt: Hausschlachtung in Kirchlauter | Zwischen Spessart und Karwendel | BR"](https://www.youtube.com/watch?v=KE8dLocsKwI), 10 min, 22. April 2016, tag:offline
    * YT altersbeschränkt, [Original-Link ARD](http://www.ardmediathek.de/tv/Zwischen-Spessart-und-Karwendel/Hausschlachtung-in-Kirchlauter/BR-Fernsehen/Video?bcastId=14913712&documentId=34583896), 2016
    * siehe [tiere-artgerecht.md](tiere-artgerecht.md)
    * (Ein Schwein war sein ganzes Leben lang glücklich und zufrieden. Und dann wird dieses glückliche Leben abrupt beendet?)
* Video: ["Tierschutz: Mangelhafte Kontrolle | SWR odysso"](https://www.youtube.com/watch?v=69_qp5rDy1s), 2014, 7 min, tag:class?
    * Ein Fehler im industriellen Prozess getrifft einzelne Tiere. Diese Individuen spielen in diesem Prozess keine Rolle.
    * Großschlachterei über 1000 Schweine / Stunde
    * vorbildlich: bei jedem einzelnen Schwein auf das Auge drücken, ob betäubt
        * => Gutes Beispiel dafür wie es in den _vorbildlichsten_ Betrieben zugeht. Und nicht alle sind vorbildlich.
    * Es werden Hohlmesser verwendet, so dass man gar kein Blut sieht
    * ...
    * industriell, krass
* Video_: ["Schlachtefest im Brandenburgischen Freilichtmuseum Altranft"](https://www.youtube.com/watch?v=K1PmqHiQa_Y), 10 min, 2015
    * Schwein schon tot
    * Kompletter Zerlegeprozess und Wurstherstellung, untermalt mit freundlicher Musik
* Video: [Tierschutz: Besser schlachten? | SWR odysso](https://www.youtube.com/watch?v=s80j_AkI0sM), 8 min, 2014
    * Man kann jedenfalls nicht sagen, sie hätten sich nicht bemüht.
    * Fragestellung, da das Wort "Idealvorstellung" verwendet wurde: Wie sieht ein ideales Schlachthaus aus?
    * [Orig-Artikel-Link](https://www.swr.de/odysso/besser-schlachten-neue-methode-kann-schreckliche-tierquaelerei-deutlich-vermindern/-/id=1046894/did=13451574/nid=1046894/9x755v/)
        * "Die bei Schweinen übliche Betäubung mit Kohlendioxidgas ist oft mit entsetzlichen Qualen für die Tiere verbunden."
        * "Über eine Million Schweine werden in Deutschland Woche für Woche geschlachtet."
        * ""Es gibt für eine tierschutzgerechte Schlachtung natürlich eine ideale Vorstellung, die möglicherweise auch so mancher Verbraucher so hat, oder noch hat", sagt Prof. Troeger. "Nämlich dass die Tiere betäubt werden, wie ein Mensch, meinetwegen im Krankenhaus. Sozusagen ohne dass es von dieser Betäubung überhaupt etwas mitbekommt und dass es dann eben schmerzlos entblutet wird und stirbt, das wäre die Idealvorstellung. Die Realität an den Schlachthöfen ist die, dass wir zwar zwei gesetzlich vorgeschriebene Betäubungsmethoden haben für Schlachtschweine. Das wäre die Betäubung mit elektrischem Strom und die Betäubung eben mit CO2-Gas, dass aber beide Methoden für die Tiere keineswegs schmerz- und leidlos sind, sondern dass beide Methoden die Tiere in zum Teil erheblichem Umfang leiden lassen.""
        * "Über 58 Millionen Schweine werden in Deutschland jährlich geschlachtet (Stand 2013)"
        * "Schweine in der CO2-Senkgrube ersticken qualvoll"
        * "Dieses Leiden hat Klaus Troeger keine Ruhe gelassen. Über Jahre forscht er an Alternativen. Und findet schließlich eine: Helium statt CO2 als Betäubungsgas."
            * (wenn die Tiere nichts mehr spüren, wenn sie getötet werden, ist es dann alles ok?)
        * "Bei einer Anwendung im industriellen Maßstab darf so gut wie kein Helium verloren gehen, denn das Gas ist teuer."
* https://www.schlachtung-mit-achtung.de
    * "Wer wir sind -  Engagierte Menschen denen die sogenannten Nutztiere nicht egal sind."
    * "Unsere Stärke ist unsere Schwäche für Rinder (Tiere)"
    * "Achtung und Würde des Tieres schützen"
    * "Tod in vertrauter Umgebung"
    * "Billig erzeugtes Fleisch hat einen hohen Preis."
    * "„Wir brauchen nicht so fortzuleben, wie wir gestern gelebt haben. Machen wir uns von dieser Anschauung los, und tausend Möglichkeiten laden uns zu neuem Leben ein.“"
    * https://www.schlachtung-mit-achtung.de/home/themen/ethik-und-moral/
        * "Das Bundesamt für Landwirtschaft und Veterinärwesen (BLV) schreibt dazu: Das aufwendige Verfahren hat ein Ziel: "Tiere so gut wie möglich vor ungerechtfertigten Belastungen zu schützen". Nichts dergleichen ist in der Landwirtschaft der Fall, obwohl wir es auch dort mit Tieren zu tun haben. Auf jedes Tier, das in der Forschung stirbt kommen hunderte Tiere, die in der Landwirtschaft sterben."
        * "Diese Tiere haben dieselben Interessen und sind nicht weniger schutzbedürftig und -berechtigt – ein Befund, der nicht nur ethisch schlüssig ist, sondern auch in den Rechtswissenschaften aufgegriffen wird."
        * "Die Ethik des Tierschutzes ist somit auch eine artübergreifene Humanitätsethik. Damit soll die Forderung, Schäden, Schmerzen und Leiden zu vermeiden bzw. wo unvermeidbar, zu lindern, erfüllt werden. Um Wohlbefinden der Tiere zu realisieren, müssten Gemeinsamkeiten und Unterschiede abgewogen werden. Dabei darf eine Vermenschlichung von Tieren nicht erfolgen, aber auch keine Abstufung (TEUTSCH, 1987)."
    * https://www.schlachtung-mit-achtung.de/home/bildergalerie/
        * Bilder von Vision und Realität (auf der Vision ist die eigentliche Schlachtung gar nicht zu sehen, sondern nur das Leben vor der Schlachtung)
* Artikel: ["Schlachten: Gib ihr die Kugel"](https://www.deutschlandfunknova.de/beitrag/schlachten-t%C3%B6tung-auf-der-weide), 2014
    * "Wie kann man Rinder töten, ohne ihnen unnötiges Leid zuzufügen?"
    * "Die Tiere sind dann bewusstlos - im besten Fall sogar tot - und fallen dann auf der Weide einfach um."
    * "Die mutigen oder die ranghöheren Kühe, die gehen dahin und stupsen das Tier mit der Nase an, sie schnuppern und untersuchen das Tier. Aber sie haben ja keine negative Verbindung zum Tod wie wir es haben."
    * ["Tiere töten: Herzliches Beileid"](https://www.deutschlandfunknova.de/beitrag/schlachten-menschen-t%C3%B6ten-tiere), 2014
        * "Tierfreundliche Schlachtung?"
        * "Früher war ich strenge Vegetarierin. Ich dachte, es wäre unmöglich, ein Tier zu essen, um das ich mich selbst gekümmert habe. Dabei ist es das Gegenteil: Ich will kein anonymes Tier aus dem Supermarkt essen, aber bei denen, die ich kenne, da ist es ok."
            * (was machen alle anderen?)
* Artikel: ["Wie man Schlachtvieh tierfreundlich töten kann"](https://www.welt.de/wissenschaft/tierwelt/article5747766/Wie-man-Schlachtvieh-tierfreundlich-toeten-kann.html), 2010
    * "Im Schlachthöf Thönes hat das Schwein geschlafen, bevor es geschlachtet wird und schlussendlich auf dem Tisch der Metzger landet"
    * "Tierfreundlicher Metzger, das klingt wie Schnapshändler gegen Alkohol."
    * "Es wird gleich sehr blutig."
    * "Es ist laut und kalt und riecht nach Fleisch und Tier."
    * "Richtig betäuben bedeutet: Das Tier kriegt vom Halsstich und dem Entbluten nichts mit. Schlecht betäubt heißt Tierquälerei."
    * "Das ist der erste Schlachthof, den ich erlebe, wo man normal sprechen kann. Sonst muss man brüllen, weil die Schweine so aufgeregt quieken."
    * "Stöcke und Elektrotreiber sind bei Thönes verboten. Wenn ein Tier nicht weitergehen will, schieben es die Arbeiter mit einem Brett von hinten an."
        * (warum wohl will es nicht weitergehen?)
    * "Die Führung ist beendet, und es gibt Wurstbrötchen. Bis auf die muslimischen Schüler langen alle zu."
* Artikel: ["Wo Tiere gut leben und angstfrei sterben dürfen"](https://www.news.at/a/bio-tiergerecht-schlachten-betriebe-oesterreich), 2016
    * "Auf diesen Höfen werden Schweine, Rinder und Co. tierschutzgerecht geschlachtet"
    * "Wer will schon ein Schnitzerl essen, das von einem Tier stammt, das Todesängste ausgestanden oder sich zumindest in einer außergewöhnlichen Stresssituation befunden hat, bevor es gestorben ist. Vermutlich niemand. Und dennoch sind viele der Tiere, die schließlich auf unserem Teller landen, solchen lebensverachtenden Bedingungen ausgesetzt."
    * "Natürlich kann man das Schweinderl nicht zu Tode streicheln."
    * "also ohne Einsatz von CO2, wie es in großen Schlachthäusern üblich ist, ohne Tritte, Elektroschocks und ähnliche Grausamkeiten"
    * "Indem sie tagtäglich "hineingefüttert" werden, werden die Tiere mit dem Schlachthaus vertraut gemacht."
    * "Am Tag der Schlachtung werden die Schweine dann in den letzten Bereich gelockt, jenen, in dem die Schlachtung stattfindet. Einzeln, eines nach dem anderen, damit die anderen Tiere nicht sehen, was hier vor sich geht."
        * (richtig human ist das; gut, dass uns die Schweine so sehr vertrauen)
    * "Dann wird es mit der Elektrozange betäubt. Der Rest geht ganz schnell." Sobald die Betäubung wirkt – und das ist nach einigen wenigen Sekunden der Fall – wird die Halsschlagader aufgeschnitten."
    * "Nicht nur, weil ich nicht will, dass sie sich fürchten, sondern auch wegen der Fleischqualität"
    * "Das ist so, wie wenn man einen Raum ohne Fenster betritt und das Licht ausmacht. So denk ich mir das halt."
    * "Dabei sind dem Tier nicht nur die Umgebung, sondern auch die Schlächter bekannt."
    * "Leider gibt es kein Gütesiegel für artgerecht geschlachtete Tiere."
    * "Und auch, wenn auf dem Fleisch "Bio" steht, heißt das noch lange nicht, dass das Tier angst- und stressfrei gestorben ist."
        * (schwierige Abwägung: bio im Supermarkt ist in der Regel besser als konventionell, aber bei der Schlachtung oft kein Unterschied)
* ["Möchte tierfreundlich schlachten"](http://www.vol.at/moechte-tierfreundlich-schlachten/5132960), 2017
    * "Ich möchte mit einem mobilen Schlachtbetrieb arbeiten und so möglichst tierfreundlich schlachten.
        Das wirkt sich auch sehr positiv auf die Fleischqualität aus, denn Stresshormone, die vor der Tötung ausgeschüttet werden,
        gehen direkt ins Fleisch. Durch einen mobilen Schlachtbetrieb sind die Tiere davor in einer gewohnten Umgebung und haben deutlich weniger Stress"
* ["Tierschutzgerechtes Töten kleiner Labortiere"](https://www.medizin.uni-tuebingen.de/tierschutz/Toten-von-Versuchstieren.pdf)
* ["Manchmal notwendig ... Fische töten"](www.rhusmann.de/aqua/toeten.htm)
    * ...
    * Tierschutzgesetz: "Ein Wirbeltier darf nur unter Betäubung oder sonst, soweit nach den gegebenen Umständen zumutbar, nur unter Vermeidung von Schmerzen getötet werden."
* ["Abschuss auf der Weide: Eine tierfreundliche Alternative zum Schlachthof?"](https://www.vegan.eu/abschuss_weide/), 2014
    * ...

### "Bio-Tiere haben es ihr Leben lang besser; also ist es in Ordnung sie zu schlachten"
* ["Tabu? – Schlachten gehört dazu"](http://schrotundkorn.de/ernaehrung/lesen/tabu-schlachten-bio.html), schrot-und-korn, 2017
* ["Haltung von Mastschweinen verfassungswidrig?"](http://schrotundkorn.de/news/lesen/haltung-von-mastschweinen-verfassungswidrig.html), schrot-und-korn, 2017
    * Wenn etwas gesetzlich erlaubt ist, ist es dann auch moralisch richtig?

Neue Kultur
-----------
siehe auch biovegan.md

### 2020: Rinderhaltung ohne Schlachtung
* Rinderhaltung ohne Schlachtung (ROS), Dissertation, Berlin, 2019
    * https://edoc.hu-berlin.de/bitstream/handle/18452/22120/dissertation_meyer-glitza_patrick.pdf?sequence=5
* https://orgprints.org/27232/
    * "**Pioniere einer Rinderhaltung mit natürlichem Sterben**
        Meyer-Glitza, Patrick (2015) Pioniere einer Rinderhaltung mit natürlichem Sterben.
        Paper at: 13. Wissenschaftstagung Ökologischer Landbau, Hochschule für nachhaltige Entwicklung Eberswalde, 17. - 20. März 2015."
        * https://orgprints.org/27232/1/27232_meyer_glitza.pdf
            * "Die Rinderhaltung ohne Schlachtung bietet für sich ökologisch ernährende Vegetarier eine ethische Möglichkeit
                das Leben der Tiere zu erhalten und dabei in begrenztem Umfang Milchprodukte zu verzehren."

### 2020: Fleisch von alten Schafen
* YT: "Wie schmeckt das Fleisch von alten Schafen? Sauerbraten und Gulasch | Unser Land | BR", 2017, 6 min
    * Bilder von alten Schafdamen am Haken und Zerlegung
    * (viele wollen Lammfleisch)
* altes Schaffleisch
    * Es gibt Kulturkreise (z. B. in Afrika), dort wird das alte zähe Schaffleisch gerne gegessen -> eine Kultur für mehr Tierwohl
        * Es handelt sich dabei um Mutterschaffleisch, wenn die Tiere kein Nachwuchs (Lämmer) mehr bekommen.

Alte Kultur
-----------
### Metzger-Ästhetik, Fleisch-Ästhetik
juxtapose = nebeneinanderstellen mit Bildern der noch lebenden Tiere => Gefühle?

* Neuland
    * https://www.neuland-fleisch.de/
        * "Imagefilm von Artgemäß, unserer Vermarktungsgesellschaft im Norden"
        * "und übernehmen mit unserem NEULAND-Qualitätsfleischprogramm ethische Verantwortung für Tier und Umwelt."
            * welche Ethik?
* Heiderrinder
    * https://www.heiderinder.de/unser-fleisch/ (Bilder)
    * https://www.heiderinder.de/steak-wine-sommeraktion/ (Bilder und Text)
* Worms
    * https://www.metzgerei-david.de/hall-of-beef.php (Bilder)
    * YT: "Metzgerei David Worms - BBQ Hähnchen Bauerngockel", 2014, 9 min
        * Hähnchen vom Ochsenschläger
        * "Vom Biss her etwas fester. Es sind freilaufende Hähnchen. Die werden bis zu 90 Tage alt."
        * schön mit Olivenöl benetzen, damit die Marinade besser hält; einmassieren
        * Gewürzmischung rieche fantastisch; noch besser auf dem Grill
        * ...
    * YT: "Metzgerei David Worms - BBQ High End Kotelett"
        * Fleischteile

Verschiedenes
-------------
### Broschüre "Gesichter der Angst. Ein Appell wider die Gleichgültigkeit"
    * Download alte Version: http://www.tiere-leben.de/imags/Gesichter_der_Angst.pdf
        * A6, 16 Seiten
        * die Autorin ist "Tierärztin und arbeitet unter anderem seit 2007 als amtliche Tierärztin der Fleischbeschau in einem Schlachtbetrieb."
    * [Bestellen](https://albert-schweitzer-stiftung.de/shop/broschuere-gesichter-der-angst)

### Bolzenschussgerät
* Doktorarbeit: ["Untersuchungen zum korrekten Treffpunkt für den Bolzenschuss bei der Betäubung von Rindern bei der Schlachtung"](https://edoc.ub.uni-muenchen.de/13520/1/Kohlen_Simone.pdf), 2011
    * u. a. "Einflussfaktoren bei einer mechanischen Betäubung"
        * "Hautdicke und Haare bei Rindern"
        * "Stressfaktoren vor der Betäubung"
            * "Die höchste Priorität vor der Tötung von Schlachttieren muss in der Verringerung von Angst und Stress liegen (Schatzmann, 1997). Nicht nur die objektive Intensität eines Stressors ist für den Grad der Stressbelastung entscheidend, sondern vor allem auch die subjektive Bewertung durch  das  Individuum  (Loeffler, 2002)."
            * "Zu größerem Stress als der Tötungsvorgang selbst, können bei Rindern ein Zusammenbringen mit unbekannten Tieren und längere Transportzeiten führen"
        * "Fehlermöglichkeiten und Probleme bei der Betäubung mit Bolzenschuss"
            * "Die Betäubung war erfolgreich, wenn das Tier sofort niederstürzt, keine Aufstehversuche unternimmt, die Augen starr und reflexlos bleiben und die Atmung  ausfällt"
        * "Betäubungspersonal"
            * "Das Betäubungspersonal muss regelmäßig und fachgerecht geschult werden, da durch gewohnheitsmäßiges Töten von Tieren psychische Störungen beim  Menschen auftreten  können."
            * ...
            * "Wenn der Betäuber ermüdet, kann das zu Fehlschüssen führen, daher sollte abwechselnd an verschiedenen Arbeitspositionen gearbeitet werden."
    * ...
* mechanische Betäubung: ["Tierschutzgerechte Bolzenschussbetäubung"](https://www.bfr.bund.de/cm/343/tierschutzgerechte_bolzenschussbetaeubung.pdf), 2001, BgVV
* https://de.wikipedia.org/wiki/Schlachtschussapparat
    * "Der Schlachtschussapparat ist ein Gerät, das bei der Schlachtung zum Betäuben von Schlachttieren verwendet wird. Damit soll den Tieren unnötiges Leid erspart werden."
    * Bild: "Aufsetzpunkte des Bolzenschussapparates bei Schlachttieren"
    * "Bei Tieren mit dicker Kopfhaut und starker Schädeldecke wie Rindern oder Pferden wird dieses mittels eines gezielten Schusses ins Gehirn betäubt. Bei Rindern zielt der Fleischer dabei auf den gedachten Kreuzungspunkt zweier Linien, die den Hornansatzpunkt und das gegenüberliegende Auge verbinden. Bei Pferden wird auf der Höhe des Mähnenansatzes leicht seitlich der Mitte geschossen. Nur bei Hausschlachtungen werden auch Schweine mit dem Bolzenschussgerät betäubt."
