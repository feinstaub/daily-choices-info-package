Überfischung
============

<!-- toc -->

- [2020](#2020)
  * [Beifang](#beifang)
  * [Mikroplastik durch Fischernetze](#mikroplastik-durch-fischernetze)
  * [Ist Fisch als Nahrungsmittel notwendig?](#ist-fisch-als-nahrungsmittel-notwendig)
- [2019](#2019)
  * [Delfine als Beifang](#delfine-als-beifang)
- [Fische haben Gefühle](#fische-haben-gefuhle)
  * [Fühlen Fische Schmerzen?, 2020](#fuhlen-fische-schmerzen-2020)
  * [Tintenfische](#tintenfische)
  * [2017](#2017)
  * [Tierphilosoph Markus Wild](#tierphilosoph-markus-wild)
  * [What a fish knows](#what-a-fish-knows)
- [Meere](#meere)
  * [Überfischung](#uberfischung)
  * [Zahlen: fishcount.org](#zahlen-fishcountorg)
  * [Zahlen 2](#zahlen-2)
  * [Fischmehl (im Tierfutter)](#fischmehl-im-tierfutter)
  * [Die Bucht - The Cove (2009)](#die-bucht---the-cove-2009)
  * [Fang-Methoden](#fang-methoden)
  * [Schleppnetze](#schleppnetze)
  * [Stellnetze in der Ostsee](#stellnetze-in-der-ostsee)
  * [Elektro-Fischen](#elektro-fischen)
  * [Shrimps = Garnelen = Gambas](#shrimps--garnelen--gambas)
  * [Haie](#haie)
  * [Wale - Sea Sheperd](#wale---sea-sheperd)
- [Süßwasser](#susswasser)
  * [Karpfen: wie alt und groß?](#karpfen-wie-alt-und-gross)
- [Menschen](#menschen)
  * [Robert Marc Lehmann](#robert-marc-lehmann)
- [Fischerei-Siegel](#fischerei-siegel)
  * [MSC-Siegel - Meeresfisch](#msc-siegel---meeresfisch)
  * [ASC-Siegel - Aquakultur](#asc-siegel---aquakultur)
- [Angeln](#angeln)
- [Siehe auch](#siehe-auch)

<!-- tocstop -->

2020
----
### Beifang
* https://de.wikipedia.org/wiki/Nachhaltige_Fischerei#Friend_of_the_Sea
    * Selbst dieses vorbildliche Siegel lässt bis zu 8 % Beifang zu.
* siehe Stellnetze

### Mikroplastik durch Fischernetze
* https://www.br.de/wissen/geisternetze-fischernetze-plastik-meer-gefahr-fuer-meerestiere-100.html (2018)
* siehe auch Albert-Schweitzer-Stiftung

### Ist Fisch als Nahrungsmittel notwendig?
* https://www.tierrechte.de/2018/08/20/ist-fisch-als-nahrungsmittel-notwendig/, nein
* Giftige Fische: "Verbrauchertipp für Schwangere und Stillende, den Verzehr von Thunfisch ein-zuschränken, hat weiterhin Gültigkeit"
    * https://www.bfr.bund.de/cm/343/verbrauchertipp_fuer_schwangere_und_stillende_den_verzehr_von_thunfisch_einzuschraenken.pdf

2019
----
### Delfine als Beifang
* ["Frankreich: Viele tote Delfine durch Fischernetze"](https://www.animalequality.de/neuigkeiten/frankreich-viele-tote-delfine-durch-fischernetze)
    * ...
    * Quelle ORF
* Delfine als Beifang schon min. seit 2010 ein Problem (Jaenicke: "die größte Gefahr für Delfine ist unsere Nachfrage nach Fisch")
* siehe auch "delfin"

Fische haben Gefühle
--------------------
### Fühlen Fische Schmerzen?, 2020
* https://www.swr.de/wissen/1000-antworten/umwelt-und-natur/fuehlen-fische-schmerzen-100.html
    * Die Fische haben Schmerzrezeptoren, sie zeigen ein typisches komplexes Schmerzverhalten – also liegt die Vermutung nahe, dass sie auch Schmerzen empfinden. Und die Wissenschaftler erweisen sich am Ende dann doch nicht als Sadisten, denn ihre Schlussfolgerung lautet: Wenn Fische Schmerzen empfinden, dann muss das Konsequenzen haben."
    * "Angeln mit Angelhaken ist höchstwahrscheinlich sehr schmerzvoll, sagen sie. Das gleiche gilt, wenn Fische in Treibnetzen in einem langwierigen Prozess verenden. Die Forderung aus diesen Forschungen ist daher ein Verbot solcher Methoden, da der Fischfang in der heutigen Form vielen schmerzempfindenden Wesen tatsächlich viele Schmerzen zufügt."

### Tintenfische
* https://albert-schweitzer-stiftung.de/aktuell/tintenfische-schuetzen-nicht-essen, 2019

### 2017
* ["Angeln verbieten? - Auch Fische haben Schmerzen"](https://www.swr.de/swr2/wissen/fische-empfinden-schmerzen/-/id=661224/did=19481312/nid=661224/1sd49il/), 2017, OFFLINE
    * "Sie reicherten das Wasser eines von zwei Aquarien mit einem Schmerzmittel an. Dann fügten die Forscherinnen den Tieren Schmerz zu – und die Fische lernten, in jenes Bassin zu schwimmen mit dem Schmerz stillenden Medikament. Sie taten es, obwohl das Becken ohne Schmerzmittel für sie viel angenehmer gewesen wäre."
    * "In Deutschland soll es über vier Millionen Angler geben."
    * "Markus Wild sagt: Ja, man sollte das Angeln verbieten. Denn Angeln sei höchst problematisch und sollte seiner Meinung nach eigentlich auch unter die Bestimmung des Tierschutzgesetzes fallen, die schon existiert."
    * "Wie der Mensch mit Fischen umgeht, widerspiegelt sein Wissen – oder vielmehr Nichtwissen über dieses Tier. Ein Fisch ist eben ein Fisch. Und einen Fisch isst man. Markus Wild glaubt aber, dass die Wissenschaft dazu beigetragen hat, dass sich das Image dieses Wasserbewohners verändert hat. Früher dachte man, dass Fische dumme Tiere sind. Doch Fische haben ein sehr gutes Gedächtnis. Fische lernen sehr viel und sind auch sehr soziale Tiere. Das heißt, man hat eigentlich in den letzten zwanzig Jahren immer mehr ein völlig neues Bild vom Fisch bekommen, als ein intelligentes, lernfähiges, ja sogar emotionales Wesen."

### Tierphilosoph Markus Wild
* https://www.migrosmagazin.ch/unser-seltsames-verhaeltnis-zur-tierwelt, 2018
    * "Die meisten Philoso­phinnen und Philosophen, die sich ernsthaft Gedanken machen um unseren Umgang mit Tieren, stellen ihre Ernährung um."
    * "In die Wiege gelegt wurde es mir jedenfalls nicht, ich stamme aus einer Bauern und Metzgerfamilie. Manchmal hätte ich schon Lust auf eine Saucisson, aber das ist ein bisschen, wie mit Rauchen aufzuhören: Man muss einfach widerstehen, dann geht es auch wieder vorbei."
    * "In manchen Kulturen wurden Tiere den Göttern geopfert, nachdem man sie zuerst fast als heilig gehegt und gepflegt hatte. Wir waren schon immer gespalten zwischen Liebe und Fürsorge einerseits und Brutalität und Ausnutzung andererseits."
    * "Letztlich sind Tiere für uns immer Zweckobjekte, das ist das Problem. Sie existieren für uns nur in Bezug zu uns."
    * Beispiel zu Kinder und Hasen
        * "Ausser bei unseren Hasen, die ab und zu auf dem Teller landeten – was für mich aber weniger schlimm war als für meinen Bruder, der sich jeweils weigerte, das zu essen. Manchmal jedoch wurde er ausgetrickst: Man sagte ihm, was er da auf dem Teller habe, sei kein Hase. Und nachdem er gegessen hatte, hiess es, siehst du, das war eben doch Hase, und du hast keinen Unterschied geschmeckt. Worauf er in Tränen ausbrach. Das Problem war eben nicht der Geschmack, sondern das Wissen, woher das Fleisch kam. Auch sonst tun sich Kinder schwer, Fleisch zu essen, wenn sie wissen, von welchem Tier es stammt. Diese Skrupel müssen ihnen erst abgewöhnt werden."
    * "Noch immer gilt ein gutes Stück Fleisch auf dem Teller als Krönung der Kulinarik. Von dieser Idee müssen wir wegkommen. Dann verschwindet auch das Gefühl, es sei ein enormer Verzicht, kein Fleisch zu essen."
    * Zoos
        * "Würde man all das Geld, das man in Zoos steckt, vor Ort in die Lebensräume der Wildtiere investieren, könnte man damit sehr viel mehr erreichen."
        * "Zoos sind in erster Linie ein Geschäftsmodell, das der Unterhaltung dient. Es gibt durchaus Kinder, die auf diese Weise für das Thema sensibilisiert werden; viel wertvollere Erziehungseffekte ergeben sich jedoch aus Begegnungen in der Wildnis oder über Filme und Bücher. Problematisch finde ich auch, wenn man in Zoos noch Produkte aus Palmöl kaufen kann, während nebenan für den Erhalt des Lebensraums von Orang-Utans geworben wird."
    * Trophäenjagd
    * Tierversuche
        * "Dennoch zögere ich, ein vollständiges Verbot zu fordern. Kommt eine grosse Epidemie, brauchen wir schnell einen Impfstoff. Darum schlage ich ein Moratorium bei Tierversuchen in der Schweiz vor, begleitet von der Suche nach Alternativen, das wäre ein erster Schritt."
    * "Wo entsteht das grösste Tierleid: bei der Jagd, der Forschung, der Zoohaltung oder der industriellen Lebensmittelherstellung?"
        * "Ganz klar bei letzterem. **Dort ist das Leid auch völlig akzeptiert.** Dabei liegt etwa in Deutschland, wo viel geschlachtet wird, die Fehlerquote bei der Betäubung bei etwa fünf Prozent. Das heisst Hundertausende der etwa 55 Millionen geschlachteten Schweine pro Jahr sind dabei nicht richtig betäubt. Schwein zu sein in Deutschland, das ist die Hölle"
        * "In der Zukunft könnten die aktuellen Exzesse der Massentierhaltung ähnlich absurd wirken wie für uns heute Hexenverbrennungen."
    * Tierrechte?
        * "Sie hätten dann das Recht auf körperliche Unversehrtheit und dürften nicht getötet werden. Das würde bedeuten, dass wir uns nicht mehr von den Produkten dieser Tiere ernähren könnten. Es blieben künstliches Fleisch, Insekten sowie Fleisch von Tieren, die eines natürlichen Todes starben. Aber eigentlich brauchen wir tierisches Protein gar nicht, es gibt ausgezeichnetes pflanzliches Protein."
    * Mehr
        * https://sentience-politics.org/de/prof-markus-wild-machen-wir-der-tierholle-ein-ende
            * ...
        * ["VEGANISMUS – DER FILM | Argumente für eine vegane Lebensweise | Maturaarbeit"](https://www.youtube.com/watch?v=bVI-T8txNSs), 2017
            * mit Interviews mit Markus Wild

### What a fish knows
* "What a Fish Knows: The Inner Lives of Our Underwater Cousins" von Jonathan Balcombe, Ethologe. - https://www.scientificamerican.com/store/books/what-a-fish-knows-the-inner-lives-of-our-underwater-cousins/
    * Vortrag: ["Videovortrag zum Buch »Was Fische wissen«"](https://albert-schweitzer-stiftung.de/aktuell/videovortrag-zum-buch-was-fische-wissen), 1 h, 2018
* Video: ["Facts for Fish-Eaters: What You Didn't Know About Fish*es And Sealife – Lisa Kemmerer [IARC2017]"](https://www.youtube.com/watch?v=SPpSatWlTf4), 30 min
    * Fishes
    * Anymals
    * Es werden Leute angesprochen, die ungefähr so aussehen wie die Sprecherin (Industrieland, genug zu essen); und nicht andere Menschen (aus armen Ländern; außer Fisch nichts zu essen)
    * Inhalt
        * Wer sind Fische?
        * Fischerei-Methoden und -Praktiken (Netze, Haken, Beifang, Beitötung, Juvenescence)
        * Industrie und Klimawandel (Todeszonen, Versäurerung)
        * Aquakultur
        * "Nachhaltiger" Fisch
    * ... todo, 3 min 30 ...
    * ...
    * Buch_: ["Eating Earth: Environmental Ethics And Dietary Choice” By Lisa Kemmerer"](https://global.oup.com/academic/product/eating-earth-9780199391844?cc=de&lang=en&), 2014
        * Auszug: ["“Eating Earth: Environmental Ethics And Dietary Choice” By Lisa Kemmerer"](http://www.ourhenhouse.org/2015/04/excerpt-from-eating-earth-environmental-ethics-and-dietary-choice-by-lisa-kemmerer-and-a-bonus-giveaway-for-the-flock/), 2015

Meere
-----
### Überfischung
* Video: ["Überfischung der Meere"](https://www.youtube.com/watch?v=3KPUlMzFRks) einfach erklärt in 5 Minuten, Greenpeace Österreich, 2012, tag:class?
    * ...
    * Andere Fischer gehen leer aus
    * ...
    * Schleppnetze
    * ...
    * Beifang
        * Zahlen: 1 kg Garnelen, 9 kg Beifang
    * ...
    * Aquakulturen
    * ...
    * Politiker
        * diese ermutigen, die richtigen Entscheidungen zu treffen

* Artikel: ["Wir essen die Weltmeere leer"](https://www.zeit.de/wissen/umwelt/2016-01/ueberfischung-bedrohung-arten-fehler-angaben), 2016
    * "Darf's noch Fisch sein? Besser nicht: Seit Jahrzehnten könnten die Daten zum Fischfang weltweit fehlerhaft sein. Wir haben die Meere wohl stärker ausgebeutet als gedacht."

### Zahlen: fishcount.org
* http://fishcount.org.uk/

### Zahlen 2
* http://adaptt.org/about/the-kill-counter.html
    * "the world's fisheries record their catches in terms of metric tons, not the numbers of individuals" ()
    * Zahlen: "Weltweit werden min. 90 Millarden Meerestiere getötet." (adaptt)
        * "We at ADAPTT have settled on a rough estimate of 90 billion marine animals killed worldwide each year"
* https://www.vegan.eu/fischozid/ - Zahlen sind wesentlich höher. Warum?
    * Zahlen: "Jedes Jahr werden 900 bis 2700 Milliarden Fische für unseren Konsum gefangen und getötet, von diesen werden 450 bis 1000 Milliarden zu **Fischmehl oder Fischöl** verarbeitet.
        Jedes Jahr werden 37 bis 120 Milliarden Fische auf Fischfarmen für unseren Konsum getötet." (fishozid)
        * "Die Schätzung der Anzahl der getöteten Einzelfische ist schwierig, aber dadurch möglich,
            dass die FAO Zahlen zum jährlichem "Ertrag" aus Wildfang und Fischzucht in Tonnen vorlegt."
* https://live-counter.com/weltweit-getoetete-tiere/
    * ...

### Fischmehl (im Tierfutter)
* https://de.wikipedia.org/wiki/Fischmehl

* Fischmehl im Schweinefutter
* https://www.aquakulturinfo.de/fischmehl
    * Zahlen: "Das Gros (ca. 75 %) des weltweit produzierten Fischmehls wird in der Fischzucht
        verwendet und nur ca. 25 % für die Aufzucht anderer Nutztiere, wie Hühner und Schweine."
        * siehe auch Graphik, fast 100% des Fischmehls wird zur Tierfütterung verwendet
    * ... ausführliche Diagramme wo was wie ...
    * ...
    * Tiere sind Produkt: https://www.aquakulturinfo.de/schmerzempfindung-bei-fischen

* Beispiel-Firma: https://www.kmp-fischmehl.de/de/fischmehl/verwendung
    * "Seit vielen Jahrzehnten dient Fischmehl als Protein-Quelle für Nutztiere"
    * https://www.kmp-fischmehl.de/de/fischmehl/rueckverfolgbarkeit
        * praktisch nicht vorhanden

### Die Bucht - The Cove (2009)
* siehe tiere-artgerecht.md

### Fang-Methoden
* Doku: ["Schützt Fischzucht in Aquakulturen vor Überfischung der Weltmeere?"](https://www.youtube.com/watch?v=bjLZWcybXKg) (nein), NDR, 9 min, 2011, tag:class2017
    * Infos zu Fangmethoden, Antibiotika-Einsatz etc. in Fischzucht und Aquakulturen
    * Bilder von Lagern mit säckeweise Giften und Antibiotika. Uah. Medikamentenlager.
    * Lebendtransport zur Fabrik. Die armen Fische. Am Beispiel Pangasius.
    * Blutiges Sammelbecken mit Todesstich.
    * Arbeiter verdienen wenig.
    * Grund genug für einige YT-User sich darin bestärkt zu fühlen, dass es richtig ist, kein Fisch zu essen.

### Schleppnetze
* https://de.wikipedia.org/wiki/Schleppnetzfischerei

### Stellnetze in der Ostsee
* https://de.wikipedia.org/wiki/Stellnetz
    * "wegen des Verdachts auf großen Beifang"
* Schweinswale
    * https://taz.de/Arlamierende-Zahlen-aus-der-Ostsee/!5717557/
        * "In der Ostsee sind Schweinswale vom Aussterben bedroht. Die Zahl der tot aufgefundenen Meeressäuger bleibt hoch."
    * "Der Schweinswal ist der einzige heimische Wal an Deutschlands Küsten – und massiv bedroht. In der Ostsee kämpft er ums Überleben, die Schutzverpflichtungen werden nicht erfüllt."
        (https://www.greenpeace.de/themen/meere/meeresschutzgebiete/schweinswale-kleine-tummler-seenot)

### Elektro-Fischen
* 2018: Elektrofischen: http://www.tagesschau.de/wirtschaft/elektrofischen-101.html
    * "Erlaubt die EU das Elektrofischen?"
    * "Wir befürchten eine noch stärkere Überfischung der Meere durch die Elektrofischerei"
    * "Darüber hinaus können zu starke Stromschläge die Tiere verletzen. Bei größeren Rundfischen wie Kabeljau komme es in einigen Fällen zu so starken Muskelkrämpfen, dass dadurch das Rückenmark durchtrennt werde"
    * Nebelkerze: "Befürworter halten die Elektrofischerei dagegen für umweltschonender als die konventionelle Fischerei"
    * Würde des Fischs?

### Shrimps = Garnelen = Gambas
* Doku: ["Schmutzige Shrimps: Die Geschäfte der Garnelen-Industrie"](https://www.youtube.com/watch?v=Udvnt_erdcE), phoenix, 30 min, 2013
    * Erheblicher Chemieeinsatz etc.
    * Umweltprobleme

* "Europas größte Garnelenzucht liegt mitten in Bayern", 2018, https://www.augsburger-allgemeine.de/kultur/Journal/Europas-groesste-Garnelenzucht-liegt-mitten-in-Bayern-id44240131.html
    * "Sanfte" Tötungsmethode

* Marketing
    * "Wir lieben Seafood!" - https://www.crustanova.com/ / Superfrisch: https://gambanatural.es/

### Haie
* http://www.stop-finning.com, shark finning = Haiflossenfischen
    * "Im Sommer 2014 wurde Stop Finning Deutschland e.V. als gemeinnütziger Verein anerkannt."
    * https://de.wikipedia.org/wiki/Shark-Finning
        * "Shark-Finning (auch Hai-Finning oder kurz Finning) bezeichnet das Abtrennen der Finne (Rückenflosse) und anderer Flossen des Hais (englisch shark) und die anschließende Entsorgung des Tieres im Meer. Der Hai wird dafür normalerweise nicht getötet, er ist danach jedoch schwimmunfähig und sinkt zu Boden, wo er durch Ersticken verendet oder von anderen Fleischfressern gefressen wird."
    * Video: [Image-Film Stop Finning](https://www.youtube.com/watch?v=BXuOmYB_qlI), 2016, 11 min
        * Mischung aus Infos, was der Mensch mit Haien macht und warum und Naturaufnahmen von Haien im Meer
    * Video: [Was ist Finning?](https://www.youtube.com/watch?v=3vBnQxZZykE), 1 min, 2016, tag:offline, tag:class?
    * Doku: ["Der Mensch rottet die Haie aus - Faszination Wissen"](https://www.youtube.com/watch?v=HZba5Xu_15A), Bayerischer Rundfunk BR, 2015, 30 min, tag:offline, tag:class?
        * "Jedes Jahr tötet Homo Sapiens - die Krone der Schöpfung - über 100.000.000 Haie"
        * 1:45 "Das Fleisch der Tiere als Nahrung für den Menschen spielt dabei kaum eine Rolle"
            * Es geht um den Aberglauben Haifischflossen. Gelten als Potenzmittel in Asian. Fast 200mal teurer als Haifleisch.
                * (Es gibt hierzulande auch einen Aberglauben: dass der Mensch unbedingt alle möglichen Tierprodukte braucht, um gut und gesund zu leben)
        * 2:10 abgeschnittene Flossen; qualvoller Tod
        * 2:30 Haiabfangnetze an Stränden; Tod
        * 2:40 Der Weiße Hai, ein Mythos ist geboren
        * Naturbilder bis 8:50
        * 9:00 Straße von Messina
            * 9:20 Fischmarkt
        * ...todo...
* http://www.stop-finning.com/dr-erich-ritter-interview
* http://www.stop-finning.com/phil-watson-shaaark-cartoonist-interview

### Wale - Sea Sheperd
* Bekenntnisse eines...
    * https://de.wikipedia.org/wiki/Paul_Watson_%E2%80%93_Bekenntnisse_eines_%C3%96ko-Terroristen

Süßwasser
---------
### Karpfen: wie alt und groß?
* https://derkarpfenangler.de/wie-alt-und-gross-werden-karpfen/
    * K3 = 3 Jahre alt
    * werden 7 Jahre, 20 Jahre und älter, wenn man sie lässt
    * Alter und Größe

Menschen
--------
### Robert Marc Lehmann
* http://www.robertmarclehmann.com/home/
* u. a. Amerikanischen Serie "The Operatives": https://www.youtube.com/watch?v=g_lhGaPmJLg, "Military-style"-Trailer, 2 min

Fischerei-Siegel
----------------
### MSC-Siegel - Meeresfisch
* [MSC](https://de.wikipedia.org/wiki/Marine_Stewardship_Council)-zertifizierter Fisch - von einigen Umweltorganisationen empfohlen - ist leider nicht gut. Das Label wird u. a. von Greenpeace kritisiert; Details siehe z. B. hier: https://de.wikipedia.org/wiki/Marine_Stewardship_Council#Kritik
* Am besten Fisch aktiv vermeiden. Dies ist heutzutage sogar ganz praktisch machbar, siehe [vegan](../Vegan).
* Also z. B. Fischvermeidung als Regel; und Fischverzehr als seltene Ausnahme (z. B. im [Urlaub](../Urlaub)).
* Hinweis: Fische sind fühlende Lebewesen, siehe oben
* MSC-One-Way-Fragen:
    * https://www.msc.org/de/ueber-uns/fragdenMSC

### ASC-Siegel - Aquakultur
* 2018: https://albert-schweitzer-stiftung.de/helfen/petitionen/tierquaelerei-unter-wasser-stoppen

Angeln
------
Ein Bild von Janosch:

![](img/IMG_20160825-ZEIT-mag.jpg)

Ein Angler: "Natürlich ist es schön, wenn ein großer Fisch an der Angel hängt. Das Wichtigste am Angelsport ist für mich aber die Erholung.", Dez. 2016

Tradition oder Tierschutz/Mitgefühl? Beides ist vereinbar, siehe Tipp von Janosch.

Siehe auch
----------
* siehe auch [Zoo, Aquarien](../Zoo)
