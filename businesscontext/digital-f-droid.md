Team F-Droid
============

<!-- toc -->

- [Team F-Droid Teil 2](#team-f-droid-teil-2)
- [Team F-Droid Teil 1](#team-f-droid-teil-1)
- [F-Droid als Choice-Enabler für die Android-Platform / weg von der Vorschreibe-Mentalität](#f-droid-als-choice-enabler-fur-die-android-platform--weg-von-der-vorschreibe-mentalitat)
- [F-Droid ist Open Source - Was bedeutet das?](#f-droid-ist-open-source---was-bedeutet-das)

<!-- tocstop -->

Team F-Droid Teil 2
-------------------
* F-Droid - Das Datenschutz-Angebot
* F-Droid - Das Plus für den Datenschutz
* F-Droid - Das Plus für den Datenschutz, das erst da sein muss, bevor es die Kunden auswählen können
* F-Droid - Vorwärts in die digitale Freiheit
* F-Droid - Der Unabhängigkeits-Enabler
* F-Droid - Für mobile _und_ digitale Freiheit
* Mobilität, Freiheit, Unhängigkeit, das will ich! mit F-Droid

Team F-Droid Teil 1
-------------------
'Team F-Droid'

Unser Ziel: Den Usern die derzeit sicherste Plattform anbieten.

Willkommen zum freien Distributionskanal. Du willst Freiheit auf ganzer Linie?
Komfortabel mobil sein und das auch mal ohne Tracking?
Ihre User wollen das (siehe regelmäßige Umfragen zu Vertrauen in Google, todo).
Ermöglichen Sie Ihren Usern dieses Feature und das ohne Lizenzkosten.

Beste Technik mit Datenschutz by design.

Gesucht: App-Entwickler für Demo, etc. Hypothesen

Digitalisierung (kein Fokus auf Politik)

F-Droid als Choice-Enabler für die Android-Platform / weg von der Vorschreibe-Mentalität
----------------------------------------------------------------------------------------
* Menschen fühlen sich mit dem Monopol-Google unwohl
* Google macht auf Android einen Friss-oder-lass-es-Datenschutz
    * dem User wird alternativlos vorgeschrieben, was im Bereich Datenschutz gemacht wird
    * entweder man akzeptiert das Gebaren oder man verlässt die Platform
    * Alternative zu Googles Gutsherren-Mentalität gewünscht
* Es geht auch anders...
* Mit F-Droid geben Sie dem User eine Wahl
* Mit F-Droid geben Sie dem User die Möglichkeit, den Datenschutz nach seinen Wünschen zu gestalten
* Der Kunde als Gestalter
* Datenschutz in Selbstorganisation
* Datenschutz selber gemacht
* Kreativität des Kunden entfesseln
* Lassen Sie die User entscheiden, wie er möchte, dass man mit seinen Daten umgeht.
* Freiheit und Chancen für den Nutzer

F-Droid ist Open Source - Was bedeutet das?
-------------------------------------------
* siehe digital-oss.md
