Gemeinschaftsverpflegung
========================

<!-- toc -->

- [Inbox](#inbox)
  * [2020 - Nudging - Clevere Essentscheidungen anstoßen](#2020---nudging---clevere-essentscheidungen-anstossen)
  * [2016 - Bund Naturschutz Bio Kantine, Kitas / Bio-Essen vs. Krebs](#2016---bund-naturschutz-bio-kantine-kitas--bio-essen-vs-krebs)
  * [2019 Interview Veganfreundliche Mensa Heidelberg](#2019-interview-veganfreundliche-mensa-heidelberg)
  * [2019 Vegan in öffentlichen Kantinen, Schweiz](#2019-vegan-in-offentlichen-kantinen-schweiz)
  * [2017 Klimaschutz normal - vegetarisch - vegan](#2017-klimaschutz-normal---vegetarisch---vegan)
- [Bio-Essen in der Unternehmens-Kantine](#bio-essen-in-der-unternehmens-kantine)
  * [Vorreiter](#vorreiter)
  * [Vorbild BMUB](#vorbild-bmub)
- [Nahrung in der Klimawandel-Strategie: Drawdown](#nahrung-in-der-klimawandel-strategie-drawdown)
  * [Lösungen, die etwas mit Nahrung zu tun haben](#losungen-die-etwas-mit-nahrung-zu-tun-haben)
- ["Plant-based options must be available, visible, and enticing, including high-quality meat substitutes."](#plant-based-options-must-be-available-visible-and-enticing-including-high-quality-meat-substitutes)
  * [ProVeg Food Services](#proveg-food-services)
  * [Vegan aus verschiedenen prominenten Perspektiven](#vegan-aus-verschiedenen-prominenten-perspektiven)
- [Vegan-Infos für Caterer / Großküchen](#vegan-infos-fur-caterer--grosskuchen)
- [Veg* Essen in Kantinen - Beispiele](#veg-essen-in-kantinen---beispiele)
  * [WeWork-Coworking ohne Fleisch, 2018](#wework-coworking-ohne-fleisch-2018)
  * [WBS Training AG, vegetarisch und Bio](#wbs-training-ag-vegetarisch-und-bio)
  * [Portugal (min. 1x vegan pro Tag), 2017](#portugal-min-1x-vegan-pro-tag-2017)
  * [Berlin, 2017](#berlin-2017)
- [Veganes Essen in der Mensa](#veganes-essen-in-der-mensa)
  * [Berlin: Mensa Veggie No. 1 - Die grüne Mensa](#berlin-mensa-veggie-no-1---die-grune-mensa)
  * [Vegan Taste Week in Heidelberg](#vegan-taste-week-in-heidelberg)
  * [vegeTable Jena](#vegetable-jena)
  * [Mensa Mainz](#mensa-mainz)
- [Veganes Essen an Schulen](#veganes-essen-an-schulen)
  * [Vegane Kita in Frankfurt](#vegane-kita-in-frankfurt)
  * [Denkanstöße](#denkanstosse)
- [Positive Beispiele in Hotels und/oder Restaurants](#positive-beispiele-in-hotels-undoder-restaurants)
- [Fast-Food-Kette »Swing Kitchen« aus Österreich](#fast-food-kette-%C2%BBswing-kitchen%C2%AB-aus-osterreich)
- [Hintergrundwissen für Anbieter](#hintergrundwissen-fur-anbieter)
  * [Gestaltungsraum "Beschreibungen": gesund vs. genussvoll](#gestaltungsraum-beschreibungen-gesund-vs-genussvoll)
  * [Gestaltungsraum "Bilder": junge Tiere reduzieren Appetit auf Fleisch](#gestaltungsraum-bilder-junge-tiere-reduzieren-appetit-auf-fleisch)
  * [Lobby-Aktivitäten und Greenwashing bei Palmöl](#lobby-aktivitaten-und-greenwashing-bei-palmol)
  * [Kantine: Sonntagsbraten-Szenario / Bevormundungsangst](#kantine-sonntagsbraten-szenario--bevormundungsangst)
  * [Kantine: Vorreiter oder dem Gesetz hinterher?](#kantine-vorreiter-oder-dem-gesetz-hinterher)
  * [Gesundheit und DGE](#gesundheit-und-dge)
- [Hintergrundwissen 2](#hintergrundwissen-2)
  * [Tierprodukte und Umwelt](#tierprodukte-und-umwelt)
  * [Greenpeace-Report mit weltweitem Gerechtigkeitsaspekt](#greenpeace-report-mit-weltweitem-gerechtigkeitsaspekt)
  * [Warum ist der Bio-Ertrag niedriger und ist das schlimm?](#warum-ist-der-bio-ertrag-niedriger-und-ist-das-schlimm)
  * [Was ist mit Gentechnik gegen den Welthunger?](#was-ist-mit-gentechnik-gegen-den-welthunger)
  * ["Früher ging es doch auch ohne Bio"](#fruher-ging-es-doch-auch-ohne-bio)
  * [Gast/Konsument vs. Angebot/Unternehmen vs. allgemeine Politik?](#gastkonsument-vs-angebotunternehmen-vs-allgemeine-politik)
  * [Debatte Veggie-Day 2013](#debatte-veggie-day-2013)
- [Fragen klären](#fragen-klaren)
  * [Fleisch von Schweinen](#fleisch-von-schweinen)
  * [Fleisch von Hühnern](#fleisch-von-huhnern)
  * [Kuhmilch-Produkte](#kuhmilch-produkte)
  * [Fleisch vom Rind](#fleisch-vom-rind)
  * [Diquat bei Kartoffeln](#diquat-bei-kartoffeln)
  * [Hindernisse bei Bio-Bananen?](#hindernisse-bei-bio-bananen)
  * [Sicherstellen, dass nicht nur Überdüngung beitragen?](#sicherstellen-dass-nicht-nur-uberdungung-beitragen)
- [Vorschläge](#vorschlage)
  * [Kompetenz einkaufen (vegane Rezepte, giftarme Landwirtschaft)](#kompetenz-einkaufen-vegane-rezepte-giftarme-landwirtschaft)
  * [Bestellformular für Cater-Service / Projekt-Essen](#bestellformular-fur-cater-service--projekt-essen)
  * [Veranstaltungen für das höhere / Nachhaltigkeitsmangement](#veranstaltungen-fur-das-hohere--nachhaltigkeitsmangement)
  * [Finanzierungsidee](#finanzierungsidee)
  * [Nachhaltigkeit in der Landwirtschaft als Problem anerkannt?](#nachhaltigkeit-in-der-landwirtschaft-als-problem-anerkannt)
- [Motivation](#motivation)
  * [Umweltschutz](#umweltschutz)
  * [Mitarbeiter](#mitarbeiter)
  * [Aktive](#aktive)
  * [Vermeintliche Ohnmacht des Einzelnen](#vermeintliche-ohnmacht-des-einzelnen)
  * [Eier: McDonalds: teilweise Freilandeier](#eier-mcdonalds-teilweise-freilandeier)
  * [Maissilage und Mais-Monokultur](#maissilage-und-mais-monokultur)
- [Thema: Verschwendung](#thema-verschwendung)
- [Thema: Müllvermeidung](#thema-mullvermeidung)
  * [Mehrwegbecher vs. Einwegbecher](#mehrwegbecher-vs-einwegbecher)

<!-- tocstop -->

Inbox
-----
### 2020 - Nudging - Clevere Essentscheidungen anstoßen
* http://www.kern.bayern.de/wissenschaft/237916/index.php

### 2016 - Bund Naturschutz Bio Kantine, Kitas / Bio-Essen vs. Krebs
* Kantinen, kitas: "Seit 20 Jahren setzt sich der Bund Naturschutz für ökologisch erzeugte Lebensmittel ein – und zieht nun ein Resümee." https://www.abendzeitung-muenchen.de/inhalt.az-check-bio-in-kantinen-das-rechnet-sich-oeko-essen-fuer-wenig-geld.8906137e-e9b8-4146-9da2-549332e01782.html
    * Video über Bio-Essen vs. Krebs -> https://www.oekotest.de/gesundheit-medikamente/Bio-Lebensmittel-koennten-vor-Krebs-schuetzen-Grosse-Verbraucherbefragung-in-Frankreich-deutet-auf-niedrigere-Krebsrate-durch-Biokonsum-hin_600702_1.html
        * "25 Prozent weniger Krebsfälle"
        * "Die Häufigkeit des Konsums von Bio-Produkten beruht nur auf Aussagen der Teilnehmer."
        * "Besser konventionell angebautes Obst und Gemüse als gar kein Gemüse!"

### 2019 Interview Veganfreundliche Mensa Heidelberg
* https://albert-schweitzer-stiftung.de/aktuell/interview-veganfreundliche-mensa

### 2019 Vegan in öffentlichen Kantinen, Schweiz
* 2019: "Eine Initiative in Luzern setzt sich dafür ein, veganes Essen in die Kantinen zu bringen" https://www.veganblatt.com/vegane-kantinen

### 2017 Klimaschutz normal - vegetarisch - vegan
* https://cardamonchai.com/2017/02/klimaschutz-und-vegane-ernaehrung/

Bio-Essen in der Unternehmens-Kantine
-------------------------------------
* Begründung Bio, z. B.
    * [Umweltbundesamt: Landwirtschaft mit Zukunft – unser Film](https://www.umweltbundesamt.de/themen/landwirtschaft-zukunft-unser-film)
    * [2017: Ernährungsminister für mehr Bio-Essen in Kantinen und Mensas](http://www.morgenpost.de/politik/article210209321/Ernaehrungsminister-fuer-mehr-Bio-Essen-in-Kantinen-und-Mensas.html)
        * Forderung 20%-Anteil ist leider etwas tief gegriffen, aber besser als nichts.
    * Voraussetzung für einen nachhaltigen, selbst-bestimmten Konsum durch den Mitarbeiter
    * Positive Auswkirkungen: Nachhaltigkeit, Zukunftsfähigkeit, Generationengerechtigkeit

### Vorreiter
* Positive Beispiele: Bio in Unternehmenskantinen
    * [Abendzeitung München, 2016, Essen unter 4 Euro - Günstig Bio Mittagessen: In diesen Kantinen geht's](http://www.abendzeitung-muenchen.de/inhalt.essen-unter-4-euro-guenstig-bio-mittagessen-in-diesen-kantinen-geht-s.8906137e-e9b8-4146-9da2-549332e01782.html)
        * Die Mitarbeiterverpflegung bei Linde seit 15 Jahren - bio
            * "60 Prozent der Ware ist Bio, für Firmenverpflegung ist das ein sehr hoher Wert"
            * "Schweinefleisch bekommt man in den Mengen nicht in Bio-Qualität her."
                * => weniger Fleisch essen
            * "Gemüse ist fast ausschließlich Öko, genauso wie Reis, Nudeln oder Beilagen wie Spätzle."
            * "Die Leute merken, dass sie durch das Essen bei uns nicht nur satt, sondern auch gesund werden."
            * "einmal pro Woche wird an einer der Essensausgaben ein Mittagmenü frei von tierischen Produkten angeboten."
                * "Etwa eine vegane Paella mit mariniertem Tempeh aus Sojabohnen. „Das essen 200 Leute, das ist schon enorm für ein veganes Angebot“"
        * Die Kantine des Landwirtschaftsministeriums - bio
            * "Von den fünf Gerichten täglich ist eines regional, mindestens zwei Mal die Woche gibt es ein Menü mit ausschließlich Bio-Zutaten"
        * Die Kantine im Technischen Rathaus - bio und Tendenz vegan
            * "Fleischmann geht aber auch Wege, die mancher riskant fände: Er setzt auf Vegan. Vier Gerichte stehen täglich auf der Karte, Montag bis Donnerstag ist eines davon ohne tierische Produkte. Freitag kommt ein indischer Koch, der Exotisch-Veganes anbietet. „Das ist der Renner. Viele Beamte, die gerne schon Freitagmittag heimgehen, bleiben extra da“, sagt Fleischmann."
        * KVR - Kreisverwaltungsreferat - bio und Tendenz vegan
            * "80 von 400 Essen werden im Schnitt vegan bestellt"
            * "Wo vegan ist, ist meist Bio"
            * "Die Stadt schreibt zehn Prozent Bio-Anteil vor"
            * "Transparenz ist wichtig, damit die Leute den Geldbeutel aufmachen"
            * "Natürlich war die Umstellung auf Bio Arbeit. Sie brauchten neue Lieferanten. Aber das lief gut, sagt der Kantinen-Chef: „Es ist ein Prozess, der nicht abgeschlossen ist. Aber das ist er nie.“"
        * Das Casino der Versicherungskammer Bayern - bio
            * "Ein Viertel aller eingesetzten Waren sind in der VKB-Kantine Bio"

### Vorbild BMUB
* http://www.bmub.bund.de/bmub/aufgaben-und-struktur/catering-bei-veranstaltungen-des-bmub/, Februar 2017
    * "Kriterien für Nahrungsmittel, Beschaffung und Transport Für Dienstleister und Caterer, die Veranstaltungen des BMUB beliefern, gelten folgende Kriterien:
        * verwenden saisonale und regionale Lebensmittel mit kurzen Transportwegen,
        * verwenden nur Produkte aus ökologischem Landbau,
        * bevorzugen Produkte aus fairem Handel (beispielsweise bei Kaffee, Tee und Säften) und
        * verwenden weder Fisch oder Fischprodukte noch Fleisch oder aus Fleisch hergestellte Produkte."

Nahrung in der Klimawandel-Strategie: Drawdown
----------------------------------------------
https://www.drawdown.org/

* https://www.drawdown.org/solutions
    * Electricity Generation, Food, Buildings and Cities, Land Use, Transport, Materials
        * https://www.drawdown.org/solutions/food
            * Supply-side
            * Demand-side
            * inklusive Graphiken

### Lösungen, die etwas mit Nahrung zu tun haben
* 80 Solutions by Rank, die etwas mit Nahrung zu tun haben
    * 3 	Reduced Food Waste 	Food - Lebensmittelverschwendung
        * https://www.drawdown.org/solutions/food/reduced-food-waste
            * "In regions of higher income, willful food waste dominates farther along the supply chain. Retailers and consumers reject food based on bumps, bruises, and coloring, or simply order, buy, and serve too much."
            * "There are numerous and varied ways to address key waste points."
                * "In higher-income regions, major interventions are needed at the retail and consumer levels. National food-waste targets and policies can encourage widespread change."
    * 4 	Plant-Rich Diet 	Food
        * "Shifting to a diet rich in plants is a demand-side solution to global warming that runs counter to the meat-centric Western diet on the rise globally. That diet comes with a steep climate price tag: one-fifth of global emissions."
        * "Bringing about dietary change is not simple because eating is profoundly personal and cultural, but promising strategies abound."
            * KANTINE: "Plant-based options must be available, visible, and enticing, including high-quality meat substitutes."
            * "Also critical: ending price-distorting government subsidies, such as those benefiting the U.S. livestock industry,
                so that the prices of animal protein more accurately reflect their true cost."
        * "making the transition to a plant-based diet may be the most effective way an individual can stop climate change."
    * 5 	Tropical Forests 	Land Use
        * "Burning continues to be the preferred means of clearing land in the Amazon to make way for cattle."
    * 9 	Silvopasture 	Food
        * "Silvopasture is an ancient practice that integrates trees and pasture into a single system for raising livestock."
        * "Research suggests silvopasture far outpaces any grassland technique for counteracting the methane emissions of livestock and sequestering carbon under-hoof."
        * FINANCE: "Carbon aside, the advantages of silvopasture are considerable, with financial benefits for farmers and ranchers."
        * "Silvopasture often runs counter to farming norms and can be costly and slow to implement. Peer-to-peer education has proven effective for spreading it. "
        * KANTINE: wo kommt das Fleisch her?
    * 11 	Regenerative Agriculture 	Food
        * "Conventional wisdom has long held that the world cannot be fed without chemicals and synthetic fertilizers."
            * "Evidence points to a new wisdom: The world cannot be fed unless the soil is fed. [...] just the opposite of conventional agriculture."
            * FINANCE: "one of the greatest opportunities to address human and climate health, along with the financial well-being of farmers."
        * KANTINE: Bio?
    * 16 	Conservation Agriculture 	Food
        * "Conservation agriculture was developed in Brazil and Argentina in the 1970s, and adheres to three core principles": ...
            * minimale Bodenbearbeitung
        * KANTINE: Bio?
    * 19 	Managed Grazing 	Food
        * Grasende Tiere
        * KANTINE: wo kommt das Fleisch her?
    * 21 	Clean Cookstoves 	Food
    * 23 	Farmland Restoration 	Food
    * 24 	Improved Rice Cultivation 	Food
    * 28 	Multistrata Agroforestry 	Food
    * 53 	System of Rice Intensification 	Food
    * 60 	Composting 	Food
    * 65 	Nutrient Management
        * Alternativen zu Stickstoff-Düngung
    * 67 	Farmland Irrigation
    * 72 	Biochar

"Plant-based options must be available, visible, and enticing, including high-quality meat substitutes."
--------------------------------------------------------------------------------------------------------
* siehe oben

### ProVeg Food Services
* https://vebu.de/vebu-business/gastro/
    * "ProVeg Food Services (ehemals VEBU-Gastro) bietet sowohl für die Gemeinschafts- als auch für die Systemgastronomie umfassende Serviceleistungen an. Die Zielgruppe der Flexitarier sowie der vegetarisch und vegan lebenden Menschen wird immer relevanter. Mit Abwandlungen oder Ergänzungen Ihres Angebotes reagieren Sie auf aktuelle Trends, um der steigenden Nachfrage nach vollwertigen pflanzlichen Speisen gerecht zu werden. Wir freuen uns auf Ihre Anfrage und senden Ihnen gern individuelle Leistungsangebote zu."
    * Trainings
    * Status-Quo-Analyse
    * Menüentwicklung
    * Kommunikationskonzepte
        * !
    * "Vorteile für Ihr Unternehmen"
        * "Erweiterung des Kundenportfolios um die stark wachsende Zielgruppe der vegan-vegetarisch lebenden Menschen. Gleichzeitig erreichen Sie eine die immer größer werdende Zielgruppe bewusster, gesundheitsorientierter Konsumenten wie Flexitarier, die ihren Fleischkonsum reduzieren wollen."
        * "Geringer Wareneinsatz: Pflanzliche Gerichte sind oft kostengünstiger als Fleischgerichte."
        * "Ihr Angebot orientiert sich weiterhin an den offiziellen Standards der Deutschen Gesellschaft für Ernährung (DGE), denn auch die DGE unterstützt eine pflanzenbasierte Ernährung."
        * "Verbesserung des Nachhaltigkeitsprofils Ihres Unternehmens durch ein klimafreundliches und ressourcenschonendes Speisenangebot."

* https://vebu.de/vebu-business/ - "Deutschlands erste Unternehmens­beratung für den vegan-vegetarischen Markt"
    * "Diese Kunden vertrauen uns bereits": ...
    * ...

### Vegan aus verschiedenen prominenten Perspektiven
* das heißt nicht, dass irgendwer zu irgendwas gezwungen wird; dennoch ist ein Hintergrundwissen zu ganzheitlich veganer Ernährung aus verschiedenen Perspektiven sinnvoll...

* gesundheitlich
    * siehe gesundheit-ernährung.md
* Lebensmittelverschwendung
    * siehe unten: weggeworfene Tierprodukt-Kalorien gehen ca. 7 - 10 Mal mehr in die Ressourcen-Bilanz ein
* philosophisch:
    * [Richard David Precht: „Wir brauchen Kulturfleisch und Bad-Trade-Siegel“](https://www.animalequality.de/neuigkeiten/interview-richard-david-precht-fleisch-massentierhaltung)
        * ...
        * Clean Meat
        * Gülle
        * ...
        * "Wie reagiert denn die Tierindustrie, wenn Sie sie auf Entwicklungen wie Clean Meat hinweisen? - So etwas ist überzeugten Massentierhaltern nicht einfach begreiflich zu machen. Ich versuche, einem Tierhalter dann zu erklären, dass er pleite geht, wenn er das etablierte Geschäftsmodell noch zehn Jahre beibehält. Eine Ausnahme war jedoch das Publikum meines Vortrags auf der MEAT: Fleischproduzenten allesamt, sei es als Metzger oder als Tierhalter. Nach dem Hinweis, dass ihr Geschäftsmodell ausstirbt – und dass sie nicht den gleichen Fehler begehen sollen wie etwa die deutsche Automobilindustrie mit dem Elektroauto –, waren das sehr andächtige Zuhörer."
        * "Auch, aber **nicht alleine beim Konsumenten**. Die Politik macht es sich zu einfach, wenn sie sagt, der Konsument müsse durch seinen Einkauf entscheiden, ob er etwa Massentierhaltung unterstützen möchte oder nicht. Ich denke, man muss politisch nachhelfen, z. B. durch die Einführung eines Bad-Trade-Siegels."
            * "„Besonders tierverachtend produziert“. Das wäre eine erfolgreiche Kampagne, das Geschäftsmodell wäre tot. Ich bin felsenfest davon überzeugt, dass der mündige Bürger sich solche Produkte nicht mehr kaufen würde."
        * " Eine nachhaltigere Form zu leben und eine **nachhaltigere Wirtschaft zu betreiben sind schließlich wichtige Bausteine, um Wohl und Frieden zu erhalten**. Die Tierrechtsbewegung kann da sehr produktiv an einem gesellschaftlichen Wandel mitarbeiten. Aber um weithin gehört zu werden, sollte sie neben dem ethischen das ökologische Argument noch viel stärker nach außen tragen. Dass Massentierhaltung global gesehen der größte Umweltkiller überhaupt ist, ist schließlich ein starkes Argument – und zudem unabhängig von individueller Empathie und Sensibilität gegenüber Tieren."
    * siehe geo.de Harald Lemke
* ethisch / humanistisch:
    * ["Verleihung des Peter-Singer-Preises 2018 - Veganismus als neue Aufklärungsbewegung"](https://hpd.de/artikel/veganismus-neue-aufklaerungsbewegung-15690), 2018
        * details_available
* journalistisch:
    * ["Verschärfte Wahrnehmung"](https://www.zeit.de/zeit-magazin/2018/32/vegan-leben-fleisch-tiere-tierschutz)
        * "Nichts vom Tier – ist das denn so schwer? Vor einem Jahr fasste unser Autor den Entschluss, vegan zu leben – ein Jahr, in dem er viel gelernt hat. Von Bernd Ulrich"
            * Buch: "Alles wird anders: Das Zeitalter der Ökologie", Oktober 2019, von Bernd Ulrich (ZEIT)
                * ...
                * ...
                * ...
* religiös:
    * konfessionell katholisch: [Sozialethiker Kurt Remele über die Würde der Tiere - "Vegetarisches Essen ist Christenpflicht"](https://www.katholisch.de/aktuelles/aktuelle-artikel/vegetarisches-essen-ist-christenpflicht), 2016
* Umwelt:
    * todo link zu greenpeace, planetare grenzen, gülle
* Nachhaltigkeit:
    * todo link ...
* Klimaschutz / Staatsziele
    * FAZ: ["Rügenwalder-Mühle-Chef: „Die Politik übersieht den Einfluss der Ernährung auf den Klimawandel“"](http://www.faz.net/aktuell/wirtschaft/ruegenwalder-muehle-chef-politik-vernachlaessigt-agrarwende-15731885.html), 2018
        * Frage: Und was ist mit uns - übersehen wir das auch?
    * https://www.geo.de/natur/nachhaltigkeit/19473-rtkl-deutschlands-erster-vegan-professor-vegane-eltern-stehen-unter, siehe oben
        * "Die Grünen sind vor fünf Jahren krachend gescheitert mit der Forderung nach einem Veggi-Day in öffentlichen Kantinen. Ist das Thema für die Politik verbrannt? Oder geht da noch was? - Da muss was gehen. Denn wenn wir das Ganze aus Klimaschutz-Sicht betrachten, haben wir gar keine Alternative dazu, unsere Ernährung deutlich pflanzlicher zu gestalten als das momentan der Fall ist. Nachzulesen ist das auch im Klimaschutz-Gutachten des Wissenschaftlichen Beirats für Agrarpolitik, Ernährung und gesundheitlichen Verbraucherschutz der Bundesregierung. Da kam zum ersten Mal von einer großen Zahl von Wissenschaftlern in einem offiziellen Gremium die klare Handlungsempfehlung für die deutsche Politik: Wenn wir unsere Klimaschutzziele erreichen wollen, müssen wir den Konsum tierischer Lebensmittel, insbesondere von Fleisch, schnell und deutlich reduzieren. Es ist ja nachvollziehbar: Keiner möchte sich in so private Dinge wie das Essen reinreden lassen. Aber Essen ist eben heute nicht mehr nur Privatsache. Wie ich mich ernähre, hat globale Auswirkungen, unter anderem auf das Klima. Insofern wünsche ich mir von der Politik auch deutliche Schritte zur Umsetzung einer klimafreundlichen Ernährung."
            * ["Klimaschutz in der Land- und Forstwirtschaft sowie den nachgelagerten Bereichen Ernährung und Holzverwendung - Gutachten"](https://www.bmel.de/SharedDocs/Downloads/DE/_Ministerium/Beiraete/agrarpolitik/Klimaschutzgutachten_2016.pdf?__blob=publicationFile&v=3), 2016, 482 Seiten
                * **"Gutachten des Wissenschaftlichen Beirats für Agrarpolitik, Ernährung und gesundheitlichen Verbraucherschutz**
                    und des Wissenschaftlichen Beirats für Waldpolitik beim Bundesministerium für Ernährung und Landwirtschaft, November 2016"
                * ...
                * "Informationskampagnen schärfen und weiterentwickeln"
                    * "Allerdings ist die **Klimarelevanz von Fleisch, aber auch von Milchprodukten und Käse, bisher nicht hinreichend kommuniziert.** "
                * "Mit der öffentlichen Gemeinschaftsverpflegung eine Vorreiterrolle einnehmen (Adressat: Bund, Länder, Kommunen)"
                    * " **Die Förderung einer größeren Auswahl an Mahlzeiten ohne Fleisch bzw. Molkereiprodukten** wird als sehr vielversprechende Maßnahme zur Steuerung des Konsumverhaltens angesehen. Gleichzeitig lassen sich durch geeignete Maßnahmen in der Zubereitung der Mahlzeiten Lebensmittelabfälle reduzieren."
                * ...
                * "Während die Bereitstellung tierischer Produkte generell höhere Emissionen pro Produkteinheit verursacht, weisen Butter und Rind-/Kalbfleisch innerhalb der tierischen Produkte die höchsten Emissionen pro Kilogramm Lebensmittel auf"
                * "Erwähnenswert ist zudem, dass bei tierischen Produkten von Monogastriern, d. h. Kraftfutterverwertern wie Geflügel und Schweinen, rund ein Drittel der Gesamtemissionen aus direkten Landnutzungsänderungen (dLUC) und Landnutzung (LU) herrühren. Diese stammen zu einem erheblichen Teil aus ausländischer Produktion."
                * ...
                * "Mit dem Ziel des Klimaschutzes schlägt z. B. der dänische Ethikrat eine Steuer auf Rindfleisch vor."
                * Preiseffekte
                    * "Preisänderungen für tierische Produkte haben zudem Auswirkungen auf die Nachfrage nach anderen Produkten (Kreuzpreiseffekte). So zeigen verschiedene Studien, dass die Verbraucher Preissteigerungen bei bestimmten Produkten nicht nur kompensieren, indem sie auf andere Produkte ausweichen („Substitution“, also positive Kreuzpreiselastizitäten), sondern auch, indem sie den Konsum von anderen Lebensmitteln wie Gemüse und Obst einschränken (negative Kreuzpreiselastizitäten)."
                    * "Fiskalische Instrumente beeinflussen das Verhalten sozial schwächerer Zielgruppen besonders stark. Dagegen erreichen Informations- und Bildungsinstrumente  sozial schwache bzw. geringer gebildete Zielgruppen kaum. Beide Instrumentengruppen könnten daher komplementär eingesetzt werden."
                    * ...
                    * "obwohl Arbeiten zeigen, dass den Verbrauchern die Klimarelevanz z. B. von tierischen Produkten kaum bekannt ist"
                * "Implizite Ansätze zur Verhaltensänderung"
                    * "So haben verschiedene bundesweite Erhebungen deutlich gemacht, dass **in der Gemeinschaftsverpflegung nach wie vor Fleisch und Fleischerzeugnisse zu häufig** und Gemüse, Rohkost und Salat zu selten auf dem Speisenplan stehen. Ein direkter Ansatz kann darin bestehen, **das Angebot und die Präsentation der Speisen** so zu steuern, dass nur bestimmte Wahlmöglichkeiten gefördert werden."
                        * KANTINE
                    * ...
                    * "In der Nudge-Bedingung erhielten die Teilnehmenden eine **Speisekarte, die nur vegetarische Gerichte enthielt (Standardoption)**, mit dem zusätzlichen Hinweis, dass rund 3,5 Meter entfernt eine zweite Speisekarte an der Wand hängt, die noch andere Speisen inklusive Fleischgerichte enthält. Dies bedeutet, die Teilnehmenden konnten ein Fleischgericht wählen, nur mussten sie dafür aktiv werden („opt-out“), um die „Nichtstandard-Bedingung“ (Fleischgericht) zurealisieren. In dieser Bedingung wählte die überwiegende Mehrheit (73 %) nun ein vegetarisches Gericht."
                        * KANTINE
                            * Idee: siehe Instinkt
                * ...
                * ...
* landwirtschaftlich:
    * todo link zu Gülle, Schweinen, Kühen und Hühnern
* preislich:
    * ...
* geschmacklich / Ur-Instinkte:
    * ["Endlich verstehen - Stimmt es, dass Vegetarier kein Fleisch mehr vertragen?"](https://www.geo.de/wissen/ernaehrung/18846-rtkl-endlich-verstehen-stimmt-es-dass-vegetarier-kein-fleisch-mehr-vertragen), 2018
        * "Das ist offenbar ein Mythos. Denn zwar verschwindet bei vielen Vegetariern oder Veganern irgendwann der Appetit auf gebratenes Tier. [...] Doch essen könnten sie es – aus ernährungsphysiologischer Sicht – bedenkenlos."
        * "Menschen haben schon immer mal pflanzlich, mal tierisch gegessen.
        Denn der Mensch ist von Natur aus ein Allesfresser. Und Tage oder Wochen, in denen sich Menschen ausschließlich pflanzlich ernähren mussten,
        gab es in der Menschheitsgeschichte wohl schon immer – bis die Jagd wieder erfolgreich war."
            * Allesfresser, Allesesser => "können" vs. "dürfen"
                * Frage: Kennst du andere Beispiele von Dingen, die du als Mensch gut kannst, aber in der Regel nicht solltest (abgesehen von akuter Not)? / ::freiheit
                    * Hände und geistiges Geschick:
                        * im Kaufhaus stehlen
                        * Den Schwächeren (von dem man weiß, dass er sich nicht wehren kann) regelmäßig eine runterhauen, ohne dass es andere sehen
                        * Dinge kaputtmachen
                    * "wissen, was man tun": Bei Rot über die Ampel gehen
                    * "wissen, was man tun": Geschwindigkeitsbegrenzungen missachten
    * Frage: Du gehst an einer Weide vorbei, auf dem hinter einem Zaun Kühe und Kälber grasen. An was denkst du zuerst? (an Kalbsschnitzel und Steak aus der Hüfte?)
    * Idee: analog zu "Der T. Rex möchte nicht gefüttert werden, er möchte jagen." (http://de.jurassicpark.wikia.com/wiki/Tyrannosaurusgehege)
        * Tierprodukte etwas schwerer zugänglich präsentieren (siehe Vorschläge der Studie oben), damit die natürlichen Jagdinstinkte besser bedient werden.
    * siehe auch Nahrungsinstinkt
* bequemlich:
    * ...
* Gewohnheit:
    * ...
* Ernährungsgerechtigkeit
    * ...
* Tierwohl
    * ...
* Für Haustierliebhaber
    * Haustiere haben und selber vegan essen schließt sich nicht aus
    * ["Vegane Hundeernährung? Die armen Tiere!“](https://friederikeschmitz.de/vegane-hundeernaehrung-die-armen-tiere), 2017
        * ...
    * https://vebu.de/tiere-umwelt/haustierhaltung/
        * ...
    * Frage: Tierfutter tierischen Ursprungs wird schon lange nicht mehr nur aus den vom Menschen nicht mehr verwertbaren Resten hergestellt, richtig? Speziell bei Hunden, die man anscheinend auch vegan ernähren kann, wo zieht ihr da die Grenze zwischen den Bedenken vielleicht doch etwas falsch zu machen und den Bedürfnissen der Tiere, die für das Tierfutter leiden und sterben müssen?
* Tierrechte
    * ["Es ist unmoralisch, Haustiere [als Gefährten] zu [züchten]"](https://www.geo.de/magazine/geo-magazin/17325-rtkl-vegane-ernaehrung-es-ist-unmoralisch-haustiere-zu-halten), Geo 10/2017
        * aus Tierheimen
        * analog Flüchtlinge
        * ...
        * "Aber die Leute werden das moralische Problem mit der Haustierhaltung nie verstehen, solange sie Tiere essen und damit an der Ausbeutung von Tieren teilhaben."
            * => a) zumindest für Verständnis werben, b) Veganer: gleiche Grundwerte, nur die Sache wird stärker aus der Opfersicht betrachtet
        * ...
        * "Football-Star Michael Vick; er ergötzte sich daran, Hunde aufeinanderzuhetzen und umzubringen"
        * beantwortet auch die Frage, ob Veganer Haustiere halten "dürfen".

Vegan-Infos für Caterer / Großküchen
------------------------------------
* https://albert-schweitzer-stiftung.de/aktuell/vegan-schulung-caterer, 2018
    * "Die Nachfrage nach veganen Speisen steigt, auch im Bereich Catering und Großverpflegung."
    * "Die anfängliche Skepsis einiger Teilnehmenden wich schnell der Begeisterung für die Vielfalt und die Geschmackserlebnisse der pflanzlichen Küche."

* [Praxisleitfaden für vegane Großverpflegung](https://albert-schweitzer-stiftung.de/aktuell/praxisleitfaden-vegane-grossverpflegung), 2017

Veg* Essen in Kantinen - Beispiele
----------------------------------
Positive Beispiele / veganes Essen in Kantinen

### WeWork-Coworking ohne Fleisch, 2018
* [Positiver FAZ-Artikel](http://www.faz.net/aktuell/beruf-chance/beruf/firma-wework-streicht-fleisch-von-ihrem-speiseplan-15731605.html)
    * "Die 6000 Beschäftigten von „WeWork“ müssen künftig auf Currywurst und Co. verzichten – zumindest auf der Arbeit. Fleisch wurde dort vom Speiseplan gestrichen. Gut so, sagt ein Ernährungsexperte."
    * "Zwar geht der Trend ebenso zu leichterer Kost wie Salaten und gesunden Snacks, zudem bieten viele Kantinen mittlerweile auch ein vegetarisches, ganz modern geführte Unternehmen vielleicht sogar ein veganes Gericht an."
    * ...
    * "Die Initiative sei eine wichtige Maßnahme, um den ökologischen Fußabdruck des Unternehmens zu verbessern."
* https://albert-schweitzer-stiftung.de/aktuell/coworking-ohne-fleisch, 2018
    * "WeWork, einer der größten Anbieter von Coworking-Büros, hat angekündigt, bei firmeneigenen Veranstaltungen ab sofort kein Fleisch mehr zu servieren. Auch in den Selbstbedienungs-Kiosken, die sich in einigen der von WeWork betriebenen Büroräumen befinden, wird es nur noch fleischfreie Snacks geben. Zudem bekommen MitarbeiterInnen, die auf Firmenkosten essen, keine Ausgaben für Fleischgerichte mehr erstattet."
    * "Das Unternehmen betreibt in über 20 Ländern etwa 400 Coworking Büros, in denen Arbeitsplätze und Infrastruktur für einen begrenzten Zeitraum angemietet werden können. Mit der Neuausrichtung will WeWork seinen Angestellten die Möglichkeit geben, »mehr über gesundes, nachhaltiges und bewusstes Leben zu lernen«."
    * "Als Grund führte er den Umweltschutz an: Wer seinen ökologischen Fußabdruck verkleinern möchte, könne mit dem Umstieg auf eine fleischfreie Ernährung am meisten erreichen – mehr als etwa mit dem Wechsel auf ein Auto mit Hybridantrieb."
    * "Das Team habe hart gearbeitet, um ein nachhaltiges, reichhaltiges und leckeres Menü ohne Fleisch zu erstellen."

### WBS Training AG, vegetarisch und Bio
* https://blog.gls.de/der-wirtschaftsteil/ernaehrung-wirtschaftsteil-299/
* Webseite: https://www.wbstraining.de/
* ["Gemeinwohlbilanz, Bericht 2015"](https://www.wbstraining.de/fileadmin/user_upload/media-center/pdf/161201_GWOE-Bericht_WBS.pdf)
    * "Sofern ein Catering erfolgt, was jedoch nur auf Kundenwunsch im BT-Geschäft und bei Firmenveranstaltungen üblich ist, legen wir auf ein Angebot von ausschließlich vegetarischer Kost, Bio-Produkten und Getränken in Glasflaschen Wert."
    * "Zur Versorgung der Mitarbeiter mit Getränkenund Obs werden nur nachhaltige Bio-Produkte von zertifizierten Lieferanten eingekauft."
    * "... Zum Abschluss erhielt jeder beteiligte Mitarbeiter als Geschenk noch ein Paket mit vegetarischen Bio-Produkten"
    * "Firmenseminare, Meetings und sonstige überregionale Events werden überwiegend im Essentis bio-seminarhotel an der Spree durchgeführt."
        * "ausschließlich vegetarische und Bio-zertifizierte Speisen und Getränkegereicht."
    * C3.1 ERNÄHRUNG WÄHREND DER ARBEITSZEIT (RELEVANZ:HOCH):
        * "Der Vorstand der WBS TRAINING AG ist selbst überzeugter Vegetarier und Verfechter von Bio-Produkten. Mit seiner bewusst ökologisch und gesundheitsorientierten Lebenshaltung und deren Propagierung fungiert er als Vorbild für die Mitarbeiter."
        * "Bei Meetings, Tagungen, Seminaren, Feiern und ähnlichen Firmenevents wird grundsätzlich vegetarisches und möglichst Bio-Essen bereitgestellt."
        * "Auch wenn es keine firmeninternen Kantinen gibt, sind alle Standorte dazu angehalten, ihren Mitarbeitern Bio-Obst, Bio-Tee und Bio-Kaffee zur Verfügung zustellen. Trinkwasser wird nur in Glasflaschen oder über einen Wasserspender bereitgestellt, wodurch gesundheitsschädliche Plastikflaschen ausdrücklich nicht genutzt werden"
    * "Im Bereich Business Training wird je nach Auftrag auch ein Catering zum Training angeboten. Dabei legt die WBS Wert auf ein Angebot von biologischer und vegetarischer Kost.  WBS-weit macht dieses Verpflegungsangebot aber nur einen quantitativ geringen Anteil aus"

### Portugal (min. 1x vegan pro Tag), 2017
* Portugal: per Gesetz mindestens 1 vegane Speise sind in öffentlichen Kantinen, 2017
    * https://albert-schweitzer-stiftung.de/aktuell/portugal-vegane-speisen-in-kantinen-pflicht

### Berlin, 2017
* ["Bürgerbegehren: 9.500 Berliner wollen veganes Kantinenessen"](https://albert-schweitzer-stiftung.de/aktuell/buergerbegehren-9-500-berliner-wollen-veganes-kantinenessen

Veganes Essen in der Mensa
--------------------------
Positive Beispiele: wie ein Speiseplan mit einem täglichen veganen Gericht aussehen kann

### Berlin: Mensa Veggie No. 1 - Die grüne Mensa
* ["Willkommen in der ersten vegetarischen Mensa Deutschlands"](https://www.stw.berlin/mensen/mensa-fu-veggie-i.html)
    * Beispiele für gute Piktogramme inklusive einer Art Ampel
    * "Wir verwöhnen Sie mit unserem saisonalen, vegetarischen und veganen Angebot."
    * "leckere Kaffeespezialitäten aus nachhaltigem fairem Handel mit dem Fair-Trade-Logo zertifiziert an"
* ["Ernährung und Umwelt - In der Mensa ist vegan schon Standard"](https://www.tagesspiegel.de/berlin/ernaehrung-und-umwelt-in-der-mensa-ist-vegan-schon-standard/20473032.html), 2017
    * "Vielen Studierenden geht es beim Essen auch um Ethik. Die Grüne Mensa „Veggie N°1“ der FU belegt beim Ranking der Tierschutzorganisation Peta einen Platz ganz oben."
    * todo: "Besonders intensiv beschäftigen sich neben Berlin die Mensen in Augsburg, Essen/Duisburg, Erlangen/Nürnberg, Heidelberg und Koblenz mit veganer Ernährung. Dort gibt es täglich mehrere pflanzliche Gerichte zur Auswahl, Themenkochkurse, kostenfreie Ernährungsseminare, vegane Kochbücher zum Ausleihen, Vorträge und Aktionswochen."
* https://www.fu-berlin.de/campusleben/70-jahre/aktuell/180316-65-jahre-mensa/

### Vegan Taste Week in Heidelberg
* https://albert-schweitzer-stiftung.de/aktuell/vegan-taste-week-in-heidelberg, 2018
    * "Bereits seit Jahren überzeugt das Heidelberger Studierendenwerk mit seinem veganen Engagement."

### vegeTable Jena
* vegeTable in Jena: [Speiseplan](http://www.stw-thueringen.de/deutsch/mensen/einrichtungen/jena/vegetable.html#speisepl2)

### Mensa Mainz
* Mensa in Mainz: https://www.studierendenwerk-mainz.de/essentrinken/speiseplan/

Veganes Essen an Schulen
------------------------
Positive Beispiele / veganes Essen in Schulen

### Vegane Kita in Frankfurt
* positiver Artikel: ["Erster veganer Kinderladen in Frankfurt eröffnet"](https://www.echo-online.de/lokales/rhein-main/erster-veganer-kinderladen-in-frankfurt-eroffnet_19011228), Regine Herrmann, 2018
    * "„Wir wollten nicht, dass unsere Kinder im Kindergarten auf vegane Ernährung verzichten müssen”, erzählt Lucien Coy, einer der Initiatoren." ':-)'
    * "gesunde, vegetarische Bio-Ernährung vor allem in Städten ständiges Thema in Kindergärten und Schulen. Aber vegan? Das ist noch immer exotisch genug für eine Menge Aufregung."
    * "Ausreichende Nährstoff- und Vitaminzufuhr sei bei Kindern besonders wichtig: „Darauf achten vegane Familien sowieso.”"
    * "Dazu sei es nicht nötig, Vitaminpillen zu schlucken, angereicherte Lebensmittel oder spezielle Zahnpasta erfüllten den gleichen Zweck."
    * "Auch das Frankfurter Stadtschulamt hat keine Bedenken gegen den veganen Kinderladen, einmal im Quartal soll es Gespräche geben."
    * "Die Mokita-Gründer lassen sich von Ökotrophologen beraten, haben zusammen mit Köchen ein Ernährungskonzept ausgetüftelt."
    * "Der Bedarf ist offenbar da, Mokita hatte mehr Nachfrage als Plätze – auch von nicht vegan lebenden Familien. 40 Kinder werden es im Herbst sein, wenn die Eingewöhnungsphase vorbei ist."
    * "Ständiger Austausch mit den Eltern ist geplant und ein eigener Koch, erfahren mit der Zubereitung pflanzlicher Kost, sorgt dafür, dass immer alles frisch auf den Tisch kommt."
        * "„Wir wollten kein in Alu verschweißtes Essen, das im Konvektomaten aufgewärmt wird.” Er würde sich wünschen, sagt Coy, dass bei dieser Art der Kinderernährung auch mal so kritisch gefragt werde wie bei veganer Kost."
    * "Zoobesuche etwa wird es nicht geben, auch keine Kinderbücher, die das Leben auf dem Bauernhof mit Kühen und Schweinen idyllisch darstellen."
* skeptischer Artikel, aber mit positven User-Comments: http://www.taz.de/!5526707/

### Denkanstöße
* 2018: https://albert-schweitzer-stiftung.de/aktuell/vegane-tage-der-albert-schweitzer-kinderdoerfer
    * »Leichter als gedacht«
    * "Denkanstöße und lange Gespräche"
    * "Verwandte Themen wie Nachhaltigkeit, Umweltschutz und Gesundheit kamen bei den Projekttagen ebenfalls zur Sprache"
    * "Familie wolle in Zukunft »auf weniger Verpackung sowie mehr Saisonales und Regionales achten"
    * »Die Kinder sollten lernen, bewusst zu essen«

Positive Beispiele in Hotels und/oder Restaurants
-------------------------------------------------
für Ideen

Fast-Food-Kette »Swing Kitchen« aus Österreich
----------------------------------------------
* https://albert-schweitzer-stiftung.de/aktuell/vegane-fast-food-kette-kommt-nach-deutschland, 2018
    * "nachhaltiges veganes Fast Food"
    * "Uns fiel auf, dass unsere Gäste besonders an den Burgern und Beilagen interessiert waren. Das hat uns auf die Idee gebracht, mit diesen Speisen ein systemgastronomisches Konzept zu entwickeln, das die Eröffnung von wesentlich mehr Filialen ermöglichen würde."
    * "bei jedem einzelnen Teil darauf geachtet haben, die grünste, ökologischste Option einzusetzen"
    * "Antrieb ist es, Menschen zu zeigen, dass ökologisches, zukunftsorientiertes Handeln und pflanzliche, nachhaltige Konzepte absolut mit Genuss zu vereinbaren sind"
    * "All das ist ohne erhobenen Zeigefinger, dafür mit viel Freude möglich. Mit hochwertigem Essen, mit gutem Geschmack, kann man Menschen wesentlich besser erreichen. Wenn erstmal der Magen überzeugt ist, kommt oft auch der Verstand hinterher."
    * "Bei uns ist der Zucker nur in den Desserts."
    * "Man hat also nicht nur keinen Nachteil, sondern zusätzliche Vorteile, die mit dem Genuss einhergehen. Alles, was uns derzeit noch abhält, ist Tradition und Gewohnheit"
    * "Sobald Menschen dann bereits zum Großteil vegetarisch leben, werden sie sich auch mit dem Thema Tierethik befassen. All das sind Entwicklungen, die nicht mehr umkehrbar sind."
    * "Neben der Gestaltung von Einrichtung, Verpackungsmaterial, Energieeffizienz der Technik und natürlich der Beschaffung pflanzlicher Lebensmittel, halte ich die Sorge um Mitarbeiter und Mitarbeiterinnen für einen oft vernachlässigten Aspekt zum Thema Nachhaltigkeit. Wertschätzender, respektvoller Umgang miteinander hat bei uns einen hohen Stellenwert."

* 2017
    * Berghüs Schratt (plus Bio)
    * https://vebu.de/essen-genuss/vegane-restaurants/

Hintergrundwissen für Anbieter
------------------------------
Voraussetzung: Handlungsbedarf erkannt.

### Gestaltungsraum "Beschreibungen": gesund vs. genussvoll
* 2018: https://albert-schweitzer-stiftung.de/aktuell/label-studie-genuss-gesundheit
    * "»Cholesterinfrei«, »leicht«, »ohne Zuckerzusatz« – um den Verkauf zu fördern, werben die Anbieter häufig mit den gesundheitlichen Vorzügen einer Speise. Doch das ist offenbar die falsche Strategie, wie eine Studie von Forschern der Universität Stanford zeigt. Ihren Erkenntnissen zufolge verkaufen sich Speisen bedeutend besser, wenn sie als genussvoll, aromatisch und interessant beschrieben werden und der Gesundheitsaspekt nicht erwähnt wird."
    * "Bewirbt man gesunde Speisen, sollte man sich vermutlich nicht (in erster Linie) auf ihre gesundheitlichen Vorzüge beziehen. Stattdessen eignet sich eine kreative und genussvolle Beschreibung der Speisen wahrscheinlich besser, um mehr Käuferinnen und Käufer zum Zugreifen zu animieren."

### Gestaltungsraum "Bilder": junge Tiere reduzieren Appetit auf Fleisch
* 2018: https://albert-schweitzer-stiftung.de/aktuell/tierkinder-verderben-appetit-auf-fleisch

### Lobby-Aktivitäten und Greenwashing bei Palmöl
* 2017:
    * Info-Material für Großküchen von z. B. Unilever und vom "Roundtable on Sustainable Palm Oil (RSPO)"
* RSPO als Greenwashing-Label: https://de.wikipedia.org/wiki/Roundtable_on_Sustainable_Palm_Oil
   * "Zweck: Förderung des Wachstums und der Nutzung nachhaltigen Palmöls durch Kooperation innerhalb der Lieferkette und offenen Dialog mit allen Akteuren"
   * "Die Mitglieder sind gezwungen, den Absatz von nachhaltigem Palmöl «zu fördern und nicht zu beeinträchtigen». Das schreiben die internen Richtlinien zur Marktkommunikation seit November 2016 vor. Der springende Punkt: Negativwerbung ist nicht erlaubt. Die Mitglieder dürfen nicht einmal sagen, dass der Verzicht auf Palmöl bei einem Produkt umweltverträglicher sei als nachhaltiges Palmöl. Falls sie sich nicht daran halten, kann die RSPO-Organisation rechtliche Schritte gegen sie einleiten."
   * siehe auch https://de.wikipedia.org/wiki/Roundtable_on_Sustainable_Palm_Oil#Kritik

### Kantine: Sonntagsbraten-Szenario / Bevormundungsangst
* Allgemein wird empfohlen: 1x pro Woche Fleisch (Sonntagsbraten) und Tierprodukte im Allgemeinen reduzieren im Vergleich zum Durchschnitt
* Das heißt logischerweise, gibt es Menschen, die sich möglicherweise an irgendeinem Tag der Woche fleischfrei oder vegan ernähren möchten (oder sogar an 5 Tagen die Woche)
* Nachhaltigkeit im Unternehmensziel: Um die umweltbewussten Mitarbeiter zu unterstützen (und eben nicht zu bevormunden) ist es sinnvoll, an allen fünf Kantinentagen mindestens eine vegane Gerichtsoption anzubieten. Dieses Vorgehen hat nur Vorteile, oder?

* siehe auch NABU: https://www.nabu.de/umwelt-und-ressourcen/oekologisch-leben/essen-und-trinken/fleisch/13310.html
    * "Versuchen Sie, weniger Fleisch zu essen, und kochen Sie überwiegend vegetarisch.
    * "Verzichten Sie auch öfters mal auf andere tierische Erzeugnisse, wie zum Beispiel Milchprodukte."

### Kantine: Vorreiter oder dem Gesetz hinterher?
* 2017: https://albert-schweitzer-stiftung.de/aktuell/portugal-vegane-speisen-in-kantinen-pflicht
    * "Alle öffentlichen Kantinen in Portugal sind seit diesem Monat gesetzlich dazu verpflichtet, vegane Speisen anzubieten."

### Gesundheit und DGE
* ["Position zur veganen Ernährung"](https://www.dge.de/wissenschaft/weitere-publikationen/dge-position/vegane-ernaehrung/)
    * "Vegane Ernährung ist eine sehr strenge Form der vegetarischen Ernährung. Veganer verzehren ausschließlich pflanzliche Lebensmittel, sie lehnen alle tierischen Lebensmittel ab, teilweise auch Honig"
    * vorsichtiges Fazit, aber im Fließtext steht:
        * **"Vegetarische Ernährungsformen haben hinsichtlich der oben genannten Lebensmittel häufig eine günstigere Zusammensetzung als die in Deutschland übliche Mischkost in Bezug auf die Zufuhr von Nährstoffen und sonstigen positiv wirksamen (sekundären) Pflanzenstoffen."**
        * **"Allerdings kann angenommen werden, dass eine pflanzenbetonte Ernährungsform (mit oder ohne einen geringen Fleischanteil) gegenüber der derzeitig in Deutschland üblichen Ernährung mit einer Risikosenkung für ernährungsmitbedingte Krankheiten verbunden ist."**
        * ...
        * (siehe auch "Darum empfiehlt die DGE eine vegane Ernährung nicht")
        * (die vegane Menülinie kann kommen / **eigenverantwortliches Handeln des Mitarbeites möglich machen**)

* ["Die aktuelle vegane Lebensmittelpyramide"](https://albert-schweitzer-stiftung.de/aktuell/die-aktuelle-vegane-lebensmittelpyramide)

Hintergrundwissen 2
-------------------
### Tierprodukte und Umwelt
* 2019:
    * Über die Produktionsbedingungen informiert zu sein, hilft Menschen, über ihr Verhalten nachzudenken
        * --> Infos dazu einholen (rationale: IST-Situation einholen, um zu sehen, was schon auf dem neusten Stand ist und wo effektive Hebel für Verbesserungen sind)
* 2018:
    * https://blog.gls.de/der-wirtschaftsteil/ernaehrung-wirtschaftsteil-299/
        * Artikel: ["Grillen ist das neue Rauchen"](https://www.klimareporter.de/gesellschaft/grillen-ist-das-neue-rauchen), 2018
            * siehe auch ../Garten/README.md/Grillen
        * Artikel: SZ: [“Fleischkonzerne schaden dem Klima mehr als die Ölindustrie.”](https://www.sueddeutsche.de/wirtschaft/klimawandel-fleischkonzerne-co-emissionen-1.4058225)
            * "Die fünf weltgrößten Fleisch- und Molkereikonzerne sind für mehr Treibhausgas-Emissionen verantwortlich als die großen Ölkonzerne. Das ist das Ergebnis einer neuen Studie."
            * "Berechnungen zufolge müsste der jährliche Fleischkonsum bis 2030 auf 22 Kilo pro Person sinken"
        * “Ganz grundsätzlich essen die Deutschen viel zu wenig Gemüse. Deshalb ist jedes Gemüse, das Sie zusätzlich kaufen, zunächst einmal gut, ganz unabhängig von seiner Herkunft. Es ist gesünder und auch umweltfreundlicher als tierische Nahrungsmittel, weil der Anbau von Pflanzen viel weniger natürliche Ressourcen verbraucht als zum Beispiel die Produktion von Fleisch.”
        * aus Kommentaren: siehe oben "WBS Training AG"
    * https://www.wetter.de/cms/olis-klimablog-das-klima-kreist-aus-und-keinen-juckts-4125726.html
        * ...
        * "Aber wir machen einfach so weiter als gäbe es kein Morgen. Und wir hinterlassen mit dieser Arroganz und Ignoranz unseren Kindern einen kaputten Planeten. Einfach, weil wir nicht einsehen wollen, wie schnell sich alles wandelt. Augen zu und ab in die Kimakatastrophe."
    * Gibt es einen Unterschied zwischen Öko und Bio?
        * Nein, siehe https://natur-ratgeber.de/oekolandbau-unterschied-zwischen-bio-und-oeko/, 2014

### Greenpeace-Report mit weltweitem Gerechtigkeitsaspekt
* 2018: ["Weniger ist mehr - weniger Fleisch und Milch für ein besseres Leben und einen gesünderen Planeten"](https://www.greenpeace.de/sites/www.greenpeace.de/files/publications/05.03.18_greenpeace-report_weniger_ist_mehr_-_weniger_fleisch_und_milch.pdf)
    * S. 14 und 15: Graphik: aktueller Durchschnittsverbrauch von Fleisch und Milch aufgeschlüsselt nach weltweiter Region und regionsspezifische Reduktionsziele
        *  Westeuropa ist einer der Kandidaten, die ziemlich stark reduzieren müssen, wenn es gerecht auf der Welt zugehen soll.
    * dazu: https://www.greenpeace.de/themen/landwirtschaft/keine-geschmackssache
        * "Eine Greenpeace-Studie zeigt das globale Desaster der Landwirtschaft – und die erfrischend einfache Lösung: Weniger Fleisch- und Milchprodukte tun allem und allen gut, weltweit."

### Warum ist der Bio-Ertrag niedriger und ist das schlimm?
* 2018:
    * ["Landwirtschaft Bio bringt mehr Ertrag als gedacht"](http://www.spiegel.de/wissenschaft/natur/landwirtschaft-ertraege-im-biolandbau-werden-unterschaetzt-a-1007533.html), 2014
        * "Konventionelle Landwirtschaft liefert höhere Erträge als Ökolandbau - das bestreitet kaum jemand. Doch die Unterschiede sind geringer als viele glauben, wie eine umfangreiche Vergleichsstudie zeigt."
        * das gleiche auf [topagrar](https://www.topagrar.com/news/Home-top-News-Ertraege-im-Biolandbau-hoeher-als-gedacht-1625805.html)
            * "So sei der Ertrag im Ökolandbau durchschnittlich um 19 % geringer. Je nach Anbauverfahren schrumpfe der Unterschied aber auf 8 bis 9 %, schreiben Forscher im Fachblatt "Proceedings of the Royal Society B""
    * ["Bio schlägt konventionell"](https://enorm-magazin.de/bio-schlaegt-konventionell), Jahr?
        * "Zu teuer und zu wenig Erträge – so lauten die größten Kritikpunkte an biologischer Landwirtschaft. Eine neue Studie zeigt jedoch, dass der Ökolandbau das Potenzial hat, eine große Rolle in der Welternährung zu spielen"
        * "Da bisher erst auf einem Prozent der weltweiten landwirtschaftlichen Nutzfläche Ökolandbau betrieben werde, besitze er großes Potenzial, eine viel größere Rolle bei der Welternährung zu spielen"
    * ["Interview zur Ertragsstagnation im Ökolandbau"](https://www.oekolandbau.de/erzeuger/pflanzenbau/allgemeiner-pflanzenbau/duengung/ertragsstagnation-im-oekolandbau/), 2017
        * ...

### Was ist mit Gentechnik gegen den Welthunger?
* In den Drawback.org-Lösungen kommt die Gentechnik nicht vor. Wie relevant ist sie?
* ["Are Genetically Altered Foods The Answer to World Hunger?"](http://www.earthisland.org/journal/index.php/eij/article/are_genetically_altered_foods_the_answer_to_world_hunger/), 2002
    * ...
    * "their products have yet to do a thing to reverse the spread of hunger. There is no more food available for the world’s less fortunate. In fact, most of the fields were growing transgenic soybeans and corn that are destined for livestock feed."
    * “Neither Monsanto nor any of the other genetic engineering companies appears to be developing genetically engineered crops that might solve global food shortages.”
        * "Monsanto would be developing seeds with certain predictable characteristics including:
            - able to grow on substandard or marginal soils;
            - able to produce more high-quality protein with increased per-acre yield, without the need for expensive machinery, chemicals, fertilizers or water;
            - engineered to favor small farms over larger farms;
            - cheap and freely available without restrictive licensing; and
            - designed for crops that feed people, not meat animals."
    * "In 1999, he compared the soybean yields in the 12 states that grew 80 percent of US soybeans and found that the yields from genetically modified soybeans were 4 percent lower than conventional varieties."
    * "If genetically modified foods really were an answer to world hunger, it would be a powerful and persuasive argument in their favor. How could anyone stand in the way of feeding desperate and starving people?"
        * "But Dr. Vandana Shiva, one of the world’s foremost experts on world hunger and transgenic crops, claims that the argument that biotechnology will help feed the world “is on every level a deception... Soybeans go to feed the pigs and the cattle of the North. All the investments in agriculture are about increasing chemical sales and increasing monopoly control.""
            * "The food they will produce will be even more costly."
            * https://de.wikipedia.org/wiki/Vandana_Shiva
                * "Für ihr Engagement in den Bereichen Umweltschutz, biologische Vielfalt, Frauenrechte und Nachhaltigkeit wurde sie mehrfach ausgezeichnet."
        * Stimmen aus Afrika: "Similarly, delegates from 18 African countries at a meeting of the UN Food and Agriculture Organization responded to Monsanto’s advertisements with a clear statement: “We... strongly object that the image of the poor and hungry from our countries is being used by giant multinational corporations to push a technology that is neither safe, environmentally friendly, nor economically beneficial to us. We do not believe that such companies or gene technologies will help our farmers to produce the food that is needed... On the contrary... it will undermine our capacity to feed ourselves.”"
    * Lobbying: "In 2000, a coalition of biotech companies began a $50 million media campaign to keep fears about genetically altered foods from spreading through the US. Bankrolling the campaign (which included $32 million in TV and print advertising) were Monsanto, Dow Chemical, DuPont, Swiss-based Novartis, the British Zeneca, Germany’s BASF and Aventis of France."
* Material für Schulen: ["Gentechnik – keine Lösung des Hungerproblems"](https://www.schule-und-gentechnik.de/lehrer/themen/welthunger/#gsc.tab=0)
    * "Der Arbeitsbericht „Forschung zur Lösung des Welternährungsproblems – Ansatzpunkte, Strategien, Umsetzung“ des Büros für Technikfolgenabschätzung beim Deutschen Bundestag stellt klar, dass bei der Bekämpfung des Welthungers viele Aspekte eine Rolle spielen.", 2011
    * ["Pro und Contra"](https://www.schule-und-gentechnik.de/schueler/pro-und-contra/#c7869)
    * ["Über uns"](https://www.schule-und-gentechnik.de/ueber-uns/das-projekt/#gsc.tab=0)
        * "Europa will keine gentechnisch veränderten Lebensmittel auf dem Teller, auch viele Landwirte lehnen Gentechnik auf dem Acker und im Stall ab (gesammelte Meinungsumfragen). Für diese Skepsis in der Gesellschaft gibt es gute Gründe."
    * Impressum: Zukunftsstiftung Landwirtschaft
* ["Kampf gegen den Welthunger - Zwischen Gentechnik und Ökozucht"](https://www.deutschlandfunk.de/kampf-gegen-den-welthunger-zwischen-gentechnik-und-oekozucht.724.de.html?dram:article_id=371340), DLF, 2016
    * "Weltweit leisten Kleinbauern den mit Abstand größten Beitrag zur Welternährung. Statt teuren Hightech-Saatguts brauchen sie aber Zugang zu Saaten, die sie selbst reproduzieren können. Keine einfache Aufgabe, denn ihr Gegner ist der größte Agrochemiekonzern der Welt."
    * ...
    * "Die Ernte wandert oft in die Futtertröge in Nordamerika und Europa."
    * ...
    * Grundnahrungsmittel, Ernährungssicherung: "Die großen Saatgut- und Chemie-Konzerne stecken ihre Entwicklungs- und Forschungsgelder allerdings lieber gezielt in Pflanzen, die sich kommerziell gut verwerten lassen, kritisiert der kanadische Entwicklungshelfer und Technikkritiker Pat Mooney: "Die Unternehmen forschen an zu wenigen Sorten; sie reden zwar viel über ihre Forschung, aber tatsächlich investieren sie ihr Geld lieber in Werbung. ...""
    * ...
* ["Gentechnik - Keine Lösung für den Welthunger"](http://www.umweltinstitut.org/themen/gentechnik/gentechnik-basisinformationen/gentechnik-und-welternaehrung.html)
* Was sagen unabhängige Quellen zur Rolle von Monsanto - einem der größten Agrar-Gentechnik-Konzerne - in Bezug auf Zukunftsthemen wie Nachhaltigkeit und Umwelt?
    * ["Glyphosat: Hat Monsanto Wissenschaftler gekauft?"](https://www.zeit.de/wissen/umwelt/2017-10/glyphosat-monsanto-wissenschaftler-bestechung-eu-kommission), 2017
        * "Schwere Vorwürfe: Monsanto soll Forscher bezahlt haben, damit diese positiv über Glyphosat berichten. Das legen neue Veröffentlichungen von internen Mails nahe."
        * "Ohne Glyphosat muss sich die Landwirtschaft ändern"
        * ...
    * https://de.wikipedia.org/wiki/Monsanto
        * siehe Kontroversen, derzeit mindestens 9 Felder
        * https://de.wikipedia.org/wiki/Monsanto#Bewertungen
            * ...
        * Filme und Literatur, z. B. "Monsanto auf Deutsch. Seilschaften der Agrogentechnik zwischen Firmen, Behörden, Lobbyverbänden und Forschung", 2010
* Wer kennt sich gut mit Welternährung aus?
    * die "Internationalen Ernährungs- und Landwirtschaftsorganisation der Vereinten Nationen (FAO)"
        * siehe z. B. ["Projekte zur Ernährungssicherung - der Bilaterale Treuhandfonds mit der FAO"](https://www.bmel.de/DE/Landwirtschaft/Welternaehrung/_Texte/Ernaehrungssicherungsprojekte.html;jsessionid=35F83B63CA94615FCB8430E87539B590.1_cid385)
            * "Schwerpunktthema 1: Das Recht auf Nahrung und andere Strategien zur nachhaltigen Ernährungssicherung"
            * "Schwerpunktthema 2: Die Freiwilligen Leitlinien für die verantwortungsvolle Verwaltung von Boden- und Landnutzungsrechten, Fischgründen und Wäldern"
            * "Schwerpunktthema 3: Unterstützung nachhaltiger Erzeugung und standortangepasster Produktionsmethoden"
            * "Schwerpunktthema 4: Bedeutung der Bioenergie für die Ernährungssicherung"
            * "Schwerpunktthema 5: Bekämpfung von Unter- und Mangelernährung"
            * Gentechnik scheint da nicht von überragender Relevanz zu sein.
* ["Kann Gentechnik den Kampf gegen den Welthunger gewinnen?"](https://www.morgenpost.de/wirtschaft/article208468255/Kann-Gentechnik-den-Kampf-gegen-den-Welthunger-gewinnen.html), 2016
    * ...
    * "Der Biotechnologe Wilhelm Gruissem von der Eidgenössischen Technischen Hochschule Zürich (ETH) hält dieses Szenario für unrealistisch: „Es ist illusorisch zu denken, dass wir bis 2050 alle Menschen mit Biolandbau ernähren können.“"
        * https://de.wikipedia.org/wiki/Wilhelm_Gruissem
            * "1998 stand Gruissem im Zentrum einer wissenschaftspolitischen Kontroverse in den USA. Sein Department an der UC Berkeley ging mit dem Novartis Agro Discovery Institute (heute Syngenta) eine Zusammenarbeit ein, die sich Novartis 25 Millionen Dollar kosten ließ. Gruissem gilt als der Architekt dieses Abkommens, das von den Forschern unter anderem verlangte, einen Vertrag zu unterzeichnen, der es ihnen verbot, wissenschaftliche Resultate ohne Einwilligung von Novartis zu publizieren."
            * "Gruissem war für zahlreiche Firmen als Berater tätig, unter anderen für Monsanto und Syngenta."
            * unabhängig?
    * ...
    * "Landwirtschaftsexperte Herren stimmt zu. „Der Staat darf sich nicht aus der landwirtschaftlichen Forschung zurückziehen und der Privatwirtschaft überlassen“, so Herren, „Ernährung und Gesundheit sind ein Menschenrecht.“"

### "Früher ging es doch auch ohne Bio"
* Früher WAR alles bio. Erst seit dem ersten Weltkrieg gibt es billigen Stickstoff-Dünger und einige Jahrzehnte später chemische Pestizide.

### Gast/Konsument vs. Angebot/Unternehmen vs. allgemeine Politik?
* https://blog.gls.de/der-wirtschaftsteil/konsum-wirtschaftsteil-300/ - Thema Konsum
    * https://www.fraumeike.de/2018/wollen-koennen-machen/ - bewusster Konsum für Anfänger - Bewusster Konsum: vom schlechten Gewissen zum Handeln
        * “Ich bin es so leid, dabei zuzusehen, wie sich Verbraucher („Ich kann nicht“), Unternehmen („Dafür besteht keine Nachfrage“) und Politik („Wir können Verbraucher und Unternehmen nicht entmündigen“) gegenseitig die Verantwortung für eine Veränderung der globalen Situation in die Schuhe schieben und sich genau deshalb absolut nichts ändert.”
        * schönes Intro für Anfänger und alle anderen: "Wer mir bei Twitter folgt, weiß, dass ich im Moment sehr damit hadere, dass wir Bewohner der reichen Ersten Welt immer noch viel zu zögerlich damit sind, unser Alltags- und Konsumverhalten dem Wissen um die gravierende Ausbeutung und Umweltschädigung, die daraus entstehen, anzupassen. Ich war selber lange ganz groß darin zu glauben, es sei mit einer kritischen Grundhaltung getan. Es würde irgendwie reichen, ein schlechtes Gewissen zu haben bei den unvernünftigen, ausbeuterischen, egoistischen und bequemen Dingen, die wir jeden Tag tun. Aber es reicht nicht. Das Plastik in den Weltmeeren reduziert sich um genau nichts, wenn ich zwar kritisch bin, aber weiterhin eingeschweißte Lebensmittel, Wasserflaschen, Kleidung aus Kunstfaser und Plastiktüten kaufe bzw. benutze. Die Menschen und Umwelt ausbeutenden Aktivitäten des Nestlé-Konzerns werden nicht nur nicht besser, sondern sogar schlimmer, wenn wir weiter Evianwasser schlürfen, uns ein Kitkat wegknuspern oder irgendwelche anderen Nestlé-Marken kaufen – egal, wie kritisch wir es tun."
        * sehr gute Kommentare mit Tipps
            * "Dort findet man eine interaktive Deutschlandkarte, auf der Leute eintragen, wenn Sie Obst, Kräuter und Nüsse kostenlos zum Sammeln finden."
                * https://mundraub.org/ - Karte mit Obstbäumen, Kräuter und Nüsse

### Debatte Veggie-Day 2013
* ["Kantinencheck - Einmal vegetarisch, bitte"](https://www.zeit.de/lebensart/essen-trinken/2013-08/kantinen-check-deutschland)
    * "In Deutschland wird über einen fleischlosen Tag in Kantinen debattiert. Wir fragten nach: Was steht bei der Techniker Krankenkasse oder ThyssenKrupp auf dem Speiseplan?"
    * ...
    * User-Comments
        * "Als wenig-Fleisch-Esser möchte ich mir nicht von der Kantine vorschreiben lassen, wann ich Fleisch essen MUSS"
        * Erweiterung 2018: Als Fast-Vegan-Esser... wann ich Tierprodukte essen muss.

Fragen klären
-------------
### Fleisch von Schweinen
* 1 Frage: Werden Ferkel kastriert und wenn ja, geschieht dies - wie empfohlen - mit Betäubung?
    * siehe https://www.tagesschau.de/inland/ferkel-kastration-101.html, 2018
        * "In Deutschland sollen Ferkel noch länger ohne Betäubung kastriert werden dürfen. Das hat die Koalition in der Nacht mitgeteilt. Ab 2019 ist dies verboten, aber die Regierung will noch einen Aufschub um zwei Jahre erreichen."
* Backlog
    * Wo kommt das Schweinefleisch her?
        * siehe [arte: "Das Tier und wir - Das Schwein"](https://www.arte.tv/de/videos/070357-003-A/animalisch-das-tier-und-wir/), 2018, 26 min, Kategorie "Positive Beispiele"
            * Alternative Freiland-Schweinehaltung
            * todo
        * Punkte klären siehe tiere-artgerecht.md
        * Antibiotikaeinsatz?
        * Negative Beispiele:
            * ["Ferkel erschlagen in Schweinezuchtbetrieb in Brandenburg"](https://www.mdr.de/brisant/schockierende-aufnahmen-schweinezuchtbetrieb-brandenburg-100.html), 2018
                * gefilmt durch ariwa
                * "Betreiber ahnungslos"

### Fleisch von Hühnern
* 1 Frage: Antibiotika (vor allem Reserve) in der Hühnerhaltung ist nach wie vor ein großes Problem. Wieviel wird konkret zur Produktion von Hühnerfleisch und Eiern verbraucht?
* Backlog
    * Wie sieht es in den Produktionsbetrieben aus? Beispielbilder?
    * Rassen?

### Kuhmilch-Produkte
* Enthornung?
* Stallgröße?
* (Rassen?)
* ...

### Fleisch vom Rind
* ...

### Diquat bei Kartoffeln
* ...

### Hindernisse bei Bio-Bananen?
* ...

### Sicherstellen, dass nicht nur Überdüngung beitragen?
* ...
* siehe Trinkwasser-Bericht

Vorschläge
----------
### Kompetenz einkaufen (vegane Rezepte, giftarme Landwirtschaft)
* Schulungen für vegane Rezepte
* Bio-Zutaten einkaufen: nachhaltige Landwirtschaft den Profis überlassen

### Bestellformular für Cater-Service / Projekt-Essen
* So abändern, dass Tierprodukte auf einem separaten Formular (das gesondert zu beantragen ist) zu bestellen sind
    * "Unsere tierproduktbasierte Auswahl finden Sie auf dem Formular T2"
    * Unterstützer finden analog zu Erasmus School of Economics
        * Tipps zur generellen Vorgehensweise: https://www.reddit.com/r/vegan/comments/8acs5b/how_i_turned_my_universityfaculty_vegan/

### Veranstaltungen für das höhere / Nachhaltigkeitsmangement
* Wie sieht es dort aus, wo die Nachhaltigkeitsziele erstellt werden?
* Welche Art von Catering wird dort geordert?

### Finanzierungsidee
* IST-Zustand Verhältnis pflanzenbasierte/tierbasierte Absatz feststellen
* von der wissenschaftlichen Studie empfohlenes Nudging einführen
    * gleichzeitig Pilot Bio-Gemüse / sollte immer günstiger sein als Tier-Alternativen
    * Fleisch preislich nicht nach oben anpassen, um Abwanderungseffekte zu vermeiden
* Wenn sich das Verhältnis nach 1 Jahr Richtung Pflanzen verbessert hat, dann Zuschuss vom Konzern für noch mehr Bio-Gemüse.

### Nachhaltigkeit in der Landwirtschaft als Problem anerkannt?
* ...

Motivation
----------
### Umweltschutz
* Mögliche Kette eines einfachen Frage-Ansatzes:
    * **Ist eine intakte Umwelt heute und morgen (Kinder/Enkel) wünschenswert?** (Vielleicht sogar in Drittländern?, siehe Importware aus Übersee)
        * NEIN: ok, fertig (unwahrscheinlich)
        * JA: Was sind derzeit die Haupt-Herausforderungen? (siehe Planetare Grenzen)
            Welche Lösungen werden empfohlen (Drawdown.org)
            Welche Probleme speziell im Bereich Landwirtschaft? (Artensterben, Stickstoffkreislauf, Grundwasserbelastung wegen Überdüngung und Pestiziden, Tierhaltung, ..., ...)
            Welche Lösungen? (s. auch letzter Punkt)
            * Welche Themen liegen in unserem Verantwortungsrahmen und wie stehen wir pro Thema da im Vergleich zu nachhaltigen den Empfehlungen?
                * alle Themen: SEHR GUT: (unwahrscheinlich)
                * pro Thema:
                    * Wie ist die IST-Situation?
                    * Welche Hindernisse gibt es, die IST-Situation zu erheben? (z. B. Angst vor Transparenz?)
                    * Welche Hindernisse gibt es bei einer möglichen Umsetztung aufgezeigter Verbesserungswege?
* Generelle Einwände:
    * "Wir machen schon viel" / "Wir haben schon viel gemacht" (siehe auch denkweise.md)
        * FRAGE: Sind die Probleme schon gelöst? (beim Umweltschutz kann man immer einen Schritt voran gehen; siehe auch "Verantwortung Hochverbrauchsländer")
        * FRAGE: Was sind die nächsten Schritte?
        * FRAGE: Nach welchen Kriterien wurde die Schritte ausgewählt? Transparenter Auswahlprozess?

* "Frau Klöckner plädiert für mehr Forschung. Das ist richtig. Wir müssen endlich beginnen, anstatt in Gentechnik und Chemielandwirtschaft in die Entwicklung stabiler Agrarsysteme zu investieren. Systeme, in denen Tiere und Pflanzen nicht in der Regel sondern in der Ausnahme krank werden. Der Ökolandbau steckt voller Ideen für eine solche Entwicklung. Die wollen wir der Politik gerne zur Verfügung stellen!" (https://www.boelw.de/presse/pm/loewenstein-widerspricht-kloeckner-bio-braucht-keine-chemisch-synthetischen-pestizide/, 2018)

### Mitarbeiter
* ["Kantinen in Unternehmen - Je hochwertiger das Essen, desto höher die Motivation"](https://www.wiwo.de/erfolg/management/kantinen-in-unternehmen-je-hochwertiger-das-essen-desto-hoeher-die-motivation/20716998.html), 2017
    * "Veganes Essen ist für eine breite Mehrheit nicht nur ein vorübergehender Modetrend: 71 Prozent der Befragten halten vegane Lebensmittel langfristig für relevant."
    * "Neun von zehn Deutschen wünschen sich verbindliche Qualitätsstandards für Essen in Kitas und Schulen."
    * "Fast neun von zehn Deutschen finden wichtig oder sehr wichtig, dass Ernährung auch als Schulfach gelehrt wird, wie Mathematik oder Deutsch."

### Aktive
* "10 % als kritische Masse" --> siehe denkweise.md

### Vermeintliche Ohnmacht des Einzelnen
* siehe "Philosophie der Nachhaltigkeit - Es ist nicht egal, was du tust" / wirksamkeit-des-einzelnen.md

### Eier: McDonalds: teilweise Freilandeier
* https://albert-schweitzer-stiftung.de/aktuell/aktion-kaefigeier-mcdonalds
    * 2016:
        * "Wenn die Fast-Food-Kette Eier direkt bezieht, dann wählt sie [...] Freilandhaltung"
        * "ihren Lieferanten für fertige Produkte erlaubt sie, Käfigware zu verwenden."
            * versteckte Käfigeier in Saucen und Backwaren
* Werbung 2018: "100 % Eier aus Freilandhaltung beim Mc-Donalds-Frühstück" (was ist mit anderen Eiprodukten?)

### Maissilage und Mais-Monokultur
* https://de.wikipedia.org/wiki/Maissilage, 2018
    * "eine Silage aus der ganzen Maispflanze, die als Grundfutter für Wiederkäuer und als Substrat für die Biogaserzeugung eingesetzt wird"
    * "wird in der Landwirtschaft vorwiegend an Rinder verfüttert"
    * "in der Schweinehaltung hat etwa seit 1980 der Mais in Form von CCM (Corn-Cob-Mix) Einzug gehalten"
    * Zahlen: "Mehr als 90 Prozent der Biogasanlagen in Deutschland, die Nachwachsende Rohstoffe einsetzen, nutzen Maissilage als Gärsubstrat"
    * Zahlen: "An der Masse der zur Biogaserzeugung eingesetzten Nachwachsenden Rohstoffe beträgt der Maisanteil 79 Prozent"
* https://de.wikipedia.org/wiki/Mais, 2018
    * Zahlen: "Bei der Weltgetreideernte nimmt Mais mit über einer Milliarde Tonnen (2016) vor Weizen und Reis den ersten Platz ein.
        * "Über 60 Prozent davon wird, zu Maissilage verarbeitet, an Nutztiere verfüttert."
            * "Über 60 % des weltweit produzierten Maises wird verfüttert"
        * "Dazu kommt der Einsatz von Energiemais als nachwachsender Rohstoff für die Erzeugung von Bioethanol und Biogas"
    * Anbau in Deutschland
        * früher weniger als 1 % der Anbaufläche
        * "Erst in den 1970er Jahren wurden den mitteleuropäischen Standortverhältnissen angepasste Sorten entwickelt, so dass sich der Maisanbau hier stark ausweitete"
            * siehe auch Vermaisung der Landschaft
    * Gentechnisch modifizierte Sorten, siehe https://de.wikipedia.org/wiki/Transgener_Mais#%C3%96konomische_Auswirkungen
    * Zahlen: "Etwa 15 % der globalen Maisernte werden als Lebensmittel verwendet (Zeitraum: 2005–2007)"
    * ! Zahlen: "In Entwicklungsländern liegt der Anteil von Lebensmitteln an der Maisnachfrage bei 25 %, in Ost- und Südafrika bei 73 %, während er in Industrieländern 3 % beträgt." (Ernährungsrechtigkeit)
        * "In Industrieländern wird 23 % des verbrauchten Mais verarbeitet, vor allem zu Bioenergie. Die Nachfrage nach Mais als Futtermittel (vor allem für Schweine und Geflügel) steigt um 6 % jährlich, insbesondere aufgrund des zunehmenden Fleischkonsums in Asien."
        * "Für etwa 900 Millionen Menschen, vor allem in Afrika und Lateinamerika, ist Mais das wichtigste Grundnahrungsmittel." (Ernährungsrechtigkeit)
    * "wird kritisiert, dass Mais-Monokulturen (eine „Vermaisung der Landschaft“) negative Auswirkungen auf die Artenvielfalt (Biodiversität) haben."
    * "wird die Verdrängung der [...] ernährungsphysiologisch wertvolleren Hirsenahrung durch Maisnahrung kritisch gesehen"

Thema: Verschwendung
--------------------
Lebensmittelverschwendung

* todo: was empfehlen Experten hier?
    * Link zur Erasmus School of Economics: Wenn pflanzenbasierte Produkte statt Tierprodukte weggeworfen werden werden bei gleicher Wegwerfmenge bis zu 10x so viele Ressourcen geschont: https://www.eur.nl/en/ese/news/vitam-and-erasmus-school-economics-choose-100-plant-based
        * "The immediate advantage of the nudge is that, in case excess food is thrown away, it doesn’t contain any wasteful meat or dairy.”"
* Welche Rolle spielt das MHD (Mindesthaltbarkeitsdatum)?

Thema: Müllvermeidung
---------------------
### Mehrwegbecher vs. Einwegbecher
* für Kaffee (eigentlich verzichtbar, siehe dort)
* Positive Beispiele / Vorreiter:
    * ["Erste Kantine bleibt Bechern treu"](https://www.morgenweb.de/mannheimer-morgen_artikel,-mannheim-erste-kantine-bleibt-bechern-treu-_arid,1307606.html), 31. August 2018
        * "Betriebsrestaurant der Berufsgenossenschaft Nahrungsmittel und Gastgewerbe setzt auf Mehrweg / Personalrat kam im Urlaub auf die Idee dazu"
        * "Die erste Betriebskantine in der Quadratestadt bietet den „Mannheim-Becher“ an – nämlich die der Berufsgenossenschaft Nahrungsmittel und Gastgewerbe (BGN). Zusammen mit dem Kantinenbetreiber Eurest Deutschland..."
        * Allgemein Mannheim:
            * Konzept: https://www.bleibdeinembechertreu.de/
            * [Video](https://www.youtube.com/watch?v=-xnVRoKKLyo), 30 min
                * "In Mannheim landen jeden Tag 32.000 Einwegbecher im Müll. Die städtische Kampagne „Bleib deinem Becher treu!“"
            * ["Einwegbecher ade"](https://www.rnz.de/nachrichten/mannheim_artikel,-mehrwegsystem-in-mannheim-einwegbecher-ade-_arid,346396.html), 2018
* Frage: Warum kann der Einwegbecher nicht einfach 1 EUR mehr kosten? Und dann schauen, was die Magie des Preises bewirkt.
