Digital OSS
===========

<!-- toc -->

- [Was ist OSS?](#was-ist-oss)
  * [Nicht die Lösung für alle Probleme](#nicht-die-losung-fur-alle-probleme)
  * [Prävention gegen Datensilos](#pravention-gegen-datensilos)
  * [Nachhaltigkeit](#nachhaltigkeit)
  * [Datensouveränität](#datensouveranitat)
  * [Diversity](#diversity)
  * [2020 - Positives Beispiel Corona-App](#2020---positives-beispiel-corona-app)
  * [2019](#2019)
  * [A better Qt because of Open Source and KDE](#a-better-qt-because-of-open-source-and-kde)
  * [2020 - Warum OSS](#2020---warum-oss)

<!-- tocstop -->

Was ist OSS?
------------
### Nicht die Lösung für alle Probleme
* Wichtiger Baustein für IT-Sicherheit und Demokratie
* Beispiel Heartbleed
    * Aber für IT-Sicherheit müssen auch andere Aspekte beachtet werden, z. B. Software-Vielfalt (siehe BSI)
        * Dies war bei Heartbleed nicht erfüllt und kommt auch bei nicht OSS vor (z. B. MS Word)
* Aber aber F-Droid und OSS wird ja nur von so wenigen Menschen verwendet
    * Erstens kann das auch am Angebot liegen.
    * Aber die Leute fragen das ja gar nicht nach.
        * Vielleicht liegt das daran, dass die Experten, die das Wissen haben, nicht genug Aufklärungsarbeit leisten
    * Analogie Meinungsfreiheit
        * wichtig für Demokratie, siehe dort
        * Die wenigsten Menschen machen von ihrer Meinungsfreiheit so Gebrauch, dass die Äußerungen tatsächlich aktiv geschützt werden müssen.
        * Das heißt aber nicht, dass man ohne Meinungsfreiheit und die Menschen, die durch sie geschützt werden, besser dran wäre.
        * "Zu argumentieren, dass Sie keine Privatsphäre brauchen, weil Sie nichts zu verbergen haben, ..."
        * analog Meinungsfreiheit ist Privatsphäre, die durch die Eigenschaften von OSS effektiv erreicht werden kann, ein Baustein der Demokratie
    * Wahlfreiheit, Freiheit der Wahl (seine IT auch ohne Datensilos zu gestalten), Wahlmöglichkeit, ::freiheit

### Prävention gegen Datensilos
* siehe BSI-Strategie: FOSS und Software-Vielfalt
* siehe digitalcourage Digitale Selbstverteidigung
* Einfacher Test, ob Angebot auf einer privatsphären-freundlichen Plattform abläuft
    * kann ich den Serveranbieter wechseln (z. B. den Store-Anbieter, wo die App zu finden ist)?
        * also z. B. zu einem Nicht-Silo-Anbieter

### Nachhaltigkeit
* steht für Nachhaltigkeit

### Datensouveränität
* Leute können frei entscheiden, wie mit ihren Meta-Daten verfahren wird
* siehe auch "Digitale Souveränität"
* 2020: "Ansonsten ist man sich über die "Microsoft-Abhängigkeit" und dem Thema
    "Datensouveränität" bewusst. Es ändert nur nichts an den Entscheidungen"
    * heise: Marktanalyse: Microsoft-Abhängigkeit führt zu "Schmerzpunkten" beim Bund (19.09.2019)
    https://www.heise.de/newsticker/meldung/Marktanalyse-Microsoft-Abhaengigkeit-fuehrt-zu-Schmerzpunkten-beim-Bund-4533951.html
    * CCI: Digitale Souveränität - Die Bundesverwaltung ist abhängig von Microsoft (04.10.2019)
  https://www.cloudcomputing-insider.de/die-bundesverwaltung-ist-abhaengig-von-microsoft-a-867577/
    * PwC: Strategy "Strategische Marktanalyse zur Reduzierung von Abhängigkeiten von einzelnen Software-Anbietern" (August 2019)
  https://www.cio.bund.de/SharedDocs/Publikationen/DE/Aktuelles/20190919_strategische_marktanalyse.pdf?__blob=publicationFile

### Diversity
* steht für Diversity (Leute, die einfach nur in der Region zzz machen wollen, aber deswegen nicht gleich alle Daten zu Google nach Amerika schicken wollen)

### 2020 - Positives Beispiel Corona-App
* SAP und Telekom gehen auf github => Vertrauen
* Daten werden lokal auf dem Gerät gespeichert, statt zentral

### 2019
* "Warum Open Source so erfolgreich ist", 2019, https://www.sueddeutsche.de/digital/open-source-linux-microsoft-google-erfolg-1.4508159
    * ...
* "Lehre aus dem Fall Huawei: Die Wiederentdeckung der freien Software", 2019, https://www.faz.net/aktuell/wirtschaft/diginomics/lehre-aus-dem-fall-huawei-die-wiederentdeckung-der-freien-software-16198584.html
* **Wahlmöglichkeit** bei Überwachung ja/nein
    * "Exklusivinterview: Snowden warnt vor Massenüberwachung" - https://www.youtube.com/watch?v=fjt0U4-30ao, 2019
        * ...
        * zu Google, Amazon, Facebook: Leute klicken "AGBs akzeptieren" und wissen nicht, was ihr Telefon eigentlich macht
            * Wenn die Benutzer die Wahl hätten, einen Knopf zu drücken, der das Spionieren abschaltet, würden sie es tun
            * Ziel sollte es sein, die Menschen mit so einer Wahlmöglichkeit auszustatten (nicht nur "nimm alles inkl. Überwachung oder nichts")

### A better Qt because of Open Source and KDE
* http://www.olafsw.de/a-better-qt-because-of-open-source-and-kde/

### 2020 - Warum OSS
* Dreiklang: Gesellschaft - Individuum (inkl. Kunde) - Unternehmen
    * ...
* Which thinks do you find important which money can't buy?
    * Kreativität, Vertrauen
    * true commitment
    * real love for a product / process
* Wie können möglicherweise viele gewinnen, ohne dass andere verlieren müssen?
