F-Droid-Projekt
===============

<!-- toc -->

- [Was ist F-Droid](#was-ist-f-droid)
- [Zielgruppe](#zielgruppe)
  * [Wer?](#wer)
  * [Wie groß?](#wie-gross)
- [Angewandte Forschung: SECUSO am KIT](#angewandte-forschung-secuso-am-kit)
  * [Security - Usability - Society](#security---usability---society)
  * [Sensibilisierungsforschung: Wie erreichen Sie die Kolleginnen und Kollegen?](#sensibilisierungsforschung-wie-erreichen-sie-die-kolleginnen-und-kollegen)
  * [Privacy Friendly Apps](#privacy-friendly-apps)
  * [Beispiel: Privacy Friendly Aktive Pause](#beispiel-privacy-friendly-aktive-pause)
  * [SECUSO für den Bürger / für KMU](#secuso-fur-den-burger--fur-kmu)
- [Beispiele von externen F-Droid-Repositories](#beispiele-von-externen-f-droid-repositories)
  * [Tagesschau-App](#tagesschau-app)
  * [c:geo](#cgeo)
  * [RetroArch](#retroarch)
  * [Wiki: Known Repositories](#wiki-known-repositories)
- [Das Privacy Paradox - Digitalisierung versus Privatsphäre](#das-privacy-paradox---digitalisierung-versus-privatsphare)
  * [iwkoeln](#iwkoeln)
  * [Prof. Dr. Melanie Volkamer etc.](#prof-dr-melanie-volkamer-etc)
- [Verschiedenes](#verschiedenes)
  * [App für F-Droid bereitstellen](#app-fur-f-droid-bereitstellen)
  * [Accessibility](#accessibility)
  * [Privacy vs. social connection](#privacy-vs-social-connection)

<!-- tocstop -->

Was ist F-Droid
---------------
* [F-Droid: Verbraucherfreundlicher Store für Android-Apps](https://mobilsicher.de/hintergrund/verbraucherfreundlich-f-droid), mobilsicher.de, 2016

Zielgruppe
----------
### Wer?
* ...

### Wie groß?
* Direkte potentielle Nutzer - LineageOS-User
    * https://www.lineageoslog.com/statistics
        * Deutschland ist mit 67598 Installationen auf Platz 6 weltweit

* Nutzer, die auf Ansprache positiv reagieren
    * iwkoeln
        * "Lediglich knapp ein Fünftel der Deutschen vertraut der Internetwirtschaft (Eurobarometer, 2015, 66)."
        * "Nur drei Prozent der deutschen Internetnutzer ist es egal, was mit ihren Daten im Internet geschieht (Bitkom, 2015)"
        * "In Deutschland nutzen 87 Prozent der Verbraucher die Dienste, obwohl sie kein volles Vertrauen in deren Datenschutz haben (Bitkom, 2015)"
        * "Tsai et al. (2011) zeigen, dass Verbraucher bereit sind, einen um 5 Prozent höheren Preis zu bezahlen, wenn ihnen offensichtliche Datenschutzvorteile angeboten werden."

Angewandte Forschung: SECUSO am KIT
-----------------------------------
### Security - Usability - Society
https://secuso.aifb.kit.edu/, Prof. Dr. Melanie Volkamer

"SECUSO ist Mitglied bei KASTEL External Link - dem Kompetenzzentrum für angewandte Sicherheitstechnologie - eines von deutschlandweit drei Kompetenzzentren für Cybersicherheit, die vom Bundesministerium für Bildung und Forschung (BMBF External Link) im März 2011 initiiert wurden."

### Sensibilisierungsforschung: Wie erreichen Sie die Kolleginnen und Kollegen?
"Melanie Volkamer spricht bei der Fachtagung "Informationssicherheit" (23-05-2019)" - https://vp.spk-akademie.de/vp/action?securedGetRequest=l1z44NQcnR0Oe_mLK9S9zkN4cZ80eoz9SpI6NAvuRrc

### Privacy Friendly Apps
https://secuso.aifb.kit.edu/105.php

### Beispiel: Privacy Friendly Aktive Pause
https://secuso.aifb.kit.edu/630.php

"Keine störende Werbug
Viele andere kostenlose Apps im Google Play Store blenden störende Werbung ein, die unter anderem auch die Akkulaufzeit verkürzt und Datenvolumen verbrauchen kann."

"Poster mit allen Übungen fürs Büro"

### SECUSO für den Bürger / für KMU
https://secuso.aifb.kit.edu/642.php

Beispiele von externen F-Droid-Repositories
-------------------------------------------
### Tagesschau-App
* 2018: https://f-droid.org/en/2018/10/20/twif-26-tagesschau-f-droid-repo-and-translation-workflow-improvement.html
    * "We recently learned that Tagesschau distributes their official app through their own F-Droid repository at https://service.tagesschau.de/app/repo."
    * "We see this move as a great step towards user rights and freedom. Sadly the app isn’t free and open source software, although it is financed with public money. If it is public money, it should be public code as well."

### c:geo
* ...

### RetroArch
https://retroarch.com/?page=platforms

### Wiki: Known Repositories
https://forum.f-droid.org/t/known-repositories/721

Das Privacy Paradox - Digitalisierung versus Privatsphäre
---------------------------------------------------------
### iwkoeln
https://www.iwkoeln.de/studien/iw-kurzberichte/beitrag/barbara-engels-mara-grunewald-das-privacy-paradox-digitalisierung-versus-privatsphaere-356747.html

* Zahlen, siehe potentielle Nutzerzahlen
    * "Lediglich knapp ein Fünftel der Deutschen vertraut der Internetwirtschaft (Eurobarometer, 2015, 66)."
    * "Nur drei Prozent der deutschen Internetnutzer ist es egal, was mit ihren Daten im Internet geschieht (Bitkom, 2015)."

* Rationalität
    * "[...] Dieser Nutzen ist unmittelbarer als die Kosten, die oft erst ex post wahrgenommen werden (Acquisti, 2004). Auch könnten die Informationen, die den Verbrauchern gerade zur Verfügung stehen, eher für die Entscheidung genutzt werden als Informationen, die die Verbraucher erst suchen müssen (Verfügbarkeitsheuristik)."

* Kontextabhängigkeit
    * "Laut Nissenbaum (2009) hängt die Wahrnehmung einer Privatsphärenverletzung stark vom Kontext ab. Je nach sozialem Umfeld wird Privatsphäre unterschiedlich wahrgenommen – in der Onlinewelt mischen sich allerdings die sozialen Zirkel, weshalb es zum Paradox kommen kann (Blank/Bolsover/Dubois, 2014). Acquisti et al. (2015) zeigen, dass Verbraucher eher Informationen preisgeben, wenn sie beobachten, dass Andere dies auch tun. Auch aufgrund der Netzwerkeffekte spielt der Kontext eine große Rolle: Nutzt das soziale Umfeld eines Verbrauchers einen datenschutzfreundlichen Messenger, ist die Wahrscheinlichkeit hoch, dass auch der Verbraucher sich für mehr Datenschutz entscheidet. Im Gegenzug wird sich der Verbraucher nicht alleine für einen datenschutzfreundlichen Messenger entscheiden."

* Formbarkeit von Präferenzen
    * "Unternehmen können mittels subtiler Maßnahmen die Datenschutzpräferenzen der Verbraucher ändern. So können etwa Designeinstellungen dazu verleiten, mehr Daten preiszugeben, um schneller zum Ziel zu kommen. Eine technische Voreinstellung (Default) eines Onlinedienstes beizubehalten ist bequem, wird oft als Empfehlung angesehen und dementsprechend übernommen, auch wenn der Datenschutzstandard gering ist."

### Prof. Dr. Melanie Volkamer etc.
* "Das Privacy-Paradoxon – Ein Erklärungsversuch und Handlungsempfehlungen" - https://link.springer.com/chapter/10.1007/978-3-658-16835-3_8

Verschiedenes
-------------
### App für F-Droid bereitstellen
* Beispiel: so einfach kommt eine App in F-Droid
    * "Cat Avatar Generator is now available on F-Droid", https://github.com/agateau/cat-avatar-generator-app/blob/master/CHANGELOG.md

### Accessibility
https://de.wikipedia.org/wiki/Behinderung#Anzahl_der_Menschen_mit_Behinderung

### Privacy vs. social connection
* https://www.psychologytoday.com/us/articles/200711/the-privacy-paradox, 2016
