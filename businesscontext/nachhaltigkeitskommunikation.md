Nachhaltigkeitskommunikation
============================

<!-- toc -->

- [Artikel](#artikel)
  * [Climate Change; dringend; Human attitudes, Sebastian Copeland](#climate-change-dringend-human-attitudes-sebastian-copeland)
  * ["Strategien gegen die Desinformation zum Klimawandel"](#strategien-gegen-die-desinformation-zum-klimawandel)
  * [Empörung oder Empathie](#emporung-oder-empathie)
  * [Vertrauen wichtiger als Information](#vertrauen-wichtiger-als-information)
  * [Strategien von Desinformationskampagnen offenlegen](#strategien-von-desinformationskampagnen-offenlegen)
  * [Nicht in Talkshows, George Marshall, Förderquoten](#nicht-in-talkshows-george-marshall-forderquoten)
  * [Verschiedenes](#verschiedenes)
- [Entwurf 1 - theoretisch untermauert?](#entwurf-1---theoretisch-untermauert)

<!-- tocstop -->

::Umweltkommunikation, siehe Umweltpsychologie

Artikel
-------
### Climate Change; dringend; Human attitudes, Sebastian Copeland
* siehe dort

### "Strategien gegen die Desinformation zum Klimawandel"
* https://www.transforming-cities.de/strategien-gegen-die-desinformation-zum-klimawandel/, 2019
    * "Interview mit Lance Bennett, Professor für Politikwissenschaft und Kommunikation an der University of Washington, derzeit Senior Fellow am IASS"
    * TODO
    * ...
    * "Es ist an der Zeit, stärkere Kommunikationsnetzwerke zu entwickeln.
        Deren Schwerpunkt sollte auf Wirtschaftssystemen liegen, die in nachhaltigere Gesellschaften investieren und Konsum und Abfall vorbildlich managen.
        Anders als die nationalistische Rechte haben Nachhaltigkeitsakteure mächtiges Wissen auf ihrer Seite.
        Es besteht aber ein erheblicher Mangel an Koordination zwischen Sektoren, Thinktanks, Forschungsinstituten, Geldgebern und politischen Parteien.
        Das Resultat ist eine schlechte Kommunikation, die all dieses wissenschaftliche Wissen wirkungslos macht."
    * "Die Aufgabe der Wissenschaftler besteht aber darin, gute Informationen zu liefern und nicht, politische Strategien zu entwickeln.
        Die wissenschaftlichen Erkenntnisse mit den Hauptursachen der Nachhaltigkeitsprobleme in Zusammenhang zu setzen,
        ist die Aufgabe von NGOs, Geldgebern, Aktivisten und politischen Entscheidungsträgern.
        Sie müssen klarere Visionen entwickeln, wie die Menschen in der Zukunft arbeiten und in Wohlstand leben können."

### Empörung oder Empathie
* https://www.iass-potsdam.de/de/blog/2019/10/empoerung-oder-empathie-kampf-der-kulturen-der-klimakommunikation
    * George Marshall, Climate Outreach
    * "Als erfolgreichste Erzählstrategie erwies sich die Anerkennung der natürlichen Erdöl- und Erdgasressourcen als Grundlage für den Wohlstand der Region, während gleichzeitig Sonne, Wind und Wasser als weitere natürliche Ressourcen dargestellt wurden, die der Region eine blühende Zukunft sichern und sie vor der Instabilität der internationalen Ölmärkte schützen könnten. Die Ausbeutung natürlicher Ressourcen als positive klimapolitische Botschaft zu betrachten, ist kontraproduktiv zu den Botschaften von uns in der "grünen" Blase, aber es kann funktionieren."
    * "Empathie und Verständnis als Ausgangspunkt für unsere Klimakommunikation scheint mir viel lohnender als Überzeugungsarbeit, bei der wir anderen Menschen klarmachen müssen, dass wir recht (und sie unrecht) haben. Ich erkenne in unserer heutigen Gesellschaft einen besorgniserregenden Mangel an Solidarität."
    * "Wenn wir uns bemühen, Menschen mit stark von unseren eigenen abweichenden Meinungen und Ideologien zu verstehen, kann dies zumindest einen Teil der gesellschaftlichen Gräben schließen. Gleichzeitig gebe ich zu, dass schon echte Empathie an sich harte Arbeit ist."
    * Greta Thunberg
        * "dass sich hinter Greta Thunbergs Ärger der grundsätzlich optimistische Glaube verbirgt, dass sich der Lauf der Dinge noch umkehren lässt.
        Damit steht sie im Gegensatz zu den globalen Entscheidungsträgerinnen und -trägern (und vielleicht zu den meisten Erwachsenen auf dieser Welt), die in der Tat so handeln, als hätten sie bereits aufgegeben – auch wenn sie sich das noch nicht eingestehen wollen."
    * "Sollten wir in der Klimakommunikation Empörung oder aber Verständnis fördern? Ich habe lange mit dieser Frage gerungen. Meine vorläufige Antwort ist, dass die beiden Ansätze einander nicht unbedingt widersprechen."
    * "großflächige Unterstützung gewinnen"
    * "Wir müssen fähig sein, unsere Differenzen zurückzustellen, und wirksam kommunizieren, warum die Beschränkung des Klimawandels tatsächlich dem Gemeinwohl dient. Dabei müssen wir die Werte der Menschen ansprechen, die wir erreichen wollen."

* Mehr:
    * https://www.iass-potsdam.de - "Transformative Nachhaltigkeitsforschung"
    * https://www.klimafakten.de/tags/george-marshall

### Vertrauen wichtiger als Information
* https://www.klimafakten.de/meldung/kommunikationsstrategien-vertrauen-ist-wichtiger-als-informationen, 2016
    * "Um mehr Verbündete zu gewinnen, müssten Klimaschützer ihre Kommunikationsstrategien ändern, empfehlen etliche Psychologen"

### Strategien von Desinformationskampagnen offenlegen
* https://www.klimafakten.de/meldung/der-kampf-gegen-den-klimawandel-braucht-eine-wende-der-kommunikation, 2019
    * "Statt nur die Folgen des Klimawandels zu dokumentieren oder in fiktionaler Form zu dramatisieren, sollten [in den Medien] mit Empathie die Reaktionen der betroffenen Menschen vor Ort ins Bild gerückt werden."
    * K3-Kongress
    * "die Strategien von Desinformationskampagnen offenlegen"
        * Finnland: "Das Land habe auf die Bedrohung durch Desinformationskampangen "mit einem nationalen 'Critical-Thinking-Curriculum' reagiert"
        * "Fakten, die Schülern auf Youtube und in den sozialen Netzwerken begegnen, werden im Schulunterricht durchleuchtet, Clickbaiting und Emotionalisierungsstrategien werden durchschaubar gemacht."
        * https://www.klimafakten.de/meldung/desinformations-kampagnen-kontern-mit-einer-schutzimpfung, 2017
            * "Attacken auf den Forscherkonsens zum Klimawandel sind besonders effektiv"
            * "Für die Praxis formulierten die Autoren denn auch einen klaren Rat:
                "Wann immer möglich, sollte die Informationen über den Forscherkonsens zum Klimawandel begleitet werden von einer Warnung, dass politisch oder ökonomisch motivierte Akteure die Befunde der Wissenschaft zu unterminieren suchen. Zusätzlich sollte der Öffentlichkeit in Grundzügen erklärt werden, wie solche Desinformations-Kampagnen funktionieren, um diese präventiv zu widerlegen."

### Nicht in Talkshows, George Marshall, Förderquoten
* https://www.klimafakten.de/meldung/warum-unser-gehirn-darauf-programmiert-ist-den-klimawandel-zu-ignorieren
    * Buch: "Don't Even Think About It", 2014
    * ...
    * "Das fängt bei den Worten an. Viele wissenschaftliche Termini werden von der Öffentlichkeit nicht oder falsch verstanden. Unter "Unsicherheit" verstehen Forscher beispielsweise ganz normale Limitierungen des Wissens – die Öffentlichkeit aber hört, dass man eigentlich noch gar nichts weiß."
        * "Ein großer Sieg der Marktradikalen etwa sei, dass mittlerweile das Wort "Steuererleichterung" das Wort "Steuersenkung" weitgehend ersetzt habe – denn der neue Terminus transportiere sehr wirksam die Ansicht, Steuern seien eine (ungerechte) Belastung und nicht das (gerechte) Abtreten eines Gewinnanteils an die Allgemeinheit."
    * "Problematisch sei auch, dass das Klimathema so stark mit der Umweltbewegung verknüpft ist. Das sei zwar historisch erklärbar, habe aber zu einer bestimmten (Bild)Sprache geführt, die weitere Zielgruppen (Konservative, Unternehmer, Gewerkschafter, Menschenrechtler etc.) eher abschrecke."
    * ...
    * ! "Dabei wäre es doch zum Beispiel viel einfacher, Kohlendioxid an der Quelle anzugehen – also bei der Förderung fossiler Energieträger. Man könnte etwa über Produktionslimits für die Kohle- oder Ölförderer nachdenken"
        * analog Fischfangquoten
    * Tipps
        * Sprache
        * Neue Botschafter
            * "Man kann eine breite Öffentlichkeit und auch skeptische Zuhörer besser erreichen, wenn beispielsweise Feuerwehrleute über Waldbrände erzählen oder Militärs über sicherheitspolitischen Aspekte des Klimawandels."
        * "Klimaforschern rät Marshall übrigens explizit, NICHT IN TALKSHOWS mit Wissenschaftsleugnern zu debattieren. In diese m Gesprächsformat und der dort vorgegebenen Rededauer könnten sie nur verlieren."
        * Geschichten
        * Individuelle Appelle meiden
        * Von Kirchen lernen
            * "man solle sich bei Kirchen zum Beispiel abschauen, wie sie Menschen zu einer Gemeinschaft zusammenbringen oder wie sie Schuld in konstruktive Gefühle umwandeln"
        * Raum fürs Trauern
            * "für den emotionalen Abschied vom fossilen Zeitalter, das ja liebgewonnene Vorzüge hatte.
            * "Die klimaschonennde Welt wird neue Freuden bereithalten", so Marshall, "aber halt nicht mehr das süße Röhren eines Ford Mustang V8.""
    * TODO!
    * ...
    * ...

### Verschiedenes
* https://www.transforming-cities.de/neue-ideen-zur-klimakommunikation/, 2019
    * "Als zentrale Handlungsfelder für eine zielführende Klimakommunikation, die den Menschen Orientierung auf dem Weg in die klimaneutrale Zukunft gibt, wurden identifiziert:"
        * "ehrliche Auseinandersetzung über die Sorgen und Ängste, die der Klimawandel auslöst"
        * "Intensivere Debatten in der Zivilgesellschaft über Lebensstile und Werte"
        * "Verstärkte Kooperation zwischen allen AkteurInnen, die dazu beitragen können, den Klimawandel zu stoppen"
        * "Mehr aktiver Transfer von Lösungsansätzen aus der Wissenschaft in die Praxis"
            * -> wissenschaftl. Empfehlungen zur Gemeinschaftsverpflegung
    * "Wissenschaft, Politik, Wirtschaft, Kultur und Zivilgesellschaft müssen mit vereinten Kräften das Ruder herumreißen. Klimaschutz eröffnet neue Möglichkeiten, die unseren Wohlstand langfristig sichern."

* https://www.volksverpetzer.de/social-media/kettenbrief-klimadebatte/, 2019
    * Wissenschaftsleugner: "Wenn das Klima der Straßenverkehr wäre", ::analogie
    * Blogger des Jahres Volksverpetzer.de
        * mit Team-Bild: https://www.pr-agent.media/news/2020/deutschlands-blog-des-jahres-heisst-volksverpetzer-de/12592
            * "Volksverpetzer recherchiert Fake News nach und gibt die Ergebnisse seiner Arbeit in der gleichen plakativen Art wider,
                wie extremistische Accounts und rechtsradikale Nachrichtenangebote. „Damit setzt das ehrenamtliche
                Team einen bewussten Kontrapunkt zu jenen, die das Klima im Netz vergiften“, urteilte die Jury."
        * ebenso: https://www.volksverpetzer.de/schwer-verpetzt/blogger-des-jahres/
        * WDR-Video, 5 min, mit Interview: https://www.ardmediathek.de/swr/video/landesschau-rheinland-pfalz/volksverpetzer-ist-blog-des-jahres-2019/swr-rheinland-pfalz/Y3JpZDovL3N3ci5kZS9hZXgvbzEyMjA0MTc/
            * u. a. die Frage, warum eigentlich das negativ klingende "Verpetzer" gewählt wurde
            * ...
            * möchte Demokratie stärken

Entwurf 1 - theoretisch untermauert?
------------------------------------
Umweltgerechte Kommunikation.

Hallo, deine Zukunft sieht scheiße aus.

x80.000.000-Betrachtung
x7.000.000.000-Betrachtung

Wenn jeder Mensch auf der Welt so viel Kaffee trinken würde, wie wir hier durchschnittlich, dann...
... bräuchte man x % mehr Anbaufläche, x % mehr Wasser und x % mehr CO2 für Transport...

Wenn jeder Mensch auf der Welt diesen neuen Mehrweg-Becher kaufen wollte, dann...

Anzeige Reise nach xyz. Wenn jeder Mensch auf der Welt diese Reise (100 km) mit der Bahn unternehmen wollte, dann...

Wenn jeder Mensch in D. für Fern- und Nahverkehr vom Auto auf die Bahn umsteigen würde,
- würde das x % CO2 sparen (von absolut x für Auto auf absolut y für Bahn)
- bräuchte man x mal soviele Züge

Werbeschilder:

Wenn jeder Mensch auf der Welt dieses Auto kaufen würde, dann...

Wenn jeder Mensch auf der Welt diesen Burger kaufen wollte, dann...
- braucht es statt x Tiere, y Tiere
- ...
