Kultur und Coaching
===================

<!-- toc -->

- [Retro: Was ist Nachhaltigkeit? / 3 Kreise](#retro-was-ist-nachhaltigkeit--3-kreise)
- [Value-to-Action-Coaching](#value-to-action-coaching)
- [Team Nachhaltigkeit](#team-nachhaltigkeit)
  * [Der Mensch im Mittelpunkt / sozialer Aspekt](#der-mensch-im-mittelpunkt--sozialer-aspekt)
  * [Destruction, Sustainability, Regeneration; Bedürfnisse](#destruction-sustainability-regeneration-bedurfnisse)
  * [Ungeahnte Geschmacks- und Gesundheitswelten](#ungeahnte-geschmacks--und-gesundheitswelten)

<!-- tocstop -->

Retro: Was ist Nachhaltigkeit? / 3 Kreise
-----------------------------------------
(sustain)
* "Wenn ich an Nachhaltigkeit denke... / Das finde ich schade... / Das könnte besser laufen... / Das wäre optimal...
    * Wie wir denken: Vom 3-Säulen-Modell zum 3-Kreise-Modell: https://de.wikipedia.org/wiki/%C3%96konomische_Nachhaltigkeit
        * "Schrei-These":
            * derzeit: Wirtschaft schreit am lautesten, weil Geld bei uns ein hoher Wert ist
                * danach das Soziale, weil direkt greifbar, aber auch schon problematisch, wenn es um Menschen geht, wo Ungerechtigkeit nicht gesehen wird
                * danach kommt die Natur und die Tiere, die keine eigene Stimme haben
                * Ds heißt, zudem, dass die Ökologie unsere Grundlage ist, hat sie auch noch das geringste Schreipotential; bzw. erst dann, wenn es schon sehr spät ist
    * Träumen zulassen
    * z. B. https://ideaboardz.com/for/Sustain/3132593
        * ...in meinem Team
        * ...in meiner Einheit
        * ...in meinem erweiterten Umfeld
        * ...bei mir selber
        * ...im Lauf eines typischen Arbeitstages
        * ...im Unternehmen
        * ...im Konzern
        * ...in der Welt drumrum (über den Tellerrand) / wo gibt es Beispiele, wo jemand soetwas (ansatzweise) schon praktiziert?

Value-to-Action-Coaching
------------------------
* Value-to-Action-Coaching - Was ist das? (2019)
    * Nachhaltigkeit ist eines der Core-Werte der meisten Menschen.
    * Siehe z. B. Umfrage zu Nachhaltigkeit bei Reisen: immer mehr Menschen sehen Nachhaltigkeit als immer wichtiger an = Wert
        Gleichzeitig steigt die Anzahl der Fernflugreisen = Handlung
    * Ziel des Value-to-Action-Coaching ist es Menschen zu begleiten zu dem gewünschten Lebenskontext (Reisen, Mobilität, Ernährung, Selbstentwicklung etc.)
        die eigenen Werte herauszuarbeiten, diese mit den tatsächlichen Handlungen abzugleichen
        um danach Wege zu beschreiten Werte und Handlungen in Einklang zu bringen.

* "14-points of culture" - https://www.actioncoach.com/about/our-culture/


Team Nachhaltigkeit
-------------------
Alternative Titel:

- FastTrack Nachhaltigkeit
- Einfach Nachhaltig
- Nachhaltigkeit Thinking

Was können wir für dich und/oder dein Team tun:

- Vernetzung

    - ein starker Fokus liegt auf Kommunikation und Querverbindung von bereits vorhandenen Ideen und Denkansätzen, um möglichst effektiv zum Ziel zu kommen

- Analyse Nachhaltigkeitssituation im Büroalltag (inkl. Aufzeigen von Wegen)

    - Einwertung in ein Nachhaltigkeitsrating

- Impuls-Vorträge / -Workshops zum Nachhaltigkeitsdenken im Unternehmen

   - z. B. im Rahmen von Einführungsveranstaltungen für neue Mitarbeiter:innen)

- Initiierung / Begleitung von **Transformationen hin zu regenerativer Nachhaltigkeit**


Service:

- Tabelle Mülltrennung: welche Gebäude- / Büroanbieter können was? Ansprechpartner?

### Der Mensch im Mittelpunkt / sozialer Aspekt
* Der Mensch muss im Mittelpunkt stehen. Warum?
    * Weil die Grundbedürfnisse des Einzelnen und von Gruppen erfüllt sein müssen, um den Blick für das erweiterte Umfeld frei zu haben
    * Mitbestimmung (Demokratie) und Frieden sind Grundvoraussetzungen
    * Der Mensch ist (in Summe; individuell zu sehr unterschiedlichen Graden) Ursache der Probleme
    * Im Menschen und in seiner Kooperationsfähigkeit liegen aber auch die Lösungen

* Sozialer Aspekt
    * Beispiel Essen/Fleisch -> Frage an Akademiker
        * Welches Fleisch ist ethisch korrekt? (... mit Fragen einsteigen zu arbeiten...)
            Wieviel würde das wohl mehr kosten? / bzw. wer trägt die Kosten? (...) Können sich das auch Ärmere leisten?
            (...) Ist es eigentlich notwendig? (...) Wird es als Luxus betrachtet?
            (...) Sollte man sich diesen Luxus auf Kosten anderer leisten?
            (...) Ein möglicher Bereich, wo man bewusst neue Wege im Denken und Handeln gehen könnte
    * Die Frage ist: was braucht der Mensch? / Die Luxus-Frage
        * Der Familie einen Strandurlaub bieten?
            * Die Frage ist: wo und wie oft? Passt das ins Umweltverbrauchs-Budget?

* Die Schönheit der Natur wertschätzen, ::natur
    * https://www.youtube.com/watch?v=2AYOViszK_A&feature=youtu.be&t=880 - "VEGAN 2019 - The Film"
        * ...
        * ab 14:35 min
            * vegan permaculture / ::permakultur
                * https://veganpermaculture.org/
                    * https://veganpermaculture.org/category/prepping/
                        * https://veganpermaculture.org/opinions-debate/how-permaculture-can-save-humanity-and-earth-but-not-civilisation/
                            * siehe dort
            * ...
            * vast cathetral of biodiversity
            * ...
            * love nature and all its creatures
        * 18 min: Kanada streicht Milch- und Milchprodukte fast komplett aus der offiziellen Ernährungsempfehlung, ::gesundheit
            * warum? Weil mehr auf die Wissenschaft gehört wurde
                * siehe auch Rittenau "Kanada streicht Milch NICHT aus der Ernährungspyramide (mit Der Artgenosse)", https://www.youtube.com/watch?v=oI1ScwndS5Q, 2019, 10 min
                    * richtig argumentieren
                    * gute faktenbasierte Einordnung
                    * nicht gestrichen, aber in die Protein-Kategorie verschoben
            * "Health Canada’s 2019 food guide sees some major changes from the 2017 version, with dairy almost scrapped entirely and a bigger focus on plant-based food"
                * https://www.livekindly.co/canada-2019-nutrition-guide-removes-dairy-more-vegan-food/
            * https://food-guide.canada.ca/en/
                * https://food-guide.canada.ca/en/healthy-food-choices/
                    * "Eat plenty of vegetables and fruits, whole grain foods and protein foods. Choose protein foods that come from plants more often."
                    * "Choose foods with healthy fats instead of saturated fat"
                    * "Limit highly processed foods."
                    * "Make water your drink of choice"
                    * "Be aware that food marketing can influence your choices"
                        * https://food-guide.canada.ca/en/healthy-eating-recommendations/marketing-can-influence-your-food-choices/
                            * sehr ausführlich
            * https://albert-schweitzer-stiftung.de/aktuell/kanada-empfiehlt-vegane-ernaehrung, Feb. 2019
                * "Fleisch- und Milchprodukte werden zur Proteingruppe gezählt, aber nicht explizit empfohlen."
                * "Stattdessen heißt es: »Wählen Sie häufiger pflanzliche Proteinquellen« wie Nüsse, Samen, Bohnen, Linsen und Sojaprodukte."
                * "Tipps auf der Website des Guides unterstützen dabei, mehr pflanzliche Proteine in den Speiseplan einzubauen."
                * "Das Getränk der Wahl soll Wasser sein."
                * "Da die Empfehlungen in der Vergangenheit dafür kritisiert wurden, sich zu sehr nach den Wünschen der Fleisch- und Milchindustrie zu richten, achteten die Verantwortlichen jetzt besonders darauf, aktuelle und unabhängige Studienergebnisse aus der Ernährungsforschung einfließen zu lassen."
                * "Auch die BürgerInnen nehmen sich die Empfehlungen zu Herzen: In Teilen Kanadas konnte nach der Veröffentlichung des Guides die gestiegene Nachfrage nach Tofu nicht mehr bedient werden."
                * "In Deutschland erarbeitet die Deutschen Gesellschaft für Ernährung (DGE) die offiziellen Ernährungsempfehlungen. Sie rät in ihrem »Ernährungskreis« zwar dazu, pflanzliche Lebensmittel zu bevorzugen. Jedoch empfiehlt sie auch, täglich Milch und Milchprodukte zu konsumieren. Fisch soll ein- bis zweimal pro Woche auf dem Speiseplan stehen."
            * https://www.vegan.at/inhalt/kanada-pionier-der-pflanzenbasierten-ernaehrung - "Kanadische Ernährungsgesellschaft: Vegane Ernährung für alle Lebensphasen"
                * "Im Gegensatz zur Österreichischen [und Deutschen] Gesellschaft für Ernährung stehen die Dietitians of Canada, die Interessensvertretung von 6.000 kanadischen Diätolog_innen [Ernährungswissenschaftler:innen], der veganen Ernährung positiv gegenüber."
                * "So ist laut ihnen eine gut geplante vegane Ernährung für alle Lebensphasen [inkl. Schwangerschaft und stillende Mütter] geeignet. Veganer_innen werden mit den Healthy Eating Guidelines for Vegans unterstützt."
                * https://www.unlockfood.ca/en/Articles/Vegetarian-and-Vegan-Diets/What-You-Need-to-Know-About-Following-a-Vegan-Eati.aspx
                    * "A well-planned vegan eating pattern is healthy - Anyone can follow a vegan diet – from children to teens to older adults. It’s even healthy for pregnant or nursing mothers. A well-planned vegan diet is high in fibre, vitamins and antioxidants. Plus, it’s low in saturated fat and cholesterol. This healthy combination helps protect against chronic diseases."
                    * Praktische Hilfe
                        * "Getting enough essential nutrients as a vegan [...] This chart shows some of the nutrients and common food sources that require planning if you follow a vegan diet."
                        * "Vegan on-the-go": Tipps fürs Auswärtsessen
                    * "Bottom line: Whether you make the choice to become full-time vegan or simply choose to eat meat-free meals more often, vegan food is nutritious and delicious."
            * Die folgenden Empfehlungen zu B12 und Kinderernährung der kanadischen Ernährungsberatervereinigung enthalten einiges an tierischen Produkten; es werden aber auch ausreichend pflanzliche Alternativen genannt:
                * Explizite Empfehlungen zu B12: https://www.unlockfood.ca/en/Articles/Vitamins-and-Minerals/What-You-Need-to-Know-About-Vitamin-B12.aspx
                * Kinderernährung: https://www.unlockfood.ca/en/Articles/Childrens-Nutrition/Healthy-Eating-and-Healthy-Weights.aspx
                    * Calcium and Kids: https://www.unlockfood.ca/en/Articles/Bone-Health/Calcium-and-Kids.aspx
        * Carnivore movement
        * David Attenborough on sixth mass extinction
        * Greta Thunberg
            * "even climate scientists and green politicians fly around the world and eat meat and dairy"
            * 24:00 Schwarzenegger talks about action
        * 25:45 Games Changers film
            * Prominente, die öffentlich sagen, dass sie (schon länger) vegan sind; inkl. Sportler (inkl. Bodybuilders)
                * u. a. Natalie Portman
        * 30:00 Gegen-Kampagnen
            * Fleisch sei gut für die Umwelt (in seltenen Einzelfällen)
            * Food labeling by politics/industry: Pflanzenmilch darf nicht Milch genannt werden; Fleischalternativen nicht Fleisch
            * Australien: "slaughter" soll als Begriff in Zusammenhang mit Tierprodukten verboten werden (stattdessen "verarbeitet")
        * 33:00 "Grill'd Meat Free Monday"
        * Verwirrung stiften bei einer öffentlichen Sitzung, wo die US Food Guidelines diskutiert wurden
        * Evidence that plant-based diets are very good against type 2 diabetes
            * https://drc.bmj.com/content/6/1/e000534, 2018
        * 38:50 "US attraction Fair Oaks Farms Dairy Adventure accused of animal rights violations" - https://www.theguardian.com/environment/2019/jun/06/secret-footage-calves-fair-oaks-farms-illinois
            * "Secret footage exposes abuse of calves at Coca-Cola affiliated dairy farm"
            * systematic violence
            * practice violates multiple laws
            * kill young
        * Amazon wood burning to make land for farm animals or farm animal food
        * 46:00 people angry that marketing told them meat and dairy is needed
            * blogger: vegan is easy in Asia as well as in the US and Europe
        * "This university just banned beef at campus cafeterias" at Goldsmiths, University of London
            * "part of the school’s plan to be fully carbon neutral"
            * https://www.gold.ac.uk/conference-services/catering/
                * aber mit Hühnchen und Milch
                * Catering: "Vegan option possible on request"
                * nur mit (V) für vegetarisch ausgezeichnet
        * Disney land vegan options
            * see essen-2.md
        * a vision is provided about compassion, health etc.

### Destruction, Sustainability, Regeneration; Bedürfnisse
* http://tobyhemenway.com/videos/redesigning-civilization-with-permaculture/, 2013
    * siehe "How Permaculture Can Save Humanity and Earth, But Not Civilisation"

### Ungeahnte Geschmacks- und Gesundheitswelten
* offensiv die negative Gedankenspirale des "Verzichts"
* je weniger Tierprodukte, desto größer das neue Nahrungsuniversum
