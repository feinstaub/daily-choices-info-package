Vorreiter-Unternehmen
=====================

<!-- toc -->

- [2020](#2020)
  * [Lieferkettengesetz](#lieferkettengesetz)
  * [BKK Provita + Gemeinwohlökonomie-Video](#bkk-provita--gemeinwohlokonomie-video)
  * [CO2-Neutralität bis 2040](#co2-neutralitat-bis-2040)
  * ["Im Dickicht der Weltrettung" -> Vorbilder nötig](#im-dickicht-der-weltrettung---vorbilder-notig)
  * [Solutionismus, alles mit Technik](#solutionismus-alles-mit-technik)
  * [on Twitter](#on-twitter)
- [Beispiele 2019](#beispiele-2019)

<!-- tocstop -->

2020
----
### Lieferkettengesetz
* Kann Politik hier weiterhelfen? Was braucht es von uns dazu? (courage to make it a topic)
* siehe https://lieferkettengesetz.de
    * ... TODO ...

### BKK Provita + Gemeinwohlökonomie-Video
* https://blog.gls.de/kundenportraet/bkk-provita-krankenkasse-setzt-auf-nachhaltigkeit/, 2020
    * Gesundheit
    * Gemeinwohlökonomie-Video
        * ...
        * Müssen die Menschen in anderen Ländern darunter leiden, dass wir unsere Produkte konstant billig anbieten können?
            * (siehe palmöl.md)
        * ...
* Prämie: Spende (https://www.mellifera.de/mitmachen/nbl-bluehpatenschaft/bluehpatenschaft-verschenken/) oder Bar

### CO2-Neutralität bis 2040
* "Amazon setzt sich Klimaziel: CO2-Neutralität bis 2040"
    * https://www.logistik-watchblog.de/unternehmen/2188-amazon-klimaziel-co2-neutralitaet-bis-2040.html

### "Im Dickicht der Weltrettung" -> Vorbilder nötig
* https://blog.gls.de/bankspiegel/im-dickicht-der-weltrettung/
    * "Unverpackt einkaufen, vegan essen, Upcycling, Urban Gardening, Mikrolandwirtschaft: Was sind die wirklichen Hebel?"
    * "Nicht davon.
        Ob wir unverpackt einkaufen,
        auf Plastik
        und Fleisch verzichten,
        Mitglied einer solidarischen Landwirtschaft werden,
        aufs Fliegen
        und sogar aufs Auto verzichten —
        allein hat das natürlich so gut wie keine direkte Auswirkung aufs Weltklima.
        Selbst wenn sich ein paar Millionen Menschen so umweltbewusst verhalten würden,
        wären die Änderungen nur marginal.
        Trotzdem werden Gegner der Klimakrise oft gefragt, was sie persönlich gegen die Klimakrise tun.
        Haben Sie schon einmal gehört, dass etwa Gegner der Arbeitslosigkeit gefragt wurden,
        was sie persönlich gegen die Arbeitslosigkeit tun?
        Solche Fragen sind also in erster Linie Ablenkungsmanöver."
    * "Kann durch individuelles Handeln überhaupt etwas bewirkt werden?
        Natürlich, auf einer übergeordneten Ebene. Es ist wichtig, dass jede*r Einzelne von uns entsprechende Schritte unter nimmt.
        Der Sozialpsychologe Prof. Harald Welzer geht davon aus, dass es drei bis fünf Prozent einer Gesellschaft braucht,
        damit „ein System kippt“ — denn der Mensch folgt Vorbildern."
        * siehe auch "Soziale Kipp-Punkte" (https://www.heise.de/newsticker/meldung/Studie-Soziale-Kipp-Punkte-koennten-den-Klimawandel-eindaemmen-4642335.html)
    * ...
    * ...
    * ...

### Solutionismus, alles mit Technik
* https://www.deutschlandfunkkultur.de/kritik-an-tech-elite-demokratie-ist-nicht-einfach-eine.2950.de.html?dram:article_id=448224, 2019
    * "Alle gesellschaftlichen Probleme können mit Technik gelöst werden.
        Dieses Denken sei in den Chefetagen der Tech-Elite „weit verbreitet“, / ::techniküberschätzung
        sagt Bijan Moini. Doch selbst wenn es solche Lösungen geben würde: sie hätten einen hohen Preis."
    * "Gibt es für jedes Problem eine technische Lösung – und eine viel bessere, als sie in
        mühsamen politischen Prozessen erzielt werden könnte?
        Davon scheinen jedenfalls in der Tech-Community viele überzeugt zu sein."
    * "„Solutionismus“ und „Tech-Religion“ nannte der Soziologe Oliver Nachtwey diese Haltung im Rahmen eines Vortrags auf der Republica in Berlin.
        „Deshalb fokussiert man auch auf das Konzept der **Smart Cities**, in denen die kommunale Infrastruktur und Verwaltung
        in die Hände von Technologieunternehmen gelegt werden soll.
        Die parlamentarische Demokratie als Ganzes wird dementsprechend auch oft als ‚veraltete Technologie‘ betrachtet.“"", siehe smartcitiy.md
* 2013: "Evgeny Morozov prangert "Solutionismus" an", heise.de

### on Twitter
* https://twitter.com/refnode
    * "Wenn es um die Sache geht, ist die Expertise entscheidend.
        Wenn es um Egos geht, ist entscheidend von wem die Expertise kommt. #culture"

Beispiele 2019
--------------

* 2019: SAP-Klima-Arena
    * https://www.faz.net/aktuell/gesellschaft/menschen/dystopie-in-der-klima-arena-baeume-ueberleben-nur-in-glasvitrinen-16421591.html
        * "Die „Klima-Arena“ von Dietmar Hopp zeigt dystopisch, was mit der Erde passieren könnte, wenn die Erderwärmung nicht begrenzt wird. Die Ausstellung soll für eine „Grundalphabetisierung zu Klimathemen“ sorgen."
            * in other news: "Konkretes für den Klimaschutz: Kauft nichts!" (https://www.faz.net/aktuell/feuilleton/klimaschutz-was-kann-der-einzelne-fuer-das-klima-tun-16417584.html)

* „Transformations-Literacy“? -> siehe Maja Göpel
