* Klimastreik: „Wir brauchen einen Aufschrei der Wirtschaft“, Nov. 2019
    * https://blog.gls.de/bankspiegel/klimastreik-wir-brauchen-einen-aufschrei-der-wirtschaft/
    * Hambacher Forst
        * "Er habe sich aber bestärkt gefühlt, weil Jorberg am Vortag per YouTube RWE für dessen Braunkohlepolitik kritisiert hatte: „Das ist an Rückwärtsgewandtheit nicht mehr zu überbieten.“"
        * „Danke! Hätte nie gedacht, dass ich mal einer Bank danke.“
        * "Wenig später charterte die GLS Bank zwei Busse aus Bochum zur Großdemo am „Hambi“"
    * "Taten müssen folgen"
        * "Etwa vom Axel-Springer-Verlag, der seiner Belegschaft Freizeit für die Demoteilnahme gewährte"
    * "Wie reagierte die GLS Bank auf das Klimapaket?"
        * Jorberg: "„Wenn angesichts der fortschreitenden Klimakatastrophe, klarer Forderungen aus Wissenschaft, Zivilgesellschaft, der jungen Generation und der Wirtschaft nur ein solches Klimapaket möglich ist, dann brauchen wir eine veränderte politische Führung.“"
    * "Learning vom Klimastreik 3"
        * "Denn noch nie in der 45-jährigen Bankgeschichte hatten wir innerhalb weniger Tage eine so breite Presseresonanz. Learning: Lasst die eigenen Fähnchen ruhig mal weg. Irgendwie erhöht das die Sichtbarkeit."

* https://blog.gls.de/aus-der-bank/gls-bank-mobilisiert-fuer-klimastreik/, Sept. 2019
    * "Ladet junge Menschen von Fridays for Future für Vorträge und Workshops in euer Unternehmen ein."
        * https://klima-streik.net/fff-einladen/
