Essen / Umfrage-Skript
======================

<!-- toc -->



<!-- tocstop -->

"with this script"

Anstelle von Unterschriftensammlungen machen wir was anderes: mit den Leuten ins Gespräch kommen mittels Umfragen

Titel: **Nachhaltigkeit und Tierwohl - Interessiert?**

Intro:

    Wir suchen Mitarbeiterinnen und Mitarbeiter, denen Nachhaltigkeit und Tierwohl etwas bedeutet / wichtig ist.
    Wir möchten durch eine Umfrage eine Datengrundlage schaffen, um den Dreiklang von
        Nachhaltigkeit, Tierwohl und gesunde Ernährung
    fundiert voranzubringen.
    Mitmachen?
    Je nach Tiefe dauert die Abfrage 5 - 30 Minuten.
    Keine Angst, die wichtigsten Fragen sind vorne und es soll spannend sein; wenn es langweilig wird, können Sie jederzeit abbrechen
        und wir haben dennoch einen Mehrwert geschaffen.

1. Wenn Sie an die Kombination **Nachhaltigkeit und Tierwohl** denken, was fällt Ihnen dazu ein?

    bio         -> OPT_1_a
    vegan       -> OPT_1_b
    (?, sammeln)

OPT_1_a:
2. Wie wichtig ist Ihnen **Tierwohl**? Sind die Herstellungsbedingungen für Sie ausreichend **transparent**?

    Transparenz, ja ausreichend:        -> OPT_2_a
    Transparenz, nein nicht ausreichend -> OPT_2_b

OPT_1_b:
3. Ernähren Sie sich **vegan oder überwiegend vegan oder würden es gerne**?

    Wenn ja,    -> OPT_3_a
    Wenn nein,  -> OPT_3_b
    Wenn würden -> **Was hält Sie davon ab?**

OPT_3_a:
4. Sind Sie mit den pflanzlichen Optionen **zufrieden**?
    Haben Sie konkrete Verbesserungsvorschläge zu den pflanzlichen Optionen?

    NEIN, unzufrieden und JA Vorschläge:     -> OPT_4_a

OPT_3_b:
5. Würden Sie ein **Ausbau von veganen Optionen** begrüßen?
    Gibt es Vorbehalte?

    (sammeln)

OPT_2_a:
6. Welche Tierprodukte essen Sie am häufigsten und welche **Vorstellungen** haben Sie von den Haltungsbedingungen?
    ...
    (sammeln)

OPT_2_b:
7. Würden Sie gerne wissen, woher die Tierprodukte stammen, die Sie auswählen können? / **Transparenz**

    ja      -> OPT_7_a
    nein    -> Warum nicht? (sollte nicht vorkommen, wenn Nachhaltigkeit und Tierwohl wichtig sind)

OPT_4_a:
OPT_7_a:
9. Haben Sie schon **persönlichen Kontakt** zu Ihrer Kantine aufgenommen?

    ja      -> Ergebnis/Erkenntnisse? (sammeln)
    nein    -> Was hindert Sie daran? (Animation es zu tun)

    -> weiter mit _10.

OPT_1_a:
10. Kennen Sie **unsere Gruppe**? (Bio, Pflanzlich, Grünes Essen)
    -> Bekanntmachen / Beitreten

OPT_7_a:
11. Gibt es eine **Grenze**, wo Sie sagen würden, bestimmte Zustände würden Sie nicht akzeptieren und als Konsequenz die Tierprodukte weglassen?
    Welche Zustände wären das?

    ...

12. Thema Tierwohl: Was ist Ihnen prinzipiell wichtiger: der **Geschmack** beim Mittagessen oder das **Leben** eines Tieres?

    Geschmack:  Warum? (sammeln)
    Tier:       -> OPT_1_b

13. Kennen Sie **professionelle Dienstleister**, die Großküchen bei der Umstellung auf mehr pflanzlich und bio helfen können?

    (sammeln)

14. Wir befinden uns ja derzeit im **6. Massensterben** von Tier- und Pflanzenarten.
    (über 1000 / Tag; viele noch nicht mal entdeckt; charismatische und nicht-charismatische Arten)
    (siehe "Vortrag von Andreas Hetzel, Universität Hildesheim. Prof. Dr. Andreas Hetzel")
    Was bedeutet Ihnen der Schutz der Arten und unserer Lebensgrundlagen bzw. das Leiden der Kreaturen an sich? (sensibilisieren)
    Das Umstellen der Produktionssysteme kostet Geld (Bildung, Umschulung, Maschinen etc.)
    Wieviel mehr dürfte ein Mittagessen mehr kosten, wenn es dazu beiträgt, die neuen
        Systeme so aufzubauen, dass sie (im Gegensatz zu heute) nachhaltig funktionieren?

    50% mehr
    100% mehr doppelt so viel wie heute
    drei mal so viel wie heute
    vier mal so viel wie heute

OPT_1_a:
15. Angesichts des Aussterbens von sehr vielen Arten pro Jahr, der Verschmutzung der Umwelt
    und Situation der Menschen in armen Ländern, die davon und vom Klimawandel jetzt schon betroffen sind,
    welche **Gefühle** verbinden Sie damit?

    Trauer              -> ...
    Wut                 -> ...
    Resignation         -> ...
    Gleichgültigkeit    -> ?
    (weitere, sammeln)

    -> weiter mit _14.

16. an System-Techniker: Komplexe Systeme
    Was könnten Ihrer Ansicht nach die Folgen sein, wenn aus dem evolutionär entstandenen Netz der Arten
    nach und nach und in verhältnismäßig hohem Tempo immer Bausteine entfernt werden?
    (Denken Sie dabei insbesondere an die nicht-charismatischen Arten wie Würmer und Insekten.)

    (sammeln)
