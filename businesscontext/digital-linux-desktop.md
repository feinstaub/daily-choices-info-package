The Linux Desktop Working Group
===============================

<!-- toc -->

- [Open](#open)
- [Done](#done)
- [Beispiele von anderswo](#beispiele-von-anderswo)

<!-- tocstop -->

Bereich Flughäfen

'Linux-Desktop Working Group'

Die Software-Server-Welt läuft mehrheitlich auf Opensource und Linux.

Zeit für Konvergenz auch auf dem Desktop.

Die einen sagen: Im Unternehmensumfeld undenkbar. Ein aussichtloses Unterfangen. Wir sagen: Wir lieben Herausforderungen! Entrepreneurship vom Feinsten.

Als Unternehmen mit öffentlichem Auftrag und Verantwortung für eine wichtige staatliche Infrastruktur, scheint es angemessen auch auf den Büro- und Entwickler-Arbeitsplätzen mit unabhängiger und kontrollierbarer Software zu arbeiten.

Join the Team!

Hier ist eine Open-Issue-Liste von (in der aktuellen Interpretation der Rahmenbedingungen) noch ungelösten Problemen, die gelöst werden müssen, um weiterzukommen

Open
----
- Open: Security-Sicht: brauchbare Management-Software für Linux-Clients
- create a New issue

Done
----
- Done: Entwickler, die effektiv auf unixoide Systeme setzen, wenn ohnehin für Linux entwickelt wird
- Done: Anforderung, sichere und kontrollierbare Systeme zu verwenden
- Done: Kostensparpotential vorhanden

Beispiele von anderswo
----------------------
* CERN
* Liste in Wikipedia öffentliche Verwaltungen
* Berichte und Analysen: siehe SZ, FAZ (f-droid.md)
