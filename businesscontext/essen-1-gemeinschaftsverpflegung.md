Gemeinschaftsverpflegung
========================

<!-- toc -->

- [Möglicher Plan nach Lesen des IPCC-Reports](#moglicher-plan-nach-lesen-des-ipcc-reports)
- [Gewohntes beibehalten](#gewohntes-beibehalten)
  * [Beispiele](#beispiele)
- [Wo könnte es hingehen?](#wo-konnte-es-hingehen)
  * [Klima-Teller](#klima-teller)
  * [Quelle: Fleischatlas 2018](#quelle-fleischatlas-2018)
  * [Keeks](#keeks)
  * [Kundeninformation: Eaternity Score](#kundeninformation-eaternity-score)
- [Gemeinwohl](#gemeinwohl)
- [Diversity](#diversity)
- [Org-Fragen](#org-fragen)
- [FAQ](#faq)
- [Botschaften und Antworten](#botschaften-und-antworten)
- [Tipps und Tricks](#tipps-und-tricks)
- [Beispiele](#beispiele-1)
  * [Anbieter](#anbieter)
  * [Abnehmer](#abnehmer)
  * [Lernen vom Umweltvorreiter Volvo](#lernen-vom-umweltvorreiter-volvo)
  * [arte-Reportage inkl. Bio](#arte-reportage-inkl-bio)
  * [Speisepläne](#speiseplane)
  * [Speiseplan-Apps](#speiseplan-apps)
  * [hypocritical examples - Milch](#hypocritical-examples---milch)
- [Beratung, Bewusstsein](#beratung-bewusstsein)

<!-- tocstop -->

Möglicher Plan nach Lesen des IPCC-Reports
------------------------------------------
Dem Kantinenbetreiber werden - nachdem die Unterstützung durch die Belegschaft sicher ist - folgende Schritte vorgeschlagen.

1. (sofort durchführbar) Der oder die Hauptlieferanten werden eindringlich informiert, sich einmal den IPCC-Sonderbericht zur Landnutzung (Klimawandel, außerdem Informationen zum Artensterben) und sonstige Artikel mit wissenschaftlichen Vorschlägen, wie eine Landwirtschaft und ein Speisenangebot sinnvoll gestaltet wird, durchzulesen. Die Nachfrage wird sich in Kürze darauf einstellen.

2. (sofortiger Einstieg möglich) Einführung einer vollwertigen rein pflanzlichen Menülinie. Begleitend nötig: Personalschulung wie man das schmackhaft umsetzen kann.

3. (sofortiger Einstieg möglich; Finanzierung sichern) Nicht-frische Produktgruppen werden auf kontrolliert ökologische Varianten umgestellt

4. (möglicher Widerstand erwartet) Bepreisung von tierproduktbasierten Zutaten wird den realen ökologischen und sozialen Kosten angepasst. Kein "von 0 auf 100", sondern unternehmensinterne Kommunikation des Plans und dann schrittweise Erhöhung. Parallel sollte die Geschacksqualität und Attraktivität der pflanzlichen Menülinie schrittweise erhöht werden.
    - Aus dem worst-case - ein shitstorm - machen wir - die Unterstützer - einen storm of love.
    - Unsere Kommunikationsexperten - geschult in gewaltfreier Kommunikation - stehen bereit, um individuelle Anfragen, die Wut, Ärger oder Besorgnis ausdrücken, zu bearbeiten.

5. (kurz- bis mittelfristig möglich) Ersetzen aller Tierprodukte durch eine regionale / "artgerechte" / Bio-Variante. Ausphasen einzelner Produktgruppen

6. (mittelfristig) Umstellung von frischer und Saisonware auf Bio-Qualität oder zumindest ohne Pestizide

7. (sofort durchführbar) Süßigkeitenangebot auf Bio umstellen. Bei Bio-Kakao möglichst Fair-Trade verwenden oder weglassen. Höhere Preise für Zuckerprodukte kommen zudem der Gesundheit zugute.

8. Anwendung nicht nur auf die Kantine, sondern auch Food-Automaten, Eiskisten und Catering.

Gewohntes beibehalten
---------------------
### Beispiele
* Bio-veganer Döner
* Speisekarten

Wo könnte es hingehen?
----------------------
... wenn Nachhaltigkeit einen Fokus hat.

### Klima-Teller
* https://www.tibits.de/de/blog/detail/klima-teller
    * ... "Denn bei der Produktion von Lebensmitteln – besonders tierischen Ursprungs – entstehen Gase, die schädlich für unser Klima sind. Forscher zeigen nun auf, wie wir unsere Ernährung klimafreundlicher und gleichzeitig gesünder gestalten können"
    * 4 Schritte für mehr Nachhaltigkeit
        * Produktwahl (mehr pflanzlich statt tierisch)
        * Saisonalität
        * Regionalität
        * Abfall-Reduktion

### Quelle: Fleischatlas 2018
* https://www.bund-bergstrasse.de/fileadmin/bergstrasse/Ernaehrung/massentierhaltung_fleischatlas_2018.pdf
    * S. 12: "Ernährungsbewusst wollen viele sein. Es fällt viel leichter, wenn die Mahlzeiten an der Speisetheke entsprechend präsentiert werden"
        * mit Beispielbild und Tipps

### Keeks
https://www.keeks-projekt.de/

"Was ist Keeks?  - Das Projekt "KEEKS - Klima- und energieeffiziente Küche in Schulen" zeigt, wie in der Schulküche klima- und energieeffizient gekocht werden kann. Damit werden wichtige Maßnahmen zum Klimaschutz im Ernährungssektor aufgezeigt. Einem Sektor, in dem Deutschland noch vergleichsweise geringe Beiträge zum Klimaschutz leistet.", ... "Das KEEKS Projekt wurde 2018 vom UN Klimaschutzsekretariat UNFCCC als “Momentum for Change” Leuchtturmprojekt in der Kategorie “Planetary Health” ausgezeichnet."

Unter Materialien sind Videos zu finden, z. B. dieses hier https://elearning.izt.de/mod/page/view.php?id=1356, wo in 3 Minuten kompakt erklärt wird, wie ein zukunftsfähiger Speiseplan aussehen sollte.

### Kundeninformation: Eaternity Score
* https://eaternity.org/score/
    * Klima
    * Wasser
    * Tierwohl
    * Regenwald

Gemeinwohl
----------
* Wie können andere von der Umweltvorreiter-Kantine lernen? Arbeitsmethoden?
* Drängende Themen wie Antibiotika-Einsatz in der Tiermast, insektenschädliche Anbaumethoden und Pflanzenschutzmittel
    * Tierwohl, siehe Umfrage, dass es den meisten Menschen wichtig ist
* Hoffnung: Wunder von Mals
* Gesundheit und Fairness zu Vieh und Bio-Bauern
* Opensource/Transparenz
* Wertschätzende Kommunikation

Diversity
---------
* Wie groß muss eine Minderheit sein, damit sie nicht als zu vernachlässigende Randgruppe gilt, sondern mit Hilfe "Diversity" anerkannt und gewertschätzt wird?
    * Gewissensgründe
    * Glaubensgründe
* Unkomplizierte Teilnahme an der Gemeinschaft
* Aufwand vs. Nutzen
    * Beispiel: vegan lebende Menschen:
        * Wie schwer ist es auf Angebotsseite dies zu berücksichtigen?
            * (ganz leicht, wenn Bewusstsein da ist)
        * Haben dadurch andere (gravierende) Nachteile?
            * (nein)
* Ab wann sollten Gespräche mit relevanten Stellen geführt werden dürfen?
* Wer ist zuständig? Betriebsrat? Diversity-Beauftragte?
* Was tun gegen mediale Verzerrung?

Org-Fragen
----------
* Auf welcher ebene werden die Ökologie-Themen bearbeitet?
    * Transport in Sub-Restaurants?
* Wie werden Kundenwünsche von umweltorientierten Kunden berücksichtigt?
* Prozesse verstehen, um konstruktiv etwas beitragen zu können
* Gibt es eine Liste mit häufigen Fragen zu Produkten?
    * Ergebnisse als Grundlage für die nächsten Umweltschritte -> Besser: Umfrage unter Gästen

FAQ
---
* Bio ist nicht die Lösung für alles, aber ein effektives und transparentes Mittel zur Verbesserung der Umweltbilanz
    * mit dem Potential für bessere Gesundheit
* Was bringt eigentlich Bio außer keine chem.-synth. Pestizide (novel entities) und mehr Artenvielfalt?
    * ist das nicht schon viel genug?
    * ...

Botschaften und Antworten
-------------------------
* Transparenz
    * Die Erkenntisse liegen vor (Wissenschaft; Zustände und Auswirkungen in Tieranlagen); warum dennoch weiter um den heißen Brei herumreden?
* Fleisch-Tradition
    * Ja, Jagd und tierische Nahrungsmittel haben uns in der Geschichte mit dorthin gebracht wo wir heute sind; aber warum müssen wir heute, wir wir Menschen gereift sind (auch gesellschaftlich und moralisch), wie Kleinkinder an längst überholten Fleischgerichten festhalten.
* Wissenschaft
    * Wir sind überall rational unterwegs (z. B. Technologie etc.) und basieren unsere Entscheidungen auf Vernunft. Warum fällt uns das im Bereich Gemeinschaftsverpflegung so schwer? Insbesondere was Entscheidungen angeht, die die Angebotsseite zu treffen hat.
    * In anderen Bereichen - wie z. B. bei der Verwendung von Elektronik und die sich daraus ergebenden Veränderungen des Lebensstils oder das Vertrauen in die Digitalisierung - verlassen wir uns voll auf wissenschaftliche Erkenntnisse. Warum fällt es aber in anderen Bereichen - wie xyz - so schwer, die Erkenntnisse anzunehmen?
* Nachfrage
    * "Studie des Umweltbundesamts - Umweltbewusstsein kein Nischenthema" (https://www.tagesschau.de/inland/umweltbewusstsein-studie-101.html, 2019)
        * "Rund zwei Drittel der Befragten, 64 Prozent, schätzen Umwelt- und Klimaschutz als eine sehr wichtige Herausforderung ein. Das sind elf Prozent mehr als noch 2016."

Tipps und Tricks
----------------
* https://www.veganblog.de/aktiv-sein/vegane-gerichte-in-der-gemeinschaftsverpflegung/
    * "Vegane Gerichte in der Gemeinschaftsverpflegung"
    * Beispiele
    * Was tun
    * Erfahrungsbericht

Beispiele
---------
### Anbieter
* https://lebensdurst.com/
    * FFM
    * regional, saisonal
    * slow food
    * vegan im Programm

* https://www.phoenix-naturkost.de/
    * Bio-Großhandel
    * PLZ 6xxxx

### Abnehmer
* Sono Motors, vegan
    * https://www.goingelectric.de/forum/viewtopic.php?t=49651&start=30

### Lernen vom Umweltvorreiter Volvo
* ["Klimafreundliche Kantine"](https://www.dw.com/de/klimafreundliche-kantine/av-45145611), 2018, 2min
    * "Im schwedischen Göteborg erfahren die Gäste der Kantine eines Autoherstellers die Klimabilanz ihres Mittagessens direkt auf der Speisekarte. Gleichzeitig versucht der Chefkoch, möglichst klimafreundlich zu kochen."
    * Klimabilanz auf der Speisekarte
        * Zusatzinfo hilft Mitarbeitern sich an manchen Tagen anders zu entscheiden
    * weniger Rindfleisch, mehr Huhn und Fisch, und natürlich mehr Gemüse
    * Carbon Cloud: Beratung wie man eine Speisekarte klimafreundlich gestaltet
    * "gut für den Bauch; gut für das Klima"

### arte-Reportage inkl. Bio
* ["arte Reportage – Gutes Essen, gute Arbeit – Kreative Köche revolutionieren die Kantine"](https://dokustreams.de/arte-reportage-gutes-essen-gute-arbeit-kreative-koeche-revolutionieren-die-kantine/), 2018, 30 min
    * https://www.youtube.com/watch?v=z4rPDp013-Y
    * "Betriebskantinen machen nicht immer Appetit. Viele Arbeitnehmer weichen deshalb aus auf Lieferservice, Restaurants oder das Lunchpaket von zuhause. Dass modernes Kantinenessen auch frisch, lecker und ein Highlight des Tages sein kann, zeigen Beispiele aus Frankreich, Deutschland und Dänemark."
    * "Was es braucht, ist Mut, auch mal flexibel zu sein."
    * ...
    * "In Dänemark zeigt Kantinen-Revolutionär Mikkel Karstad, wie Bio-Erbsen und Möhren aus ökologischem Anbau zu den neuen Stars der Kantinenküche aufsteigen."

### Speisepläne
* https://www.studentenwerkfrankfurt.de/essen-trinken/speiseplaene/mensa-casino/
    * jeden Tag ein sichtbar ausgezeichnetes veganes Gericht

### Speiseplan-Apps
* https://openmensa.org
    * todo: nach Filter schauen

* MensaPlan, github
    * todo: nach Filter schauen

### hypocritical examples - Milch
* Danone - sustainable company?
    * siehe "Das zynische Geschäft mit Säuglingsnahrung"
    * siehe milch.md, "Das System Milch"
    * Milch: ""Unsere Überschüsse machen uns kaputt"", https://www.heise.de/tp/features/Unsere-Ueberschuesse-machen-uns-kaputt-4001364.html?seite=all, 2018
        * "Die globalisierte Milchwirtschaft quält nicht nur Tiere, sondern beutet auch Menschen in und außerhalb Europas aus"
        * "Die mächtigsten Akteure auf dem Milchmarkt sind Nestlé (Schweiz) und Danone (Frankreich). In Deutschland gehören das Deutsche Milchkontor (DMK) und die Müller-Gruppe zu den führenden Molkereien"
        * "Die Lebensmittelindustrie inklusive Einzelhandel mit einem Gesamtumsatz von 1,4 Billionen Euro ist der größte Wirtschaftszweig. Lebensmittelgiganten wie Nestlé und Danone werden in Brüssel durch Food Drink Europe vertreten."
        * "Die europäische Agrarpolitik, die damals stark reglementiert war, sollte dem Weltmarkt geöffnet werden. Seitdem erhält der Landwirt Direktzahlungen und darf produzieren, was er will. Anstatt die die 13 Millionen in Europa zu unterstützen, investieren europäische Molkereien wie ArlaFoods und Danone in Asien. Damit erhöhen sie den Druck auf die Bauern in Europa."
        * ...
    * Danoe führt Ampel ein?, spiegel, 2018, nutriscore

Beratung, Bewusstsein
---------------------
* https://www.vier-pfoten.de/kampagnen-themen/themen/nutztiere/tierleid-in-deutschlands-kantinenessen, 2018
    * "Bezahlt von Ihrem Steuergeld: Fleisch aus Intensivhaltung in Kitas, Schulen, Mensen & Krankenhäusern"
    * Tierschutzkrierien: https://www.vier-pfoten.de/kampagnen-themen/themen/haltungskennzeichnung/kriterien-fuer-haltungskennzeichnung
    * https://whatthelunch.vier-pfoten.de/
