Werbeblocker / Der Werbe-Transformer
====================================

Für die besten Dinge der Welt wird keine Werbung geschaltet? Die Zeiten sind vorbei!

<!-- toc -->

- [Inspiriert durch Ecosia](#inspiriert-durch-ecosia)
- [Hintergrund Werbung](#hintergrund-werbung)
- [Werbeblocker](#werbeblocker)
- [Der Werbetransformer](#der-werbetransformer)

<!-- tocstop -->

Inspiriert durch Ecosia
-----------------------
Inspiriert durch Ecosia (https://en.wikipedia.org/wiki/Ecosia, https://de.wikipedia.org/wiki/Ecosia). Und dann noch einen Schritt weitergedacht.

* Startseite Ecosia:
    * "70 Mio (70.000.000) Bäume wurden von Ecosia-Nutzern gepflanzt" (insgesamt)
        * 2018: ca. 30.000.000 Bäume
* zum Vergleich
    * Jeder Beitrag zählt! Nur sollte man das große Ganze dabei nicht aus dem Blick verlieren:
    * "Anzahl der Bäume weltweit ist unerwartet hoch, aber sinkt stetig"
        * https://www.forstpraxis.de/neue-studie-anzahl-der-baeume-weltweit-ist-unerwartet-hoch-aber-sinkt-stetig/, 2015, Waldökologie
        * "derzeit jährlich mehr als 15 Milliarden (15.000.000.000) Bäume gefällt"
        * "rund 0,5 % des derzeitigen weltweiten Baumbestands, gehen damit jährlich verloren"
        => in 200 Jahren gibt es keine Bäume mehr
        * "Die zunehmend intensive Forstwirtschaft und Veränderungen in der Landnutzung infolge der wachsenden Weltbevölkerung sind nach Einschätzung der Autoren wesentliche Ursachen"
        => Ecosias 30.000.000 Bäume x 500 (= 15 Millarden) => aktuelle Fällrate wird gerade so ausgeglichen (wenn die Bäume auch wirklich länger stehenbleiben und nicht durch eigene Fällung genutzt werden)
    * -> Info auf die Ecosia-Webseite bringen?
    * Holzpellets
        * https://www.welt.de/wissenschaft/umwelt/article153440851/Die-naive-Sorglosigkeit-der-Holzpellets-Heizer.html, 2016
            * "Laut Angaben der Umweltschutzorganisation WWF werden in Bulgarien Jahr für Jahr vier Millionen Festmeter Holz illegal gefällt"
            * "in Rumänien werde der illegale Holzschlag durch Korruption zusätzlich ermöglicht."
            * "Außerdem, so der WWF, leide unter einer Ausrichtung der Wälder auf eine effektive Holzproduktion die Artenvielfalt."
    * Suchmaschine Bing von Microsoft
        * Microsoft: "Bilanzgewinn" 2019: ca. 39 Millarden Dollar (39 000 000 000) (https://de.wikipedia.org/wiki/Microsoft)
        * Von Ecosia investierte Geldsumme in Bäume in 2018: 4 000 000 EUR = 4 452 000 Dollar
            => 1000x mehr Bäume pflanzen kosten ca. 4 500 000 000 = 1/8 von Microsofts Jahresgewinn. Sinnvoll investiert?
* Carbon negative?
    * https://ecosia.zendesk.com/hc/en-us/articles/201531072-How-does-Ecosia-neutralize-a-search-s-CO2-emissions-
    * missleading?
    * "Nach eigenen Angaben betreibt Ecosia alle Server mit Ökostrom von Greenpeace Energy; dies gilt allerdings nicht für den eigentlichen Suchvorgang auf den Servern von Bing."
        * das heißt, es wird sogar noch zusätzlicher Strom verbraucht; wenn auch vermutlich weniger
* Aktivitäten
    * Spenden 2014
        * ca. 500.000 EUR an The Nature Conservancy
        * https://de.wikipedia.org/wiki/The_Nature_Conservancy
            * https://en.wikipedia.org/wiki/The_Nature_Conservancy#Criticism - Nähe zu Big Business
    * "Am 9. Oktober 2018 erregte das Unternehmen Aufsehen mit einem an den Energiekonzern RWE gerichteten öffentlichen Kaufangebot in Höhe von 1.000.000 € für den noch existenten Teil des Hambacher Forstes"
* https://en.wikipedia.org/wiki/Ecosia#Business_model

* Erinnerung an
    * Papierkrise / steigenden/hohen Papierverbrauch, insbesondere absolute Werte in Deutschland: siehe papier.md
    * Regenwaldwaldrohdung für Palmöl
        * https://de.blog.ecosia.org/uberleben-von-ecosia-gepflanzten-baume/
            * "[...] opfert beispielsweise Urwälder auf dem Altar kurzfristigen Wirtschaftswachstums. In Uganda zerstört die Kohleindustrie unaufhaltsam den Lebensraum der Schimpansen. In Indonesien werden große Waldflächen abgebrannt, um sterile Palmölmonokulturen anzupflanzen."
                * think of Nutella
                * think of Stromverbrauch

Hintergrund Werbung
-------------------
* siehe werbung.md
* siehe werbung.md / "Für die besten Dinge wird keine Werbung gemacht"
* Für Nachhaltigkeit ist derzeit vor allem Problembewusstsein, Lösungsorientierung und Änderung von gewohnten Verhaltensweise erforderlich
    * Werbung zielt derzeit vor allem auf Verkauf von Produkten und Dienstleistungen ab.
* Fragen
    * Hat die Größe des Werbebudgets (und damit die Sichtbarkeit eines Produkts in der Werbung) etwas mit der Qualität oder der Nachhaltigkeit des Herstellers zu tun?
        * Wenn ja, was?
* menschliche Tageskapazität an Botschaften ist begrenzt (todo: wo?)
* Tipps/Botschaften für nachhaltige Verhaltensweisen
    * "Machen Sie doch mal wieder einen Spaziergang"
    * "Nachbarschaft zum Kennenlernen einladen anstatt Städtereise"
    * "Wann zum letzten Mal im Wald"
    * "Geld sparen, anstatt ausgeben"
    * "Geld gemeinnützig spenden, anstatt ausgeben"
    * "Mehr geben als selber nehmen"
    * "Gemeinsam kochen, anstelle Fertiggerichte kaufen"
    * "Tierdoku schauen anstelle Afrikareise mit Safari"
    * "Oma anrufen"
    * "Eltern besuchen"
    * "Schulfreundin anrufen"
    * "Für die Kollegen einen Kuchen backen"
    * "Mehr Gemüse"
    * "Siehst du dich als Verbraucher oder als Bürger?"

Werbeblocker
------------
* Vorteile
    * Weniger Netzwerktraffic => Energie sparen
    * Weniger Ablenkung (vom eigentlichen Inhalt der Webseiten)
    * Datenschutz => weniger Tracker
    * IT-Sicherheit => weniger Einfallstore für Schadsoftware
    * Nachhaltigkeit, wegen weniger Werbung
    * Keine Werbungunterbrechung in Youtube-Videos

Der Werbetransformer
--------------------
hackathon-Idee aus dem Haus der Nachhaltigkeit.

--> "Add Transformer Mode to uBlock Origin (Open Source Project)

Tags:

    #umwelt
    #nachhaltigkeit
    #datenschutz
    #betriebsklima
    #gesundheit
    #opensource
    #hackathon
    #digitalisierung

Features:

* Per KI als Fingerübung erkennen, welche Art von Werbung herausgefiltert wurde.
    * Dann ersetzen, zum Beispiel: FastFood-Werbung --> "Iss mehr Obst"
* Alle Botschaften sind
    * lokal gespeichert
    * komplett einsehbar
    * und individuell konfigurierbar
* Persönliche Daten für personalisierte Auswahl der Botschaften sind alle lokal gespeichert

Botschaften:

* General Mode
    * "Heute schon jemanden angelächelt? :-)"
    * "Wann mal wieder den Keller ausmisten?" (vielleicht findet sich ja was wieder)
    * ...siehe oben
* Arbeitskontext
    * "Wer ist dein Kunde?"
    * "Weißt du, an was deine Kollegin im Nachbarbüro arbeitet?"
    * (zeitabhängig) "Wie wärs mal mit Mittagspause mit den Kollegen?"
    * (stockwerkabhängig) "Heute Treppe statt Fahrstuhl?" ("Do not show this again")
    * "Wann hast du der Teamkollegin, die dir immer sehr hilft, ein Lob ausgesprochen?"
    * "Die beste Sitzposition ist die nächste!"
    * "Streck dich mal"
    * "Wann zum letzten Mal was getrunken?"
    * "Nächstes Mal mit dem Rad zur Arbeit?" ("Do not show this again")
    * (Stadtbewohner) "Wenn du morgens läufst, ist die U-Bahn für andere nicht so voll." ("Do not show this again")
    * ...
* Arbeitskontext / Gesundheit / Elektroschrott
    * "Was klingt besser: das Piepsen des Fitnessarmbandes oder die Stimme deiner Kollegin, die dich zum gesunden Mittagessen einlädt?"
    * "Was fühlt sich besser an: das Vibrieren des Fitnessarmbandes oder dein netter Kollege, der dich an die Hand nimmt, um Treppe zu laufen?"
* Scrum-Master-Mode
    * "Kennst du die Sprintziele?"
    * "Gibt es Abstimmungsbedarf mit dem Nachbarteam?"
* Interaktive Mode
    * Vokabeltrainer
    * Wettervorhersage für die Heimfahrt
    * Quiz aus dem Projekt-Glossar
    * Spruch des Tages
    * Sonstige Lern-Apps
    * Non-Distract mode (einfach Werbung blockieren, wie bisher)
* Eventuell mit Bildbotschaften arbeiten
