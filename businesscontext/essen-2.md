Essen 2
=======

<!-- toc -->

- [pestizidbites](#pestizidbites)
  * [Ideen für nächste Posts](#ideen-fur-nachste-posts)
- [2020](#2020)
  * [Was ist Vegan? - Definition](#was-ist-vegan---definition)
  * [Positive Vision entwickeln](#positive-vision-entwickeln)
  * [Vegane Unternehmer: REWE, Rügenwalder](#vegane-unternehmer-rewe-rugenwalder)
  * [Bio-Obst vom Bodensee biozyklisch-vegan](#bio-obst-vom-bodensee-biozyklisch-vegan)
  * [Oxford-Studie - Kommunikation etc.](#oxford-studie---kommunikation-etc)
  * [2020: Oxford-Studie - Graphik: lokal vs. Food-Choice, Transportweg vs. tierisch](#2020-oxford-studie---graphik-lokal-vs-food-choice-transportweg-vs-tierisch)
  * ["Parkinson: Berufskrankheit bei Landwirten?"](#parkinson-berufskrankheit-bei-landwirten)
  * [Gesundheit: Workshop-Ideen, 2020](#gesundheit-workshop-ideen-2020)
  * [Genuss: veganer Käse aus Frankreich / Positive Emotionen nutzen](#genuss-veganer-kase-aus-frankreich--positive-emotionen-nutzen)
  * [Gefühle / Männer-Angst vor Tofu / Lachs](#gefuhle--manner-angst-vor-tofu--lachs)
  * [Qualitative Einschätzung: Umweltvorreiter-Kantine](#qualitative-einschatzung-umweltvorreiter-kantine)
  * [Die richtigen Fragen](#die-richtigen-fragen)
  * [Kennzeichnung: vegan nicht geschützt](#kennzeichnung-vegan-nicht-geschutzt)
  * [Möglichkeiten von Akteuren außerhalb des Gastro-Anbieters](#moglichkeiten-von-akteuren-ausserhalb-des-gastro-anbieters)
  * [Gesundheit: Metzger wird vegan, kein Fleisch, voller Genuss, 2020](#gesundheit-metzger-wird-vegan-kein-fleisch-voller-genuss-2020)
  * [Gesundheit: BKK Provita, 2020](#gesundheit-bkk-provita-2020)
  * [Beispiel: Golden Globes: 100% plant-based menu](#beispiel-golden-globes-100%25-plant-based-menu)
  * [More on Hollywood: a powerful message](#more-on-hollywood-a-powerful-message)
- [2019](#2019)
  * [Gesundheit: Kanada, 2019](#gesundheit-kanada-2019)
  * [Prominente](#prominente)
  * [Gesundheit: Game Changers, 2019](#gesundheit-game-changers-2019)
  * [Kennzeichnung: Disney Land](#kennzeichnung-disney-land)
  * [Gesundheit mit Metzgern, 2019](#gesundheit-mit-metzgern-2019)
  * [Nachhaltigkeit](#nachhaltigkeit)
  * [CO2-neutral: ohne Rindfleisch](#co2-neutral-ohne-rindfleisch)
  * [Studie zu Fleisch von artec](#studie-zu-fleisch-von-artec)
  * [scinexx - Umweltschutz - Verantwortung übernehmen](#scinexx---umweltschutz---verantwortung-ubernehmen)
  * [Maßnahmen](#massnahmen)
  * [Gesundheit: Schlaganfallriskio niedriger, wenn fleischlos; aber..., 2019](#gesundheit-schlaganfallriskio-niedriger-wenn-fleischlos-aber-2019)

<!-- tocstop -->

pestizidbites
-------------
### Ideen für nächste Posts
* Weihnachtszeit - Tannenbaumzeit:
    * "Öko-Christbäume sind besser für Boden, Wasser und Artenvielfalt"
        * aus: https://www.geo.de/natur/nachhaltigkeit/17952-rtkl-gesundheitsgefahr-christbaum-so-viel-gift-steckt-weihnachtsbaeumen, 2017
    * https://www.daserste.de/information/wirtschaft-boerse/plusminus/sendung/sr/sendung-vom-05-12-2018-weihnachtsbaeume-100.html, 2018
        * ""Auf konventionellen Plantagen werden Pestizide und Düngemittel eingesetzt. Das heißt Pestizide gegen Insekten, die man nicht haben will, gegen Unkräuter, oder auch gegen Pilzkrankheiten." Diese Stoffe schaden dem Ökosystem, führen zum Sterben vieler wichtiger Nützlinge und zerstören deren Lebensraum."
        * "Auf allen fünf Bäumen sind zumindest Spuren von Schadstoffen sichtbar, bei drei davon sogar in höheren Dosen. Alles zwar offiziell zulässig."
        * "Können diese Gifte potenziell auch der Gesundheit schaden? Jürgen Stellpflug erklärt: "Die können nervengiftig sein,
            die können das Hormonsystem schädigen, sie können wie Glyphosat eben auch unter Krebsverdacht stehen.
            Und sie können ausgasen, gerade durch die warme Luft, die wir im Winter im Wohnzimmer haben."
* Winterzeit - Dosenzeit => China-Doku: Pestizide an Tomaten, Spargel und Pilzen
* Taxonomie Pestizide und Wirkweisen
* Osterzeit - Reifenwechselzeit => Exkurs: Autoreifendoku: mit Herbiziden gegen Schlangen
* Aktion: verlinken unter saisonal-Rezeptvorschlägen
* Footer: wünscht euch was... um welches Pestizid oder Lebensmittel soll es in der nächsten Ausgabe gehen?
    * (Gerne auch selber etwas beitragen)
* Kartoffelzeit (Frühkartoffeln im Mai, ansonsten zwischen August und Oktober) - Diquat-Zeit
* Pestizid-App von Chemiefirmen für Landwirte
    * Noch einfacher das richtige Pestizid finden
* Bananen...
* Der Chemieunterricht war langweilig? besser lernen mit Praxis-Beispielen
* Aus den Augen, aus dem Sinn? Mit unseren Bites kratzen die Pestizide regelmäßig am Rand des Bewusstseins
* BaaS - Bio as a Service
    * für den einzelnen ist es nicht immer leicht, mittags eine warme Bio-Mahlzeit zu finden
    * hier kann die Beschaffung der Gemeinschaftsverpflegung einen wertvollen Beitrag leisten

2020
----
### Was ist Vegan? - Definition
* Eine vegane Ernährung ist Teil einer ethischen Betrachtungsweise, die in einem Tiere keine bloße Ressource sieht, sondern ein schützenswertes Mitgeschöpf
    * Diese Sicht spiegelt sich z. B. auch in §1 unseres Tierschutzgesetzes wider
        ("Zweck dieses Gesetzes ist es, aus der Verantwortung des Menschen
        für das Tier als Mitgeschöpf dessen Leben und Wohlbefinden zu schützen.
        Niemand darf einem Tier ohne vernünftigen Grund Schmerzen, Leiden oder Schäden zufügen.")
* Die Betrachtung ist überraschend undogmatisch, da sie zwar einen hohen Anspruch definiert, aber keine konkreten Handlungen vorschreibt:
    * "Veganism is a way of living which seeks to exclude, as far as is possible and practicable,
        all forms of exploitation of, and cruelty to, animals for food, clothing or any other purpose."
        (https://www.vegansociety.com/go-vegan/definition-veganism, seit 1979)
        * "soweit praktisch möglich" weist darauf hin, dass Notsituationen separat betrachtet werden
    * Der Begriff "vegan" geht auf die Gründung der Vegan Society 1944 zurück.
    * Ziel ist es systematische Gewalt gegenüber Tieren möglichst zu vermeiden. Bei der Ernährung betrachtet man dazu
        die derzeit gängige Praxis des Herstellungsprozesses von Tierprodukten aller Art:
        diese geht von Reproduktionszwang (Züchtung)
        über die typischen qualvollen Zustände in unserer industriellen Intensivtierhaltung
        bis hin zur Anwendung der ultimativen Gewalt in Form des Schlachtens im besten Alter.
        Auch die tödlichen Auswirkungen auf Meereslebewesen (Fische zum Verzehr oder Fischmehl, ungenutzer Beifang etc.) sind Teil der Betrachtung.
        Diese Praxis wird - wo möglich - abgelehnt.
    * Eine Ernährung ohne solche Tierprodukte erweist sich in unseren Breiten unter den typischen Alltagsbedingungen als praktisch möglich,
        da eine tierleidfreie Ernährung bei richtiger Ausgestaltung auch sehr gesund sein kann.
* Der konkrete Ernährungsstil reicht von einer ungesunden, einseitigen Ernährungsweise (z. B. Chips, Pommes und Gummibärchen)
    bis hin zu einem Speiseplan für Leistungssportler, die täglich und insbesondere in Wettkampfsituationen körperliche Top-Leistungen erbringen wollen (Olympia, Motorsport etc.)
* Die Ausgestaltung ist in der Praxis facetten- und abwechslungsreich, kann individuell angepasst und mit anderen bekannten Ernährungspraktiken und -philosophien
    wie Intervallfasten, Clean Eating, High Carb, Low Carb, kein zugesetzter Zucker, Slow Food etc. kombiniert werden.
* Wir interessieren uns im Rahmen der Gesundheitsvorsorge für die gesunden Varianten eines veganen Ernährungstils
    und sprechen dazu von einer **vollwertig-pflanzlichen Ernährung** (im Englischen "plant-based diet").
* Als Nebeneffekt hat ein Ernährungsstil mit möglichst hohem oder vollständig pflanzlichen Anteil ein hohes **Umwelt-** und Klimaschutzpotential
    * siehe z. B. https://de.wikipedia.org/wiki/Veganismus#Umweltvertr%C3%A4glichkeit - Treibhausgase
    * Oxford-Studie
    * ...

### Positive Vision entwickeln
* Träumen in der Gastronomie?
    * z. B. Thema Antibiotika oder Tierwohl? Was ist das beste Vorstellbare?
        * z. B. Gäste fragen solche Produkte erst gar nicht nach?
        * wie kommt man da hin?
* siehe "Obstanbau ganz ohne Chemie möglich? | ARTE Re:"

### Vegane Unternehmer: REWE, Rügenwalder
* REWE mit veganem Besitzer
    * https://www.waz.de/staedte/muelheim/warum-veganer-aus-ganz-deutschland-in-muelheim-einkaufen-id230962196.html, Jahr?

* Rügenwalder
    * https://www.ruegenwalder.de/, Dezember 2020
        * Spot "Fleisch aus Pflanzen"
        * Weihnachtsrezept: Königsberger Klopse mit veganem Hack
        * Vegetarische und Vegane Rezepte

### Bio-Obst vom Bodensee biozyklisch-vegan
* siehe "" dort, BayWa Obst

### Oxford-Studie - Kommunikation etc.
* https://science.sciencemag.org/content/360/6392/987 - "Reducing food’s environmental impacts through producers and consumers", 2018
    * Joseph Poore and Thomas Nemecek
    * "Poore and Nemecek consolidated data on the
        multiple environmental impacts of ∼38,000 farms
        producing 40 different agricultural goods around the world
        in a meta-analysis comparing various types of food production systems."
    * "**Impact can vary 50-fold among producers of the same product**, creating substantial mitigation opportunities."
        * ...
    * "Most strikingly, impacts of the lowest-impact animal products typically exceed those of vegetable substitutes,
        providing new evidence for the importance of dietary change."
    * ...
    * Consumers
        * "Today, and probably into the future, **dietary change can deliver environmental benefits on a scale not achievable by producers**. ::human_attitudes
            Moving from current diets **to a diet that excludes animal products** has transformative potential, reducing food’s land use by [...]"
        * "Consumers can play another important role by avoiding high-impact producers.
            **We consider a second scenario** where consumption of each animal product is halved by replacing production with above-median GHG emissions with vegetable equivalents."

            * -> "**Communicating average product impacts to consumers enables dietary change** and should be pursued."
            * "Though dietary change is realistic for any individual,
                widespread behavioral change will be hard to achieve in the narrow timeframe remaining to limit global warming
                and prevent further, irreversible biodiversity loss.
                Communicating producer impacts allows access to the second scenario, which multiplies the effects of smaller consumer changes."

    * Summary article: http://www.ox.ac.uk/news/2018-06-01-new-estimates-environmental-cost-food
        * schöne **Protein**-Graphik, die auch zeigt Kuhmilch vs. Sojamilch zeigt
        * "**Animal product free diets, therefore, deliver greater environmental benefits than purchasing sustainable meat or dairy.**"
            * ...
        * Second scenario
            * "**Reducing consumption of animal products by 50% by avoiding the highest-impact producers**"
            * ...
            * "However, this scenario **requires communicating producer (not just product) environmental impacts to consumers**.
                This could be through environmental labels in combination with taxes and subsidies."
        * "We need to **find ways to slightly change the conditions so it’s better for producers and consumers**
            to act in favour of the environment,' says Joseph Poore. 'Environmental labels and financial incentives
            would support more sustainable consumption, while creating a positive loop [...]"

* (Luxus-Frage? => Fokus auf Produktionsbedingungen und -auswirkungen, nicht Genuss an sich)

### 2020: Oxford-Studie - Graphik: lokal vs. Food-Choice, Transportweg vs. tierisch
* "You want to reduce the carbon footprint of your food? Focus on what you eat, not whether your food is local"
    * https://ourworldindata.org/food-choice-vs-eating-local, 2020
    * based on study by Joseph Poore and Thomas Nemecek
    * **Graphik: "Food: greenhouse gas emissions across the supply chain"**
        * Beef
        * Lamb
        * Cheese
        * Beef (dairy herd)
        * Chocolate
        * Coffee
        * Prawns (Shrimps)
        * Palm Oil
        * Pig Meat
        * Poultry Meat
        * Olive Oil
        * Fish (farmed)
        * Eggs
        * Rice
        * Fish (wild catch)
        * Milk
        * Cane Sugar
        * Groundnuts (Erdnuss)
        * Wheat & Rye
        * Tomatoes
        * Maize (Mais)
        * Cassava (Maniok?)
        * Soymilk
        * Peas
        * **Bananas**
        * Root vegetables
        * Apples
        * Citrus Fruit
        * Nuts
    * "Transport is a small contributor to emissions. For most food products, it accounts for less than 10%"
    * "Transport typically accounts for less than 1% of beef’s GHG emissions: choosing to eat local has very minimal effects on its total footprint."
    * Zahlen: "going ‘red meat and dairy-free’ (not totally meat-free) one day per week would achieve the same as having a diet with zero food miles."
    * "There are also a number of cases where eating locally might in fact increase emissions.": if not in-season
    * "The impact of transport is small for most products, but there is one exception: those which **travel by air**.", ::flugreisen
        * "Very little food is air-freighted; it accounts for only 0.16% of food miles."

### "Parkinson: Berufskrankheit bei Landwirten?"
* https://www.ndr.de/nachrichten/niedersachsen/Parkinson-Berufskrankheit-bei-Landwirten,parkinson174.html, 2019
    * in Frankreich seit 2012
* plus Geschichte vom Winzer
* https://www.sciencedaily.com/releases/2018/05/180523133158.htm, 2018
    * "Study uncovers cause of pesticide exposure, Parkinson's link"
    * "Previous studies have found an association between two commonly used agrochemicals (paraquat and maneb) and Parkinson's disease."
    * "Adding the effects of the chemicals to a predisposition for Parkinson's disease drastically increases the risk of disease onset, said Ryan."

### Gesundheit: Workshop-Ideen, 2020
siehe gesundheit-ernährung.md

### Genuss: veganer Käse aus Frankreich / Positive Emotionen nutzen
* https://srstorage01-a.akamaihd.net/Video/FS/GL-WIMS/BTRMM_gl-wims_2020-01-27_Veganer-Kaese_nosubs_P.mp4 (MediathekView), ca. 5 min, 2020
    * "ARD - Veganer "Käse" aus Cashewnüssen (27.01.2020)"
    * "Anne Guth und Yannick Fosse haben sich im Herbst 2017 mit einer kleinen Manufaktur für käseartige Produkte in Sarralbe selbstständig gemacht. Ihre vier Sorten "Petits Veganne" bestehen nicht aus Milch, sondern aus Cashewkernen. Damit ist es das erste Unternehmen in der Region, das eine solche vegane Alternative herstellt."

* VeggieWorld 2020: "Soyananda Veganes Ei & Omelette, 200g"

* https://odmdr-a.akamaihd.net/mp4dyn2/d/FCMS-d162c6f7-e3ea-4027-901b-0c990265fb0e-be7c2950aac6_d1.mp4 (MediathekView), 6 min, 2020
    * "Veganuary: Vegan kochen und essen mit Anne Menden und Patrik Baboumian", ARD Brisant
    * "Der Veganuary ist in vollem Gange: 31 Tage vegane Ernährung - Natur, Klima und der Gesundheit zuliebe.
        Eine Kampagne, die auch Anne Menden und Patrik Baboumian unterstützen - u.a. mit einem "Chili con Tofu"-Rezept."
    * Gesundheit
    * Kraft
    * Prominente
        * Anne Menden
            * "es ist halt auch eine sehr umwelt- und tierfreundliche Art zu genießen"
            * SeaSheperd
            * Vorurteile abbauen
    * Beispiel einer 50-köpfigen Büro-Crew, die in Berlin Caterings macht => machen alle (auch privat) beim Veganuary mit
        * Rezepte austauschen

### Gefühle / Männer-Angst vor Tofu / Lachs
* https://www.wiesbadener-kurier.de/panorama/leben-und-wissen/vegetarischer-sternekoch-paul-ivic-der-gemusemann_20538057#, 2019
    * "Vegetarischer Sternekoch Paul Ivic: Der Gemüsemann"
    * "Wer in Wien nur Schnitzel und Tafelspitz isst, verpasst ein kulinarisches Abenteuer: Im Restaurant „Tian“ kocht Paul Ivic ohne Fleisch – auf höchstem Niveau."
    * " Es war eine Schnapsidee, auf die Paul Ivic durch den Dokumentarfilm „Super Size Me“ kam. Wie der Protagonist ernährte er sich nur von Junkfood.
        Eine Woche lang. Aus dem Experiment wurde eine Gewohnheit, drei Monate, in denen er ausschließlich Tiefkühl-Pizza, Wurstbrote und Chips aß –
        aus seinen 78 Kilogramm wurden 99. „Ich wurde tierisch faul, meine Laune war unterirdisch“, sagt er.
        Bis ein Mitarbeiter zu ihm sagte: „Chef, du bist so fett und schlecht gelaunt.“"
    * "Es sei in den Restaurants, die er leitete, nur noch um den Profit gegangen, sagt der heute
        40-Jährige: „Hauptsache, der Preis hat gestimmt. Ich hatte mich schon geekelt in der Küche.
        Ich wusste ja, woher der Pangasius kam, in welcher Kloake die Garnelen schwammen.“"
    * "Und mir wurde klar, warum ich eigentlich Koch geworden bin. Nämlich, um den Menschen ein richtiges Essen zu servieren. Meine Aufgabe ist es, dem Kunden zu sagen, dass er bei mir keinen **Lachs** bekommt, weil das einer der giftigsten Fische der Welt ist.“"
        * siehe auch: https://istdasvegan.eu/2018/05/die-gesundheitsluege-lachs-ist-eines-der-giftigsten-lebensmittel-der-welt/
            * Link zur Doku: "Lachs und Avocado für Europa - Die dunkle Seite von Trend-Food", 2019, ARD
                * https://www.ardmediathek.de/ard/player/Y3JpZDovL2Rhc2Vyc3RlLmRlL3dlbHRzcGllZ2VsLzFiMjdiMmJmLWQ5NTktNDM2OS05Y2FjLThjZTUxZjA5MDMwZQ/
                * "Die Nachfrage nach Lachs und Avocado in Europa ist immens. Doch in Lateinamerika hat dieser Boom nicht selten brutale Auswirkungen:
                    Ausbeutung von Natur und Menschen, damit in der nördlichen Hemisphäre der Tisch gedeckt ist."
                * ...
        * https://www.maennersache.de/lachs-ist-das-giftigste-lebensmittel-der-welt-5720.html
    * "Das Gemüse, für viele Menschen Beilage, ist hier der Star."
    * „Man braucht eine gute Ausdauer und Konsequenz. Die Gäste dachten, wir servieren ihnen Tofu. **Vor allem die Männer hatten Angst**“
    * "Auf einmal war da ein Koch, der darauf Wert gelegt hat, dass **das Gemüse höchste Qualität hat**.
        Inzwischen hat das Tian mehr als 20 unabhängige Kleinbetriebe, von denen es beliefert wird."
    * "Der Eigentümer, Christian Halper, Gastronom, Millionär und Vegetarier, wollte ein Restaurant, in dem es Gerichte aus Gemüse, Obst und Getreide gibt – auf Sterneniveau."
        * "Ivic wollte keinen Fleisch- oder Fischersatz auf den Tellern haben.
            „Viele Rezepturen waren dann auf einmal wertlos. Ich musste das Kochen noch mal neu denken. Man muss da ganz von vorne anfangen.“"
    * "„Wir sind so erzogen, dass uns über Jahre eingetrichtert wurde, dass nur ein Fleisch eine anständige Mahlzeit ist.“"
    * "Schmeckt es? Hat es genug Tiefe? Löst es eine positive Emotion aus?"
        * "Ivic ist einer, der alles kritisch hinterfragt: Gojibeeren, Avocados, industrielle Produkte sowieso. All das kommt im Tian nicht in die Töpfe."
        * "Der Wareneinsatz ist bei rein vegetarischer Küche nicht unbedingt geringer: „Ich kaufe auch mal einen Spinat
            um 20 Euro das Kilo, der eben ohne Herbizide und Pestizide gezogen wurde."
        * "„Qualität ist es, die sich durchsetzen muss.“"

### Qualitative Einschätzung: Umweltvorreiter-Kantine
* Frage: Was ist für Sie eine Umweltvorreiter-Kantine?
    * **Bio:** Die landwirtschaftlichen Rohstoffe stammen, wenn möglich, aus kontrolliert biologischen Anbau, solange es noch keine breit angelegte Agrarwende gibt
        * Eine Auszeichnung für den Gast ist hier weniger wichtig, als ein möglichst hoher Anteil im Einkauf
        * wegen **Artenvielfalt / Artensterben**
        * (Faustregel: Was gesund ist, ist auch nachhaltig (außer, wenn es von weit her kommt)?)
    * Es gibt eine rein pflanzliche Menülinie mit folgenden Eigenschaften... (**vegan**)
        * täglich verfügbar
        * bei täglicher Wahl ausgewogen und gesund ("vollwertig")
        * Haupt-Zielgruppe: "Flexitarier:innen" (Hauptanteil Frauen)
        * auch für vegan lebende Menschen (eher jung und weiblich) einfach ersichtlich und täglich wählbar
            * jung und weiblich siehe z. B. Foto hier: https://de.veganuary.com/about => **Diversity**
        * Vorteile
            * **Diversity**-Aspekt: nicht nur aus ethischen Gründen, sondern auch religiöse Anforderungen (wie z. B. kein Schweinefleisch oder halal) sind damit abgedeckt
            * siehe veganuary
    * **Leitungswasser** als Durststiller Nr. 1 ist einfach ersichtlich zu bekommen
        * (ggf. aufbereitet)
        * sehr nachhaltig und auch gesund
    * **Preisgestaltung** orientiert sich auch an den externen Umweltkosten
    * **Ampel / Bewusstseinsbildung**
        * 1. es ist leicht ersichtlich, welche Gerichte - bei täglicher Wahl - einen großen oder kleinen Fußabdruck hinterlassen (CO2, Wasser, Regenwald, Tierwohl)
        * 2. ggf. dasselbe für den Gesundheitswert: z. B. Zuckergerichte sollten als nicht täglich empfohlen sichtbar sein
    * **Mut zur Transparenz** (auch zur Bewusstseinsbildung)
        * dazu gehört auch pro-aktiver Umgang mit Nicht-Wissen
        * Herkunft / **China**?
    * **Kreativität**: Rezept-Reichtum im Bereich pflanzlicher Gerichte
    * **Tierische Produkte**
        * stammen aus nachvollziehbaren Quellen
        * stammen zumindest aus Quellen, die über dem deutschen Durchschnitt liegen (Intensivtierhaltung, enge Käfige etc.)
            * **Antibiotika**
            * Wo kommt die **Gülle** hin? **Dünger** vs. Trinkwasserqualität
            * **Tierwohl**
            * **Lebensmittelskandale** (z. B. Wilkes Wurst beliefert auch Kantinen)
        * werden in möglichst geringen Maßen eingesetzt, um **Lebensmittelverschwendung** zu vermeiden
        * Eier
            * keine Rezepte mit unsichtbaren verarbeiteten Eiern
            * sichtbare Eier min. Freiland, besser Bio
        * Milch
            * Milch und Sahne als reinen Geschmacksverstärker durch pflanzliche Alternativen ersetzen
            * Milchprodukte (Milch, Käse, Sahne) min. in Bio-Qualität (wegen Antibiotika, Gülle und überzüchteten Eutern)
        * Fleich
            * ganzjährige Weidehaltung
            * Bio-Qualität, wenn verfügbar
        * **max 200g** Fl. / Woche laut Nachhaltigkeitsspeiseplan
    * Quellen:
        * Vorschläge aus (siehe 2016 wiss.Beirat)
        * "Eine vegane Ernährung ist eine der effektivsten Maßnahmen,
            um die Umwelt zu schützen,
            Tierleid zu vermeiden,
            den Klimawandel aufzuhalten
            und die Gesundheit von Millionen Menschen zu verbessern" (https://de.veganuary.com/about -> Spiegel.de)
        * https://uk.veganuary.com/corporate-collaborations - "Business Support Toolkit"
            * Zahlen S. 6, z. B. 2/3 Frauen, 1/3 Männer
    * Next 2021:
        * https://de.veganuary.com/workplace-challenge - Plakat aufhängen

### Die richtigen Fragen
In formalisierten, hoch-arbeitsteiligen Unternehmenskontexten ist es wichtig, die richtigen Fragen zu stellen:

1. Steht Nachhaltigkeit regelmäßig auf der Agenda? Wie oft?
2. Gibt es dedizierte Ansprechpartner für Nachhaltigkeit? Wen bzw. Wieviele?
3. (Gibt es Umwelt-Aspekte, die man sogar höher als Geld priorisieren würde?)
3a. Was genau bedeutet für Sie Nachhaltigkeit? Welche Aspekte? (z. B. "nicht allein auf der Welt")

4. Sehen Sie sich als Einkäufer von landwirtschaftlichen Produkten in der Mitverantwortung bei der Gestaltung unserer Landwirtschaft?
4a. Sehen Sie sich als Einkäufer von Tierprodukten mit in der Verantwortung, ob es sich um eher qualvolle Haltungseinrichtungen oder vorbildliche Strukturen handelt
    (ist das Maß allein Geld und Produkt-Qualität?)
5. Wie stehen Sie zu der Art biologisch zu wirtschaften, z. B. Bioland; zunächst unabhängig von der aktuellen Preis- und verfügbarkeitsstruktur?

6. Wenn Sie wollten (also den Auftrag bekämen), könnten Sie dann neue Wege bei der Beschaffung (und den Rezepten) finden?

7. Artensterben / Gülle, Trinkwasser etc. Hat das etwas mit Landwirtschaft zu tun?

x. Würden Sie sagen, Einkaufspolitik, die Umwelt- und Sozialaspekte mit berücksichtigt, ist gut für das Gemeinwohl?
x. Schlachthausbesichtigung möglich?

### Kennzeichnung: vegan nicht geschützt
* https://www.dge.de/wissenschaft/weitere-publikationen/faqs/vegane-ernaehrung/ - FAQ
    * Frage 18: "Die Bezeichnung „vegan“ für Lebensmittel ist bisher lebensmittelrechtlich nicht definiert und geschützt.
        [...] Daher ist es schwierig, verarbeitete vegane Lebensmittel genau zu identifizieren."
    * Gesundheit: "22. In anderen Ländern wird eine vegane Ernährung nicht so kritisch gesehen,
        sondern sogar für alle Altersgruppen empfohlen. Wie könnten sich die unterschiedlichen internationalen Empfehlungen für die vegane Ernährung erklären?"
        * ...Nordamerika...

### Möglichkeiten von Akteuren außerhalb des Gastro-Anbieters
* Anerkennung, dass (insb. Tier-)Landwirtschaft ein Riesenthema ist
    * "selbstlose" (also "nur" aus Umweltgründen)
        Unterstützung des Gastro-Anbieters ausloben (in Form von Weiterbildungsgutscheinen; weitere Ideen?)

### Gesundheit: Metzger wird vegan, kein Fleisch, voller Genuss, 2020
siehe gesundheit-ernährung.md

### Gesundheit: BKK Provita, 2020
siehe gesundheit-ernährung.md

### Beispiel: Golden Globes: 100% plant-based menu
* https://www.refinery29.com/en-us/2020/01/9124631/golden-globes-vegan-menu-food-2020
    * "Hollywood Foreign Press Association's decision to serve a 100% plant-based meal at the 77th Golden Globes on Sunday"
    * Twitter
        * Leonardo DiCaprio: "Thank you HFPA"
        * Mark Ruffalo: "Our industry leads by example. Vegetarian food is delicious and healthy
            and reduces green house gasses about as much as driving electric cars.
            The HFPA should be commended for this and all the other awards shows should follow suit."
        * Game of Thrones' Nathalie Emmanuel: yes
    * HFPA "planning to make this year's Golden Globe Awards more sustainable than ever, starting with an all-vegan menu"
    * "The climate crisis is impossible to ignore and after speaking with our peers, and friends in the community,
        we felt challenged to do better.
        The decision to serve an entirely plant-based meal was embraced by our partners at the Beverly Hilton,
        and represents a small step in response to a big problem."

### More on Hollywood: a powerful message
* "“First, I’d like to thank the Hollywood Foreign Press for
    recognizing and acknowledging the link between animal agriculture and climate change,”
    he said in his acceptance speech. He added that the vegan menu “really sends a powerful message.”"
    * Joaquin Phoenix, https://www.livekindly.co/oscars-party-vegan-joaquin-phoenix/

2019
----
### Gesundheit: Kanada, 2019
siehe gesundheit-ernährung.md

### Prominente
* Schwarzenegger: less meat, less heat campaign
* Lewis Hamilton: schnell, erfolgreich und vegan
    * https://www.independent.co.uk/life-style/food-and-drink/lewis-hamilton-vegan-training-formula-one-plant-based-food-a9293696.html
    * 2020 außerdem noch Black Lives Matter und das Mercedes-Team macht mit, indem der Silberpfeil schwarz angemalt wird:
        * https://www.tz.de/sport/formel-1/formel-1-mercedes-lewis-hamilton-silberpfeil-valtteri-bottas-schwarz-rassismus-zr-13815910.html

### Gesundheit: Game Changers, 2019
siehe gesundheit-ernährung.md

### Kennzeichnung: Disney Land
* https://happiestveganonearth.com/2019/10/23/your-guide-to-vegan-options-at-disneyland-october-2019-update/
    * "Very recently, they have started adding the **“plant-based”** symbol to the menus, [...] now has the **little green leaf symbol** next to it,
    indicating that it is plant-based, **which they define as: “Contains no animal meat, dairy, eggs, or honey.”**"
        * also ganz einfach

### Gesundheit mit Metzgern, 2019
siehe gesundheit-ernährung.md

### Nachhaltigkeit
* siehe "Welt im Wandel – Gesellschaftsvertrag für eine Große Transformation" / transformation.md
    * "vermehrte Aufklärungsarbeit"!
    * "Kantinen der öffentlichen Hand sollten als Vorbild ein bis zwei fleischfreie Tage pro Woche einlegen."

* Weltwetterorganisation - Problem auch Methan aus Rinderzucht, 2019
    * https://www.tagesschau.de/ausland/co2-rekordwert-athmosphaere-101.html
    * "Ein Plus gibt es zudem bei den Stickoxiden. Hier sei der massive Gebrauch von Dünger ein Grund für den Anstieg." -> Bio

### CO2-neutral: ohne Rindfleisch
* siehe "Goldsmiths, University of London"

### Studie zu Fleisch von artec
* siehe "Minna Kanerva | Meat consumption in Europe: Issues, trends and debates"

### scinexx - Umweltschutz - Verantwortung übernehmen
* https://www.scinexx.de/businessnews/umweltschutz-darum-ist-es-wichtig-verantwortung-zu-uebernehmen/
    * ...

### Maßnahmen
* "Kochausbildung zum Vegetarisch/Veganen Koch anbieten"
* "Finanzierung von pflanzlichen Bio-Gerichten zulasten von Fleischgerichten"

### Gesundheit: Schlaganfallriskio niedriger, wenn fleischlos; aber..., 2019
siehe gesundheit-ernährung.md
