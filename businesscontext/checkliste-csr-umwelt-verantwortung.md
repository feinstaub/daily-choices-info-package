Checkliste Corporate Umwelt Verantwortung
=========================================

<!-- toc -->

- [Gemeinschaftsverpflegung](#gemeinschaftsverpflegung)
- [Gerechter Welthandel](#gerechter-welthandel)
- [Reisekostenerstattung](#reisekostenerstattung)
- [Werbestrategie](#werbestrategie)
- [Werbemittel](#werbemittel)
- [Werbepartner](#werbepartner)
- [Transparenz](#transparenz)

<!-- tocstop -->

Mitarbeiterinnen und Mitarbeiter innerhalb und außerhalb der Organsisation ermutigen und ermächtigen, umwelt- und sozial-verantwortlich zu handeln.

Mit dieser Checkliste kann geprüft werden, ob die aktuell implementierten CSR-Regeln effektiv wirken.

Gemeinschaftsverpflegung
------------------------
* Regelmäßig umweltfreundliche Auswahlmöglichkeit vorhanden?
    * nach aktuellen wissenschaftlichen Empfehlungen derzeit: eine pflanzliche Option, die so ausgestaltet ist, dass man sie täglich wählen kann
        * Plus: Diversity
* Umweltfreundliche Auswahlmöglichkeit als Standard und auf Platz 1?
* Catering: Auswahl nach Umweltfreundlichkeit ist möglich, auch wenn es mehr kostet?
    * z. B. Bio/regional
    * siehe auch Welthandel
* Preisgestaltung: Umweltfreundliche Auswahlmöglichkeit spiegelt externe Kosten wider?
    * z. B. Fleisch/Fisch als Hauptkomponente teurer als pflanzliche Hauptkomponente
* Trinkwasserspender vorhanden?
    * als Alternative zu Plastikflaschen
    * als Alternative zu zuckrigen "Softdrinks"
    * im öffentlichen Bereich: "in den zunehmend heißen Sommermonaten können sie einen Beitrag zur Gesundheit leisten und zum Trinken von Wasser statt (Süß)getränken in Plastikflaschen einladen"
        * "In Gebäuden seien Trinkwasserspender einfacher zu realisieren als im öffentlichen Raum", wegen Hygienevorschriften
* Ist es möglich, Transparenz zu leben?
    * Beispiel: im Fall von Tierprodukten: dürfen Informationen über Antibiotika-Verbrauch oder Bilder von Tierfabriken gezeigt werden?

Gerechter Welthandel
--------------------
* Wenn Kaffee angeboten wird, ist es möglich, Fair Trade auszuwählen?
    * Hintergrund: Weltmarktpreise sind wegen Überangebot regelmäßig im Keller und setzen die Bauern unter Druck
* Wenn kakao-haltige Produkte angeboten werden (z. B. Schokolade), ist es möglich Fair Trade auszuwählen?
* Ist es möglich Schokolade auszuwählen, die im Herkunftsland komplett hergestellt wurde?
    * siehe https://fairafric.com/de/
        * Wertschöpfung der Schokoladenproduktion nach Afrika verlegen.
        * Video
            * bean-to-bar
            * What money is left in Ghana for a 100 g chocolate bar?
                * conventional: 0,075 $
                * fair-trade:   0,080 $
                * fairafric:    0,700 $
        * Warum? https://fairafric.com/de/impact-2, Sept. 2019
            * "Qualifizierte Arbeitsplätze"
            * "Besserer Zugang zu Gesundheitsfürsorge"
            * "Bildung"
            * "Gleichberechtigung"
            * "Fähigkeiten"
            * "Nachhaltigkeit"
        * Bio, https://fairafric.com/de/bio
            * "Übrigens: Aktuell (Stand Mai 2019) trägt unsere fairafric-Schokolade kein Bio-Siegel. Hintergründe dazu findet ihr hier."
                * https://fairafric.com/de/was-bedeutet-eigentlich-bio, 2018

Reisekostenerstattung
---------------------
* Ist es möglich, der Umwelt Priorität vor als Zeit und Geld einzuräumen?
    * z. B. Zug statt Flugzeug

Werbestrategie
--------------
* Dürfen die Werbebotschaften so konzipiert sein, dass sie nicht auf maximalen Konsum abzielen?
    * Hintergrund: je mehr Konsum, desto schlechter für die Umwelt
    * Konsum wird durch das Erzeugen von Bedarfen erzeugt, die vorher nicht da waren
* Dürfen die Werbebotschaften so gestaltet werden, dass sie keine neuen Bedürfnisse wecken?
    * Anti-Beispiel: "Wie wärs mal wieder mit einer Städtereise?"
    * Beispiel: "Sie planen eine Städtereise mit dem Auto? Dann haben wir was besseres für Sie."
* Dürfen die Werbebotschaften ohne milde Formen der Nötigung gestaltet werden?
    * Anti-Beispiel: "Beeilen Sie sich, Produkt x zu kaufen oder Service y in Anspruch zu nehmen, denn dann kriegen Sie es billiger"
    * Beispiel: "Hinweis: Wir bieten x, y, z an. Wir ermutigen Sie aber, zuhause einen Waldspaziergang zu machen"
* Ist Information vor ungewollter Kundenbindung möglich?
    * Anti-Beispiel: "Ihr Abo wurde soeben verlängert. Nächste Möglichkeit der Kündigung ist in einem Jahr."
    * Beispiel: "Ihr Abo verlängert sich nach Ablauf von 8 Wochen. Sie können innerhalb der nächsten 4 Wochen kündigen, wenn Sie den Dienst nicht mehr benötigen"

Werbemittel
-----------
* Bei essbaren Geschenken: pflanzlich? Vorbildlich vor konventionell? Fair vor unfair?
    * Umwelt
    * Diversity

Werbepartner
------------
* Auswahl nach Nachhaltigkeitsrenomee möglich?
    * z. B. Coca-Cola fällt raus

Transparenz
-----------
* Gibt es eine konzernintern dauerhaft einsehbare Liste an Umwelt-Pain-Points?
    * so dass Mitarbeiter:innen wissen, wo Bedarf an Engagement besteht
