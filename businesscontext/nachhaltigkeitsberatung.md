Nachhaltigkeitsberatung
=======================

<!-- toc -->

- [2020 kontinuierliche Kommunikation und Austausch; beyond tech, beyond to-do-list](#2020-kontinuierliche-kommunikation-und-austausch-beyond-tech-beyond-to-do-list)
- [Beratungsangebot 2019](#beratungsangebot-2019)

<!-- tocstop -->

### 2020 kontinuierliche Kommunikation und Austausch; beyond tech, beyond to-do-list
* Wenn Nachhaltigkeit nicht nur als Item auf der to-do-Liste angesehen wird... was dann?
    * bzw. Was kann das Unternehmen in seiner **Verantwortung** für Umwelt und Gesellschaft beitragen?
    * Kommunikation und Austausch ermöglichen
    * **Kontinuität** - mit dem sich ändernden Bewusstsein werden auch die Maßnahmen kontinuiert angepasst
        * Liste von bisherigen Maßnahmen
            * auch die, die weniger erfolgreich waren --> nach einer gewissen Zeit re-evaluieren
                * Was früher nicht ging, geht vielleicht heute.
            * Was hat funktioniert? Was nicht? Warum?
        * Mut zur "Black list"? -> Was liegt offensichtlich im Argen und erfordert Lösungen?
    * **Austausch**
        * Was können wir von anderen lernen?
        * Was können andere von uns lernen?
            * intern
            * Ausstrahlkraft über Unternehmensgrenzen hinweg
        * **Bausteine** / Vorbilder sammeln und (re)kombinieren
            * Welche Bausteine gibt es? -> Welzer, Schneidewind
    * Welche **Felder** sind zu beachten?
        * Physikalische Grenzen
            * CO2, Methan und andere Treibhausgase (-> Klima)
                * Transportwege (wo kommen Rohstoffe und Produkte her?)
            * Flächenverbrauch (-> Anbau von Nahrung vs. andere Nutzung), ::flächenfraß
                * Landnutzungsänderung (= zerstörung) (-> Ökosysteme, Insektensterben, Artenvielfalt)
                    * Insektenatlas
                * Reduzierung der Waldflächen (-> Papier, Möbel, Soja)
                * Lebensmittelverschwendung
            * Müll-Output / Elektroschrott
                * chemische Verschmutzung der Umwelt inkl. Meere
            * Süßwasser
            * Biochemische Flüsse (Phosphor, Stickstoff) (-> welche Art der Landwirtschaft fördern wir?)
            * Plastik im Ozean
            * -> nachhaltige Beschaffung
            * Über Nachhaltigkeit hinaus Richtung Regeneration denken
            * siehe Zusammenschau von XR und Paech, siehe auch "Earth System Trends"
                * XR: https://www.youtube.com/watch?v=lnPAL4TIAzQ
                    * https://www.gwdg.de/
                        * Folien und Script: https://owncloud.gwdg.de/index.php/s/gN1gW0uY4lL9Gk1
                            * Verschmutzung, Unfruchtbare Böden, Artensterben, Extremwetterereignisse
                            * Gesellschaftlicher Kollaps
                            * Wir brauchen keine Hoffnung, sondern Mut
                * Paech: siehe https://www.youtube.com/watch?v=0xR2JeOpzug
                    * Ablasshandel aufdecken
                    * 2-Grad-Klimaziel
                    * welche materiellen Freiheiten darf sich ein Indiviidum leisten, um nicht ökologisch und sozial (globale Gerechtigkeit) über seine Verhältnisse zu leben?
                        * nicht "zumutungsfrei" (= grünes Wachstum, "wasch mir den Pelz, aber mach mich nicht nass") zu erreichen
                        * vgl. greffrath: Ungerechtigkeit mit Wachstum überdecken
                        * (Beispiel Bio-Baumwolle: super, ist ja jetzt alles aus Bio-Baumwolle, da kann ich nun endlich mit ruhigem Gewissen einkaufen)
                        * Selbstbegrenzung
                    * (Wo ist die Zusammenschau?)
        * Soziale Gegebenheiten / Normen / Gerechtigkeit
            * z. B. Entsorgung Müll
            * z. B. Lieferketten; Arbeitsrecht, Arbeitsbedingungen am Ende der Kette
            * Paech
                * wer darf sich wieviel nehmen?
                * Technischer Fortschritt vs. Kultureller Wandel (inkl. technischem Fortschritt)
                    * Kreise: Soziales, Ökonomie, Ökologie
                        * 1. gleichberechtigt mit Schnittmengen (wobei Ökonomie expandiert)
                        * 2. Ökologie außenrum, darinnen Soziales und dann Ökonomie (wobei die kontrahiert)
                    * Was ist das gute und verantwortbare gute Leben?
            * Paech siehe https://www.youtube.com/watch?v=-NXy7bqSzVU, 2015
                * "Wir haben ein ökologisches und ein soziales Problem."
                    * "Denn wenn klar ist, dass die Begrenztheit des Planeten es nicht erlaubt, jeden beliebigen Lebensstil zu praktizieren, dann haben wir ein Verteilungsproblem"


### Beratungsangebot 2019
Pitch "Unternehmensinterne Nachhaltigkeitsberatung"

Relevante Abteilungen fragen

1. Wie hoch schätzen Sie die Wichtigkeit von Nachhaltigkeit ein?
2. Wie hoch schätzen Sie das derzeitige Engagement ihrer Gruppe ein?
3. Denken Sie alle Möglichkeiten ausgeschöpft zu haben?
4. Folgende Sektoren sind klimarelevant: Energie, Mobilität (siehe dazu ::mobilität), Landwirtschaft/Ernährung, ...
    In welchen aktiv?
5. Wieviel Geld Sie für eine ganzheitliche Klimaschutzberatung, die effektiv umsetzbare Maßnahmen liefert, aufbringen würden.

Anzahl angefragt: x
Anzahl Rückmeldungen: y
Max/Min/Durchschnitt: z EUR / Jahr

Beantwortet werden soll:

* Lohnt sich eine Nachhaltigkeitsberatung?
* Wie wichtig ist Nachhaltigkeit?
* Wieviel Geld sind Verbesserungen in diesem Bereich wert?

Mitarbeitern helfen, die denken "Ich würde gerne xyz mehr für Nachhaltigkeit
tun - z. B. bestimmte Sachen nicht bestellen", aber sich nicht trauen.
