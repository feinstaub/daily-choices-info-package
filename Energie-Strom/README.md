Energie, Strom / daily-choices-info-package
===========================================

[zurück](../../..)

<!-- toc -->

- [Promote Solar 2020](#promote-solar-2020)
- [Solar-Strategie](#solar-strategie)
  * [SolarStrategie 2020](#solarstrategie-2020)
  * [Tony Seba, CleanDisruption](#tony-seba-cleandisruption)
  * [Energiewende auf dem Bierdeckel](#energiewende-auf-dem-bierdeckel)
  * [Frage: Kann man mit nur Solar den Energiebedarf decken?](#frage-kann-man-mit-nur-solar-den-energiebedarf-decken)
  * [Vorreiter-Beispiele](#vorreiter-beispiele)
- [Atomstrom / Atomkraft / Kernenergie](#atomstrom--atomkraft--kernenergie)
  * [Schiffsinhaber-Erzählung](#schiffsinhaber-erzahlung)
  * [Reichweite von Uran höchstens 80 - 200 Jahre](#reichweite-von-uran-hochstens-80---200-jahre)
  * [Umwelt](#umwelt)
  * [Abrüstung, Atomwaffen](#abrustung-atomwaffen)
  * [Klimaretter?](#klimaretter)
  * [Versicherbarkeit, Unversicherbarkeit](#versicherbarkeit-unversicherbarkeit)
  * [Wirtschaftlichkeit](#wirtschaftlichkeit)
  * [AtomkraftwerkePlag](#atomkraftwerkeplag)
  * ["Die Kraftwerke wurden doch schon gebaut... abschalten ist Verschwendung"](#die-kraftwerke-wurden-doch-schon-gebaut-abschalten-ist-verschwendung)
  * [Initiativen rund um den Ausstieg aus Atomkraft](#initiativen-rund-um-den-ausstieg-aus-atomkraft)
  * [Geschichte](#geschichte)
- [Kohlestrom](#kohlestrom)
  * [2020: Datteln 4](#2020-datteln-4)
  * [Sammlung zu Kohlestrom und Kohleabbau](#sammlung-zu-kohlestrom-und-kohleabbau)
  * [Ausstieg sozialverträglich gestalten](#ausstieg-sozialvertraglich-gestalten)
- [Zielkonflikte: Windkrafträder: Naturschutz vs. Klimawandel](#zielkonflikte-windkraftrader-naturschutz-vs-klimawandel)
- [Fazit](#fazit)

<!-- tocstop -->

Promote Solar 2020
------------------
* siehe SolarStrategie, Metropolsolar
    * Link zum Dokument fehlt noch, aber eingebettetes Video
        https://energiewende-rocken.org/solarstrategie-die-intelligente-umsetzung/ (21 min)
        * Sonne
        * nötige Fläche in D.
        * Ausbaupfad Solar (exponentiell anstreben)
        * ...
* "10-Punkte-Plan für eine Solaroffensive"
    * http://www.umweltinstitut.org/aktuelle-meldungen/meldungen/2020/klima/so-bekommen-wir-die-daecher-voller-solaranlagen.html
        * http://www.umweltinstitut.org/fileadmin/Mediapool/Bilder/02_Mitmach-Aktionen/67_Geht_uns_aus_der_Sonne/10-Punkte-Plan_Solaroffensive.pdf
* siehe Tony Seba, CleanDisruption

Solar-Strategie
---------------
### SolarStrategie 2020
* http://www.metropolsolar.de/
    * ...

* PDF
    * Autonomie
    * ...

* Solarzellen umweltschädlich?
    * https://www.ingenieur.de/technik/fachbereiche/energie/chinesische-solarzellen-verheerende-umweltbilanz/
        * „Die hierfür nötige Energie spielt eine Solaranlage erst nach einem dreiviertel Jahr wieder ein“ - "erst"?
* Solarzellen Materialverfügbarkeit?
    * https://de.wikipedia.org/wiki/Solarzelle#Materialverf%C3%BCgbarkeit
* Schwarzmaler?
    * vergleiche Talks von Schwarzenegger, Steve Jobs etc.
* Sonstiges
    * Mont Pèlerin Society
        * https://de.wikipedia.org/wiki/Mont_P%C3%A8lerin_Society
        * https://www.montpelerin.org/
    * https://de.wikipedia.org/wiki/Manfred_Fischedick
        * https://wupperinst.org/en/topics/energy/renewables/

### Tony Seba, CleanDisruption
* "Clean Disruption of Energy and Transportation - Book Trailer - Tony Seba" (https://www.youtube.com/watch?v=bmnSFdk5ISg), 4 min, 2014
    * "The industrial age of energy and transportation will be over by 2030. Maybe before.
    Exponentially improving technologies such as solar, electric vehicles, and autonomous (self-driving)
    cars will disrupt and sweep away the energy and transportation industries as we know it."
    * Silicon Valley
    * Electric cars are computers on wheels

* "#CleanDisruption and the Collapse of the Oil, Coal & ICEV Industries. #1stWCWeC #EarthDay2020", https://www.youtube.com/watch?v=O-kbzfWzvSI, 60 min, 2020
    * New York Pferde -> Autos in 13 years
        * Transportation Disruption => Food Disruption: eating horse meat
    * Predictions of exponential shifts often fail inkl. Beispiele
    * Technology cost curves / Beispiele: Computing, Data Storage, Li-ion-Batteries
    * Market/System dynamics (S-Curve) / Beispiele: Color TVs, Car market share, Shipping container port
    * Technology Convergence / Beispiel: Smartphone
    * Business model innovation / Beispiel: Uber vs. US Taxi Industry
        * Product innovation
        * Disruption accelerators
    * Market Trauma
        * Market Share Fallacy
    * 2019 Key technologies for Clean Disruption of Energy and Transportation
    * Projected cost of Li-ion Batteries $/kWh
        * Tesla's South Australia Battery Disrupting Gas FACS (Frequency Control and Ancillary Services) market
    * decentralized batteries vs. inefficient central grid that works like a just-in-time supply chain without inventory
    * EV disruption (eletric vehicles)
        * EV 10x cheaper to fuel/charge than ICE (internal combustion engines)
        * EV 10x cheaper to maintain (18 moving parts vs. 2000 moving parts); Tesla infinite mile warranty
        * EV 2,5x lifetime; insb. für LKW; Amazon schafft 100.000 Electric Vans an
        * Grid services
    * Autonomous Vehicles
        * all you need is _one_ company to be ready => market is appearing!  (like iPhone)
        * Safety? -> Tesla Autopilot (2019) 6x safer than average car
        * autonomous everything on wheels
    * 35:00 Disrution of Transportation
        * ...
        * ... TODO
        * ...

### Energiewende auf dem Bierdeckel
* siehe youtube metropolsolar

### Frage: Kann man mit nur Solar den Energiebedarf decken?
* ...bzw. wie groß darf der Energiebedarf dann sein?
* siehe Solar-Stragie
* siehe Paech?
* Preis Solar am günstigsten; dicht gefolgt von Windkraft an Land

### Vorreiter-Beispiele
* Positiv-Kriterium: Möglichst lokale Erzeugung
    * **Postives Beispiel**: https://buergerwerke.de
        * Genossenschaftsstrom aus der Region auswählen und als neuen Stromanbieter auswählen
* Regionale Energiegenossenschaften
    * Erneuerbare Energien
    * Regionales Wirtschaften


Atomstrom / Atomkraft / Kernenergie
-----------------------------------
### Schiffsinhaber-Erzählung
* siehe z. B. Schiffsinhaber-Erzählung, siehe wirksamkeit-des-einzelnen.md / Die Verantwortlichkeit der Intellektuellen

### Reichweite von Uran höchstens 80 - 200 Jahre
* https://de.wikipedia.org/wiki/Uranwirtschaft#Reichweite_der_Uranvorr%C3%A4te, 2019, 12 min
* "Atomkraft jetzt! Rettung für das Klima? | Harald Lesch", https://www.youtube.com/watch?v=qdAH4019or0, 2019, 12 min
    * grob 200 Jahre aktuell
    * Ausbau x20 => 10 Jahre Reichweite
    * Neuer Reaktortyp - Laufwellenreaktor, Bill Gates - soll Uran 238 (von dem viel mehr da ist) können. Brutreaktor.
        * Nachteile
            * Flüssiges Natrium (statt Wasser) als Moderator (Unfälle führen zu schwer löschbaren Bränden)
            * Plutonium wird erzeugt
            * Es gibt immer noch kein Endlager
                * https://www.heise.de/newsticker/meldung/Weltweit-existiert-noch-kein-Endlager-fuer-Atommuell-4585214.html, 2019
                * siehe Film Into Eternity: https://www.videoproject.com/Into-Eternity.html
        * Abwägung: Nachteile besser als Klimakrise?
        * Noch im Versuchsstadium / nicht praxistauglich
    * Reaktor ist staatlich gelenktes Großprojekt
        * zentral vs. dezentral insbesondere in Entwicklungsländern
    * MIT-Studie zur Wirtschaftlichkeit
        * Einkaufspreise für Energieversorger für 1 kWh
            * Windstrom:  4 Cent, Tendenz fallen
            * Solarstrom: 7 Cent, Tendenz fallen
            * Atomstrom: 10 Cent, Kosten steigen (inkl. Subventionen)
    * Dezentral + Energiespeicher

### Umwelt
* z. B. http://www.umweltinstitut.org/themen/radioaktivitaet/radioaktivitaet-uebersicht.html
* Sicht der Physiker
    * 2011
        * http://www.th.physik.uni-bonn.de/th/People/dreiner/fukushima.pdf
    * ...
* http://www.bpb.de/apuz/222978/aufstieg-und-niedergang-der-atomkraft-in-der-westlichen-welt?p=all
    * "Utopie ohne Ökonomie: Aufstieg und Niedergang der Atomkraft in der westlichen Welt"
    * ...

### Abrüstung, Atomwaffen
* z. B. http://www.umweltinstitut.org/themen/radioaktivitaet/atomwaffen.html

### Klimaretter?
* http://www.umweltinstitut.org/themen/radioaktivitaet/klimaretter-atomkraft.html
    * Ausführliche Hintergrundinfos: http://www.umweltinstitut.org/themen/radioaktivitaet/klimaretter-atomkraft/hintergrundinformationen.html
        * ...
        * "Eine 2007 veröffentlichte Studie des Ökoinstituts Darmstadt kommt zu dem Schluss, dass Atomstrom mehr Emissionen verursacht als Strom aus erneuerbaren Energien."
        * "Klimawandel selbst verbietet Ausbau": wegen Kühlwasser und möglichen Überschwemmungen an Küsten
        * Unwirtschaftlichkeit

* Gegenstandpunkt: https://www.cicero.de/wirtschaft/atomkraft-kernenergie-energieversorgung-energiewende
    * ...
    * Endlager gibt es auch für andere hochgiftige chemische Produkte
        * https://www.chemie.de/lexikon/Endlagerung.html
    * ...
    * TODO
    * ...
    * "Buße leistet man durch Verzicht. Das ist die Richtung, in die sich die Greta-Jugendbewegung orientiert, im Sinne von: So wie wir leben, kann unser Planet uns nicht ertragen, wir müssen unser Leben von Grund auf ändern. Und da zucke ich als Osteuropa-Historikern, die ich auch bin, sofort zusammen."
        * "Das wird nicht gutgehen, ihr bekommt das ohne Zwang nicht hin. Schaut euch an, was in unserer Geschichte aus Projekten zur Großtransformation der Gesellschaft geworden ist: Die Schaffung des neuen Menschen, der total anders lebt und sich selbst befreit, das alles hatten wir schon"
            * Ist das wirklich das Anliegen (bzw. alternativlose Folge) der Menschen, die sich für Nachhaltigkeit einsetzen?
    * ...
    * User comments
        * https://nuklearia.de/wir-ueber-uns/ - "Die Nuklearia sieht in der Kernenergie eine wesentliche Säule der Energieversorgung."
            * https://nuklearia.de/2016/10/27/neue-energie-warum-eine-echte-energiewende-widerspruch-braucht/
                * https://www.herder-institut.de/institut-personal/personal/personen/person/wendland.html

### Versicherbarkeit, Unversicherbarkeit
* http://de.atomkraftwerkeplag.wikia.com/wiki/Versicherbarkeit_von_Atomkraftwerken
* aus den Bedingungen einer Kfz-Versicherung
    * "Was ist nicht versichert? ... Kein Versicherungsschutz besteht für Schäden durch Kernenergie."

### Wirtschaftlichkeit
* interessante Punkte: https://de.wikipedia.org/wiki/Kernenergie#Wirtschaftlichkeit
    * ...
    * https://de.wikipedia.org/wiki/Kernenergie#Volkswirtschaftliches_Schadensrisiko_und_unzureichende_Haftpflichtversicherung
        * "„Der Vorteil der Haftungsbegrenzung besteht für den Betreiber einer nuklearen Anlage darin, eine wirtschaftliche Planung vornehmen zu können. Damit soll auch gewährleistet sein, dass nukleare Aktivitäten stattfinden. Es ist allen Fachleuten klar, dass im Falle eines nuklearen Unfalls die Haftungsbegrenzungen nicht ausreichen, und die einzelnen Staaten öffentliche Gelder dazu verwenden müssen, um die entstanden Schäden zu ersetzen.“"

### AtomkraftwerkePlag
* http://de.atomkraftwerkeplag.wikia.com/wiki/Aktuelles

### "Die Kraftwerke wurden doch schon gebaut... abschalten ist Verschwendung"
* Argument: "Die Kraftwerke wurden doch schon gebaut und das Atommüllproblem haben wir ja jetzt so oder so. Das wäre doch Verschwendung sie jetzt abzustellen"
    * Die Kosten (inkl. sicherer Lagerung des neuen Abfalls), die anfallen, werden hauptsächlich von der zukünftigen Generation getragen. Das ist ungerecht.
        * die Kosten, die die aktuell junge Generation mit der Endlagersuche etc. hat, sind schon die Folgen der Aktionen der Vorgeneration
    * Es gibt jetzt schon potentiell Mauscheleien bei der Entsorgung: Stichwort "Freimessen".
    * Bei Weiterbetrieb wird jährlich neuer Müll erzeugt
        * http://www.kiefermedia.de/fakten/atommuell (bis 1995 im Meer verklappt, dann verboten)
        * https://atomkraftwerkeplag.wikia.org/de/wiki/Problem_Atomm%C3%BCll
    * Atomkraft ist in der Summe die teuerste Technik (vergleiche Lage in Frankreich; alt gewordene Technik)
    * Entsorgungsstrategien: https://de.wikipedia.org/wiki/Radioaktiver_Abfall#Entsorgung
        * unter anderem Waffenproduktion
        * Probleme mit dem [Vorschlag Weltraumentsorgung](https://de.wikipedia.org/wiki/Radioaktiver_Abfall#Entsorgung_im_Weltraum)
    * [Anfallende und angefallene Mengen](https://de.wikipedia.org/wiki/Radioaktiver_Abfall#Anfallende_und_angefallene_Mengen)
        * "Nach Angaben der World Nuclear Association entstehen Jahr für Jahr 12.000 Tonnen hochradioaktive Abfälle. Bis Ende 2010 sind weltweit etwa 300.000 Tonnen hochradioaktiven Abfalls angefallen, davon etwa 70.000 in den USA. In den deutschen Atomkraftwerken werden jährlich rund 450 Tonnen hochradioaktive abgebrannte Brennelemente erzeugt."
    * https://www.deutschlandfunk.de/sicherheitsrisiko-hitzewelle-atomkraftwerke-muessen.697.de.html?dram:article_id=424372
        * bei großer Hitze müssen wegen weniger Flusswasser Großkraftwerke (Atom, Kohle) gedrosselt werden

### Initiativen rund um den Ausstieg aus Atomkraft
    * Video: "Initiative in Aachen zur Messung von Radioaktivität gegründet" - Tagesschau 2016-12, http://www.tagesschau.de/multimedia/video/video-239891.html
        * TDRM - Tihange Doel radiation monitoring network, https://tdrm.fiff.de/index.php?lang=de, "Das TDRM-Projekt schafft Transparenz. Bürgerinnen und Bürger in den direkt betroffenen Regionen in Belgien, den Niederlanden und Deutschland sollen jederzeit ein Auge auf die Situation werfen können. Unser Ziel ist, dass dies Behörden und Politik motivieren wird, auch auf ihrer Seite mehr Transparenz herzustellen."
            * Ein Projekt von FIfF - Forum InformatikerInnen für Frieden und gesellschaftliche Verantwortung e.V., https://www.fiff.de/themen, ::frieden, siehe dort

### Geschichte
* https://www.ews-schoenau.de/energiewende-magazin/zur-sache/atomkraft-visionen-fuer-die-tonne/

Kohlestrom
----------
* todo: Quecksilber

### 2020: Datteln 4
* https://www.bund-nrw.de/themen/klima-energie/hintergruende-und-publikationen/steinkohlenkraftwerke/uniper-kohlekraftwerk-datteln-iv/
    * "Schon mehrfach ist das Uniper Kohlekraftwerk Datteln IV vor Gericht gescheitert. Trotzdem soll es mit allen Mitteln doch noch durchgesetzt werden."
    * s. auch Video

### Sammlung zu Kohlestrom und Kohleabbau
* 2017: ["Bericht 2017: Globale Energiewirtschaft und Menschenrechte"](http://germanwatch.org/13958), Germanwatch und Misereor
    * "Deutsche Unternehmen und Politik auf dem Prüfstand"
    * "So betreffen rund ein Drittel der unternehmensbezogenen Menschenrechtsvorwürfe international den Energiesektor. Das beginnt mit dem Kohleabbau in Kolumbien, wo es zu Zwangsumsiedlungen und Verfolgung von Gewerkschaftern kommt. Doch auch für große Staudämme müssen Menschen oft unfreiwillig von ihrem Land weichen, erhalten nur unzureichende Entschädigungen und werden für Proteste kriminalisiert."
* 2017: ["Klage gegen Kohleabbau im Hambacher Forst abgewiesen"](https://www1.wdr.de/nachrichten/rheinland/koeln-braunkohle-klage-bund-rwe-entscheidung100.html)
* Probleme: Landschaftszerstörung, Gewässerverschmutzung, Luftvergiftung (NABU Naturschutz heute 2/2016)

### Ausstieg sozialverträglich gestalten
* ARD-Doku "Kohle oder Klima - Angst um die Zukunft" (28.11.2018), 45 min
    * Die Politik muss Antworten liefern wie den Menschen, die derzeit in der Kohle arbeiten, geholfen werden kann. Das kann ruhig was kosten; denn: Klimaschäden zu reparieren ist richtig teuer.
    * Staatsrechtler: die Politik kann sehr wohl früher getroffene Entscheidungen und Verträge widerrufen, wenn sich die Sachlage geändert hat und das Gemeinwohl gesichert werden muss; eventuell muss man mit Regress rechnen, aber die Handlungsoptionen sind da (Weitermachen wie früher mal geplant ist nicht alternativlos)
    * 35 min: krass: der Ministerpräsident redet vor den RWE-Mitarbeitern und polarisiert stark; wirft die wenigen gewaltbereiten Waldbesetzer in einen Topf mit den vielen anderen Menschen, die friedfertig für eine intakte Umwelt zum Wohle aller demonstrieren
* siehe auch "Klimaschutz? Nein, das ist nicht sozialverträglich"

Zielkonflikte: Windkrafträder: Naturschutz vs. Klimawandel
----------------------------------------------------------
- ...
- Windrad, das um die Ecke steht, im Vergleich zu einem Kohle- oder Atomkraftwerk
    - relative Vorteile
        - kann jederzeit (bzw. nach 20 Jahren) relativ schadstoffarm wieder entfernt werden
        - keine Emissionen (chemische Schadstoffe oder Radioaktivität)
    - Nachteile:
        - ...

Fazit
-----
* Auf Erneuerbare Energien achten ([Hintergrund: Klimawandel](../Hintergrund/klimawandel.md))
* Unternehmen mit rückschrittiger Unternehmenspolitik aktiv vermeiden
    * ein Negativ-Kriterium könnte z. B. Lobbyarbeit zum Nachteil des Gemeinwohls sein, siehe z. B. https://lobbypedia.de/wiki/RWE
