Mobilität / daily-choices-info-package
======================================

[zurück](../../..)

<!-- toc -->

- [Mobilitätskonzepte](#mobilitatskonzepte)
  * [2020](#2020)
  * [2019](#2019)
- [Fahrrad](#fahrrad)
  * [Meldeplattform Radverkehr](#meldeplattform-radverkehr)
  * [Hilfe zum Umstieg auf das Fahrrad](#hilfe-zum-umstieg-auf-das-fahrrad)
- [ÖPNV](#opnv)
- [Individuelle Mobilität, Auto](#individuelle-mobilitat-auto)
  * [Eigenes Auto](#eigenes-auto)
  * [Carsharing](#carsharing)

<!-- tocstop -->

::mobilität

Mobilitätskonzepte
------------------
### 2020
* Fuß / Fahrrad
* ÖPNV, siehe unten, nie dagewesene Anstrengungen
    * Nachhaltigkeit und Datenschutz in der Digitalisierung
        * z. B. neue Art der Ticket-Abrechnung
            * (auch ohne Smartphone möglich sein)
            * Nutzer lädt _eine_ Karte _vorher_ mit Geldeinheiten auf (=> anonym)
                * DIE "ÖPNV-Karte"
            * Wird am Bahnhof oder im Zug gelesen; belastet
                * Was ist mit Fernreisen und Vorabbuchungen?
            * Im Backend werden die Beträge dem jeweiligen Verkehrsunternehmen gutgeschrieben

### 2019
* ["Anders unterwegs sein - wie wollen wir leben?"](https://www.ardmediathek.de/swr/player/Y3JpZDovL3N3ci5kZS9hZXgvbzExMzYwMjg/), 16.07.2019, ARD
    * "Jahrzehntelang war das Auto der Deutschen liebstes Kind. Stadtentwicklung hieß Straßenentwicklung. Lebensqualität maß sich an der Größe des Parkraums. Aber inzwischen quellen die Städte über von Verkehr, die Atemluft wird jährlich giftiger. Deshalb steuern immer mehr Kommunen und ihre Bürgerinnen und Bürger dagegen. Reutlingen zum Beispiel will die Zahl der Buslinien verdoppeln und Radschnellwege bauen. Die Stadt erhält dafür als eine von bundesweit fünf Modellstädten Geld vom Staat. Und nutzt dabei die Erfahrungen europäischer Metropolen, die schon viel weiter sind."

Fahrrad
-------
### Meldeplattform Radverkehr
* https://www.meldeplattform-radverkehr.de

### Hilfe zum Umstieg auf das Fahrrad
aus Schrot und Korn 07/2017, S. 60: www.carexit.org/de

- "95% unserer Entscheidungen treffen wir unterbewusst. Das Autofahren ist eine Gewohnheit, die wir kaum hinterfragen, die aber viele negative Konsequenzen für den Einzelnen und die Gesellschaft hat. Wir helfen Menschen, die diese Gewohnheit ablegen und auf das Rad umsteigen möchten. Da wir wissen, wie schwer das fällt, haben wir die Initiative carEXIT ins Leben gerufen."

- "carEXIT ist ein Projekt der FahrradBande der BUNDjugend Berlin in Zusammenarbeit mit Meerkat Planet und Reclaim The Streets."

ÖPNV
----
* https://de.wikipedia.org/wiki/Fahrgastbeirat

Individuelle Mobilität, Auto
----------------------------
### Eigenes Auto
* siehe README-Auto.md

### Carsharing
* Carsharing bringt nur dann etwas, wenn in der Summe weniger das Auto benutzt wird als ohne Carsharing (todo: quelle UBA)
    * siehe auch Tony Seba zu autonomem Fahren

