Auto
====

Die Entscheidung, ein neues Auto zu kaufen ist bereits gefallen. Nun geht es darum, welche Art von Auto und welcher Hersteller. Wie bei den meisten Entscheidungen gibt es auch hier nicht für jede Situation _das_ absolut beste, aber es gibt relative "x-ist-besser-als-y"-Bewertungen.

<!-- toc -->

- [E-Auto - ein Baustein](#e-auto---ein-baustein)
  * [Nicht-fossil oder fossil?](#nicht-fossil-oder-fossil)
  * [Probleme mit Batterie-Rohstoffen, aber...](#probleme-mit-batterie-rohstoffen-aber)
  * [Brennstoffzelle](#brennstoffzelle)
- [Schwere Fahrzeuge](#schwere-fahrzeuge)
- [Reifen-Doku](#reifen-doku)
  * [News dazu](#news-dazu)
- [Benzin oder Diesel?](#benzin-oder-diesel)
  * [2018: "Auch moderne Benziner brauchen Feinstaubfilter"](#2018-auch-moderne-benziner-brauchen-feinstaubfilter)
  * ["Aber Diesel ist doch billiger in der Herstellung" (falsch)](#aber-diesel-ist-doch-billiger-in-der-herstellung-falsch)
  * [Schädlichkeit von Abgasen, insbesondere Diesel](#schadlichkeit-von-abgasen-insbesondere-diesel)
  * [Lobbyarbeit](#lobbyarbeit)
- [Tanken: "Biosprit": E10 oder Super?](#tanken-biosprit-e10-oder-super)

<!-- tocstop -->

E-Auto - ein Baustein
---------------------
* ["Sind E-Autos wirklich umweltfreundlicher?"](http://www.tagesschau.de/wirtschaft/e-auto-109.html)
    * vs. Kohlestrom:
        * User-Comment: "Ist Ihnen eigentlich klar, daß man die zentral erzeugten Abgase bei der Stromgewinnung von leider immer noch benötigten fossilen Kraftwerken leichter filtern/reinigen kann als bei Millionen Autos?"
    * User-Comment: Gutes werde vorzeitig verschrottet
        * falsch, denn nichts wird so gut weiterverwendet, wie ein Auto, ggf. nicht hier, sondern woanders. Vorteil E-Auto-Produktion: es werden dafür immer weniger Fossil-Fahrzeuge produziert und die Situation verbessert sich schrittweise
* Was ist mit dem Lithium für die Akkus?
    * 2017: http://www.tagesschau.de/ausland/lithium-bolivien-101.html
        * Nebenthema: "Hier bauen 160 Arbeiter aus China eine Fabrik zur Produktion von jährlich 350.000 Tonnen Kaliumchlorid, das als Düngemittel eingesetzt wird."
    * Was ist besser? Einmaliger Lithium-Abbau (Voraussetzung: Recycling) oder dauerhafte Ölförderung?
        * Rohstoffförderung muss allgemein teurer werden
            * => damit sich Recycling lohnt
            * => dann wird Öl automatisch unwirtschaftlich
* [China bereitet Abschied von Verbrennungsmotoren vor](https://www.heise.de/newsticker/meldung/China-bereitet-Abschied-von-Verbrennungsmotoren-vor-3827166.html), heise, 2017

### Nicht-fossil oder fossil?
--> **Nicht-fossil** (also weg von Diesel, Kerosin oder Benzin)

* https://www.umweltbundesamt.de/themen/neuer-kurzfilm-klimaschutz-im-verkehr-das-geht
    * Video: ["Klimaschutz im Verkehr – das geht!"](https://www.youtube.com/watch?v=UiR7y5Hs7HE) (2 min), Kurzvideo 2016 des BMUB
* Studie 2015: https://www.umweltbundesamt.de/themen/abschied-von-fossilen-kraftstoffen-wie-sieht-die
* siehe auch [Hintergrund: Klimawandel](../Hintergrund/klimawandel.md)

### Probleme mit Batterie-Rohstoffen, aber...
* Doku_: ["Kann das Elektroauto die Umwelt retten? | WDR DOKU"](https://www.youtube.com/watch?v=aS_xTJmzdgA), 2019
    * "Sind E-Autos wirklich die Rettung für die Umwelt oder nur ein Milliardengeschäft für die Autoindustrie?"
    * ...
    * Besser leichte Autos mit kleinen Batterien und dafür weniger Reichweite
    * ...
    * Brennstoffzelle wurde nicht erwähnt
    * Gut belegte Gegendarstellung des Films: https://graslutscher.de/wie-eine-ard-doku-absurdes-zeug-ueber-elektromobilitaet-verbreitet-und-dadurch-den-klimawandel-verstaerkt/
        * "Zu Beginn baut die Sendung einen Strohmann auf, also ein Argument, dass der Gegner so eigentlich gar nicht formuliert, das man aber toll widerlegen kann"
        * ...
        * "Elektroauto-Akkus: So entstand der Mythos von 17 Tonnen CO2": https://edison.handelsblatt.com/erklaeren/elektroauto-akkus-so-entstand-der-mythos-von-17-tonnen-co2/23828936.html
            * "Eine Studie aus Schweden soll belegen, dass die Akkus eines Elektroautos das Klima mit 17,5 Tonnen CO2 belasten. EDISON begab sich auf Spurensuche und ermittelte: Die Zahl ist falsch. Und der Urheber erklärte uns, wie sie zustande kam."
        * ...
        * "Biodiesel aus Palmöl: Tank statt Teller": https://edison.handelsblatt.com/erklaeren/biodiesel-aus-palmoel-tank-statt-teller/22972166.html
            * "Der Verbrauch von Palmöl in Europa steigt. Ein großer Teil landet aber nicht in der Nahrung, sondern im Autotank. Und das, obwohl der Biokraftstoff eine schlechte Klimabilanz hat."
        * ...
* Was ist mit Brennstoffzelle?

### Brennstoffzelle
* ["Brennstoffzelle im Auto: Besser als Lithiumakkus? | Harald Lesch"](https://www.youtube.com/watch?v=TswNLBnAPjU), 2019
    * nur Strom-Batterie ist landesweit schwierig (Rohstoffe, Leistungsvorhaltung, Netzstabilität)
    * eine Kombination halbe-halbe Brennstoffzelle und Strom-Batterie sinnvoll?

Schwere Fahrzeuge
-----------------
* ...
* 2018 Studie: viel von dem CO2 rührt daher, dass die Autos immer größer und schwerer werden

Reifen-Doku
-----------
Reifendoku

* "Autoreifen - Ein schmutziges Milliardengeschäft | WDR Doku", https://www.youtube.com/watch?v=u_QPxTJbwTw, 2019
    * ...
    * "Der größte Produzent von Gummi, dem Naturkautschuk, ist Thailand"
        * Produktion stark angestiegen (in den letzten Jahren um 300 %)
    * Plantage eines Bauern
        * früher Regenwald, dann Zuckerrohr und nochwas; danach nur noch Kautschuk
        * mit Essig wird das fest
        * 50 Arbeiter
        * Überangebot von Kautschuk => es gibt immer weniger Geld; vor ca. 5 Jahren stürzte der Preis ein => Sparen wo es nur geht
        * Arbeiter:innen kommen zumeinst aus Nachbarland Kambodscha, weil noch billiger als thailändische Arbeiter
            * 12 h / Tag, 5 - x Tage die Woche; Nacharbeiten
            * 140 EUR / Monat für Kraft, die mit 23 J. (seit 7 J. dabei) weder lesen noch schreiben kann
                * = Hälfte vom Mindestlohn in Thailand; warum ist das so? s. unten
            * Familie: zwei Matratzen für 8 Personen (inkl. Baby)
        * **Herbizideinsatz mit Paraquat** (in Europa wegen Giftigkeit schon verboten)
            * Junge trägt Atemmaske
            * leider wird Paraquat auch **über die Haut aufgenommen und verursacht Nieren- und Leberschäden**
            * nur das **weiß der Junge nicht** und arbeitet mit kurzen Hosen anstelle mit einem Schutzanzug
            * 10:30 Gras muss vor der Ernte weg, damit sich keine gifigen Schlagen verstecken können; vorher/nacher
            * hält sich Monate im Boden
            * Tausende Arbeiter sollen an den Folgen sterben
    * Abgeben an ersten Zwischenhändler, der die Preise diktiert
    * Dann geht es zu einem Gummifabrik (zum Großteil in chin. Hand)
        * Interviews abgelehnt
    * Ökonom an der Universität: Traditionell gelten Kautschuk-Arbeiter als Geschäftspartner und fallen daher nicht unter den Mindestlohn
    * Kleinere Gummifabrik
        * auch diese Angestellten sind nicht angestellt, sondern werden am Gewinn beteiligt; momentan ist der Gummipreis niedrig
        * altes Ehepaar (er: 80 J.) arbeitet schon seit 50 Jahren auf den Plantagen der Firma
            * wohnt in ärmlicher Hütte; selbst für thailändische Verhältnisse sehr arm
            * fürs Klo gräbt er ein Loch
    * Reifenhersteller Bridgestone (Japan, größter der Welt) lehnt Interview ab; auch die anderen
        * Continental: 4.größter Reifenhersteller
    * Kambodscha
        * auch hier: Kautschuk-Bäume soweit das Auge reicht
        * auch recht arm alles
        * 21:00: Land-Grabbing/Landraub. Solaten und Bagger haben das Land einer Frau (heute 30 J.) mit Kindern umgepflügt und Plantagen draus gemacht
            * früher Lebensmittel, heute nur noch Kautschuk
            * (moderner Kolonialismus)
            * betroffen insbesondere die indigene Bevölkerung
            * Angst machen durch Armee und Gewehre; Schläge; auch Morde
        * jedes Jahr 3000 ha Land zu Kautschuk-Plantagen umgewandelt
        * hauptsächlich zwei vietnamesische Firmen betreiben das Land-Grabbing
        * Regierung verpachtet das Land und hat dabei das Besitzrecht der Landbevölkerung ignoriert
        * Beispiel einer Region / eines Dorfes, wo die Menschen ihre Felder bestellten und ein gutes, bescheidenes Leben führten, innerhalb ihrer Traditionen. => Vernichtet für Gummi.
        * traurige Bilder und Interviews
        * früher 1000 Dollar / Jahr für Ernte; Angebot der Firma für Land: 300 Dollar (einmalig)
        * ehemaliger Dorfbewohner weigern sich für die neuen Herren zu arbeiten => neue (ärmliche) Siedlungen mit Zugezogenen
        * Gummi-Industrie wächst jährlich mit 6-7 %, Ende nicht abzusehen; Menschen vor Ort profitieren nicht
    * Continental und andere Marktführer
        * verweist auf Allgemeinplätze
        * kein Interview
    * Lobbyverband in Frankfurt
        * hat Code of Conduct
        * Interviewpartner sagt, dass es nie möglich ist, den Farmer mit Geld zu erreichen, weil System zu verzweigt
            * (Also weiter wie bisher? Wir haben billiges Gummi und was haben die Menschen dort für Vorteile davon?)
            * ist erschüttert; müssen nach vorne schauen
    * Runderneuerte Reifen
        * oft nur für Nutzfahrzeuge und Flugzeuge
        * einzige Firma für Runderneuerung für PKW im Teutoburger Wald
            * manuelle Eingangsprüfung für jeden Reifen
            * ca. 15 % Ausschuss
        * Deutschland hat keine Recycling-Pflicht für Reifen => Firma muss aus Frankreich und Spanien die Reifen holen
        * D.: alte Reifen werden üblicherweise geschreddert und in Zementwerken verheizt
            * durch Überangebot muss man sogar fürs Verheizen nochwas draufzahlen
            * Zahlen: "in Deutschland werden jährlich 200.000 t Altreifen einfach verbrannt"
        * Polizei Recklinghausen macht Ausschreibung für Winterreifen, wo explizit runderneuerte Reifen ausgeschlossen werden. Der Grundlage der Entscheidung wird nicht genannt.
        * unabhängiges Prüflabor
            * runderneuerte Reifen kamen in den 50ern auf, weil damals wenig Resourcen da waren
            * heute: praktisch gleiche Performance wie Neureifen
        * und preiswerter
        * Zahlen: Marktanteil (bei PKW?) in D. von runderneuerten Reifen: unter 5 %
    * YT-comment:
        * "Ich habe das Gefühl, so eine Doku kann man über so ziemlich alles was wir importieren machen.
            Schon traurig unsre Welt. Trotzdem gute Doku, ist wichtig dass man sich bewusst macht wieso wir hier so leben können, wie wir leben."

### News dazu
* https://www.tagesschau.de/inland/mobilitaet-autobauer-rohstoffe-101.html - Die Wirtschaftslenker zu Rohstoffen
    * oft: "die werden geliefert" a la Lesch
    * Thema Batterien
        * "VW verlässt sich auf seine Zulieferer"
            * "Wir sind nicht in der Position zu entscheiden, wo unser Rohstoff herkommt, das tun unsere großen Partner und Lieferanten auf der Zellseite."
        * es geht auch anders: "BMW kauft Rohstoffe selbst ein"

Benzin oder Diesel?
-------------------
Die Entscheidung ein privates Fossil-Fahrzeug zu kaufen ist gefallen, aber nun welcher Kraftstoff?

--> wenn fossil, dann besser **Benzin** anstelle Diesel

**Diesel hat einige Nachteile** gegenüber dem Benzin

* "[Wie gefährlich sind Feinstaub und Stickstoffdioxid?](http://www.tagesschau.de/multimedia/video/video-276419.html)", tageschau 06.04.2017
    * Der Verkehr ist für die meiste Feinstaubbelastung verantwortlich und dabei der Reifenabrieb und das Aufwirbeln von kleinen Partikeln
    * Folgen: Husten, Asthma, Krebs
    * **Stickstoffdioxid**
        * an über 50 % der verkehrsnahen Messstationen: Grenzwerte überschritten
        * Folgen: Atemwegsprobleme, Herz-Kreislauferkrankungen
        * Ursache in den Innenstädten: Verkehr (84 %)
            * _davon_ Anteil Dieselfahrzeuge: ca. 95 %
        * Maßnahmen: z. B. Stuttgart: zeitweise Fahrverbote für ältere Dieselfahrzeuge
        * "Die Werte der neueren Dieselfahrzeuge seien wesentlich besser, sagen die Hersteller"
            * hust
            * welche Hersteller?
            * welche Modelle?
            * um wieviel besser?

### 2018: "Auch moderne Benziner brauchen Feinstaubfilter"
* stimmt: https://www.zeit.de/2018/14/benzinmotoren-russ-luftverschmutzung-feinstaub
* beim giftigen NOx sind sie dennoch besser als Diesel
* Lösung für saubere Luft: keine Verbrennung im Automobil, sondern wenn dann in stationären Kraftwerken mit guter Filtertechnik oder EE.

### "Aber Diesel ist doch billiger in der Herstellung" (falsch)
* Oft hört man Diesel sei billiger in der Herstellung und daher vermutlich auch umweltfreundlicher.
    * Das ist falsch
        * siehe https://de.wikipedia.org/wiki/Dieselkraftstoff#Steuern_und_Abgaben_in_Deutschland
        * Benzin wird in D. höher besteuert (historisch bedingt) und deshalb ist Diesel günstiger.
    * Diesel wird derzeit (Stand 2017) steuerlich noch begünstigt:
        * "Derzeit fließen dem Staat für einen Liter Benzin 65,45 Cent zu, für einen Liter Diesel nur 47,04 Cent. [...] BRH-Präsident Kay Scheller merkt an, die Auswirkungen von Diesel auf Gesundheit und Umwelt seien ebenso groß wie die des Kraftstoffs Benzin: "Insofern stellt sich die Frage, weshalb der eine gegenüber dem anderen privilegiert wird."" (http://www.tagesschau.de/inland/bundesrechnungshof-diesel-besteuerung-101.html, Oktober 2017)

### Schädlichkeit von Abgasen, insbesondere Diesel
* Doku: ["Exclusiv im Ersten: Keine Luft zum Atmen, 10.07.2017, 29 Min"](http://www.ardmediathek.de/tv/Reportage-Dokumentation/Exclusiv-im-Ersten-Keine-Luft-zum-Atmen/Das-Erste/Video?bcastId=799280&documentId=44317960), Das Erste, verfügbar bis 10.07.2018

### Lobbyarbeit
* 2018: ["Affen mussten Abgase einatmen US-Tierversuche für deutsche Autolobby"](http://www.tagesschau.de/wirtschaft/usa-abgasskandal-affen-101.html)
    * ["Studien mit Stickstoffdioxid - Experimente im Namen der Autolobby"](http://faktenfinder.tagesschau.de/inland/humanversuche-101.html)

Tanken: "Biosprit": E10 oder Super?
-----------------------------------
Biosprit (z. B. E10) hat so viele Nachteile, dass man sich wundert, warum die Politik es so stark fördert. Weg damit.

* Konkurrenz zur Nahrung, Abholzung von Regenwald
    * 2017: https://www.abenteuer-regenwald.de/bedrohungen/biosprit
* Förderung von schädlichen Palmölplantagen
    * 2017: https://www.regenwald.org/petitionen/908/palmoel-stopp-biodiesel-stopp-sofort
    * siehe auch [Palmöl](../Konsum/palmöl.md)
* "Umweltpolitischer Unfug", Greenpeace 2014
    * https://www.greenpeace.de/themen/landwirtschaft/biosprit/biosprit-ist-umweltpolitischer-unfug
* https://de.wikipedia.org/wiki/Biokraftstoff

Alternative Mittelverwendung: Die Gelder könnten stattdessen in langfristige nicht-fossile Projekte investiert werden (Verkehrsreduktion, erneuerbare Energien etc.).
