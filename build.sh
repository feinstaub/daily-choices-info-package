#!/bin/bash

# Buch- und Filmliste, Zahlenliste
Hintergrund/generate-collections.sh

# needs https://github.com/jonschlinkert/markdown-toc
# use `npm install` to init
# find . -type f -iname '*.md' -exec echo Process {}... \; -exec markdown-toc -i {} \;
export PATH=$(pwd)/node_modules/.bin:$PATH
tool-markdown-toc-all

find -name '*.md' -not -path "./node_modules/*" -print0 | sort -z | xargs -0 wc -l
echo "(number of lines in md files)"
