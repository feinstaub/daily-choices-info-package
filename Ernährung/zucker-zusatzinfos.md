Zusatzinfos / Zucker
====================

[zurück](../../..)

Alternativen
------------
* Zucker im Übermaß ist schädlich, Positiv-Beispiel von Unternehmen, die Alternativen anbieten:
    * http://xucker.de

Kontroverse Positionen
----------------------
Gesundheit ist ein komplexes Thema. Absolute Aussagen sind so gut wie unmöglich.

Zufällig ausgewählte Positionen:

* Doku "What The Health (2017)"
    * Zucker sei gar nicht so schlimmer. Schlimmer sei Tier-Protein, Hühner-Eier etc.
* [Zu viel Zucker kann der Bauchspeicheldrüse schaden](http://www.tellerrandblog.de/zu-viel-zucker-kann-der-bauchspeicheldruese-schaden/), 2012
    * "Schwedische Studie: Hoher Konsum gesüßter Nahrungsmittel fördert Pankreaskarzinom."
* https://www.psiram.com/de/index.php/Zuckermythen - Kritiker der Kritiker

Nachrichten:

* 2017: http://www.evidero.de/schaedlicher-maissirup
    * 2017 fällt die Zuckerquote weg und es wird noch mehr Isoglukose verwendet
    * "Bis 2017 wird Isoglukose (high fructose corn Sirup) in Europa nur in kleinen Mengen produziert, da seine Wirkung auf die Gesundheit nachweislich schädlich ist. Aber Brüssel hat mit TTIP (Transatlantisches Freihandelsabkommen) seine europäische Agrarpolitik und damit auch den Zuckermarkt neu geregelt: Ab 2017 fällt die Zuckerquote für den Maissirup Isoglukose, den Zuckerzusatz, der uns dick, krank und unersättlich machen kann."
    * Was ist an dem Artikel dran?
    * ["Verschiedene Zucker: Die wichtigsten Zuckerverbindungen und wie sie auf den Körper wirken"](http://www.evidero.de/unterschiedliche-zuckerverbindungen)
        * => komplexes Thema; am besten möglichst wenig Zucker
