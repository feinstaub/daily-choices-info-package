Bio / Zusatzinfos
=================

<!-- toc -->

- [Autoren](#autoren)
- [Gesundheit](#gesundheit)
- [Herausforderungen in der Landwirtschaft allgemein](#herausforderungen-in-der-landwirtschaft-allgemein)
  * [Ausbeutung von Erntehelfern](#ausbeutung-von-erntehelfern)
- [Kritik](#kritik)
  * ["Aber Bio im Supermarkt ist aber gar nicht so viel teuer, also ist da was faul"](#aber-bio-im-supermarkt-ist-aber-gar-nicht-so-viel-teuer-also-ist-da-was-faul)
  * [2015: ARTE: Bio-Kritik, aber kein Grund zum Resignieren](#2015-arte-bio-kritik-aber-kein-grund-zum-resignieren)
- [2016: Gesunde Böden als Hochwasserschutz](#2016-gesunde-boden-als-hochwasserschutz)
- [2017: fibl.org](#2017-fiblorg)
- [2017: SÖL - Stiftung Ökologie & Landbau](#2017-sol---stiftung-okologie--landbau)
- [Geschichten](#geschichten)

<!-- tocstop -->

Autoren
-------
* Astrid Lindren
    * https://andrewcrouthamel.wordpress.com/2018/09/19/how-i-got-involved-in-kde/

Gesundheit
----------
* Unzureichende Forschung von Kombinationswirkungen
* todo: Beispiele von Leuten, die durch Pestizide (Herbizide, Fungizide, Insektizide) krank wurden.
* todo: das Beispiel. wo der Hund gestorben ist

Herausforderungen in der Landwirtschaft allgemein
-------------------------------------------------
### Ausbeutung von Erntehelfern
* ["Tödliche Unfälle in Italien - Die Wut der Erntehelfer"](https://www.tagesschau.de/ausland/italien-erntehelfer-101.html), 2018
    * "Zehntausende Erntehelfer schuften in den Tomatenfeldern Süditaliens. Viele von ihnen kommen aus Afrika, haben keine Papiere und hausen in Baracken. Nach tödlichen Unfällen entlädt sich nun ihre Wut."

Kritik
------
### "Aber Bio im Supermarkt ist aber gar nicht so viel teuer, also ist da was faul"
* Mögliche Auflösung: Bio ist gar nicht so teuer wie allgemein behauptet wird
* Mögliche Auflösung: Es wird querfinanziert
* Mögliche Auflösung: Teuer muss ja nicht unbedingt gut sein
* Mögliche Auflösung: Anonymes Bio (gerade aus dem Nicht-EU-Ausland) hat Transparenz-Probleme. Vertrauen in die (landes)regionale Herstellerfirma nötig.
* Fragestellung: sagt der Preis immer etwas darüber aus, wie gut etwas ist?
    * siehe "Die besten Dinge sind kostenlos" und "Für die besten Dinge wird keine [Werbung](../Konsum/werbung.md) gemacht"

### 2015: ARTE: Bio-Kritik, aber kein Grund zum Resignieren
* http://future.arte.tv/de/bio-ein-massenproblem
    * "Kaufen Sie selbst noch Bio? - Christian Jentzsch: (lacht) Warum denn nicht? Es geht im Film ja nicht darum, dem Konsumenten Bio auszureden. Wir wollen zeigen, welche Fehlentwicklungen die Bio-Szene genommen hat."
    * "Genauso wie wir Konsumenten auf das ein oder andere Schnäppchen verzichten müssen. Wollen wir nun die belohnen, die in Massen billig produzieren oder die Kleinbauern, die es verdienen?"

2016: Gesunde Böden als Hochwasserschutz
----------------------------------------
* [Gesunde Böden als Hochwasserschutz](http://www.ardmediathek.de/tv/Frankenschau-aktuell/Gesunde-B%C3%B6den-als-Hochwasserschutz/BR-Fernsehen/Video?bcastId=14913732&documentId=35837660), 3 min, BR, 2016, link dead
    * Hochwasserverschärfung durch Bodenerosion und verdichtete Böden
    * Artenerhalt auf nicht-"konventionellen" Wiesen

2017: fibl.org
--------------
* Argumente für den Biolandbau und Biowissen: http://www.fibl.org/de/themen/argumente.html
    * ""Foliensammlung Biolandbau" für Unterricht und Beratung in der Schweiz (2016)"
        * z. B. Thema **Unkraut**: http://www.fibl.org/fileadmin/documents/de/themen/foliensammlung_biolandbau/07_Pflanzenbau_FS_2016.pdf
            * aus "Foliensammlung Biolandbau" http://www.fibl.org/de/themen/argumente/download-foliensammlung.html
    * "100 Argumente für den Biolandbau (2015)"
    * "Biowissen: Fakten und Hintergründe zur Bioproduktion (2014)"
    * "Gute Gründe für den Biolandbau (2011)"
    * "Gesellschaftliche Leistungen der biologischen Landwirtschaft (2009)"
* "FiBL Deutschland - Wissenschaftlicher Service für den Ökolandbau" - http://www.fibl.org/de/deutschland/standort-de.html
    * "Forschungsinstitut für biologischen Landbau (FiBL)" / Frankfurt am Main
    * Interessante aktuelle Nachrichten
* "FiBL Schweiz – 40 Jahre Forschung für die biologische Landwirtschaft" - http://www.fibl.org/de/schweiz/standort-ch.html
* Infos zu Biobaumwolle: http://www.fibl.org/de/themen/biobaumwolle.html und anderen Bio-Themen
    * siehe auch Kleidung/
* http://www.fibl.org/de/ueber-uns.html - Forschungsinstitut für biologischen Landbau FiBL
* http://www.fibl.org/de/startseite.html
    * z. B. "Tutorial: Phosphorus recycling for organic agriculture (CORE Organic/Improve-P)" (Video, 30 min)
        * http://www.fibl.org/en/switzerland/communication-advice/communication/videos.html
        * vergleiche Planetary Boundaries

2017: SÖL - Stiftung Ökologie & Landbau
---------------------------------------
* z. B. Seminare tiergerechte Haltung - http://www.soel.de/projekte/seminare-tiergerechte-haltung/
    * Downloads Flyer Milchvieh
* Stellenbörse Bio - http://www.soel.de/stellenboerse/

Geschichten
-----------
Erzählungen/Beispiele von Bekannten (keine wissenschaftliche Quellen).

* 2018:
    - zu Wein: Person erkennt Bio-Wein, weil sie von konventionellem Ausschlag bekommt
    - zu Südtiroler Spritz-Apfelplantagen: Erzählung von Südtirol-Urlaub => jeden Tag hörte man die Spritzgeräte
    - zu Pestiziden: Ehemaliger konv. Winzer erntet eigenen Kirschbaum nicht mehr, weil an der Grenze Wein gespritzt wird

* 2017:
    - zu Pestiziden: Früher (1970?): Landwirte schmuggeln hochwirksame Pestizide von Frankreich nach D. Heute gibt es in F. strengere Richtlinien
    - zu Pestiziden: Dilemma: Bewusstsein vs. eingeschlagener Beruf: Landwirt baut für sich selber im Garten ungespritzes Gemüse an
