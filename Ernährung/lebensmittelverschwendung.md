Lebensmittelverschwendung
=========================

siehe "Lebensmittelverschwendung"

* [Bundeszentrum für Ernährung zu Lebensmittelverschwendung](https://www.bzfe.de/lebensmittelverschwendung-1868.html)
* ["Lebensmittel: Zwischen Wertschätzung und Verschwendung"](https://www.verbraucherzentrale.de/wissen/lebensmittel/gesund-ernaehren/lebensmittel-zwischen-wertschaetzung-und-verschwendung-6462), 2016
* Welthungerhilfe: ["Schluss mit der Wegwerfmentalität"](https://www.welthungerhilfe.de/aktuelles/blog/lebensmittelverschwendung/)
* [Antwort foodwatch auf eine Kampagne zur Lebensmittelverschwendung](https://www.foodwatch.org/de/newsletter/diese-kampagne-ist-ein-skandal), die dem Konsumenten einseitig die Schuld gibt, 2017
    * u. a. auch etwas zum Mindesthaltbarkeitsdatum
* Stellungnahme foodwatch 2013: ["Was mich am meisten ärgert: Schuld soll IMMER der Verbraucher sein!"](https://www.foodwatch.org/de/newsletter/was-mich-am-meisten-aergert-schuld-soll-immer-der-verbraucher-sein/)

Ansätze:

* Food-Sharing
* https://sirplus.de, 2019
    * "Wir sind SIRPLUS und bringen überschüssige Lebensmittel über unsere Supermärkte und unseren Online Shop dorthin, wo sie hingehören - in den Magen! Wir haben schon vier Rettermärkte eröffnet"
