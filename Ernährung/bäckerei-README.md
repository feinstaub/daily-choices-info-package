Bäckerei / daily-choices-info-package
=====================================

[zurück](../../..)

Bäckerei-Produkte
-----------------
* Doku: ["Billige Brötchen: Die Spur der Teiglinge"](https://www.youtube.com/watch?v=81Ueu--ufTM), Impuls-Film (45 min), 14.12.2011, SWR
    * Brot
    * billige Tiefkühl-Industrie-Teiglinge im Gegensatz zum handwerklichen Backen
    * bunte Aufbackbrötchen-Kataloge, ganz einfach online bestellen
        * inklusive Aufbackanleitung für das Verkaufsgeschäft
    * Beispiel Laden in Wiesbaden. Teiglinge geliefert aus Berlin. Herstellt in Osteuropa.
        * z. B. aus Polen, laut dem Bäcker und Brötchenmakler
    * Bäckermeister aus Heppenheim arbeitet jetzt in Warschau Industriebäcker
        * u. a. ist der Verbraucher selber schuld
    * Pre-bake-System
    * Zusatzstoffe und Enzyme (= Backmittel) für das Mehl für die industrietaugliche Verarbeitung
        * künstlich veränderte Eiweiße
        * bakerielle Protease
        * über 100 zugelassene Stoffe
        * Gummibrot
        * Gesundheitsfolgen? Nicht erforscht.
            * zerfallen die Zusatzstoffe wirklich beim Backen?
            * z. B. möglicherweise allergische Reaktionen
    * technische Enzyme müssen nicht auf der Zutatenliste deklariert werden
        * wie ist das mit Bioprodukten?
    * Enzym-Mischungen sind ein großes Betätigungsfeld für Firmen
        * z. B. Clean/Lean-Label (so dass nur das verwendet wird, was nicht deklarationspflichtig ist)
    * Interview mit einem Lebensmittelprüfer
        * er könne gar nicht alles verstehen, also versucht er erst gar nicht über Kombinationswirkungen nachzudenken
        * der Verbraucher könne es ja dann erst recht nicht verstehen


Exkurs: Convenience in Restaurants
----------------------------------
* Doku: ["Frisch auf den Tisch? Industrieprodukte in Restaurantküchen, 2017, NDR](https://www.youtube.com/watch?v=JFSJ54S38po), 45 min, todo:offline?
    * z. B. leckere Sauce-Hollondaise von Unilever
    * Fertigsaucen für die Profi-Küche
    * Beispiel: "Professional Konzentrierter Fond Kalb", mit Geschmacksverstärker etc., aber nicht auf der Speisekarte deklarationspflichtig
        * "Konzentrat"
    * Labels für Profi-Produkte, z. B. "Ohne deklarationspflichtige Zusatzstoffe (O.D.Z)
        * öffentlichkeitsscheu
    * Aviko Foodservice, Niederlande
        * Convenience für die Profiküche im Restaurant
        * insbesondere Kartoffelprodukte
        * "Tüte auf und rauf aufs Blech"
        * ein Fertig-Rösti, der extra so aussehen soll wie selbst gemacht!
            * extra ein bisschen nicht ganz rund und gröber
        * "auch in der Spitzengastromomie"
        * weil es in den Restaurantküchen immer weniger Fachkräfte gibt
    * 24:50 min: fertige TK-Spiegeleier aus dem Karton
        * Karton aufmachen, die TK-Eier aufs Blech und ab in den Ofen
        * (das sind sicher keine Bio-Eier; die armen Hühner)
    * 26:25 min: Eistangen
        * sieht geschnitten aus wie normale hart gekochte Eier
        * Für Salate: das Eigelb sitzt immer schön fest, mittig und bröselt nicht
        * (wieder ein Argument im konventionellen Billig-Restaurant vorsichtig mit Tierprodukten zu sein)
    * 28:40 min: Schnitzelstand bei der Internorga
        * "steht Schnitzel auf der Speisekarte, ist Absatz garantiert"
        * (irgendwo hieß es, die Leute wollen keine Kottlets mehr essen, wegen der Knochen, sondern nur noch das saubere Schnitzel)
            * siehe auch Fischfilet in "Schützt Fischzucht in Aquakulturen vor Überfischung der Weltmeere?"
        * Unilever ist immer groß dabei
    * 30:00 min: Schnitzelwerk in Bukarest
        * Cook in '5
        * soll so aussehen wie selbst gemacht
        * soll der Stolz des Kochs sein
        * extra mit aufwerfeneden Blasen in der Panada
        * man sieht auch etwas von der Anlieferungsware: größere grobe rote Stücke eines ehemaligen Tieres, in vielen Kartons verpackt
        * 1 Schnitzel im Verkauf 1,20 EUR. Im Restaurant wird das verdreifacht.
        * saubere Fabrik
        * Behandlung mit geheimer Würzlaken-Rezeptur für immer gleichen Geschmack
            * im Gegensatz zur heimischen Küche, wo man vielleicht punktuell zuviel oder zuwenig Salz aufträgt
        * vordefinierte Größen: vom Kinderschnitzel bis XL
        * Pannade: statt mit Wasser wird hier mit Vollei gearbeitet, damit die Blasen entstehen
        * Geschäftsführer Gerlinger hat früher als Koch auch viele Convenience-Produkte verwendet, "ganz klar", macht man halt so.
            * Die Diskussion mit dem Gast will man nicht führen müssen. Daher sieht alles immer selbstgemachter aus.
            * "Und wenn einer gefragt hat?" - "Hausgemacht. ... Das ist ja hausgemacht... in einer Produktionsstätte"
            * "Hausgemacht" ist gesetzlich nicht geschützt
            * (die Gäste werden für dumm verkauft; die wenigsten wissen vermutlich, was uns heutzutage alles krasses angeboten wird)
            * (Folge: bei unbekannten Restaurantküchen konsequent auf Bio setzen; denn da gibt es noch nicht so viele Convenience-Dinger)
    * 33:50 min: komplette feine Spitzenmenüs ohne sie selber zu machen
        * Hollyfood aus Bonn
        * "Highconvenience"
        * frisch und ohne Zusatzstoffe wird dort vorgekocht
        * marktfrisches Gemüse etc.
        * Beispiel-Auftrag: "geschmorte Kalbsbäckchen in Portweinsauce mit Kartoffelstamp und frischem Gemüse" (sieht lecker aus)
            * (wo kommen wohl die benötigten Kälber für die Bäckchen her? Uah.)
            * wird dann einzeln sauber in Plastiktüten verpackt, dann ab in den Karton und dan an den Spitzenkoch, den Profikunden, ausgeliefert
            * 6 EUR Gericht im Einkauf, 18 EUR auf der Speisekarte
                * sieht lecker aus
        * Geschäftsführer: McDonalds-Effekt: das ist doch toll, wenn es überall kontrolliert und gleich gut schmeckt und man nicht von dem Koch abhängig ist; ob der einen guten Tag hat oder nicht. Wer würde das nicht wollen?
    * immer weniger gekocht => Kochhandwerk leidet, insbesondere Nachwuchs, weil in den Ausbildungsbetrieben immer mehr auf Convenience gesetzt wird
        * uah: beim Fleisch, bei den Saucen, überall Convenience (z. B. besorgt man sich von den Hühnern nur die Brüste, wenn das gerade so gebraucht wird)
        * Kochlehrling: man kann die Gäste schneller abarbeiten, ist nicht so viel Stress, aber es ist halt auch nicht so schön
    * ca. 80% der Restaurantküchen in Deutschland Convenience-Produkte in verschiedenen Fertigungsgraden

Exkurs: Warum wenig veg* in Restaurants?
----------------------------------------
* 2017: Möglicherweise vegetarische Gerichte in Restaurants auch deswegen Mangelware, weil die Köche gar nicht dafür ausgebildet sind.
