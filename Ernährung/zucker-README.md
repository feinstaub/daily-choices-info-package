Zucker / daily-choices-info-package
===================================

[zurück](../../..)

<!-- toc -->

- [Das Zucker-Problem / Kinder](#das-zucker-problem--kinder)
- [Überzuckerte Getränke aktiv meiden](#uberzuckerte-getranke-aktiv-meiden)
  * [Wissenschaft](#wissenschaft)
  * [Arte-Doku 2015, Zucker-Doku](#arte-doku-2015-zucker-doku)
  * [Süße Kaffee-Getränke](#susse-kaffee-getranke)
- [Die 7 größten Mythen über Zucker und Übergewicht](#die-7-grossten-mythen-uber-zucker-und-ubergewicht)
- [Welche Hersteller?](#welche-hersteller)
- [Zuckerkartelle](#zuckerkartelle)
- [Aussagen von Ärzten](#aussagen-von-arzten)
  * [2018](#2018)
  * [2016 Bundesärztekammer](#2016-bundesarztekammer)
- [Verwandtes Thema: Rauchen](#verwandtes-thema-rauchen)
  * [Rauchen – Krankheit oder Lifestyle?](#rauchen-%E2%80%93-krankheit-oder-lifestyle)
  * [Widerlegung durch Gegenbeispiel](#widerlegung-durch-gegenbeispiel)
  * [Scheinargumente gegen ein konsequenteres Vorgehen gegen Zucker](#scheinargumente-gegen-ein-konsequenteres-vorgehen-gegen-zucker)
- [Zusatzinfos](#zusatzinfos)

<!-- tocstop -->

Das Zucker-Problem / Kinder
---------------------------
(Hochverarbeitete) Produkte bekannter Hersteller beim Einkauf bewusst im Regal stehen lassen, wenn diese Hersteller eine große Produktauswahl an zucker-angereicherten Produkten anbieten (und diese auch speziell für Kinder vermarkten, siehe unten).

Warum? --> Weil die Folgekosten einer überzuckerten Gesellschaft unnötig hoch sind:

* Stand 2017: **Diabetes verursacht in Deutschland ca. 10 % der Gesundheitskosten**. Siehe z. B. http://www.diabetes-heute.uni-duesseldorf.de/fachthemen/entstehungausbreitungverbreitung/?TextID=1025)
* Ursache von Diabetes: u. a. zuviel zuckerhaltige Nahrung (siehe z. B. https://de.wikipedia.org/wiki/Diabetes_mellitus#Ursachen_2)

Überzuckerte Getränke aktiv meiden
----------------------------------
### Wissenschaft
* ["Ärzte fordern gesetzliche Maßnahmen gegen überzuckerte Lebensmittel"](http://mobile.aerzteblatt.de/news/70180.htm), 24.08.2016, aerzteblatt.de
    * "„Wir brauchen mehr **Schutz vor Zucker für Kinder und Jugendliche**, denn Zucker macht dick und krank, er verursacht Diabetes Typ II, Gefäßerkrankungen, orthopädische Pro­bleme und Karies. Das ist wissenschaftlich bewiesen“, sagte der Präsident des Berufs­ver­bandes der Kinder- und Jugendärzte (BVKJ)"
    * Vorschlag Zuckersteuer (solange das noch nicht etabliert ist, kann man selber an der Ladentheke abstimmen)
    * Fazit: Wenn ein Hersteller für Zucker-Produkte teure Werbung schaltet, die nur die guten Seiten (süßer Geschmack) bewirbt und die gravierenden Nachteile außer Acht lässt, handelt unethisch und kann daher aktiv vermieden werden.

### Arte-Doku 2015, Zucker-Doku
* https://www.youtube.com/watch?v=UDiRnemBqPI, Die große Zuckerlüge | Doku | ARTE, 2015, 90 min
    * ...
    * https://www.youtube.com/watch?v=dBnniua6-oM, "Sugar: The Bitter Truth", 2009, 90 min
        * University of California Television (UCTV)
        * "Robert H. Lustig, MD, UCSF Professor of Pediatrics in the Division of Endocrinology, explores the damage caused by sugary foods."
        * "He argues that fructose (too much) and fiber (not enough) appear to be cornerstones of the obesity epidemic through their effects on insulin. Recorded on 05/26/2009"
    * ...
    * Cola sollte nicht an Kinder verkauft werden dürfen (sondern nur über die Eltern)
        * (Foodwatch 2018)
            * "Coca-Cola ist Kern des Problems"
            * https://www.foodwatch.org/de/aktuelle-nachrichten/2018/coca-cola-mitverantwortlich-fuer-fettleibigkeit-und-diabetes/
                * "Mit millionenschweren Marketingkampagnen nimmt Coca-Cola Kinder und Jugendliche als Zielgruppe ins Visier.
                    Gleichzeitig versucht der Konzern durch gezielte Lobbyarbeit und gekaufte Wissenschaftler wirksame Regulierungen wie Werbeverbote oder Sondersteuern
                    zu verhindern – Methoden, wie wir sie von der Tabakindustrie kennen."
                * "Coca-Cola erweckt durch eine Selbstverpflichtung den Eindruck, keine Werbung an Kinder unter
                    zwölf Jahren zu richten. Doch die Praxis sieht ganz anders aus, wie der neue foodwatch-Report zeigt."
                    * mit dem Weihnachtstruck
                    * mit Fußballspielern und
                    * Youtube-Stars
                * "Coca-Cola versteht es wie kaum ein anderer Konzern, ein positives Image zu kreieren – auch und
                    gerade bei jungen Menschen. Dabei sind die Zuckergetränke von Coca-Cola flüssige Krankmacher."
    * ...
    * nicht-alkoholische Fettleber
    * ...
    * 1 zuckerhaltiges Getränk pro Tag erhöht das Diabetes-Risiko um 29 %
    * ...
    * enorme Gesundheitskosten
    * ...
    * Diabetes, Herzerkrankungen
    * ...
    * Beispiel: 1 Orange essen vs. Frucht-Smoothie aus ganz vielen (ehemaligen) Orangen
    * ...
    * Stopfleber geht bei Gänsen genauso wie bei Menschen
    * ...
    * vor 30 Jahren haben sich die wissenschaftlichen Erkenntnisse zu schädlichem Zucker verdichtet
        => große PR-Maßnahmen, um einen Zustand der Verunsicherung zu erzeugen, so dass eindeutige politische Maßnahmen ausbleiben / ::werbung
            z. B. Verunsicherung, dass es ja eigentlich der Bewegungsmangel sei, der Fettleibigkeit oder Diabetes auslöst
    * ...
    * Werbung der Lebensmittelindustrie ist faktenverdrehend
        * auch auf Verpackungen: da steht ohne Zuckerzusatz, aber es ist Traubenzucker, Fruchtzucker etc. drin
        * Transparenz ist wichtig
    * Zuckerkonsum damals 2 kg / Jahr. Heute 150 kg / Jahr (check, oder "nur" 60?)
    * PR: Wissenschaftler Yudkin vs. Keys, ::pr
        * Yudkin hat früh herausgefunden, dass Zucker mit vielen Krankheiten zusammenhängt und viel publiziert
        * Keys wurde von der Lobby unterstützt und erstellte regelmäßig widersprechende Studien, z. B. dass das Fett schuld sei
            * => Lebensmittelindustrie: Fett raus => schmeckt scheiße => Zucker rein
    * ...
    * viele Menschen verhalten sich genauso gefräßig wie es klischeemäßig Fettleibige tun, gelten aber nicht so, weil sie nicht so aussehen
        dennoch haben sie ähnliche Stoffwechselstörungen
    * Video: "The Heart and Stroke Foundation Owes Canadian Parents an Apology", 2013, Yoni Freedhoff, https://www.youtube.com/watch?v=PfGzJ4mkSa0, 3 min
        * https://en.wikipedia.org/wiki/Yoni_Freedhoff
            * ...
        * Erdbeer-Fruchtgummi-Produkt, das aus 80 % Zucker besteht
        * Beispiel: Wenn ein Kind die gleiche Menge Zucker (17 kleine Gummiwürfel) über echte Erdbeeren essen wollte, wäre das eine ganze Schale voll
        * https://www.youtube.com/watch?v=mZA_Bsh4M9E, "It's Definitely Not "Just One", And You Can't "Just Say No""
            * 3:30: remember: Die Lebensmittelindustrie ist kein public health service;
                sie können gar nicht pro Gesundheit agieren;
                **sie können keinen moralischen Kurs fahren, außer es deckt sich zufällig mit dem Ziel des Profits**
            * Lobby etc etc.
            * Verantwortung der nicht-Lobby-Leute, Lehrer, Ärzte etc.
    * Zucker sollte nicht Obst genannt werden dürfen
        * Beispiel: "Wenn sie Frucht**saft** dehydrieren, dann bleibt ein Haufen Zucker übrig".
        * "Ja, stimmt der Zucker stammt aus Früchten. Trotzdem ist es bloß Zucker!"
        * Das ist KEIN Ersatz für richtiges Obst!
        * Was ist mit Trockenfrüchten? (todo)
    * 47 min: Forschung, dass Zucker (Fruktose) schädlich ist, ist über 30 Jahre alt; insbesondere Diabetes und Herzkrankheiten
        * Zuckerindustrie: PR: "Zucker ist ein natürliches Lebensmittel", "unbedenklich", "hat wenig Kalorien"
        * PR: Lenkung auf Übergewicht (obwohl auch viele Normalgewichtige Diabetes haben)
        * **stehen mit Zucker heute da, wo wir mit Tabak in den 1960er Jahren standen**, ::pr
            * Werbung damals: Ärzte rauchen lieber Camel; Rauchen ist gut für die T-Zone (Rachenraum)
            * obige Dokument der Western Sugar Company => gleiches Drehbuch
            * Tabakdokumente: nicht nur Strategie, sondern auch **Zynismus**
                * fiese Leute
                * ehrliche Forscher als paranoiden Wahn darstellen
    * Harward-Ern.-Professor-und-Leiter Stare hat großes PR-Netzwerk; Auftritte im Fernsehen etc.
        * widerspricht regelmäßig: es sei nicht der Zucker, es sei die hohe Kalorienmenge; Ablenken auf Fett
        * auch schon für Tabakinteressen geforscht
        * umfangreiche Spenden von der Lebensmittelindustrie (z. B. Kellogs)
    * Unterschied Lebensmittel- und Tabakindustrie
        * das eine braucht man
        * Problem auch hier: mit dem derzeitigen Lebensmittelangebot gibt es maximalen Profit;
            sinnvolle Lösungen erfordern, dass Konzerne erst mal weniger Geld verdienen; das geht nicht so einfach;
            eine mögliche Zusammenarbeit wird systematisch für eigene Interessen ausgenutzt
    * Adipositas-Konferenz von Kanada wird großzügig unterstützt von McDonalds und Coca-Cola
        * inkl. Reden
    * freiwillige Selbstkontrolle hat bei der Industrie noch nie gut funktioniert: Anschnallgurte, Tabak, Umweltverschmutzung; Regulierung mindert Profite / ::freiheit, siehe "Beispiele von guten Einschränkungen"
        * "da brauchen wir aber noch mehr Untersuchungen", bevor man ins Handeln übergeht, ::pr
    * FDA-Kommission über Zucker: Vorsitz aus der Zuckerindustrie
    * Täuschungen => Leiden (z. B. steigende Todesfälle wegen Diabetes; steigende Gesundheitskosten)
    * Japanische Methode: Gesundheitschecks- und Aufklärung, ::lösungsweg
        * ...
    * **Freier Wille?** -> ich lasse mir nichts vorschreiben (von der Regierung)
        ... nur seit 40 Jahren hat die Industrie Ihre Ernährung zu ihren Gunsten verändert. Wo ist da der freie Wille?
        Wer will schon die Regierung in der Küche? Niemand; außer es befindet sich dort schon was Gefährliches
    * https://www.heartandstroke.ca/get-healthy/healthy-eating/reduce-sugar = WHO-Empfehlung von 2015
        * "Heart & Stroke recommends you consume no more than 10% total calories per day from added sugars,
            and **ideally less than 5%**; that is, for an average 2,000 calorie-a-day diet,
            10% is about 48 grams (or 12 teaspoons **[6 teaspoons]**) of **added sugars**.
            **One can of pop** contains about 85% (or approx. **10 teaspoons**) of daily added sugar."
        * "Tips to reduce your sugar intake"
            * "1. Don’t drink your calories."
            * "2. Try whole foods."
            * "5. Cook at home more often."
            * "6. Read the Nutrition Facts table and the ingredient list on packaged foods."
    * 1h 19min: Beispiel EBL (Europäische Lebensmittelbehörde)
        * Fruktose wird als unbedenklich zugelassen, weil es einen niedrigen glykemischen Index hat (das sei klar, weil es eben nicht Glukose ist)
        * Teil des Problems
    * Credit Suisse-Bericht: fordert (höhere) **Steuern auf zuckrige Lebensmittel**, um steigende Diabetes- etc.-Kosten zu finanzieren
    * hat nichts dagegen, dass die Industrie Geld verdient; nur nicht wissentlich auf Kosten der Gesundheit anderer (so wie das Tabak- und Zuckerindustrie macht)

### Süße Kaffee-Getränke
* siehe Starbucks

Die 7 größten Mythen über Zucker und Übergewicht
------------------------------------------------
* https://www.foodwatch.org/de/informieren/zucker-fett-co/mehr-zum-thema/zucker-mythen/
    * "Mythos 1: „Der Mensch hat einen (Industrie-)Zuckerbedarf“"
    * "Mythos 2: „Zuckergetränke machen nicht dick“"
        * "Es herrscht ein breiter wissenschaftlicher Konsens darüber, dass ein erhöhter Konsum zuckergesüßter Getränke die Entstehung von Übergewicht fördert – sowohl bei Erwachsenen als auch bei Kindern."
        * ...
    * "Mythos 4: „Ernährungsbildung ist das beste Mittel gegen Übergewicht“"
        * "eine gesunde Lebensweise zu erleichtern, beispielsweise durch eine Änderung des Lebensmittelangebots, der Kennzeichnung oder des Marketings an Kinder"
    * "Mythos 6: „Lebensmittelsteuern zeigen nicht die gewünschte Wirkung“"
        * "Doch das Gegenteil stimmt: In Mexiko, Finnland, Berkeley oder auch Frankreich ging der Zuckergetränke-Konsum nach Einführung einer Limo-Steuer zurück. In Ungarn änderten 40 Prozent der Hersteller nach Einführung einer Steuer ihre Rezepturen."
    * "Mythos 7: „Jeder ist selbst für sein Gewicht verantwortlich. Wer staatliches Handeln fordert, hält die Verbraucher für unmündig“"
        * "Was logisch klingt, hat einen Haken: Wir leben in einer Welt, die dick macht. Es wird uns erschwert, die gesunde Wahl zu treffen."
        * "Hier mangelt es nicht an individueller Willenskraft. Hier mangelt es am politischen Willen, sich mit einer großen Industrie anzulegen."
    * [7 Seiten komplett](https://www.foodwatch.org/fileadmin/Themen/Zucker__Fett_und_Co/Dokumente/fw_Sieben_Mythen_zu_Zucker_und_Uebergewicht_final.pdf)

Welche Hersteller?
------------------
Um welche Hersteller handelt es sich konkret?

* Einfach selber beim Einkaufen auf die Zutatenliste schauen.
* Schauen, **welche Hersteller mit ihrer Werbung auf Kinder abzielen**
    * ["Werbung schauen macht Kinder dick. Kinderärzte fordern Werbebeschränkungen"](http://www.dgkj.de/presse/meldung/meldungsdetail/werbung_schauen_macht_kinder_dick_kinderaerzte_fordern_werbebeschraenkungen/), 2010
* im Internet suchen oder
    * ["WHO und Kinderärzte fordern Zuckersteuer auf Softdrinks"](https://www.aerzteblatt.de/nachrichten/70827/WHO-und-Kinderaerzte-fordern-Zuckersteuer-auf-Softdrinks), 2016, aerzteblatt.de; siehe auch [https://www.kinderaerzte-im-netz.de/news-archiv/meldung/article/kinder-und-jugendaerzte-fordern-zuckersteuer-auf-softdrinks-in-deutschland/](www.kinderaerzte-im-netz.de)
* z. B. https://www.foodwatch.org besuchen.
    * [Wie die Industrie aus Kindern Junkfood-Junkies macht](http://www.foodwatch.org/de/informieren/kinderernaehrung/2-minuten-info/) (2 min)
    * [Zucker, Fett etc. - Wenn Essen krank macht](http://www.foodwatch.org/de/informieren/zucker-fett-co/2-minuten-info/) (2 min)
* Das Praktische ist, dass die meisten dieser Hersteller sich auch an anderer Stelle eher rückschrittig verhalten, was die Motivation zur Alternativ-Auswahl erleichtert.
    * siehe auch [Hintergrund: Bekannte Marken namhafter Hersteller](../Hintergrund/marken-multinational.md)

Zuckerkartelle
--------------
* siehe https://metager.de/meta/meta.ger3?eingabe=zuckerkartell

Aussagen von Ärzten
-------------------
### 2018
* siehe "Appell an Regierung Ärzte fordern Zuckersteuer"

### 2016 Bundesärztekammer
* ["Montgomery: Bewusstsein für gesunde Ernährung möglichst früh fördern"](http://www.bundesaerztekammer.de/ueber-uns/landesaerztekammern/aktuelle-pressemitteilungen/news-detail/montgomery-bewusstsein-fuer-gesunde-ernaehrung-moeglichst-frueh-foerdern/), 2016
    * "Eine Kennzeichnung könnte die Unterscheidung zwischen gesunden und ungesunden Lebensmitteln erleichtern. Gerade Kinder und Jugendliche werden zum Konsum von ungesunden, überzuckerten Getränken und Snacks verführt. Daher ist es wichtig, das Bewusstsein für eine gesunde Ernährung so früh wie möglich zu fördern. Wir müssen bereits in den Kindergärten und Schulen vermitteln, dass gesunde, frische Lebensmittel ein Genuss sind. Diese Erfahrung ist die beste Grundlage, um sich auch als Erwachsener bewusst zu ernähren."

Verwandtes Thema: Rauchen
-------------------------
### Rauchen – Krankheit oder Lifestyle?
* ["Rauchen – Krankheit oder Lifestyle?"](https://www.aerzteblatt.de/archiv/65221/Rauchen-Krankheit-oder-Lifestyle), 2009
    * "Rauchen – Krankheit, nicht Lifestyle"
    * ...
    * "[...] und die Ärzteschaft fordern daher, Konsequenzen aus der Anerkennung des Rauchens als Abhängigkeitserkrankung zu ziehen."
    * "Darüber hinaus wäre die Anerkennung der Tabakabhängigkeit als Suchtkerkrankung durch die Kostenträger mit der Konsequenz einer Vergütung ärztlicher Leistungen [...] für den Raucher erstrebenswert."
        * z. B. dass auch Präventionsmaßnahmen von den Kassen unterstützt werden (denn die Folgekrankheitskosten belasten die Kassen erheblich)

### Widerlegung durch Gegenbeispiel
Im Gegensatz zur validen Verwendung von "Widerlegung durch Gegenbeispiel" ist "Beweis durch Beispiel" logisch nicht möglich, siehe z. B. "Mein kettenrauchender Opa ist 100 Jahre alt, und deswegen kann Rauchen ja gar nicht gesundheitsschädlich sein".

### Scheinargumente gegen ein konsequenteres Vorgehen gegen Zucker
z. B. Zuckersteuer

* Analog zu Anti-Rauchverbotsargumenten:
    * "Das Verbot von Rauchen in Kneipen hat dazu geführt, dass viel weniger Herzinfarkte passieren, dass weniger Leute passiv rauchen. Und was gab es da vorher für ein Geschiss, dass die Welt untergeht und die freie Marktwirtschaft bedroht ist und alle Kneipen zumachen - es ist nichts davon eingetreten."
        * aus ["Appell an Regierung Ärzte fordern Zuckersteuer"](https://www.tagesschau.de/inland/zucker-105.html)
            * "Es reicht, sagen mehr als 2000 Ärzte: Im Kampf gegen die Folgen des Zuckerkonsums müsse die Bundesregierung endlich handeln. Einer von ihnen ist Fernsehmoderator Eckart von Hirschhausen, der im Rahmen der Aktion "Ärzte gegen Fehlernährung" in einem offenen Brief an Bundeskanzlerin Angela Merkel Maßnahmen fordert, um vor allem Kinder vor zu viel Zuckerkonsum zu schützen"

Zusatzinfos
-----------
[Zusatzinfos](zusatzinfos.md)
