Kaffee / daily-choices-info-package
===================================

[zurück](../../..)

<!-- toc -->

- [Problematik](#problematik)
- [Lösung Nr. 1](#losung-nr-1)
- [Bringt Fair-Trade überhaupt was?](#bringt-fair-trade-uberhaupt-was)
- [Was ist Bio-Kaffee?](#was-ist-bio-kaffee)
- [Handlungsoptionen](#handlungsoptionen)
- [Fair-Trade-Kritik](#fair-trade-kritik)
  * [zeit.de, 2014, Fairtrade: Wenn Kaffee bitter schmeckt](#zeitde-2014-fairtrade-wenn-kaffee-bitter-schmeckt)
- [StarBucks](#starbucks)

<!-- tocstop -->

Problematik
-----------
* Erinnerung: Bohnen-Kaffee ist ein Luxusgut. Es ist nicht lebensnotwendig. Es gilt die Genussmittelabwägung, wenn die Bedingungen der Herstellung unklar sind.
* Warum eigentlich Fair-Trade-Kaffee: https://utopia.de/ratgeber/warum-sollte-man-eigentlich-fair-trade-kaffee-trinken (2015)
* Wo Fairtrade Kaffee kaufen: https://utopia.de/ratgeber/fairtrade-kaffee-kaufen (2015)

Lösung Nr. 1
------------
* Bohnenkaffee von der Liste des Anspruchsdenkens streichen. Getreidekaffee (z. B. Dinkel oder Lupine) tut es auch und ist gerechter.

Bringt Fair-Trade überhaupt was?
--------------------------------
* ["Kenia: Wie fair ist der faire Kaffee-Handel?"](http://www.daserste.de/information/politik-weltgeschehen/weltspiegel/sendung/wdr/weltspiegel-19012014-100.html), ARD-Weltspiegel, 2016
    * "Fairer Handel sieht anders aus. Fair wäre es, wenn Afrika seinen Kaffe selbst rösten, verpacken und vermarkten könnte."
    * Bewertung: Kaffee-Konsum reduzieren
* ["Welthandel - Hilft Fairtrade wirklich?](http://www.geo.de/natur/nachhaltigkeit/3066-rtkl-welthandel-hilft-fairtrade-wirklich)
    * aus ökonomischer Sicht; Fair-Trade-Konsum sei sogar kontraproduktiv
    * "Die Maßnahmen [zur Überwindung der Kinderarbeit] sollten an den wirtschaftlichen Ursachen ansetzen und reformorientierte politische Kräfte stärken, anstatt Erfolge durch ein verändertes Kaufverhalten zu erwarten."
    * "Daher ist auch Wirtschaftsförderung in Entwicklungsländern zugleich ein Schritt zur Überwindung der Kinderarbeit. Wenn die westlichen Industriestaaten dazu einen Beitrag leisten wollen, wäre der Abbau von Barrieren im Welthandel der beste Anfang: Entgegen den Aussagen vieler Globalisierungskritiker können nämlich Entwicklungsländer von einem freien Handel meist deutlich profitieren."
        * Bewertung:
            * Vorsicht, der Barrierenabbau findet oft einseitig statt, so dass sich die Lage nur noch verschlimmert.
            * Wachstumsdoktrin.
    * Bewertung:
        * also vor allem darauf achten, _von welchem Unternehmen_ man den Kaffee kauft (also rücksichtslose Unternehmenspolitik nicht mit dem eigenen Geld unterstützen)
        * wenn man sich nicht selber politisch engagieren will, dann zumindest den Kaffee-Konsum reduzieren
* ["Wie fair ist Fairtrade?"](https://uni.de/redaktion/wie-fair-ist-fairtrade), uni.de, 2013
    * Bewertung: Fair-Trade ist nicht optimal, aber es ist besser als nichts
* Weitere Infos, wie dem Konsumenten Fair-Trade oder sonstige soziale Ansätze **madig gemacht** werden und ihn mit einem **Gefühl der diffusen Ohnmacht** zurücklässt. **Und was man dagegen tun kann**.
    * [fairtrade-kritik](fairtrade-kritik.md)

Was ist Bio-Kaffee?
-------------------
* https://www.coffeeness.de/biokaffee/
    * in einer Gegenüberstellung mit Standardkaffeeanbau wird deutlich: Standard = Pestizide, Bio = keine Pestizide
        * weiterhin: Überschaubarere Plantagen, Mischkultur, Einsatz sogenannter Schattenbäume, Keine Kunstdünger & Pestizide, Ernte von Hand, Offizielle Biozertifikate
        * also Bio ist besser für die Umwelt (siehe Pestizide)
    * [Link zur EU-Ökoverordnung](http://www.bmel.de/SharedDocs/Downloads/Landwirtschaft/OekologischerLandbau/834_2007_EG_Oeko-Basis-VO.pdf?__blob=publicationFile)
* https://de.happycoffee.org/bio-kaffee/
    * "Im Unterschied zu konventionellem Kaffee wird Bio Kaffee nicht in strenger Monokultur, sondern auf so genannten Mischplantagen angebaut."
        * also Bio ist besser für die Umwelt / Artenvielfalt
* Liste mit Bio- und Fairtradekaffees: https://utopia.de/bestenlisten/bio-kaffee-fair-trade-kaffee/
* ["Umweltfolgen des Kaffeeanbaus"](http://www.kaffee-lexikon.net/umweltfolgen.html)
    * "Eine unerfreuliche Begleiterscheinung des Kaffeeanbaus ist der Einsatz von Pestiziden in den meisten Kaffeeplantagen in den Ländern Afrika, Asien, Süd- und Mittelamerika und den pazifischen Inseln. Dort werden große Mengen an gesundheits- und umweltschädlicher Pestizide eingesetzt. Diese belasten das Grundwasser und die Böden, gefährden die Gesundheit der Einwohner und die Artenvielfalt. Dies führt zu Bodenerosionen, die schützende Vegetationsschicht der Böden wird vernichtet und die Wasserqualität im nahen Umkreis von Kaffeeplantagen sinkt. Ein weiteres Problem ist die Rodung von Waldgebieten um den Ertrag zu steigern. Zugvögel finden in den baumfreien Plantagen keine Nistmöglichkeiten mehr, deswegen besteht kein Gleichgewicht zwischen Schädlingen und Nützlingen mehr, welches mit weiteren umweltschädlichen Pestiziden ausgeglichen wird."
    * mit Video
* ["Kaffeeanbau und die Umwelt"](https://www.drinkomat.de/kaffeeanbau_und_umwelt.html)
    * "Doch wir als Kaffeekonsumenten sollten uns bewusst sein, dass wir eine gewisse Verantwortung gegenüber den Produzenten und auch der Umwelt tragen. Die Tatsache, dass durch ökologischen Kaffeeanbau die Schäden für die Umwelt und auch für die Arbeiter, die sonst mit Pestiziden hantieren müssten, zumindest verringert werden können, sollte es uns wert sein, einen angemessenen, d.h. höheren Preis dafür zu bezahlen."

Handlungsoptionen
-----------------
* Einfachste und effektivste Lösung für alle: statt Bohnenkaffee auf nachhaltigen Getreidekaffee oder Wasser setzen
* Wenn überhaupt anonymen Weltmarkt-Kaffee kaufen, dann IMMER Fair-Trade-Siegel oder mit spezifischen Regeln vertrauenswürdiger Unternehmen (Beispiel Hand-in-Hand-Projekte von Rapunzel). Ja, es kann am Anfang ruhig etwas anstrengend sein; die unterprivilegierte Mitwelt ist aber darauf angewiesen.
    * Rapunzel lässt sich z. B. in die Karten schauen; Kunden-E-Mails werden freundlich und umfassend beantwortet
* Im Weltladen nach vorbildlichen Händlern und Projekten fragen.
    * Tipp: die bekannten Mainstream-Marken sind es nicht; insbesondere die von gut aussehenden Schauspielern beworbene Marke
* Herausfinden, welche wirtschaftlichen Strukturen für einen nachhaltigen und fairen Welthandel sorgen und sich dafür engagieren.

Fair-Trade-Kritik
-----------------
### zeit.de, 2014, Fairtrade: Wenn Kaffee bitter schmeckt
http://www.zeit.de/wirtschaft/2014-08/fairetrade-kaffee

"Wer Fairtrade-Kaffee trinkt, macht die Welt mit jedem Schluck ein bisschen besser. Stimmt, oder? Neue Untersuchungen zeigen: Wir machen uns was vor."

Bewertung:

* Die obige Aussage ist von vornerein falsch. Jeder Konsum macht die Welt ein bisschen schlechter.
* Je weniger konsumiert wird, desto besser.

User-Comment:

* "Schön und gut, aber was machen wir jetzt? ...auf Kaffee verzichten will ich nicht und das würde auchganz bestimmt keinen Bauern in Afrika helfen.
Da hätte der Artikel mal was beitragen können/müssen, anstatt es dem gut meinenden Kunden einfach nur madig zu machen. Also was machen wir jetzt?"
    * reply 1: "Sie könnten sich selbst informieren / Sprich, wie auch schon in anderen Kommentaren besprochen, wieder ein direktes Verhältnis zum Produzenten eingehen und so selbst eine gewisse Art der Kontrolle gewinnen. Bei Importware gestaltet sich dies natürlich schwieriger, ohne Vertrauen geht nicht viel. Doch auch hier kann ich mich entscheiden wem ich vertrauen will. Es gibt auch fairen Kaffee ohne "Fairtrade" Siegel, mit eigenen, spezifischeren Regelungen."
    * reply 2: "Danke für Ihre Frage! / Ich habe nämlich den Artikel auch so gelesen, eine Antwort auf die Frage wollen wir nicht liefern, **Hauptsache wir haben fairtrade madig gemacht - und damit alle Konsumenten-Bewegungen, die neben Marktwirtschaft auch parallel soziale Verantwortung, respektive soziale Marktwirtschaft in der Welt fördern wollen ... wenn man diesen Artikel unkritisch liest, liest man heraus, am Besten den Markt alleine alles regeln lassen - inklusive multinationaler Konzerne, globaler Marktmacht, egal was in den Ländern für Lebensbedingungen herrscht** ... und deswegen wird es weltweit nur noch schlechter"

StarBucks
---------
* Arte-Doku: "Starbucks ungefiltert - Die bittere Wahrheit hinter dem Erfolg | Doku | ARTE", https://www.youtube.com/watch?v=hC65nHwAg9g, 2018, 1h 30min
    * "28.000 Cafés in 75 Ländern: Die international tätige Starbucks Coffee Company wurde zu einem Symbol der Globalisierung. Die Marke ist in unserem Alltag präsent wie kaum eine andere. Gilles Bovon und Luc Hermann haben genauer untersucht, welche Strategien hinter dem Erfolg von Starbucks stehen. Und wie es hinter der Fassade des guten Images aussieht."
    * "Die gewaltige Marketingmaschinerie von Starbucks basiert auf sozialem Engagement und humanistischen Leitbildern ebenso wie auf der Betonung von Spitzenqualität und Verantwortungsbewusstsein den Kaffeeerzeugern und der Umwelt gegenüber."
    * ...
    * Strategie: Kunden fühlen sich gut, weil sie sich die hohen Preise leisten können
        * Werbung zunächst für die weiße Upper-Class (BMW, teure Markenklamotten);
            danach kamen die anderen nach, weil sie dazugehören wollen
    * Vornamen statt Zahlen
    * komplizierte Sprache => Gefühl, zum Club der Insider zu gehören
    * Ist die Starbucks-Erfahrung authentisch?
    * Hinter die Kulissen eines Cafes unter der Bedingung: den Angestellten keine Fragen stellen
    * zwei Monate Undercover als Neuling
        * Kunden sind wie Mitmenschen zu behandeln; immer freundlich zum Kunden sein; lächeln; zeigen, dass man für ihn da ist
        * Verbindung zum Kunden herstellen schwierig, weil alles schnell gehen muss (vs. Bio-Laden)
            * max. 3 Minuten / Kunde
            * Tipp: alle, die hier arbeiten, müssen ein bisschen wie Roboter funktionieren
            * Umsatz ist wichtiger als Kontakt zum Kunden
    * keine große Werbung, außer der Kaffeebecher in der Hand des Kunden
    * Dritter Ort, nur ohne politische Dimension
        * geräumig
        * gemütliche Sitze
        * man hält sich dort gerne auf
        * man kann dort sitzen, ohne was (nach?)kaufen zu müssen
        * in der Öffentlichkeit, aber allein
    * keine Putzfirma, Angestelle putzen selber
    * Gründung in Seattle innerhalb der 60er-Jahre-Hippie-Kapitalismus-Gegenkultur
        * nur Bohnenverkauf und zeigen, was guter Kaffee ist
        * dann neuer Marketingchef mit neuer Strategie
            * will expandieren
            * Kaffee vor Ort verkaufen
            * beides wollen die 3 Gründer nicht; wollen im kleinen Rahmen bleiben
        * kauft ihnen dann das Unternehmen ab, Logo wird keusch => multinationaler Konzern => aus mit der Gegenbewegung
    * der Kaffee soll in allen Ländern gleich schmecken
    * Angestelle werden Barista genannt, obwohl sie nur auf einen Knopf drücken müssen (im Ggs. zum echten Barista mit Tango; Tanz mit der Maschine)
        * für Schaumzeichnung bleibt keine Zeit
    * 31:30: neue, süße Produkte
        * dominiert den Weltmarkt mit süßen, sahnehaltigen Getränken
        * Übergrößen sind Norm; Gebäck mit sehr viel Zucker
    * die Getränke enthalten oft min. so viel Zucker wie eine Dose Cola, wenn nicht noch mehr
        * das größte Produkt enthält 3x mehr Zucker als die empfohlene Tagesdosis für einen Erwachsenen
        * Kunden: "heute hab ich mir das verdient"; die Kalorienbombe wird als Geschenk empfunden
    * Verkaufsgespräch: immer nochwas dazufragen, was aber extra kostet (und nicht dazusagen, weil das wäre ja geschäftsschädigend bzw. gewinnmaximierungsschädigend)
        * soviel zum Thema "der Kunde als Mensch"
        * Ziel: jedes Jahr 3 % mehr Umsatz
    * ca. 40:00 Soziales Engagement, wegen eigener Historie
        * jeder Mitarbeiter inkl. die ganz unten, Barista (= Partner), bekommen eine Krankenversicherung (einzigartig für Unternehmen dieser Größe)
        * Bezeichn. Partner, als Zeichen der Anerkennung
        * je nach Zugehörigkeitsdauer und Stellung: Aktien
        * finanziert Fernstudium
    * ca. 45:00 Interview mit Angestelltem, der 11 Jahre dabei ist
        * hoher Druck
        * immer was zu tun; Putzen; Augenkontakt zum Kunden etc. etc.
        * oftmals unterbesetzt, trotz hohem Gewinn
    * Interviews
        * studentischer Nebenjob; wird dauernd angerufen, wegen Vertretung und so.
        * man kommt nie pünktlich raus
        * Store Manager kann Arbeitszeiten flexibel für die Mitarbeiter festlegen
            (z. B. Reduzieren, falls die erfasste Leistung nicht so gut ist, wie erwartet)
    * (anti-racial-bias training: https://www.zerohedge.com/news/2018-06-01/uncomfortable-starbucks-employees-respond-becoming-worlds-biggest-public-toilet)
    * ...
    * ... 52:45...
    * ...

* https://de.wikipedia.org/wiki/Starbucks#Gesch%C3%A4ftspraktiken
    * "Der Zielkundschaft wird vermittelt, dass der Kaffee dieser Marke ein erschwingliches Prestige gehobener Gesellschaft ist, dass 99 % der eingekauften Kaffeebohnen aus fairem Handel stammen und der Konsum sich als intellektuell vereinbar mit der Konsumverweigerung der 68er-Bewegung darstellt. In der Ausstattung der Cafes werden eckige Tische vermieden, es wird angestrebt, dem Kunden ein Dritter Ort zu sein."
    * viel zuviel Zucker
