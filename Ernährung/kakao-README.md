Kakao / daily-choices-info-package
==================================

[zurück](../../..)

<!-- toc -->

- [Problematik](#problematik)
- [Positiv- und Negativbeispiele](#positiv--und-negativbeispiele)
- [Bio-Kakao](#bio-kakao)
- [Siegel](#siegel)
- [Fazit](#fazit)
- [Weiteres](#weiteres)

<!-- tocstop -->

Problematik
-----------
* DLF-Beitrag aus dem Bereich Wirtschaft, Feb. 2016:
    * Kakao wird hauptsächlich von Kleinbauern angebaut (Fläche je ca. 4 ha).
    * Es gibt zwei große Firmen, die zusammen **80% des Marktes der Beschaffung abdecken** (Cargill und Barry Callebaut). Danach kommt erst Nestle und Mars.
* [Kakao aus der Elfenbeinküste - Rohstoff als Fluch und Segen](http://www.deutschlandfunk.de/kakao-aus-der-elfenbeinkueste-rohstoff-als-fluch-und-segen.724.de.html?dram:article_id=309710), DLF 25.01.2015
    * Infos über Kakao und Kinderarbeit, Cargill-Firma
* [Rohstoff für Schokolade Millionen Kinder müssen auf Kakaoplantagen schuften](http://www.spiegel.de/wirtschaft/service/schokolade-kinderarbeit-auf-kakaoplantagen-nimmt-zu-a-1046525.html), Spiegel Online, 2015
* [Drohender Kakao-Mangel - Warum der Nikolaus fair gehandelt sein sollte](http://www.deutschlandradiokultur.de/drohender-kakao-mangel-warum-der-nikolaus-fair-gehandelt.1008.de.html?dram:article_id=338888), DLF 05.12.2015
    * [Kakao-Handel - Von Kinderarbeit und Zockern](http://www.deutschlandfunk.de/kakao-handel-von-kinderarbeit-und-zockern.1773.de.html?dram:article_id=305408), DLF 2014
* [Fairer Handel - Harte Arbeitsbedingungen auf afrikanischen Kakaoplantagen](http://www.deutschlandfunk.de/fairer-handel-harte-arbeitsbedingungen-auf-afrikanischen.772.de.html?dram:article_id=272481), DLF 2013
* [Kakao und Kinderarbeit - Afrikanische Kinder schuften für unseren Schokoladengenuss](http://www.deutschlandradiokultur.de/kakao-und-kinderarbeit.954.de.html?dram:article_id=235424), DLF 2013
* Doku: ["Schmutzige Schokolade II"](https://www.youtube.com/watch?v=1bmvJkBTp6g), 2012, ARD
    * Schokoladenindustrie, vor allem Nestle, siehe YT-Comments
    * TODO

Positiv- und Negativbeispiele
-----------------------------
* http://www.foodispower.org/schokoladenliste/
    * Liste von zu vermeidenden Herstellern
    * Positive Beispiele
    * http://appetiteforjustice.blogspot.de/2011/05/understanding-food-empowerment-projects.html

Bio-Kakao
---------
* für Kinder: http://www.schoko-seite.de/Schokolade/oeko.html
    * nicht so toll: "Gründe um eure Eltern zu überzeugen, dass Schokolade nicht ungesund ist"
        * besser wäre die Ergänzung "in Maßen"
    * siehe auch:
    * http://www.kuh-projekt.de/
    * http://www.baumwoll-seite.de/
    * http://www.bananen-seite.de/
* Schadstoffbelastung durch Pestizide: https://de.wikipedia.org/wiki/Kakao#Schadstoffbelastung
* ["Discounter oder Bio - die Preisfrage Lebensmittel müssten teurer sein"](http://www.tagesspiegel.de/politik/discounter-oder-bio-die-preisfrage-lebensmittel-muessten-teurer-sein/12928298.html), 2016
    * "Immer wieder heißt es: Warum ist "Bio" oder "Fairtrade" so teuer? Falsche Frage. Umgekehrt müsste es heißen, warum ist der Rest so billig? Ein Kommentar."
* Brot für die Welt: https://www.brot-fuer-die-welt.de/projekte/indonesien-bio-kakao/

Siegel
------
* 2016: Nach­haltig­keits­siegel im Test
    * https://www.test.de/Nachhaltigkeitssiegel-Koennen-Verbraucher-Fairtrade-Utz-Co-vertrauen-5007466-5008410/
        * "Den höchsten Stan­dard setzt das Siegel Natur­land Fair"
        * "Auch Fairtrade und Hand in Hand garan­tieren Bauern Mindest­preise für ihre Ernte, dazu Sonderzah­lungen"
        * Hand in Hand: "Siegel mit hoher Aussagekraft. Vereint bio und fair"
        * GEPA fair plus: "Der Test belegt: Gepa setzt auf direkte, lange Beziehungen mit Produzenten. Sie berück­sichtigt Wirkungs­analysen und kann Produkte zurück­verfolgen."
        * siehe auch https://utopia.de/stiftung-warentest-nachhaltigkeitssiegel-vertrauen-17737/

Fazit
-----
* Es gilt die Genussmittelabwägung: Wieviel Leid bin ich bereit für meine Gaumenfreude in Kauf zu nehmen?
* Erster Schritt:
    * kakao-haltige Produkte NUR kaufen, wenn mit Fair-Trade-Siegel
        * siehe z. B. Weltladen oder siehe Positiv-Liste oben
        * obige Negativ-Liste aktiv meiden
    * Vorteile sehen: Verzicht auf zuviel Schokolade hat auch gesundheitliche Vorteile

Weiteres
--------
* Doku: ["Schokolade - Von der Bohne zur Tafel | Wie geht das?"](https://www.youtube.com/watch?v=xxMYB8blQW0), NDR-Doku, 30 min, 2016, todo
