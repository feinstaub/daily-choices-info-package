Pestizid-Info
=============

<!-- toc -->

- [2019](#2019)
  * [Gift im System - Pestizidzulassungen in der EU, BR](#gift-im-system---pestizidzulassungen-in-der-eu-br)
- [Häufige Fragen](#haufige-fragen)
  * [Aber sind Pestizide nicht notwendig?](#aber-sind-pestizide-nicht-notwendig)
  * [Kriegt man ohne Pestizide die Menschheit überhaupt satt?](#kriegt-man-ohne-pestizide-die-menschheit-uberhaupt-satt)
  * [Warum tut sich die Gesellschaft so schwer?](#warum-tut-sich-die-gesellschaft-so-schwer)
- [Beispiel Glyphosat](#beispiel-glyphosat)
  * ["Das meist-untersuchte Herbizid"](#das-meist-untersuchte-herbizid)
  * ["Es gibt wissenschaftlichen Konsens, dass..."](#es-gibt-wissenschaftlichen-konsens-dass)
  * ["Eine Verbesserung zu früher"](#eine-verbesserung-zu-fruher)
  * ["Sonst müsste man wieder zurück auf schlimmere Mittel"](#sonst-musste-man-wieder-zuruck-auf-schlimmere-mittel)
  * ["Es hat eine Zulassung"](#es-hat-eine-zulassung)
  * [Gleise](#gleise)
- [Nachrichten zum Thema Pestizide](#nachrichten-zum-thema-pestizide)
  * [Stadt Dachau](#stadt-dachau)
  * [Gentechnik / Pestizid-Resistenzen](#gentechnik--pestizid-resistenzen)
  * [Pestizid-Resistenzen](#pestizid-resistenzen)

<!-- tocstop -->

Allgemein und am konkreten Beispiel Glyphosat. Siehe auch [Bio](../Bio)

2019
----
### Gift im System - Pestizidzulassungen in der EU, BR
* https://web.br.de/interaktiv/pestizide/
    * "Das europäische Zulassungsverfahren für Pestizide ist intransparent und fehleranfällig. Eine Datenanalyse des BR zeigt, wie Behörden Risikobewertungen der Industrie übernehmen. Ob die Gefährlichkeit eines Stoffes wirklich überprüft wurde, ist nicht nachvollziehbar.
    — von Eva Achinger, Renate Ell, Steffen Kühne und Lisa Wreschniok"

Häufige Fragen
--------------
### Aber sind Pestizide nicht notwendig?
In Ausnahmefällen sind sie möglicherweise ganz praktisch (was nicht heißt, dass es auch anders geht).

ABER:

* Pestizide sind Umweltgifte. Die Umwelt und die dortigen Lebewesen leiden automatisch.
* In der konv. Landwirtschaft werden Pestizide regelmäßig, vorbeugend und in riesigen Mengen eingesetzt
* Resistenzbildung ist nicht zu vermeiden. Die eingesetzten Mengen steigen immer weiter.

Lösung:

* Pestizide sollten Ausnahme und nicht die Regel sein. So macht man es im Ökolandbau.
    * keine Pestizide erlaubt (außer im Weinbau)

### Kriegt man ohne Pestizide die Menschheit überhaupt satt?
* Ja. Besser sogar als mit. Siehe Weltagrarbericht.

### Warum tut sich die Gesellschaft so schwer?
* Historisch bedingt: nach dem Krieg war eine schnelle / einfache / auf Masse ausgelegt Landwirtschaft von Vorteil
* Heute ist dieser Weg eine Sackgasse, siehe z. B. "Landwirtschaft mit Zukunft" (Video 6 min) vom Umweltbundesamt: https://www.umweltbundesamt.de/themen/umweltschutz-in-der-landwirtschaft
* Es ist ein Bildungsproblem. Denn
    * an den meisten Insituten wird konventioneller Landbau gelehrt. Die Absolventen **wissen nicht**, dass und wie man Landwirtschaft auch ohne Pestizide betreiben kann. Das liegt dann jenseits der Vorstellungskraft
    * wer eine Ausbildung in Ökolandbau absolviert hat, der weiß gar nicht, wie man mit Pestiziden wirtschaften soll. Sondern hat das nötige Wissen wie es ohne geht
    * Ausbildung schwer zu finden:
        * ["Ausbildung im Ökolandbau"](https://www.oekolandbau.de/erzeuger/ausbildung)
        * ["Mehr Ökolandbau in der Ausbildung"](https://www.bildungsserveragrar.de/service/zeitschrift-bub-agrar/online-spezial/online-spezial-archiv/mehr-oekolandbau-in-der-ausbildung/), 2017
        * ["Freie Ausbildung im biologisch-dynamischen Landbau"](http://www.freie-ausbildung-nrw-hessen.de/index.php)

Beispiel Glyphosat
------------------
* Video: ["Wie gefährlich ist Glyphosat?"](https://www.swr.de/natuerlich/pflanzengift-glyphosat-der-aktuelle-stand/-/id=100810/did=17511516/nid=100810/sotagm/index.html), 5 min, SWR, 2017
    * "Die Unkrautbekämpfung mit Glyphosat ist noch immer weit verbreitet. Nicht nur Pflanzen, Insekten und Amphibien leiden unter den Folgen, auch der Mensch scheint betroffen."
    * ...
    * Missbildungen bei Schweinen
    * sich widersprechende Studien
    * ...
* 2016, br.de: Artikel: ["Die unendliche Glyphosat-Story"](http://www.br.de/br-fernsehen/sendungen/unkraut/dokthema-glyphosat-entscheidung-186.html)
    * Video
    * Umfangreiche Infos im Artikeltext, u. a.:
        * im Urin nachgewiesen
        * Anwendung: "in Deutschland wird Glyphosat vor allem vor der Saat auf die Äcker gesprüht, denn das erspart das Pflügen" (ARGH)
        * Gründe: "Denn es ist billiger als jedes andere Herbizid."
        * Wirkweise und Gentechnik
        * "Immer wieder werden Schafe seiner Herde krank."
        * "Viele Landwirte haben schlichtweg Angst, es ohne Glyphosat nicht zu schaffen."
            * Lösung: Bio-Landwirtschaft
        * Pro-Glyphosat-Organisation "AG Glyphosat" kommt zu Wort
        * Zahlen: "700.000 bis 800.000 Tonnen Glyphosat werden jährlich weltweit auf die Felder ausgebracht." (2016)
        * Zahlen: "In Deutschland werden pro Jahr ungefähr 6.000 Tonnen Glyphosat auf die Felder ausgebracht. Das ist sechsmal so viel wie noch im Jahr 2001." (2016)
        * "40% der deutschen Äcker werden mit Glyphosat behandelt "
        * "Nur bei 0,05 % aller Lebensmittelproben wird auch auf Glyphosat kontrolliert." !!!
        * "Doch was kann nun jeder Einzelne von uns tun? Nicht viel. Glyphosat-belastete Nahrungsmittel meiden ist nicht so einfach. Schon alleine, weil die wenigsten darauf getestet wurden. Für die kritischen Konsumenten hat Agrarhändler Josef Feilmeier einen Tipp: Einfach mal beim Bäcker nachfragen, ob in seinem Brot Glyphosat drin wäre. Das wird er nicht beantworten können. Dann geht man - mit der Ansage, dass man erst wieder kaufen würde, wenn er das wüsste. Wer weiß, vielleicht denkt dann der eine oder andere um. Und lässt seine Lebensmittel zumindest mal testen."
* Pestizide sind neben den negativen gesundheitlichen Aspekten für den Menschen eine der größten Bedrohungen für die Biodiversität. Glyphosat ist nicht nur gesundheitsschädlich, sondern schädigt auch nachweislich das Bodenleben (Quelle: ["Regenwürmer auf Rückzug"](http://www.ardmediathek.de/tv/Unser-Land/Regenw%C3%BCrmer-auf-R%C3%BCckzug/BR-Fernsehen/Video?bcastId=14912650&documentId=35468728), Bayrischer Rundfunk, 2016)
* Video: ["Manipulation bei Glyphosat-Studien?"](https://www.zdf.de/nachrichten/heute-plus/videos/glyphosat-gekaufte-studien-100.html), vom 23.03.17, ZDF, 2 min
* im Bier nachgewiesen
* Resistenzen
* PDF vom ZDF: https://www.zdf.de/ZDF/zdfportal/blob/38532504/1/data.pdf, broken-link
* Video: [Missbildungen in Schweineställen wegen Glyphosat](http://www.infosperber.ch/Umwelt/Glyphosat-Missbildungen-in-Schweinestallen-2)

### "Das meist-untersuchte Herbizid"
* Stimmt das überhaupt?
* Urheber der meisten Studien?
    * Wieviele negative Studien wurden gar nicht veröffentlicht?
    * Wieviele davon positiv/negativ?
* Wenn etwas Schädliches häufig untersucht wird, wird es dann unschädlicher?

### "Es gibt wissenschaftlichen Konsens, dass..."
* zu was? laut dem Video von 2017 oben widersprechen sich sogar die Studien verschiedener UN-Behörden
* viel Geld ist im Spiel
* viele Studien direkt von den Herstellern

### "Eine Verbesserung zu früher"
* Mag sein, aber vielleicht ist es nun Zeit für eine weitere Verbesserung

### "Sonst müsste man wieder zurück auf schlimmere Mittel"
* Wer sagt denn, dass man das muss? Wenn man will, kann man auch ganz ohne.

### "Es hat eine Zulassung"
* Wenn man die Studien zur möglichen Krebserregung ernst nimmt, dann dürfte es keine Zulassung geben, weil in Europa das Vorsorgeprinzip gilt.

### Gleise
* 2018: [Deutsche Bahn ist größter Glyphsat-Einzelabnehmer](http://www.fnp.de/nachrichten/politik/Protest-gegen-Glyphosat-Einsatz;art673,2876472), 2018
    * Positive Beispiele: "Anlass für den offenen Brief, den alle Landtagsfraktionen der Grünen bundesweit unterzeichneten, ist eine Ankündigung der Österreichischen Bundesbahn (ÖBB), aus der Nutzung von Glyphosat auszusteigen. Die ÖBB habe bereits in den vergangenen drei Jahren die eingesetzte Menge Glyphosat auf 4,7 Tonnen halbiert. Man wolle „möglichst schnell und deutlich vor Ablauf der EU-Frist von fünf Jahren“ aus der Verwendung von Glyphosat aussteigen, sagte ÖBB-Chef Andreas Matthä."
* 2017: ÖBB: https://derstandard.at/2000070346074/Glyphosat-OeBB-plant-moeglichst-raschen-Ausstieg
    * ""Wir werden möglichst schnell und deutlich vor Ablauf der EU-Frist von fünf Jahren aus Glyphosat aussteigen" - derstandard.at/2000070346074/Glyphosat-OeBB-plant-moeglichst-raschen-Ausstieg"
* 2018: DB: https://community.bahn.de/questions/1650983-beendet-db-einsatz-glyphosat
    * "Ausgelassen werden Bereiche in Schutzgebieten, über offenen Gewässern, Durchlässen, Brücken, und bei geplantem Gleisumbau (Schutz der Mitarbeiter). In der aktuellen Anwendung befinden sich die Wirkstoffe Glyphosat, Flumioxazin und Flazasulfuron."
    * Alternativen? -> "Sie sind jedoch keine Alternative für Glyphosat. So dauert u. a. die Durcharbeitung deutlich länger und die Energiebilanz ist schlechter."
        * Wieviel länger? Auswirkung? Wieviel schlechter?

Nachrichten zum Thema Pestizide
-------------------------------
### Stadt Dachau
* ["Die Stadt Dachau verbietet Glyphosat"](https://utopia.de/dachau-verbietet-glyphosat-51445/), Mai 2017
    * "Das umstrittene Pflanzengift Glyphosat darf nicht mehr auf Ackerflächen der Stadt Dachau ausgebracht werden."
    * "Nach Dresden verbietet nun auch die Stadt Dachau den umstrittenen Unkrautvernichter Glyphosat."

### Gentechnik / Pestizid-Resistenzen
* ["Gentechnik: Super-Unkraut resistent gegen Vernichtungsmittel"](https://deutsche-wirtschafts-nachrichten.de/2014/02/02/gentechnik-super-unkraut-resistent-gegen-vernichtungsmittel/), 2014
    * "Zahlreiche US-Bauern stehen vor dem Problem, nichts mehr gegen Unkraut auf ihren Feldern ausrichten zu können. Die Pflanzen sind resistent gegen das Vernichtungsmittel Roundup von Monsanto. Millionen Hektar sind betroffen. Die Industrie setzt nun auf ältere Vernichtungsmittel, die jedoch noch gesundheitsgefährdender sein sollen."

* Kontrolle: https://de.wikipedia.org/wiki/Percy_Schmeiser

### Pestizid-Resistenzen
* Auch hierzulande gibt es bereits einige "Ackerunkräuter", die resistent gegen fast alle Herbizide geworden sind.
    * Z. B. der Ackerfuchsschwanz: https://www.agrarheute.com/pflanze/getreide/diese-herbizide-helfen-ackerfuchsschwanz-getreide-519827, 2016
        * "Für eventuell nötige Nachbehandlungen gegen Ackerfuchsschwanz in der Wintergerste kommt nur Axial infrage. Im Winterweizen stehen auch die Alternativen Atlantis oder Broadway zur Wahl." (schön klingende Namen)
        * "In der Praxis fällt die Wahl aufgrund des einfacheren Handlings oft auf Broadway. Sobald aber resistenter Ackerfuchsschwanz stark auftritt, zeigt sich bei vergleichbarem Aufwand ein Leistungsvorteil für Atlantis in der Größenordnung von etwa zehn Prozent Wirkung."
        * "Gegen resistenten Ackerfuchsschwanz gibt es also derzeit keine Alternative zu Atlantis. Je nach Resistenzintensität und Besatzdichte lässt sich die Aufwandmenge auf bis zu 500 g/ha aufziehen und in Extremfällen noch mit einer Zugabe von Ammonium-Harnstofflösung (AHL) verschärfen. Unter solchen Verhältnissen spielen Kosten und Kulturverträglichkeit fast keine Rolle mehr."
        * "Es zeigt sich aber auch das hohe Risiko für viele Flächen mit einer schleichenden Resistenzentwicklung, denn nach Atlantis ist bisher kein wirksameres Präparat in Aussicht."
* https://www.julius-kuehn.de/media/Veroeffentlichungen/Flyer/Ackerfuchsschwanz.pdf
    * "Hohe bis sehr hohe Ungrasdichten beinhalten immer einen
    gewissen Anteil natürlich resistenter Biotypen in einer Popula-
    tion. Der ständige Herbizideinsatz mit gleicher Wirkungsweise
    verursacht einen Selektionsdruck, der dazu führt, dass sich über
    einen längeren Zeitraum hinweg Nachkommen der resistenten
    Pfl anzen bevorzugt vermehren und die noch sensitiven weitge-
    hend verdrängen (‚Shifting’)."
    * "Herbizidresistenz bei Acker-Fuchsschwanz ist die Folge einseiti-
    ger Anbausysteme. Sie wird selektiert, wenn hohe Ungrasdich-
    ten ständig mit Herbiziden derselben Wirkungsklasse bekämpft
    werden. Hohe Ungrasdichten müssen durch vorbeugende
    ackerbauliche Maßnahmen gesenkt werden.
    Im Rahmen des Herbizideinsatzes muss sowohl in dem jeweili-
    gen Fruchtfolgeglied als auch in der gesamten Fruchtfolge
    ein konsequenter Wechsel der Wirkstoff  klassen durchgeführt
    werden."
* Was ist Atlantis
    * https://cropscience.bayer.co.uk/our-products/herbicides/atlantis-wg/#
        * "As the leading post-emergence herbicide for use on black-grass in winter wheat, **we recommend that you put Atlantis WG at the centre of your black–grass control strategy**. Used as part of a programme – preferably in the autumn – it offers good control of both black-grass and rye-grass."
    * https://agrar.bayer.de/de-DE/Produkte/Pflanzenschutzmittel/Atlantis%20WG
        * 50 Bonuspunkte kg/l
        * Datenblatt
            * "Haut- und Körperschutz: Standard-Overall und Schutzanzug Kategorie 3 Typ 5 tragen.
            Bei dem Risiko einer signifikanten Exposition ist ein höherwertiger
            Schutzanzug in Betracht zu ziehen.
            Möglichst zwei Schichten Kleidung tragen: Unter einem
            Chemieschutzanzug sollte ein Overall aus Polyester/Baumwolle oder
            reiner Baumwolle getragen werden. Overalls regelmäßig
            professionell reinigen lassen."
