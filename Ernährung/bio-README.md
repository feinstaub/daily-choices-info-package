Bio / daily-choices-info-package
================================

[zurück](../../..)

<!-- toc -->

- [Konventionelle Landwirtschaft - gängige Praxis](#konventionelle-landwirtschaft---gangige-praxis)
  * [Entlaubungsmittel auf Kartoffeln](#entlaubungsmittel-auf-kartoffeln)
  * [Feldbestellung: Unkraut chemisch abspritzen statt mechanisch umpflügen](#feldbestellung-unkraut-chemisch-abspritzen-statt-mechanisch-umpflugen)
  * [2016: Grundwasser: Die Überdüngung ist weiterhin ein Problem](#2016-grundwasser-die-uberdungung-ist-weiterhin-ein-problem)
  * [Gülle](#gulle)
  * [Gewässerschutz](#gewasserschutz)
  * [Pestizide - Gift für die Artenvielfalt (DLF-Artikel) / Artensterben](#pestizide---gift-fur-die-artenvielfalt-dlf-artikel--artensterben)
  * [Bodenfruchtbarkeit](#bodenfruchtbarkeit)
  * [Obst, speziell Äpfel](#obst-speziell-apfel)
  * [Abdrift](#abdrift)
  * [Weinbau](#weinbau)
  * [Bio-Weine](#bio-weine)
  * [Versteckte Kosten](#versteckte-kosten)
  * [Politik: nicht-nachhaltige Förderstrukturen](#politik-nicht-nachhaltige-forderstrukturen)
- [Einsteiger](#einsteiger)
  * [2021 - Vorurteil: Ökologische Landwirtschaft ist ineffizient](#2021---vorurteil-okologische-landwirtschaft-ist-ineffizient)
  * [2020 - Obstanbau](#2020---obstanbau)
  * [2019 - mit den Menschen](#2019---mit-den-menschen)
- [Was bringt es, Bio-Essen zu kaufen](#was-bringt-es-bio-essen-zu-kaufen)
  * [Warum kommt der Bio-Landbau eigentlich ohne Pestizide aus?](#warum-kommt-der-bio-landbau-eigentlich-ohne-pestizide-aus)
  * [Für Einsteiger mit Vegan ist ungesund](#fur-einsteiger-mit-vegan-ist-ungesund)
  * [Gesundheit: Gefährliche Pestizide (2018)](#gesundheit-gefahrliche-pestizide-2018)
  * [Gesundheit: Pestizide im Obstanbau (2019)](#gesundheit-pestizide-im-obstanbau-2019)
  * [Überblicksbeitrag vom Umweltbundesamt (2017)](#uberblicksbeitrag-vom-umweltbundesamt-2017)
  * [FAQ Umweltinstitut zum Ökolgischen Landbau](#faq-umweltinstitut-zum-okolgischen-landbau)
  * [2017: Pestizide vermeiden (Weltagrarbericht)](#2017-pestizide-vermeiden-weltagrarbericht)
  * [Hintergrund: Warum ist Artenvielfalt überhaupt wichtig? (Planetary Boundaries)](#hintergrund-warum-ist-artenvielfalt-uberhaupt-wichtig-planetary-boundaries)
  * [Infos vom BUND Berlin](#infos-vom-bund-berlin)
- [Kann Bio die Welt ernähren? / Kann Bio Europa ernähren?](#kann-bio-die-welt-ernahren--kann-bio-europa-ernahren)
  * [2019](#2019)
  * [Filme](#filme)
  * [2018...](#2018)
- [Herkunft, Einfluss des Konsumenten](#herkunft-einfluss-des-konsumenten)
  * [Wo kommt was her?](#wo-kommt-was-her)
  * [Einfluss des Konsumenten / Nahrungsinstinkt? (Harald Lemke)](#einfluss-des-konsumenten--nahrungsinstinkt-harald-lemke)
- [Spezialfall Tierhaltung](#spezialfall-tierhaltung)
  * [Auch "Bio-Tiere" leiden leider (wenn auch teilweise weniger und teilweise umweltfreundlicher)](#auch-bio-tiere-leiden-leider-wenn-auch-teilweise-weniger-und-teilweise-umweltfreundlicher)
  * [Honig](#honig)
- [Sorten / Saatgut / Gesunder Boden](#sorten--saatgut--gesunder-boden)
- [Positive Beispiele](#positive-beispiele)
- [Permakultur](#permakultur)
  * [Vegane Permakultur - Was ist Nachhaltigkeit?](#vegane-permakultur---was-ist-nachhaltigkeit)
  * [Grass sucks, food is better. Make a food forest.](#grass-sucks-food-is-better-make-a-food-forest)
- [Häufige Fragen](#haufige-fragen)
  * [Kann der Ökolandbau uns alle ernähren? / Welternährung](#kann-der-okolandbau-uns-alle-ernahren--welternahrung)
  * [Was ist mit Kontrolle?](#was-ist-mit-kontrolle)
  * [Regional vs. Bio?](#regional-vs-bio)
  * [Konflikt von Trog und Teller](#konflikt-von-trog-und-teller)
- [Hindernisse](#hindernisse)
- [Fazit](#fazit)
  * [Zusatzinfos](#zusatzinfos)

<!-- tocstop -->

Informationen zur Abwägung für Bürger und verantwortungsvolle Entscheider in höheren Positionen.

Konventionelle Landwirtschaft - gängige Praxis
----------------------------------------------
* Sektion um Dinge ins Bewusstsein zu rufen, die sonst bei täglichen Entscheidungen fehlen
* "konventionell" = das, was begann, als die Pestizide und Düngemittel billig wurden, ab ca. 1950

### Entlaubungsmittel auf Kartoffeln
* Video: ["Entlaubungsmittel in der Landwirtschaft"](http://www.ardmediathek.de/tv/alles-wissen/Entlaubungsmittel-in-der-Landwirtschaft/hr-fernsehen/Video?bcastId=3416170&documentId=45246394), ARD, 2017, (45246394).mp4), 6 min, tag:class2019, tag:offline
    * Diquat Südhessen
    * Zahlen: "Jährlich landen etwa 250 Tonnen Diquat auf unseren Feldern"
    * Mehr
        * https://de.wikipedia.org/wiki/Chlorpropham
            * "Chlorpropham ist ein im Rahmen der Rückstands-Höchstmengenverordnung in Deutschland zugelassener Keimungshemmer für die Behandlung von Kartoffeln zum Zwecke der Haltbarmachung nach der Ernte.[5] Es besteht Kennzeichnungspflicht, der Stoff muss aber nicht namentlich genannt werden."
        * https://de.wikipedia.org/wiki/Diquatdibromid
            * "Die Anwendung der Verbindung ist jedoch in Deutschland auf wenige Fälle eingeschränkt (Kartoffeln, Hopfen). Eine Anwendung in Naturschutzgebieten und Nationalparks ist verboten (der Stoff wird als besonders gefährlich für Bienen[8] und Wasserorganismen[9] angesehen)."
        * https://www.heise.de/tp/features/Chemiekeule-auf-dem-Kartoffelacker-3813603.html?seite=all, 2017
            * ...
            * wegen geforderter Kartoffelgröße => Thema Lebensmittelverschwendung
            * ...
* ["Giftige Entlaubungsmittel: Herbizide in der Landwirtschaft"](https://utopia.de/ratgeber/giftige-entlaubungsmittel-herbizide-in-der-landwirtschaft/)
    * "Ein häufig zur Krautabtötung eingesetzter Wirkstoff ist Diquat – er wird im Handel z. B. als Reglone vom Schweizer-Agrarkonzern Syngenta vertrieben", 2017
* ["Warum Kartoffeln „entlaubt“ werden"](https://www.volksstimme.de/lokal/schoenebeck/landwirtschaft-warum-kartoffeln-entlaubt-werden), 2017
* ["Chemische Entlaubung von Pflanzen und Obstbäumen"](http://www.deutschlandfunkkultur.de/wie-im-vietnamkrieg.993.de.html?dram:article_id=265325), 2013

### Feldbestellung: Unkraut chemisch abspritzen statt mechanisch umpflügen
* ... todo ...

### 2016: Grundwasser: Die Überdüngung ist weiterhin ein Problem
* Video: ["Neue Düngeverordnung: Weniger Nitrat im Trinkwasser | Unser Land | BR"](https://www.youtube.com/watch?v=l6S7z_Ujf4M), Doku-Ausschnitt, 6 min
* arte-Doku: "Der unsichtbare Fluss - Unter Wasser zwischen Schwarzwald und Vogesen" über Grundwasser, 2018, Serge Dumont
    * https://www.arte.tv/de/videos/073114-000-A/der-unsichtbare-fluss/
        * ...
        * "Bislang nie beobachtete Verhaltensweisen von Fischen und Vögeln konnte Serge Dumont, der an der Universität in Straßburg als Biologieprofessor lehrt, festhalten."
        * ...
        * " Die Feuchtgebiete im Oberrheintal verdanken ihre Existenz dem ständigen Zufluss von sauberem, schadstofffreiem Grundwasser.
            Das aber wird durch den übermäßigen Einsatz von Düngemitteln und Pestiziden in der Landwirtschaft zunehmend belastet, was die kleinen Paradiese gefährdet."

### Gülle
* https://www.vsr-gewässerschutz.de/projekte/
    * "Projekt Nitrat - Riesige Mengen an Gülle aus Massentierhaltungen und Gärresten aus Biogasanlagen belasten zusammen mit dem zusätzlich gekauften Mineraldünger unser Grundwasser."
    * 404 => https://www.vsr-gewässerschutz.de/nitratbelastung/

### Gewässerschutz
* VSR-Gewässerschutz e.V. - https://www.vsr-gewässerschutz.de
    * ["Bewusst Ernähren"](https://www.vsr-gewässerschutz.de/tipps-tricks/ern%C3%A4hrung/)
        * Foodsharing
        * https://mundraub.org/
        * Solidarische Landwirtschaft
        * https://www.ackerhelden.de/
        * Weniger Fleisch essen
        * Wochenmärkte und Hofläden
        * Biolebensmittel
        * Nachernte
    * 2018: Untersuchung von privaten Brunnen: viele nicht mehr zum Trinken geeignet, wegen Nitrat
        * Ursache: intensive Landwirschaft
        * Lösungswege: mehr ökologische Landwirtschaft
        * Leitungswasser trinken ist ok, weil es aufbereitet werden muss
            * wird sogar empfohlen, weil regional und besser für die Umwelt

    * Mehr Umwelt-Tipps: https://www.vsr-gewässerschutz.de/tipps-tricks/

### Pestizide - Gift für die Artenvielfalt (DLF-Artikel) / Artensterben
* http://www.deutschlandfunk.de/pestizide-gift-fuer-die-artenvielfalt.676.de.html?dram:article_id=313368 (Lesezeit: 5 - 10 Minuten), 2015
* Weiteres
    * ["Der Schwund an Arten ist dramatisch"](http://www.ardmediathek.de/tv/SWR-Aktuell-Baden-W%C3%BCrttemberg/Der-Schwund-an-Arten-ist-dramatisch/SWR-Baden-W%C3%BCrttemberg/Video?bcastId=254078&documentId=47327326), 2017, ARD, 3 min
        * Lösungswege sind sehr komplex. Alle sind gefragt.
        * Einseitige Schuldzuweisungen sind nicht angebracht
            * "Man muss unterscheiden zwischen 'den Bauern' und der 'Landwirtschaftspolitik'"
            * Sinngemäß: Nicht der einzelne Bauer an sich ist Schuld am Artensterben, sonderen eher die Art der modernen Landwirtschaft mit ihrem hohen Pestizid- und Düngeeinsatz und die politischen Rahmenbedingungen.
        * Menge an Pestiziden, die ausgebracht werden sinkt nicht, sondern sie steigt.
        * Problem: zu viel Düngeeinsatz

### Bodenfruchtbarkeit
* ["Florian Schwinns Kampf für die Rettung der fruchtbaren Böden"](https://www1.wdr.de/fernsehen/west-art/sendungen/florian-schwinn-100.html), 5 min
    * "Der Boden unter unseren Füßen wimmelt von Kleinstlebewesen"
    * https://www.westendverlag.de/buch/rettet-den-boden/
        * Buch: "Rettet den Boden! - Warum wir um das Leben unter unseren Füßen kämpfen müssen"
* und Glyphosat?
    * https://www.bfn.de/fileadmin/BfN/landwirtschaft/Dokumente/20180131_BfN-Papier_Glyphosat.pdf, 2018
        * "Die in diesem Papier aufgeführten Studien belegen, dass der Einsatz von glyphosathaltigen Pflanzenschutzmitteln erhebliche Auswirkungen auf die Biodiversität hat."
        * Mögliche Maßnahmen: "Zum Beispiel sollten Betriebe mit Anwendung glyphosathaltiger Pflanzenschutzmittel einen Mindestanteil an Fläche mit ökologischer Ausgleichsfunktion ohne jeglichen chemisch-synthetischen Herbizideinsatz nachweisenmüssen."
    * https://www.deutschlandfunk.de/glyphosat-mikroben-wuermer-und-insekten-leiden.740.de.html?dram:article_id=332279, 2015
        * "Desaströse indirekte Wirkung von Glyphosat"
            * "Wenn auf großen Teilen der Landschaft der gesamte Pflanzenbewuchs abgetötet wird, fehlt dem Ökosystem die Nahrungsbasis."
        * "Eine intensive Bodenbearbeitung hat nahezu einen gleichen Effekt, räumt auch die Pflanzen vom Acker."
            * "Das sollte man freilich nicht als Legitimation für ungestraftes Spritzen missverstehen."
            * "Denn es gibt Hinweise, dass die Umweltwirkung von Glyphosat noch weiter reicht als gedacht. Eine kürzlich erschienene Studie zeigt, dass der Wirkstoff auch der Bodenfauna das Leben schwer macht."
        * "dass wir zwei bis drei Wochen nach der Applikation dieses Herbizids einen völligen Rückgang der Regenwurmaktivität festgestellt haben."
        * "dass auch die Mykorrhiza im Boden stark dezimiert wurden. Mykorrhiza sind symbiontische Pilze, die an den Wurzeln der Pflanzen sitzen und ihnen dabei helfen, Phosphat aus dem Boden aufzunehmen."

### Obst, speziell Äpfel
* "Auf einer konventionellen Apfelplantage wird bis zu 15 Mal im Jahr gespritzt", https://www1.wdr.de/radio/wdr5/sendungen/leonardo/aepfel-pestizide-100.html, 2015
* "53 verschiedene Pestizide werden in europäischen Apfelanlagen gespritzt", http://www.natur.de/de/20/AEpfel-Lieblingsobst-mit-giftiger-Schattenseite,1,,1672.html, 2015
* Pestizidbelastung für Gesundheit und Umwelt (Insekten- und Vogelschwund), Arsenal an Pestiziden
* Alternativen: Äpfel von Streuobstwiesen oder von bio-zertifizierten Plantagen

### Abdrift
* Auch die Menschen in den betroffenen Anbaugebieten freuen sich über pestizid-freie Anbaumethoden
* Pestizid-Abdrift
    * https://www.global2000.at/abdrift-wenn-sich-pestizide-verirren
    * http://www.tageszeitung.it/2017/08/10/suedtirol-ist-pestizidtirol/
    * https://www.kreiszeitung.de/lokales/diepholz/bassum-ort51127/bassumerin-aergert-sich-ueber-pestizid-abdrift-ihrem-grundstueck-5704110.html
* Bio-zertifizierte Bauern bekommen Probleme, wenn das Gift vom Nachbarfeld rüberweht. (Schrot und Korn 05 2017)

### Weinbau
* "Die Traube wird von allen Obstsorten am meisten gespritzt". (Nach Äpfeln), Quelle: Zeitung 2017
* In einer konv. Weinflasche befinden sich Rückstände von bis zu 20 unterschiedlichen Pestiziden. Quelle?
* Hinweise:
    * Wein ist kein Lebensmittel.
    * Es gibt auch Bio-Wein.
    * Warum also - wenn überhaupt - nicht einfach immer Bio-Wein kaufen?

### Bio-Weine
* FAZ: https://www.faz.net/aktuell/stil/essen-trinken/warum-bioweine-so-unbekannt-sind-16773341.html, 2020
    * Warum Bio-Weine so unbekannt sind.
    * "Gespräch mit Biowein-Expertin : „Man darf die Umwelt nicht für ein Genussmittel verpesten“"
    * "Warum kauft die jüngere Generation denn keinen nachhaltig produzierten Wein?"
        Da gibt es mehrere Gründe. Manche wissen gar nicht, dass es Biowein gibt.
        Anderen ist nicht klar, was bei Biowein anders ist als bei normalem, also konventionell hergestelltem. Wein wird allgemein als sehr natürliches Produkt angesehen."
    * "Wenn man sich vor Augen hält, dass die Weinberge in Deutschland nur 0,7 Prozent der Agrarfläche ausmachen,
        aber für 20 Prozent der eingesetzten Pestizide stehen, spricht das für sich.
        Aber kaum jemand weiß das. Ich komme selbst aus der Pfalz, einer Weinregion, und hatte dafür auch keinen Blick."

### Versteckte Kosten
* Bei herkömmlichen Produktion von billigen Lebensmitteln entstehen Kosten, die nicht eingepreist sind, z. B.
    * Grundwasserverschmutzung mit Nitrat
    * Artensterben
    * negative Auswirkungen auf die Gesundheit des Menschen
    * (Schrot und Korn 05 2016)

### Politik: nicht-nachhaltige Förderstrukturen
* siehe z. B. ["Artensterben und abhängige Bauern - Bayer-Chef ist „Dinosaurier des Jahres 2016“"](https://www.nabu.de/news/2016/12/21706.html)
    * "„Das Geschäftsmodell Bayer-Monsanto profitiert von einer industriellen Landwirtschaft, die auf anachronistischen Förderstrukturen fußt und durch öffentliche Gelder künstlich aufrecht erhalten wird. Dabei trägt es dazu bei, diese Systeme mit ihren schädlichen Folgen für Mensch und Natur weiter zu manifestieren. Zudem ist davon auszugehen, dass die Abhängigkeit von Bäuerinnen und Bauern weltweit durch die führende Marktmacht von Bayer-Monsanto in den Bereichen Saatgut und Pestiziden weiter zunehmen wird. Die Saatgutvielfalt dürfte dagegen weltweit schrumpfen“"
* "Diese Strategie des „alles aus einer Hand“ mit auf einander abgestimmten Saatgut und Pestiziden treibt aber nicht nur die Bäuerinnen und Bauern zunehmend in die Abhängigkeit, sie forciert eine Intensiv-Landwirtschaft, die als Hauptverursacher des globalen Verlustes von Biodiversität gilt."
* "Bayer-Monsanto beherrschen gemeinsam zu einem Viertel den weltweiten Markt für Pestizide."
* "Der gemeinsame Saatgut-Anteil am Weltmarkt von Bayer-Monsanto liegt bei fast 30 Prozent."
* "Am Beispiel des Sojaanbaus in den USA wäre diese Entwicklung besonders drastisch ablesbar: hier besäße Bayer nach dem Zukauf von Monsanto nahezu 100 Prozent des angebauten gentechnisch veränderten Saatguts, für Mais läge die Marktmacht bei etwa 75 Prozent."
* "Bereich „Digital Farming“ führend ist, in Zukunft über enorme Datenmengen zur Beschaffenheit von Böden, Düngemengen, Saatgutmischungen und Pestiziden verfügen könnten und damit einen massiven Einfluss auf die landwirtschaftliche Produktion von Europa über Afrika bis Südamerika hätte."

Einsteiger
----------
### 2021 - Vorurteil: Ökologische Landwirtschaft ist ineffizient
* Die Aussage, dass Bio-Landwirtschaft "ineffizienter" ist, ist so nicht ganz richtig. Du meinst vermutlich den Ertrag pro Fläche. Das ist aber nur ein Aspekt von vielen, siehe z. B. https://de.wikipe...wirkungen. In der Gesamtbilanz ist eine ökologisch orientierte Landwirtschaft effizienter. Die heutige gängige Praxis der fossil- und umweltgift-basierten Landwirtschaft ist zwar jetzt scheinbar "effektiv", weil sie weniger kostet, aber das liegt eben daran, dass versteckte Kosten und Zukunftskosten nicht mit eingerechnet sind. Beispiel 1: Im Durchschnitt ist der Düngereintrag bei uns so hoch, dass unser Grundwasser mit Nitrat belastet wird (damit verstoßen wir sogar gegen EU-Recht). In anderen Ländern werden so die Meere eutrophiert, was dort die Ökosysteme belastet. Beispiel 2: Wenn ich einen fruchtbaren Boden habe mit 50 cm Humusschicht und meine "effiziente" Landwirtschaft trägt jedes Jahr 1 cm ab. Dann ist halt in 50 Jahren Schluss mit der "Effizienz".

* Tierprodukte
    * Bei tierischen Produkten reicht das Bio-Label für ein ruhiges Gewissen leider nicht aus. Beispielsweise werden bei 95 % der Bio-Eier (Code 0) die Küken am ersten Tag getötet. Auch im Bio-Bereich werden die Hühner alle nach 1,5 Jahren getötet, obwohl sie ca. 7 Jahre alt werden würden. Wenn es um Ethik geht, muss die erste Frage lauten, ob ich bestimmte tierische Produkte überhaupt brauche. Welche Bedürfnisse, die durch den Konsum tierischer Produkte befriedigt wird, kann ich auch anderweitig befriedigen?

    * Wer keine Tierprodukte zum Leben braucht, der wird auch im Bio-Bereich nur schwer etwas finden, was kein unnötiges Tierleid verursacht. Aber: Für die Pflanzenproduktion ist die Bio-Landwirtschaft sehr zu empfehlen. Denn wir haben massive Probleme in der konventionellen LW: Nitrat-Belastung des Grundwassers, Humusabbau und Bodenerosion, hoher fossiler Energieeinsatz und Dünger - insbesondere Phosphor - aus endlichen Quellen => irgendwann sind die Ressourcen aufgebraucht und dann ist Schluss.

* Bezüglich regional vs. bio: zumindest beim CO2 macht der Transport macht nur einen geringen Anteil an den Emissionen der Gesamtbilanz der Produktion aus: https://ourworldi...ing-local, 2020

* Bodenerosion, Biodiversität

* "Eine umfassende Studie hat die gesellschaftlichen Leistungen des Ökolandbaus im Vergleich zur konventionellen Landwirtschaft untersucht.
    Beim **Wasserschutz, der Bodenfruchtbarkeit und der Biodiversität** liegt der Ökolandbau vorn."
    https://www.topagrar.com/oekolandbau/news/oekolandbau-uebertrumpft-konventionell-im-pflanzenbau-10266540.html, 2019, "Ökolandbau übertrumpft Konventionell im Pflanzenbau"

### 2020 - Obstanbau
* Doku: "Obstanbau ganz ohne Chemie möglich? | ARTE Re:", https://www.youtube.com/watch?v=rIGdD4B7dsE, 2019, 30 min
    * "Äpfel und Wein zählen zu den Lebensmitteln, bei denen am meisten Pestizide eingesetzt werden. Dabei geht es auch anders."
    * Banker und Jurist, der nun Äpfel und Pflaumen anbaut
        * mit Hühnern statt Pestiziden
        * Kupfer nur deswegen, weil der Konsument derzeit keine Äpfel ohne Schorf kauft
    * Weinbauer aus der Pfalz, der auf robuste Sorten umgestellt hat; ohne Pestizide, dafür mit Backpulver
    * ...
    * "Der Schweizer Michael Dusong bringt mit seinem Versandhandel „Ugly Fruits“ optisch minderwertige Früchte auf den Markt."
    * ...

### 2019 - mit den Menschen
* #SaveBeesAndFarmers, Umweltinstitut München e.V.

Was bringt es, Bio-Essen zu kaufen
----------------------------------
Warum ist es bei jeder Gelegenheit besser, sich etwas Mühe zu machen und die Konsum-Wahl auf Produkte aus kontrolliert-biologischem Anbau fallen zu lassen? Besser wäre es natürlich, wenn die Landwirtschaft prinzipiell nach ökologischen Maßstäben umgesetzt würde, aber so weit ist es derzeit noch nicht.

### Warum kommt der Bio-Landbau eigentlich ohne Pestizide aus?
* F: Im konv. Landbau wird doch nur das allernötigste an Pestiziden angewendet, auch weil es Geld kostet.
    * A: in Summe ist es aber offensichtlich zu viel (siehe Artensterben etc.)

* F: Wie ist es möglich, ganz ohne Chemie Ertrag zu erwirtschaften? / Wie funktioniert Landwirtschaft ohne Pestizide?
    * Saatgut
        * Strapazierfähige Sorten / Langsameres Wachstum
    * Nützliche / Gleichgewichte / Arbeiten MIT der Natur und nicht dagegen
    * Fruchtfolgen
    * ...
    * Doku: ["Landwirtschaft ohne Chemie? Bauern suchen neue Wege"](https://www.swr.de/betrifft/landwirtschaft-ohne-chemie/-/id=98466/did=24510310/nid=98466/7tuhog/index.html), 2019, 45 min
        * "Die industrielle Landwirtschaft verspricht hohe Erträge und billige Preise durch den Einsatz großer Maschinen und viel Chemie. Gibt es Alternativen? Welches Modell der Bewirtschaftung ist umweltfreundlich und stellt sicher, dass die Bäuerinnen und Bauern davon leben können?"
        * TODO
* F: Höheres Risiko?
    * ...

### Für Einsteiger mit Vegan ist ungesund
* Video: https://www.youtube.com/watch?v=N3xLbDHif6g - "DARUM IST "BIO" SO WICHTIG", 10 min, vegan ist ungesund
    * in Kooperation mit Sonnentor
        * https://www.youtube.com/watch?v=rGYtCxSv8SY - "Vegan ist ungesund zu Besuch bei SONNENTOR"
            * Gründer Hannes
            * Permakultur
                * auf die nächsten 7 Generationen achten

### Gesundheit: Gefährliche Pestizide (2018)
* Buch: ["Die Pestizid-Lüge – Wie die Industrie die Gesundheit unserer Kinder aufs Spiel setzt"](https://www.oekom.de/nc/buecher/gesamtprogramm/buch/die-pestizidluege.html), von Andre Leu, 2018
    * https://www.ifoam.bio/en/andre-leu
    * entlarvt folgende Aussagen
        * 'Alle in der Ernährungsmittelindustrie eingesetzten Pestizide sind eingehend getestet worden und bewiesenermaßen unschädlich.'
        * 'Die Menge an Pestiziden in unserem Essen ist so gering, dass sie überhaupt keinen Effekt haben.'
        * 'Der Einsatz von Pestiziden ist unvermeidbar, wenn wir die ganze Weltbevölkerung ernähren wollen.'

* Siehe auch pestizid-info über die Verwendung älterer, noch schädlicheren Pestiziden bei Resistenzen

### Gesundheit: Pestizide im Obstanbau (2019)
* ...
* Gemüse: http://www.umweltinstitut.org/aktuelle-meldungen/meldungen/2019/gemuese-massiv-mit-pestiziden-belastet.html
* https://fragdenstaat.de/blog/2019/02/14/verklagt-uns-doch-bundesinstitut-will-glyphosat-gutachten-geheimhalten-wir-veroffentlichen-es/

### Überblicksbeitrag vom Umweltbundesamt (2017)
* "Landwirtschaft mit Zukunft" (Video 6 min) vom Umweltbundesamt: https://www.umweltbundesamt.de/themen/umweltschutz-in-der-landwirtschaft

### FAQ Umweltinstitut zum Ökolgischen Landbau
* http://www.umweltinstitut.org/fragen-und-antworten/oekolandbau.html
    * "Was ist ökologischer Landbau?"
    * "Was sind die Vorteile der ökologischen Landwirtschaft?"
    * "Kann der Ökolandbau die Weltbevölkerung mit ausreichend Nahrung versorgen?"
    * "Trägt die industrielle Landwirtschaft mit ihren hohen Hektarerträgen nicht maßgeblich zur weltweiten Ernährungssicherheit bei?"
    * "Ist die industrielle Landwirtschaft nicht die beste Art von Naturschutz, da sie durch mehr Effizienz weniger landwirtschaftliche Fläche benötigt?"
    * "Geht es Tieren auf Bio-Bauernhöfen wirklich besser?"
        * Anmerkung: oft geht es auch ganz ohne
    * "Ist eine ökologische Landwirtschaft auch eine faire Landwirtschaft?"

### 2017: Pestizide vermeiden (Weltagrarbericht)
"UN-Bericht fordert Abkehr von Pestiziden"

Man hört ja immer mal wieder, der Pestizid-Einsatz in der Landwirtschaft sei notwendig, um alle Menschen satt zu machen. Das ist leider immer noch falsch. Objektiv betrachet ist genau das Gegenteil der Fall.

Hier ist ein aktueller Artikel dazu: http://www.weltagrarbericht.de/aktuelles/nachrichten/news/de/32469.html (14.03.2017).

Z. B. steht da "Hilal Elver, die UN-Sonderberichterstatterin für das Recht auf Nahrung, [...] stellten am Mittwoch dem UN-Menschenrechtsrat einen Bericht vor, in dem sie den Pestizideinsatz und die Geschäftspraktiken der Hersteller scharf kritisieren. Jährlich enden 200.000 akute Pestizidvergiftungen tödlich, gerade in Entwicklungsländern mit niedrigeren Sicherheits- und Umweltstandards."

### Hintergrund: Warum ist Artenvielfalt überhaupt wichtig? (Planetary Boundaries)
Siehe Artikel zu den Planetary Boundaries: https://de.wikipedia.org/wiki/Planetary_Boundaries ("Es wurde von einem 28-köpfigen Wissenschaftlerteam unter Leitung von Johan Rockström (Stockholm Resilience Centre) entwickelt und 2009 in der Zeitschrift Nature publiziert.").

Dort ist das Artensterben der Aspekt mit dem größten Risikopotential:

![Artensterben](https://upload.wikimedia.org/wikipedia/commons/2/27/Oekologische_Belastungsgrenzen_planetary_boundaries.png "planetary boundaries aus Wikipedia")

(Bildquelle siehe Link)

### Infos vom BUND Berlin
* Einstieg: http://www.bund-berlin.de/bund_berlinde/hg_texte_startseite/bio_in_berlin.html
    * Fünf gute Gründe
    * Behauptungen von BIO-Skeptikern
* [Was man sonst noch tun kann](http://www.bund-berlin.de/bund_berlinde/hg_texte_startseite/bio_in_berlin/was_man_sonst_noch_tun_kann.html)
    * Solidarische Landwirtschaft
    * Vegetarismus und Veganismus
    * Fairtrade


Kann Bio die Welt ernähren? / Kann Bio Europa ernähren?
-------------------------------------------------------
### 2019
* ["Kann der Öko-Landbau Europa ernähren?"](https://www.heise.de/tp/features/Kann-der-Oeko-Landbau-Europa-ernaehren-4313239.html?seite=all), 2019, Susanne Aigner
    * "Mehrere aktuelle Studien vergleichen die Effekte von Öko- und konventionellem Anbau miteinander"
        * Nature-Studie: Bio ist schlecht fürs Klima: https://www.agrarheute.com/markt/marktfruechte/studie-oekolandbau-schlecht-fuer-klima-550610
        * "2018 werteten Dr. Eva-Marie Meemken und Prof. Matin Qaim von der Universität Göttingen [...] aus"
            * "In einer chemikalienbasierten Landwirtschaft hingegen sehen die Wissenschaftler auch keine nachhaltige Alternative. Stattdessen befürworten sie eine "intelligente Kombination aus beidem""
    * "Das Hauptproblem der beiden oben genannten Studien besteht darin, dass sie sich nur einseitig auf den Ertrag pro Ackerfläche beziehen. Viele andere Bezugsgrößen werden ausgeblendet. Zum Beispiel der Wasserschutz"
    * "Der ökologische Landbau schützt nicht nur Boden und Klima. Die auf Biobetrieben artgerechte Tierhaltung wird dem Tierwohl am ehesten gerecht. Zu diesem Schluss kommt eine umfangreiche Meta-Studie am staatlichen Thünen-Institut und sechs weitere Forschungsorganisationen, die die Leistungen von Ökolandbau und konventioneller Landwirtschaft für Umwelt und Gesellschaft miteinander verglichen."
        * verlinktes PDF: "Leistungen des ökologischen Landbaus für Umwelt und Gesellschaft", gefördert von der Bundesregierung, 2019
            * Herausgeber sind sind Dr. der Betriebswirtschaft und ein Professor für ökologische Agrarwissenschaften
            * Leistungsbereiche:
                * Wasserschutz
                * Bodenfruchtbarkeit
                * Biodiversität
                * Klimaschutz
                * Klimaanpassung
                * "Ressourceneffizienz: Die Ressourceneffizienz wurde am Beispiel der Stickstoffeffizienz (Stick‐stoffinput,  Stickstoffoutput,  Stickstoffsaldo,  Stickstoffeffizienz)  und  der  Energieeffizienz (Energieinput, Energieoutput, Energieeffizienz)"
                * Tierwohl
    * "Trotz aller positiven Effekte - auch im Ökolandbau könnten Nährstoffkreisläufe noch optimiert werden, erklären die Wissenschaftler des Thünen-Institutes" (logisch)
    * "In der Diskussion darüber, welche Anbaumethode die beste bzw. klimafreundlichste ist, wird selten das Problem der Lebensmittelverschwendung berücksichtigt: In ärmeren Ländern vergammeln Nahrungsmittel, weil sie nicht optimal gelagert oder transportiert werden. In Industrieländern landen sie im Müll, weil die Mindesthaltbarkeitsdaten abgelaufen sind. Der meiste Essensmüll in der EU entsteht in privaten Haushalten."
    * "Es geht also weniger darum, noch mehr Essen auf weniger Fläche mit noch mehr Pestiziden und Kunstdünger zu produzieren, sondern mehr darum, bereits erzeugte Lebensmittel besser zu nutzen und zu verteilen. Das ist vor allem eine logistische Herausforderung."
    * "Was würde passieren, wenn die gesamte europäische Landwirtschaft auf Öko-Landbau umgestellt würde? Würden wir Europäer verhungern? Im Gegenteil, sagt eine Studie, die im September 2018 am Institute for Sustainable Developement and International Relations (IDDR) veröffentlicht wurde" (Original-Studie leider nur Französisch)
        * "Der Ausbau agrarökologischer Infrastrukturen mit natürlichen Graslandschaften, Hecken, Bäumen, Teichen und steinigen Lebensräumen könnte 530 Millionen Menschen in den nächsten 30 Jahren auf nachhaltige Weise ernähren. Gleichzeitig würde sich der Ausstoß von Treibhausgasen in der Landwirtschaft um 40 Prozent verringern, während sich die Artenvielfalt erhöht."
        * "Bei einem Verzicht auf Pestizide und Mineraldünger läge der Ertragsrückgang je Erzeugnis bei 10 bis 50 Prozent. Trotzdem könnten noch landwirtschaftliche Produkte exportiert werden. Allerdings würden die Futtermittel-Importe zurückgehen, denn es würden viel weniger Tiere gehalten, die weniger Futtermittel verbrauchen."
        * "Der Ökolandbau könnte die Menschen in Europa bis 2050 also mühelos ernähren." (was ist danach?)

* Fragen an die Pestizid- und Acker-Chemie- und Tier-Arzneimittel-Industrie:
    * Warum werden Ackergifte und auf Monokulturen ausgerichtete Gentechnik von Nachhaltigkeitsforschern eher als Problem (z. B. für die Biodiversität), anstatt als Lösung dargestellt?
    * Gibt es eine schlüssige Erklärung, warum die Geschäftsmodelle, die auf die Produktion und den Verkauf von immer mehr Agrarchemikalien und Antibiotika ausgelegt sind, dafür sorgen sollen, dass eine Umstellung auf eine nachhaltige Landwirtschaft effektiv unterstützt wird?
    * Warum sollte die aktuelle - von den führenden Unternehmen des obigen Industriezweigs - aktiv geförderte Agrarwirtschaftsweise, die bei uns zu einem massiven Artensterben führt (mittlerweile auch gut sichtbar am Beispiel der Insekten und Vögel), in anderen Ländern besser funktionieren als bei uns?
    * Wie genau übernehmen Hersteller von Antibiotika, die für unsere Intensiv-Tierhaltung verwendet werden und damit zunehmend Resistenzen verursachen, Verantwortung? Eine Möglichkeit, die ich sehe, wäre aktiver Lobbyismus zur Reduktion unserer Tierbestandszahlen und hin zu Tierhaltungsformen, die kein oder nur minimal Antibiotika benötigen. Welche Ansätze dazu kommen aus dem Industrieumfeld?

### Filme
* 10 Millarden
* ARTE: "Wie schaffen wir die Agrarwende?" (siehe dort)

### 2018...
* http://fes-online-akademie.de//fileadmin/Inhalte/01_Themen/03_Nachhaltigkeit/dokumente/FES_OA_Debatte_Loewenstein_oL.pdf
    * "OnlineAkademie-Debatte: Kann ökologische Landwirtschaft die Welt ernähren?
* siehe z. B. Weltargrarbericht
* ["Kann Bio die Welt ernähren?"](https://www.bioland.de/im-fokus/hintergrund/detail/article/kann-bio-die-welt-ernaehren.html), 2017
    * "Die Frage, ob der ökologische oder der konventionelle Anbau "besser" ist, wird oft engagiert oder sogar verbittert geführt. Dabei ist schon die Fragestellung heikel."
    * schwer vergleichbar...
    * ...
* ["Kann Bio die Welt ernähren? - Ja - denn wir müssen die Welt nicht nur ernähren, sondern auch erhalten!](https://www.bioland.de/im-fokus/meinung/detail/article/ja-denn-wir-muessen-die-welt-nicht-nur-ernaehren-sondern-auch-erhalten.html)
    * "Das derzeitige System der globalen Landwirtschaft ist ein Irrweg. Es ist nicht nachhaltig. Es ist nicht widerstandsfähig gegen Störungen und gegen Entwicklungen wie den Klimawandel. Und - es ist ungerecht."
        * "Hans Rudolf Herren ist Präsident der Stiftung Biovision. Als Vorreiter einer ökologisch geprägten Landwirtschaft wurde er 2013 mit dem Right Livelihood Award ausgezeichnet, dem "Alternativen Nobelpreis". Er hat das Buch "So ernähren wir die Welt" geschrieben."
            * http://www.biovision.ch/home/
        * weniger Fleisch
* ["Bio für alle: So einfach ließe sich die Weltbevölkerung gesund ernähren"](https://www.geo.de/natur/nachhaltigkeit/18213-rtkl-oekologische-landwirtschaft-bio-fuer-alle-so-einfach-liesse-sich-die), 2018
* ["Kann Biolandbau die Menschheit ernähren?"](http://www.spiegel.de/wissenschaft/mensch/kann-oekologische-landwirtschaft-die-menschheit-ernaehren-a-1177968.html), 2017
    * "Ist es möglich, die komplette Landwirtschaft weltweit auf Öko-Anbau umzustellen? Ja, errechnen Forscher - dafür müssten wir allerdings unsere Ernährung umstellen."
        * Achtung, der Umkehrschluss "ohne Umstellung auf eine nachhaltige Wirtschaftsweise kann sich die Menschheit ernähren, ohne sich irgendwie umzustellen" ist nicht möglich!
    * "Ökologische Vorteile"
        * ...
    * (Das Ende des Fazits des Artikels ist typisch motivationshemmend gestaltet / Förderung der Ohnmacht des Einzelnen Perzeption)

Herkunft, Einfluss des Konsumenten
----------------------------------
### Wo kommt was her?
* siehe essen-herkunft.md

### Einfluss des Konsumenten / Nahrungsinstinkt? (Harald Lemke)
* Doku: ["Was wir morgen essen werden!"](https://www.youtube.com/watch?v=d6JcclXwg9s), arte, 2017
    * Buch: Politik des Essens - Wovon die Welt morgen lebt, von Harald Lemke
    * Buch: Ethik des Essens - Einführung in die Gastrosophie, von Harald Lemke
    * "Essen ist keine Privatsache wie uns das der Liberalismus vermittelt."
        * "Weil, jedesmal, wenn ich was esse, beeinflusse ich Prozesse draußen in der Welt."
        * "Essen bedeutet Welt zu konstituieren, Welt zu gestalten."
        * "Ich nehme Einfluss auf die Landwirtschaft, auf die bäuerliche Arbeit, welche Chemikalien auf den Acker kommen, wie die Arbeitsverhältnisse sind in Schlachthöfen"
        * "Ich nur sagen, als Konsument, mir ist das egal. Das ändert aber nichts an der Tatsache, dass ich in jedem Fall dazu Stellung nehme."
    * "Wir sind ja genau das Tier, was keinen angeborenen Nahrungsinstinkt hat."
        * "Wir müssen unseren ganzen Geist benutzen, um überhaupt artgerecht zu essen."
        * "Wir müssen Dinge verstehen. Wir müssen Zubereitungsarten erfinden."
        * "Wir müssen wahnsinnig viel Gehirnschmalz aufwenden, um uns menschengerecht zu ernähren."
        * Dadurch ist der Geist gewachsen?
    * ... todo ...
    * YT-User DrSelbstdenker: "Wer will sich das Festtagsmenü von Gedanken an Tierschutz vermiesen lassen? Gerade zum Weihnachtsfest denken wir so selten wie nie daran, wie die Bestandteile des Festtagbratens entstanden sind. Doch warum nicht einfach diese Gewohnheit ändern? Denn immerhin vollzieht sich aktuell ein struktureller Wandel der Ernährung."

Spezialfall Tierhaltung
-----------------------
### Auch "Bio-Tiere" leiden leider (wenn auch teilweise weniger und teilweise umweltfreundlicher)
* Bio-Tiere leiden in der Regel zwar weniger, aber es geht auch noch besser, siehe Bio-vegan
* Vorteil: kein konventionelles Futter
* Vorteil: strengere Tierwohlauflagen als bei konventionell
* Als Tierprodukt-Konsument hat man es aber auch wirklich nicht leicht, wenn man ethisch korrekt einkaufen will.
    * siehe z. B. [Bio-Eier](../Vegan/bio-eier.md)
* Lösung: Konsumseite:
    * [Vegan](../Vegan), Produzentenseite: [Biovegan](../Vegan/biovegan.md)
    * Wenn überhaupt Tierprodukte, dann mindestens nach Bio-Richtlinien
* Analogie "Mann schlägt Hund mit Stock": Ein Mann schlägt seinen Hund jeden Tag mit einem Stock. Wenn der Mann den Hund nun nur noch 1x die Woche schlägt, ist das für den Hund eine Verbesserung. Aber ist das wirklich ein ambitioniertes Ziel? Wäre es nicht besser den Hund gar nicht erst zu schlagen, weil es unnötig ist?
    * Die Analogie besteht darin, dass die Mehrheit der Menschen - wenn sie es nur probieren würden - merkten, dass sie auch ohne Nutzung von Tieren ein gutes Leben führen können.

### Honig
* Wenn überhaupt Honig, dann Bio-Honig.
    * Es gibt nämlich einiges an Quälerei, die bei Bio-Bewirtschaftung nicht auftritt, siehe http://www.bioimkerhonig.de/bienenhonig/ist-honig-vegan.php
* Doku: ["More than Honey - Bitterer Honig"](https://www.br.de/mediathek/video/preisgekroente-doku-ueber-das-bienensterben-more-than-honey-bitterer-honig-av:5c7e9479c590b0001cd411d2), 2012 Swiss documentary film, 90 min
    * "Preisgekrönte Doku über das Bienensterben"
        * "visually magnificent."
        * Eindrucksvolle (und verstörende) Bilder von der industriellen Bienenhaltung und Honigproduktion
    * "Suche nach den Ursachen des globalen Bienensterbens und fragt nach den Folgen für Mensch und Natur"
    * "Ohne die Bestäubungsleistung von Milliarden von Honigbienen käme ein Großteil unseres Obstes und Gemüses nie auf die Teller"
        * was ist mit Wildbienen?
    * ...
    * Bienenvölker werden mit dem LKW quer durch Amerika transportiert
    * Verbreitung von Krankheiten
    * Interview Imker in Amerika:
        * Bienentod von einem ganzen Volk. Unbekannte Ursache => "Früher hab ich mich gefühlt, wie bei einem Tod in der Familie. [...] Heute ist es ein Problem, das gelöst werden muss. Das gehört zum Geschäft."
        * nun schlägt er mit dem zurück was er hat: Chemie
        * ...
        * "Mein Opa würde sich entsetzt abwenden, wenn er sehen würde, wie wir heute Bienen halten. Er würde sagen, ihr habt eure Seele verloren."
    * Massenhaft Antibiotika und andere Medikamente für Honigbienen
    * ...
* Gift im Honie, https://www.arte.tv/de/videos/078229-005-A/re-gift-im-honig-tote-bienen/, 2019, 30 min
    * "nsektenvernichtungsmittel auf Nikotinbasis, sogenannte Neonicotinoide, gelten als besonders bienen- und insektenschädlich. Die Verwendung von drei Insektiziden aus dieser Klasse wurden in der EU deshalb 2013 befristet verboten. Doch was nutzen die Verbote, wenn EU-Mitgliedsstaaten wie Rumänien immer wieder Ausnahmegenehmigungen erhalten?"

Sorten / Saatgut / Gesunder Boden
---------------------------------
Für die Menge des benötigten Düngemittels und der Pestizide ist wichtig, welche Pflanzen-**Sorte** eingesetzt wird. Ist sie auf maximalen Ertrag gezüchtet? Oder ist auch Widerstandsfähigkeit gegenüber Mangel und Schädlingen ein Zuchtziel?

Konventionelle Sorten sind oft ertragreich, können aber ohne Dünger und Pestizide nicht überlegen.

Biosaatgut: siehe z. B. https://www.bingenheimersaatgut.de

* "Nachbaufähige, samenfeste Sorten. Keine Hybrid-Sorten, Patente, Gen- oder Biotechnologie."
* Warum ist samenfest wichtig?
    * https://de.wikipedia.org/wiki/Nachbau_(Saatgut)
    * https://de.wikipedia.org/wiki/Biosaatgut
    * https://voelkeljuice.de/unternehmen-stiftung/engagement/samenfest.html
    * ["Bio-Dinkel-Saatgutarbeit am Hofgut Oberfeld in Darmstadt"](http://alnatura-hilft.de/alnatura-hilft-e-v-spendet-fur-saatgutarbeit/)
        * "Ohne jeglichen Einsatz von Mineraldüngern oder Pestiziden hat sich die Pflanzenzüchterin zur Aufgabe gemacht den Dinkel züchterisch weiterzuentwickeln."
* Doku: ["Die Saatgutretter"](https://www.youtube.com/watch?v=Gz3oZOeY6FE), ARTE, 50 min
* Doku: ["Verbotenes Gemüse"](https://www.youtube.com/watch?v=g9Jfn5rQVPA), NDR, 45 min
    * (2017 von [User DrSelbstdenker](https://www.youtube.com/user/DrSelbstdenker/videos), todo)
    * "Wer alte Sorten anbaut, muss aufpassen: Mit dem Saatgut nicht zugelassener Pflanzensorten zu handeln, ist gesetzlich verboten. Warum tun drei Gemüsezüchter es trotzdem?"
    * todo
* [Trailer von Good food, bad food](https://www.youtube.com/watch?v=0gYG51zxODE), 2011
    * "Alle verfolgen sie ein gemeinsames Ziel: die Verbesserung der Bodenqualität und die Wiederherstellung der Saatenvielfalt - zum Schutz der Umwelt und für gesündere Lebensmittel."
    * "Coline Serreau zeigt in dem Dokumentarfilm "Good Food, Bad Food" Menschen, die dagegen kämpfen, dass unsere Böden durch chemische Dünger und Pestizide vergiftet werden. Und die sich dagegen wehren, dass nur wenige skrupellose Konzerne (u. a. Monsanto) weltweit das Saatgutangebot kontrollieren und die Bauern erpressen."
    * "begegnen wir faszinierenden Persönlichkeiten, die vielfältige Lösungen für die intelligentere Nutzung unserer begrenzten Ressourcen gefunden haben."
    * "knüpft dort an, wo Erwin Wagenhofers "We Fee The World" aufgehört hat."

Positive Beispiele
------------------
* Positive Beispiele: Demeter, Bioland, Naturland, EU-Ökoverordnung, Permakultur
* Positive Beispiele: 2017: GLS-Bank fordert ["Konsequente Abgabe auf Spritz- und Düngemittel"](https://www.gls.de/privatkunden/ueber-die-gls-bank/presse/pressemitteilungen/gls-bank-fordert-nachhaltigen-wandel/)

Permakultur
-----------
* todo: Intro

### Vegane Permakultur - Was ist Nachhaltigkeit?
* Video: https://veganpermaculture.org/opinions-debate/how-permaculture-can-save-humanity-and-earth-but-not-civilisation/, 2010, 60 min
    * "How Permaculture Can Save Humanity and Earth, But Not Civilisation"
    * siehe auch "Vegan 2019"
    * https://en.wikipedia.org/wiki/Toby_Hemenway
        * "spent ten years creating a rural permaculture site"
    * "How Permaculture Can Save Humanity and The Planet, but not Civilization."
    * What is Sustainability?
        * see UN, needs today and tomorrow
        * "Degenerative - Sustainable - Regenerativ"
    * ...
    * ...
    * ... TODO
    * ...
    * 30 min ff Engery history and future
    * ...
    * 44 min: "rebirthing a horticultural society"
        * hortus = plant
        * ager = field
        * a garden, not a farm
        * eine Hacke, kein Pflug

* http://tobyhemenway.com/videos/redesigning-civilization-with-permaculture/ - "Redesigning Civilization with Permaculture", 2013
    * ...
    * TODO
    * ...
    * ...

### Grass sucks, food is better. Make a food forest.
* Canadian Permaculture Legacy: https://www.youtube.com/channel/UCfz0O9f_Ysivwz1CzEn4Wdw, 2020, 12 min, Food Forest

Häufige Fragen
--------------
### Kann der Ökolandbau uns alle ernähren? / Welternährung
* ja, siehe oben

### Was ist mit Kontrolle?
* www.oekokontrollstellen.de, Kontrollstelle Prüfverein Verarbeitung ökologischer Landbauprodukte, Bundesverband der Ökokontrollstellen
* "Auch bei den Kontrollstellen gibt es Unterschiede" (Schrot und Korn Interview 10,2017), aber es wir kontrolliert.

### Regional vs. Bio?
* Wenn nicht beides gleichzeitig geht, dann lieber regional oder lieber Bio?

### Konflikt von Trog und Teller
* 2018:
    * https://albert-schweitzer-stiftung.de/aktuell/fao-kein-konflikt-von-trog-und-teller
    * Aufklärung von fehlinterpretierter Studie: ["Konkurrenz von Trog und Teller"](https://friederikeschmitz.de/echte-fake-news-keine-konkurrenz-von-trog-und-teller/), 2018
        * ["Keine Konkurrenz zwischen Trog und Teller?"](https://germanwatch.org/de/14894)
            * Zahlen: "In Deutschland landen laut Bundeslandwirtschaftsministerium (BMEL) sogar fast 60 % der Getreideernte  im Trog und weniger als ein Fünftel auf unseren Tellern." (Graphik des BMEL für ca. 2015)

Hindernisse
-----------
* Unwissenheit des Verbrauchers
* Vorgaben/Anforderungen des Einzelhandels
    * z. B. harte Preisvorgaben
    * z. B. Kartoffeln müssen gewaschen sein
    * ...
* Jeder Akteur kann etwas zur Verbesserung beitragen

Fazit
-----
* Ein möglichst hoher BIO-Anteil und das gleichzeitige Vermeiden "konventionell" produzierter Ware im persönlichen Warenkorb ist eine gute Sache und für jede Alters- und Einkommensgruppe sowie für jede Tageszeit nachdrücklich zu empfehlen.
* Großmutter-Effekt: "Was man heute Bio-Essen, nannte man früher einfach Essen" / "Früher wurden weniger Tiere pro Person konsumiert."
* Weiterhin ist das aktive Vermeiden von Produkten von großen (vor allem aus der Werbung) bekannten Herstellern, die wenig bis gar kein BIO im Programm haben, zielführend.
    * Das Praktische ist, dass die meisten dieser Hersteller, die fast ausschließlich konventionelle Ware anbieten, sich auch an anderer Stelle eher rückschrittig verhalten, was die Motivation zur Alternativ-Auswahl erleichtert.

### Zusatzinfos
* [Pestizid-Info](../Hintergrund/pestizid-info.md)
* [Zusatzinfos](zusatzinfos.md)
* [Rezeptsammlung](../Rezepte)
