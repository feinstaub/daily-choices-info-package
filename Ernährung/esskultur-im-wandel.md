Esskultur im Wandel
===================

Metzger, die vegan werden
-------------------------
* https://www.derwesten.de/panorama/metzger-wird-veganer-wegen-dieses-films-aenderte-er-sein-leben-radikal-id216831579.html, 2019
    * wegen Earthlings (2005)
    * "Und plötzlich war das Fleisch auf seinem Teller nicht mehr bloß Fleisch, sondern ein Tier mit einer Geschichte. Brian Kavanagh entschloss sich dazu, ab sofort vegan zu leben. Sensibilisiert dafür hatte ihn seine Frau Rebecca, die zuerst den Lebensstil frei von Tierprodukten wählte. Seitdem ernährt sich die Familie, auch die beiden Kinder (6 und 9), vegan."
    * "Und Brian Kavanagh hat auch schon selbst ein Produkt entwickelt: die klassisch schottische „Lorne-sausage“ (eine rechteckige Wurst, die in Schottland gern zum Frühstück gegessen wird) in vegan. Darauf ist er besonders stolz.
 Seine neue Lebensweise mache ihn einfach nur glücklich. Sogar seine Haut sei reiner geworden und sein Kopf klarer. „Es ist ein psychologisches Ding, du isst etwas Gesundes und schon fühlst du dich gut“, erklärt er."

* Bio Spahn FFM
    * Spiegel.de, 2016
        * "Ich mache jetzt Würstchen ohne Fleisch"
