Alkohol
=======

<!-- toc -->

- [IST-Situation](#ist-situation)
  * [Hintergründe](#hintergrunde)
  * [Bereits in geringen Mengen schädlich](#bereits-in-geringen-mengen-schadlich)
- [Alkoholwerbung wirkt](#alkoholwerbung-wirkt)
  * [ZEIT-Artikel 2009](#zeit-artikel-2009)
  * [Fragen](#fragen)
- [Jugendliche](#jugendliche)
  * [Verfügbarkeit](#verfugbarkeit)
  * [Aufklärung](#aufklarung)
  * [Empfehlungen: Beschränkung des Verkaufs und höhere Preise](#empfehlungen-beschrankung-des-verkaufs-und-hohere-preise)
- [Prävention](#pravention)
  * [Offiziell](#offiziell)
  * [Empfohlene Maßnahmen: Einschränkung Verfügbarkeit, höherer Preis, Werbe- und Vermarktungsverbot](#empfohlene-massnahmen-einschrankung-verfugbarkeit-hoherer-preis-werbe--und-vermarktungsverbot)
  * [Sonstiges](#sonstiges)
- [Erhöhung des Demenz-Risikos](#erhohung-des-demenz-risikos)
  * [2018](#2018)
- [Verantwortung der Hersteller und Vermarkter](#verantwortung-der-hersteller-und-vermarkter)
- [Wege für Winzer](#wege-fur-winzer)

<!-- tocstop -->

IST-Situation
-------------
### Hintergründe
* http://www.alkoholnachrichten.de
* Oft werden von den Herstellern gezielt Jugendliche angesprochen, obwohl mittlerweile bekannt ist, dass junge Leute nicht zum Alkohol verführt werden sollten.
    * Idee: https://de.wikipedia.org/wiki/Liste_von_Negativpreisen
    * https://www.werberat.de/anleitung

### Bereits in geringen Mengen schädlich
* ["Gesundheitsrisiken schon bei geringen Mengen Alkohol"](https://www.wnoz.de/Gesundheitsrisiken-schon-bei-geringen-Mengen-Alkohol-493e4874-30d4-41ed-83d1-d7c957566821-ds), 2018
    * "Ein Gläschen Wein am Abend schadet nicht, nehmen viele Menschen an. In einer großen Auswertung kommen Forscher zu anderen Ergebnissen."
    * "Zu diesem Schluss gelangen die Autoren einer Studie über den weltweiten Konsum alkoholischer Getränke und den Zusammenhang mit 23 Krankheiten."
    * "Max Griswold von der University of Washington in Seattle"
    * "694 Studien über Alkoholkonsum und 592 Studien über Gesundheitsrisiken durch den Genuss von Alkohol ausgewertet"
    * "Daten umfassen die Bevölkerung von 15 bis 95 Jahren in 195 Ländern"
    * "Die konkreten Todesursachen unterscheiden sich je nach Alter: In der Altersgruppe 15 bis 49 Jahre sind Tuberkulose, Verkehrsunfälle und Selbstverletzung am häufigsten. In der Gruppe ab 50 Jahren hat Krebs den höchsten Anteil an den alkoholbedingten Todesursachen."
    * "Der Konsum von Alkohol ist in Deutschland sehr verbreitet: 94,3 Prozent der Männer und 90 Prozent der Frauen trinken wenigstens gelegentlich Alkohol."
    * "Weil in diversen Studien positive Effekte von mäßigem Alkoholkonsum auf Diabetes oder die Erkrankung der Herzkranzgefäße festgestellt wurden, rechneten die Forscher dies gegen die negativen Folgen."
        * "Insbesondere der starke Zusammenhang zwischen Alkoholkonsum und dem Risiko von Krebs, Verletzungen und Infektionskrankheiten gleicht die schützenden Wirkungen für Erkrankungen der Herzkranzgefäße bei Frauen in unserer Studie aus"
    * "Weltweit müssen wir die Alkoholkontrollpolitik und Gesundheitsprogramme überdenken und Empfehlungen für den Verzicht auf Alkohol in Erwägung ziehen"
    * Lösungen:
        * "Die Erhöhung der Besteuerung schafft Einkommen für bedrängte Gesundheitsministerien, und wenn Kindern weniger dem Alkoholmarketing ausgesetzt sind, hat das keine Nachteile." --> **höhere Steuern und Marketing-Verbot**

Alkoholwerbung wirkt
--------------------
### ZEIT-Artikel 2009
ZEIT-Artikel aus dem Jahr 2009: http://www.zeit.de/online/2009/20/alkohol-werbewirkung

Überschrift: "Alkoholwerbung wirkt" / Untertitel: "Eine Studie zeigt, dass Kinder und Jugendliche,
die häufig Alkoholwerbung sehen, mehr trinken."

Zitat auf Seite 2: "Freilich wäre es zu weit gegriffen, Werbung für Alkohol als größten oder gar
alleinigen Faktor für die Verbreitung von Alkohol unter Jugendlichen verantwortlich machen zu
wollen. Sie leistet jedoch sicher ihren eigenen Beitrag zur individuellen Alkoholsozialisation."

### Fragen
* Sollte Alkoholwerbung nicht eingeschränkt werden? Analog zur Tabakwerbung.

Jugendliche
-----------
### Verfügbarkeit
* http://www.maz-online.de/Brandenburg/Brandenburgs-Kids-kommen-leicht-an-Alkohol, 2015
    * "Das Fazit der Forscher: Kommen Jugendliche allzu leicht an Alkohol, liegt das Risiko für exzessives Komatrinken um 26 Prozent höher als in der Vergleichsgruppe, die nur schwer an Alkohol kommt."
    * "Die Verfügbarkeit spielt eine große Rolle für ein späteres Suchtverhalten"
        * "Das ist beim Glücksspiel genauso. Dort, wo sich viele Gelegenheiten bieten, nimmt die Zahl der Menschen mit problematischem Spielverhalten zu."
    * "Nur 11 Prozent erklärten, sie kauften Bier oder Wein im Supermarkt."

### Aufklärung
* http://www.maz-online.de/Brandenburg/Brandenburgs-Kids-kommen-leicht-an-Alkohol, 2015
    * "Tabuisieren dürfe man Alkohol zu Hause aber nicht. „In begrenztem Maße kann es sinnvoll sein, wenn Eltern ihre Kinder an Alkohol heranführen.“"
    * "Wenn die Eltern bewusst und vernünftig mit dem Thema Alkohol umgehen, übernehmen die Kinder viel davon"

### Empfehlungen: Beschränkung des Verkaufs und höhere Preise
* http://www.maz-online.de/Brandenburg/Brandenburgs-Kids-kommen-leicht-an-Alkohol, 2015
    * "Suchtexperten fordern eine stärkere Beschränkung des Verkaufs und höhere Preise."

Prävention
----------
### Offiziell
* https://www.kenn-dein-limit.info/alkohol-ohne-risiko.html
    * "Anders als früher angenommen, ist die Gehirnentwicklung nicht in der Kindheit abgeschlossen, sondern erst mit etwa 25 Jahren. In dieser Zeit kann Alkohol schon in kleinen Mengen erheblichen Schaden anrichten."
* https://www.kenn-dein-limit.info/warum-trinken-wir-alkohol.html
    * "Wenn allerdings Gruppendruck oder einfach nur die Verfügbarkeit von Alkohol dazu führen, dass man trinkt, kann es gefährlich werden"

### Empfohlene Maßnahmen: Einschränkung Verfügbarkeit, höherer Preis, Werbe- und Vermarktungsverbot
* ["Alkohol: Regelmäßiges Trinken erhöht Demenz-Risiko enorm"](https://derstandard.at/2000074661422/Alkohol-Regelmaessiges-Trinken-erhoeht-Demenz-Risiko-enorm), 2018
    * "Es bedarf einer Vielzahl von Maßnahmen, etwa die Einschränkung der Verfügbarkeit, die Erhöhung der Steuern auf Alkohol und das Verbot der Werbung und Vermarktung von Alkohol sowie die Früherkennung und Behandlung von Alkoholproblemen"

### Sonstiges
* https://www.ift-nord.de/praevention

Erhöhung des Demenz-Risikos
---------------------------
### 2018
* ["Alkohol: Regelmäßiges Trinken erhöht Demenz-Risiko enorm"](https://derstandard.at/2000074661422/Alkohol-Regelmaessiges-Trinken-erhoeht-Demenz-Risiko-enorm), 2018
    * "Studienautor Michael Schwarzinger vom Translational Health Economics Network"
    * selbst bei moderaten Mengen, siehe Forschung von M. Schwarzinger
        * empfohlene gesundheitspolitische Maßnahmen:
            * Werbeverbote
            * höhere Steuern auf Alkohol
            * geringere Verfügbarkeit

Verantwortung der Hersteller und Vermarkter
-------------------------------------------
* ist vorhanden
* Frage: wie wird diese konkret wahrgenommen?

Wege für Winzer
---------------
Ein Vorgehen nach wissenschaftlichen Empfehlungen wird den Absatz von Alkoholprodukten inklusive Wein senken. Welche Wege können gefördert werden, um die Situation für Winzer abzufedern?

* Bio-Traubensaft
    * todo: aktueller Absatz im Vergleich zu Wein?
* todo
    * Vergleich mit der Situation von Kohleabbau-Arbeitern? Oder Arbeitsplätzen in anderen schädlichen Industrie-Zweigen?
