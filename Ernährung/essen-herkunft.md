Essen: Herkunft
===============

<!-- toc -->

- [Aus China](#aus-china)
  * [Manadrinen, Orangen, Tomaten, Pilze](#manadrinen-orangen-tomaten-pilze)
- [Tomaten](#tomaten)
  * [2019](#2019)
  * [Tomatenpüree](#tomatenpuree)
  * [Tomaten aus China](#tomaten-aus-china)
- [todo](#todo)
  * [Sesam](#sesam)
  * [Bohnen](#bohnen)
- [Unsorted](#unsorted)
  * [Erdbeeren](#erdbeeren)

<!-- tocstop -->

* siehe auch
    * tierprodukte-herkunft.md (TODO). Jeweils fragen: "Was ist davon zu halten?"
    * Weltacker (unter bauen.md)

Aus China
---------
### Manadrinen, Orangen, Tomaten, Pilze
* Doku: "Billige Lebensmittel aus Fernost - China in Dosen | SWR Doku" - https://www.youtube.com/watch?v=nL_78p6kljg, 2019, 45 min, China-Doku, Dosen-Doku
    * Einstieg: Familie, die auf günstig setzt, kocht viel mit Dosenessen
    * Mandarin-Orangen
        * Fabrik
        * Schälen per Hand
        * Segmente zerlegen per Hand
        * 10h-Schichten
        * bis zu 390 EUR / Monat => Billiglohnland (vergleiche "Billiglohnland DDR")
        * Arbeiterwohnungen, Testfeld: rings rum Industrie; Luft sei beißend vor Smog
        * Manadrinen-Hautreste durch Lauge wegätzen
        * Dosen für Deutschland befüllen plus Zuckerwasser, plus Etiketten
    * Importeur: "inzwischen zu 80 - 90 % Eigenmarkengeschäft" der Einzelhändler => Herkunft steht nicht auf dem Etikett
    * Foodwatch forder verbindliche Herkunftskennzeichnung
        * Industrie wehrt sich (daher auch die Politik)
    * Station: Champignon-Fabrik
        * Akkord-Arbeit für deutsche Discounter und Supermärkte
        * Importeur: "Europa kann den Bedarf nicht decken", Faktor Arbeit und "auch die Geschwindigkeit, in der das vonstatten geht"
        * 220 EUR Mindestlohn (selbst für chin. Verhältnisse wenig)
    * "Immer mehr Tomatenprodukte kommen aus Fernost"
        * Importeur: größter Importeur von Tom.mark ist Italien, die das dann dort nochmal verpacken.
        * Reise nach Süd-China zu Tomatenbauern
            * nebenan wieder Industrie
            * Kleinbauer mit Spritzkanister für die Pestizide
            * Wasser vom Fluss
            * Tomatenernte wird jährlich schlechter, vermutlich weil Boden schlechter
            * Wissenschaft: Industrie wurde zur Entlastung der Ballungszentren aufs Land verlagert. Dort schaden sie der Landwirtschaft.
        * Industrieabwässer in Flüssen
            * "China's looming water crisis": https://www.chinadialogue.net/article/show/single/en/10583-China-is-heading-towards-a-water-crisis-will-government-changes-help-, 2018
                * "Acknowledgement of the problem should be at the top of the political agenda."
                    * "back in 2005 the then Minister of Water Resources said that China must “fight for every drop of water or die”. Former Premier Wen Jiabao said that water shortages threatened “the very survival of the Chinese nation”"
        * beim Bauern
            * mindestens zwei verbotene Pestizide (z. B. Fungizid) werden im Eimer angerührt
            * Laut Experten: bis zu 70% der Anbauflächen in China sind verseucht (=> min. 30% sind ok)
            * und ein braun-schwarzer Gift-Cocktail wird angerührt
            * und auch noch jede Menge Dünger, weil die Pflanzen von selber kaum noch wachsen
            * nach 15 Tagen nochmal Dünger plus Pestizide
            * plus Rattengift gegen Ratten
            * und noch ein Zeug gegen Bodenschädlinge
    * Lebensmittelsicherheit in China?
        * laut einem Blogger ist das mit einem neuen Gesetz nun verboten, in Medien darüber zu berichten
        * Verschmutzung in China durch Industrie: u.a. Schwermetalle
        * Umweltgesetze kommen schleppend voran, wegen enormem Wachstumsdruck
    * Pilz-Fabrik
        * z. B. "Pilz-Topf"-Glas für deutschen Discounter mit schönem Bild eines Ackers auf dem Etikett, wo man nicht an China oder Fabrik-Hallen denkt
        * Champignon-Pilze werden auf Schweine- und Hühnermist ("aufwändig sterilisiert") in Regalen gezüchtet; alles sehr sauber
        * im Pilztopf sind aber noch andere Pilzsorten, die von Zulieferen kommen
            * traditionelles Pilzhaus
            * auch mal Pestizide
            * Lebensmittelkontrollen schwierig, weil sehr kleinteilige Lieferkette
        * Importeur: nimmt im Endeffekt das, was der Markt gerade hergibt
    * 30:50: Europäpisches Schnellwarnsystem, Liste mit Beanstandungen, oft China
        * https://www.bvl.bund.de/DE/Arbeitsbereiche/01_Lebensmittel/lebensmittel_node.html
        * wie weiter?
    * Hamburg, Kontrollstation Außengrenze
        * Ladung mit Schweinedärmen aus China
            * "daraus werden später Häute, sogenannte Saitlinge, für deutsche Bock- oder Bratwürste"
            * von deutschen Schweinen
            * nur zum Reinigen und Zuschneiden nach China, weil halt dort billiger
        * aber es gibt auch Saitlinge von chinesischen Tieren, z. B. Schafen (wo z. B. auch mal verbotene Antibiotika festgestellt werden)
        * nur tierische Lebensmittel werden so kontrolliert; vor allem wegen Tierseuchenabwehr
            * verarbeitete pflanzliche Produkte: da überwacht der Markt sich selber
                * Chefin: so langsam denkt man drüber nach auch bei nicht-tierischen Lebensmitteln oder bei Spielzeug ("die ja in Massen reinkommen") die Kontrollen an die Außengrenze zu verlegen
    * 25 % (?) der Fischprodukte im Tiefkühlregal kommt aus China
        * bei Fisch gibt es laut EU öfter mal Probleme (Kühlkette, verbotene Stoffe)
    * Wurst-Saitling-Fabrik aus chinesischen Schafsdärmen in einem Zentrum für Tierdarmproduktion
        * "Als Saitling wird der **Dünndarm vom Schaf** bezeichnet. Der Naturdarm gilt als besonders zart und eignet sich vor allem für feine Brühwürste, aber auch für Räucherwürste. So werden beispielsweise Frankfurter und Wiener Würstchen sowie Nürnberger Rostbratwürste in der Regel mit Saitling hergestellt. Der feine, dünne Naturdarm kann bedenkenlos mitverzehrt werden."
            * aus: https://www.edeka.de/ernaehrung/expertenwissen/1000-fragen-1000-antworten/was-ist-ein-saitling.jsp
        * oft bei chin. Schafen: verbotene Antibiotika
        * Handarbeit, Akkord, mit Pinzette Haare und sonstige Fremdstoffe entfernen; Tag ein Tag aus; im Winter kalt; 12h/Tag
            * kommt heim, fühlt sich leer und erschöpft; kann nichts unternehmen oder Hausarbeit oder für ihre Tochter kochen
    * Positives Vorbild: Frosta: gibt alle Herkunftsländer auf dem Etikett an
        * ansonsten schweigen die Hersteller und Discounter lieber
    * Importeur sieht großes Potential im Export von sicheren europäischen Lebensmitteln, die sich die wachsende wohlhabende chinesische Schicht leisten möchte
        * die armen Bauern haben davon nichts

Tomaten
-------
### 2019
* TODO: "Das Geschäft mit Tomaten", 2019, 44 min, SWR Marktcheck
    * https://www.youtube.com/watch?v=ChkpkXAlIyw
        * "Auf Etiketten von Tomatenprodukten wird oft mit Tomaten aus Italien geworben. Doch viele verarbeitete Tomaten kommen tatsächlich aus China. Daran erkennen Sie echte, italienische Tomaten."
        * ...
        * ganze Pflanzen rausreißen; harte Arbeit
        * ...
        * ...

### Tomatenpüree
* TODO: "Rotes Gold – Die Geheimnisse der Tomatenindustrie", 2018, 44 min
    * https://www.zdf.de/dokumentation/zdfinfo-doku/rotes-gold--die-geheimnisse-der-tomatenindustrie-100.html
        * "Tomaten, das rote Gold - Tomatenpüree, eine weltweite Handelsware. Der Autor geht auf Spurensuche in China, Afrika und Europa und zeigt die Herstellung und die weite Reise des Tomatenpürees. "

### Tomaten aus China
* TODO: "Tomaten aus China ORF konkret", 2018
    * https://www.youtube.com/watch?v=Og996ia_m3U, 5 min
        * "Chinas Wirtschaft erobert immer mehr europäische Märkte: das Reich der Mitte ist etwa der größte Lieferant von Agrar-Produkten für Italien. Italienische Pomodori stammen oftmals nicht mehr aus Italien, sondern aus China, ohne dass der Kunde davon weiß."
        * ...
        * singender Erntehelfer in China
        * ...
        * ...

todo
----
### Sesam
...

### Bohnen
...

Unsorted
--------
### Erdbeeren
* Misereor Frings 2017 - viel Kiloware wie Erdbeeren kommt heute alles aus China -- TODO
