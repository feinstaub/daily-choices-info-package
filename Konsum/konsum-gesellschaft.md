Hintergrund: Konsumgesellschaft
===============================

<!-- toc -->

- [Einführung](#einfuhrung)
  * [Die 10 größten Umweltprobleme der Welt](#die-10-grossten-umweltprobleme-der-welt)
  * [Grüner Konsum / Nachhaltiger Konsum, (Wie) geht das?](#gruner-konsum--nachhaltiger-konsum-wie-geht-das)
- [Maß halten - Suffizienz](#mass-halten---suffizienz)
- [Langeweile](#langeweile)
- [Beispiele](#beispiele)
  * [Ernährung](#ernahrung)
  * [Die Geschichte des Plastiklöffels](#die-geschichte-des-plastikloffels)
  * [Plastik](#plastik)
  * [Reparieren](#reparieren)
  * [Erziehung, Gesundheit](#erziehung-gesundheit)
- [Arbeitsplätze um jeden Preis?](#arbeitsplatze-um-jeden-preis)

<!-- tocstop -->

Einführung
----------
* Doku: ["Die gelenkte Konsumgesellschaft"](https://www.youtube.com/watch?v=QztcpDcjHPk), zdf_neo, 1h 50min, 2016
    * u. a. wie es ab ca. den 70er-Jahren (auch dank CAD) dazu kam, dass es immer leichter wurde, neu aussehende Produkte zu produzieren. Und beim Menschen das Bedürfnis beweckt wurde, immer das neuste haben zu wollen.
* Doku: ["Die 20 größten Konsumsünden (3sat)"](https://www.youtube.com/watch?v=rB8k6Xs0Wbs), 1 h, 2013
    * Kreuzfahrtschiffe -> siehe Urlaub
    * Diamanten
    * ...
* siehe auch wachstum.md

### Die 10 größten Umweltprobleme der Welt
* http://www.tenoftheday.de/die-10-groessten-umweltprobleme-der-welt/, Jahr?
    * Klimawandel
    * Artensterben
    * Ozonloch
    * Überfischung der Meere
    * Luftverschmutzung
    * Wasserverschwendung (z. B. für einige Golfplätze)
    * Vermüllung
    * Umweltkatastrophen (z. B. Öltanker, Atomkraft etc.)
    * Bergbau (z. B. Kohle)
    * Welthunger

### Grüner Konsum / Nachhaltiger Konsum, (Wie) geht das?
* ["Grüne Produkte - Warum nachhaltiger Konsum nicht funktioniert: 14 Thesen"](https://www.geo.de/natur/nachhaltigkeit/19468-rtkl-gruene-produkte-warum-nachhaltiger-konsum-nicht-funktioniert-14), 2018
    * "Die Idee, mit ressourcen- und energiesparenden Produkten der Umwelt zu helfen, ist gut 25 Jahre alt. Funktioniert hat sie noch nie, meint GEO.de-Redakteur Peter Carstens"
* Die Thesen (inkl. Ernährung):
    * "1. Das Potenzial nachhaltigen Konsums wird überschätzt."
        * "[...] Der größere Teil des enormen Potenzials wird schlicht und einfach nicht zu heben sein. In den allermeisten Fällen wird es bei Symbolhandlungen bleiben. Der Klassiker: Einkauf im Bio-Hofladen mit dem SUV."
    * "2. Wer energiesparende Geräte kauft, verbraucht mehr Strom."
        * Rebound-Effekt
        * "[...] das Licht länger brennen zu lassen. Verbraucht ja nichts. Selbst wer disziplinierter ist, kann das bei der Stromrechnung eingesparte Geld in ein neues Auto, einen Urlaubsflug oder fossile Aktien investieren. Und so den Umweltvorteil durch die sparsamere Beleuchtung zunichtemachen."
    * "3. Steigende Ansprüche machen Effizienzgewinne zunichte."
        * "Grüne Produkte könnten helfen [...] Tun sie aber nicht"
        * "Die Ansprüche der Konsumentinnen und Konsumenten an Wohnraum, Ausstattung, individuelle Mobilität und Ernährung steigen seit Jahren an. Pro Kopf steigt die Wohnfläche kontinuierlich an, elektronische Geräte werden vermehrt angeschafft und häufiger genutzt, es werden längere Wege zurückgelegt, und der Fleischkonsum ist anhaltend hoch"
    * "4. Umweltbewusstsein hin oder her: Wer viel verdient, schädigt die Umwelt mehr."
        * "Eine Auswertung des Umweltbundesamts zeigt: Wer viel verdient, lebt umso mehr auf Kosten der Umwelt. Denn egal, was wir mit unserer Gehaltserhöhung tun, ob Haus- oder Autokauf, ob Flugreise oder Elektronik: Jede Umwandlung von Geld in Dinge oder Dienstleistungen wird sich klima- und umweltschädlich auswirken."
        * "Einen ernüchternden Beleg für den Zusammenhang von Einkommen und umweltschädlichem Verhalten erbrachte eine Umfrage unter Grünen-Wählern: Von allen Parteianhängern fliegen sie am meisten. Weil sie überdurchschnittlich viel Geld verdienen."
    * "5. Kompensationssysteme machen umweltschädliches Verhalten moralisch erschwinglich."
        * ...
    * "6. Echte grüne Produkte sind immer schwerer zu erkennen."
        * "Während Bio-Siegel und andere Label zumindest handfeste Mindeststandards garantieren"
        * "Ein Beispiel: Wer "Ökostrom" von einem der großen Versorger bezieht, tut nicht unbedingt etwas für die Energiewende [...]"
    * "7. Umweltbewusste Konsumenten werden mehr. Die anderen auch."
        * "Es gibt immer mehr aufgeklärte Konsumenten, die wirklich was für die Umwelt tun wollen. Gleichzeitig gibt es immer mehr Egal- und Hauptsache-billig-Konsumenten. Unter dem Strich verliert die Umwelt."
    * "8. Die nachhaltige Produktwelt wird immer supermarktiger."
        * "Bioläden sehen heute aus wie ganz normale Supermärkte: Regale voll mit überflüssigen Putz- und Waschmitteln, natürlich alle super biologisch abbaubar, Kartoffeln aus Ägypten, Avocado aus Peru, regalweise Kaffee und Schokolade aus Südamerika oder Afrika. So macht bio Spaß, so lässt es sich herrlich unbeschwert shoppen. Wie früher eben."
    * "9. Freundliche Einladungen zum Ausprobieren wirken genauso wenig wie Moralpredigten."
        * "Früher nervte der erhobene Zeigefinger. Heute sagt man: "Fang einfach an und genieße und rette die Welt". Klingt gechillt. Aber ein echter Anreiz, irgendetwas zu ändern, ist dieser Slogan auch nicht. Er nervt nur nicht mehr. So haben alle ihre Ruhe: die Nachhaltigkeits-Prediger, die jetzt nicht mehr als Moralapostel beschimpft werden können, und ihre Schäfchen, die nur noch niedrigschwellige Angebote erhalten. Die sie getrost ignorieren dürfen. Was sie dann auch tun."
    * "10. Nachhaltigkeit ja - aber bitte nur, wenn sie nicht wehtut."
        * "Im Schnitt ist jede/r Deutsche für jährlich zwölf Tonnen Klimagas-Emissionen verantwortlich. Global verantwortbar wäre: höchstens eine. Da kommen wir mit einem neuen A+++-Kühlschrank, einem Elektroauto, einer hippen Solar-Taschenlampe und einem bio-fairen Kaffee im Mehrwegbecher nicht ganz hin.  Wer es mit der Nachhaltigkeit ernst meint, muss bereit sein, sein Leben zu ändern. Statt niedrigschwellige "Einfach-Anfangen!"-Angebote an notorische Vielkonsumierer zu machen, sollten Politiker, Umweltbewegte und Medienmenschen Tacheles reden: Eine Tonne, das geht. Aber es wird richtig wehtun!"
    * "11. Falsche Vergleiche sollen den "nachhaltigen" Konsum ankurbeln."
        * "Um Dinge attraktiv erscheinen zu lassen, braucht man nur einen geschickten Vergleich zu wählen. Zum Beispiel alte und neue Autos: Neuerdings soll es sogar nachhaltig sein, sich ein neues, spritsparendes Auto zu kaufen. Weil das schon nach ein paar Jahren in der Energiebilanz besser dasteht als das alte Auto. Kann sein. Eine Faustregel dafür gibt es nicht. Garantiert vorteilhaft sind andere Alternativen: das alte Auto einfach seltener benutzen. Oder es ganz abschaffen. Alt oder neu, fossil oder elektrisch: Es gibt kein umweltfreundliches Autofahren."
    * "12. Solange die Preise nicht die Wahrheit sagen, wird die Produktion umweltschädlich bleiben."
        * "Am Ende entscheidet immer der Preis. Auch wenn viele Konsumenten sich gegen die Verlockung stemmen, zum billigeren und umweltschädlichen Produkt zu greifen: Hier muss die Politik eingreifen. Ihre Aufgabe ist es, im Sinne von Nachhaltigkeit und Tierschutz dafür zu sorgen, dass die wahren (Umwelt-)Kosten der Billigproduktion in den Verkaufspreis einfließen."
        * "Wenn im Preis von Fleisch alle Klima- und Umweltschäden enthalten wären (vom Tierleid ganz zu schweigen), wäre mit einem Schlag ein riesiger Posten unserer ernährungsbedingten Emissionen erledigt."
    * "13. Konsumenten konsumieren. Politik machen müssen Politiker."
        * "Die Debatte, ob und inwiefern Konsum auch politisch ist, ist so alt wie die Idee des nachhaltigen Konsums. Aber es hilft nichts: Wir werden mit dem Kassenzettel keine drastischen Geschwindigkeitsbegrenzungen, kein Straßenbau-Moratorium erwirken, keine CO2-Steuer einführen oder gar Emissions-Budgets für jeden. Das Erneuerbare-Energien-Gesetz, das Wind- und Sonnenstrom zum Durchbruch verholfen hat, ist nicht das Ergebnis einer Konsumenten-Petition mit dem Rechnungsbeleg. Sondern politische Rahmensetzung."
    * "14. Die Idee des nachhaltigen Konsums verkennt das Wesen der Konsumgesellschaft."
        * Deren Antrieb ist: Immer mehr in immer kürzerer Zeit. Um diesen Anspruch zu realisieren, müssen permanent neue Bedürfnisse geweckt werden - um die Nachfrage nach immer neuen Produkten aufrecht zu erhalten. Nachhaltig wäre genau das Gegenteil: nicht konsumieren. Sondern Dinge pflegen, reparieren, tauschen, lange nutzen. Wer Nachhaltigkeit will, sollte auf Wirtschaftswachstum verzichten können. Wie wir auch ohne Wachstum gut leben können, das erzählen uns Postwachstumsökonomen seit Jahren."

* https://www.bmu.de/themen/wirtschaft-produkte-ressourcen-tourismus/produkte-und-konsum/nachhaltiger-konsum/
    * "Mit Blick auf eine stetig wachsende Weltbevölkerung und begrenzte Ressourcen auf unserer Erde stellt sich die Frage, wie zukünftig der Lebensbedarf von neun Milliarden Menschen gedeckt und Partizipation sichergestellt werden kann. Allein der Konsum der privaten Haushalte ist für mehr als ein Viertel aller Treibhausgasemissionen in Deutschland verantwortlich. Die Produktion der Konsumgüter ist dabei noch nicht einmal einbezogen. Das bedeutet: Der Konsum von Produkten beeinflusst immer stärker nicht nur die wirtschaftliche und soziale Situation der Menschen, sondern auch den Zustand der Umwelt. Im Gebrauch und der Herstellung von Produkten liegt folglich ein großes Potenzial zur Verringerung der Umweltbelastung. Es geht darum, dieses Potenzial zu erkennen und zu nutzen. Eine Diskussion um unsere Lebensstile und um unsere Verantwortung auch beim Konsum ist unerlässlich!"

* https://www.welt.de/wirtschaft/bilanz/article192004831/Nachhaltiger-Konsum-Marken-retten-nicht-die-Welt-aber-das-Gewissen.html, 2019
    * ...
    * Nutella
    * ...

* https://www.umweltdialog.de/de/politik/UN-Entwicklungsziele/2018/Nachhaltiger-Konsum-2030.php, 2018
    * "Wie lässt sich das Ziel einer nachhaltigen Lebensweise in Deutschland verwirklichen – individuell und als Gesellschaft? Wie kann eine an nachhaltiger Entwicklung ausgerichtete Verbraucherpolitik den Wandel sinnvoll unterstützen? Und was sind mögliche Rahmenerzählungen dieser tiefgreifenden Transformation, die dem Handeln im Alltag einen größeren Zusammenhang und eine Richtung geben?"
    * ...
    * "Szenario 3: „Warum haben technologische Innovationen bislang kaum zu mehr Nachhaltigkeit geführt?“"
    * ...

* https://www.nachhaltigkeitsrat.de/nachhaltige-entwicklung/nachhaltiger-konsum/
    * ...

* https://www.nachhaltiger-warenkorb.de/

* https://www.bdkj-freiburg.de/html/definition.html
    * Was bedeutet "Nachhaltiger Konsum"?
    * "Konsumbereiche, in denen Nachhaltigkeit realisiert werden kann": ...

Maß halten - Suffizienz
-----------------------
* [Suffizienz (Ökologie)](https://de.wikipedia.org/wiki/Suffizienz_(%C3%96kologie))
    * "Der Begriff Suffizienz (von lat. sufficere, dt. ausreichen) steht in der Ökologie für das Bemühen um einen möglichst geringen Rohstoff- und Energieverbrauch."
    * "In der praktischen Nachhaltigkeits­diskussion wird Suffizienz komplementär (ergänzend) zu Ökoeffizienz und Konsistenz gesehen. Er wird im Sinne der Frage nach dem rechten Maß sowohl in Bezug auf Selbstbegrenzung, Konsumverzicht oder sogar Askese, aber auch Entschleunigung und dem Abwerfen von Ballast gebraucht."
    * "In jedem Fall geht es um Verhaltensänderungen (insbesondere) als Mittel des Umweltschutzes – im Gegensatz zu technischen Umweltschutzstrategien wie einer gesteigerten Energie- und Ressourceneffizienz oder dem vermehrten Einsatz regenerativer Ressourcen (Konsistenz)."
* BUND-Jugend: https://www.bundjugend.de/was-ist-suffizienz/
* ["Gutes Leben für alle! Eine Einführung in die Suffizienz"](https://www.bund.net/bund-tipps/detail-tipps/tip/gutes-leben-fuer-alle-eine-einfuehrung-in-die-suffizienz/)
    * [PDF](https://www.bund-bawue.de/fileadmin/bawue/Dokumente/Themen/Nachhaltigkeit/Suffizienz_Gutes_Leben_fuer_Alle_web.pdf), 2017
    * "In manchen Gesprächssituationen sind Sie mit skeptischen Einwänden, Zweifeln oder Ablehnung konfrontiert. In diesem Teil
finden Sie Anregungen, wie Sie mit häufigen Gegenargumenten und kritischen Gesprächspartnerinnen klug umgehen und zu
Ihrer eigenen Kernbotschaft zurückkehren können."
        * "„Wollt ihr etwa zurück in die steinzeit? wir brauchen technische innovationen, nicht Verzichtspredigten!“"
            * "Suffizienz ist eine ergänzung"
        * "„Wenn ich persönlich suffizient lebe, bringt das in der Konsumgesellschaft doch sowieso nichts!“"
            * "„...Und deshalb mache ich so weiter wie bisher“. Dieses Argument kann Ausrede für die eigene Bequemlichkeit sein, Zweifel an der Wirksamkeit ausdrücken oder Zeichen von Resignation sein. Da lohnt es zu diskutieren"
            * "Dass individuelle Maßnahmen tatsächlich nur begrenzte Wirkung haben und deshalb Suffizienzpolitik nötig ist, damit ein anderer Lebensstil leichter wird."
        * "„Wo bleibt denn da der Spaß und Genuss? Ich will mir ja auch mal was gönnen!"
            * "Genuss ist oft eine Frage der Haltung, Konsum eine Frage des rechten Maßes"
        * "„Suffizienz ist unbequem und braucht viel mehr Zeit!"
            * ja, aber es gibt positive Nebeneffekte
        * "„Suffizienz schränkt unsere Freiheit ein, in einer liberalen Gesellschaft können wir nicht ständig neue Verbote schaffen!""
            * "**Gegenfrage: Wessen/welche Freiheiten werden durch den Status quo eingeschränkt?** **Wessen Interessen werden wie gewichtet?** Suffizienz schafft einen Rahmen für verschiedene Lebensentwürfe. Dazu zählt auch: „Niemand soll immer mehr haben wollen müssen“ (Uta von Winterfeld). Deshalb ermöglicht Suffizienzpolitik mehr Selbstbestimmung, zum Beispiel die Freiheit, kein eigenes Auto zu haben und in Teilzeit zu arbeiten, um mehr zeitliche Spielräume zu haben."
        * **"Meine Freiheit wird eingeschränkt, wenn fliegen teurer wird!"**
            * "Es gibt kein Recht auf billige Flüge zu Lasten anderer Menschen und der Umwelt. Fliegen schränkt die Freiheit der Menschen ein, die jetzt schon oder in Zukunft vom  Klimawandel betroffen sind. Wir werden uns darauf einstellen müssen, dass die Preise sich verändern, wenn darin die Umweltschäden enthalten sind. Und Klamotten werden  auch teurer, wenn die Arbeitsplätze der an der Produktion Beteiligten sicher werden und sie faire Löhne bekommen."
        * "'Weniger ist mehr‘ muss man sich erstmal leisten können. Das ist doch nur ein neues Statussymbol für Privilegierte!“"
            * "müssen Menschen mit geringem Einkommen mitgedacht werden und geeignete Ausgleiche eingeführt werden."
        * "Es ist total zynisch, Suffizienz zu fordern, wenn im globalen Süden Menschen an extremer Armut leiden!"
            * "...soll Suffizienz eine Strategie für die „entwickelten“ Länder des globalen Nordens sein"
            * "...muss der Norden seinen Lebensstil ändern, um die Entwicklungschancen des globalen Südens nicht einzuschränken."
        * "„Wer bitte kann sich anmaßen, über das rechte Maß für alle zu bestimmen?“"
            * "Wir müssen demokratisch und transparent über den Zugang und die Verteilung bei begrenztem Geld, Fläche, Ressourcen diskutieren und entscheiden."
        * Wirtschaft
            * ...
        * "Wenn es für Suffizienz so viele gute Argumente gibt, dann wird sie sich doch von allein durchsetzen – warum braucht es da die Politik?"
            * ...
        * "Suffizienz ist nicht politikfähig, solche Themen kommen einfach nicht an!"
            * "Es gibt bereits politische instrumente, die Suffizienz fördern – wovor also Angst haben? Zum Beispiel der Landeszuschuss zum Jobticket in Baden-Württemberg und  autofreie Innenstädte und Wohngebiete."
    * Mehr unter http://www.postwachstum.de/suffizienzpolitik
        * [Blog Postwachstum](http://www.postwachstum.de)
* Der Weltacker, siehe bauen.md

Langeweile
----------
* siehe psychologie.md

Beispiele
---------
### Ernährung
* Video: [Einfaches Info-Video zur Effektivität von bewusst (und richtig) ausgewählter Ernährung](https://www.youtube.com/watch?v=jQ2UbTUr1uA), 7 min, Greenpeace 2017
    * ab der Mitte ca.: Infos über Kleiderkonsum
    * ...

### Die Geschichte des Plastiklöffels
* Video: [Die unglaubliche Geschichte eines Löffels](https://www.youtube.com/watch?v=ebuRHcv7TtI), Greenpeace 2015, 2 min, englisch mit deutschen Untertiteln

### Plastik
* Doku: ["Plastik: Der Fluch der Meere"](https://www.youtube.com/watch?v=8X9q8XkXupw), arte, 2012, 50 min
    * ...
    * todo
    * ...
* Video: ["Plastikverseuchter Kompost macht Äcker zu Müllhalden"](https://www.youtube.com/watch?v=Hr13sj_qoSo), ARD kontraste, 2016, 10 min
    * ...
    * wegen der Biotonne
    * ...

### Reparieren
* https://www.ardmediathek.de/ard/player/Y3JpZDovL3N3ci5kZS9hZXgvbzEwODc3NTU/, 2019
    * "Eine Innovation jagt die nächste. Immer schneller kommt die nächste Produktgeneration auf den Markt. Und wir Verbraucher spielen größtenteils mit. Wir kaufen und werfen weg - in immer kürzeren Abständen."
    * http://www.blitzblume-ingelheim.de/ - "Elektro-Haushaltsgeräte-Reparatur"
    * Kleidung, siehe Kleidung/README.md

### Erziehung, Gesundheit
* ["Smartphone-Nutzung - Kasse sieht negative Folgen für Kinder"](https://www.tagesschau.de/inland/smartphones-kinder-103.html), 2018
    * ""Bei den Heranwachsenden sind Krankheiten auf dem Vormarsch, die früher eher untypisch waren", teilte die Krankenkasse in Hannover mit. In den Jahren 2006 bis 2016 gab es demnach unter anderem einen enormen Anstieg von Sprach- und Sprechstörungen und von motorischen Entwicklungsstörungen."

Arbeitsplätze um jeden Preis?
-----------------------------
* siehe nachhaltigkeit.md: Louis C.K. "If God Came Back"
