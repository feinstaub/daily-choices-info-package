Das BigLEH-Script
=================

<!-- toc -->

  * [Ärmere Menschen](#armere-menschen)
  * ["Der andere ist auch nicht besser"](#der-andere-ist-auch-nicht-besser)
  * [Umwelt vs. Preisdruck](#umwelt-vs-preisdruck)
  * [Lebensmittel-Verschwendung vs. Handelsmacht](#lebensmittel-verschwendung-vs-handelsmacht)
- [Weiteres](#weiteres)
  * [Beispiele Druck auf Bauern](#beispiele-druck-auf-bauern)
  * [Lange Lieferketten](#lange-lieferketten)
  * [Unsortiert](#unsortiert)
- [Haltungen a la Freiheit für alle und insbesondere für mich](#haltungen-a-la-freiheit-fur-alle-und-insbesondere-fur-mich)

<!-- tocstop -->

BigLEH = Lebensmitteleinzelhandel-Oligopol, u. a. ::aldi, Lidl, REWE, Edeka, siehe oligopole.md

Sokrates-Methode

start: Ich kaufe bei BigLEH und das ist auch gut so.

Q1: Warum?

A1a: Weil BigLEH dafür sorgt, dass die Preise unten sind. Das ist ein Zeichen von Effizienz.

Q1a: Welche Größe wird bevorzugt optimiert? Eher Geld und Gewinn oder Umwelt und faire Bezahlung?

A2a: Jedes private Unternehmen arbeitet gewinnmaximierend. Also kaufe ich da, wo das Preis/Leistungsverhältnis am besten ist.

Q2a: Würdest du sagen, der **ethische Kompass** aller Unternehmer ist gleich?
     (Oder gibt es Grenzen, die einer überschreiten würde und ein anderer nicht?)
     Ist diese **gefühlte Alternativlosigkeit** vielleicht Ausdruck einer **Phantasielosigkeit**?

Q2: Legst du Wert auf auf eine intakte Umwelt?

Q3: Meinst du sagen, wenn man die Größe 'Geld' optimiert, dass dann die Umwelt ausreichend berücksichtigt wird.

A3a: Im Moment nicht, aber die Politik muss dafür sorgen, dass möglichst überall ein Preisschild dran ist.

Q3a: Würdest du sagen, die Politik ist da gerade auf einem guten Weg?

A3aa: Ja.
Q3aa: Also die Politik ist An was machst du das fest?

A3ab: Nein.
...

Eigenmarken:
- Preisdruck       Wird weitergegeben an Produzenten.
                Wo sparen die? Personal? Umwelt? an Nachhaltigkeit? (vergleiche Bahn-Börsengang)

An was denkst du, wenn du das Wort "Kaputtsparen" hörst?

An was würdest du sparen, wenn du unter Preisdruck gerätst (und nicht super schnell deinen Beruf wechseln kannst)?
    (in deinem Beruf / als Landwirt)


Eigenmarken:
    - Preisdruck auf Markenhersteller
    - Gibt es Unterschiede im sozialen Engagement?
    - Ist es gut, wenn der Kunde seine eigenen Vorstellungen beim Kauf berücksichtigen kann?
        (was er als ethisch gut arbeitendens Unternehmen belohnen möchte, und welches nicht?)
        Hat er bei Eigenmarken die Möglichkeit dazu?
            Was ist mit Transparenz?
            Was ist uns **Freiheit der Wahl** wert?
            Ist es gut, wenn BigLEH diese Entscheidungen vorwegnimmt?

            A: ja, es gibt ja Siegel.

            Q1: Der überwiegende Teil ist konv. Landwirtschaft ohne Siegel, wo es keine Siegel und gesetzlich Minimalstandards gibt, die unzureichend sind?
                Ist ein kurzfristig orientierter Preisdruck vorteilhaft für nachhaltiges Wirtschaften?

            (Q2: Sollte alles mit Siegeln bestätigt werden?)

A: BigLEH treibt Big-Bio voran
Q: Kaufst du bei BigLEH nur Bio-Sachen? Was ist mit konv. Produkten?

Q: Sind niedrige Preise (induziert durch **Marktmacht und Preisdruck**) eher **kurzfristig oder langfristig** orientiert?
    Q: Bringt Marktmacht tatsächlich die gewünschte Effizienz?
        Was ist mit dem Bundes**kartell**amt?
        Was ist mit fairem Wettbewerb?

Q: Für Nachhaltigkeit... braucht es da eher kurzfristiges oder langfristiges Denken und Planen?

Q: Wie passt beides zusammen?
    --> sehr gut. Wie?
        --> Q2a: ethischer Kompass
    --> gar nicht. Und nun? Ist BigLEH weiterhin eine gute Wahl?

A: Tierprodukte kaufe ich nicht bei BigLEH.
Q: Warum?
Q: Gibt es eine Verantwortung des Einkäufers? Kann er sich hinter dem "Wunsch" der Kunden verstecken?
    Sollte er Transparenz bieten? (--> knallharter Wettbewerb...)

Q: Was hälst du von der Arbeit des Bundeskartellamtes? (notwendig für fairen Wettbewerb)
A: ...

Q: Was ist mit Wahlmöglichkeit/Wettbewerb. Ist das was Gutes?

### Ärmere Menschen

A: Arme Menschen können sich nichts anderes leisten.

Q: Warum sind dann auch so viele gut verdienende bei BigLEH?

Q: Billige Preise, automatische Kassen, Automatisierung... bleibt da noch Platz für regionale Jobs für Geringqualifizierte?
    (die Jobs werden bisher von allen getragen, so dass reichere Menschen die Jobs von ärmeren mitbezahlen)

Q: Geht billig nicht zuerst auf Arbeitsplatzabbau? Was haben Geringqualifizierte/Arme Leute davon?

Q: Ist es woanders wirklich unverhältnismäßig viel teurer?

### "Der andere ist auch nicht besser"
* siehe ethischer "Kompass" oben

### Umwelt vs. Preisdruck

Beispiel: Diquat für Kartoffeln: kurzfristig zuverlässiger und billiger, aber...

### Lebensmittel-Verschwendung vs. Handelsmacht

Beispiel: billig auf Masse produzieren
    1. => mehr wegwerfen, denn alles, was sich geldmäßig nicht lohnt, wird nicht gemacht
        (je billiger, desto weniger Spielraum für vernünftige Lösungen)
        **Q: Ist Optimierung auf Geld von außen gesehen derzeit in den meisten Fällen (insb. Umwelt und Langfristigkeit) vernünftig?**
        (LEH steht für diese Art der Optimierung im großen Stil
         inkl. Ausnutzung der Marktmacht. Kann daraus etwas Gutes entstehen?)
        ("Teuer ist nicht immer nachhaltig; aber was ist mit billig?")
Beispiel: Anforderungen an Kartoffeln
    1. => schon bei der Ernte Verschwendung
    2. => durch Verlust kleiner Läden auch keine Absatzmöglichkeit der "unschönen" Ware
Beispiel: Effizienzsteigerung durch Automatisierung und weniger Personal
    1. => weniger Flexibilität
       => lieber packungsweise wegwerfen als anderweitig verwerten, weil Sortierkosten zu hoch

Weiteres
--------
### Beispiele Druck auf Bauern
* 2020: Abends 40 Paletten Salat bestellen. Morgens 15 davon stornieren; nicht mehr verkaufbar.  / ::macht
    Wer meckert, wird ausgelistet. Landwirte in deutlich schlechterer Verhandlungsposition als Handel

### Lange Lieferketten
* LEH vs. Reformhaus
    * LEH-only vs. freie Einkaufswahl
* Initiative Lieferkettengesetz, siehe dort

### Unsortiert
* Nachhaltiges Denken kann man nicht verordnen.
    * Was nun?
* Geld als Messgröße für Erfolg
    * ... wie geht das ohne passenden Ordnungsrahmen?
* Verantwortung des Einzelnen

Haltungen a la Freiheit für alle und insbesondere für mich
----------------------------------------------------------
...
