Blumen / daily-choices-info-package
===================================

[zurück](../../..)

Problematik
-----------
* Vergiftung und Ausbeutung der armen Leute dafür, dass sich hier jemand Schnittblumen in die Vase stellt.
    * [Giftige Schnitt-Rosen aus Übersee](https://www.bund.net/bund-tipps/detail-tipps/tip/auch-zum-valentinstag-giftfreie-liebesgruesse-verschenken), BUND-Artikel 2017

Beispiel für fairen Handel
--------------------------
* [Fairer Handel treibt Blüten](http://www.geo.de/natur/3575-rtkl-fairer-handel-treibt-blueten), geo.de, 2012
    * "Was der gerechte Handel für die Menschen in der Region bedeutet, hat sich unsere Autorin Kira Crome vor Ort angesehen - auf einer kenianischen Fairtrade-Rosenfarm"

Quelle?
-------
* ["An billigen Blumen aus Afrika für Europa klebt Blut – Rosen für die Welt statt Gemüse gegen den Hunger! Aldi und die Rosen aus Äthiopien!"](https://netzfrauen.org/2016/12/07/an-billigen-blumen/), 2016, ::aldi
    * "Deutschland ist der größte Markt für Schnittblumen innerhalb der EU."
    * "Kenia zum Beispiel ist einer der größten Exporteure von Schnittrosen der Welt. Täglich werden 500 Tonnen Blumen von Kenia aus weltweit in 60 verschiedene Länder geflogen. Die afrikanischen Arbeiter, meist Frauen, sind auf der untersten Stufe der Blumenindustrie und arbeiten oft unter menschenunwürdigen Bedingungen. Wie hoch der Preis der billigen Blumen ist, zeigen aktuelle Fälle aus Uganda und Kenia."

Mögliche Lösungswege
--------------------
* Blumen in der Natur genießen, dort wo sie wachsen (= Kombination aus Konsumverzicht und frischer Luft)
* Dafür sorgen, dass die Artenvielfalt in der eigenen Umgebung erhalten bleibt, siehe z. B. [Bio](../Bio).
    * z. B. artenreiche Blumenwiese statt gedüngtes und dadurch artenarmes Grasland (das Grasland wird oft für Tierfutter benötigt; siehe auch [Vegan](../Vegan))
* Dafür sorgen, dass auch anderswo die Umwelt nicht zerstört wird, indem z. B. keine Schnittblumen aus Übersee gekauft werden.
