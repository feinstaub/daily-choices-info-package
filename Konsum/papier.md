Papier
======

<!-- toc -->

- [Hintergrund](#hintergrund)
- [Sonstiges](#sonstiges)

<!-- tocstop -->

Hintergrund
-----------
* Papierverbrauch 2019:
    * "Deutschland verbraucht so viel Papier wie kein anderer Staat", 2019
        * https://www.welt.de/wirtschaft/article201430786/Rasanter-Anstieg-Deutschland-verbraucht-so-viel-Papier-wie-kein-anderer-Staat.html
* Papierverbrauch 2018:
    * Papierverbrauch steigt trotz Digitalisierung
 - Jeder Deutsche verbraucht im Durchschnitt pro Jahr 250 kg Papier. Damit konsumieren wir fast die fünffache Menge eines durchschnittlichen Erdenbürgers, der pro Jahr und Kopf nur 57 kg nutzt. Ökologisch gesehen ist dieser Verbrauch nicht tragbar und hat weitreichende Folgen."
        * https://www.verbraucherservice-bayern.de/themen/umwelt/papierverbrauch-steigt-trotz-digitalisierung
* Papierkrise
    * https://www.pro-regenwald.de/hg_papier
        * Graphik mit Verbrauchsanstieg in D. über die Jahre
* Papierverbrauch allgemein:
    * "Knapp jeder fünfte der jährlich geschlagenen Bäume fällt weltweit für die Produktion von Zellstoff für Papier."
        * https://www.regenwald-schuetzen.org/verbrauchertipps/papier/, abgerufen 2019
    * "Fast jeder zweite industriell gefällte Baum weltweit wird zu Papier verarbeitet - Zeitungen, Zeitschriften, Geschenkpapier, Verpackungen, Küchentücher oder Toilettenpapier. Damit ist die Papierindustrie eine Schlüsselindustrie, wenn es um die Zukunft unserer Wälder geht."
        * https://www.wwf.de/themen-projekte/waelder/papierverbrauch/zahlen-und-fakten/, abgerufen 2019
    * 2017: "Noch nie wurde so viel Papier verbraucht wie heute"

* Illegaler Holzeinschlag
    * https://www.wwf.de/themen-projekte/waelder/waldvernichtung/illegaler-holzeinschlag/

Sonstiges
---------
* Haushalt: Papier mit dem [Blauen Engel](https://www.blauer-engel.de/de)
    * Positive-Beispiele: Frosch

* Bewusstsein / Handeln
    * https://www.deutscheumweltstiftung.de/spenden-sie-baeume-fuer-unsere-schulpflanzaktionen/

