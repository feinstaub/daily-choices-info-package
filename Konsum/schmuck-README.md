Edelmetall-Schmuck / daily-choices-info-package
===============================================

[zurück](../../..)

Gold
----
### Problematik
* Gold-Abschnitt aus Film: [Let's make money (2008)](https://de.wikipedia.org/wiki/Let%E2%80%99s_Make_Money)
* Film: [Blood Diamond (2006)](https://de.wikipedia.org/wiki/Blood_Diamond), siehe Zitat von DiCaprio
* 2018
    * https://www.br.de/radio/bayern2/sendungen/radiowissen/mensch-natur-umwelt/gold-edelmetall-100.html
        * Knappheit
        * Mythos
        * "Sucht und Beschaffungskriminalität"
        * Geschichte: "Eroberung, Plünderung und Zerstörung der Inka-Hauptstadt Cusco im November 1533"
        * Minenarbeit
        * "Homo homini et terrae lupus"
        * "lohnt sich auch der Abbau auch in entlegenen, minder ergiebigen und schwer erschließbaren Minen. Vorausgesetzt, das Umfeld stimmt"
            * "billige Arbeitskräfte, wenig staatliche Hindernisse, keinen Bürgerprotest [...] keine ökologischen Hürden"
        * "Säuren, Laugen, Umweltgifte"
        * Menschenrechte: "Eigentums- und Besitzrechte indigener Völker missachtet"
* 2017
    * z. B. in der [Zentralafrikanischen Republik](https://de.wikipedia.org/wiki/Zentralafrikanische_Republik)
        * siehe ["Kämpfe in Zentralafrika Es geht um Land, Macht und Geld"](http://www.tagesschau.de/ausland/zentralafrikanische-republik-buergerkrieg-101.html), ARD, 2017, Video, 2 min
            * unter anderem geht es auch um Diamanten
            * Kämpfe begannen als Glaubenskrieg zwischen Christen und Muslimen; mittlerweile geht es aber um andere Themen, siehe Titel
    * FRAGEN zu folgender Situation: Europa, Amerika und China sichern sich Boden in Afrika, um Lebensmittel anzubauen
        * Ohne die Technik aus dem Westen wären die Flächen ungenutzt, also gut so?
        * Mit der Pestizid- und Intensiv-Technik aus dem Westen werden die Böden schnell ausgelaugt. Was bleibt danach für die regionale Bevölkerung zurück?
        * Bei der aktuellen Flächennutzung geht das meiste ins Ausland; ein bisschen fällt sicher für die lokale Bevölkerung ab. Ist das gut so?
            * oder über Umwege über Europa wieder zurück nach Afrika. Ist das sinnvoll?
        * mögliche Antworten siehe z. B. https://de.wikipedia.org/wiki/Land_Grabbing
            * ausländische Investitionen prinzipiell gut, wenn Rahmenbedingungen eingehalten werden, ansonsten gibt es "Auswirkungen auf Arme, deren Zugang zu Land gefährdet sei"
                * also die Armen haben mal wieder Pech
            * "weist darauf hin, dass das von Regierungen verpachtete Land oft kein Niemandsland sei, sondern Teil traditioneller Landnutzungssysteme, für die es selten einklagbare Eigentumsrechte gebe."
            * "Oft gebe es keine hinreichenden Entschädigungen und für die Befriedigung des lokalen Bedarfs stehe weniger Fläche zur Verfügung."
            * "Auch die Wassernutzung könne zum Problem werden, wenn umliegenden Regionen weniger Wasser erhielten."
            * Neokolonialismus?
            * Wer sorgt für korrekte Rahmenbedingungen? Was passiert in der Zwischenzeit?

### Lösungen?
* Es gilt der Luxus-Güter-Abwägungsansatz
    * Muss das sein?
    * Kommt Schönheit kommt von innen?

* 2018
    * ["Gutes Gold, schlechtes Gold: Welche Alternativen es gibt"](https://www.ecoreporter.de/artikel/gutes-gold-schlechtes-gold-wieso-das-edelmetall-so-schaedlich-ist-und-welche-alternativen-es-gibt-25-01-2018/), ECOreporter - Magazin für nachhaltige Geldanlagen, 2018
        * ...
* 2012
    * ["Faires Gold"?](http://www.geo.de/natur/oekologie/3338-rtkl-faires-gold), geo.de, 2012
        * "In Großbritannien, Kanada und den Niederlanden kann man schon heute Gold kaufen, das sozialverträglich und ohne Gifteinsatz geschürft wurde. Deutschland soll 2013 dazukommen"
            * Wo bekommt man das konkret?
            * Trauringe?
