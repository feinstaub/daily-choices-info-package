Hintergrund: Palmöl
===================

Warum und Folgen
----------------
### Warum
* Schnell wachsende Pflanze; billig

### Woher
* vor allem aus Indonesien
    * Borneo, Home of the Orang-Utan

### Folgen
* Schlimme Folgen: https://www.regenwald.org/themen/palmoel
    * "Regenwald im Tank und auf dem Teller"
    * "Auf einer Fläche so groß wie Neuseeland mussten die Regenwälder, Mensch und Tier bereits den „grünen Wüsten“ weichen."
    * "dass Palmöl inzwischen in jedem zweiten Supermarktprodukt steckt."
        * das heißt Umweltvorreiter zeigen, dass es auch anders geht; wie z. B. leckere Nutella-Alternativen auf Basis von heimischem Sonnenblumenöl
    * "Die Reportage Asimetris zeigt, warum die Menschen zu den Verlierern des Palmölbooms gehören."
        * TODO, 30 min
    * Was kann man tun?
        * "Bitte lesen Sie die Inhaltsangaben auf den Verpackungen und **lassen Sie palmölhaltige Produkte im Laden stehen.**"
    * **Landraub**
        * "In Indonesien stehen mehr als 700 Landkonflikte in Zusammenhang mit der Palmölindustrie.
        Auch auf sogenannten „nachhaltig bewirtschafteten“ oder „Bio“-Plantagen kommt es immer wieder zu Menschenrechtsverletzungen."
    * Doku: "Geheimnisse Asiens - Die schönsten Nationalparks - Borneos wilde Elefanten", arte, 2018, NDR, arte, 45 min
        * https://www.arte.tv/de/videos/059524-004-A/geheimnisse-asiens-die-schoensten-nationalparks/
        * "Durch Sabah im Norden Borneos fließt der 560 Kilometer lange Kinabatangan.
        Dort lassen sich zahlreiche wilde Tiere entdecken: Orang-Utans, Elefanten, Krokodile und Nasenaffen.
        Aber die Artenvielfalt ist bedroht: Immer mehr Regenwald muss **Palmöl-Plantagen** weichen.
        Die **Einheimischen** kämpfen für den Erhalt des Regenwaldes und der einzigartigen Tierwelt."
        * war das in dieser arte-Doku?
            * für Elefanten ist nur noch ein dünner Streifen Urwald übrig
            * Elefanten werden sogar vergiftet, wegen Palmölplantagen

* [Graphiken zur Verwendung von Palmöl in Deutschland](http://www.zeit.de/wissen/umwelt/2016-08/palmoel-plantagen-regenwald-umweltschutz-studie-wwf), zeit.de, 2016
    * "Palmöl steckt in jedem zweiten Supermarktprodukt, seinetwegen sterben Regenwälder. Jetzt sagt der WWF: Das Öl komplett zu ersetzen, wäre für die Umwelt noch schlimmer."
    * Laut User-Comments bestes Zitat: "Würden wir auf Palmöl als Biokraftstoff verzichten und einen bewussten Verbrauch von Konsumgütern wie Schokolade, Süß- und Knabberwaren, Fertiggerichten und Fleisch etablieren, könnten wir rund 50 Prozent des Palmölverbrauchs einsparen." / außerdem: sinnvolle Lösung nur durch Konsumreduktion unnötiger Güter: User: "Einfach mal weniger unnötiges Zeug kaufen scheint zu schwer zu sein.", siehe weitere User-Comments
* Verwendung:
    * in Lebensmitteln
        * Lösung: auf Zutatenliste schauen und vermeiden (mit etwas Übung gut möglich)
    * für Biodiesel
        * Förderung von schädlichen Palmölplantagen
            * 2017: https://www.regenwald.org/petitionen/908/palmoel-stopp-biodiesel-stopp-sofort
        * Lösung: siehe [Auto](../Auto)
* Bekanntes Negativ-Beispiel:
    * Die Schokostreichcreme mit dem N
        * Palmöl (nicht fair), Zucker (ungesund), Kakao (Kinderarbeit)
    * Positive Beispiele: ...

Kleine Beispiele
----------------
* Zwiebackhersteller schreibt: "seit 1850 [...] Genuss aus hochwertigen und möglichst regionalen Zutaten". 1) Wieviel Pestizide und Dünger wurden für die Zutaten verwendet? 2) In der Zutatenliste steht nach Weizenmehl, Zucker an dritter Stelle Palmöl.

Lobby-Aktivitäten und Greenwashing
----------------------------------
* siehe kantine.md
