Rohstoffe
==========

<!-- toc -->

- [Beispiel Coltan](#beispiel-coltan)
- [Wasserraub](#wasserraub)
- [Beispiel Gold](#beispiel-gold)
- [Beispiel Kupfer](#beispiel-kupfer)

<!-- tocstop -->

Beispiel Coltan
---------------
* "Coltan - Kongos Verfluchter Schatz - DOKU", 2018, ZDF, 45 min
    * https://www.youtube.com/watch?v=5qM3BgGATPo
    * TODO
    * ...
    * ...
    * ...

* Beispiel Websuche "Coltan"
    * http://www.regenwald-schuetzen.org/regenwald-wissen/holz-papier-soja-co/coltan-handy.html
    * https://www.aktiv-gegen-kinderarbeit.de/produkte/bodenschatze/coltan/
    * Doku: [Blood Coltan (2007)](https://www.youtube.com/watch?v=in0A8SFL3XM)
    * [Weltspiegel-Reportage: "Sie hatten keine Menschlichkeit"](http://www.tagesschau.de/ausland/ostkongo-101.html), 2018
        * "Im Ost-Kongo kämpfen mehr als 100 Rebellengruppen um wertvolle Bodenschätze. Systematisch vergewaltigen sie Frauen, um die Gesellschaft zu zerstören. Hunderttausende erleiden unermessliches Leid und benötigen Hilfe."

Wasserraub
----------
* http://www.wasserraub.de/abbau-von-rohstoffen/
    * Bergbau
    * Lithium
    * Gold

Beispiel Gold
-------------
* ...

Beispiel Kupfer
---------------
* Kupfer: [Stealing Africa - Why Poverty? (2013)](https://www.youtube.com/watch?v=WNYemuiAOfU)
    * [Why poverty YT channel](https://www.youtube.com/channel/UC5McHEvkqTK4wovjv5ChLgg)


Was tun?
* Positive Beispiele:
    * Fairphone: moved
    * [Pangea-Sun-ein-modulares-Open-Source-Notebook](http://www.heise.de/newsticker/meldung/Pangea-Sun-ein-modulares-Open-Source-Notebook-3127864.html)
    * konsequentes Recycling, Cradle-to-Cradle: moved
    * memo.de
    * Frosch
