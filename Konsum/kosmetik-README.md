Kosmetik / daily-choices-info-package
=====================================

[zurück](../../..)

<!-- toc -->

- [Tierversuche](#tierversuche)
  * [Sonstiges](#sonstiges)
- [Lösung](#losung)
- [Fazit - Auf Labels achten](#fazit---auf-labels-achten)

<!-- tocstop -->

Tierversuche
------------
* Video: ["Unsinn Tierversuch - Ein animierter Aufklärungsfilm der Ärzte gegen Tierversuche"](https://www.youtube.com/watch?v=xxFbQfbxXjI), 2013, 6 min
    * ...
    * ...
    * Tiere haben keine Zivilisationskrankheiten
    * Tiere müssen daher erst künstlich krank gemacht werden (Diabetes, Herzinfarkt, Arteriosklerose, Bauchfellentzündung)
    * maßgeschneiderte Tiere in großer Auswahl
    * Symptome vs. Ursachen
    * im klinischen Test: 90 % wirken nicht oder haben zu viele Nebenwirkungen
    * psychische Krankheiten wie Depression: Ratten Elektroschoks => ausweglose Situation
    * Sinnlose Trivialversuche
    * ...
    * ...
* [Infos zu Tierversuchen von Soko Tierschutz](https://www.soko-tierschutz.org/#!tierversuche/z50r9)
* [Anti-Vivisection Commercial](https://www.youtube.com/watch?v=KYSb-ewqvII), 30 Sekunden, 2000, engl.
* Video: [Woran soll man denn sonst testen? - Tierversuchsfreie Forschung](https://www.youtube.com/watch?v=m_a92xvL5is), 30 min, 2014
    * positive Beispiele von Firmen
    * wirkt fortschrittlich und human
* Webseite: https://aerzte-gegen-tierversuche.de/de/
* Datenbank mit detaillierter Abfragemöglichkeit zu Tierversuchen (z. B. Filterung nach Tierart): https://aerzte-gegen-tierversuche.de/de/datenbank-tierversuche
* Video: [Aufnahmen aus Tierversuchslaboren - Animal Equality Recherche](https://www.youtube.com/watch?v=dvc8vqiqXkA), 2015, 4 min
    * ruhige und sachliche Sprecherin
    * Universität Madrid, Spanien
    * die meisten an Mäusen
    * irgendwas reingespritzt und geschwenkt
    * Schwein aufgeschnitten, Orange beobachtet, zur Übung wieder zugenäht und dann getötet
    * Kahle Käfige für Makaken (Affen), die auf Versuche warten - Medienwirkung
    * Beagle (Hunde) - Medienwirkung
    * (zum Glück eher weniger Bilder als Text)
* Kosmetik: [Blog-Artikel über Cruelty-Free Kosmetika](http://www.kosmetik-vegan.de/erbse/faq-tierversuche-in-der-kosmetikindustrie/) (2011-2014). Pflege auf Mineralölbasis vermeiden. Mehr zu [Kosmetik und Tierversuche](../Kosmetik)

### Sonstiges
* Pflege auf Mineralölbasis vermeiden (?)
* siehe auch [Palmöl-Problematik](palmöl.md)

Lösung
------
* https://aerzte-gegen-tierversuche.de/de/infos/kosmetik-chemikalien/2056
    * "Auch wenn es »Tierversuchsfreiheit« im Kosmetikbereich nicht gibt, gilt generell: Mit der Unterstützung von Firmen, die sich zu einer tierversuchsfreien, veganen Firmenpolitik bekennen, zeigen Sie den großen, Tierversuche durchführenden Konzerne die rote Karte."

Fazit - Auf Labels achten
-------------------------
* Übersicht siehe https://aerzte-gegen-tierversuche.de/de/infos/kosmetik-chemikalien/2056
* siehe [Blog-Artikel über Cruelty-Free Kosmetika](http://www.kosmetik-vegan.de/erbse/faq-tierversuche-in-der-kosmetikindustrie/) (2011-2014).
