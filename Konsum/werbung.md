Werbung
=======

<!-- toc -->

- [Hintergrund](#hintergrund)
  * [arte: Public Relations - Manipulation der Masse](#arte-public-relations---manipulation-der-masse)
  * [Bedarfsweckung und Bedarfslenkung](#bedarfsweckung-und-bedarfslenkung)
  * [Einstieg, Werbe-Psychologie](#einstieg-werbe-psychologie)
  * [Sell me this pen](#sell-me-this-pen)
  * [Werbung: Konsumauswirkungen ausblenden oder verzerren](#werbung-konsumauswirkungen-ausblenden-oder-verzerren)
  * [Teil 2](#teil-2)
  * [Weiteres zu Psychologie und Neurowissenschaften im Dienst der Werbung](#weiteres-zu-psychologie-und-neurowissenschaften-im-dienst-der-werbung)
- [Beispiele](#beispiele)
  * [Falscher Winterreifen-Claim](#falscher-winterreifen-claim)
  * [Die Kunst der Verführung](#die-kunst-der-verfuhrung)
  * [Milchpulver für Babynahrung](#milchpulver-fur-babynahrung)
  * [Werbelügen aufgedeckt - Der Goldene Windbeutel](#werbelugen-aufgedeckt---der-goldene-windbeutel)
- [Für die besten Dinge wird keine Werbung gemacht](#fur-die-besten-dinge-wird-keine-werbung-gemacht)
- [User-Comments](#user-comments)
  * [bei Werbung: Konsumverzicht](#bei-werbung-konsumverzicht)
- [Humor](#humor)
- [Siehe auch](#siehe-auch)

<!-- tocstop -->

Hintergrund
-----------
### arte: Public Relations - Manipulation der Masse
* Doku: Public Relations - Manipulation der Masse | Doku | ARTE, 2019, ::pr
    * https://www.youtube.com/watch?v=87T3SMkL8HM, 45 min
    * (u. a. vom Bürger zum Verbraucher)
    * (wie das amerikanische Frühstück plötzlich nicht mehr ohne Schinken und Ei auskam)
    * ...
    * TODO
    * ...

### Bedarfsweckung und Bedarfslenkung
* zum Thema, dass Werbung zu mehr Konsum führt
    * z. B. https://www.aok-gesundheitspartner.de/imperia/md/gpp/he/hilfsmittel/vertraege/orthopaedietechnik/01_07_14_g_anlage_15_werbung.pdf
        * "Anlage 15 zum Gesamtvertrag      Werbung Orthopädie-Technik / Sanitätsfachhandel / Orthopädie-Schuhtechnik / Apotheken"
        * "Werbung hat den Charakter der Bedarfsweckung und Bedarfslenkung."
        * "Eine  Werbung  für  Gesundheitsleistungen  berührt  mithin  weniger  den  eigentlichen Nachfrager  der  Leistungen  als  vielmehr  die  Krankenkasse.
            Aus  dieser  Sicht  ist  eine Werbemaßnahme  dann  unzulässig,  wenn  sie  darauf  gerichtet  ist,  Versicherte  zur  In-anspruchnahme von Vertragsleistungen zu veranlassen, die dem Gesundheitszustand nach nicht notwendig sind."
        * "Als  Beispiel  einer  unzulässigen  W erbemaßnahme  sind  Anschreiben von  Vertragspartnern  an  ihre  „Kunden“  zu  nennen,
            in  denen  sie  darauf  hinweisen, dass  turnusmäßig  ein  neuer  Leistungsanspruch  bestehen  würde
            oder  Anzeigen  in Print- oder elektronischen Medien, mit denen die Begehrlichkeit geweckt werden soll."
        * "Ebenso  die  gezielte  Beeinflussung  des  Arztes, bestimmte  Artikel  namentlich  zu  verordnen."

### Einstieg, Werbe-Psychologie
* Video: ["Was wäre TV-Werbung für Pizza ohne geschmolzene Käsefäden, deren Anblick uns den Mund wässrig macht?"](https://www.youtube.com/watch?v=2ug36E3xO4E), 2012, 9 min
    * welche Arbeit hinter Lebensmittelwerbung steckt
* Artikel: ["Werbung und Psychologie: So manipulieren Marken unser Unterbewusstsein"](https://blog.hubspot.de/marketing/wie-marken-uns-manipulieren), 2015 - 2018
    * mit Beispielwerbespott
* Artikel: [Zur Werbepsychologie aus einem Gründerblog](https://www.gruenderszene.de/allgemein/psycho-tricks-werbung), 2015
    * "Wer verkaufen will, muss wissen, wie potenzielle Abnehmer ticken. Schon seit jeher nutzt die Werbung diese raffinierten Psycho-Tricks, um mehr zu verkaufen."
* Artikel: Studienarbeit: ["Ethik und Moral in der Werbung. Im Kontext der persuasiven Kommunikation"](https://www.grin.com/document/267113), 2012

siehe auch psychologie.md

### Sell me this pen
* ["Sell me this pen"](https://www.youtube.com/watch?v=E6Csz_hvXzw), 2018
    * People [mostly] buy because of emotion and justify with logic
        * (vs. homo economicus myth)
        * "don't just push your product / your services, don't push the features and benefits; think about those emotional heart buttons you are pushing."
    * "People don't buy their way into something, they buy their way out of something"
        * have a problem that needs to be solved
        * what are the pains people have and how can you relief them from it
    * "People don't buy products and services, they buy stories"
        * add story to an item
        * how can you inject story into things?
            * own stories and/or customer stories
        * "Facts tell, stories sell"

### Werbung: Konsumauswirkungen ausblenden oder verzerren
Häufiger - auch unbewusster - Werbekonsum verändert das Weltbild. Hin zu einem Zerrbild.

nicht immer, aber sehr oft:

* Tierproduktwerbung
    * vs. Artgerechtigkeit
* Positive Darstellung von unreflektiertem, eigennützigem Resourcen-Konsum
    * vs. Umwelt und Gesellschaft
* Bekleidungswerbung
    * vs. Produktionsbedingungen (Sklavenarbeit)
* Süßigkeitenwerbung
    * vs. Produktionsbedingungen (Kinderarbeit bei Kakao) und Auswirkungen auf Konsumenten (Krankheit)
* Werbung für Software und digitale Dienste
    * vs. Privatsphäre des Endnutzers und die der Dritter (dieses Thema wird meist _komplett_ ausgeblendet bzw. mit Nebelkerzen gearbeitet)

### Teil 2
* Ein großer Teil der teuren Fernseh- und Internet-Werbung hat negative Auswirkung auf die Fähigkeit, nachhaltig und konsumkritisch zu werden oder zu bleiben.
    * **Sich aktiv trennen von Werbung** ist ein wichtiger Baustein von nachhaltigem Verhalten.
    * im Internet: **Überwechungsblocker verwenden** (z. B. uBlock Origin), auch Werbeblocker genannt

* [Informationsüberflutung in Zeiten der Massenmedien](https://hosting.1und1.de/digitalguide/online-marketing/verkaufen-im-internet/informationsueberflutung-das-zuviel-an-werbung), 15.03.17, Verkaufen im Internet, 1&1 Internet SE
    * "Im digitalen Zeitalter sind wir einer enormen Informationsüberflutung ausgesetzt. Das beeinflusst auch das Kaufverhalten bzw. erschwert die Kaufentscheidung vieler Konsumenten. Schuld daran sind vor allem die Massenmedien: Das Fernsehen bietet uns nicht nur unzählige Dokumentationen und Nachrichtensendungen, sondern zahlreiche Werbespots. Auch das Internet überschüttet User täglich mit Angeboten und Werbemails. Zudem ist der Markt im Zuge der Globalisierung mit **einer Vielzahl an Produkten nahezu übersättigt**. Ein kleines Beispiel: Allein in Deutschland gibt es mehr als 85 Sorten Toilettenpapier. Nach welchen Kriterien soll der Nutzer noch seine Auswahl treffen, wenn es selbst zu den simpelsten Alltagsgegenständen nicht nur unzählige Varianten, sondern auch Informationen gibt?"
    * Artikel: Vortragsscript: [Zur Käuflichkeit der Moral - Wie politischer Konsum zu einer ethischen Ökonomie beiträgt](http://www.haraldlemke.de/texte/Lemke_Moralkaufen.pdf), 2014
        * Die Übersättigung mit Produkten bringt den Konsumenten in eine einmalige Situation.
        * Artikel: Interview: ["Kaum etwas ist so politisch wie unsere tägliche Ernährung, findet der Philosoph Harald Lemke"](http://www.geo.de/GEO/natur/green-living/ernaehrung-warum-haben-sie-unser-essen-satt-herr-lemke-79973.html), geo.de-Interview, 2015
    * "Information Overload bzw. Informationsüberlastung führt dazu, dass eine Person aufgrund eines Übermaßes an Informationen nicht mehr in der Lage ist, eine gut begründete (Kauf-)Entscheidung zu treffen."
        * Also: sich von Werbung fernhalten, überall, wo es geht.
        * Beispiele von vermeidbarer Werbung:
            * Einlog-Bildschirme von **Mailanbietern mit sinnlosem News/Werbungsmix**: https://www.gmx.de, https://www.web.de
            * Vermeidung durch Mailanbieter ohne Werbung; Verwendung eines Mailclients (Mozilla Thunderbird)
    * Die Auswirkungen von Informationsüberflutung
        * stressauslösend
    * "So herrscht ein harter Konkurrenzkampf, bei dem Unternehmer mit allen Werbemitteln versuchen, die Aufmerksamkeit der Konsumenten zu gewinnen. Doch das erweist sich aufgrund der zunehmenden Informationsfülle als äußerst schwierig. Und die Informationsüberflutung wird voraussichtlich weiterhin zunehmen."
    * "Die begrenzte Kapazität des menschlichen Gehirns"
        * ...
        * "Die Strategie von Marketern: Aggressive Werbekampagnen"
        * ...
        * wieder: Werbung vermeiden hilft.

### Weiteres zu Psychologie und Neurowissenschaften im Dienst der Werbung
* http://de.euronews.com/2011/09/19/werbung-versus-geschmack, 2011
    * "Neurowissenschaftler bewiesen 2004, dass Werbung unser Kaufverhalten mehr beeinflusst als Geschmack. Dies macht sich inzwischen das Neuro-Marketing zunutze, eine neue und umstrittene Disziplin des Marketings."
* [Neuro-Blog: "Unterschwellige (subliminale) Werbung"](http://www.thinkneuro.de/2011/04/30/video-unterschwellige-subliminale-werbung/), 2011

Beispiele
---------
### Falscher Winterreifen-Claim
* [Artikel 2017](http://www.echo-online.de/wirtschaft/wirtschaft-suedhessen/gerasch-communication-gmbh--co-kg-in-darmstadt_18346317.htm)

### Die Kunst der Verführung
* 2017: "Seit der erste Supermarkt in den Fünfizigerjahren eröffnet wurde,
    haben wir uns von der Bedarfsdeckung zur Bedarfsweckung entwickelt" -- Susanne Umbach, Verbraucherzentrale Rheinland-Pfalz

### Milchpulver für Babynahrung
* Werbung wirkt
* siehe marken-multinational.md

### Werbelügen aufgedeckt - Der Goldene Windbeutel
* https://www.foodwatch.org/de/informieren/werbeluegen/2-minuten-info/

Für die besten Dinge wird keine Werbung gemacht
-----------------------------------------------
* z. B.:
    * einen Waldspaziergang
    * einen Spaziergang in der Natur
    * die Natur an sich
    * zwischenmenschliche Beziehungen (gerade bestimmte Softwaredienste haben die Vereinsamung des Nutzers als Nebeneffekt)
    * respektvoller Umgang mit der Natur und unserer Mitwelt
    * [freiheitsgewährende Software](../FreieSoftware)
* Warum? --> Weil es für einen einzelnen Werbetreibenden keinen Sinn ergibt für sowas Geld auszugeben.
    * siehe auch die Allmende-Herausforderung (nachhaltigkeit.md)
* Das heißt folglich: diese schönen Dinge werden durch Werbung aus dem Bewusstsein gedrängt und mit Quatsch ersetzt.

User-Comments
-------------
### bei Werbung: Konsumverzicht
* [über Seiten mit aggressiver Werbung](https://www.heise.de/forum/heise-online/News-Kommentare/Verlegerverband-warnt-vor-Googles-Adblocker/Die-ultimative-Faehigkeit-Konsumverzicht/posting-30488253/show)
    * "Die Erfahrung hat gezeigt: Betreiber die am aggresivsten Werbung schalten, die nutzen auch oft Clickbait und Inhalte minderwertiger Qualität." => Diese Seiten aktiv meiden.

Humor
-----
Werbung ist vor allem lustig, wenn sich die Bürger einen Spaß draus machen:

* [Supermarkt-Theke: Warengewichte gut einschätzen](https://www.youtube.com/watch?v=ozIDIhTsfA4)
* [Supermarkt: Was ist im Ei?](https://www.youtube.com/watch?v=kFUZs_tCrc8) (Thema: Kloake)
* [Supermarkt: Was ist im Ei 2?](https://www.youtube.com/watch?v=qEgl7gQ7xQk)
* [Auto-Fenster-Reparatur-Gewinnmaximierung](https://www.youtube.com/watch?v=KcWYsgJBoz4)
* [Computerkauf für Listige](https://www.youtube.com/watch?v=clTSOT-6-Iw)

Siehe auch
----------
* [Hintergrund: Konsumgesellschaft](konsum-gesellschaft.md)
