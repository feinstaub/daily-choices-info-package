Pelz / daily-choices-info-package
=================================

<!-- toc -->

- [Bildmaterial](#bildmaterial)
- [2017: Warum uns echtes Fell so oft als Kunstpelz verkauft wird](#2017-warum-uns-echtes-fell-so-oft-als-kunstpelz-verkauft-wird)
- [2016: Echter Pelz statt Kunstfell](#2016-echter-pelz-statt-kunstfell)
- [2014: Echter Pelze im Handel nicht erkennbar / Kennzeichnungsgesetze werden nicht kontrolliert](#2014-echter-pelze-im-handel-nicht-erkennbar--kennzeichnungsgesetze-werden-nicht-kontrolliert)
- [2018: "Aber es ist doch besser geworden als früher; alles Kunstfell" (leider falsch)](#2018-aber-es-ist-doch-besser-geworden-als-fruher-alles-kunstfell-leider-falsch)
- [Chemikalien im Pelz](#chemikalien-im-pelz)
- [Hintergrund, Personen, Labels](#hintergrund-personen-labels)
- [Beispiele von Unternehmen, die aktiv Pelz verkaufen](#beispiele-von-unternehmen-die-aktiv-pelz-verkaufen)
- [Positive Entwicklungen - Norwegen](#positive-entwicklungen---norwegen)

<!-- tocstop -->

### Bildmaterial
* Video: [Wer Pelz trägt, trägt den Tod - Kino-Spot mit Peter Maffay](https://www.youtube.com/watch?v=K1ivSql2yf0), ANIMALS UNITED, 30 Sekunden, 2014, tag:offline
    * Peter Maffay setzt sich dafür ein, dass keiner mehr Pelz kauft
    * http://www.gelabelt.de/

* Doku: ["Gier nach Pelz: So leiden die Tiere in Europa - ARD Plusminus"](https://www.youtube.com/watch?v=Y3_7Kqo3_Dg), 2017, 13 min, 15.02.17, tag:class2018
    * 2017: aktuelle (leider weiterhin schlimme) Zustände
    * Die Leute kaufen dieses Jahr wie verrückt Pelz und die meisten sind nicht aufgeklärt.
        * Korrektur am Ende: laut Umfrage wissen die Leute, was sie tun, es ist ihnen aber egal. Oder sind so konditioniert zu denken, als Einzelner könne man nichts tun.
    * Undercover-Einsatz von Tierschützern
    * Fies: Füchse in Polen
        * Das ist das "gute Produktion"
        * Verstecke Kamera: Brutale Behandlung durch Mitarbeiter
        * (Hintergrundwissen: anal electrocution)
    * Nerzfarmen in Deutschland verstoßen gegen geltendes Recht. Regierungs-Politiker ist dies egal.
    * ...
    * Online-Shops
        * "Woher haben Sie den Bommel?"
            * ... weils schöner aussieht
    * ...
    * Einfluss der Lobby in der EU
    * ...
    * Leute die Bilder zeigen
        * "das müsste das ja von jedem beachtet werden". Also jeder weiter wie bisher?

* 2014: NDR-Info
    * http://www.ndr.de/ratgeber/verbraucher/So-leben-und-sterben-Pelztiere,pelz128.html

* Video: [Anti-Fur Advertisement](https://www.youtube.com/watch?v=aiJm59sG6SI) (90er-Jahre, Gary Yourofsky), 1 min, engl.
    * "Injustice anywhere is a threat to justice everywhere. This IS injustice."

### 2017: Warum uns echtes Fell so oft als Kunstpelz verkauft wird
* [" Verbrauchertäuschung - Warum uns echtes Fell so oft als Kunstpelz verkauft wird](https://www.geo.de/natur/nachhaltigkeit/18054-rtkl-verbrauchertaeuschung-warum-uns-echtes-fell-so-oft-als-kunstpelz), 2017
    * "Wer Kunstpelz will, kauft oft - ohne es zu wissen - Haut und Haar von echten Tieren. Woran liegt das? Und wie lässt sich die Täuschung erkennen?"
    * "Bei 44 von 49 Teilen fehlte die korrekte Deklaration"
    * "Intransparente Lieferketten"
        * Selbst wer guten Willens ist, hat es schwer. Lösung: keine Pelzmode kaufen

### 2016: Echter Pelz statt Kunstfell
* Vorsicht: "Echter Pelz statt Kunstfell", falsch deklarierter echter Pelz, weil günstiger
    * Artikel: [Stiftung Warentest 2016](https://www.test.de/presse/pressemitteilungen/Echter-Pelz-statt-Kunstfell-Immer-wieder-tricksen-Hersteller-denn-echter-Pelz-ist-oft-guenstiger-als-Kunstfell-4972399-0)
    * "Immer wieder tricksen Hersteller, denn echter Pelz ist oft günstiger als Kunstfell"
    * => am besten kein Pelz
* ["Pelzmode: Wie uns echte Tierfelle als Kunst­pelz verkauft werden "](https://www.test.de/Pelzmode-Wie-uns-echte-Tierfelle-als-Kunstpelz-verkauft-werden-4970078-0/), Stiftung Warentest, 2016
    * "Aus der Wintermode sind Pelz­kragen und Fell­bommel nicht mehr wegzudenken. Für die Accessoires sterben jedes Jahr Millionen Tiere. Selbst wer Kleidung mit Kunst­pelz kaufen will, bekommt teil­weise echtes Fell unterge­jubelt"
    * "Nur zwei der befragten Händler oder Hersteller reagieren"
    * "Was sind die Gründe für den Schwindel? Zum einen Profit: Vor allem Felle von Marderhunden werden sehr billig auf dem Markt angeboten. In China werden Marderhunde massenhaft in Farmen gehalten, oft unter erbärmlichen Bedingungen, wie Tierschützer berichten."
    * "„Wasch­bär“ klingt besser als „Marderhund“"
    * "Verstöße werden kaum geahndet"
    * "Kein Pelz ohne Tierleid"
    * "Füchse und Katzen in Käfigen"
    * "Tiere werden mit Knüppeln erschlagen"
        * "Friedrich Mülln von Soko Tier­schutz filmte 2013 verdeckt auf einem der größten Pelzmärkte in China: „Über 10 000 Tiere wurden da angeboten, vor allem Marderhunde und Füchse. Sie werden meist mit Knüppeln erschlagen und anschließend gehäutet. Teil­weise leben sie dann noch.“ Anschließend landen die Felle bei großen Auktions­häusern – in der Regel über zahlreiche Zwischen­stationen von der Gerberei über das Färben und Zuschneiden des Fells bis hin zum Aufnähen auf Vorprodukte. „Dort geht es nur nach Qualität“, sagt Mülln. „Woher der Pelz stammt, ist dann nicht mehr fest­stell­bar.“"
    * "Das schwäbische Mode­haus Marc Cain schrieb uns, es würde ausschließ­lich Felle von Nutztieren verarbeiten, in der Regel Lamm oder Ziege. Auf unseren Hinweis, dass die Jackenkragen der aktuellen Kollektion nicht nach Nutztier aussehen, räumte die Firma ein: Die Nutztier­regelung gelte erst ab der kommenden Saison. Derzeit verwende das Unternehmen unter anderem Marderhundfell. Woher es stammt, verriet Marc Cain nicht. Auch auf die Frage, warum sie echten Pelz statt Webpelz verwenden, erhielten wir keine Antwort."
    * "Es geht auch anders - Viele große Marken­anbieter sehen das anders. Sie lassen Pelz­besätze nur noch aus Textil fertigen."
    * "Gift im Pelz"
        * "die von uns gekauften Pelze stark chemisch behandelt worden waren"
        * "Ein Grund mehr, nicht jedem Trend zu folgen."

### 2014: Echter Pelze im Handel nicht erkennbar / Kennzeichnungsgesetze werden nicht kontrolliert
* ["Kunstpelz oder Echtpelz? - Handel verheimlicht Herkunft"](https://www.br.de/br-fernsehen/sendungen/kontrovers/kunstpelz-echtpelz-kennzeichnung-100.html), BR, 2014
    * eingebettes Video: "Pelz vs. Kennzeichnung 2014-11 - Handel verheimlicht Herkunft, BR, 5min (cbba03ea-3062-4f15).mp4", 2014-11, tag:offline
    * "Handel verheimlicht Verarbeitung echter Pelze"
    * "Ein flauschiger Bommel an der Mütze, ein kuscheliger Kragen an der Jacke – aber bitte Kunstpelz. Viele Deutsche lehnen echten Pelz ab. Und ahnen nicht, dass der Handel ihnen genau den unterjubelt, trotz Kennzeichnungspflicht."
    * Passantin auf der Straße denkt, wenn es so billig ist, kann es doch kein echtes Fell sein. Falsch gedacht.
    * inkl. Bilder von Tieren, die totgeschlagen werden
* ["Pelze an Kleidung: Panorama 3 hakt nach"](https://www.ndr.de/fernsehen/sendungen/panorama3/Pelze-an-Kleidung-Panorama-3-hakt-nach,pelz234.html), NDR, 2014
    * eingebettes Video: "Pelz vs. Kennzeichnung 2014-12 - Pelze an Kleidung, NDR hakt nach, 4min (panoramadrei1558).mp4", 2014-12, tag:offline
    * "Statt 100 Prozent Polyester Kaninchenfell und "hundeähnliche" Pelze."
    * leider keine Verbesserung

### 2018: "Aber es ist doch besser geworden als früher; alles Kunstfell" (leider falsch)
* kurz
    * Umsatz mit Pelzen steigt (siehe nachfolgender Artikel). Wenn weniger Pelz verkauft würde, wie passt das zusammen?
* Artikel: ["Deutschland, Land der Pelzkragen"](http://www.zeit.de/wissen/umwelt/2018-02/tierschutz-pelz-fellkragen-mode-nerz-fuchs-fleischkonsum), ZEIT.de, 2018
    * User-Comment: "Krass. Ich dachte das wäre alles Kunstpelz inzwischen."
    * "Kein Mensch will noch Pelz? Von wegen! Der Parka-mit-Fellkragen-Trend lässt das Geschäft boomen. Wer Echtfell kauft? Pelzfans, Ahnungslose und alle, denen es egal ist."
    * Zahlen: "Zwischen den Jahren 2005 und 2015 haben sich die Umsätze der europäischen Pelzbranche fast verdoppelt"
        * "Knapp sieben Milliarden US-Dollar setzte sie nach Angaben des europäischen Pelzverbands Fur Europe 2015 durch den Pelzverkauf um. 2005 waren es noch 3,6 Milliarden."
    * Zahlen: "Die überwiegende Mehrheit von Pelz (geschätzte 85 Prozent im Jahr 2014) stammt von Tieren, die in – legalen wie illegalen – Farmen gezüchtet wurden."
    * Zahlen: "Weltweit wurden nach Schätzungen von Fur Europefür Pelzprodukte 2017 knapp 63,1 Millionen Nerze, 12,7 Millionen Füchse und 167.000 Marderhunde getötet. Sie machen den größten Teil der Pelztiere aus."
    * Zahlen: Woher kommt der Pelz?
        * "In der deutschen Pelzbranche, das heißt im handwerklichen Kürschnerbetrieb, setzen sich die verwendeten Felle ungefähr wie folgt zusammen:"
        * Zucht und Farmhaltung: ca. 47 %
        * Fleischproduktion: ca. 38 %
        * Schädlingsbekämpfung: ca. 15 %
        * Jagd: 0,2 %
        * "Wegen ihres Fells gezüchtet werden unter anderem Nerze, Füchse, Nutrias, Marderhunde, Iltisse und Chinchillas. Aus der Schlachtung stammen Lamm- und Ziegenfelle, Persianer, Kaninchen und Kalbsfelle. Als Schädlinge gelten Bisame, Waschbären, Kojoten, Opossums, Nutrias, Rotfüchse, Wildkaninchen, Hamster und Wiesel. Gejagt werden beispielsweise Biber, Eichhörnchen, Rotluchse und Zobel."
        * Quelle: Deutsches Pelz Institut e.V.
    * Woher importiert Deutschland Pelzprodukte?
        * ![](zeit-graphik-pelz-import-2018.png)
            * Das meiste aus China
    * "Das Label Origin Assured (OA) ist bisher eher der Versuch, Vertrauen zu schaffen."
    * Liste mit allen Unternehmen ohne spezielles Fell: https://furfreeretailer.com/
        * HINWEIS: "Fell oder Leder von Nutztieren betrifft das nicht. Hier geht es nur um Tiere, wie Nerze, die wegen ihres Pelzes gezüchtet oder etwa Biber, die deswegen mit Fallen gejagt werden."
        * "Produkte, die im Internet angeboten werden, müssen bislang überhaupt nicht gekennzeichnet werden, was ihren Echtfellanteil angeht. Hier ist lediglich geregelt, dass keine illegal gejagten, geschmuggelten und unter Artenschutz stehenden Tierarten gehandelt werden dürfen."
    * Wie sterben die Tiere?
        * "Bei Füchsen werden dafür Elektroden in den Mund und das Rektum eingeführt"
            * "Diese Methode ist erlaubt und gewährleistet, dass das Fell unverletzt bleibt."
* Artikel: ["Kunstfell - Die Pelzfrage"](http://www.zeit.de/zeit-magazin/mode-design/2017-10/kunstpelz-gucci-echtpelz-china), 2017
    * "Immer mehr Modeunternehmen ringen sich durch, kein echtes Fell mehr zu verarbeiten. Leider kann das trotzdem falsch sein: Der Kunstpelzbesatz stammt oft doch vom Tier."
* ["Überall Jacken mit echtem Pelz"](http://www.20min.ch/schweiz/news/story/-berall-Jacken-mit-echtem-Pelz-15355741), 2017
    * "Kapuzen mit echtem Pelz liegen vor allem bei Jungen im Trend."
    * "Fell bei lebendigem Leib abgezogen"

### Chemikalien im Pelz
* siehe z. B. https://furfreeretailer.com/why-fur-free/

### Hintergrund, Personen, Labels
* http://veganemode.info/
* http://www.peta.de/themen/Pelz, http://pelz.peta.de/
* [Kein-Pelz-Label](http://www.peta.de/petaapprovedvegan)
* ["Echtpelz oder Kunsthaar? PETA-Experte gibt Verbrauchern Tipps zur Unterscheidung beim Kleidungseinkauf"](https://www.peta.de/echtpelz-oder-kunsthaar-peta-experte-gibt-verbrauchern-tipps-zur-unterscheidung), 2015
    * Tipp: Pelz-Optik aus Modegründen vermeiden
    * "Einige große Modeketten setzen aus Tierschutzgründen bereits ausschließlich auf Kunstpelz"
    * siehe auch Video: ["Pelz sieht scheiße aus" - Kai Schumann für PETA](https://www.youtube.com/watch?v=jCTCSRLUdnI), 4 min, 2014
        * "Wir tun so als haben Tiere keine Seele", "Tiere denken und fühlen", sogar Fische, Bilder von Pelzfarmen
        * [Interview 2017](https://istdasvegan.eu/2017/02/kai-schumann-im-interview-eigentlich-muessten-sich-die-fleischesser-rechtfertigen/) vom Blog ["Ist das vegan oder kann das weg?"](https://istdasvegan.eu/uebermich/)
            * „Eigentlich müssten sich die Fleischesser rechtfertigen!“

### Beispiele von Unternehmen, die aktiv Pelz verkaufen
* https://www.fellkapuze.de/
    * "Aus der Modewelt sind Fellkapuzen schon lange nicht mehr weg zu denken! In Paris, Mailand und London sind Fellkapuzen auf jeder Modenschau ein echter Blickfang"
        * Es geht nur um Mode, nicht um Funktion
    * "Verschönern Sie Ihre Jacke"
        * "Wählen Sie aus verschiedenen Fellen Ihren gewünschten Fellstreifen für Ihre Fellkapuze aus, und gönnen Sie sich einen Hauch von Luxus. Frei nach unserem Unternehmensmotto „luxurize yourself“."
    * Qualitätsgarantie: https://www.fellkapuze.de/garantie/
        * "Wir verarbeiten in unseren Finnraccoon- und Fuchsaccessoires ausschließlich OA™* zertifiziertes skandinavische Felle. ( KEINE polnischen oder chinesischen Felle )"
            * also folgende Tiere: Fuchs und "Finnraccoon"
            * Was ist "Finnraccoon"?
                * siehe https://de.wikipedia.org/wiki/Seefuchsfell
                    * "Im Pelzhandel wird das Fell des Marderhunds schon immer unter vielen Namen gehandelt, nur meist nicht als Marderhundfell."
                    * "Die gebräuchlichen Bezeichnungen sind eigentlich Seefuchs oder Tanuki"
                    * "wegen seines in Teilen waschbärähnlichen Aussehens inzwischen mit den irreführenden Namen Finnraccoon (aus Finnland), Russisch Raccoon oder Chinesisch Raccoon angeboten (englisch raccoon = Waschbär)"
            * Sind skandinavische Felle wirklich so gut?
                * http://vgt.at/filme/fotos/recherchen/skandinavienPelz/index.php, 2003
                    * "In Skandinavien werden 55% aller Pelze weltweit produziert"
                    * "Trotz Drohungen der gewaltbereiten Pelzfarmer und unglaublichen Schikanen der finnischen Behörde (3-tägige Isolationshaft) konnten die Zustände in 80 Pelzfarmen dokumentiert und die Recherche-Ergebnisse nach Österreich gebracht werden."
                    * „Fast alle der zahllosen Pelztiere, denen ich auf meiner Skandinavien-Tour in die Augen geschaut habe, werden jetzt gerade mit der Metallzange aus den Käfigen geholt und mit Gas oder Elektroschock getötet, nachdem sie ihr kurzes Leben in einem winzigen Drahtgitterkäfig verbringen mussten. Wer das, wie ich, mit eigenen Augen gesehen hat, kann nie wieder Pelz kaufen! Egal, ob der Pelz aus Asien, Osteuropa oder Skandinavien stammt.“
    * https://www.fellkapuze.de/faq/
        * "Origin AssuredTM (aus gesicherter Herkunft) wird ein freiwilliges Transparenzpropgramm der Pelzbranche genannt, um verantwortungsvoll hergestellte Pelzprodukte für den Kunden zu kennzeichnen."
            * "Es garantiert Ihnen also ein Produkt mit höchsten ethischen Grundsätzen."
                * Diese Aussage ist FALSCH, siehe z. B. ARD-Doku. Eine Garantie ist nicht möglich.
        * "Um welche Art von Pelz handelt es sich bei diesen Fellkapuzen?"
            * "Finnraccoon-, Waschbär- oder Koyotefelle"

### Positive Entwicklungen - Norwegen
* ["Norwegen schafft alle Pelzfarmen ab"](http://www.faz.net/aktuell/gesellschaft/tiere/erfolg-fuer-tierschutzaktivisten-norwegen-schafft-pelzfarmen-ab-15400020.html), 2018
    * "Etwa 300 Pelzfarmen gibt es in Norwegen – jedes Jahr werden dort mehr als 800.000 Tiere getötet. Das soll aufhören, hat die neue Regierung in Oslo nun beschlossen. Aber noch nicht sofort."
    * "Bis 2025 sollen die Farmen stillgelegt werden"
        * Bis dahin ist der Konsument gefordert, keine Pelzmode zu kaufen.
    * "Erst vor wenigen Monaten fanden Tierrechtler auf finnischen Pelzfarmen völlig überzüchtete Silberfüchse, unter anderem einen „Monsterfuchs“ von 20 Kilogramm. In der freien Natur wiegen Silberfüchse etwa 3,5 Kilogramm."
