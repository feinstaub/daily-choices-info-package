Kleidung / daily-choices-info-package
=====================================

[zurück](../../..)

<!-- toc -->

- [Fair](#fair)
- [Bio / Warum Bio-Baumwolle wichtig ist](#bio--warum-bio-baumwolle-wichtig-ist)
- [Tierprodukte](#tierprodukte)
  * [Pelz](#pelz)
  * [Daunen](#daunen)
  * [Leder](#leder)
- [Billig - Mode - Müll](#billig---mode---mull)
  * [Schnelllebigkeit](#schnelllebigkeit)
  * [Film: The True Cost](#film-the-true-cost)
  * [Outlet-Verbrauchertäuschung](#outlet-verbrauchertauschung)
- [Positive Beispiele](#positive-beispiele)
- [Andere Beispiele](#andere-beispiele)

<!-- tocstop -->

Fair
----
* Basis-Infos: http://www.fairtradekleidung.org/ (inkl. Infos zu Bezugsquellen)
* Vertrauenswürdige Siegel: http://www.fairtradekleidung.org/fair-trade-kleidung-siegel
* Positive Beispiele: HessNatur, maas Natur, ..., http://www.miwai.de/
* Negativ-Beispiele: [ARD-Reportage zu kik](https://www.youtube.com/watch?v=NPoHGr6uiq0), 2011, 29 min

Bio / Warum Bio-Baumwolle wichtig ist
-------------------------------------
* Bio ist gerade im Hinblick auf die Dritte-Welt-Hersteller-Länder wichtig, denn "Im Gegensatz zum konventionellen Baumwollanbau ist der Einsatz von chemischen Pestiziden und Düngemitteln verboten." (http://www.umweltinstitut.org/fragen-und-antworten/bekleidung/bio-baumwolle.html)
    * Ohne Bio: Da es in den armen Ländern einen wesentlich schlechteren Arbeitsschutz gibt, werden die Menschen massiv durch die Chemikalien geschädigt.
    * Also zweimal überlegen, ob es das Xte Billig-Shirt wert ist, dass andere Menschen leiden müssen.
* ["Biobaumwolle zu Discounter-Preisen – mehr Schein als Sein?"](https://utopia.de/ratgeber/biobaumwolle-discounter-preise/), 2015

Tierprodukte
------------
### Pelz
* siehe [pelz](pelz.md)

### Daunen
* siehe [Daunen](tiere-artgerecht.md)

### Leder
* siehe Earthlings, insbesondere aus dem Ausland
* siehe "Indien: "Heilige" Kühe?" (https://www.animalequality.de/neuigkeiten/verehrt-und-gequaelt-kuehe-in-indien)
    * viel Leder bei uns kommt aus Indien; siehe schlimmen Zustände dort
    * bei Lederprodukten daher genau nachfragen
* todo

Billig - Mode - Müll
--------------------
### Schnelllebigkeit
* ["Schnelllebigkeit der Mode wird zum Müllproblem"](https://www.ardmediathek.de/ard/player/Y3JpZDovL25kci5kZS9jNDQwNjc5OS1mNDZhLTQxZWYtOGU4Yy03MTZjNWQyZDY0NmU/), 08.03.2019 DAS! ∙ NDR Fernsehen, 4 min
* Erster Schritt: weniger kurzfristige Mode kaufen / weniger ist mehr
* siehe auch: https://www.ardmediathek.de/ard/player/Y3JpZDovL3N3ci5kZS9hZXgvbzEwODc3NTU/ (elsewhere)
    * Billig und schlecht produziert => schlecht weiterverwendbar
    * Zitate von Kunden: Ich kauf billiger, aber dafür mehr. Wenn was billig ist, dann nehm ich es mit; später kommt's halt ggf. weg.
    * Lange verwenden und wenig neues kaufen, ist am besten.

### Film: The True Cost
* Doku: "The True Cost - Der Preis der Mode (2015)" - 7,7

### Outlet-Verbrauchertäuschung
* "DIE OUTLET-LÜGE - Outlet-Ware auf dem Prüfstand (Doku WDR 05.07.2017) HD", 45 min
    * unlauterer Wettbewerb, wenn auf dem Preisschild ein "Original-Preis" steht, wo die Ware aber nicht angeboten wurde
    * teilweise schlechtere Qualität
    * Ausverkauf der Innenstadt-Läden
    * Wie man bei Weben Geld sparen kann (weniger Material, Geschwindigkeit)
* siehe auch https://de.wikipedia.org/wiki/Outlet

Positive Beispiele
------------------
* siehe obige Listen
* Worauf man als Hersteller alles achten kann und auch praktisch durchführbar ist, sieht man hier:
    * https://www.armedangels.de/faq/ - Nachhaltigkeit
        * seit 2007, Köln
    * Recolution, http://www.recolution.de/recoworld/about/
        * vegan, GOTS
    * https://de.wikipedia.org/wiki/Patagonia_(Unternehmen)
        * http://eu.patagonia.com/deDE/home
        * Aussagen: https://de.wikipedia.org/wiki/Patagonia_(Unternehmen)#.C3.96kologische_Verantwortung
* Weiteres
    * [Web-Suche "Nachhaltige Kleidung"](https://www.google.de/search?q=nachhaltige+kleidung)
    * [Fairtrade und Fairer Handel](https://news.utopia.de/ratgeber/fair-trade-fairer-handel-fragen-antworten/) (2015) (Websuche "warum fair trade")
    * http://www.fairwear.de
    * https://www.fairmondo.de

Andere Beispiele
----------------
- Zara
    - https://de.wikipedia.org/wiki/Zara_(Unternehmen)#Kritik
        - http://www.zukunft-braucht-erinnerung.de/judensterne
    - vom Tellerwäscher-zum-Millionär-Geschichte
