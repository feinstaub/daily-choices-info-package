Private Finanzen / daily-choices-info-package
=============================================

[zurück](../../..)

<!-- toc -->

- [Allgemeines](#allgemeines)
  * [Finanzwende](#finanzwende)
  * [finanzwesir - Blog zu privaten Finanzen](#finanzwesir---blog-zu-privaten-finanzen)
- [Hintergründe, filmisch](#hintergrunde-filmisch)
  * [Allgemein](#allgemein)
  * [Finanzkrise 2007](#finanzkrise-2007)
- [Hintergründe](#hintergrunde)
- [Positive Beispiele](#positive-beispiele)
  * [Beispiel: GLS Bank](#beispiel-gls-bank)
- [Nachhaltigkeit](#nachhaltigkeit)
  * [Harald Lesch, Technische Hochschule Köln 2019, Vortrag Zeitalter des Kapitalozän](#harald-lesch-technische-hochschule-koln-2019-vortrag-zeitalter-des-kapitalozan)
  * [Kapitalozän, Capitalocene - Lesch, 2018](#kapitalozan-capitalocene---lesch-2018)
  * [Harald Lesch fordert Konsequenzen aus Erkenntnissen, 2016, inkl. Beispiel Landwirtschaft](#harald-lesch-fordert-konsequenzen-aus-erkenntnissen-2016-inkl-beispiel-landwirtschaft)
- [Hintergründe, humoristisch](#hintergrunde-humoristisch)
- [Weiteres](#weiteres)
  * [Hinter den Kulissen von Vertrieb/Marketing/Verkauf](#hinter-den-kulissen-von-vertriebmarketingverkauf)
  * [Armut](#armut)
  * [Arm und Reich](#arm-und-reich)
  * [Bitcoin](#bitcoin)
- [Handlungsoptionen / Geldanlage-Atlas](#handlungsoptionen--geldanlage-atlas)
  * [Was ist mit Investmentfonds?](#was-ist-mit-investmentfonds)
  * ["Bankwechsel jetzt"](#bankwechsel-jetzt)
  * [Banken mit besonders zukunftstauglichen Anlagekriterien](#banken-mit-besonders-zukunftstauglichen-anlagekriterien)
  * [divest - gofossilfree](#divest---gofossilfree)
  * [Lokale Energiegenossenschaften](#lokale-energiegenossenschaften)
  * [Oikocredit](#oikocredit)
  * [Spenden](#spenden)
- [Inbox](#inbox)
  * [2020 - Natur, Wert und Geld](#2020---natur-wert-und-geld)
  * [2020 - Wesen des Geldes](#2020---wesen-des-geldes)
  * [2020 - Olivier Bossard](#2020---olivier-bossard)
  * [2019 - Vertrauensbruch bei Sparkassen](#2019---vertrauensbruch-bei-sparkassen)
  * [2019 - Flowtex-Skandal](#2019---flowtex-skandal)
  * [2019 - HSBC](#2019---hsbc)

<!-- tocstop -->

Wer konsumieren will, braucht Geld.

Geld ist ein Gestaltungsinstrument.

Allgemeines
-----------
### Finanzwende
* https://www.finanzwende.de/ueber-uns/unsere-ziele/

### finanzwesir - Blog zu privaten Finanzen
* https://www.finanzwesir.com/neu-hier
    * Blog zum Thema private Finanzen
    * Link zum Businesskasper
    * Bodenständige Finanz-Philosophie
        * https://www.finanzwesir.com/finanzielles-glaubensbekenntnis
* Eigene Arbeitskraft wichtiger als alles andere: https://www.finanzwesir.com/blog/humankapital-passives-einkommen-selbstaendigkeit
* Coaching-Tipps: https://www.finanzwesir.com/blog/wie-bin-ich-finanziell-gut-aufgestellet
* Haus kaufen, ja oder nein?
    * https://www.finanzwesir.com/blog/eigenes-haus-immobilie
    * https://www.finanzwesir.com/haus-kaufen
    * http://www.finanztip.de/fileadmin/images/Attachments/PDF/Kredit/Finanztip-E-Book-Immobilien-Mieten-oder-kaufen.pdf (23 Seiten)
* Begriffe: https://www.finanzwesir.com/was-ist
* Bewertung Riester: https://www.finanzwesir.com/blog/was-spricht-gegen-riesterrente

Hintergründe, filmisch
----------------------
### Allgemein
* Film: [Let's make money](https://de.wikipedia.org/wiki/Let%E2%80%99s_Make_Money), 2008
    * ...
    * Geld, das in Blasenzeiten in leerstehende Hoteanlagen investiert wurde
    * ...
    * Steuerhinterziehung / Steuerparadiese
    * ...

* Ponzi-Schema
    * Madoff
        * Chasing Madoff (2010)
        * The Wizard of Lies (2017)

### Finanzkrise 2007
* Filme die über **Finanzkrise ab 2007**:
    * Spielfilm: [Der große Crash – Margin Call](https://de.wikipedia.org/wiki/Der_gro%C3%9Fe_Crash_%E2%80%93_Margin_Call), 2011
    * Spielfilm: [The Big Short](https://de.wikipedia.org/wiki/The_Big_Short_%28Film%29), 2015
    * Doku: [Inside Job](https://de.wikipedia.org/wiki/Inside_Job), 2010
    * https://de.wikipedia.org/wiki/Finanzkrise_ab_2007#Dokumentationen_und_Spielfilme
    * Weitere Filme:
        * Spielfilm: [The Wolf of Wall Street (2013)](https://www.themoviedb.org/movie/106646-the-wolf-of-wall-street)
        * Doku: [Master Of The Universe (2013)](https://www.bpb.de/mediathek/225092/master-of-the-universe), Ein Dokumentarfilm von Marc Bauder, 1:30 h, Bundeszentrale für politische Bildung
            * "In einem verlassenen Bankengebäude in Frankfurt am Main schildert ein ehemaliger Investmentbanker seine Sicht auf den Finanzmarkt von heute. Der Dokumentarfilm "Master Of The Universe" gibt Einblicke in die Gesetzmäßigkeiten einer Parallelwelt, deren Geschäfte globale Krisen zur Folge haben können."
            * "Der Privatanleger hat an der Börse im Durchschnitt immer das Nachsehen"
            * "Du kommst da rein an deinen Arbeitsplatz, du weißt, was der Tag bringt und es wird für dich gesorgt"
            * "Man entfernt sich von der Welt da draußen", "Abkopplung von gesellschaftlichen Prozessen"
            * inkl. Filmheft für die Schule
* Thema **private Altersvorsorge**:
    * Doku: ["Der Drückerkönig und die Politik"](http://daserste.ndr.de/panorama/aktuell/Der-Drueckerkoenig-und-die-Politik,awd151.html), 30 min, 2011, ndr.de
        * interessant, welche Personen da befreundet sind
        * Infos über Riester-Erfinder, Wirtschaftsweisen
        * Vertriebsmethoden für riskante Finanzprodukte
        * MB = Monatsbeiträge, z. B. 1 Jahr der Monatsbeiträge geht an den Abschlussvertreter
* Thema **private Krankenversicherungen** bzw. Versicherungsvertrieb allgemein:
    * Buch: Bernd Hontschik: "Erkranken schadet Ihrer Gesundheit", 2019
        * https://www.westendverlag.de/buch/erkranken-schadet-ihrer-gesundheit
        * "Wie Konzerne Medizin und Gesundheitswesen übernehmen - Es tut sich was in unserem Gesundheitswesen, schon lange, in kleinen, unmerklichen Schritten und immer in die gleiche beunruhigende Richtung: Es ist die Verwandlung der Humanmedizin in einen profitorientierten Industriezweig – auf Kosten der Patienten und des Allgemeinwohls. In das Gesundheitswesen hat unsere Gesellschaft bislang einen Teil ihres Reichtums investiert, zum Wohle aller. Nun wird das Gesundheitswesen zur Quelle neuen Reichtums für Investoren. Die neuen Ziele werden nicht innerhalb der Medizin erarbeitet, sondern werden in Konzernen geplant und von Politikern in die Tat umgesetzt. Die Medizin wird dabei zu einer Ware, die nur noch als Quelle von Profit interessant ist. Mit seiner 40jährigen Berufserfahrung als Chirurg, immer begleitet von wissenschaftlicher und publizistischer Tätigkeit, gelingt Bernd Hontschik ein spannender, manchmal erschütternder Blick auf Medizin und Gesundheitswesen. Jedes Kapitel dieses Buches ist ein flammender Appell aus immer neuen Blickwinkeln, zur eigentlichen Bestimmung der Medizin zurückzukehren."
        * Bernd Hontschik:
            * "Was wäre ihr Rezept, um das Gesundheitssystem zu verbessern" - "Das wichtigste wäre, die private Krankenversicherung abzuschaffen"
    * Doku: [Über Aufstieg und Pleite der MEG AG](https://www.youtube.com/watch?v=ppD4RqvGLlY), 45 min, 2012, hr-fernsehen
        * Datenquelle: Spam-Mails über kostenlose Vergleiche privater Krankenversicherungen
        * Provisionen (aus den Beiträgen der Versicherten)
        * alle (Versicherungskonzerne) haben was vom Kuchen abbekommen; peinliche Lobhuldigungen
        * verstehen wie Versicherungsverkäufer motiviert werden
        * Führungsstil und Mitarbeitermotivation
        * Super-Vertriebler
        * TODO: Pleite warum?
    * Verwandte Themen:
        * siehe [pharma.md](pharma.md), Buch "Patient im Visier"
* Thema **Online-Konsum** (ohne Beachtung von Nachhaltigkeit):
    * Doku: ["Die große Samwer-Show - Die Milliardengeschäfte der Zalando-Boys"](https://www.zdf.de/politik/frontal-21/dokumentation-die-grosse-samwer-show-100.html), 45 min, frontal21, 2014
        * weitere Stichworte: Rocket Internet, HelloFresh, Delivery Hero/Foodpanda/Lieferheld/pizza.de/Foodora
* Thema Nahrung/Armut:
    * Video: [Profitgier - Ein Kurzfilm aus der Reihe "Warum Armut?"](http://www.bpb.de/mediathek/203981/profitgier), 5 min, 2012, Bundeszentrale für politische Bildung, Nahrungsmittelspekulation von zwei Seiten betrachtet
        * "Nahrungsmittelspekulation ist ein lukratives Geschäft. Jedoch treibt sie die Preise für Lebensmittel so stark in die Höhe, dass sie für viele Menschen zu teuer werden. Dieser Kurzfilm lässt beide Seiten zu Wort kommen."
    * Reichtum:
        * Jedes Jahr eine neue Meldung dieser Art "Reiche werden immer reicher", "Vermögen der Superreichen erreicht Rekord":
            * 2018: "Milliardärsstudie Vermögen der Superreichen erreicht Rekord - Der Club der Milliadäre weltweit wächst und wächst. Das Vermögen der reichsten Menschen ist 2017 auf einen Rekordwert geklettert - so eine Studie der Beratungsgesellschaft PwC und der Schweizer Bank UBS." (https://www.tagesschau.de/wirtschaft/milliadaere-rekord-103.html), 2018
                * Botschaft: auch du kannst es schaffen: "Die Studie sieht einen steigenden Trend zu Milliardären, die es mit einer guten Geschäftsidee aus eigener Kraft in den Club der Superreichen geschafft haben: Von den 332 Aufsteigern im Jahr 2017 waren 199 sogenannte Selfmade-Milliardäre."
                * Frage an die Politik: Ist das gut, ...?
            * 2018: ["Einkommen in Deutschland: Die Reichen werden immer reicher"](https://www.handelsblatt.com/politik/deutschland/einkommen-in-deutschland-die-reichen-werden-immer-reicher/20852252.html)
                * "Seit der Wiedervereinigung steigern die oberen zehn Prozent ihren Anteil am Volkseinkommen stetig, zeigt eine DIW-Studie. Immer weniger vom Wohlstand bekommt die ärmere Bevölkerungshälfte."
                * seit ca. 2000: "Der neue Niedriglohnsektor führte dazu, dass die ärmste Hälfte der Bevölkerung einen geringeren Anteil am Volkseinkommen hat als davor."
                * Empfehlungen:
                    * "Im Weltreport empfehlen die Forscher, gegen das Auseinanderdriften von Arm und Reich ein stark progressives Steuersystem,
                        * also die Umkehr jener Steuerreformen, die seit den 1980er-Jahren in Westeuropa und den USA zu niedrigeren Spitzensteuersätzen geführt haben.
                    * Für Deutschland verlangt Bartels zusätzlich, dass untere Einkommensgruppen an der Unternehmensrendite stärker teilhaben müssten."

Hintergründe
------------
* [Divestment - gofossilfree](http://gofossilfree.org/de/)
* Artikel [Steuertricks internationaler Konzerne](http://www.attac.de/index.php?id=394&no_cache=1&tx_ttnews[tt_news]=8580), attac.de, 16.02.2016
* siehe auch [Monopole und Oligopole](../Hintergrund/oligopole.md)
* siehe auch [Wachstumsproblematik](../Hintergrund/wachstum.md)

Positive Beispiele
------------------
### Beispiel: GLS Bank

[GLS Bank](https://www.gls.de)

* [strenge Ausschlusskriterien bei Nachhaltigkeitsfragen](https://www.gls.de/privatkunden/ueber-die-gls-bank/arbeitsweisen/anlage-und-finanzierungsgrundsaetze/)
* seit [1974](https://de.wikipedia.org/wiki/GLS_Gemeinschaftsbank)
* [Mitglied](http://www.gabv.org/the-community/members/banks) von [Global Alliance for Banking on Values](https://www.gabv.org/)
    * "an independent network of banks using finance to deliver sustainable economic, social and environmental development."
    * [Wikipedia](https://de.wikipedia.org/wiki/Global_Alliance_for_Banking_on_Values)
* 2019:
    * Zukunftsstiftung Landwirtschaft
        * ... auch viel Milchwirtschaft...
    * "GLS Bank schließt sich Aufruf zum globalen Streik für das Klima an"
* 2018: Blog
    * Ernährung
        * https://blog.gls.de/bankspiegel/lasst-uns-ueber-tiere-sprechen/
        * https://blog.gls.de/der-wirtschaftsteil/ernaehrung-wirtschaftsteil-299/
            * siehe auch kantine.md
* 2017: [unterstützt und fordert](https://www.gls.de/privatkunden/ueber-die-gls-bank/presse/pressemitteilungen/gls-bank-fordert-nachhaltigen-wandel/) die "die 17 Entwicklungsziele der Agenda 2030 der Vereinten Nationen (Sustainable Development Goals) und die Verpflichtung zum Pariser Klimaabkommen"
    * Forderungen von Maßnahmen:
        * "1. Arbeit entlasten, Kapital belasten",
        * "2. Ein bedingungsloses Grundeinkommen",
        * "3. Ausnahmslose Abgabe auf CO2-Ausstoß",
        * "4. Konsequente Abgabe auf Spritz- und Düngemittel"
    * Was sind die Entwicklungsziele, die Sustainable Development Goals (SDG) der UN?
        * [Neue UN-Entwicklungsziele verständlich erklärt von unicef](https://www.unicef.de/informieren/ueber-uns/unicef-international/neue-entwicklungsziele/entwicklungsziele-verstaendlich-erklaert)
            * Bei Ziel "8. Gute Arbeitsplätze und Wirtschaftliches Wachstum" ist diskussionswürdig, was mit wirtschaftlichem Wachstum gemeint ist
            * ![](https://www.unicef.de/blob/83200/fe7dce54da44ae9570566ab174d1116e/nachhaltige-entwicklungsziele-data.jpg)
            * "Mitmachen!
                * Jeder einzelne muss bei sich selbst anfangen und sein Umwelt- und Konsumverhalten überdenken. Weniger Auto fahren, auf eine Flugreise verzichten, Kleidung im Second Hand kaufen - Ihnen fällt dazu bestimmt eine Menge ein.
                * Weitersagen! Bitte helfen Sie mit, die neue Welt-Agenda bekannt zu machen und erzählen Sie Ihren Freunden davon oder teilen Sie diese Seite in Ihren sozialen Netzwerken.
                * Helfen! - Jeder Beitrag - egal wie klein oder groß - hilft, das Leben von Kindern zu verbessern und die Erde zu einem gerechteren Ort zu machen."
* [Jahreshauptversammlung 2017](https://www.gls.de/privatkunden/aktuelles/neuigkeiten/jahresversammlung-2017/)

* Sammlung von positiven Beispielen im Bank-Spiegel
    * https://www.gls.de/privatkunden/bankspiegel/
    * online blättern
    * z. B. Ausgabe 2 / 2017
        * "Billig können wir uns nicht mehr leisten" / "Zu Gast beim Öko-Pionier Sonett"

Nachhaltigkeit
--------------
### Harald Lesch, Technische Hochschule Köln 2019, Vortrag Zeitalter des Kapitalozän
* Video: ["Prof. Dr. Harald Lesch an der TH Köln 2019 | Vortrag Zeitalter des Kapitalozän"](https://www.youtube.com/watch?v=BlXgHcd7tok), 2019, 1h 45 min
    * Werbung: "gebaut, um den Atem zu rauben"
    * an der Hochschule: Ingenieure
        * ein Leben lang damit beschäftigen, wie man den Außenspiegel in ein Schallplattenspieler zu verwandeln
            * ist das sinnvoll?
    * ...
    * 12 min: The Great Acceleration
        * Begriffe: Umwelt als Kulisse
    * ...
    * ...
    * 39 min: Der Ozean sammelt die Wärme
        * 39:50: Bild: "Das Handicap"
    * 40:20: Baumsterben
        * Klimawandel
        * Borkenkäfer
        * Abholzung
    * ...
    * ...
    * TODO
    * ...
    * ...
    * Frage: Mensch egoistisch?
        * ja, daher muessen Anreize gesetzt werden
    * 1:10:00
        * Es wird dann was passieren, wenn es Ereignisse gibt, die es notwendig erscheinen lassen.
        * "Ueberschaubare Katastrophe" notwendig
        * wie im Privaten: wenn der Leidensdruck da ist, gehen wir in Veraenderungsprozesse
        * "aber zugleich muessen die Optionen auch da sein!"
        * "Daher flehe ich Sie an"
            * "Arbeiten Sie da mit, wo es um Transformationsprozesse geht; wo es um Dinge geht, die wirklich wichtig und richtig gut sind"
            * "Kuemmern Sie sich um erneuerbare Energien wo immer Sie koennen"
            * Wachstum von Technologie kann nicht durch Wachstum von Technologie behoben werden? (haeh?)
            * Da mitmachen, wo es heisst, wir probieren was aus (wo andere sagen, das klappt ja nie)
                * was neues wagen
        * Vielfalt an Loesungen in den Mainstream bringen
            * (-> die Welt ist voller Loesungen)
    * 1:11:30
        * ...
    * 1:14:40
        * Beispiel Deutsche Bundesbahn als AG
        * Schienen abbauen und dann weiter Straßen ausbauen
        * was man braucht, ist Kollektivmobilität
    * 1:14:45
        * "Tun Sie sich zusammen"; nicht alleine
        * "Ritualisieren Sie Ihre Zeit"
        * "Machen Sie regelmäßige Treffen mit Menschen, die mit Ihnen ein gemeinsames Ziel haben"
        * "Machen Sie aus dieser Transformation etwas, was Ihnen persönliche was bringt"
        * Inhaltlich vielleicht düster, aber von der Tätigkeit "Ich hab mich in meinem ganzen Leben als Wissenschaftler noch nie so wohl gefühlt
            wie seit dem Moment seit dem ich mich über relevante Themen zu unterhalten und nicht nur über uninteressante."
        * Wunsch, dass Leute aus Hochschulen aufstehen und sich an öffentlichen Diskussionen beteiligen;
            vor allem dann, wenn "diese Existenzen" auftauchen, die versuchen mit ihren Fake-Thesen in öffentliche Debatten einzudringen
            * "wir lassen uns nix erzählen"; kritische Hinterfragenskultur
    * 1:18:00
        * Kernenergie?
            * zu teuer (Endlager etc.)
            * Energie sollte in staatliche Hand, weil wir als Bürger sowieso alles bezahlen müssen
                * Gewinn sollte öffentlich reinvestiert werden
    * ca. 1:18:00
        * Fake-News vs. Bevölkerung?
        * Lesch hört Deutschlandfunk (außerdem die großen Zeitungen in Deutschland, wegen Meinungsbreite)
            * nutzt öffentlich-rechtliche Anstalten, weil
                * sie dem öffentlichen Interesse näher stehen als Privatunternehmen
            * Privatunternehmen: müssen GRUNDSÄTZLICH irgendwo eine Finanzierung herholen
                * entweder gesponsort oder durch Werbung
                * Beispiel Facebook: viele Menschen vernetzen und durch KI lange am Bildschirm halten
                    * das geht am besten mit Nachrichten zu Angst, Hass und Häme => Fake-News, Lügen, Bots
        * https://www.scinexx.de/
            * finanziert von der gemeinnützigen https://de.wikipedia.org/wiki/Klaus_Tschira_Stiftung
            * Zugang zu wissenschaftlichen Original-Arbeiten
            * https://www.scinexx.de/news/biowissen/gefaehrden-neonicotinoide-auch-fische/
                * Pestizide
                * "Schon 1962 warnte die Biologin Rachel Carson in ihrem Werk „Silent Spring“ vor „…Chemikalien, die das Potenzial haben, die Vögel und das Springen der Fische in den Bächen zum Schweigen zu bringen“."
            * https://www.scinexx.de/businessnews/umweltschutz-darum-ist-es-wichtig-verantwortung-zu-uebernehmen/
                * ...gute Punkte...
        * Zeit, Achtsamkeit
        * Neoliberaler Mensch besteht aus 3 Teilen: me, myself and I
            * dieses Modell hat sich überholt
    * ca. 1:26:00
        * Gabe der Hochschule und Verantwortung der Wissenden => Engagieren!
            * Fake-News, Alternative Wahrheiten etc. sind keine Petitessen. Das ist wichtig.
    * ca. 1:28:00
        * Ernährung?
        * auf Europa wird geschaut; wir können was tun
        * etwas blabla, aber jeder kann was tun; "es wird über vegetarisch/vegan diskutiert" soziale Veränderungen über die Generationen;
            * Festivitäten; jedes Hochschulevent, jede Institution; wie machen wir unsere 100-Jahr-Feier?
                * bieten wir wieder Frikadellen oder Curry-Wurst an? Oder was anderes?
            * ! wichtig: "Die Vorschläge müssen gemacht werden.
                Dass Sie nicht still sind.
                Sondern dass Sie die ganze Zeit immer dabei sind"
                * "Möglichkeitsdruck nennt man das.
                    Also immer wieder eine Möglichkeit anbieten und eine Gruppe wird darüber entscheiden."
                    * "vielleicht nicht beim ersten Mal, aber vielleicht beim siebten Mal. Diese politischen Prozesse dauern lange."
                * (Abwehrreaktion? -> Was wir hier anbieten, sind Möglichkeiten; Sie sind frei diese anzunehmen, oder auch nicht)
            * Grundgesetz wichtig; vielleicht mal von der Fleischessernation zu Pflanzenfutterern; wer weiß
    * 1:31:00
        * Alternative zur E-Mobilität?
        * Akku schwer
            * möglichst nicht mit großen Batterien arbeiten
        * pro Wasserstoff
        * Power-2-Gas
        * Lithium-Rohstoff-Problem
        * Problem: gleichzeitig aufladen
        * Autonomes Fahren: "einen fahren lassen"
            * geht übrigens jetzt schon: mit der Bahn
        * vorhandene Tankstellen für Wasserstoff benutzen (vs. E-Ladestationen aufbauen)
            * (todo: wie lange halten eigentlich unsere Kupfervorräte für die dicken E-Kabel?)
    * 1:37:40
        * Anzahl der menschlichen Individuen reduzieren zugunsten der Mitwelt?
            * siehe Buch: "Ilija Trojanow: „Der überflüssige Mensch“, Residenz Verlag, St. Pölten-Salzburg-Wien 2013, 90 Seiten"
                * https://www.deutschlandfunkkultur.de/wuerdelos-im-spaetkapitalismus.950.de.html?dram:article_id=263411
                    * "Soziale Ungerechtigkeit halten viele heute für gottgegeben. Genau dagegen lehnt sich Ilija Trojanow [...] auf.
                    Sein Aufruf zu mehr Empathie ist faktenreich begründet und liefert gut recherchierte Beispiele beängstigender Entwicklungen."
                    * todo
                * https://www.residenzverlag.com/buch/der-uberflussige-mensch
                    * "Wer nichts produziert und nichts konsumiert, ist überflüssig – so die mörderische Logik des Spätkapitalismus."
                    * ...
                    * "In seinen eindringlichen Analysen schlägt er den Bogen von den Verheerungen des Klimawandels über die Erbarmungslosigkeit neoliberaler Arbeitsmarktpolitik bis zu den massenmedialen Apokalypsen, die wir, die scheinbaren Gewinner, mit Begeisterung verfolgen. Doch wir täuschen uns: Es geht auch um uns. Es geht um alles."
            * eigene Verantwortung (nicht auf andere zeigen)
    * 1:42:50
        * Unternehmen
        * Analogie Finanzkrise: plötzlich hat die Politik reagiert und ganz schnell große Gelder lockergemacht
            * was passiert wohl bei einer ähnlich großen Umweltkrise?
        * Es gab auch schon mal ökologischere Zeiten; hohe Benzinpreise
        * Autofreie Feiertage
        * ...

### Kapitalozän, Capitalocene - Lesch, 2018
* Video: [""The Capitalocene - Geological Age of Money" by Prof. Dr. Harald Lesch."](https://www.youtube.com/watch?v=6wLlWWp8Vcg), 2018, 2 h
    * Anthropozän
    * "The lecture was held on 2.12.2018 in the Audimax of the TU Ilmenau and organized by the VDI Studenten und Jungingenieure Ilmenau together with the Studierendenrat der TU Ilmenau"
    * Klimakommunikation
    * ... TODO ...

### Harald Lesch fordert Konsequenzen aus Erkenntnissen, 2016, inkl. Beispiel Landwirtschaft
* https://www.youtube.com/watch?v=0r39TopOe4I
    * Gutes Intro
        * ...
    * Beispiel Landwirtschaft
    * ...
    * Beispiele aus der Landwirtschaft wie sehr wir gegen die Natur arbeiten (z. B. mit Chemie oder Meere leerfischen)
        * Feststellung, dass das alles nur möglich geworden ist, dank wissenschaftlicher Erkenntnisse
        * also Natur so stark zu manipulieren wie wir das heute machen
    * ...
    * "Mit der Natur kann man nicht verhandeln." (auch wenn es den Eindruck macht, wir denken das)
    * Gier nach Geld
        * "Der Elefant in der Küche ist Geld"
    * ...

Hintergründe, humoristisch
--------------------------
* 1637: Tulpenblase: ["Historische Finanzkrisen: Niederlande 1637 Eine Blumenzwiebel für 87.000 Euro"](http://www.faz.net/aktuell/finanzen/fonds-mehr/historische-finanzkrisen-niederlande-1637-eine-blumenzwiebel-fuer-87-000-euro-1283731.html), FAZ 2008
* 2005: ["Scheibenweise - satirischer Informationsfilm zum Thema Geld und Banken"](https://www.youtube.com/watch?v=E6gR-G6kVYU), Juli 2005, [Diplomarbeit](http://www.maxvonbock.de)
    * interessante Sichtweise; teilweise stark vereinfachend (vor allem beim Zins gibt es kein Schwarz-Weiß), aber letzendlich ist die Zielgruppe, die überzeugt werden soll, ja außerirdische Wesen, die einen Planeten übernehmen wollen
    * weiterlesen: https://de.wikipedia.org/wiki/Zinsverbot, https://de.wikipedia.org/wiki/Die_Grenzen_des_Wachstums
* Hypothekenblase und Derivate erklärt: todo
* 2007: Finanzkrise erklärt: https://de.wikipedia.org/wiki/Finanzkrise_ab_2007
* 2011: [American Dream Explained in Animation](https://www.youtube.com/watch?v=mII9NZ8MMVM)
    * (humoristische Umsetzung, gut bis 3 min, danach dreht es ab mit wirren Verschwörungsmythen)
    * (die geschilderte prekäre Lage lässt sich auch einfacher mit der Komplexität des Systems, mangelnder Aufklärung der Akteure und schlechter Politik erklären)

Weiteres
--------
### Hinter den Kulissen von Vertrieb/Marketing/Verkauf
* ["Die Tricks der Banken und Versicherungen"](https://www.youtube.com/watch?v=0nMQ9L9d9n8), NDR-Doku, 45 min, 2016
    * Beispiel einer Rentnerin, die durch Schließung von Bankfilialen (und Geldautomaten) nun viel länger unterwegs ist für ihre Bankgeschäfte (statt 5 min, 1 h)
        * Was ist mit Internet? (kann sie nicht)
    * ...

### Armut
* Was ist Armut?
    * ...
* heise.de
    * [Teil 1: agil](https://www.heise.de/newsticker/meldung/Missing-Link-Agil-leben-im-digitalen-Kapitalismus-Teil-1-Der-neue-alte-Geist-des-Kapitalismus-4133676.html), 2018
    * [Teil 2: zum BGE](https://www.heise.de/newsticker/meldung/Missing-Link-Agil-leben-im-digitalen-Kapitalismus-Teil-2-Mit-dem-Bedingungslosen-Grundeinkommen-den-4141234.html), 2018
        * https://de.wikipedia.org/wiki/Christoph_Butterwegge - "deutscher Politikwissenschaftler und Armutsforscher"
            * Positionen
                * "Zudem würden vormals staatliche Aufgaben wie die Energieversorgung, die Bildung oder der Strafvollzug zunehmend privatisiert und damit warenförmig."
                * "Damit verbunden sei ein Machtverlust des demokratisch legitimierten Staates zugunsten von Einzelinteressen privater Investoren. Deren Entscheidung entziehe sich der Machtkontrolle und gefährde somit die Demokratie in Deutschland, argumentiert Butterwegge."
                * "Als Gegenvorschlag regte Butterwegge unter anderem die Einführung einer
                    * Maschinensteuer
                        * https://de.wikipedia.org/wiki/Wertsch%C3%B6pfungsabgabe
                    * die Erhebung einer Vermögensteuer
                    * das Anheben des Spitzensteuersatzes der Einkommensteuer
                    * sowie eine „soziale Grundsicherung“[30] an, womit er etwas dezidiert anderes als ein bedingungsloses Grundeinkommen meint.[31] Er spricht sich gegen ein solches aus."
    * siehe auch bedingungsloses-grundeinkommen.md
* Zitelmann
    * https://www.nzz.ch/feuilleton/stets-im-angriffsmodus-ld.1344482
    * https://www.epochtimes.de/wissen/rainer-zitelmann-warum-intellektuelle-den-kapitalismus-nicht-moegen-a2493428.html
        * http://www.rainer-zitelmann.de/wp-content/uploads/2018/05/faz-intellektuelle.pdf, 2018
            * "ist promovierter Historiker und Soziologe"
            * "Antikapitalismus ist das am meisten verbreitete und überall praktizierte ideelle Bekenntnis unter Intellektuellen“" (oha)
    * http://www.empfohlene-wirtschaftsbuecher.de/2017/11/medienbloedsinn-zum-thema-uebergewicht/
    * http://www.empfohlene-wirtschaftsbuecher.de/2014/08/armut-ist-diebstahl/
    * https://www.wallstreet-online.de/nachricht/10355827-dr-dr-zitelmann-das-elend-landes-faengt-an-menschen-markt-trauen, 2018
        * "Das Elend eines Landes fängt an, wenn die Menschen dem Markt nicht trauen"
            * siehe user comments
                * ["Die Krise kurz erklärt"](https://www.heise.de/tp/features/Die-Krise-kurz-erklaert-3392493.html?seite=all), 2011
                    * ...
                    * user comments
                        * "Und wieder wird die Ökologie außen vor gelassen"
                            * Artikel: ["Die ökologische Schuldenkrise - Faule Kredite sind derzeit auch beim Umgang mit der Natur die Regel"](https://www.deutschlandfunkkultur.de/die-oekologische-schuldenkrise.1005.de.html?dram:article_id=159360), 2011, von Christian Schwägerl
                                * "Die Finanzkrise zeigt, wie schwer Schäden an komplexen Systemen zu reparieren sind. Doch beim Geld geht es letztlich nur um Papier. Klima und Ökosysteme zu regenerieren, ist ungleich schwieriger."
                                * "Ein Bail-out, ein Übernehmen der heutigen ökologischen Schulden, könnte künftige Generationen schlicht überfordern. Höchste Zeit also, aus der akuten Finanzkrise zu lernen: Wir dürfen nicht weiter in finanzieller und ökologischer Hinsicht heute über unsere Verhältnisse leben und dadurch den Menschen der Zukunft Freiheit und Wohlstand rauben."
            * Muss man also glauben?
            * "Oh ja, ich habe unendliches Vertrauen in die Märkte und die darin agierenden, besonders "erfolgreichen" Unternehmen. Die Finanzmärkte mit Banken, Versicherungen, Investmentfonds, Steuerparadiesen, Hedgefonds"
    * Welche Beiträge gibt es zur Nachhaltigkeitsdiskussion?
        * https://www.theeuropean.de/rainer-zitelmann/12899-liebe-rechte-leserbriefschreiber, 2017
            * "Mich nerven zudem der Ökowahn und die Weltuntergangsszenarien vom Klimawandel", ::ökofaschismus
        * https://www.theeuropean.de/rainer-zitelmann/12541-interview-mit-rainer-zitelmann--2
            * "Obwohl das Wort “Nachhaltigkeit” in aller Munde ist, ich kann es schon lange nicht mehr hören"
            * "Intoleranz gegen Andersdenkende ist in der Tat ein gemeinsames Merkmal bei vielen Linken und Grünen." (oha)
            * Buch über Superreiche
            * siehe user comments
                * "Nur weil Linke etwas anderes Denken, beleidigt Zittelmann diese als "intolerant"."
                    * ..., "Liberale verunglimpfen ebenso andere, speziell mit dem inflationären Gebrauch des Begriffs "Ideologie"."
        * https://www.businessinsider.de/rainer-zitelmann-zu-wenig-kapitalismus-wird-fuer-schlimme-krisen-sorgen-2018-3
            * „Kapitalisten und der Kapitalismus werden zum Sündenbock für alle Probleme auf dieser Welt gemacht: Armut, soziale Ungerechtigkeit, Umweltverschmutzung, Krisen, Kriege oder Krankheiten. Eigentlich gibt es kaum etwas, für das der Kapitalismus nicht verantwortlich gemacht wird. Beschäftigt man sich intensiver mit diesen Themen, dann sieht man, dass der Kapitalismus jedoch tatsächlich viel eher Problemlöser ist als Problemverursacher.“
                * Frage: wer genau wirft ihm das alles vor?
                * kein Hinweis zur Lösung der Allmende-Herausforderung (nachhaltigkeit.md), insbesondere Umweltverschmutzung
    * Zum Vergleich: [Kapitalismus als Religion](https://www.youtube.com/watch?v=YzD1l-qYqnA), Pispers, 2018, 1h 15min
        * "Die Menschen im Westen haben den Glauben an alles verloren; aber sie glauben unbeirrt, dass die kapitalistische Art zu Wirtschaften langfristig für alle Menschen auf der Welt von Vorteil ist.". Die einen dürfen verdienen und die anderen dran glauben. Götter Wachstum und Produktivität. Die Natur dagegen ist verschwenderisch. Konsumkritik.
            * Löwenbeispiel mit Anklängen zur Glücksforschung
            * Erfolgsrezept zur Lösung einiger Probleme: alle Länder der Welt sollen mehr exportieren als sie importieren.
            * 6 min: Nur die Westdeutschen haben ihren Wohlstand dem eigenen Fleiß zu verdanken. ;-)
            * ...
            * Niedriglohnland DDR
                * ["West-Kataloge voller DDR-Produkte"](https://www.mdr.de/zeitreise/quelle-und-ddr-produkte-100.html), 2017
                    * "Unter der Marke "Privileg" verkaufte das westdeutsche Versandhaus Quelle über Jahrzehnte Produkte aus der DDR. Die Käufer wussten davon nichts."
                    * "steht exemplarisch für die wirtschaftliche Verflechtung zwischen westdeutschen Unternehmen und dem Billiglohnland DDR"
                    * "die westdeutschen Kunden nichts ahnten und dessen Ausmaß sich die ostdeutschen Angestellten in den volkseigenen Betrieben und Kombinaten nicht vorstellen konnten"
                    * "Auch Möbel und Kleidung, die über den Quelle-Katalog bestellt werden konnten, kamen aus der DDR."
                    * "Kittelschürzen, Bettwäsche, Anzüge. Für Produkte, die in den Westen exportiert wurden, verwendete man häufig hochwertigeres Material, als für die Ware, die im Inland blieb."
                    * "Vorwurf: [...] soll von Zwangsarbeit in der DDR gewusst haben"
            * "Es gab also früher schon 1-EURO-Jobber. Nur damals hat man die wegen der Mauer nicht sehen müssen."
            * Wenn sich alle anstrengen, dann können alle so leben wie die Westdeutschen. Das geht leider nicht.
            * ...
            * 15 min: Verteilung der weltweiten Rohstoffe
                * Mögliche Lösung: jeweils die derzeit Reichen schotten sich ab (innerhalb der Länder, Europa, Welt)
                * z. B. Festung Europa
                    * Neue Mauer (Griechenland links oder rechts?)
                    * Verteidigung notwendig, gegen die, die rein wollen
            * 16 min: Geldprobleme (Schuldenkrise) leicht lösbar, Resourcenprobleme schwer lösbar
                * Geld als bedrucktes Papier; Geld ist kein Wert, es ist eine Absprache
            * 19: min: Geld entsteht durch Kredivergabe
            * ...
* Was ist Dis­kri­mi­nie­rung?
    * siehe diskriminierung.md

### Arm und Reich
Spaltung, Schere

* 2019
    * "IWF zu deutscher Wirtschaft - Exportboom macht Reiche reicher", https://www.tagesschau.de/wirtschaft/boerse/iwf-bericht-103.html

### Bitcoin
* ["Richard Stallman on good things and bad things about Bitcoin"](https://www.youtube.com/watch?v=RZ_3MKomQzY), 2013, 5 min

Handlungsoptionen / Geldanlage-Atlas
------------------------------------
### Was ist mit Investmentfonds?
* Im Produktsteckbrief genau schauen, in welche Firmen investiert wird.
    * Oft sind bekannte große Ausbeuterfirmen, Dickmacher-Unternehmen und Öl-Förderer enthalten.
    * Der Anteil wird klein, aber die Summe aller Anleger ist entscheidend.
    * Jeder Einzelne trägt seinen Beitrag
* Offene Immobilienfonds?
    * Nachricht 2018: "Betongold" - bei Anlegern begehrt
        * so sehr, dass die Institute keine Neueinlagen mehr zulassen
            * zumindest für Deutschland. In Europa und weltweit "werde sehr aktiv investiert"
                * Was heißt das? Der Anleger gibt Geld und irgendwo auf der Welt wird anonym in Rendite investiert. Umwelt? Nachhaltigkeit?
                * siehe Let's make money-Film

### "Bankwechsel jetzt"
* siehe z. B. Kampagne "Bankwechsel jetzt" von Urgewald: https://urgewald.org/kampagne/bankwechsel-jetzt/geldwaesche-so-gehts

### Banken mit besonders zukunftstauglichen Anlagekriterien
* [Anlagekriterien GLS-Bank](https://www.gls.de/privatkunden/ueber-die-gls-bank/arbeitsweisen/anlage-und-finanzierungsgrundsaetze/)
* ...

### divest - gofossilfree
Beispiele von Organisationen, die sich für nachhaltige Geldanlage einsetzen:

* [divest - gofossilfree](http://gofossilfree.org/de/) (Geld aus der Förderung von fossilen Energieträgern abziehen)
    * [Kohlenstoffblase](http://de.wikipedia.org/wiki/Kohlenstoffblase) (Spekulationsblase)
    * [guardian: fossil free pension fund](http://www.theguardian.com/money/2015/may/09/how-get-pension-fund-divest-fossil-fuels)
    * [top 200](http://gofossilfree.org/top-200/) (Fossilunternehmen)
    * [pushyourparents](http://pushyourparents.org/)

### Lokale Energiegenossenschaften
* Anteile an regionalen Energiegenossenschaften zeichnen

### Oikocredit
* https://de.wikipedia.org/wiki/Oikocredit
* Stand 2018: 200 EUR, siehe hier: https://www.oikocredit.de/investieren/geldanlage-im-ueberblick.
    * relativ kurze Kündigungsfrist
    * Die Genossenschaft ist so groß, dass sie Regionalbereiche aufgeteilt wurde.
    * Dividende
    * Beispiel-Projekt: Kochofen mit Holzpellets (anstelle von Holz und Holzkohle und giftigem Rauch im Haus)

### Spenden
* siehe spenden-atlas.md

Inbox
-----
### 2020 - Natur, Wert und Geld
* Der beste Weg die Natur zu zerstören ist es, ihr einen Preis anstelle eines Wertes zu geben. ::natur, ::geld
    * Gib der Natur einen Preis statt einem Wert und du wirst sie verlieren.
    * Beispiel **Flächenverbrauch**, ::flächenfraß
        * Ausgleichsflächen: Flächen zerstören und dafür woanders minderwertigen Ersatz schaffen
        * Ackerflächen wenig Geld wert, aber Bauplätze viel Geld wert => mehr Geld bezahlen und dennoch zerstören
        * Land hat Preis: ich darf dein Land zerstören und du bekommst dafür ein Stück Papier mit Geld drauf oder Zahlen auf einem Computer
            * Ein gutes Geschäft? -> Kommt auf den Preis an? -> Das Land ist trotzdem weg.
            * Den Preis erhöhen? -> dann bezahlt der, der es sich leisten kann und das Land ist wieder weg.
                * Beobachtung: immer mehr Land wird verbraucht.
            * Landgrabbing in ärmeren Ländern: Land abnehmen mit Abfindung, die entweder selbst für deren Verhältnisse zu gering ist oder aus der Grabber Sicht quasi nichts ist
                * passiert - bzw. **wird von uns ohne Einwände zugelassen** - wenn die Natur nur einen Preis und keinen Wert hat
    * Beispiel **Gerechtigkeit**, soll einen Preis haben? -> wenn ich bezahle, darf ich ungerecht sein? -> Verbriefte Asozialität
        * ::Gerechtigkeit ist eine Beziehung zwischen Menschen; nicht zwischen Ländern oder Regionen (Paech? Kant, todo)
    * Alternative: **Living Planet-View** (Eisenstein)
        * father-son-example (see there)

### 2020 - Wesen des Geldes
* Harari: Geld ist notwendig für große Gesellschaften, wenn man nicht mehr jeden im Dorf (gut) kennt.
* "I don’t believe money is evil, **but it can be terribly corrosive.**" (https://en.wikiquote.org/wiki/Robert_Charles_Wilson#Spin_(2005))

### 2020 - Olivier Bossard
* YT https://www.youtube.com/channel/UCwe2PPm7MyU0Dr4EF_R0KOA/
* hat u. a. Hollywood-Filmausschnitte im Programm

### 2019 - Vertrauensbruch bei Sparkassen
* "Der rote Riese zockt ab | Doku", ARD, 2019, 45 min
    * ...
    * keine Stellungnahme zu laufenden Verfahren (weil noch läuft)
    * ...
    * keine Stellungnahme zu abgeschlossenen Verfahren (weil schon abgeschlossen)
    * ...
* siehe auch
    * https://www.finanztip.de/blog/zinsen-falsch-berechnet-kunden-koennen-hunderte-euro-nachfordern/
    * https://www.merkur.de/wirtschaft/sparkassen-volksbanken-ard-doku-deckt-auf-wie-kunden-mit-tricks-abgezockt-werden-zr-12972792.html
    * https://www.stern.de/wirtschaft/geld/abzocke-beim-girokonto--banken-sollen-jahrelang-zu-hohe-zinsen-kassiert-haben-8884482.html

### 2019 - Flowtex-Skandal
* Flowtex-Skandal, SWR-Doku 2019: https://www.youtube.com/watch?v=Mr9wHWACNP4 - "Wie ein Geschäftsmann Banken um Milliarden betrog - Big Money | SWR Doku"

### 2019 - HSBC
* "Skandalbank HSBC - Too Big to Jail | Doku | ARTE", 2018, https://www.youtube.com/watch?v=sTVPicgC5yU, 1h 30min
    * ...
    * Panama Papers
    * ...
    * 25:30...
    * ...
    * ...
