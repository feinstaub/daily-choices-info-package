Bedingungsloses Grundeinkommen
==============================

<!-- toc -->

- [Einführung - Ist das was?](#einfuhrung---ist-das-was)
- [Wie kann man das finanzieren?](#wie-kann-man-das-finanzieren)
- [Zu klärende Fragen / Kritik](#zu-klarende-fragen--kritik)
- [Zu klärende Fragen / Kritik 2](#zu-klarende-fragen--kritik-2)

<!-- tocstop -->

### Einführung - Ist das was?
* Wikipedia (umfangreich): https://de.wikipedia.org/wiki/Bedingungsloses_Grundeinkommen
* Test-Beispiel Finnland: https://de.wikipedia.org/wiki/Bedingungsloses_Grundeinkommen#Finnland
* Netzwerk: https://www.grundeinkommen.de
* todo
    * Richard David Precht
* Es gibt nicht DAS bedingungslose Grundeinkommen, sondern verschiedene Ansätze

### Wie kann man das finanzieren?
* "Das Bedingungslose Grundeinkommen - Drei Modelle", 22.03.2016, https://www.bpb.de/dialog/netzdebatte/223286/das-bedingungslose-grundeinkommen-drei-modelle
    * Bundeszentrale für politische Bildung
    * "Ute Fischer erklärt die drei meistdiskutierten Modelle in der deutschen Debatte und was sie unterscheidet."
    * z. B. von Götz W. Werner
        * "Nicht Einkommen sollen besteuert werden, sondern Ausgaben, nicht Leistungen wie Erwerbsarbeit und unternehmerische Tätigkeit, sondern der Verbrauch von Gütern, Ressourcen und Dienstleistungen, also der Konsum. Dadurch, so die Idee, wird Arbeit von Kosten befreit und Produktivität nicht mehr gelähmt."
        * http://www.unternimm-die-zukunft.de/de/
            * Finanzierung: http://www.unternimm-die-zukunft.de/de/zum-grundeinkommen/kurz-gefasst/
                * "Finanziert ist das bedingungslose Grundeinkommen schon.", weil ...
        * "Steuern, Sozialabgaben und Gehälter sind aus betriebswirtschaftlicher Perspektive Kosten, die in die Preise eingehen. Insofern bezahlen diese immer die Kunden."
        * "Ein weiterer Anstoß für diesen Ansatz des Grundeinkommens ist die Analyse, dass Vollbeschäftigung unrealistisch ist und auch kein sinnvolles Ziel sein kann in einer hochgradig arbeitsteiligen, produktiven Gesellschaft."
* https://krautreporter.de/1520-so-kann-deutschland-ein-bedingungsloses-grundeinkommen-finanzieren, 2016
    * "[...] Stefan Bergmann, promovierter Jurist und viele Jahre in der öffentlichen Finanzverwaltung tätig, erlaubt hat, einen von mir redaktionell bearbeiteten Ausschnitt aus seinem Buch „In zehn Stufen zum BGE“ zu veröffentlichen. Darin berechnet er, Stand 2014, zuerst, was es kosten würde, ein Grundeinkommen von 750 Euro zu zahlen und dann, wie Deutschland das finanzieren könnte."
* https://www.grundeinkommen.de/19/02/2009/finanzierung-des-bedingungslosen-grundeinkommens.html, 2009, "Finanzierung des bedingungslosen Grundeinkommens"
    * Unterschiedliche Modelle

### Zu klärende Fragen / Kritik
1. These: "Eine deutliche Erhöhung der Mehrwertsteuer ist nicht durchsetzbar, solange Deutschland in der EU ist."
    * weil eine erhöhte Mehrwertsteuer würde dazu führen, dass innerhalb der EU der Wettbewerbsvorteil von deutschen Produkten auf dem deutschen Markt weiter steigt. Eine Erhöhung der Mehrwertsteuer trifft ausländische Unternehmen grundsätzlich stärker als inländische (warum?). Deshalb ist Merkel vor vielen Jahren EU-weit heftig von Ökonomen kritisiert worden, als sie dir Mehrwertsteuer auf 19% erhöht hat.

2. These: "Die allermeisten Gehälter bewegen sich zwischen 3-4tsd. Euro. Da nur jeder zweite arbeitet, muss jeder, der arbeitet, 1 bis 1,5 Leute tragen, also bis zu  1500 € abführen. Und dann ist die Verwaltung des Staates, die Polizei, die Straßen und so weiter noch nicht bezahlt."

3. These: "Wenn auf den Konsum so hohe Steuern auferlegt würden, würde sich in sehr reger Schwarzmarkt entwickeln, weil man ja gerade dann damit extrem viel Geld sparen würde."
--> Siehe Argumentensammlung (u. a. "Negative Folgen und Auswege daraus"), wo das Thema Schwarzmarkt besprochen wird: https://wiki.piratenpartei.de/Bedingungsloses_Grundeinkommen/Argumente_rund_um_das_Grundeinkommen
    * These: "bei Konsumsteuermodell: Gut leben in Deutschland, billig kaufen im Ausland bzw. Schwarzmarkt"
    * todo

* Eine Blogger, der den Nutzen des BGE anzweifelt, dessen Vorschläge aber auch Probleme machen (siehe erster Kommentar auf der Seite): http://www.egon-w-kreutzer.de/0PaD2010/36.html

### Zu klärende Fragen / Kritik 2
* [mdr-Rezension zu Free Lunch Society (2017)](https://www.mdr.de/kultur/video-bedingungsloses-grundeinkommen-artour-100_zc-9a60c313_zs-451b2ff6.html), 2017
    * "Wer macht nach Einführung des BGE die Arbeiten, die keiner machen will?"
        * --> "Bei ungeliebten Arbeiten werden die Löhne steigen, ok. Aber bei geliebten Arbeiten werden die Löhne vermutlich fallen, weil Grundeinkommen wie ein Kombilohn wirkt." (https://wiki.piratenpartei.de/Bedingungsloses_Grundeinkommen/Argumente_rund_um_das_Grundeinkommen)
    * "Wer bezahlt die Krankenkasse?"
        * --> "Jeder Mensch muss ohne Wenn und Aber Zugang zu ausreichender Gesundheitsversorgung haben. Ob die Krankenkassenbeiträge im Grundeinkommen integriert sind oder nicht, hängt vom konkreten Modell ab. Entscheidend ist, dass das Grundeinkommen ohne den Krankenkassenanteil die Existenz und die Teilhabe am gesellschaftlichen Leben sichert." (https://www.grundeinkommen.de/die-idee/fragen-und-antworten)
    * Befürchtung, dass der Sozialstaat abgebaut wird
