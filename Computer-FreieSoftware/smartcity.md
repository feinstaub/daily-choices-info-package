Smart City
==========

<!-- toc -->

- [Hintergrund](#hintergrund)
- [Beispiele](#beispiele)
  * [Smart City Barcelona](#smart-city-barcelona)
  * [SMART REBEL CITY](#smart%E2%80%8Arebel%E2%80%8Acity)
  * [Cittàslow](#cittaslow)

<!-- tocstop -->

Hintergrund
-----------
* Die Smarte Stadt neu denken, ...
* siehe auch ::techniküberschätzung

Beispiele
---------
### Smart City Barcelona
* https://kontrast.at/barcelona-smart-city-wirtschaft
    * "Mehr Bürgerbeteiligung - In Barcelona schreiben die Bürger das Regierungsprogramm selbst – und machen ihre Stadt sozialer"
    * ...
    * ...
* https://decodeproject.eu
    * "Giving people ownership of their personal data"
* https://decidim.org
    * decidim - "Free Open-Source participatory democracy for cities and organizations"

### SMART REBEL CITY
"... oder die Digitale Stadt vom Mensch aus denken.
Was Planerinnen, Stadtmacher, Architektinnen und Verwaltungsmitarbeiter über die digitale und vernetzte Stadt wissen sollten."

http://www.smartrebelcity.org

* "1 Smart City neu denken"
    * ...
* ...
* About: "Können Daten, neue Technologien und digitale Tools dazu beitragen, Stadtplanung und Stadtentwicklung transparenter, selbstorganisierter und demokratischer zu machen?"

### Cittàslow
* siehe dort (wachstum.md)
