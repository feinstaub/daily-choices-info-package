Zusatzinfos / Freie Software
============================

Software-Sammlung
-----------------
...

### Spiele
* Open Source Game Clones - http://osgameclones.com/
* Open Source Text Games - https://bronevichok.ru/ttygames/
* Free, open source game and development news - http://freegamer.blogspot.de/


Interessante Fragen
-------------------
### "Aber wie soll man denn dann mit Software Geld verdienen?"
* Seit wann wird in ethischen Fragen mit dem Verdienen von Geld argumentiert?
* Praktische Beispiele wie man dennoch Geld verdienen kann
    * z. B. Computerladen um die Ecke bekommt Geld für Installationen und Wartung
    * Linux-Kernel für eigene (z. B. embedded) Produkte hernehmen, customizen und das Produkt verkaufen
* Dual-Licensing, siehe ["On Selling Exceptions to the GNU GPL"](https://www.fsf.org/blogs/rms/selling-exceptions), 2010
    * Beispiel Qt
    * "I consider selling exceptions an acceptable thing for a company to do, and I will suggest it where appropriate as a way to get programs freed."

### "Aber wer nutzt das denn?"
* Wenn es z. B. darum geht, die [Privatsphäre](../Hintergrund/privatsphaere.md) zu schützen, welche Aussagekraft hat die Anzahl der Nutzer?
* In absoluten Zahlen wird Freie Software  stark genutzt.
* Wenn du es nutzt, dann kannst du anderen helfen, es auch zu nutzen.

Was ist Programmieren?
----------------------
### Was ist es nicht

:)

![](supercoder.jpg)
