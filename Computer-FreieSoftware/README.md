Freie Software / daily-choices-info-package
===========================================

[zurück](../../..)

<!-- toc -->

- [Einstieg](#einstieg)
  * [Artikel](#artikel)
  * [Komfort vs. Freiheit](#komfort-vs-freiheit)
  * [Ich habe nichts zu verbergen, Kontrolle](#ich-habe-nichts-zu-verbergen-kontrolle)
  * [Bücher etc. über den Hintergrund der Notwendigkeit etwas zu tun](#bucher-etc-uber-den-hintergrund-der-notwendigkeit-etwas-zu-tun)
  * [Videos](#videos)
  * [Medien](#medien)
- [Vorreiter in Freier Software](#vorreiter-in-freier-software)
  * [Öffentliche Organisationen](#offentliche-organisationen)
  * [Hosting-Anbieter](#hosting-anbieter)
  * [Allgemein](#allgemein)
- [Freie Software verstehen lernen](#freie-software-verstehen-lernen)
  * [Analogie Wikipedia](#analogie-wikipedia)
  * [Das Recht auf Reparieren](#das-recht-auf-reparieren)
  * [Warum müssen Endbenutzer den Quellcode sehen und ändern dürfen?](#warum-mussen-endbenutzer-den-quellcode-sehen-und-andern-durfen)
  * ["Je größer und bekannter ein Hersteller ist..."](#je-grosser-und-bekannter-ein-hersteller-ist)
  * [Was ist schlecht am unkontrollierten Datensammeln?](#was-ist-schlecht-am-unkontrollierten-datensammeln)
- [IT an Schulen](#it-an-schulen)
  * [Don't promote open source solutions as an alternative that is cheaper or free, 2020](#dont-promote-open-source-solutions-as-an-alternative-that-is-cheaper-or-free-2020)
  * ["IT in der Schule: Sorgloser Umgang mit Schülerdaten", 2017](#it-in-der-schule-sorgloser-umgang-mit-schulerdaten-2017)
- [Freie / Offene Alternativen von Diensten und Software](#freie--offene-alternativen-von-diensten-und-software)
  * [anstelle Doodle](#anstelle-doodle)
  * [Video-Konferenzen](#video-konferenzen)
- [Weiteres](#weiteres)

<!-- tocstop -->

Einstieg
--------
### Artikel
* https://de.wikipedia.org/wiki/Freie_Software
    * Hat etwas mit Ethik zu tun
    * Rein praktische Gesichtspunkte stehen nicht im Vordergrund

* Arte-Interview: "Ein Gespräch mit Richard Stallman, dem Papst der Freien Software", https://www.youtube.com/watch?v=lONkIbwFoMM, 2015, 4 min

* Digitale Mündigkeit: https://digitalcourage.de/digitale-selbstverteidigung/digitale-muendigkeit

### Komfort vs. Freiheit
* https://www.youtube.com/watch?v=9c3sv30w158, 2019, 24 min, Stallman bei RT
    * 24:30 min: "Warum haben wir überhaupt Freiheit? Weil in der Vergangenheit Leute bereit waren Opfer für die Freiheit aufzubringen.
        Wir alle sind **kommerzieller Public Relation** ausgesetzt, die uns versucht beizubringen,
        **dass wir Bequemlichkeit mehr brauchen als irgendetwas anderes**. Wenn wir Freiheit wollen, müssen wir lernen dem nicht zuzustimmen."
        * siehe auch arte: Public Relation

### Ich habe nichts zu verbergen, Kontrolle
* siehe dort
* https://www.youtube.com/watch?v=hIXhnWUmMvw - "Shoshana Zuboff on surveillance capitalism | VPRO Documentary", 2019, 50 min, ::soziologie
    * "Harvard professor Shoshana Zuboff wrote a monumental book about the new economic order that is alarming.
        "The Age of Surveillance Capitalism," reveals how the biggest tech companies deal with our data.
        How do we regain control of our data? What is surveillance capitalism?"
    * google, facebook
    * "I like those gadgets", "I like those targeted ads", "I have nothing to hide"
    * based on the misconception that we know what we give them; that we can exert some control over what is given away
    * ...
    * Beispiel Gesichtserkennung
        * Bilder hochladen
        * Unsere Privatsphäre muss gar nicht verletzt werden, aber
        * mit unseren Bildern werden Modelle gefüttert, die weltweit eine bessere Gesichtserkennung erlauben
        * diese wird dann verkauft (nicht an uns, sondern an Unternehmen), die damit z. B. in Asien hilft per Gesichtserkennung Leute zu verhaften
    * läuft im Unerkennbaren/Geheimen ab => Ignoranz der User => Their bliss
    * Facebook experiment mit subliminal cues, Ergebnis
        * 1. hat Auswirkungen auf reale Entscheidungen oder Gefühle
        * 2. ohne, dass der User was davon merkt
    * ...
    * ... todo ...

### Bücher etc. über den Hintergrund der Notwendigkeit etwas zu tun
* ["Sicherheitsexperte Bruce Schneier fordert "Bremse für Innovationen""](https://www.heise.de/newsticker/meldung/Sicherheitsexperte-Bruce-Schneier-fordert-Bremse-fuer-Innovationen-4196961.html)
    * "Die IT-Branche muss dringend stärker reguliert werden, auch wenn der technische Fortschritt dadurch langsamer wird, fordert der IT-Sicherheitsexperte."
    * Buch: ["Click Here to Kill Everybody - Security and Survival in a Hyper-connected World"](https://www.wwnorton.co.uk/books/9780393608885-click-here-to-kill-everybody), 2018, Bruce Schneier
        * "xplores the risks and security implications of the World-Sized Web and lays out common-sense policies that will allow us to enjoy the benefits of this new omnipotent age without surrendering ourselves entirely to our creation."
    * Buch: ["Data and Goliath - The Hidden Battles to Collect Your Data and Control Your World"](https://www.wwnorton.co.uk/books/9780393244816-data-and-goliath), 2015, Bruce Schneier
        * “Bruce Schneier...grasps this revolution's true dimensions...Schneier paints a picture of the big-data revolution that is dark, but compelling; one in which the conveniences of our digitized world have devalued privacy.” — Nature
        * "The combination of qualitative analysis and detailed examples is compelling and the conclusions are stark. Surveillance matters, and not just at a theoretical level. Schneier shows how it causes damage even when it's used "properly", and also offers examples of how it can be and is abused."

### Videos
* Video: [Interview mit Richard Stallman 2017](https://www.youtube.com/watch?v=jUibaPTXSHk), 2017, 30 min, engl.
    * Professioneller Interview-Partner David Pakman
    * ...
    * 15:15: Gute Erklärung zur vermeintlichen Analogie "geschütztes Kochrezept" und "geschützter Softwarequellcoee"
        * does not reject **output** of proprietary program
        * ...
        * "I don't boycot companies because they internally use Windows. I'm just sorry for them, there the ones who were losing freedom"
            * "I hope you free yourself. You do deserve to control your own computing. But I won't boycot them because **they** lost their freeedom."
            * "I refuse to use the non-free software myself."
            * "And I won't lead anyone else to use it. Because then I would lead that person to lose freedom."
    * 18:55: Are there challenges in daily life?
        * ...
    * ...

* Video: [Interview mit Richard Stallman über IoT](https://www.youtube.com/watch?v=AAP4N3KyLmM), 2018, 17 min, engl.
    * "Richard Stallman: Dangers of IoT and Amazon Alexa"
    * z. B. Hausautomatisierung mit Thermostaten, die ans Internet angeschlossen sind, etc.
        * but the market is growing, what can we do?
            * don't buy them
            * "We have to develop the strength to say no"
        * practical comfort?
            * talks not to people who are ready to give up their freedom
            * wants to talk to people who value freedom enough to make a sacrifice for it
        * "first thought: you can't trust that thing - I don't want it"
            * "when people teach themselves whenever they see such a thing: you can't trust it, I won't take it, then we can defeat those things"
    * Business vs. consumer products
    * ...
    * "Once I was in a house with an Alexa device. I unplugged it. [...] I don't trust it at all."
        * systems must be designed not to accumulate data about people
        * "we have to limit the collection of data"
        * "the user's consent is not an excuse for spying on people"
            * "this principle is necessary because companies have become adept at manufacturing consent"
            * ...
            * e.g. basic service is providing transportation to people, then this is possible without to identify who they are
                * "therefore the law should say you are forbidden to find out who the passenger is"
                    * e.g. they would be required to accept cash money
    * siehe auch denkweise.md

### Medien
* sueddeutsche
    * 2019: "Warum Open Source so erfolgreich ist"
    * 2018: "Mit Open Source siegt die Vernunft"

Vorreiter in Freier Software
----------------------------
### Öffentliche Organisationen
* Bundesanstalt für Materialforschung und -prüfung (BAM)
    * https://www.bam.de/Content/DE/Nachrichten/2020/2020-04-16-bam-foerdert-nutzen-von-open-source-software.html
    * "„Der offene Umgang mit selbst erstellter Software stärkt die Open Science Community“, so BAM-Präsident Ulrich Panne. „Daher freue ich mich, dass wir diesen Teil unseres Selbstverständnisses nun auch schriftlich festgehalten haben.“"
* gnuHU-linux an der HU Berlin
    * https://www.projekte.hu-berlin.de/de/hu-leaks/die-projekte/free-ed/die-projekte/gnuni-linux/gnuhu-linux-1
    * https://jahr1nachsnowden.de/
* https://publiccode.eu
    * Intro-Video auf deutsch: https://publiccode.eu/de/
        * Fix My Street, von https://www.mysociety.org/ (siehe auch https://mapit.mysociety.org)
        * Züri wie neu - https://www.zueriwieneu.ch/faq
    * [Offener Brief](https://publiccode.eu/de/openletter/)
    * KDE macht mit: https://dot.kde.org/2017/09/13/public-money-public-code-join-fsfe-campaign
    * [Spiegel-Online-Artikel](http://www.spiegel.de/netzwelt/netzpolitik/public-code-aktivisten-fordern-freie-software-vom-staat-a-1167416.html)
    * Folgende bekannte Organisationen sind dafür
        * Wikimedia Deutschland
        * Wikimedia Italia
        * Wikimedia France
        * DigitalCourage
        * Digitale Gesellschaft
        * The Document Foundation (LibreOffice)
        * CreativeCommons
        * VLC Player Stiftung
        * Technologiestiftung Berlin
        * KDE
        * fsfe
        * openSUSE
        * OpenSource Initiative
* 2017: Schleswig-Holstein
    * [heise-Artikel](https://www.heise.de/newsticker/meldung/Schleswig-Holstein-laeutet-Abschied-von-Microsoft-ein-3849115.html)

### Hosting-Anbieter
* https://schokokeks.org/freie_software
    * "Wir setzen zum Betrieb unserer Services ausschließlich freie Software ein und veröffentlichen auch Eigenentwicklungen. Freie Software bedeutet, die Freiheit zu haben, Software nach Belieben zu nutzen, kopieren, verändern und verändert weitergeben zu dürfen."
    * Public Money - Public Code
* https://mtmedia.org/about/
    * "Wir machen unsere Arbeit, weil wir Datenschutz wichtig finden, weil wir davon überzeugt sind, dass sich eine Gesellschaft nur weiterentwickeln kann, wenn sie nicht überwacht wird. Wir sehen uns als Teil von Technik-Kollektiven und freier Software Bewegung."

### Allgemein
* 2018: ["Open Source in Schweizer Unternehmen und Behörden im Aufwind"](https://www.heise.de/newsticker/meldung/Open-Source-in-Schweizer-Unternehmen-und-Behoerden-im-Aufwind-4088182.html)
    * [Forschungsstelle Digitale Nachhaltigkeit der Universität Bern](https://www.oss-studie.ch/info.html)
* 2017: EU-Studie empfiehlt freie Software für die öffentliche Verwaltung und für Unternehmen
    * https://www.golem.de/news/neue-eu-studie-open-source-ist-jetzt-fuer-alle-gut-1703-126963.html

Freie Software verstehen lernen
-------------------------------
### Analogie Wikipedia
* Verstehe die Funktionsweise der Artikelhistorie und der Diskussionsseite
    * => hohe Transparenz und Nachvollziehbarkeit/Überprüfbarkeit für alle Benutzer
    * => Vertrauen in eine akzeptable bis exzellente Artikel-Qualität

* Viele Augen schauen drauf und finden Fehler oder Unzulänglichkeiten

* Benutzer legen hohe Standards an (Wikipedia-Standards)
    * Alle (die meisten) Teilnehmer sind daran interessiert, dass die Artikel höchste Qualität haben.
    * Es existiert kein monetäres Geschäftsmodell, das bedient werden muss.
        * Das heißt, die Arbeit funktioniert unabhängig von wirtschaftlichen Zwängen/Vorgaben.
        * Die Teilnehmer unterliegen keiner übergeordneten Weisungsbefugnis und können ganz nach ihren ethischen Vorstellungen handeln

### Das Recht auf Reparieren
* Siehe Politik des angebissenen Apfels und dessen Folgen
* Lizensierte proprietäre Software: kein Recht auf eigene Reparatur oder Reparatur eines unabhängigen Dritten
    * vollständige Hersteller-Abhängigkeit vs. Eigentum
* Reparierbare Produkte sind nachhaltige Produkte

### Warum müssen Endbenutzer den Quellcode sehen und ändern dürfen?
* siehe Recht auf Reparieren
* Bei Software (insbesondere bei vernetzter) können kleine Unterschiede zwischen nutzbringendem (Software tut, was sie soll) oder schädlichem (Spionage, Inkompatiblität) Verhalten führen.
* siehe z. B. luki todo

### "Je größer und bekannter ein Hersteller ist..."
"... desto mehr kann ich als Kunde darauf vertrauen, dass er die Kunden-Interessen und die Interessen der Allgemeinheit vertritt".

Widerlegung durch Gegenbeispiele:

* Existenz des Bundeskartellamtes
* Autohersteller, der auch Motoren-Steuersoftware verbaut

Argumentationslinien:

* BSI steht positiv gegenüber freier SW. RMS ist Ur-Vertreter der freien SW. Welchen Stellenwert haben dann seine Aussagen? Was sagt das über große bekannte Unternehmen aus, die hauptsächlich unfreie SW an den Enduser verkaufen?

### Was ist schlecht am unkontrollierten Datensammeln?
* ["Get your loved ones off Facebook."](http://www.salimvirani.com/facebook/) and WhatsApp
* [Warum ist Privatsphäre wichtig](../Hintergrund/privatsphaere.md)
* siehe elsewhere

IT an Schulen
-------------
### Don't promote open source solutions as an alternative that is cheaper or free, 2020
* https://blog.jospoortvliet.com/2020/04/rant-of-day-well-at-least-microsoft-is.html, 2020
    * "They are promoting open source solutions as an alternative that is cheaper or free, which just makes it look inferior to management. They are not telling organizations to pay local and open source product companies instead of Microsoft."
    * ...

### "IT in der Schule: Sorgloser Umgang mit Schülerdaten", 2017
* 15.04.2017: https://www.heise.de/newsticker/meldung/IT-in-der-Schule-Sorgloser-Umgang-mit-Schuelerdaten-3686182.html
    * "Sensible Daten von Schulkindern in den Vereinigten Staaten gelangen sehr häufig in die Hände von IT-Firmen, ohne dass die Eltern zustimmen. Das ist das Ergebnis einer Studie der Electronic Frontier Foundation."
    * Also aufpassen, wohin wir uns hierzulande bewegen wollen.

Freie / Offene Alternativen von Diensten und Software
-----------------------------------------------------
### anstelle Doodle
* http://www.der-paritaetische.de/schwerpunkt/digitalisierung/webzeugkoffer/faq/doodle-dudle-foodle-co-was-ist-das-beste-tool-zur-terminfindung/
    * https://nuudel.digitalcourage.de/
    * https://dudle.inf.tu-dresden.de/
    * https://bitpoll.mafiasi.de/ - "Bitpoll nennt sich auch Dudel und ist eine Weiterentwicklung des o.g. Programms der TU Dresden"
    * https://terminplaner4.dfn.de - auch "Foodle" genannt

### Video-Konferenzen
* http://www.der-paritaetische.de/schwerpunkt/digitalisierung/webzeugkoffer/faq/wie-organisiere-ich-eine-videokonferenz/
    * Jitsi Meet mit guter Serverliste

Weiteres
--------
* [IT an Schulen](../Hintergrund/IT-an-Schulen.md)
* [Computer-Nutzung](../Hintergrund/computer-nutzung.md)
* [Zusatzinfos](zusatzinfos.md) und interessante Fragen
* [Microsoft](../Computer/microsoft.md)
* Impulse durch den End-Nutzer
    * [Impulse durch den Nutzer](../Computer/impulse-durch-endnutzer.md)
* Freie-Software-Ressourcen / Einstieg / Material:
    * http://www.freiesoftwareog.org/
    * http://www.freiesoftwareog.org/links.html
    * Material: http://www.freiesoftwareog.org/downloads.html
