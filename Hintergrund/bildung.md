Bildung
=======

<!-- toc -->

- [Inbox](#inbox)
  * [Digitale Bildung: Personales Band wichtig, 2020](#digitale-bildung-personales-band-wichtig-2020)
  * [Ist die Schule noch zu retten? Wie wir künftig lernen müssen, 2019](#ist-die-schule-noch-zu-retten-wie-wir-kunftig-lernen-mussen-2019)
  * [Schlafforschung, 2018](#schlafforschung-2018)
  * [bildungsserver.de](#bildungsserverde)

<!-- tocstop -->

::bildung

Inbox
-----
### Digitale Bildung: Personales Band wichtig, 2020
* https://www.nzz.ch/meinung/digitale-bildung-vernunft-und-empirie-helfen-weiter-ld.1552714, 2020
    * "Digitale Bildung: Vernunft und Empirie als Antwort auf eine entgleiste Debatte"
        * "Julian Nida-Rümelin lehrt Philosophie an der Ludwig-Maximilians-Universität München.
        * Klaus Zierer ist Ordinarius für Schulpädagogik an der Universität Augsburg."
    * "Wer sich freut, dass die Welt der Bildung dank der Brachialgewalt der Corona-Krise endlich den Durchbruch zum Digitalen geschafft hat, ist auf dem Holzweg. Das direkte personale Band zwischen Lehrer und Schüler bleibt von zentraler Bedeutung für jeden Lernerfolg."
    * "Wenn es nicht nur um Lernen geht, sondern um Bildung, dann braucht der Mensch den Menschen."
    * "Ein wesentliches Ziel digitaler Bildung muss es daher sein, Jugendliche mit diesen Mechanismen vertraut zu machen und sie gegen den Trend zu Ideologisierung und Isolierung immun zu machen – anders formuliert: sie zu ermächtigen, eigenständige Akteure in den digitalen Kommunikations- und Interaktionswelten zu werden."

### Ist die Schule noch zu retten? Wie wir künftig lernen müssen, 2019
* MDR-Doku: "Ist die Schule noch zu retten? Wie wir künftig lernen müssen": https://www.mdr.de/wissen/dokumentationen/video-353664_zc-99e06b6a_zs-8a16bf16.html, 45 min, 2019
    * "Kinder, die in der ersten Klasse lernen, gehen um das Jahr 2080 in Rente. Welche Bildung brauchen sie für ihr Leben?
        Wie müssen Kinder heute ausgebildet werden, um Anforderungen in der Zukunft zu entsprechen? (nur in D)"
* ...
* "Was macht dir Spaß? Wo willst du hin?"
* Miteinander
* Kleine Schüler fragen die größeren: kannst du mir das mal erklären
* ... todo ...

### Schlafforschung, 2018
* 2018: Schlafforschung: "Es wäre besser, wenn die Schule nicht so früh anfangen würde" (Universitätsmedizin Mainz)

### bildungsserver.de
* http://www.bildungsserver.de/instset.html?Id=9577 - information.medien.agrar e.V. (i.m.a e.V.)
    * "Gemeinnütziger Verein"
    * eingetragen durch eine Person des i.m.a.
* siehe Qualitätskriterien Bildungsserver: http://www.bildungsserver.de/Redaktion/pdf/Qualitaetskriterien_Bildungsserver_allgemein.pdf
    * "Reputation: Der Herausgeber, der Autor bzw. die Kooperationspartner, Unterstützer, oder Schirmherren sind vertrauenswürdig"
        * siehe aber https://lobbypedia.de/wiki/Information_Medien_Agrar.
