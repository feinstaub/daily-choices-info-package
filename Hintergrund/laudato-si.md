ENZYKLIKA LAUDATO SI’ VON PAPST FRANZISKUS
==========================================
"ÜBER DIE SORGE FÜR DAS GEMEINSAME HAUS"

http://w2.vatican.va/content/francesco/de/encyclicals/documents/papa-francesco_20150524_enciclica-laudato-si.html
http://w2.vatican.va/content/dam/francesco/pdf/encyclicals/documents/papa-francesco_20150524_enciclica-laudato-si_ge.pdf

- Umweltkrise
- Weltweite Ungleichheit und Ungerechtigkeit, soziale Dimension
- menschliche Wurzel der ökologischen Krise
- Kultur
- Spiritualität

"13. Die dringende Herausforderung, unser gemeinsames Haus zu schützen, schließt die Sorge ein, die gesamte Menschheitsfamilie in der Suche nach einer nachhaltigen und ganzheitlichen Entwicklung zu vereinen, denn wir wissen, dass sich die Dinge ändern können."

"14. Ich lade dringlich zu einem neuen Dialog ein über die Art und Weise, wie wir die Zukunft unseres Planeten gestalten. Wir brauchen ein Gespräch, das uns alle zusammenführt, denn die Herausforderung der Umweltsituation, die wir erleben, und ihre menschlichen Wurzeln interessieren und betreffen uns alle. Die weltweite ökologische Bewegung hat bereits einen langen und ereignisreichen Weg zurückgelegt und zahlreiche Bürgerverbände hervorgebracht, die der Sensibilisierung dienen. Leider pflegen viele Anstrengungen, konkrete Lösungen für die Umweltkrise zu suchen, vergeblich zu sein, nicht allein wegen der Ablehnung der Machthaber, sondern auch wegen der Interessenlosigkeit der anderen. Die Haltungen, welche – selbst unter den Gläubigen – die Lösungswege blockieren, reichen von der Leugnung des Problems bis zur Gleichgültigkeit, zur bequemen Resignation oder zum blinden Vertrauen auf die technischen Lösungen. Wir brauchen eine neue universale Solidarität."

"43. Wenn wir berücksichtigen, dass der Mensch auch ein Geschöpf dieser Welt ist, das ein Recht auf Leben und Glück hat und das außerdem eine ganz besondere Würde besitzt, können wir es nicht unterlassen, die Auswirkungen der Umweltzerstörung, des aktuellen Entwicklungsmodells und der Wegwerfkultur auf das menschliche Leben zu betrachten."

"46. Zu den sozialen Komponenten der globalen Veränderung gehören auch die Auswirkungen einiger technologischer Neuerungen auf die Arbeit, die soziale Ausschließung, die Ungleichheit in der Verfügbarkeit und dem Konsum von Energie und anderen Diensten, die gesellschaftliche Aufsplitterung, die Zunahme der Gewalt und das Aufkommen neuer Formen sozialer Aggressivität, der Rauschgifthandel und der steigende Drogenkonsum unter den Jüngsten, der Verlust der Identität. Das sind unter anderem Zeichen, die zeigen, dass das Wachstum der letzten beiden Jahrhunderte nicht in allen seinen Aspekten einen wahren ganzheitlichen Fortschritt und eine Besserung der Lebensqualität bedeutet hat. Einige dieser Zeichen sind zugleich Symptome eines wirklichen sozialen Niedergangs, eines stillschweigenden Bruchs der Bindungen von sozialer Integration und Gemeinschaft."

"V. WELTWEITE SOZIALE UNGERECHTIGKEIT"

"48. Die menschliche Umwelt und die natürliche Umwelt verschlechtern sich gemeinsam, und wir werden die Umweltzerstörung nicht sachgemäß angehen können, wenn wir nicht auf Ursachen achten, die mit dem Niedergang auf menschlicher und sozialer Ebene zusammenhängen. Tatsächlich schädigen der Verfall der Umwelt und der der Gesellschaft in besonderer Weise die Schwächsten des Planeten: „Sowohl die allgemeine Erfahrung des alltäglichen Lebens als auch die wissenschaftliche Untersuchung zeigen, dass die schwersten Auswirkungen all dieser Umweltverletzungen von den Ärmsten erlitten werden.“[26] So beeinträchtigt zum Beispiel die Erschöpfung des Fischbestands speziell diejenigen, die vom handwerklichen Fischfang leben und nichts besitzen, um ihn zu ersetzen; die Verschmutzung des Wassers trifft besonders die Ärmsten, die keine Möglichkeit haben, abgefülltes Wasser zu kaufen, und der Anstieg des Meeresspiegels geht hauptsächlich die verarmte Küstenbevölkerung an, die nichts haben, wohin sie umziehen können. Die Auswirkung der aktuellen Formen von Unordnung zeigt sich auch im vorzeitigen Sterben vieler Armer, in den Konflikten, die durch Mangel an Ressourcen hervorgerufen werden, und in vielen anderen Problemen, die keinen ausreichenden Platz auf der Tagesordnung der Welt haben.[27]"

"49. Ich möchte darauf hinweisen, dass man gewöhnlich keine klare Vorstellung von den Problemen hat, die besonders die Ausgeschlossenen heimsuchen. Sie sind der größte Teil des Planeten, Milliarden von Menschen. Heute kommen sie in den internationalen politischen und wirtschaftlichen Debatten vor, doch oft scheint es, dass ihre Probleme gleichsam als ein Anhängsel angegangen werden, wie eine Frage, die man fast pflichtgemäß oder ganz am Rande anfügt, wenn man sie nicht als bloßen Kollateralschaden betrachtet. Tatsächlich bleiben sie im Moment der konkreten Verwirklichung oft auf dem letzten Platz."

"50. Anstatt die Probleme der Armen zu lösen und an eine andere Welt zu denken, haben einige nichts anderes vorzuschlagen als eine Reduzierung der Geburtenrate."

"DIE MENSCHLICHE WURZEL DER ÖKOLOGISCHEN KRISE"

"101. Es wird uns nicht nützen, die Symptome zu beschreiben, wenn wir nicht die menschliche Wurzel der ökologischen Krise erkennen. Es gibt ein Verständnis des menschlichen Lebens und Handelns, das fehlgeleitet ist und der Wirklichkeit widerspricht bis zu dem Punkt, ihr zu schaden. Warum sollen wir nicht innehalten, um darüber nachzudenken? Bei dieser Überlegung schlage ich vor, dass wir uns auf das vorherrschende technokratische Paradigma konzentrieren und auf die Stellung des Menschen und seines Handelns in der Welt."

"II. DIE GLOBALISIERUNG DES TECHNOKRATISCHEN PARADIGMAS"

"106. Das Grundproblem ist ein anderes, noch tieferes, nämlich die Art und Weise, wie die Menschheit tatsächlich die Technologie und ihre Entwicklung zusammen mit einem homogenen und eindimensionalen Paradigma angenommen hat."

"111. Die ökologische Kultur kann nicht reduziert werden auf eine Serie von dringenden Teilantworten auf die Probleme, die bezüglich der Umweltschäden, der Erschöpfung der natürlichen Ressourcen und der Verschmutzung auftreten. Es müsste einen anderen Blick geben, ein Denken, eine Politik, ein Erziehungsprogramm, einen Lebensstil und eine Spiritualität, die einen Widerstand gegen den Vormarsch des technokratischen Paradigmas bilden. Andernfalls können auch die besten ökologischen Initiativen schließlich in derselben globalisierten Logik stecken bleiben. Einfach nur eine technische Lösung für jedes auftretende Umweltproblem zu suchen bedeutet, Dinge zu isolieren, die in der Wirklichkeit miteinander verknüpft sind, und die wahren und tiefsten Probleme des weltweiten Systems zu verbergen."

...
...

"EINE GANZHEITLICHE ÖKOLOGIE"

"137. Angesichts der Tatsache, dass alles eng aufeinander bezogen ist und dass die aktuellen Probleme eine Perspektive erfordern, die alle Aspekte der weltweiten Krise berücksichtigt, schlage ich vor, dass wir uns nun mit den verschiedenen Elementen einer ganzheitlichen Ökologie befassen, welche die menschliche und soziale Dimension klar mit einbezieht."

...
...

"141. Auf   der  anderen  Seite  neigt  das  Wirt-schaftswachstum dazu, Automatismen zu erzeu-gen  und  zu  »homogenisieren«,  mit  dem  Zweck,  Abläufe  zu  vereinfachen  und  Kosten  zu  verrin-gern.  Daher  ist  eine  Wirtschaftsökologie  not-wendig, die in der Lage ist, zu einer umfassende-ren Betrachtung der Wirklichkeit zu verpflichten. Denn  »damit  eine  nachhaltige  Entwicklung  zu-stande kommt, muss der Umweltschutz Bestand-teil  des  Entwicklungsprozesses  sein  und  darf   nicht von diesem getrennt betrachtet werden«"

...
...
