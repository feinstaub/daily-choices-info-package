Hintergründe / daily-choices-info-package
=========================================

[zurück](../../..)

Unsortierte Informationen und Diskussionsanregungen zu alternativen, gegenwarts- und zukunftsgerichteten Mindsets aus heutiger Zeit und der Geschichte.

<!-- toc -->

- [Doku-Liste](#doku-liste)
- [Jahres-Kalender](#jahres-kalender)
- [Andere Zusammenstellungen](#andere-zusammenstellungen)
  * [NABU](#nabu)
  * [Leitungswasser](#leitungswasser)
- [Klimawandel](#klimawandel)
- [Die Allmende-Herausforderung](#die-allmende-herausforderung)
- [Computer-Nutzung](#computer-nutzung)
- [Werbung](#werbung)
- [Psychologie](#psychologie)
- [Unsortiert](#unsortiert)

<!-- tocstop -->

Doku-Liste
----------
(siehe jeweils dort)

Zum Ausdrucken und Abhaken

* Dosen-Doku
    * über Manadrinen, Orangen, Tomaten, Pilze
    * (ok ____) "Billige Lebensmittel aus Fernost - China in Dosen | SWR Doku" - https://www.youtube.com/watch?v=nL_78p6kljg, 2019, 45 min
* Flächen-Doku
    * (ok ____) "Luxusgut Lebensraum - 3sat", August 2019, 45 min (https://www.3sat.de/wissen/wissenschaftsdoku/luxusgut-lebensraum-100.html)
    * (ok ____) "Versiegeln, verbauen, zuteeren: Flächenfraß in Bayern", BR, November 2019, 30 min
    * (https://www.br.de/br-fernsehen/programmkalender/ausstrahlung-1947084.html)
* Reifen-Doku
    * (ok ____) "Autoreifen - Ein schmutziges Milliardengeschäft | WDR Doku", https://www.youtube.com/watch?v=u_QPxTJbwTw, 2019
* Sand-Doku
    * (ok ____) ["Sand - Die neue Umweltzeitbombe - Arte Doku 2013 HD"](https://www.youtube.com/watch?v=nY37sNXpf7g), 1h 15min
* Zucker-Doku
    * (ok ____) https://www.youtube.com/watch?v=UDiRnemBqPI, Die große Zuckerlüge | Doku | ARTE, 2015, 90 min
* weitere
    * zucker: von arte, dick dicker zucker
    * Tomaten-Doku (todo)
    * Biodiversität?
    * Klimawandel?
    * Phosphor etc.? (wie werden wir alle satt?)
    * Versicherungen (priv. kkv)
    * Finanz
    * Plastik Planet

Jahres-Kalender
---------------
* [jahres-kalender](jahres-kalender.md)

Andere Zusammenstellungen
-------------------------
### NABU

* ["NABU-Infografik zum umweltfreundlichen Einkauf im Supermarkt"](https://www.nabu.de/umwelt-und-ressourcen/ressourcenschonung/einzelhandel-und-umwelt/nachhaltigkeit/21716.html)
    * Eine schöne Zusammenstellung mit vielen praktischen Tipps
    * Anmerkungen zur Verbesserung (April 2017)
        * "besser als regional ist bio-regional"
            * Anmerkung: Erweiterung: bio innerhalb Europas ist vermutlich besser als regional, aber konventionell
        * "Am besten für die Umwelt: Genießen Sie Fleisch – aber auch andere tierische Produkte – nur in Maßen."
            * Anmerkung:
                * das optimale Maß ist 0.
                * das suboptimale Maß ist 1x pro Monat oder 1x pro Woche
                * bei öfter wird es kritisch
        * "0 auf dem Ei ist Ökologische Haltung"
            * Anmerkung: bitte auch diese Eier vermeiden, siehe [Bio-Eier](../Vegan/bio-eier.md)
        * "Kaufen Sie Wurst, Fleisch und Käse an der Bedientheke. Das macht meistens weniger Müll."
            * Anmerkung: das, was bei der Produktion von dieser Produkte alles kaputtgeht, ist kaum mit dem gesparten Müll aufwiegbar. :(
        * "Bei Wildfisch bietet das MSC-Label die beste Orientierung im Supermarkt"
            * Anmerkung: in Summe sind die Meere dennoch [überfischt](../Vegan/fisch-fischer-überfischung.md).
    * "Trinken Sie Leitungswasser!"
        * siehe auch ["Wie gut ist unser Leitungswasser?"](https://www.youtube.com/watch?v=lfA8pT-1eKM), 2018
    * "Kaufen Sie Toilettenpapier, Taschentücher und Küchenrolle aus Recyclingpapier mit dem **Blauen Engel**."  :)
        * siehe auch [papier](..Konsum/papier.md)
    * "Vergessen Sie nicht, Ihre eigenen Taschen, Beutel, Rucksäcke oder alte Tüten zum Einkauf mitzunehmen."  :)
    * "Spülen Sie Joghurtbecher oder andere Verpackungen nicht aus, sie müssen nur löffelrein sein."  :)

### Leitungswasser
* siehe NABU oben
* vsr-gewässerschutz.de, Januar 2019
    * selbst durchgeführten Analysen zu Brauch- und Brunnenwasser
    * Seite zu Leitungswasser: https://www.vsr-gewässerschutz.de/tipps-tricks/leitungswasser-trinken/
        * Abschnitt "Leitungswasser trinken schont Geldbeutel und Umwelt" Argumente pro Leitungswasser.
        * Abschnitt "Unsere Tipps, um Belastungen sicher auszuschließen" empfiehlt Labor "INDIKATOR" für Wasserproben aus dem Wasserhahn, weil der örtliche Wasserversorger nur bis Hausanschluss verantwortlich ist.

Klimawandel
-----------
* [Der Klimawandel](klimawandel.md)

Die Allmende-Herausforderung
----------------------------
* [die-allmende-herausforderung](nachhaltigkeit.md)

Computer-Nutzung
----------------
* [computer-nutzung](computer-nutzung.md)

Werbung
-------
* [werbung](../Konsum/werbung.md)

Psychologie
-----------
* [psychologie](psychologie.md)

Unsortiert
----------
* [Unsortiert](unsortiert.md)
* [Gedanken zum Copyright, Urheberrecht](copyright.md)
* [Natur](natur.md)
* [Spenden-Atlas](spenden-atlas.md)
* [Wissenschaftliche Praxis](wissenschaft.md)
* [Bedingungsloses Grundeinkommen](../Finanzen/bedingungsloses-grundeinkommen.md)
* [alkohol](alkohol.md)
* [Kinderarbeit](kinderarbeit.md)
* [Buch- und Filmliste](buch-und-film-liste.md)
* [Was tun?](../Allgemein/wastun.md) - Sammlung von positiven und konkreten Handlungsoptionen
* [Workshop](../Allgemein/workshop.md)
