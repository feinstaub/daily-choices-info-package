Hoffnung
========

<!-- toc -->

- [Hoffnung vs. Zuversicht](#hoffnung-vs-zuversicht)
- [Sudan: die vergessenen Löwen](#sudan-die-vergessenen-lowen)
- [Selbstvertrauen durch Sport in Afrika](#selbstvertrauen-durch-sport-in-afrika)

<!-- tocstop -->

### Hoffnung vs. Zuversicht
* ... todo bloch
* Beispiele
    * Zahnarzt (freiwillig) vs. wird schon gehen
    * Blei in der Wasserleitung vs. wird schon gutgehen
    * Impfen vs. wird schon gutgehen
        * Querverweis zur Demokratie: solange es eine gewisse Mindestanzahl macht, geht alles gut
    * Alkohol und Rauchen in der Schwangerschaft vs. wird schon gutgehen
        * Frau ist nikotionabhängig und alkoholabhängig; weiß nicht wie sie davon loskommen soll.
            * Was ist mit ihrer Freiheit schwanger zu werden ein Kind zu bekommen?
            * mögliche Behinderung vs. es wird schon gutgehen und dann "das beste bieten"
    * **Wachstum ist das neue Rauchen**
        * mit blinder Zuversicht rangehen oder nüchtern/rational?
    * ((Exkurs: Rechte des ungeborenen Menschen
        * Beispiel Philippinen (Leben ist heilig, also Pflicht so viel wie möglich zu schaffen?)
            * Sollte es Grenzen geben? Zuversicht, dass schon alles gutgeht
                (wenn nicht für jedes einzelne Kind, dann doch in der Summe)
            ))

### Sudan: die vergessenen Löwen
* https://www.tagesschau.de/multimedia/video/video-661425.html, 2020
    * Sudan arm; eine Folge von Vetternwirtschaft und Korruption
        * lange Schlangen vor der Tankstelle, vor der Bäckerei
        * Hoffnung für die Menschen dank Zusammenhalt, Bodenschätze, Stolz, Solidarität, Zuversicht
    * Man kümmert man sich um noch Schwächere: eingesperrte Löwen im engen Käfig und total ausgemergelt
        * Ziel: raus aus dem Käfig in ein Gehege vor der Stadt

### Selbstvertrauen durch Sport in Afrika
* "Karim - Kigalis Rollerblade-Star | Africa Riding | ARTE", 2019, https://www.youtube.com/watch?v=LSlz7gc1LNU, 7 min
    * Afrika, Ruanda
    * Mut, Selbstvertrauen; Grundlage für ein besseres Leben
