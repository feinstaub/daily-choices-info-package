Langlebigkeit / daily-choices-info-package
==========================================

<!-- toc -->

- [Geplante Obsolszenz](#geplante-obsolszenz)
  * [Einleitung](#einleitung)
  * [Beispiel Waschmaschine](#beispiel-waschmaschine)
  * [Beispiel Smartphone](#beispiel-smartphone)
- [Gängige Praxis](#gangige-praxis)
  * [Apple](#apple)
- [Positive Beispiele](#positive-beispiele)
  * [Smartphone: Fairphone](#smartphone-fairphone)
  * [Smartphone: Samsung Galaxy S II mit Replicant](#smartphone-samsung-galaxy-s-ii-mit-replicant)
  * [Fest verbauten Akku verbieten](#fest-verbauten-akku-verbieten)
  * [Kreislaufwirtschaft, Cradle-to-Cradle](#kreislaufwirtschaft-cradle-to-cradle)
- [Regionale Initiativen](#regionale-initiativen)
  * [Repair-Cafe](#repair-cafe)

<!-- tocstop -->

Prinzipiell ist es aus Resourcenschonungssicht meist besser, auf langlebige Produkte zu setzen.

Geplante Obsolszenz
-------------------
### Einleitung
* Schrot und Korn 07/2017 Seite 31ff: Interview mit Christian Kreiß, einem Professor, der sich einerseits mit von Konzernen gekaufter Wissenschaft beschäftigt (Sachbuch: "Gekaufte Forschung" [1], Artikel zum Buch [2], Einleitungsbeispiel: der ALDI-Süd-Hörsaal, nachrecherchiert unter [3] und [4]). ::aldi

* Auf seiner Webseite [5] beschäftigt sich Christian Kreiß anderseits mit geplanter Obsoleszenz, z. B. am Beispiel Waschmaschine [6].

* Studie „Geplante Obsoleszenz“ vom 20.3.2013 [7], Einleitung:
    - interessante Beispiele gebracht wie der geplante Verschleiß erfunden wurde,
    - warum er damals und heute so gut funktioniert,
    - wie richtig strategisch ermittelt wird, wie stark man als Hersteller die Produktqualität leiden lassen kann, ohne, dass der Kunde etwas merkt
    - und warum er aber für die Allgemeinheit ein Problem ist (siehe griffiges Konzert-Beispiel auf S. 8), das gelöst werden muss.

[1] http://www.europa-verlag.com/buecher/gekaufte-forschung/
[2] https://www.heise.de/tp/features/Missbrauchte-Wissenschaft-3373644.html
[3] http://www.deutschlandradio.de/bueffeln-im-aldi-sued-hoersaal.331.de.html?dram:article_id=235351
[4] http://www.esb-business-school.de/nc/fakultaet/aktuelles/detail/artikel/einweihung-des-aldi-hoersaals-am-12-april-2016/
[5] http://menschengerechtewirtschaft.de/
[6] http://menschengerechtewirtschaft.de/geplanter-verschleiss
[7] http://menschengerechtewirtschaft.de/studie-geplante-obsoleszenz-vom-20-3-2013 und http://menschengerechtewirtschaft.de/wp-content/uploads/2013/04/Studie-Obsoleszenz-BT-GRUENE-vorabversion1.pdf

### Beispiel Waschmaschine
* Sich beim lokalen Elektrohändler des Vertrauens informieren, welche Hersteller und Maschine er empfiehlt
    * Kriterien:
        * Langlebigkeit
        * Reparierbarkeit durch unabhängige Fachbetriebe nach Ablauf der Garantiezeit

### Beispiel Smartphone
* siehe unten, z. B. Fairphone
* Kaum Nachhaltigkeit, Beitrag über "Start-Up Unternehmen, dass dagegen etwas tun will" (https://www.ardmediathek.de/ard/player/Y3JpZDovL3dkci5kZS9CZWl0cmFnLWU3ZmEzMmEwLWYwMWItNDNkNy05NTk2LWEzNTMzYWZhZmUxMg/, "Kaum Nachhaltigkeit bei Smartphones, 17.01.2018 WDR aktuell")
    * Handlungsempfehlung: möglichst lange verwenden

Gängige Praxis
--------------
### Apple
* ...dafür bekannt, die durch seine Größe erwachsene Verantwortung nicht wahrzunehmen und seine Position sogar wiederholt zu missbrauchen. Übersicht von Medienartikeln, die das regelmäßig belegen: http://stallman.org/apple.html

* siehe auch:
    * [computer](../Computer) (Lobbyarbeit gegen Recht auf Reparieren), tp64/repaircafe
    * Zensur: ["Apple entfernt beliebte VPN-Apps aus seinem iOS-Store in China"](https://www.heise.de/newsticker/meldung/Apple-entfernt-beliebte-VPN-Apps-aus-seinem-iOS-Store-in-China-3786513.html), 2017
        * vergleich auch [Apple-Werbung von 1984](https://www.youtube.com/watch?v=2zfqw8nhUwA), 1 min

* Allgemein: Dass die Mehrheit der Verbraucher sich langlebigere Geräte wünschen, hat der [BUND schon 2013 ermittelt](https://www.heise.de/newsticker/meldung/BUND-Verbraucher-im-Umweltbewusstsein-den-Computerherstellern-voraus-1816863.html).

* [consumerism](https://www.youtube.com/watch?v=v-7v2WGiTe8), 2016, 4 min, Buy a new Y-Fone, Apple-Werbung, artwork.md
* YT: "It's our world (Steve Cutts / Yann Tiersen) FullHD 1080p", 2018, 3 min, artwork.md

Positive Beispiele
------------------
### Smartphone: Fairphone
* Robustes Design: https://www.fairphone.com/de/unsere-ziele/design/
* Ersatzteile: https://wiki.lineageos.org/devices/#fairphone
* LinageOS-Kompatibel: ttps://wiki.lineageos.org/devices/#fairphone
* Fair: https://www.fairphone.com/de/2017/11/15/closer-look-efforts-improve-cobalt-sourcing/

### Smartphone: Samsung Galaxy S II mit Replicant
* https://de.wikipedia.org/wiki/Samsung_Galaxy_S_II
    * hergestellt 2011
    * offiziell scheint der Hersteller-Support im März 2013 mit Android 4.1.2 zu einem Halt gekommen zu sein
* Lösung:
    * Stand 2017: Dank freier Software kann das Telefon mit einem relativ aktuellen Android 6.0 (siehe [Replicant 6.0](https://blog.replicant.us/2017/05/replicant-6-0-released/) weiter betrieben werden. Also 6 Jahre nach Einführung immer noch funktionstüchtig.
    * Online gebraucht gestellbar: https://tehnoetic.com/tehnoetic-s2-phone-replicant

### Fest verbauten Akku verbieten
* 2018: ["US-Gesetzentwurf für das Verbot von fest verbauten Akkus "](https://www.heise.de/newsticker/meldung/US-Gesetzentwurf-fuer-das-Verbot-von-fest-verbauten-Akkus-3952668.html)
    * u. a. Recht zum Reparieren, siehe auch User-Comments

### Kreislaufwirtschaft, Cradle-to-Cradle
* ["Studien fordern Wandel zu Kreislaufwirtschaft"](https://www.recyclingnews.info/recycling/studien-fordern-wandel-zu-kreislaufwirtschaft/), 2016
    * https://de.wikipedia.org/wiki/Kreislaufwirtschaft
        * [Crade-to-Cradle, C2C](http://c2c-ev.de/), seit 2012
            * [Michael Braungart](https://de.wikipedia.org/wiki/Michael_Braungart), Verfahrenstechniker und Chemiker, mit Interview
            * Doku: ["planet e. - Eine Welt ohne Müll"](https://www.zdf.de/dokumentation/planet-e/planet-e-eine-welt-ohne-muell-100.html), ZDF 2017, positive Beispiele, u. a. Cradle-to-Cradle
                * ... todo ...

Regionale Initiativen
---------------------
### Repair-Cafe
* Liste der deutschen Repair-Cafes: http://repaircafe.org/de/
