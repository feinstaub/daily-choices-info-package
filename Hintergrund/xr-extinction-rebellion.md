XR - Extinction Rebellion
=========================

<!-- toc -->

- [Inbox 2020](#inbox-2020)
  * [Human attitudes, Copeland](#human-attitudes-copeland)
  * [Planetary Boundaries - Will Steffen - Earth System Trends](#planetary-boundaries---will-steffen---earth-system-trends)
  * [Videos](#videos)
  * [Mattermost](#mattermost)
  * [XR Köln](#xr-koln)
  * [XR Heidelberg, Zement = Klimakiller](#xr-heidelberg-zement--klimakiller)
- [Infos](#infos)
  * [Vorträge](#vortrage)
  * [What Truth?](#what-truth)
  * [Analogie - Patient geht zum Arzt](#analogie---patient-geht-zum-arzt)
  * [News and Content](#news-and-content)
  * [Annemarie Botzki](#annemarie-botzki)
  * [Roger Hallam](#roger-hallam)
  * [Rupert Read](#rupert-read)
  * [Jem Bendell](#jem-bendell)
  * [Gail Bradbrook](#gail-bradbrook)
  * [Paddy Loughman](#paddy-loughman)
  * [Christiana Figueres / The Future we choose / 10 Things](#christiana-figueres--the-future-we-choose--10-things)
- [Videos](#videos-1)
  * [UK - Fernsehen](#uk---fernsehen)
  * [UK](#uk)
  * [Deutschland](#deutschland)
- [Entwicklungen](#entwicklungen)
- [Fridays for Future](#fridays-for-future)
  * [scientists4future](#scientists4future)
  * [Gerechtigkeit als Thema - Welzer](#gerechtigkeit-als-thema---welzer)
  * [Harald Welzer / ::soziologie](#harald-welzer--soziologie)
  * [Gesellschaftsutopien](#gesellschaftsutopien)
  * [Verschiedenes](#verschiedenes)
  * [Was-ist-so-schlimm-am-Kapitalismus](#was-ist-so-schlimm-am-kapitalismus)
  * [Prof. Dr. Maja Göpel](#prof-dr-maja-gopel)
  * [IG Metall](#ig-metall)
- [Zusammenstellung Basisinfos](#zusammenstellung-basisinfos)
  * [Wann kommt denn nun die Krise?](#wann-kommt-denn-nun-die-krise)
  * [2019](#2019)
- [Weitere](#weitere)
  * [Niko Paech](#niko-paech)
  * [Charles Eisenstein](#charles-eisenstein)
  * [Artensterben: Andreas Hetzel - Sozialphilosoph, Soziologe](#artensterben-andreas-hetzel---sozialphilosoph-soziologe)
  * [Artensterben - Extermination](#artensterben---extermination)
  * [Genügsamkeit 1](#genugsamkeit-1)
  * [Genügsamkeit 2](#genugsamkeit-2)
  * [Genügsamkeit 3](#genugsamkeit-3)

<!-- tocstop -->

Inbox 2020
----------
* https://extinctionrebellion.de/wer-wir-sind/faq/

### Human attitudes, Copeland
::lebensstil

* "Climate Change: We Do Not Have the Luxury of Time", Sebastian Copeland, 2019, https://twentythirty.com/climate-change-sebastian-copeland/
    * "Sebastian Copeland has led numerous expeditions to the Arctic and Antarctic to document these disruptions"
    * "what roles public opinion and technology can play in battling it"
    * ...
    * "Climate change is weakening our democracies" ::demokratie
    * ...
    * "The greatest challenge is with human attitudes" ::human_attitudes
        * "Borne out of the Age of Enlightenment, we cultivate the naive ideology that we are the Earth’s ruling class.
            In modern times, this has fostered a delusion that we can bend physics to our needs,
            and engineer our way out of universal rules.
            However clever we are, on this planet at least, you can challenge gravity all you want,
            but it remains that if you drop a brick on your foot, you will get injured."
        * "We will not mitigate the worst impact of climate change without **re-shaping our attitudes.**"
        * "there are three agents to systemic and lasting change: the public and private sectors, **and public opinion**."
            * "each can only move as fast as the slowest one"
            * "But the **single most effective way to institute change is education.**"
        * "**If science fails to teach us** that we **must live in harmony with Nature**, then it has taught us nothing."
        * wo? / Welzer
            * **tun so, als könnten wir mit der Natur verhandeln**; oder ihr mit Geld etwas abkaufen
            * als könnten wir uns von von der Natur emanzipieren; als könnten wir mit Technik alles lösen
            * Natur als Eigenwert betrachten UND als unverhandelbare Lebensgrundlage (so wie uns die Wissenschaft und auch die Religion lehrt)
            * Vor über 50 Jahren haben wir **das Experiment gestartet**, was wohl passiert, wenn wir versuchen in einem
                endlichen System unendlich zu wachsen. Unser rationales Denken weiß bereits seit 50 Jahren, wie das Experiment ausgehen muss.
                Das Experiment hat unbestreitbar alle Vorteile der modernen Zivilisation hervorgebracht. Wir sind als Gesellschaft in einer einzigartigen Wohlstandssituation.
                Was wir nun brauchen, ist Mut, einzugestehen, dass diese Vorteile teuer und auf Kosten der - mittlerweile absehbaren - Zukunft erkauft wurden.
                Mut, sich die existenzielle Abhängigkeit von der Natur einzugestehen.
                Mut, unsere Umwelt als Wert an sich anzusehen.
                Mut, nun **anzufangen, neue Experimente** zu wagen, die diese Tatsachen nicht mehr ängstlich ausblenden,
                    sondern Bilder einer erstrebenswerten Zukunft vorzeichnen
    * ...
    * "Of the 120 million companies existing in the world today, only one hundred contribute to 71% of global emissions"
        * "Just five of those have collectively spent one billion dollars in counter information messaging
            since 2016 **to discredit the findings of the Paris agreement** and promote the illusion that there is a legitimate debate on climate change."
        * https://www.theguardian.com/sustainable-business/2017/jul/10/100-fossil-fuel-companies-investors-responsible-71-global-emissions-cdp-study-climate-change
        * https://www.theguardian.com/environment/2019/oct/09/revealed-20-firms-third-carbon-emissions
    * ...

### Planetary Boundaries - Will Steffen - Earth System Trends
* https://de.wikipedia.org/wiki/Will_Steffen
* The Great Acceleration
    * siehe Graphik: http://old.futureearth.org/blog/2015-jan-16/great-acceleration
        * siehe auch Buch von Lesch
    * **Socia-Ecnomic Trends**
        * World population
        * Foreign direct investment
        * Primary energy use
        * Fertilizer consumption
        * Large dams
        * Water use
        * Paper production
        * Transportation
        * Internationl Tourism
    * **Earth System Trends**
        * CO2
        * Methan
        * Oberflächen-Temperatur
        * Ozean-Versauerung
        * Meeresfischfang
        * Shrimp-Aquakultur, Shrimps
        * Regenwaldverlust
        * ...
* Video: "The Big U-Turn Ahead: Calling Australia to Action on Climate Change", https://www.youtube.com/watch?v=OzQsjuzr3_M, 2019, 50 min
    * TODO
    * ...
    * ...
    * Deutsche Version: "Will Steffen - Die große Wende voraus", https://www.youtube.com/watch?v=esF6bl2H5x0, 2020, 50 min
* Video: "TEDxCanberra - Will Steffen - The Anthropocene" / **Anthropzän**, https://www.youtube.com/watch?v=ABZjlfhN0EQ, 2010, 20 min
    * ...

### Videos
* "Clare Farrell | Oxford Union | We Should NOT Have Hope For The New Decade | Extinction Rebellion" https://www.youtube.com/watch?v=cTfg5iSMcxs, 2020
    * ...
    * ...
    * ...
    * YT comments
        * aber die Kinder: "People with nothing to say will always revert to blubbering about "the poor children". The suggestion is we should lie to them. I've worked with children with life-limiting conditions and NOBODY ever felt lying to them was acceptable. **Children are not a different species.** As Greta and her striking children show us, they are often way ahead of the adults in intelligence and awareness"
* "[XR Göttingen 26.01.2020] Vortrag "Aufstand oder Aussterben" im Jungen Theater Göttingen", 1h 40 min, https://www.youtube.com/watch?v=lnPAL4TIAzQ
    * Psychologe
    * Lehrerin
    * ...
    * ...
* "[XR Oktober 2019] Kira Petersen über gewaltfreie Macht", 2 min
* "'We Would Break The Law To Save The Planet' | Dr Gail Bradbrook | Oxford Union Debate", https://www.youtube.com/watch?v=_3IYq1cpIX4, 11 min

### Mattermost
* https://taz.de/Programmierer-ueber-Umweltbewegung/!5654010/ - Julian Oliver
    * "Programmierer über Umweltbewegung - „Privatsphäre stiftet Zusammenhalt“"
    * https://criticalengineering.org/ - "The Critical Engineering Working Group"

### XR Köln
* https://twitter.com/colognexr
* https://twitter.com/Jumpsteady/status/1221197798068162560
    * "Frank Schätzing fasst die Tragweite und Dringlichkeit der #Klimakrise sehr gut in nur zwei Minuten zusammen und fordert:
        Schluss mit dem Ziel der Klimaneutralität 2050. Das reicht nicht.
        Wir müssen das weltweit bis 2040 schaffen, um die Erwärmung noch unter 2°C halten zu können." mit Video, 2 min

### XR Heidelberg, Zement = Klimakiller
* https://extinctionrebellion.de/og/heidelberg/r%C3%BCckschau/klimakiller-hdc/
* https://extinctionrebellion.de/og/heidelberg/hd-cement/
    * "Mitten in Heidelberg sitzt ein Konzern, der weltweit gegen Menschen- und Völkerrecht verstößt.
        Ein Konzern, der als weltweit zweitgrößter Zementhersteller beispielhaft für die Umweltzerstörung und die gigantischen Treibhausgasemissionen der Zementindustrie steht"
    * "Zement vs. Mensch und Umwelt"
    * Situation in anderen Ländern
    * Alternativen
    * Ad-Busting
    * Viele Quellen
    * 2016: "Der Chef der deutschen HeidelbergCement will an Trumps Mauer zu Mexiko mitverdienen" (spiegel.de)

Infos
-----
* https://extinctionrebellion.de
* https://de.wikipedia.org/wiki/Extinction_Rebellion#Offene_Briefe, siehe Basisinfos unten

### Vorträge
* Clare Farrell
    * https://rebellion.earth/act-now/, https://rebellion.earth/the-truth/
        * https://www.youtube.com/watch?v=2sUlD9ZEifo, 30 min
            * with reference to "Freedom Riders" (see there)
    * https://www.youtube.com/watch?v=5D-l_8mUxHA - Interview
* "Aufstand oder Aussterben? Die Bewegung "Extinction Rebellion"", Berlin, 2019
    * https://www.youtube.com/watch?v=eopJw91cxL0
        * ...
* "Heading for extinction and what to do about it / What to do? Responses... emotional and practical ones"
    * https://www.youtube.com/watch?v=b2VkC4SnwY0, 50 min, 2018
        * von Biologin
        * ...

### What Truth?
* Für Einsteiger und Zweifler: Scientists for Future: https://www.scientists4future.org/stellungnahme/fakten/

### Analogie - Patient geht zum Arzt
* Der erste Arzt heißt Albert Einstein (stellvertretend für Physik und Naturwissenschaft) und stellt seine Diagnose: "Sie haben einen Hirntumor. Unbehandelt werden Sie nach der bisherigen Datenlage vermutlich noch so und so lang leben und werden dabei beständig steigende Einschränkungen haben. Die infrage kommenden Therapien haben Risiken (...), versprechen aber folgende Chancen: ..."
* Der zweite Arzt heißt Nosferatu (oder Jörg Meuthen) und sagt: "Glauben Sie mir, die regelmäßigen Kopfschmerzen und der blutige Auswurf sind kein Grund zur Sorge. Der andere Arzt will Ihnen nur Angst machen, weil er und die Industrie mit der Therapie Geld verdienen will. Woher will er überhaupt wissen, was die Zukunft bringt? Genießen Sie das Leben, lachen Sie viel, machen Sie regelmäßig Sport, ernähren Sie sich gesund und vor allem: bleiben Sie kritisch. Nehmen Sie derweil Kopfschmerztabletten und Taschentücher für Ihre Symptome."
    * (die Grundlagen der Physik werden zwar anerkannt, aber unbequeme Ableitungen daraus nicht; die Klimawissenschaft kann also nicht seriös sein; sie will nur Angst machen)

### News and Content
* https://rebellion.earth/2019/07/25/newsletter-26-ice-cold-sip-of-rebellion/
    * "Strategy ideas pamphlet, ‘Truth and its consequences: A memo to fellow rebels on smart strategy, and on soul’. Written independently by Rupert Read, a member of XR UK Political circle. Note: this is not an official XR UK strategy document."
        * http://rupertread.fastmail.co.uk/XR%20pamphlet%20%20%20%20%20%20Truth%20and%20its%20consequences.pdf
    * https://blog.usejournal.com/the-real-problem-of-hypocrisy-for-extinction-rebellion-4a6851dcdeb7
        * "There is a standard response to accusations of hypocrisy that is apt enough here. The private choices of XR protesters do not make any difference to whether or not what they’re saying is true. And it is true. The scientific evidence suggests a strong chance of catastrophic global heating if we do not cut carbon emissions and ecosystem destruction radically and fast."
        * "XR can do better than the standard response. The most important point is this. There is no hypocrisy. Driving to XR protests, or using vinyl banners, or eating a Pret sandwich at an XR road block — these are not hypocritical actions. Hypocrisy is a matter of preaching one thing but practising another. But what XR preaches is a radical change of the system within which we must make our choices, not of the choices we make within the system as it stands. The rebels therefore practise strategic, non-violent, disruptive civil disobedience designed to bring about system change, not changes of private choice. And it is a core Extinction Rebellion principle that individuals are not to be blamed or shamed for their choices within the system as it stands."

### Annemarie Botzki
* "Annemarie Botzki, 32, ist eine der Sprecherinnen der Klima- und Artenschutzbewegung Extinction Rebellion,
    die auch in Deutschland immer mehr Zulauf hat. Die gebürtige Oberhausenerin hat Sozialwissenschaft,
    Europäische Politik und Umweltmanagement studiert, war als Reporterin in Brüssel und London tätig und arbeitet heute in der Solarenergiebranche.
    Sie ist eine der Herausgeberinnen des deutschen Protesthandbuchs «Wann, wenn nicht wir?»."
    * https://www.nzz.ch/international/deutschland/extinction-rebellion-im-interview-demokratiefeinde-oder-harmlos-ld.1514147, 2019
        * "Wahlen sind dem Wesen nach aristokratisch. Sie haben immer schon diejenigen begünstigt, die von vornherein wohlhabend und gebildet waren.
        Was aber nicht heisst, um das noch einmal ganz deutlich zu sagen,
        dass wir den Bundestag abschaffen wollen. Nein! Wir wollen ihn ergänzen.
        Wir wollen die deutsche Demokratie besser machen."

* "Werden wir radikal – Annemarie Botzki (XRebelllion) #Live aus dem Buchladen", CORRECTIV - https://www.youtube.com/watch?v=NEXN7qnj_sI, 2019
    * ...
    * über: Bürgerinnen-Versammlung
    * über: Arbeitsplätze
    * ...
    * 40 min: Bürgerinnen-Versammlung; was ist eigentlich Demokratie (nicht nur Wahlen)
        * aus dem Publikum: https://www.mehr-demokratie.de
            * https://www.mehr-demokratie.de/ueber-uns/profil/
                * "Wenn wir aufhören, die Demokratie zu entwickeln, fängt die Demokratie an aufzuhören."
            * Roman Huber - Geschäftsführender Bundesvorstand
            * https://www.mehr-demokratie.de/buergerrat/
                * "Der Bürgerrat Demokratie hat am 15. November 2019 sein Bürgergutachten mit 22 konkreten Vorschlägen zur Stärkung der Demokratie an Bundestagspräsident Wolfgang Schäuble überreicht. Das Bürgergutachten wurde von 160 aus den Einwohnermelderegistern gelosten Menschen auf Grundlage von Vorträgen und Diskussionen mit Experten erarbeitet. Hier ist der gesamte bisherige Prozess dokumentiert."
                * "„Unsere bewährte repräsentative Demokratie soll durch eine Kombination von Bürgerbeteiligung und Volksentscheiden auf Bundesebene ergänzt werden“, mit dieser Empfehlung gibt der Bürgerrat eine klare Antwort auf die im Koalitionsvertrag festgehaltene Fragestellung, zu der die große Koalition eine eigene Expertenkommission versprochen hat."
                * "Gemeinsam mit der Schöpflin Stiftung veranstalten wir mit den unabhängigen Prozessbegleitungs-Instituten nexus und IFOK einen Bürgerrat zum Thema Demokratie. Mit diesem für Deutschland bisher einmaligen Modell-Projekt erarbeiten per Zufallsauswahl ermittelte Menschen in enger Anbindung an die Politik Lösungen zur Stärkung und Weiterentwicklung unserer Demokratie."
    * todo ... weiter 46 min ...

### Roger Hallam
* https://www.rogerhallam.com
    * "Common Sense For The 21st Century", Booklet
        * https://de.wikipedia.org/wiki/Overton-Fenster
        * ...
    * BBC Hard Talk, 2019

* YT: "Tracey Mallaghan & Roger Hallam | Stroud, England | December 2020 | Burning Pink Party UK", 19. Dez 2020; siehe notes
* YT: "The Three Most Dangerous Ideas | Roger Hallam, Trafalgar Square | July 2020", 7 min
    * Burning Pink Party UK
    * 1. **to do the right thing**; regardless of who follows you; pursuing success will fail
    * 2. idea of **love**; pursue the well-being of each other; a revolutionary idea; act of love: you will not destroy the next generation / betray the people
    * 3. the right of revolution (neumodisch: system change); **no government has the right to kill its people**
* YT: "XR Swansea presents Roger Hallam | Extinction Rebellion UK", 2020, 40 min, **major speech**
* YT: "Pivoting to the Endgame | Roger Hallam | Extinction Rebellion UK", 2020, 1h 20min
    * ...
    * climate change: 1. is affecting everything, 2. existential, 3. exponential; trinity from hell; nightmare; 4. not everything is happend yet
    * rest good?
* Roger Hallam: https://www.youtube.com/watch?v=pZKrVFFjkBs
    * "Roger Hallam: [...] An analysis of the UK April Rebellion 2019 | Extinction Rebellion"
* "If we don't work together, we are going to die together" - Roger Hallam | Extinction Rebellion - https://www.youtube.com/watch?v=9XKTA1cSu1E, 18 min, 2019
    * Movement of Movements
* 2019 "Roger Hallam | Hypocrisy | Extinction Rebellion", 14 min, https://www.youtube.com/watch?v=gjbLr_gyOgk
    * everyone is welcome in the movement (rich, frequent flyer, driving car)
    * Naivität: glauben, dass Menschen sich ändern können (vs. Zynismus => don't actually do anything)
        * Ein gewisses Maß an Naivität ist notwendig, um zu mobilisieren.
        * Wenn man die Probleme zwar anerkennt, aber zynisch ist ("ist halt so", "es war schon immer ungerecht"), dann macht keiner was
        * let's be naive; that makes you vulnerable but it means people can come in
    * "no naming and shaming"
        * "the biggest reason why people don't get involved with social movements because they are scared of being called out"
        * welcome, small talk, ...
        * not judging people
        * what you have done in the past, is not relevant because it is past
        * important is the behaviour: how do you treat people here and now? (kind, listening to people, ...)
    * all hypocritical => greater humility (Bescheidenheit/Demut) => do the best we can (not "be THE best")
        * come in from a humble point of view: I am not that great but I want to contribute
    * spiritual aspect is important
        * ...
    * verwundbar => Angst => Angst überwinden => Handeln
    * open movement, come in, make mistakes, do what you wanna do, go setup your own team,
        make sure people respect each other - that's enforced - and if not then set up a new group
        * don't be jugdemental

### Rupert Read
* "CLIMATE CATASTROPHE: The Case For Rebellion | Dr Rupert Read Talk": https://www.youtube.com/watch?v=RnonKverhOg, 2019, 45 min
    * ...
    * Transformation nötig, siehe auch Maja Göpel
    * ...
    * 7 things to do
    * ...
    * 18 min: Wake up. Freak out. Not only indivdually but also friends and families. The more the better.
        * Need to concentrate. What to give up? What to do now rather than later?
    * Transformative Adaptation
        * compared to shallow adaptation within the current system (e.g. building higher dikes) which produces even more CO2
        * prepare for constant changes
        * may fail
    * Deep adaptation
        * nuclear power? must think what happens in case of collapse events might occur, e.g. in Russia because of some collapse all power stations are abandoned, melt down, nuclear waste
            * think now how to make existing stations safe and not even build more
    * XR: room for anger, fear, despair and grief
        * feel these emotions but don't get stuck in it
        * 27 min: then: rebel
            * which does not mean to stop enganging in the other things (campaigning, lobbying, make politics, vote green etc.). Those are needed, too.
    * 28:30 6000 people non-violent civil disobedience UK bridges
    * XR demands
        * gov should tell the truth
        * emissions down to zero by 2025
            * power plants have to be shut down
            * cars melted down to something different
            * you want to live in caves with candles? (boring, it is always the same: caves and candles)
            * lots of existing infrastructure will have to go
            * industrial growth society as we know it is at an end
                * the question is how is ended (enraged nature or by us voluntarily)
        * conventional democracy has failed
    * why not say to your children you don't have to go to school every single day where you learn things you don't need because society is likely to collapse
    * 35 min "6. We need to talk."
        * process the situation emotionally, rationally, practically
        * let it not stuck in your head
        * Angst, dass Leute sich abwenden oder ärgerlich werden, wenn man über die Probleme in Klartext spricht
        * so important, not be positive all the time, get the sad message across
            * people will be relieved that we can be honest about the situation and talk better about it
            * start talking about it among ourselves
        * 36:45: "There is an abyss opening up before us. It challenges everything we thought we knew about our culture and nature. We need to look into it and concentrate on what we can see" (Kingsnorth)
    * 37:10: "7. Stop. Pause."
    * one of the powers that might arise with XR that we can do extraordinary things. And something spiritual.
        * 38:40: picture of lion, look into the abyss
        * ...
    * ...

* "Shed A Light: Rupert Read – This civilisation is finished: so what is to be done?" - https://www.youtube.com/watch?v=uzCxFPzdO0Y, 2018
    * ...

* "Dr Rupert Read - The Uncertain Situation We Are In | Extinction Rebellion": https://www.youtube.com/watch?v=P4uckj3dbUU, Feb 2019, 1h 30min
    * ...

### Jem Bendell
* https://jembendell.com/2019/07/31/climate-scientist-speaks-about-letting-down-humanity-and-what-to-do-about-it/, 2019
    * "Climate scientist speaks about letting down humanity and what to do about it"
    * Dr Wolfgang Knorr
    * ... TODO
* https://www.lifeworth.com/deepadaptation.pdf "Deep Adaptation: A Map for Navigating Climate Tragedy"
    * insbesondere darum, wie man sich und die Bevölkerung sinnvoll auf einen bevorstehenden Zusammenbruch mental vorbereitet

### Gail Bradbrook
* About economics: "Gail talks '21st Century Economics' with Dr Kate Raworth & Prof Graham Smith | Extinction Rebellion", 2019, 12 min
    * TODO
    * Basic: "There ARE alternatives"
    * ...
    * ...
    * -> Klimacamp Berlin
        * Dr. Steffen Lange
            * https://www.ioew.de/das-ioew/mitarbeiter/dr-steffen-lange/
                * "Trotz erheblicher Effizienzfortschritte in den letzten 40 Jahren ist in Deutschland der absolute Verbrauch an Energie und Ressourcen nicht im entsprechenden Maße gesunken. Oft wird dies mit dem Rebound-Effekt begründet, also damit, dass eine..."
                * "Arbeit, Wachstum, Postwachstum"
                * "Wie entkommen wir der Abhängigkeit vom Wirtschaftswachstum?"

### Paddy Loughman
* "Extinction Rebellion's Paddy Loughman - Facing Change: Can we grasp the promise in the peril", https://www.youtube.com/watch?v=ZSZQMJg6Z-Y, 45 min, 2020
    * former innovation brand growth consultant
    * 1 min: James Baldwin: "Not everything that is faced can be changed...But nothing can be changed until it is faced"
        * https://www.competitivedge.com/not-everything-faced-can-be-changedbut-nothing-can-be-changed-until-it-faced
            * "If you have a fear, a problem, weakness or shortcoming, personally or professionally,
                you will never be able to get beyond it until you're willing to honestly face it!"
            * ...
    * 8:45: schöne Temperatorgraphik bis tief in die Vergangenheit
    * 9:00: Climante AND ecological crisis
        * e.g. Wald
        * e.g. Biodiversität, 6. Massensterben (Tieren und Pflanzen)
            * = Stabilität
    * 10:00: Change our minds
        * Ecology = Commodity
        * Ecology = **Community**, (siehe auch Eisenstein)
    * 11:00 since 30 years nothing has been done
    * 13:00 carbon bubble
    * work for **system change**
    * Instead of "carbon emissions" -> "carbon consumption" (includes imported products)
    * 15:00 like Corona: "It's all of us or none of us"
    * **Waking up is hard**
        * Status Quo bias           - easier to keep things same
        * Consumer fantasy          - someone else will fix it
        * Optimism bias             - it probably won't happen
        * Hyperbolic discounting    - present me beats future me
        * Illusory truth effect     - repeated lies create beliefs
        * Backfire effect           - **your facts make my belief stronger**
    * 16:00: Move from Story to **Setting**
        * ...
    * 17:00 ca.: wie es einem Kind geht, das heute geboren wird bei verschiedenen Temperaturen und auch was heute schon los ist
    * 22:00 Darwin: der, der am besten mit Change umgehen kann, gewinnt
    * 23:00 über notwendige Änderungen in der Wirtschaft / Economy
        * ...
    * ...
    * ... TODO
    * ...

### Christiana Figueres / The Future we choose / 10 Things
* Buch_: https://www.penguinrandomhouse.com/books/623543/the-future-we-choose-by-christiana-figueres-and-tom-rivett-carnac/
* https://www.forbes.com/sites/jeffmcmahon/2020/02/24/former-un-climate-chief-calls-for-civil-disobedience/, 2020
    * "It’s time to participate in non-violent political movements wherever possible,
        Christiana Figueres writes in “The Future We Choose: Surviving the Climate Crisis”
    * https://www.forbes.com/sites/jeffmcmahon/2020/02/25/10-things-you-can-do-about-climate-change-starting-in-your-head/, 2020
        * "10 Things You Can Do About Climate Change, According To The Shepherds Of The Paris Agreement"
        * "a book that shepherds climate activism from changing mental states to changing the world"
        * "found that what we do and how we do it is largely **determined by how we think**" (note: but also vice versa)
            * "chances of success are predicated on our attitude toward that very challenge"
        * "recommend a mindset for climate activism
            that rests on three attitudes: **radical optimism, endless abundance and radical regeneration**"
            * radical optimism - "combat pessimism and denialism"
            * endless abundance - "sense that there are resources enough for all, to combat competitiveness and tribalism"
            * radical regeneration - "caring for both nature and oneself, to combat exploitation and burnout"
        * "For change to become transformational, our change in mindset must manifest in our actions."
        * Doing What Is Necessary
            * "**1. Let Go Of The Old World**"
                * "honor the past—for example, it’s okay to acknowledge that fossil fuels have improved quality of life, for some—and then let the past go."
                * "not only pragmatic change like allowing offshore wind development"
                * but also: "psychological change like resisting the urge to engage in tribalism and the illusion of certainty"
            * "**2. Face Your Grief**"
                * "but hold a vision of the future", siehe auch "Gesellschaftsutopien"
                * "“We cannot hide from the grief that flows from the loss of biodiversity and the impoverished lives of future generations,”"
                * "advise readers to face this grief, rather than turn away from it—an approach
                    that borrows from their Buddhist influences—and then to embrace an optimist vision of the future."
                * “A compelling vision is like a hook in the future.
                    It connects you to the pockets of possibility that are emerging
                    and helps you pull them into the present.”
            * "**3. Defend The Truth**"
                * "But they also urge readers not to vilify those who embrace denialism:"
                    “If you reach them, it will be because you sincerely listened to them and strove to understand their concerns.
                    By **giving care, love, and attention to every individual**, we can counter the forces pulling us apart.”"
            * "**4. See Yourself As A Citizen**"
                * not as consumer
                * "depart from the usual approach of urging people to stop buying stuff"
                * "Instead, they focus on the psychology behind consumption.
                    “Much of what we buy,” they say, “is designed to enhance our sense of identity.”
                    Instead, they say, envision a good life that does not depend on material goods"
            * "**5. Move Beyond Fossil Fuels**"
                * fossil-fuel reliance is an attachment to the past
                    * “Only when this mindset is challenged can we migrate our thinking, finances, and infrastructure to the new energies.”
            * "**6. Reforest The Earth**"
                * "plant trees, let natural areas go wild, eat less meat and dairy,
                    boycott products that contribute to deforestation."
                    * e. g. palm oil (Palmöl)
                * "emphasizing the benefits of a plant-based diet"
            * "**7. Invest In A Clean Economy**"
                * "moving beyond a model of economic growth that rewards extraction and pollution,
                    toward “**a clean economy that operates in harmony with nature**, repurposes used resources
                    as much as possible, minimizes waste, and actively replenishes depleted resources.”"
            * "**8. Use Technology Responsibly**"
                * A.I. to solve problems like how to move to a circular economy
            * "**9. Build Gender Equality**"
                * “Women often have a leadership style that makes them more open and sensitive
                    to a wide range of views, and they are better at working collaboratively,
                    with a longer-term perspective. These traits are essential to responding to the climate crisis.”
            * "**10. Engage In Politics**"
                * "Mentioning Greta Thunberg, Extinction Rebellion, Gandhi, Martin Luther King Jr., and Nelson Mandela,
                    Figuera and Rivett-Carnac urge civil disobedience.
                    “**Civil disobedience** is not only a moral choice, it is also the most powerful way of shaping world politics.”"

Videos
------
### UK - Fernsehen
* "Are Extinction Rebellion Protesters Heroes or Villains? | Good Morning Britain", 2019, 12 min
    * Criminals in London vs. police resource usage by protests
    * ...
    * ehemaliger Policeman: Paul Stephens
    * (blocking meat: https://www.theguardian.com/environment/2019/aug/16/animal-rebellion-extinction-activists-to-blockade-smithfield-meat-market-environment)
    * moderator: thinks climate change is real but has issues with the XR way
    * politic women: says the government is doing enough
        * good response
        * government does not hear its own advisors
    * "every footprint counts"
        * hypocrite
        * sober response
    * ...

* Jonathan Pie - Hypocrite
    * https://www.facebook.com/JonathanPieReporter/videos/1425045757661077/
    * "When it comes to the climate crisis you are either a hypocrite or an arsehole. The time has come to decide which you'd rather be."
    * disruptive protest vs. disruptive ecological breakdowns

* BBC News | Clare Farrell | 17.10.2019 | Extinction Rebellion, 2019, 8 min
    * Clare Farrell Interview

* "Have Extinction Rebellion Protests Gone Too Far? | This Morning", 2019, 12 min
    * Sarah Lunnon, XR
    * sober discussion about activists on the roof of the train
    * ...

* Is the public losing patience with Extinction Rebellion protests?, 2019, 3 min
    * right reasons, wrong way

* "BBC Newsnight | Sarah Lunnon | Extinction Rebellion", 2019, 8 min
    * Sarah Lunnon, XR
    * Climate scientists, IPCC
    * Are the XR numbers scientifically correct? -> basically yes
    * ...

* "ABC's 'The View' | 21.10.2019 | Extinction Rebellion", 2019, 3 min
    * with Benedict Cumberbatch, Michael Shannon, Whoopi Goldberg

### UK
* "Who We Are | Why XR? | How You Can Help | Extinction Rebellion" - https://www.youtube.com/watch?v=pTR1JqjbD3Q, 2019
    * ...
    * do what is needed: in your family, with your friends, in your company
    * ...
* "How Extinction Rebellion was Born | CNN | Extinction Rebellion" - https://www.youtube.com/watch?v=2P6UJyaeT4M, CNN, 2019
    * ...
    * Wirkung der Proteste; Leute sagen: geh zum Friseur, geh erst mal arbeiten
        * sind die Protestierenden arme Hippies? Oder privilegierte middle-class-Leute? Das passt nicht ganz
        * Wenn man nach vorne tritt, wird man selber genau geprüft und nicht der Gegenstand des Protestes
    * ...
    * important people in the back-office
    * ...
    * Simon Bramwell (co-founder):
        * fundamentale Änderungen: wesentlich einfacheres Leben hier und in anderen Teilen der Welt bleaker
    * Disruption, weil
        * das "Business-as-Usual"-Motto ist sehr destruktiv und: es gibt keinen anderen Planeten wie diesen, den wir kennen
            * also warum nicht unsere Aufmerksamkeit auf diese Krise richten und zusammenarbeiten, um zusammen gute Lösungen zu erarbeiten?
* Lehrer, Teachers
    * "'Teach Climate Truth' | XRTeachers Action Outside Dept. of Education | Extinction Rebellion", 2019

### Deutschland
* Köln 2019
* Heidelberg 2019
* ["Act as if the Truth is Real - Extinction Rebellion"](https://www.youtube.com/watch?v=3daIhMb6bVo), 2019, 20 min
    * ...
    * illusion that there is an option called "no nothing"
    * ...
* Willem Dafoe, "I'm with you!" - https://www.youtube.com/watch?v=owRB7WHPKbM
* "Extinction Rebellion ⌛ Blood of Our Children ( Protest Fridays for Future Symbolic Act ) 2019"

* Channel: https://www.youtube.com/channel/UCYThdLKE6TDwBJh-qDC6ICA

* XR Red Rebell Brigades: https://www.cambridgeindependent.co.uk/news/stunning-images-as-xr-red-rebels-take-to-cambridge-streets-9080802/

* http://tierbefreiung-dresden.org/extinction-rebellion-vortrag-mit-friederike-schmitz/ "Vortrag mit Friederike Schmitz"

Entwicklungen
-------------
* 2019
    * "Extinction Rebellion verschiebt Airport-Blockade", https://www.klimareporter.de/protest/extinction-rebellion-verschiebt-airport-blockade
        * inkl. Link zum Planungsdokument
    * "Britische Geschworene akzeptieren ungehorsamen Protest wegen Klimakrise", https://www.klimareporter.de/protest/britische-geschworene-akzeptieren-ungehorsamen-protest-wegen-klimakrise
        * "Ein Geschworenengericht hat zwei Klimaaktivisten freigesprochen, die politische Slogans an Wände des Londoner King's College gesprüht hatten"

Fridays for Future
------------------
### scientists4future
https://www.scientists4future.org/fakten

### Gerechtigkeit als Thema - Welzer
* https://www.ews-schoenau.de/energiewende-magazin/zugespitzt/kommentar-harald-welzer-klimakrise-und-solidaritaet/
    * "Womit wir wieder beim Thema Gerechtigkeit wären, das die Jugendbewegung ins Zentrum ihres Protestes für den Klimaschutz gerückt hat. Das ist klug, denn bei allen erfolgreichen sozialen Bewegungen – der Arbeiterbewegung, der Frauenbewegung, den Bürgerrechtsbewegungen, der frühen Ökobewegung – war die vorhandene Ungerechtigkeit immer die wichtigste Antriebskraft für energischen Protest und den Willen zum Konflikt. Und natürlich gehört ziviler Ungehorsam, die gezielte, friedliche Regelverletzung, zum Repertoire erfolgreichen Protests; wenn die Schülerinnen und Schüler am Wochenende demonstrieren würden, würde sich Minister Altmaier gar nicht erst dafür interessieren."
    * «Scientists for Future» und «Parents for Future» / Ziviler Ungehorsam
        * "und da die «Parents» überwiegend berufstätig sind, müssten sie das an ihren jeweiligen Wirkungsstätten ganz genauso tun. Erst in diesem Augenblick bekäme die Forderung nach einer wirksamen Klimaschutzpolitik jenen gesamtgesellschaftlichen Rückhalt, den es braucht, um noch so potente Wirtschaftsakteure in ihre Schranken zu weisen."
        * "Die haben heute die Rolle, die Fabrikherren im Manchesterkapitalismus hatten. Und sie werden, wenn es eine echte Klimaschutzpolitik geben soll, ihre Privilegien genauso abtreten müssen, wie die Manchesterkapitalisten zähneknirschend die Kinderarbeit und den 16-Stunden-Tag aufgeben mussten."
        * "Denn das ist die zentrale Mitteilung, die die jungen Aktivistinnen und Aktivisten den Älteren zu machen haben: Klimaschutz geht nicht im Modus des Win-win. Wer zukunftsfähig sein will, muss Verantwortung übernehmen können. Und das heißt immer auch: liebgewonnene Wahrnehmungen, Überzeugungen, Erfolgsrezepte, Weisheiten und vor allem Privilegien abgeben. Und das möglichst schnell."

    * Mehr von ews-schoenau.de:
        * Risikoforscher David Spratt: https://www.ews-schoenau.de/energiewende-magazin/zur-sache/david-spratt-wir-brauchen-eine-blut-schweiss-und-traenen-rede/, 2019
            * "fordert [...] eine Mobilisierung der gesamten Gesellschaft – wie in einem Krieg"
        * "Die Industrielobby spielt auf Zeit", https://www.ews-schoenau.de/energiewende-magazin/zugespitzt/die-industrielobby-spielt-auf-zeit/, 2019
            * ...
        * "Die Klimaschuld der Milliardäre", Schellnhuber, 2019
            * https://www.ews-schoenau.de/energiewende-magazin/zur-sache/interview-joachim-schellnhuber-die-klimaschuld-der-millardaere/

### Harald Welzer / ::soziologie
* Buch: Harald Welzer - "Alles könnte anders sein - Eine Gesellschaftsutopie für freie Menschen", 2019
    * siehe auch "Gesellschaftsutopien"
    * https://www.fischerverlage.de/buch/harald_welzer_alles_koennte_anders_sein/9783103974010
    * ... TODO ...
    * Rezension: https://www.mdr.de/kultur/empfehlungen/harald-welzer-alles-koennte-anders-sein-102.html, 2019
        * "sein jüngstes Projekt hat auch ihn an seine Grenzen gebracht: Welzer wollte eine durchgängig positive Gebrauchsanweisung
            für unsere Zukunft schreiben, keine Dystopie. Herausgekommen ist ein Manifest – "Alles könnte anders sein"
            ist eine Gesellschaftsutopie für freie Menschen. Überzeugend, unterhaltsam und mit überraschenden Perspektiven."
    * "Sozialpsychologe Harald Welzer - Schluss mit der Untergangsrhetorik!", https://www.deutschlandfunkkultur.de/sozialpsychologe-harald-welzer-schluss-mit-der.990.de.html?dram:article_id=444470, 2019
        * "Statt Angst zu verbreiten will Welzer Lust auf die Zukunft machen."
        * "Alles könnte anders sein"
        * ...
        * „Realpolitik ist in Wahrheit illusionäre Politik“
        * "die Nachhaltigkeitsszene entwirft dystopische Zukünft"
        * ...
        * ... "Gleichzeitig ist es ja so, dass wir das eigentlich alle wissen, dass wir nicht unendlich wachsen können in einem endlichen System. Sie schütteln den Kopf"
            * ...
            * "das sind **Glaubenssätze**.
                Es ist in der Wissenschaft leider so wie im wirklichen Leben, dass bestimmte Überzeugungen,
                die mal historisch entstanden sind, den Charakter von Glauben annehmen. Dann hält man daran fest."
        * ...

* https://www.youtube.com/watch?v=Y5XpBeD8kwg - Zukunftsforscher Harald Welzer - Jung & Naiv: Folge 419
    * ... TODO ...
    * ...
    * müsste eigentlich sein Institut bestreiken (analog FFF)
    * ...
    * 1h31: Warum habt ihr nichts gemacht?
        * nach kaltem Krieg eine schnelle Depolitisierung der akademischen Klasse
    * ...

* https://www.youtube.com/watch?v=FpiV6ldAhjA - "Harald Welzer: Unsere Freiheit ist bedroht | Sternstunde Philosophie | SRF Kultur", 2015, SRF Kultur, 60 min, ::freiheit, ::free_sw, ::demokratie
    * "Der deutsche Kapitalismuskritiker Harald Welzer sieht die Freiheit bedroht. Deshalb ruft er zum kreativen Widerstand
        auf - gegen Wachstum, Konsum und Überwachung. Barbara Bleisch spricht mit dem Soziologen
        über subtile Zwänge und die schwindende Selbstverantwortung der Bürgerinnen und Bürger."
    * ...
    * ...
    * Moderne bürgerliche Gesellschaften (vs. Leibeigenschaft etc.): Wir brauchen Privatheit, um Pläne zu entwickeln, um dann als öffentliche Person auftreten zu können.
    * Immer wieder gute Rückfragen der Moderatorin
    * ...
    * ...
    * Verbindung zum Totalitarismus
    * ...
    * 18:00: Warum wehren sich Leute nicht? Wann wehren sich Leute
        * Eigenschaften von **totalitären Systemen**:
            * Es gibt die Zugehörigen und die Nicht-Zugehörigen
                * Zugehörige haben Vorteile und merken (eine eventuelle) Veränderung nicht
                * Nicht-Zugehörige haben Nachteile, werden unterdrückt, oder getötet
    * ...
    * 21:00: Bilder von Stanley Milgrams Experiment ("Obedience")
    * 25:00: Parallelen zum Internet: es geht schleichend
        * noch nicht mal Snowden hatte ein kollektiven Zäsureffekt
        * Man denkt an nichts Böses. In den ersten Stufen des Versuchs dachten die Versuchspersonen an nichts Böses.
    * 27:00: Machtergreifung etc. was hat die Mehrheit damals gedacht: ach, wird schon nicht so schlimm oder hmm, nicht so toll, mal sehen wie das weitergeht
        * totalitäre Systeme töten ganz viele Menschen; das kann man rückblickend feststellen; das wussten aber die Menschen am Anfang nicht
    * 28:00: Panoptikum
        * Ist es nicht ok, wenn Menschen sagen: ich mach diesen Tradeoff für mich, wenn dadurch das Marketing besser wird?
    * 32:00: Er hat Privileg (z. B. würde man ihn auch ohne Smartphone kontaktieren können); ist das nicht aus dieser Warte einfach zu sagen, man solle sich wehren?
        * Priviligien => Verpflichtung
        * ...
    * 34:00: kommen wir überhaupt weiter, wenn sich die Mehrheit gar nicht dafür interessiert, weil es ihnen ja gut geht?
        * Soziale Bewegungen dauern lange und sind frustrierend; es gibt aber Augenblicke, wo das kippt
    * 37:45: https://futurzwei.org/ - Positive Beispiele, siehe **positive-beispiele.md**
    * 45:00: Über Erziehung: es kommt auf die Persönlichkeit des Kindes an (wollte z. B. im Alter von 3-4 Jahren kein Fahrradfahren lernen; sehr autonom)
        * am besten stellt man offene Entwicklungsräume zur Verfügung, in denen sich die Menschen selber entfalten können (vs. Drohneneltern oder dauernd nur päd. wertvolles Spielzeug)
            * gut für automome Kinderpersönlichkeiten; gibt aber auch welche, die mehr Anleitung erfordern
    * 49:00: lässt sich gerne Entscheidungen abnehmen, aber nicht von Algorithmen, zu denen er kein Vertrauen hat
    * 2013: Warum ich nicht mehr wählen gehe...
        * Debatte: ist Demokratie Kreuzchen machen oder noch mehr?
        * ist dann doch wählen gegangen. Ist nicht gegen das Wählen, sondern Arroganz von Parteipolitik
    * 55:00: Neue Machtformen entstehen überraschend
        * Machtpotentiale werden genutzt
        * Entmächtigung des Bürgers, wenn es keine Privatheit mehr gibt; man sich nicht mehr organisieren kann, ohne dass es jemand merkt
        * Neue Macht erfordert neue **Gegenmacht** zu entwickeln
            * => politische Regulierungen würden folgen; z. B. bestimmte Daten werden gar nicht erhoben
    * 56:00: Übertreiben Sie? -> Denken hinter den Gegenwartspunkt erweitern; denn dann kann man nochwas machen;
        * hinterher ist man immer schlauer, aber kann nichts mehr machen

* Buch: Soldaten

* "Selbst denken: Eine Anleitung zum Widerstand"-Buch, 2014
    * ...

### Gesellschaftsutopien
* siehe Welzer
* siehe Figueres
* "But Beautiful - Interview mit Erwin Wagenhofer", https://www.youtube.com/watch?v=C6hBIRFh18A, 2019, 12 min
    * Verbundenheit und Liebe
    * Frauen
    * ...
    * ...
* siehe Eisenstein

### Verschiedenes
* "Fridays for Future: Für Greta beginnt Umweltschutz auch auf unseren Tellern" https://www.animalequality.de/neuigkeiten/fridays-future-fuer-greta-beginnt-umweltschutz-auch-auf-unseren-tellern
* "Afrikas Bevölkerung wächst rasant und soll sich bis 2050 sogar verdoppeln. Auch die Armut steigt, das Wirtschaftswachstum kann nicht mithalten. Experten fordern dringend Maßnahmen." Tagesschau 2019
* Schöne Diskussion: https://www.zdf.de/gesellschaft/markus-lanz/markus-lanz-vom-20-maerz-2019-100.html
* "Klimaschutz und Verkehr "Mobilität wird teurer werden"", tagesschau
* "Die nachhaltige Disruption (Constantin Alexander - Science Slam)" - https://www.youtube.com/watch?v=VbSbcuO0YdM, 10 min
    * Schadschöpfungsanalsye
    * ...

### Was-ist-so-schlimm-am-Kapitalismus
* Buch: https://www.randomhouse.de/Buch/Was-ist-so-schlimm-am-Kapitalismus/Jean-Ziegler/C-Bertelsmann/e547280.rhd "Eine ermutigende Streitschrift des bekannten Kapitalismus- und Globalisierungskritikers! / Leben wir mit dem Kapitalismus in der besten aller Welten? Dass Jean Ziegler dieser Ansicht entschieden widerspricht, wissen seine Leser. Jetzt erklärt er seiner Enkeltochter Zohra und ihrer Generation, welchen unmenschlichen Preis wir für dieses System zahlen, warum es „radikal zerstört“ werden muss und mit dem weltweiten Erstarken der Zivilgesellschaft eine neue Antwort der Geschichte heraufzieht. / Wie in all seinen provokanten Analysen stellt sich Ziegler in unmissverständlicher Klarheit den Fragen von Zohra: Der Kapitalismus ist als „kannibalische Weltordnung“ unreformierbar. Und er zeigt sich überzeugt, dass dessen Abschaffung eine kraftvolle Utopie ist, an deren Verwirklichung bereits Millionen Menschen arbeiten und sich als breite Widerstandsfront formieren."
    * TODO

### Prof. Dr. Maja Göpel
* Prof. Dr. Maja Göpel: "Generalsekretärin des Wissenschaftlichen Beirats der Bundesregierung Globale Umweltveränderungen (WBGU)"
    * klasse Eingangsstatement: https://m.youtube.com/watch?v=OAoPkVfeTo0
    * 2020
        * Twitter: "Wie würde die systemisch denkende Sozialwissenschaft sagen: wir alle machen Zukunft, auch wenn wir gefühlt „nix tun“. Mein (nicht) Handeln & (nicht) Sprechen ist Information für andere - ob ich es will oder nicht."
* "Maja Göpel ("Scientists For Future") - Jung & Naiv: Folge 420", https://www.youtube.com/watch?v=3vhuFlVGBeI, 2019, 2 h
    * "Die Menschen haben noch nie Ökosysteme gebaut. Aber die Menschen haben schon mehrere Finanzsysteme gebaut"
    * ...
* https://www.3sat.de/wissen/nano/im-gespraech-maja-goepel-100.html, 4 min, 2018
    * ...
* https://www.youtube.com/watch?v=cdFXn48B7w8 - e:publica 2019 – Digital Responsibility: Beyond Buzzwords – worum geht es wirklich? - mit Maja Göpel
    * ...
    * 17:50: Was ist Wertschöpfung, Wertschätzung, weg von kurzfristigen rein finanziellen KPIs
        * Dinge leben jenseits von Nachhaltigkeitsberichten
            * => Gemeinwohlbilanz?
    * ... TODO
* Buch: "The Great Mindshift - How a New Economic Paradigm and Sustainability Transformations go Hand in Hand", 2016, todo
    * https://www.transforming-cities.de/der-grosse-sinneswandel/, 2016
        * "Der große Sinneswandel"
        * "Dr. Maja Göpel, Leiterin des Berliner Büros des Wuppertal Instituts"
        * "kombiniert die bestehende Forschung zu Systemtransformationen mit Theorien politischer Ökonomie und dem Wissen über Change-Leadership.
        Damit bricht Göpel mit einem Transformationsverständnis, das noch viel zu stark dem Blick auf Technologien und ökonomische Anreize verhaftet ist.
        Sie fordert stattdessen einen Wandel der Denkmuster, die heutige Gesellschaften prägen."
        * "Was muss sich ändern, um das Ziel vom Glück und guten Leben für alle Menschen zu erreichen – im Einklang mit unserer Mitwelt? Welche Methoden und Fähigkeiten braucht es, um den Wandel voranzutreiben?"
        * "Im Zentrum ihrer Analyse steht insbesondere die Rolle von Überzeugungen und Orientierungsmustern, die heutige Gesellschaften und ihre Institutionen prägen.
        Göpel zeigt auf, wie stark insbesondere die seit dem 19. Jahrhundert dominierende ökonomische Perspektive noch heute den Nachhaltigkeits-Diskurs einschränkt."
        * "Treiber aller Transformationsprozesse, so ihr Ansatz, müssten statt bloßer ambitionierter Zieldefinitionen permanent reflektierende Akteure mit einer neuen, noch kaum erforschten Fähigkeit sein.
        Erst diese besondere „Transformations-Literacy“ ermögliche es, die nötigen Prozesse zu verstehen und den radikalen Wandel in kleinen Schritten voranzutreiben."
        * "Zur spannenden Lektüre nicht nur für Wissenschaftlerinnen und Wissenschaftler, sondern auch für Nachhaltigkeits-Aktivisten wird das Buch vor allem dank der fundierten Analyse bürgerschaftlicher Initiativen.
        Transition Town-Bewegung, die Commons Bewegung, Gemeinwohl-Geschäftsmodelle und Neue Wohlstandsmessungen, so Göpel, zeigten auf, wie die Gestaltung von Transformationsprozessen für starke Nachhaltigkeit umgesetzt werden können."
        * https://www.transforming-cities.de/impressum/
            * https://www.internationales-verkehrswesen.de/urban-mobility-looking-for-the-royal-road/, 2016
                * "Perhaps a fundamental change of attitude, moving away from the philosophy of unrestricted urban mobility towards structural traffic prevention could permit a certain amount of free movement. This approach would include, for example, reducing the distances between individual places of activity, such as residences and workplaces or production and shopping. By bringing these places closer together again, many unnecessary journeys could be avoided.
                Thanks to digital technologies, this is already possible today. Social resolve would be the next step."
        * siehe auch iass-potsdam.de
* Digitalisierung ist Brandbeschleuniger für Klimawandel
* Frage an Etablierte: Wie sieht das Zukunftsmodell aus, wenn wir /nichts/ ändern?

### IG Metall
* https://www.tagesschau.de/wirtschaft/ig-metall-kundgebung-101.html, 2019
    * "IG-Metall fordert fairen Wandel der Wirtschaft"
    * "Die Teilnehmer fordern einen sozialverträglichen Umbau der Industrie und die Sicherung von Arbeitsplätzen in der Energie- und Verkehrswende."
    * "Ökologie und gute Arbeit sind kein Widerspruch, sagt IG-Metall-Chef Hofmann"

Zusammenstellung Basisinfos
---------------------------
### Wann kommt denn nun die Krise?
* die Krise ist schon längst da: siehe aktuelle Krisen und Menschenrechtsverletzungen rund um die Erde
    * nur bei uns für viele noch nicht schmerzhaft spürbar

* "Major Climate Report Describes a Strong Risk of Crisis as Early as 2040", https://www.nytimes.com/2018/10/07/climate/ipcc-climate-report-2040.html, 2018
    * also in 20 Jahren
    * ...

### 2019
1. (englisch) Text von Roger Hallam - Co-founder - "Common Sense for 21st Century", 79 Seiten - PDF-Download rechts oben auf dieser Seite https://www.rogerhallam.com/ - Analyse der Grundprobleme und Lösungsansätze

2. (englisch) Video: Roger Hallam im BBC-Interview: https://www.youtube.com/watch?v=9HyaxctatdA, 25 min

3. Buch (nicht als PDF gefunden): "Wann, wenn nicht wir", Fischer-Verlag 2019: https://www.fischerverlage.de/buch/extinction_rebellion_wann_wenn_nicht_wir/9783103970036 - Übersetzung der englischen Originalausgabe "This is not a drill" mit deutschen Ergänzungen

4. Buch (als PDF verfügbar): "Extinction Rebellion Hannover »Hope dies – Action begins«: Stimmen einer neuen Bewegung": https://www.transcript-verlag.de/978-3-8376-5070-9/hope-dies-action-begins-stimmen-einer-neuen-bewegung/?number=978-3-8394-5070-3, 96 Seiten

5. Webseite: https://extinctionrebellion.de/
- Prinzipien und Werte: https://extinctionrebellion.de/wer-wir-sind/prinzipien-und-werte/
- Die 3 Forderungen: https://extinctionrebellion.de/wer-wir-sind/unsere-forderungen/
- Unterstützung durch die Wissenschaft: https://de.wikipedia.org/wiki/Extinction_Rebellion#Offene_Briefe

6. (englisch) Texte: Arbeiten von Professor Jem Bendell ("strategist & educator on social & organisational change") zu "Deep Adaptation", siehe z. B. https://jembendell.com/2019/07/31/climate-scientist-speaks-about-letting-down-humanity-and-what-to-do-about-it/

7. Video: Beispiel-Aktion Brückenblockade in Köln: https://www.youtube.com/watch?v=CgC5CbTZpyU, 8 min

8. Video: Beispiel-Aktion "Blut unserer Kinder" in Hamburg: https://www.youtube.com/watch?v=3fD02QgkIkQ, 7 min

9. (englisch) Vorträge von Rupert Read: Fokus auf Gefühle, die entstehen, wenn man sich einer kommenden Katastrophe gegenübersieht
- CLIMATE CATASTROPHE: The Case For Rebellion | Dr Rupert Read Talk":
https://www.youtube.com/watch?v=RnonKverhOg, 2019, 45 min
- Dr Rupert Read - The Uncertain Situation We Are In | Extinction Rebellion": https://www.youtube.com/watch?v=P4uckj3dbUU, Feb 2019, 1h 30min

10. (englisch) Video: XR Talks | Roger Hallam | Non-Violent Direct Action - Extinction Rebellion - https://www.youtube.com/watch?v=jSOlRNCO9L8, 22 min

11. (englisch) Video: "If we don't work together, we are going to die together" - Roger Hallam | Extinction Rebellion - https://www.youtube.com/watch?v=9XKTA1cSu1E, 18 min
    * "movement of movements"

12. Harald Welzer: Artikel: https://www.ews-schoenau.de/energiewende-magazin/zugespitzt/kommentar-harald-welzer-klimakrise-und-solidaritaet/ - über Gerechtigkeit, Solidarität und zivilen Ungehorsam

13. Professor Dr. Maja Göpel -  ("Scientists For Future") - Jung & Naiv: Folge 420", https://www.youtube.com/watch?v=3vhuFlVGBeI, 2019, 2 h
- sitzt im wissenschaftlichen Beirat der Bundesregierung
- "Die Menschen haben noch nie Ökosysteme gebaut. Aber die Menschen haben schon mehrere Finanzsysteme gebaut"
- Es geht um die Transformaton der Gesellschaft

14. Vortrag für "normale" Menschen: Professor Dr. Harald Lesch beim 9. Hessischen Klimaempfang 2019 - https://www.youtube.com/watch?v=F4jDk2MPZbA, HLNUG, 50 min

15. Vortrag für "normale" Menschen: "Diözesanempfang 2019 - Vortrag von Prof. Dr. Harald Lesch" - https://www.youtube.com/watch?v=_ccCrhWhkTQ, 53 min, Würzburg

16. Ziviler Ungehorsam: "Britische Geschworene akzeptieren ungehorsamen Protest wegen Klimakrise": https://www.klimareporter.de/protest/britische-geschworene-akzeptieren-ungehorsamen-protest-wegen-klimakrise

17. Ziviler Ungehorsam: https://www.klimareporter.de/protest/die-letzte-hoffnung-ein-aufstand

18. Pispers - Wirtschaftswachstum

19. Hamburg 20.7.2019 - "die größe Gefahr ist der Glaube, dass jemand anders sich der Klimakrise annimmt", "angemeldete Aktion (Level 0)", 5 min - https://www.youtube.com/watch?v=oh6ZL1c2P84

20. DDR: Friedliche Demos in Leipzig als Katalysator für den Mauerfall todo
- z. B. 2019 zum Tag der deutschen Einheit: Professor Philipp Gassert, Uni Mannheim: "Etablierte und Bewegte: Wie die bundesdeutsche Demokratie nicht zuletzt an ihren Protestbewegungen gewachsen ist"
    - "Straßenprotest sei Alltag in Deutschland"
    - "Regierungen mussten erst lernen, dass man nur Erfolg haben könnte, wenn man die Kritik der Bewegten aufgreift. Das habe man heute durch die FFF-Bewegung schneller begriffen."
    - "Protest ist politische Kommunikation mit einem Ausrufezeichen"
    - "Durch Proteste habe die Qualität der Demokratie zugenommen"
        * siehe auch https://extinctionrebellion.de/blog/demokratie-ein-absoluter-begriff/, 2019
            * todo

Weitere
-------
### Niko Paech
* siehe sl-paech.md

### Charles Eisenstein
* siehe sl-eisenstein.md

### Artensterben: Andreas Hetzel - Sozialphilosoph, Soziologe
* Video: Vortrag von Andreas Hetzel zu Artensterben; Universität Hildesheim. Prof. Dr. Andreas Hetzel, eine philosophische Perspektive - https://www.youtube.com/watch?v=rKf9j5BDJ9A - XR Hannover, 2019
    * Intro
        * ...
        * Bedeutung für das menschliche Selbstverständnis
        * Soziologie und Umweltweltwissenschaft: wie wirkt sich unserer Umgang mit der Natur auf uns zurück
        * Respekt, Ethik
    * 1. Bedrohte Vielfalt
        * ...
        * **uncharismatische Arten**
        * ...
        * **Fossilienproduktion** im Anthropozän
            * fünf andere Erdzeitalter endeten auch in einem massenhaften Aussterben
            * Anthropozän ist vom Begriff her problematisch, weil er Handlungsoptionen (unsere Gesellschaftsformen sind nicht alternativlos), naturalisiert
        * Aussterbe-Ereignis => 3/4 weg
            * Regeneration in 5 bis 10 Mio Jahren ("jeseits menschlicher Planungsmaßstäbe")
        * 22:40: große Teile der Menschheit hat sich auf eine ständig wachsen müssende Gesellschaftsform festgelegt (oder lassen)
        * im Wimpernschlag das Antlitz der Erde verändert (Kohleabbau, Plastikmüll)
        * 24:00 Bevölkerungwachstum korreliert mit **Energieproduktion**
            * interessant: Öl, Kohle, Gas: wir schöpfen gerade die Energien ab, die **durch die Produktivität früherer Ökosysteme entstanden** sind
                * ohne, dass wir das mit unserer Technik derzeit ersetzen (können)
        * **Landwirtschaft** änderte sich
            * 1900: floss als Energie hauptsächlich menschliche Arbeit in die Produktion: das ca. 9-fache der reingesteckten Energie kam als Produkt zurück
            * heute: 10x produktiver, aber das Energieverhältnis ist nun ca. 1:1
                * die Energie, die wir dafür reinstecken, beziehen wir aus endlichen Depots
                * viel Stickstoffdünger (Diagramm)
        * weiter 26:30...
    * ...
    * ... TODO ...

### Artensterben - Extermination
* "This isn't extinction, it's extermination: the people killing nature know what they're doing"
    * https://www.theguardian.com/environment/commentisfree/2019/sep/20/this-isnt-extinction-its-extermination-the-people-killing-nature-know-what-theyre-doing

* "This Is Not the Sixth Extinction. It’s the First Extermination Event."
    * https://truthout.org/articles/this-is-not-the-sixth-extinction-its-the-first-extermination-event/

### Genügsamkeit 1
* siehe sl-paech

### Genügsamkeit 2
* https://www.vernuenftig-leben.de/genuegsamkeit-ein-schmaler-grat/
    * ...
* https://www.vernuenftig-leben.de/ueber-vernuenftig-leben/
    * "Nachdem ich zwei Studiengänge als Jahrgangsbester und trotz Stipendium abgebrochen habe, habe ich erkannt, worauf es wirklich im Leben ankommt."
    * „In Widerspruch zur eigenen Vernunft zu leben, ist der unerträglichste aller Zustände.“
    * "Ein vernünftiges Leben ist nicht…
        vom Verstand beherrscht
        streng diszipliniert
        rational und berechnend
        kalt und gefühllos
        intellektuell
        verzichtend und aufopfernd"
    * "Unser Motto lautet: Vernunft ist wenn man’s trotzdem macht!"
        * "erfüllend, sinnvoll, ausgeglichen, bewusst"
    * https://www.vernuenftig-leben.de/um-die-liebe-kaempfen/
        * ...
        * "Beim Kämpfen geht es dir nicht um die Liebe!"
        * "Um die Liebe kämpfen ist egoistisch, weil..."
        * "Wer um die Liebe kämpft, der kämpft für sich!"
        * "Nicht zu kämpfen heißt nicht aufzugeben!"
            * "Die Liebe steht über allem und hat keinen Kampf nötig. Wo kein Kampf ist, da gibt es kein Gewinnen, kein Verlieren und auch kein Aufgeben!"
        * "Jemandem Liebe zu schenken bedeutet, ihm Freiheit zu schenken."
        * "Wie du „nicht kämpfst“ und was du stattdessen tun solltest"
            * "Kümmere dich um dich selbst, liebe dich selbst!"
        * ...
    * https://www.vernuenftig-leben.de/ruhe-bewahren/
        * ...
        * ... todo ...

### Genügsamkeit 3
* "Für eine "Ethik des Genug"", http://kirchenkreis-siegen.de/?katid=1&newsid=1411, 2019, Evangelische Kirche
    * https://www.emk.de/meldungen-2015/die-ethik-des-genug-vorantreiben/
        * "Die Kirche könne dazu einen Beitrag leisten, indem sie eine »Ethik des Genug« vorantreibe.
        »Es geht um die Frage, wie wir es schaffen, dass alle Menschen ein würdiges Leben führen können,
        ohne die Ressourcen zukünftiger Generationen zu verbrauchen?«"
