Nachhaltigkeit
==============

<!-- toc -->

- [Was ist Nachhaltigkeit?](#was-ist-nachhaltigkeit)
- [Was sind die Folgen bei Untätigkeit?](#was-sind-die-folgen-bei-untatigkeit)
  * [Collapse, revisited](#collapse-revisited)
  * [Vermeintliche Ohnmacht des Einzelnen](#vermeintliche-ohnmacht-des-einzelnen)
- [Inbox](#inbox)
  * [2020 - Endgame 2050](#2020---endgame-2050)
  * [2020 - GermanZero](#2020---germanzero)
  * [2020 - Ungleichheit](#2020---ungleichheit)
  * [2020 - Gladiator](#2020---gladiator)
- [Umweltbewusstsein in der Bevölkerung](#umweltbewusstsein-in-der-bevolkerung)
  * [2019](#2019)
- [Institute](#institute)
  * [artec Forschungszentrum Nachhaltigkeit](#artec-forschungszentrum-nachhaltigkeit)
  * ["unserer Lebensstil"](#unserer-lebensstil)
  * [IASS Potsdam](#iass-potsdam)
  * [Umweltbundesamt](#umweltbundesamt)
  * [„Konsumbürger“: Engagement jenseits des eigenen Konsums](#%E2%80%9Ekonsumburger-engagement-jenseits-des-eigenen-konsums)
- [Videos für Einsteiger](#videos-fur-einsteiger)
  * [Louis C.K. - Polar bears, faster, money, bacon](#louis-ck---polar-bears-faster-money-bacon)
  * [Bill Maher - Earth vs. Mars](#bill-maher---earth-vs-mars)
  * [Sammlung: Filme für die Erde](#sammlung-filme-fur-die-erde)
  * [Cartoons](#cartoons)
- [Lesch](#lesch)
  * [Lesch beim 9. Hessischen Klimaempfang 2019](#lesch-beim-9-hessischen-klimaempfang-2019)
  * [Lesch beim Diözesanempfang 2019](#lesch-beim-diozesanempfang-2019)
  * [Finanzen](#finanzen)
- [Ökologischer Fußabdruck](#okologischer-fussabdruck)
- ["Most effective individual steps"](#most-effective-individual-steps)
- [Die Allmende-Herausforderung](#die-allmende-herausforderung)
  * [Das Allmende-Problem](#das-allmende-problem)
  * [Ist das ein Problem des Wirtschaftssystems? / Markt und Regulierung](#ist-das-ein-problem-des-wirtschaftssystems--markt-und-regulierung)
  * [TODO: Die Welt als Allmende: marktwirtschaftlicher Wettbewerb und Gemeingüterschutz](#todo-die-welt-als-allmende-marktwirtschaftlicher-wettbewerb-und-gemeinguterschutz)
  * [Elinor Ostrom und die Wiederentdeckung der Allmende](#elinor-ostrom-und-die-wiederentdeckung-der-allmende)
  * [Weitere Beispiele: Weltraumschrott](#weitere-beispiele-weltraumschrott)
- [Fortschrittsversprechen als Ablenkung](#fortschrittsversprechen-als-ablenkung)
  * [Beispiel Tierwohl in der Tierhaltung](#beispiel-tierwohl-in-der-tierhaltung)
  * [Beispiel Überfischung](#beispiel-uberfischung)
  * [Beispiel Breakthrough Institute](#beispiel-breakthrough-institute)
  * [Beispiel biooekonomie.de](#beispiel-biooekonomiede)
- [Vorbilder](#vorbilder)
  * [Mit Gemeinwohlbilanz / Gemeinwohlökonomie](#mit-gemeinwohlbilanz--gemeinwohlokonomie)

<!-- tocstop -->

Was ist Nachhaltigkeit?
-----------------------
* https://de.wikipedia.org/wiki/Nachhaltigkeit
    * "Naturvölker"
    * "Nachhaltigkeit enthält in seiner Grundidee einen Nutzen für alle Beteiligten. Wenn der Umstieg auf nachhaltige Wirtschaftsformen allerdings aus der Not heraus stattfindet, weil der Raubbau an den Ressourcen bereits sehr weit fortgeschritten ist, dann liegt darin durchaus auch Konfliktpotential."
    * Digitale Nachhaltigkeit
        * https://de.wikipedia.org/wiki/Digitale_Nachhaltigkeit
* In der Politik: https://www.umweltbundesamt.de/themen/nachhaltigkeit-strategien-internationales/nachhaltigkeit-in-der-politik
    * "Den vielfältigen Herausforderungen - wie der steigenden Nachfrage nach Energie und Ressourcen oder dem Verlust von Boden und Arten - müssen wir mit umfassenden, langfristigen Lösungen begegnen. Gleiches gilt für die soziale Gestaltung unserer Wirtschafts- und Lebensweisen. Nachhaltige Entwicklung ist das übergeordnete Leitprinizip, das Ökologie, Ökonomie und soziale Verantwortung verzahnt."
* https://www.bevegt.de/nachhaltiger-leben/
    * "Nachhaltiger leben: 5 Dinge"

Was sind die Folgen bei Untätigkeit?
-----------------------------------
* 2020
    * https://www.zeit.de/video/2019-09/6087750314001/klimawandel-was-wenn-wir-nichts-tun
        * "Der Klimaforscher Stefan Rahmstorf erklärt unsere Welt mit 4 Grad mehr."
* https://de.wikipedia.org/wiki/Nachhaltigkeit
    * "Wenn der Umstieg auf nachhaltige Wirtschaftsformen allerdings aus der Not heraus stattfindet, weil der Raubbau an den Ressourcen bereits sehr weit fortgeschritten ist, dann liegt darin durchaus auch Konfliktpotential."
        * **Wald-Beispiel:** "Nachhaltigkeit in der Forstwirtschaft setzt jedoch voraus, dass genügend Bäume stehen bleiben, die zum Teil mit polizeilicher Gewalt vor dem Diebstahl durch verzweifelte Menschen geschützt werden mussten."
* Buch: [Kollaps](https://de.wikipedia.org/wiki/Kollaps_(Buch)): Warum Gesellschaften überleben oder untergehen, 2005
    * ...
    * "Eine entscheidende Rolle spielt für Diamonds Betrachtung immer **das Vorliegen einer konkreten Überbevölkerung**. Denn würden Teile der Gesellschaft mit lebensbedrohlichen Veränderungen ihrer Umwelt konfrontiert werden, würden sie **keinesfalls passiv sterben**, sondern **bei dem Versuch zu überleben auch die Teile der Gesellschaft gefährden**, die bisher nicht betroffen waren und sich **möglicherweise in falscher Sicherheit wähnten**. Nicht allein der notleidende Teil, sondern die **eng vernetzte Gesamtgesellschaft** bricht in einer schnellen Katastrophe zusammen und erfährt einen "Kollaps"."
    * ...
    * siehe auch
        * Artikel dazu: http://www.spiegel.de/spiegel/print/d-43961198.html, 2005
            * "In vieler Hinsicht ähnelt Diamonds Methode derjenigen, die ich als Romanautor wähle: Er wendet sich nicht an den Intellekt oder das Gewissen seiner Leser, sondern an deren Einbildungskraft."
        * Buch: https://de.wikipedia.org/wiki/Der_dritte_Schimpanse
        * Buch: https://de.wikipedia.org/wiki/Arm_und_Reich_(Diamond)
            * "Als ab dem 15. Jahrhundert Europäer die nicht-eurasischen Erdteile systematisch erkundeten und besiedelten, verfügten sie über eine stark überlegene Kriegstechnik und politische Organisation, die ihnen die rasche Unterwerfung einheimischer Gesellschaften ermöglichte. So wurde etwa das Inka-Reich in kurzer Zeit von nur 160 Spaniern vernichtet."
            * "Diamond führt aus, dass es neben der überlegenen Waffentechnik insbesondere die von den Europäern eingeschleppten Krankheiten waren, die oftmals über 90 Prozent der einheimischen Bevölkerung auslöschten, so dass Keime faktisch zum wichtigsten Faktor bei der Unterwerfung und Dezimierung indigener Völker wurden."
* Buch: [Food Crash - Wir werden uns ökologisch ernähren oder gar nicht mehr](https://www.droemer-knaur.de/buch/7768538/food-crash), 2011

### Collapse, revisited
https://en.wikipedia.org/wiki/Collapse:_How_Societies_Choose_to_Fail_or_Succeed

"In the last chapter, he discusses environmental problems facing modern societies and addresses objections that are often given to dismiss the importance of environmental problems (section "One-liner objections"). In the "Further readings" section, he gives suggestions to people who ask "What can I do as an individual?". He also draws conclusions, such as:

    In fact, one of the main lesson to be learned from the collapses of the Maya, Anasazi, Easter Islanders, and those other past societies ... is that a society's steep decline may begin only a decade or two after the society reaches its peak numbers, wealth, and power. ... The reason is simple: maximum population, wealth, resource consumption, and waste production mean maximum environmental impact, approaching the limit where impact outstrips resources.

Finally, he answers the question, "What are the choices that we must make if we are to succeed, and not to fail?" by identifying two crucial choices distinguishing the past societies that failed from those that survived:

    - Long-term planning: "... the courage to practice long-term thinking, and to make bold, courageous, anticipatory decisions at a time when problems have become perceptible but before they have reached crisis proportions."
    - Willingness to reconsider core values: "... the courage to make painful decisions about values. Which of the values that formerly served a society well can continue to be maintained under new changed circumstances? Which of these treasured values must instead be jettisoned and replaced with different approaches?"
"

### Vermeintliche Ohnmacht des Einzelnen
* siehe "Philosophie der Nachhaltigkeit - Es ist nicht egal, was du tust" / wirksamkeit-des-einzelnen.md

Inbox
-----
### 2020 - Endgame 2050
* Doku: YT: "ENDGAME 2050 | Full Documentary [Official]", 2020
    * Biodiversität
    * Animal Agriculture
    * Meere
    * Bevölkerungswachstum, Paul Ehrlich, siehe fortpflanzung.md

### 2020 - GermanZero
* GermanZero, Das 1,5-Grad-Gesetz, "Vorstellung der neuen Initiative GermanZero – Heinrich Strößenreuther, 17.12.2019"
    * https://www.youtube.com/watch?v=ZPI-VScliSo, 15 min, 2020
    * ehemals bei der Bahn

### 2020 - Ungleichheit
* "Nichts verhindert Nachhaltigkeit mehr als Ungleichheit", https://agora42.de/ungleichheit-und-nachhaltigkeit-interview-butterwegge/, Prof. Dr. Christoph Butterwegge
    * ...

### 2020 - Gladiator
* http://www.actorpoint.com/movie-scripts/scripts/gladiator.html
    * "And what pays for it?", "The future.  The future pays for it...", "He's selling Rome's reserves of grain. The people will be starving in two years."

Umweltbewusstsein in der Bevölkerung
------------------------------------
### 2019
* https://www.umweltbundesamt.de/presse/pressemitteilungen/umweltbewusstseinsstudie-2018
    * "Bevölkerung erwartet mehr Umwelt- und Klimaschutz von allen Akteuren"

Institute
---------
### artec Forschungszentrum Nachhaltigkeit
https://www.uni-bremen.de/artec/

"Das artec Forschungszentrum Nachhaltigkeit ist ein interdisziplinäres Zentrum der Universität Bremen zur wissenschaftlichen Erforschung von Fragen der Nachhaltigkeit und sozial-ökologischen Transformation. Im Kern dieser Wissenschaftlichen Einrichtung der Universität Bremen steht ein Zusammenschluss von Wissenschaftlerinnen und Wissenschaftlern aus unterschiedlichen Fachbereichen, darunter derzeit die Sozialwissenschaften, die Produktionstechnik, die Kulturwissenschaften sowie die Human- und Gesundheitswissenschaften."

### "unserer Lebensstil"
https://www.uni-bremen.de/artec/artec-paper/

* "Roland Bogun | Konsum, Umweltverbrauch und soziale Ungleichheit – eine Frage "unseres Lebensstils"?", 2012
    * "Reichtum und Verbrauch von "Umweltraum""
        * ...
    * "Nichtverkonsumierte Einkommen: Über die ökologische Relevanz von Kapitalanlagen"
        * ...
        * ! "dass  insgesamt  pro  10.000  investierter  Euro  fünf  Tonnen  Treibhausgas-Emissionen  im  Jahr  mitfinanziert  werden.
            Am  besten  schneiden  dabei  klimafreundliche Sparprodukte ab, pro Euro werden hier nur 66 g Treibhausgas  (THG)  mitfinanziert.
            Am  anderen  Ende  der  Skala  befinden  sich  die  untersuchten  Unternehmensaktien  aus  besonders  marktrelevanten
            europäischen  Standardindizes, deren Treibhausgasintensität bei 1243 g pro Euro liegt."
        * "etwa zwei Drittel der erwachsenen Deutschen – so groß ist der Anteil derjenigen, die über kein oder
            nur  geringes  Geld-  und  Sachvermögen  verfügen  (Frick/Grabka  2009)  –
            an  dieser  Form  der  Förderung  des  Ressourcenverbrauchs  überhaupt  nicht  beteiligt  sind."
        * ...
    * Fazit
        * "Wie bereits die Ausführungen über die Verursachung von Treibhausgasemissionen gezeigt haben,
            sind die KonsumentInnen am größten Teil des ihnen zugerechneten Umweltverbrauchs nur indirekt beteiligt.
            Dies  gilt  nicht  nur  für  die  Emission  von  Treibhausgasen, sondern auch für andere ökologische Probleme.
            Die unmittelbare Erzeugung  von  Umweltbelastungen  findet  nach  wie  vor  primär  im  Bereich  der  Produktion  statt."
        * "Geht es um den Beitrag der Konsumenten zu einzelnen ökologischen  Problemen  im  Zusammenspiel  mit  anderen  Akteuren,
            erscheint  es  deshalb  sinnvoll,  genauer  danach  zu  unterscheiden,  über  welche  –  direkten  und  indirekten  –
            Einflussmöglichkeiten  welche  Akteure  jeweils  verfügen."
        * "Mythos   vom   souveränen   Konsumenten"
            * "Diese  Vorstellung  leugnet  den  Einfluss  des  Staates  und  der  Wirtschaft  auf  (und  ihre  Verantwortung  für)
            Umfang  und  Struktur  der  Optionen,  die  den  Konsumenten  zur  Verfügung  stehen."
            * "Mit  vermutlich  größerer  Berechtigung  könnte  man  ebenso  auch  umgekehrt  die  einschlägigen  Akteure  in  den
            Bereichen  der  Produktion,  des  Handels,  der  Werbung  und  des  Staates  mit  ihren  mehr  oder  weniger  offenen
            Anreizen  und  Appellen  ("Konsum  als  Bürgerpflicht“)  als  Treiber  des  Konsums  bezeichnen."
        * Einkommen
            * "Vieles  spricht  indes   dafür,   dass   die   hier   vorgestellten   Befunde   und   Einschätzungen   die
            tatsächliche  Spannbreite,  insbesondere  den  Umweltverbrauch  der  einkommens-stärksten Gruppen, deutlich unterschätzen."
            * "Mengenproblem  –  und  zwar  eines,  das  maßgeblich  durch  die  Höhe des verfügbaren Einkommens reguliert wird."
            * "Solche  Befunde  bedeuten  freilich  nicht,  dass  die  Höhe  des  Umweltverbrauchs  vollständig  von  der  Einkommenshöhe  determiniert  wäre,"
        * "finanziellen  Aktivitäten  nicht  umwelt-neutral  sind,  sondern  ebenfalls  einen  indirekten  Einfluss  auf  den  Ressourcen-verbrauch  haben"

* "Minna Kanerva | Meat consumption in Europe: Issues, trends and debates", 2013
    * ...
    * ...

* "Felix Wilmsen | Je mehr, desto weniger? Das Verhältnis von Wachstumskritik und alternativen Wirtschaftskonzepten des Postwachstums zum kapitalistischen System", 2015
    * ?

### IASS Potsdam
https://www.iass-potsdam.de

https://www.iass-potsdam.de/de/unser-ansatz

"Transformation ist die Gestaltung unserer Lebensweisen nach den Grundsätzen der nachhaltigen Entwicklung. Damit ist die Entwicklung zu einer ökonomisch und ökologisch verträglichen sowie ethisch vertretbaren Wirtschafts- und Gesellschaftsform gemeint, bei der die kommenden Generationen nicht schlechter gestellt sein dürfen als die Generationen, die heute leben."

"Dabei geht es zum einen um politisch wirksames Wissen zu den notwendigen Grundlagen einer nachhaltigen Lebensweise (Sachwissen), zum anderen geht es um die Erfolgsbedingungen, unter denen politische Prozesse zum Erreichen einer nachhaltigen Gesellschaft führen (Prozesswissen)"

"Wissen und demokratisches Regieren im Anthropozän: Was sind Bedingungen für gelingende Nachhaltigkeitstransformationen?"

"Nicht Belehrung steht im Vordergrund, sondern Hilfestellung für Entscheidungsträgerinnen und Entscheidungsträger in Wirtschaft, Politik und Zivilgesellschaft, um die notwendigen Entscheidungs- und Umsetzungsprozesse so zu gestalten, dass der Weg in eine nachhaltige Zukunft gelingt. Denn nur dann haben wir und folgende Generationen Aussicht auf eine lebenswerte Welt."

### Umweltbundesamt
...

### „Konsumbürger“: Engagement jenseits des eigenen Konsums
* https://www.umweltbundesamt.de/themen/wirtschaft-konsum/konsum-umwelt-zentrale-handlungsfelder, 2019
    * ...
    * "Das Engagement am Arbeitsplatz ermöglicht oft Einsparmöglichkeiten, die weit über den persönlich zurechenbaren CO2e-Ausstoß hinausgehen."

Videos für Einsteiger
---------------------
### Louis C.K. - Polar bears, faster, money, bacon
* Video: [Louis C.K. "If God Came Back"](https://www.youtube.com/watch?v=WrahQpIWD08), 2013, 2 min
    * "If you believe that God gave you Earth, that God created the Earth for you, why not look after it?"
        * Video: ["Gary Yourofsky Talks About God in Israel"](https://www.youtube.com/watch?v=OZX9VUeKNc4), 2015, 4 min
            * "God is not in a Wall", "God is in his creations", "worship everything that we made in his name?", "Every animal that God made, God is inside of",
    * "What did you do to the polar bears?"
    * "Cus I wanted to go faster"
    * "Jobs"
    * Money
    * Food on the ground
    * "it doesn't have like Bacon around it"

### Bill Maher - Earth vs. Mars
* Video: ["Make Earth Great Again | Real Time with Bill Maher"](https://www.youtube.com/watch?v=mrGFEW2Hb2g), 2017, 8 min
    * Earth vs. Mars
    * ...
    * "You want to explore something cold and hard? How about the facts."
    * ...
    * Scientific chart Mars vs. Earth
    * ...

* Other Videos
    * "I Didn't Reproduce Day", https://www.youtube.com/watch?v=6ZxzTLcsEEo, 2017, 5 min

### Sammlung: Filme für die Erde
https://filmsfortheearth.org/de/

### Cartoons
* siehe wachstum.md

Lesch
-----
### Lesch beim 9. Hessischen Klimaempfang 2019
* https://www.youtube.com/watch?v=F4jDk2MPZbA, HLNUG, 50 min, @Regierung, @Zweifler, Motivator
    * 14. Mai 2019
    * mit Link zu Rede Prof. Schellnhuber 2018: auf dem Klimaempfang 2018: https://youtu.be/br6f_xy3quo
    * gute Rede: Einführung, Meta-Infos, Geschichte, Wissenschaft etc.
    * Warum tun... so wenige Leute was?
    * ...
    * Kapitalozän
    * ...
    * 8 min: Powerpointkoma
    * ...
    * Die Wissenschaft schreibt und erklärt viel, aber es ändert sich wenig
        * "Wir brauchen eine psychologische Veränderung und wir brauchen eine Hinwendung zu einem Lebensmodell, das deutlich anders funktioniert als alles, was wir bis jetzt getan haben!"
    * ca. 13:00: Dinge wurden monetarisiert, die nie hätten monetarisiert werden dürfen!
        * keine ethischen Grundlagen für z. B. Quantenmechanik und Digitalisierung
        * "so schnell wie heute waren wir noch nie"
        * ...
    * "Kontakt verloren zu all den Quellen des Wohlbehagens in dieser Welt"
        * Wasser, Strom, Internet
        * nimmt man einfach hin
        * Vertrauen in Technologie, die gebaut wurde durch die Erkenntnisse derselben Wissenschaft, die sagt: "Dieser Planet ist begrenzt"
    * 15:00 Frage an ein Unternehmen (zum Thema Verantwortung): Wo kriegen Sie Ihre Rohstoffe her? -> Die werden geliefert.
    * 15:30 Handy klingelt
    * 16:40 Wachstumsbegriff
        * historisch positiv konnotiert
        * größere Gruppe ist sicherer als eine kleine
        * mehr Nahrungsvorräte ist besser als wenig
        * die Zeiten sind aber nun vorbei, wir können uns aber wohl nicht trennen
    * 17:20: Sauberer Standpunkt / Ethische Reinheit der FFF-Forderungen
        * eine Generation fordert ihr Recht / Würde
        * Erwachsenen zerstören faktisch (willentlich oder unwillentlich) die Zukunftsmöglichkeiten der neuen Generation
        * 18:40: "Ach die Schüler, die haben ja auch keine Lösungsvorschläge. ... Ich dachte, ich hab sie nicht mehr alle."
            * ...
            * Hilflosigkeit... mit der Reinheit des ethischen Arguments umzugehen
    * 19:45: Politiker haben dauernd mit Interessen zu tun
        * es ist für Politiker schwierig, sich nun nicht mehr mit individuellen Interessen zu beschäftigen, sondern mit einem existenziellen Interesse einer großen Gruppe
    * 20:10: erwartet, dass bald auch die Erwachsenen endlich auf die Straße gehen
    * 23:00: Beschleunigung; Vergangenheit zur Gegenwart (indem wir Öl verbrennen)
    * 24:00: In Diskussionen offensiv Klimazweifler aufklären (z. B. selbe Phyisk wie in der Technik)
        * 27:30: klar machen, dass über die Effekte des Treibhauseffekts nicht mehr diskutiert werden muss; das ist gesetzt
        * "diese Leute sind nicht satisfaktionsfähig"
    * ca. 30:00: Morgan-Stanly-Bank-Typ: "Don't be carried away with humanistic philosophy" (Würde, Menschenrechte, Freiheiten)
        * Es gilt die totalitäre Welt der Shareholder => Kapitalozän
    * 34:00: in was investieren die Leute, die viel Geld haben, dieses Geld?
        * Wir brauchen richtig viel Geld, um ökologische Wenden hinzubekommen in der erforderlichen kurzen Zeit
    * 36:00: statt langfristig zu agieren, wird immer nur geschaut wie man dies oder jenes jetzt möglichst schnell hinbekommt.
        * weil Wirtschaftssystem immer nur **auf unmittelbare Renditen optimiert**
        * wo ist der **Glaube und Zuversicht in langfristige Zukunft**?
        * auch die Zukunft wird immer näher an die Gegenwart geholt
            * ein Business-Case muss innerhalb möglichst kurzer Zeit ein Return-on-investment liefern
    * 39:00: Verweis auf WBGU zu Digitalisierung als Brandbeschleuniger, wenn nicht politisch gestaltet
    * Gesellschaft muss "mehr auf weniger setzen"
        * großer gesellschaftlicher Impuls
        * weniger
        * z. B. mehr Feiertage im Jahr; Zwangsfeiertage wo man wirklich daheim bleibt (bzw. zu Fuß zum Nachbarn rüber)
        * wieder "bei sich sein", anstelle dauernd Kurzurlaub
        * regelmäßig Offline sein; selbstverordneter Sabbat
        * Morgens Zeit haben, mal jemand auf der Straße zu fragen wie es ihm geht.
        * eine entschleunigte Republik
        * Mal mit solchen Utopien nachdenken
        * "achte auf das, was dir gegeben wurde"
        * einfach da sein, bei und mit Leuten, die man gern hat
        * Hannah-Arendt-Fan
            * nicht zuhause rumhocken, sondern in den öffentlichen Raum gehen! Politik ist nicht nur für Politiker
    * 42:00: vielleicht mal weniger sich auf die ökonomischen berechenbaren Quantitäts-Ziele einlassen
        * "der Mensch ist ein Qualitätswesen"
        * "wir leben nicht von dem, was man messen kann, sondern eher von dem, was man nicht messen kann"

### Lesch beim Diözesanempfang 2019
* Video: ["Diözesanempfang 2019 - Vortrag von Prof. Dr. Harald Lesch"](https://www.youtube.com/watch?v=_ccCrhWhkTQ), 53 min, Würzburg
    * "Die Sorge um unsere Erde bestimmt das Lebensgefühl vieler Menschen und ist Thema im öffentlichen Diskurs. Der Mensch beeinflusst durch sein Verhalten massiv die Natur. Deutliche Warnungen kommen nicht von apokalyptischen Visionären, sondern aus der nüchternen Wissenschaft. Dieser unausweichlichen Brisanz steht das zögerliche Verhalten von Individuen und vielen gesellschaftlichen Gruppen entgegen."
    * schöner Vortragsstil, guter Einstieg
    * Umweltethik, Technikethik
        * https://wirtschaftslexikon.gabler.de/definition/umweltethik-54001
    * Umweltbegriff
        * (Natur verändern können, ohne sie zu verändern?)
        * z. B. statt "Mitbewohner" "Umbewohner" -> Mitweltministerium
            * wir hätten verstanden, dass wir _mit_ der Natur zusammenleben müssen
        * "Natur völlig unterschätzt"
        * Eingriff in jahrtausendealte Kreisläufe mit dem Glauben, dass dann nichts passiert
    * ...weiter mit 14:00 min... TODO
    * ...
    + ...
    * 5 Minuten morgens um 9:30 Uhr alle halten inne
    * Astronomie, sehr genau
    * kompliziert vs. komplex
    * CO2 vs. FCKW, ppm
    * Moleküle
    * Handel mit Lichtgeschwindigkeit
    * Wirtschaftswunderland-Narrativ: alles muss wachsen
        * seit 1972 Club of Rome; eigentlich bräuchten wir ein anderes Narrativ, aber davor haben Menschem mit Geldgier Angst
    * Vergangenheit (fossil) und Zukunft (schnelle Rendite) werden in die Gegenwart geholt; langfristiges Denken geht da nicht
    * Natur, Naturgesetze, nicht demokratisch
    * kommt sich manchmal vor wie der Hofnarr
    * Alptraum
    * ...
    * Schnelligkeit: früher hatten Leute einen Piepser bei denen es um Leben und Tod geht (Ärzte, Hebammen)
        * heute ist jeder mit seinem Smartphone auf 180 und meint bei ihm ginge es um Leben und Tod
    * Digitalisierung kostet mächtig Strom
    * ...
    * 45:40: "Die Digitalisierung ist doch keine Bewegung um die Welt besser zu machen, sondern alleine dazu da noch mehr Geld zu generieren."
        * 46:25: Wir müssen aus Kohlenstoff raus. Das ist hinreichend klar.
        * Für den Ausbau der Digitalisierung werden aber immer mehr Geräte benötigt, deren Verbrauch schneller wächst, als erneuerbare Energien nachgebaut werden können.
        * Es wird in D. nicht mehr über das **Energiesparen** diskutiert => irgendwas läuft komplett falsch!
            * Das ist aber das, was nötig ist! Wie verbrauchen wir weniger Energie? Viel weniger.
            * "Daran sehen Sie, dass Technik und Naturwissenschaften da gar nicht die Lösung in erster Linie sind!"
        * "Stellen Sie sich ein Land vor, dass aus ökologischen Gründen sagt: wir brauchen mehr Feiertage"
            * 1 für Atmosphäre, 1 Boden, 1 Flüsse, 1 Eis, 1 Spaß
            * an diesen Feiertagen bleibt Deutschland zuhause!
            * Autobahnen sind autofrei (abgesehen von Rettungsfahrzeugen)
            * wir gönnen uns eine freiwillige Einschränkung und bleiben an diesen Tagen zuhause
            * reden mit unseren Um- oder Mitbewohnern
            * das würde zeigen: wir haben verstanden
    * 49:25: was sicherlich falsch ist, ist weitermachen wie bisher
        * insbesondere das, was Wissenschaft und Technik längst rausgefunden haben, uns massiv schadet
    * 50:00: Kapitalozän
    * 51:00: "2018 - Das Jahr, in dem wir (wieder) Kontakt aufnahmen" (mit der Natur)
    * 52:00: wir können nochwas machen / Verantwortung; wir als eines der reichsten Länder sollten uns dabei all die Fehler leisten, die sich andere, ärmere Länder nicht leisten können
        * wir müssen ihnen helfen, diese Transformation viel schneller zu durchlaufen als wir das im Moment machen (wenn wir mal so richtig anfangen)
        * wir werden viele neue Prozeduren als erste entwickeln und werden das dann möglicherweise verschenken müssen
        * eine ganz neue Art von Weltwirtschaft, wenn wir das wirklich ernst nehmen, was da draußen passiert
    * ...

### Finanzen
* siehe Lesch dort

Ökologischer Fußabdruck
-----------------------
* ["Ökologischer Fußabdruck und Biokapazität"](http://www.bpb.de/nachschlagen/zahlen-und-fakten/globalisierung/255298/oekologischer-fussabdruck-und-biokapazitaet), 2016
    * Zahlen von 2012
* https://de.wikipedia.org/wiki/%C3%96kologischer_Fu%C3%9Fabdruck
* [Teste deinen Ökologischen Fußabdruck](http://www.fussabdruck.de) von Brot für die Welt
    * Testergebnis 2018:
        * "4.0 gha"
            * nachhaltig: 1.7 gha
            * Deutschland Durchschnitt: 5.3 gha
            * Welt Durchschnitt: 2.8 gha
            * "Wenn alle Erdenbürger Deinen Fußabdruck hätten, bräuchten wir dafür 2.3 Planeten."
        * Tipps:
            * "Zu Deinem persönlich beeinflussbaren Fußabdruck wird ein Sockelbetrag von 0,8 gha addiert. Dieser kollektive Fußabdruck steht für die Infrastruktur in Deutschland (z.B. Straßen und Krankenhäuser). Diesen Teil Deines Fußabdrucks kannst Du indirekt beeinflussen, z.B. indem Du Dich für die Energiewende, öffentliche Verkehrsmittel oder für öko-faire Beschaffung in öffentlichen Einrichtungen einsetzt."
            * "Super, Du isst kein Fleisch! Das ist gut für die Umwelt und Deinen ökologischen Fußabdruck. Vielleicht können andere von Deinen Erfahrungen profitieren und Du kannst ihnen vegetarischen Genuss schmackhaft machen?"
            * "Respekt: Du kommst ganz ohne tierische Produkte aus. Vielleicht kannst Du helfen anderen ihre Berührungsängste vor veganer Ernährung zu nehmen."
            * "Du isst kein Fisch. Gut! Da freut sich der Fisch und Dein Fußabdruck."
            * "Dir ist bio wichtig! Vielleicht kannst Du anderen Tipps geben, wie sie ihren Bio-Anteil am leichtesten vergrößern können.Wie wäre es mit Gemüse, Obst oder Kräutern im Garten oder auf dem Balkon?"
            * "Du isst regional und saisonal. Das braucht eine Menge Wissen. Organisiere doch einen saisonalen Kochkurs und gib etwas davon weiter!"
            * "Du bist ein Vorbild bei der Wertschätzung von Lebensmitteln. Fordere auch den Lebensmittelhandel auf, Verschwendung zu vermeiden!"
            * "Fliegen ist richtig übel für das Klima. Toll, dass Du ohne auskommst."
        * https://info.brot-fuer-die-welt.de/blog/brot-welt-jugend-ist-gegruendet

"Most effective individual steps"
---------------------------------
* siehe fortpflanzung.md

Die Allmende-Herausforderung
----------------------------
### Das Allmende-Problem
* http://globale-allmende.de/gesellschaft/allmendeproblem
    * "Ostrom identifizierte das Allmendeproblem als eine Unterkategorie des sogenannten "sozialen Dilemmas", das immer dann vorhanden ist, **wenn in einer Gruppe egoistische Einzelentscheidungen zu schlechteren Einzel- und Gruppenergebnissen führen als kooperierende Entscheidungen**."
    * "Das Allmendeproblem tritt auf, wenn eine öffentliche Ressource durch egoistisches Handeln überlastet oder zerstört wird, wodurch der Gruppennutzen (Summe der Einzelnutzen) letztlich geringer wird."
    * "Hierzu gibt es auch den Begriff "Schwarzfahrerproblem": Der Einzelne mag sich frei bedienen wollen, ohne sich an den Kosten / dem Aufwand der Erhaltung zu beteiligen."
* "Beispiel: Die "Tragik der Allmende"" - https://www.forschungsinformationssystem.de/servlet/is/328924/
    * "im Mittelalter die Dorfwiese auf der jeder Dorfbewohner sein Vieh weiden lassen konnte. Die Allmende war also Gemeineigentum und durfte von allen frei benutzt werden. Dies führte in der Regel zu einer Übernutzung."
    * "Ohne Absprachen oder höhere Instanz wird ein rationaler Entscheider sich so viel wie möglich von diesem Gut beschaffen"
    * "Da sich erwartungsgemäß alle Individuen so verhalten, kann dies zu nachhaltigen Schäden an der Ressource führen."
    * ["Überfischung der Weltmeere"](../Vegan/fisch-fischer-überfischung.md)
    * "Nutzung der Atmosphäre als Senke für Schadstoffe und Treibhausgase"
    * "Öffentliche Straßen bei Staugefahr"
    * "Plünderung von Wildtierbeständen vor allem in Entwicklungsländern"
    * "Trinkwasservorkommen vor allem in Entwicklungsländern"
    * Lösungsansätze: "Lösungsansätze dieses Problems sind klassischerweise Quoten, Steuer oder Zertifikate die durch staatlich oder suprastaatlich durchgesetzte Strafen bei Verstößen implementiert werden. Auch die Verstaatlichung oder anderweitige eindeutige Eigentumszuteilung der Ressource kann eine Lösung sein."

* https://de.wikipedia.org/wiki/Tragik_der_Allmende

* 2018: ["Von der Tragik zur Komödie der Allmende - über Gemeingüter, Open Source und freies Wissen"](https://www.heise.de/newsticker/meldung/Missing-Link-Von-der-Tragik-zur-Komoedie-der-Allmende-ueber-Gemeingueter-Open-Source-und-freies-4259557.html)
    * nur die ersten zwei Seiten interessant; über Wege die Allmende zu erhalten und als Mensch zu interagieren
    * "Vor 50 Jahren stellte Garrett Hardin seine vielbeachtete These auf, Kollektivgüter seien dem Untergang geweiht. Gilt dies in der digitalen Wissensgesellschaft?"
    * https://de.wikipedia.org/wiki/Garrett_Hardin
    * "Eine endliche Welt kann nur eine endliche Bevölkerung unterstützen; daher muss das Bevölkerungswachstum letztendlich gegen Null gehen."
    * "Er versteht die Erde als ultimatives Beispiel einer Allmende, also eines kollektiv genutzten Gutes, das letztlich wegen Überbeanspruchung der natürlichen Ressourcen dem Untergang geweiht ist"
    * "Auch die sonst als Allheilmittel gefeierte und eingesetzte moderne Technik könne dieses Schicksal angesichts der Grenzen des Wachstums langfristig nicht abwenden."
    * "Hardin schreibt aber selbst, dass die Privatisierung kein Allheilmittel sei und just etwa beim Allmende-Gut der Umwelt versage: Ein Fabrikbesitzer schert sich ihm zufolge im Eigeninteresse zunächst nicht darum, dass er dreckiges und gesundheitsgefährdendes Abwasser in einen Fluss einleitet oder Giftmüll auf einer öffentlichen Halde entsorgt."
    * https://de.wikipedia.org/wiki/Elinor_Ostrom, siehe unten
    * "dass Privatregimes nicht immer das A und O sind und gemeinschaftliches Eigentum von Nutzerorganisationen durchaus erfolgreich verwaltet werden kann", Nobelpreis
    * "Eine Selbstregulierung von Allmende-Gütern "jenseits von Markt und Staat" funktioniert laut Ostrom nur, wenn einige Voraussetzungen gegeben sind"
        * ...

* https://de.wikipedia.org/wiki/Elinor_Ostrom
    * "Ostrom war weltweit angesehen als eine führende Forscherin im Bereich der Umweltökonomie. Sie setzte sich mit der Frage auseinander, **wie Menschen in und mit Ökosystemen nachhaltig interagieren können**. Inhaltlich befasste sie sich u. a. mit der Fischereiwirtschaft, mit Bewässerungssystemen, mit Wald- und Weidewirtschaft, in späteren Arbeiten auch mit Wissen und der Problematik des geistigen Eigentums. Ostroms Forschung befasste sich mit der Frage, wie sich Menschen organisieren, um gemeinschaftlich komplexe Probleme zu lösen. Sie analysierte, wie institutionelle Regeln sich auf Handlungen von Individuen auswirken, die bestimmten Anreizen ausgesetzt sind, Entscheidungen treffen (müssen), und sich zudem noch gegenseitig beeinflussen, und sie zeigte praktikable, gerechte und effiziente Lösungen für diese Probleme auf."
    * Buch: "Die Verfassung der Allmende. Jenseits von Staat und Markt.", 1999, Elinor Ostrom, Mohr Siebeck
        * "Das Buch erschien 1999 in deutscher Sprache"
        * http://www.socioeco.org/bdf_fiche-publication-1167_en.html
            * ... TODO ...
        * ...
    * https://de.wikipedia.org/wiki/Elinor_Ostrom#Governing_the_Commons_(1990) - "»Elinor’s Law« – Design-Prinzipien"
        * = https://www.band2.dieweltdercommons.de/essays/elinors_law-design_prinzipien.html
        * ... TODO ...
    * https://wtf.tw/ref/ostrom_1990.pdf, Elinor Ostrom
        * "Buch Governing the Commons: The Evolution of Institutions for Collective Action (1990)"

* Buch: "Was mehr wird, wenn wir teilen - Vom gesellschaftlichen Wert der Gemeingüter", Elinor Ostrom, Silke Helfrich (Hrsg.), 2011
    * Verlag: https://www.oekom.de/buch/was-mehr-wird-wenn-wir-teilen-9783865812513?p=1
    * Online verfügbar: https://www.solawi.ch/wordpress-solawi/wp-content/uploads/was_mehr_wird_wenn_wir_teilen.pdf
    * ... todo ...
    * S. 84: "Wir alle müssen verstehen, dass jeder Einzelne an derpermanenten Gestaltung eines regelbasierten  Gemeinwesens teilhat. / ::demokratie, ::freiheit
        Die Bürgerinnen und Bürger müssen die Kunstdes sich »Zusammentuns« erlernen. Wenn dies nicht gelingt,dann waren alle Forschung und alles theoretische Bemühen vergebens."
    * ...
    * Rezension: http://www.wloe.org/fileadmin/Files-DE/PDF/Buecher/rezension_ostrom.pdf

* Buch_: Elinor Ostrom's Rules for Radicals: Cooperative Alternatives beyond Markets and States von Derek Wall, 2017

### Ist das ein Problem des Wirtschaftssystems? / Markt und Regulierung
Zunächst: Die Marktwirtschaft (egoisitische Einzelentscheidungen) hat sich bewährt,
wenn es um optimale Resourcenallokation **privater** Güter geht.

ABER:

Es gibt notwendige Randbedingungen:

* Die Vermögen sind nicht krass ungleich verteilt (derzeit ist leider nur eine Verschärfung zu beoachten). TODO: Quelle
* Die Verteilung frei verfügbarer, begrenzter Resourcen muss irgendwie zentral geregelt werden.
    * Die naheliegende Lösung, alles zu privatisieren, ist nicht zielführend.
    * TODO: eine Quelle angeben, warum nicht -> "Die Welt als Allmende: marktwirtschaftlicher Wettbewerb und Gemeingüterschutz"?

Fazit:

* Wer als Publizist _ohne zu differenzieren_ nach weniger Staat, weniger Regulierungen und mehr Privatisierungen
    ruft (oder den Eindruck erwecken will, Regulierungen seien von Grund auf schlecht,
    weil der Markt von sich aus alles regeln kann), der handelt nicht im Interesse des Gemeinwohls.
* Die Lösungen sind nicht einfach. Sie müssen mit gutem Willen hart erarbeitet werden.
* Es ist per se kein Problem des Wirtschaftssystems.

### TODO: Die Welt als Allmende: marktwirtschaftlicher Wettbewerb und Gemeingüterschutz
* https://www.bpb.de/apuz/33212/die-welt-als-allmende-marktwirtschaftlicher-wettbewerb-und-gemeingueterschutz?p=all, 2011
    * **Der Substanzverzehr an den globalen Gemeingütern wird von der Wettbewerbs­ordnung wie eine erwünschte Marktleistung behandelt.**
        **Die Erhaltung der Ge­meingüter erfordert deshalb eine staatliche Revision der Wettbewerbsordnung.**
    * ...TODO...
    * ...
    * ...

### Elinor Ostrom und die Wiederentdeckung der Allmende
* https://www.bpb.de/apuz/33204/elinor-ostrom-und-die-wiederentdeckung-der-allmende?p=all, 2011
    * "Eigennutz führe zur Übernutzung von Gemeingü­tern. Ostrom hat gezeigt, dass es auch anders geht."
    * "Komplexität ist nicht gleich Chaos"
    * "Spielregeln für Kooperation"
    * "Übersicht: Elinor Ostroms "Design-Prinzipien" erfolgreicher Allmenden"
        * Abgrenzbarkeit
        * Kohärenz mit lokalen Bedingungen
        * Gemeinschaftliche Entscheidungen
        * Monitoring
        * Abgestufte Sanktionen
        * Konfliktlösungsmechanismen
        * Anerkennung von Rechten
        * Verschachtelte Institutionen
    * "Keine Patentrezepte"
    * ...TODO...

* https://www.bpb.de/apuz/33214/die-allmendeklemme-und-die-rolle-der-institutionen-oder-wozu-maerkte-auch-bei-tragoedien-taugen?p=all, 2011
    * "Die Allmendeklemme und die Rolle der Institutionen. Oder: Wozu Märkte auch bei Tragödien taugen"
    * ...(todo)... Märkte sind auch ein Baustein

### Weitere Beispiele: Weltraumschrott
* [tagesschau-Video](http://www.tagesschau.de/multimedia/video/video-279861.html), 2 min, 17.04.2017
    * Konferenz in Darmstadt
    * Optimale Lösung: Jeder Betreiber sollte seinen Satellit am Ende der Lebenszeit mit dem Resttreibstoff selber zum Verglühen in die Erdatmosphäre senden, solange der Satellit _noch unter Kontrolle_ ist.

Fortschrittsversprechen als Ablenkung
-------------------------------------
(Technikversprechen)

Hinweis: Jeder Fortschritt, egal wie klein ist gut. Problematisch ist es nur, wenn Ankündigungen von (oder Hinweise auf Erwartung von) Fortschritt genutzt werden, um größere Probleme gar nicht erst angehen zu müssen. Siehe auch Rebound-Effekt.

### Beispiel Tierwohl in der Tierhaltung
* http://agrarlobby.de/propaganda/fortschrittsversprechen/

### Beispiel Überfischung
* [Omega-III-Fettsäuren - Überfischung: Technik von DSM und Evonik, die Fische rettet](http://www.badische-zeitung.de/wirtschaft-3/ueberfischung-technik-von-dsm-und-evonik-die-fische-rettet--134762714.html), Badische Zeitung 2017
    * "Die Nachfrage nach Fisch als Nahrungsmittel steigt – unter anderem wegen der Omega-III-Fettsäuren. Überfischung ist die Folge. Ein neues Verfahren könnte diese gefährliche Kette sprengen."
    * Einschätzung:
        * überdeutliche Betonung, dass der Fischfang nötig wäre, weil der Mensch sonst nirgends sein Omega-III herbekommt.
        * minimaler Effekt für Umwelt und Tiere
    * User-Comment: "Welcher anständige Mensch nimmt denn noch Omega3-Öl aus Fisch? Es gibt doch inzwischen pflanzliche Alternativkapseln aus Leinöl."
    * bessere Lösung: ["Nicht der Lachs soll Vegetarier werden – sondern der Mensch: PETA kritisiert Pläne des Chemiekonzerns Evonik"](http://www.peta.de/nicht-der-lachs-soll-vegetarier-werden-sondern-der-mensch-peta-kritisiert-chemiekonzern-evonik), 2017

### Beispiel Breakthrough Institute
* https://www.sourcewatch.org/index.php/Talk:Breakthrough_Institute
    * ""a project of Rockefeller Philanthropy Advisors", is a think tank established by Michael Shellenberger and Ted Nordhaus"
* "The Breakthrough Institute's Inconvenient History with Al Gore", https://ethics.harvard.edu/blog/breakthrough-institutes-inconvenient-history-al-gore, 2014
    * ...

### Beispiel biooekonomie.de
Einschätzung:

* gute Ansätze
    * http://biooekonomie.de/nachrichten/klee-pellets-als-gemueseduenger
* aber auch vieles Seltsames / wirkt ein bisschen Klein-Klein / eher Symptom-Bekämpfung als Ursachenbehebung
    * http://biooekonomie.de/nachrichten/digitalisierung-evonik-setzt-auf-watson
        * siehe auch Überfischung oben
    * Gentechnik recht unreflektiert:
        * http://biooekonomie.de/nachrichten/berlin-dialog-zum-genome-editing-gestartet
            * "Durch neue Zuchtmethoden zu mehr Tierwohl?"
        * http://biooekonomie.de/nachrichten/genschere-zu-hoeheren-rapsertraegen
        * http://biooekonomie.de/nachrichten/crispr-technik-laesst-erbgut-leuchten

Vorbilder
---------
Beispiele vorbildlicher Unternehmen. Zum Beispiel ausgedrückt in der freiwilligen Erstellung einer Gemeinwohlbilanz.

### Mit Gemeinwohlbilanz / Gemeinwohlökonomie
* Was ist das?
    * siehe "BKK Provita + Gemeinwohlökonomie-Video"
    * https://de.wikipedia.org/wiki/Gemeinwohl-Bilanz
        * Liste mit Unternehmen
        * Kritik: https://de.wikipedia.org/wiki/Gemeinwohl-%C3%96konomie
    * https://wetell-change.de/2019/03/27/gwoe/
        * "Falls ihr noch nie von der Gemeinwohlökonomie (GWÖ) gehört habt: Eine Gemeinwohlbilanz ist eine umfassende Unternehmensbewertung mit Fokus auf dem Beitrag des Unternehmens zum Gemeinwohl statt auf Profitmaximierung. Eine gute Übersicht der Kriterien bietet die Gemeinwohl-Matrix."
    * Gemeinwohl-Matrix: https://www.ecogood.org/de/gemeinwohl-bilanz/gemeinwohl-matrix/
    * Gemeinwohl-Bilanz Beispiele
        * https://www.ecogood.org/de/gemeinwohl-bilanz/unternehmen/gemeinwohl-bilanz/bilanzbeispiele/
            * ...
            * Christian Felber
                * "Christian Felber über die Gemeinwohl-Ökonomie - Jung & Naiv: Folge 459": https://www.youtube.com/watch?v=7mRe1ntgbj8, 2020, > 2h
            * Kirchner Konstruktionen GmbH
            * Ökofrost GmbH
            * Sparda Bank München
            * Sonnentor
                * siehe auch "Vegan ist ungesund zu Besuch bei SONNENTOR"

* https://wetell-change.de/2019/03/27/gwoe/
    * TAZ
    * Vaude
        * https://www.sazbike.de/inside/vaude-sport-gmbh-co-kg/un-wirtschaftskommission-europa-zieht-vaude-vorbild-heran-1697301.html
            * "UN-Wirtschaftskommission für Europa zieht Vaude als Vorbild heran", 2019
    * BKK ProVita
    * Shiftphones
    * WEtell
