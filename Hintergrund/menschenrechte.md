Menschenrechte
==============

<!-- toc -->

- [Einleitung](#einleitung)
- [Verstöße durch Unternehmen](#verstosse-durch-unternehmen)
- [Menschenrechte für die Frau](#menschenrechte-fur-die-frau)
- [Verschiedenes](#verschiedenes)
  * [Tagespolitik, Deutsche Verantwortung, Profit durch Duldung](#tagespolitik-deutsche-verantwortung-profit-durch-duldung)
  * [Recht auf Privatsphäre vs. Nichts zu verbergen](#recht-auf-privatsphare-vs-nichts-zu-verbergen)
  * [Philosophie: Do Human Rights Actually Exist?](#philosophie-do-human-rights-actually-exist)
  * [Menschenwürde](#menschenwurde)
  * [Falun Gong](#falun-gong)
  * [Palästina](#palastina)
  * [Folter](#folter)
- [Internationale Institutionen / Menschenrechte](#internationale-institutionen--menschenrechte)
  * [Internationale Strafgerichtshof (ICC)](#internationale-strafgerichtshof-icc)
  * [PEN, Writers in Prison](#pen-writers-in-prison)

<!-- tocstop -->

Einleitung
----------
* siehe philosophie.md / Humanismus
* siehe spenden-atlas.md
* siehe "Und was hat das mit mir zu tun? - Zehn Gründe, warum dir die Menschenrechte nicht egal sein können"
* Menschenrechte gelten bedingungslos, siehe auch diskriminierung-speziesismus.md
    * korrekt?

Verstöße durch Unternehmen
--------------------------
* siehe "Sammlung zu Kohlestrom und Kohleabbau"
* 2017: ["Bericht 2017: Globale Energiewirtschaft und Menschenrechte"](http://germanwatch.org/13958), Germanwatch und Misereor
    * "Deutsche Unternehmen und Politik auf dem Prüfstand"
    * siehe klimawandel.md / Kohle
* siehe marken-multinational.md
* siehe schmuck-README.md / Gold

Menschenrechte für die Frau
---------------------------
* siehe diskriminierung-sexismus.md

Verschiedenes
-------------
### Tagespolitik, Deutsche Verantwortung, Profit durch Duldung
* 2019
    * Nov. 2019: China Cables
        * https://www.tagesschau.de/inland/china-cables-uiguren-107.html
            * Adrian Zenz: Kultureller Genozid
                * https://www.tagesschau.de/investigativ/ndr/china-cables-uiguren-103.html
            * "Ignorieren deutsche Firmen die Unterdrückung?" - https://www.tagesschau.de/investigativ/ndr/china-cables-wirtschaft-101.html
        * Internierungslager
        * https://www.cfr.org/backgrounder/chinas-repression-uighurs-xinjiang - "China’s Repression of Uighurs in Xinjiang"
        * https://de.wikipedia.org/wiki/China_Cables
            * https://www.icij.org/investigations/china-cables/exposed-chinas-operating-manuals-for-mass-internment-and-arrest-by-algorithm/
            * https://www.focus.de/politik/ausland/china-cables-geheime-dokumente-zeigen-das-schockierende-ausmass-von-chinas-internierungslagern_id_11385626.html

    * "Trickserei mit Menschenrechten Von "Bald-Erfüllern" und "Fast-Erfüllern"", https://www.tagesschau.de/investigativ/monitor/menschenrechte-wirtschaftsministerium-101.html
        * "Das Wirtschaftsministerium sperrt sich offenbar gegen ein Gesetz, das Unternehmen verpflichten würde, auf die Einhaltung von Menschenrechten zu achten. Das zeigen Recherchen des ARD-Politikmagazins Monitor."

    * "Reportage Was geschah an Bord der "Sea-Watch 3"?", https://www.tagesschau.de/investigativ/panorama/sea-watch-rackete-111.html
        * "Das Sterben im Mittelmeer wurde lange verdrängt - "Sea Watch-3"-Kapitänin Rackete machte es wieder zum Thema. Reporter von STRG_F und Panorama waren mit an Bord und erzählen die Geschichte hinter den Schlagzeilen."
        * ...

### Recht auf Privatsphäre vs. Nichts zu verbergen
* https://de.wikipedia.org/wiki/Nichts-zu-verbergen-Argument
    * "Zu argumentieren, dass Sie keine Privatsphäre brauchen, weil Sie nichts zu verbergen haben,
        ist so, als würden Sie sagen, dass Sie keine Meinungsfreiheit brauchen, weil Sie nichts zu sagen haben.“ – Edward Snowden

* https://de.wikipedia.org/wiki/Privatsph%C3%A4re
    * "Privatsphäre bezeichnet den nichtöffentlichen Bereich, in dem ein Mensch unbehelligt von äußeren Einflüssen
        sein Recht auf freie Entfaltung der Persönlichkeit wahrnimmt.
        Das Recht auf Privatsphäre gilt als Menschenrecht und ist in allen modernen Demokratien verankert."

* https://de.wikipedia.org/wiki/Freie_Entfaltung_der_Pers%C3%B6nlichkeit
    * "Artikel 2 des Grundgesetzes für die Bundesrepublik Deutschland garantiert die freie Entfaltung der Persönlichkeit. [...] Die Entfaltung der Persönlichkeit stellt ein erstrebenswertes, wenn nicht sogar das höchste Gut im Rahmen einer radikal humanistischen Weltanschauung dar. Erich Fromm etwa sieht in seinem Werk Haben oder Sein die Entfaltung der eigenen Persönlichkeit und der des Mitmenschen als das höchste Ziel des menschlichen Lebens an."

* https://www.amnesty.de/allgemein/und-was-hat-das-mit-mir-zu-tun
    * "Zehn Gründe, warum dir die Menschenrechte nicht egal sein können"
    * 9. "Mir ist es egal, dass meine Daten gesammelt werden, ich habe nichts zu verbergen."
        * Auch wenn vor 70 Jahren noch niemand ahnte, dass wir heute dauerhaft online sind: Der Schutz unserer Privatsphäre war von Anfang als Menschenrecht verankert.
        * Wenn uns beim Chatten mit dem Smartphone jemand zu nah über die Schulter schaut, empfinden wir das ganz klar als übergriffig, aber dass Firmen und Staaten uns stärker ausspähen als jemals zuvor, ist uns oft gar nicht richtig bewusst. Wir lassen zu, dass fast alle unserer täglichen Schritte detailliert verfolgt werden.
        * Mehrere neue Polizeigesetze in Bayern und anderen Bundesländern machen es leicht, dich ohne Anlass zu überwachen. Mit wem du telefonierst, welche Seiten du im Internet besuchst – das kann schon reichen, dich für die Polizei verdächtig zu machen. Es ist also wichtig, für dein Recht auf Privatsphäre zu kämpfen.

* Datenschutz ist sowohl ein Grundrecht (https://de.wikipedia.org/wiki/Informationelle_Selbstbestimmung),
    als auch ein internationales Menschenrecht (https://www.heise.de/tp/news/Datenschutz-ist-Menschenrecht-2102696.html)

### Philosophie: Do Human Rights Actually Exist?
* https://www.youtube.com/watch?v=jv-daraEJu8 - "Do Human Rights Actually Exist?", 2019, 20 min, CosmicSkeptic
    * religious man vs. athetist
    * Menschenrechte existieren nicht wirklich, aber es gut, wenn man sich so verhält, als ob sie existierten
    * ...

### Menschenwürde
* https://www.grundrechteschutz.de/gg/menschenwurde-2-255
    * "Als Menschenwürde versteht man die Vorstellung, dass alle Menschen unabhängig irgendwelchen Merkmalen wie etwa Herkunft, Geschlecht oder Alter denselben Wert haben, da sie sich alle durch ein dem Menschen einzig gegebenes schützenswertes Merkmal auszeichnen, nämlich die Würde. Seine Verankerung als erste Norm des Grundgesetzes ist eine bewusste Reaktion auf die massive Missachtung der Menschenwürde im Nationalsozialismus."
    * "Aus der Menschenwürde ergibt sich nach dem Verständnis des Bundesverfassungsgerichts der Anspruch eines jeden Menschen, in allen staatlichen Verfahren stets als Subjekt und nie als bloßes Objekt behandelt zu werden. jeder einzelne Mensch hat damit ein Mitwirkungsrecht, er muss staatliches Verhalten, das ihn betrifft, selber beeinflussen können."

### Falun Gong
* https://de.wikipedia.org/wiki/Falun_Gong
    * Falun Dafa
    * "spirituelle chinesische Praktik"
    * "kombiniert Meditation und Qigong-Übungen mit moralischen Grundsätzen basierend auf den drei Tugenden"
        * Wahrhaftigkeit (zhēn 眞)
        * Barmherzigkeit (shàn 善)
        * und Nachsicht (rěn 忍)
    * "Die Praktik betont Moral und die Kultivierung dieser Tugenden"
    * Verfolgung
        * "Am 19. Juli 1999 wurde während eines Treffens der Kader der Kommunistischen Partei Jiang Zemins Entscheidung mitgeteilt, Falun Gong auszulöschen. Die Niederschlagung begann am darauffolgenden Tag, dem 20. Juli 1999.[156] Eine landesweite Propagandakampagne wurde ins Leben gerufen, um Falun Gong zu diskreditieren und zu beseitigen.[157] Dennoch tauchten in der Propaganda gegen Falun Gong zunächst keine Begriffe wie „Kult“ oder „Sekte“ auf."
        * Verleumdung in anderen Ländern

### Palästina
* https://de.wikipedia.org/wiki/Staat_Pal%C3%A4stina

### Folter
* z. B. https://www.daserste.de/information/wissen-kultur/ttt/videos/folter-assange-video-100.html, 2019
    * "Der UN-Sonderberichterstatter über Folter hat Assange in der Haft besucht und einen alarmierenden Bericht über dessen Gesundheitszustand gegeben. Der 48-Jährige weise massive Angststörungen auf und habe dramatisch an Gewicht verloren."

Internationale Institutionen / Menschenrechte
---------------------------------------------
### Internationale Strafgerichtshof (ICC)
* Artikel: ["Internationaler Strafgerichtshof ermittelt gegen Venezuela und die Philippinen"](http://www.luzernerzeitung.ch/nachrichten/international/venezuela-und-philippinen-icc-ermittelt;art46446,1196480)
    * Philippinen: staatlicher Anti-Drogen-Krieg

### PEN, Writers in Prison
* http://wip.penclub.at/
* Für Worte hinter Gittern
