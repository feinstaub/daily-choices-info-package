Schöne Natur
============

<!-- toc -->

- [Der Biber](#der-biber)
- [Wald](#wald)

<!-- tocstop -->

Beispiele von schöner Natur.

### Der Biber
* 2017: wurde früher systematisch ausgerottet, heute wieder da
    * [Beitrag SWR mit Biberbildern](http://www.ardmediathek.de/tv/nat%C3%BCrlich/Comeback-eines-Nagers-der-Biber/SWR-Fernsehen/Video?bcastId=1026394&documentId=43347848), 4 min, 2017

### Wald
* Waldspaziergänge
* Stresspegel sinkt
* Waldbäder in Japan
