Bauen
=====

<!-- toc -->

- [Flächenversiegelung / Bodenversiegelung / Flächenfraß](#flachenversiegelung--bodenversiegelung--flachenfrass)
  * [Dokus](#dokus)
  * [Weltacker](#weltacker)
  * [Inbox 2021: Blog: Verbietet das Bauen](#inbox-2021-blog-verbietet-das-bauen)
  * [Inbox](#inbox)
- [Sand-Doku](#sand-doku)
- [Recycling](#recycling)
- [Wohnungskrise](#wohnungskrise)
  * [Lösungswege?](#losungswege)
  * [Beispiel Mieten](#beispiel-mieten)
- [Zement](#zement)

<!-- tocstop -->

Flächenversiegelung / Bodenversiegelung / Flächenfraß
-----------------------------------------------------
siehe ::flächenfraß

### Dokus
Dokus, die das Thema Flächenverbrauch anschaulich darstellen, inkl. konkreter Beispiele von Städten, bei denen das auch anders geht:

* "Luxusgut Lebensraum - 3sat", August 2019, 45 min (https://www.3sat.de/wissen/wissenschaftsdoku/luxusgut-lebensraum-100.html, Trailer https://vimeo.com/356258494)
    * Der Blick wird auf Deutschland, Österreich und die Schweiz gerichtet; inklusive Kritik an unwirksamen Ausgleichsmaßnahmen.
    * Es werden auch konkrete Ansätze vorgestellt, wie man alternativ mit dem Thema umgehen kann.

* "Versiegeln, verbauen, zuteeren: Flächenfraß in Bayern", BR, November 2019, 30 min
    * (https://www.br.de/br-fernsehen/programmkalender/ausstrahlung-1947084.html, https://www.br.de/mediathek/video/unkraut-18112019-versiegeln-verbauen-zuteeren-flaechenfrass-in-bayern-av:5d9c7e8bf9b298001a956e13);
    * TODO... nur den Anfang gesehen, aber auch hier scheint das Thema gut aufbereitet zu sein
    * ...
    * ...

* todo: Beispiel Verkehr (öst. Verkehrsforscher): wo neue Straßen neue Bebauung anziehen
    * Quelle/Senke-Problem lösen, nicht Verbindungsproblem

### Weltacker
* Doku: "Anders essen - Das Experiment | Doku | ARTE" (https://www.youtube.com/watch?v=IJLnnEBmFis), Dokumentarfilm von Andrea Ernst (D 2018, 91 Min)
    * "Für die Dokumentation „Anders essen - Das Experiment“ wird erstmals ein Acker mit genau jenen Getreiden, Gemüsen, Früchten, Ölsaaten und Gräsern bepflanzt, die pro Person auf unseren Tellern landen – und die die Industrie unter anderem zu Futtermitteln für Tiere verarbeitet. Es entsteht ein Feld von 4.400 Quadratmetern Größe, die Fläche eines kleinen Fußballfeldes, das der „durchschnittliche“ Bürger benötigt. Zwei Drittel davon liegen im Ausland. Insgesamt verbrauchen wir doppelt so viel, wie uns eigentlich zusteht"

    * **Futtermittel-Import**, ...
    * **Palmöl**

* [Der Weltacker](https://www.2000m2.eu/de/der-globale-acker/)
    * aus BUNDmagazin 4-2017

### Inbox 2021: Blog: Verbietet das Bauen
* https://www.verbietet-das-bauen.de/
    * https://www.verbietet-das-bauen.de/fuenf-jahre-paris-abkommen-klimakrisenbuecher/, stellt Selbstverbrennung vor

### Inbox
* 2016: ["Entwickelt sich Bayern zur Betonwüste?"](https://www.welt.de/regionales/bayern/article150761749/Entwickelt-sich-Bayern-zur-Betonwueste.html)
    * "Grüne Wiesen und Wälder, so präsentiert sich der Freistaat gerne. Doch die Realität heißt: Betonwüste Bayern. Umweltschützer beklagen den Flächenverbrauch. Besserung ist nicht in Sicht, im Gegenteil."
* 2013: https://www.umweltbundesamt.de/daten/flaeche-boden-land-oekosysteme/boden/bodenversiegelung
    * "Etwa 46 Prozent der Siedlungs- und Verkehrsflächen sind versiegelt, das heißt bebaut, betoniert, asphaltiert, gepflastert oder anderweitig befestigt. Damit gehen wichtige Bodenfunktionen, vor allem die Wasserdurchlässigkeit und die Bodenfruchtbarkeit, verloren."
    * ...
* 2008: Österreich: ["Was beeinflusst die Flächenversiegelung?"](https://www.agrarforschungschweiz.ch/artikel/2008_04_1371.pdf)
    * "Über 400 Quadratmeter pro Person dienen heute im Durchschnitt als Siedlungs- und Verkehrsfläche. Diese Flächen wurden bis anhin und werden auch weiterhin fast ausschliesslich der landwirtschaftlichen Nutzung entzogen."

Sand-Doku
---------
* Doku: ["Sand - Die neue Umweltzeitbombe - Arte Doku 2013 HD"](https://www.youtube.com/watch?v=nY37sNXpf7g), 1h 15min
    * Ist das wirklich ein Problem?
    * Video_: ["Kampf um Sand - Der neue Goldrausch"](https://www.zdf.de/wissen/leschs-kosmos/kampf-um-sand-goldrausch-harald-lesch-in-leschs-kosmos-100.html), 2016, ZDF, 30 min
        * "Der Kampf um den Rohstoff Sand ist eröffnet. Harald Lesch verfolgt die Spuren des Sandes und erkundet, woher er kommt und wohin er für immer verschwindet."
        * ...
        * todo
* ["Der Sand wird knapp Mit offenen Karten ARTE"](https://www.youtube.com/watch?v=2Ey-Z2zXiUg), 2018, 12 min

Recycling
---------
* Zahlen: 90 % der Baumaterialien werden recycelt. Das deckt aber nur 13 % des Bedarfs. (st.echo, röhrig, Jan 2020)

Wohnungskrise
-------------
### Lösungswege?
* https://de.wikipedia.org/wiki/Deutsches_Architekturmuseum
    * Leitung: Peter Cachola Schmal
        * https://www.hr1.de/programm/hr1-talk/der-hr1-talk-mit-peter-cachola-schmal,talk-304.html
            * "gegen Wohnungsnot hilft nur Verdichtung, also Wohnhochhäuser in großen Städten"
        * St.echo
            * "Nur wenn die öffentliche Hand wieder im großen Stil Wohnungen baut, ..."

### Beispiel Mieten
* ["Faire Vermieter werden abgestraft | quer vom BR"](https://www.youtube.com/watch?v=fzONXZQps2A), 2019

Zement
------
* siehe Zement = Klimakiller
