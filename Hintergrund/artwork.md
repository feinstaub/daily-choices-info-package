Kunst
=====

<!-- toc -->

- [Vegan, Konsumkritik](#vegan-konsumkritik)
  * [Steve Cutts](#steve-cutts)
  * [Moby](#moby)
  * [polyp-Cartoons](#polyp-cartoons)

<!-- tocstop -->

Vegan, Konsumkritik
-------------------
### Steve Cutts
* YT: "Moby & The Void Pacific Choir - In This Cold Place (Official Video)", 2017, Comic, Superman, Trump, ...
* YT: "Moby & The Void Pacific Choir - Are You Lost In The World Like Me? (Official Video)", 2016
* YT: "The Turning Point" by Steve Cutts, 2020, 3 min, Wenn Tiere sich wie Menschen verhalten würden
* YT: "Happiness" by Steve Cutts, 2017, 4 min, todo
* YT: "In The Fall" by Steve Cutts, 2011, 2 min, Über einen sinnlosen Bürojob
* YT: "MAN 2020" by Steve Cutts, 1 min
    * Video: [Man - by Steve Cutts](https://www.youtube.com/watch?v=WfGMYdalClU), 4 min, 2012
        * Music: In the Hall of the Mountain King by Edvard Grieg.
        * (walk of man, march of man)
* Video: [consumerism](https://www.youtube.com/watch?v=v-7v2WGiTe8), 2016, 4 min, Buy a new Y-Fone, Apple-Werbung
* YT: "It's our world (Steve Cutts / Yann Tiersen) FullHD 1080p", 2018, 3 min

### Moby
* Video: [Musik-Video zu Moby - Disco Lies](https://www.youtube.com/watch?v=RjJYznmSjkg), 2008, 3 min, Hühner, Kentucky Fried Chicken

### polyp-Cartoons
* https://polyp.org.uk/consumerism_cartoons/sscon4.html - QR code - buy more shit
* https://polyp.org.uk/consumerism_cartoons/sscon6.html - Nike
* https://polyp.org.uk/consumerism_cartoons/sscon7.html - Mäuse - Happiness is just around the corner
* https://polyp.org.uk/consumerism_cartoons/sscon8.html - It's not me
* https://polyp.org.uk/consumerism_cartoons/sscon13.html - What's wrong with you people?! ... You want us all to drown?!
* https://polyp.org.uk/consumerism_cartoons/sscon14.html - You can't enjoy a WesternLifestyle (Cola style) because WE already consume more than our fair share. - Consumerism
* https://polyp.org.uk/consumerism_cartoons/sscon16.html - ... cheap flights... - Is say it was worth it
* https://polyp.org.uk/consumerism_cartoons/sscon17.html - Animal testing
* https://polyp.org.uk/consumerism_cartoons/sscon20.html - Spot the addict
* https://polyp.org.uk/consumerism_cartoons/sscon22.html - Marketing - like a machine gun
* https://polyp.org.uk/consumerism_cartoons/sscon23.html - I want 80 % for the riches vs. Spoilt brat
* https://polyp.org.uk/consumerism_cartoons/sscon26.html - Walk two once a day

* More on http://www.polyp.org.uk/
    * Charity rich vs. poor: http://www.polyp.org.uk/cartoons/wealth/polyp_cartoon_charity_aid_trade_exploitation_sweatshop_cash_crops_wealth_poverty_media.jpg
    * Easter planet: http://www.polyp.org.uk/cartoons/environment/polyp_cartoon_easter_island_environment_climate_change_collapse_apocalypse.jpg
    * CO2 let's not argue over the bill: http://www.polyp.org.uk/cartoons/environment/polyp_cartoon_co2_bill.jpg
    * Suicide with CO2: http://www.polyp.org.uk/cartoons/environment/polyp_cartoon_global_warming_CO2_suicide_carbon_dioxide.jpg
