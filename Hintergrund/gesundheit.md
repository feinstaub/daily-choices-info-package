Gesundheit
==========

<!-- toc -->

- [Sonne und Bewegung](#sonne-und-bewegung)
- [Gesunde und nachhaltige Ernährung](#gesunde-und-nachhaltige-ernahrung)
  * [EAT-Lancet](#eat-lancet)
  * [Oxford-Studie](#oxford-studie)
  * [Indien / Kultur](#indien--kultur)
- [Krankenkassen](#krankenkassen)
  * [Elektronische Gesundheitskarte](#elektronische-gesundheitskarte)
- [Zucker](#zucker)
- [Gesundheit beim Schulessen](#gesundheit-beim-schulessen)
- [Abgase von Verbrennungsmotoren](#abgase-von-verbrennungsmotoren)
- [Pestizide](#pestizide)
  * [in der Luft und Umwelt](#in-der-luft-und-umwelt)
  * [im Essen](#im-essen)
- [Zigaretten](#zigaretten)
  * [Zigarettenrauch](#zigarettenrauch)
  * [Verzehr von Zigaretten](#verzehr-von-zigaretten)
  * [Umwelt: Zigarettenstummel](#umwelt-zigarettenstummel)
  * [Taschenaschenbecher](#taschenaschenbecher)
  * [Geschichte: Methoden und Rhetorik zur Vertuschung und Verharmlosung durch die Tabakindustrie](#geschichte-methoden-und-rhetorik-zur-vertuschung-und-verharmlosung-durch-die-tabakindustrie)
- [Alkohol](#alkohol)
- [Raken und Böller vs. Augen](#raken-und-boller-vs-augen)
- [Aluminium](#aluminium)
- [Antibiotika](#antibiotika)
- [Kuh-Milch](#kuh-milch)
- [Allergien / Asthma](#allergien--asthma)
- [Tierisches Eiweiß](#tierisches-eiweiss)
- [Tiergesundheit](#tiergesundheit)
- [Gesundheit von Arbeitern](#gesundheit-von-arbeitern)
- [Aspartam](#aspartam)
- [Lobbyismus](#lobbyismus)
- [In anderen Ländern](#in-anderen-landern)
- [Big data](#big-data)
- [Medizinprodukte](#medizinprodukte)

<!-- tocstop -->

Themen rund um die Gesundheit.

Sonne und Bewegung
------------------
* "Wie aktiv und fit sind unsere Kinder? Die Weltgesundheitsbehörde empfiehlt 60 Minuten Bewegung am Tag.
    Klingt nicht viel, aber tatsächlich erreichen heute einer neuen Studie zufolge gerade noch 15 Prozent
    der Kinder dieses Minimum an Bewegungszeit.", 2019,
    https://www.mdr.de/wissen/mensch-alltag/kinder-bewegen-sich-weniger-als-frueher-100.html
    * Lancet

Gesunde und nachhaltige Ernährung
---------------------------------
### EAT-Lancet
* https://www.sueddeutsche.de/gesundheit/essen-umwelt-fleisch-1.4291259, 2019, Lancet EAT, EAT-Lancet
    * "Eine Forschergruppe empfiehlt einen Ernährungsplan, der die Gesundheit des Menschen und der Erde gleichermaßen schützen soll."
    * "Fleisch- und Zuckerkonsum müssten halbiert, der Verzehr von Obst, Gemüse und Nüssen etwa verdoppelt werden."
    * "Der Menschheit bleibt etwa eine Generation Zeit, ihre Ernährungsweise und die Landwirtschaft entsprechend anzupassen."
    * "Sieben Gramm Rindfleisch pro Tag"
    * "sieben Gramm Schweinefleisch pro Tag"
    * "circa 0,2 Eier"
    * "ein Hauch Schweinefett - aber keine Butter"

### Oxford-Studie
* Oxford-Studie, siehe dort
    * Kultur
        * z. B. Indien

### Indien / Kultur
* Indien: https://www.welt.de/gesundheit/article132774043/Wo-Fleischesser-Nicht-Vegetarier-genannt-werden.html, 2014
    * "Wo Fleischesser „Nicht-Vegetarier“ genannt werden"
    *  "Die meisten Vegetarier weltweit leben in Indien: Fast nirgendwo auf der Welt wird so wenig Fleisch gegessen: Da beugen sich selbst große Hamburger-Ketten und bieten ausschließlich fleischlose Kost an." (--> das zeigt, dass die Leute das so wollen und nicht unfreiwillig verzichten)

    * "Indiens Regierungschef Narendra Modi ließ seinem Amtskollegen aus China, Xi Jinping, bei dessen Besuch jüngst 100 Spezialitäten auftischen. Sie alle hatten eines gemeinsam: Sie waren vegetarisch." (--> wäre ein Beispiele für unsere Staatsoberhäupter und zeigt die Vielfalt, die ohne Fleisch möglich ist)

    * "Fleischesser werden noch immer „Nicht-Vegetarier“ genannt."

    * McDonalds: "wurden fast drei Viertel der angebotenen Speisen extra für Indien erfunden, und in den Küchen würden „vegetarische und nicht-vegetarische Produkte“ völlig getrennt behandelt, erklärte das Unternehmen." (--> zeigt welche Kraft in der menschlichen Kultur steckt)

    * "Selbst im Supermarkt müssen alle Produkte mit Fleischzutaten einen braunen Punkt auf der Verpackung tragen, während vegetarische Produkte einen grünen Punkt bekommen."

    * ...

Krankenkassen
-------------
* Kosten (gesetzlich, privat)
* [BKK ProVita-Vorstand isst vegan](https://www.bkk-provita.de/bekenntnis-der-vorstandes-der-bkk-provita-zu-veganer-ernaehrung/) (10 min Interview mit Vorstand, 2014?, wünscht sich, dass auch andere Menschen Erfahrungen mit veganer Ernährung haben)

### Elektronische Gesundheitskarte
* siehe dort

Zucker
------
* siehe Zucker/

Gesundheit beim Schulessen
--------------------------
* siehe dort

Abgase von Verbrennungsmotoren
------------------------------
* siehe Auto/

Pestizide
---------
### in der Luft und Umwelt
* siehe Bio/

### im Essen
* siehe Bio/

Zigaretten
----------
### Zigarettenrauch
* https://de.wikipedia.org/wiki/Zigarette#Gesundheitsgefahren
    * "Das Alkaloid Nikotin gehört zu den am schnellsten süchtig machenden Substanzen überhaupt und verursacht Nikotinabhängigkeit."
    * ...
    * "Besonders riskant sind Billigzigaretten aus Schmuggelware oder Internethandel. Bei diesen kann die Belastung mit Pestiziden bis zu 200-mal höher liegen als die zulässigen Grenzwerte, was das Risiko für Krebserkrankungen und Nierenschäden erhöht."

### Verzehr von Zigaretten
* https://de.wikipedia.org/wiki/Zigarette#Gesundheitsgefahren
    * "Eine zusätzliche Gefahr stellen insb. von Kleinkindern im Spiel verzehrte Zigaretten oder Zigarettenkippen dar. Eine Zigarette setzt im Magen zirka 12 Milligramm Nikotin frei, was 10–20 Prozent der tödlichen Dosis für Erwachsene entspricht."

### Umwelt: Zigarettenstummel
* https://de.wikipedia.org/wiki/Zigarettenstummel
    * "Neben Nikotin sind Arsen und Schwermetalle wie Blei, Kupfer, Chrom und Cadmium enthalten"
    * "Die Zahl der weltweit pro Jahr weggeworfenen Zigarettenstummel wird auf 4,5 Billionen geschätzt."

### Taschenaschenbecher
* als praktische Lösung: https://www.aschenbecher.com/taschen-aschenbecher/

### Geschichte: Methoden und Rhetorik zur Vertuschung und Verharmlosung durch die Tabakindustrie
* Film: The Insider
    * https://de.wikipedia.org/wiki/Jeffrey_Wigand
    * https://www.nytimes.com/1996/07/16/us/thomas-sandefur-tobacco-leader-dies-at-56.html
    * ...
    * Diskussion at CBS - Privater Fernsehsender
        * Gefahr, dass, im Falle der Ausstrahlung der Whistleblower-Geschichte, die Brown&Willamson Tobacco Company CBS verklagen würde sie ggf. aufkaufen
        * (vs. öffentlich-rechtlicher Rundfunk)
        * möglicherweise auch Risiko, dass eine Fusion aufgrund der Story weniger Profit für Involvierte abwirft
        * ...
    * 1h50: Schmutzkampagne gegen Hauptzeugen
    * ...

* https://www.spiegel.de/spiegel/print/d-9232819.html, 1995, "Rauchen „Blick durchs Schlüsselloch“ auf die Tabak-Industrie"
    * ...
    * ...
    * "häuften sich die Hinweise, daß Zigarettenrauchen nicht nur Lungenkrebs verursachen kann, sondern für eine ganze Reihe weiterer Leiden verantwortlich zu machen ist, so etwa für Herz- und Hirninfarkte, für Lippen-, Speiseröhren- und Blasenkrebs oder - wenn während der Schwangerschaft geraucht wird - für Wachstumsbeeinträchtigungen der Leibesfrucht."
        * "Anfangs reagierte die Industrie mit **verstärktem Forschungsaufwand**. Ziel war es damals, die giftigen Bestandteile im Zigarettenrauch zu identifizieren und sie dem Tabak zu entziehen - eine "sichere Zigarette" sollte entwickelt werden. Das Projekt wurde **eingestellt**: Die **Anzahl der giftigen Bestandteile im Zigarettenrauch erwies sich als zu hoch**, sie alle auszufiltern als zu schwierig."
    * Konzernwissenschaftler wussten was Sache ist, aber das Stand nicht in den **PR**-Broschüren
        * "hieß es in einer Verlautbarung der Brown & Williamson-Pressestelle aus dem Jahre 1971, " . . . halten wir es für hinlänglich erwiesen, daß die Aussage ,Zigaretten verursachen Krebs'' keine Tatsachenfeststellung ist, sondern **allenfalls eine Hypothese**.""
            * Analog zu: Klimawandel-Hypothese
        * Sieben Jahre später schrieb Ross Millhiser, Präsident des Tabakmultis Philip Morris, in einem Meinungsartikel, den die New York Times abdruckte:
            Es gebe "gute Gründe, die Schuldhaftigkeit von Zigarettenkonsum bei Herzkrankheiten **anzuzweifeln**".
                * Analog zu: Es gibt gute Gründe CO2 ist als Haupt-Mitverursacher des Klimawandels anzuzweifeln.
        * "Intern wußten die Topmanager der Tabakindustrie spätestens seit Anfang der achtziger Jahre die ganze Wahrheit."
        * "In "nahezu unglaublich akribischer Forschungsarbeit" hatten die von der Tabakindustrie beschäftigten Wissenschaftler die gesundheitlichen Gefahren des Zigarettenrauchens eingekreist. "Besser", so Glantz rückblickend, "als der Rest der Medizinerzunft und vor allem früher. Sie waren uns 20 Jahre voraus.""
    * 1994: "Ein Tabakchef nach dem anderen versicherte während der sechsstündigen Befragung: "Zigaretten sind nicht suchtauslösend." Mittlerweile ist nicht mehr auszuschließen, daß die Tabakbosse mit dieser **eidlichen Aussage** [...] bewußt [ge]logen haben."
    * "Nikotin macht süchtig. Wir betreiben also unser Geschäft mit dem Verkauf von Nikotin, einer suchtauslösenden Droge."
    * mögliches Werbeverbot etc.?
        * Methode Emotionen: "beschworen Anzeigen der Tabakindustrie das Schreckgespenst der Prohibition und verwiesen auf die drohenden Einschränkungen der bürgerlichen Freiheit."
        * Methode Staat ist böse, nicht wir: "Sie zitierten darüber hinaus die Ergebnisse eiligst durchgeführter Umfragen. "Die überwiegende Mehrheit der amerikanischen Bevölkerung" lehne "jede Art staatlicher Kontrolle des Zigarettenverkaufs" ab"
        * Methode die Gegner angreifen/beschuldigen: "Geld- und Machtgier, so erläutert Parrish,
            treibe "Vertreter einer **Antiraucherindustrie**" und bislang "einflußlose Abgeordnete in Washington" dazu, die Zigarettenindustrie zu attackieren."
            * Analog zu: "Klimawandelindustrie"
            * Analog zu: "Veganindustrie"
    * "suchen die Zigarettenhersteller nach neuen Märkten."
        * "So war Japan dem US-Beispiel gefolgt, im Fernsehen keine **Zigarettenwerbung** mehr zu senden."
        * "US-Senator [...] hatte anläßlich einer Dienstreise in Tokio mit Importbeschränkungen für japanische Autos gedroht, wenn das TV-Werbeverbot für Zigaretten nicht aufgehoben werde. Inzwischen ist die TV-Zigarettenwerbung die zweitgrößte Einnahmequelle japanischer Fernsehsender."
        * "Die absehbaren Folgen des wachsenden Zigarettenkonsums in Ländern der Dritten Welt kalkulierte der britische Epidemiologe Richard Peto: In 30 Jahren, schätzt der Mediziner, [...]"
        * Analog zu: Milch- und Schweinefleisch-Export
    * "Patrick, der Enkel des Gründers von Amerikas zweitgrößtem Tabakkonzern, hat selbst zehn Jahre gebraucht, um sich das Rauchen abzugewöhnen; jetzt kämpft er im Lager der Zigarettengegner"

* https://www.spiegel.de/plus/whistleblower-gegen-tabakindustrie-dann-lag-eine-revolverpatrone-in-meinem-briefkasten-a-421a1f21-870b-4ec9-b7e6-950bdf7354ed, 2019
    * "Whistleblower gegen die Tabakindustrie - "Und dann lag eine Revolverpatrone in meinem Briefkasten"
        Jeffrey Wigand enthüllte, wie die Tabakindustrie vorsätzlich und geplant die Suchtwirkung von Zigaretten verstärkte. Hier erklärt er, wie seine Familie bedroht wurde und was das mit ihm gemacht hat."

Alkohol
-------
* siehe alkohol.md

Raken und Böller vs. Augen
--------------------------
* siehe dort

Aluminium
---------
* 2020: YT: "Alu-Deos sind unbedenklich | Parabene, Silikone, Aluminium wissenschaftlich geprüft", maiLab, 22 min
    * siehe auch https://de.wikipedia.org/wiki/MedWatch_%28Online-Magazin%29
        * "Im Dezember 2019 setzte sich Medwatch gemeinsam mit Zeit Online kritisch mit einer Warnung des Bundesinstituts für Risikobewertung (BfR) vor aluminiumhaltigen Deodorants auseinander.[16] Im Juli 2020 gab das BfR eine diesbezügliche Neubewertung des Risikos bekannt.[17]"
* Aluminium in Deos
    * Warum sollte man das vermeiden? -> siehe z. B. [Aluminium im Alltag, ARD](http://www.daserste.de/information/wissen-kultur/w-wie-wissen/aluminium-104.html) (2015)
    * Positive Beispiele: Speick, Sonett
* Doku "Die Akte Aluminium"

Antibiotika
-----------
* siehe antibiotika.md

Kuh-Milch
---------
* siehe Vegan/milch.md

Allergien / Asthma
------------------
* ["Allergien auf dem Vormarsch"](https://www.ndr.de/fernsehen/sendungen/45_min/video-podcast/Allergien-auf-dem-Vormarsch,minuten2746.html), 45 Min - 11.03.2019 22:00 Uhr, NDR
    * "Es gibt nicht nur mehr Allergien - sie dauern auch länger und sind heftiger. Welchen Einfluss hat die Umwelt, besonders auf die Zunahme von Atemwegsallergien?"
    * "Fast jeder Vierte leidet hierzulande im Laufe seines Lebens an Allergien."
    * https://de.wikipedia.org/wiki/Asthma_bronchiale#Allergisches_Asthma
    * "Birkenpollen gelten neben Gräserpollen als die stärksten Allergene in Europa. Besonders die Birke hat nach Ansicht von Wissenschaftlern einen großen Anteil an der Zunahme der Allergien."
    * "einzigartigen Klimakammer des Helmholtz-Zentrums in München"
        * "Pollen werden offenbar aggressiver, je mehr Stickoxide in der Luft sind und wenn die Ozonwerte steigen."
            * mehr von den allergieauslösenden Eiweisen
        * Kohlendioxid => höhere Pollenproduktion
    * "Was Prävention und Behandlung der Allergien betrifft, hinkt Deutschland hinterher. Zum Beispiel gibt es in Finnland bereits seit zehn Jahren einen Aktionsplan, um etwas gegen die Volkskrankheit Allergie zu unternehmen"
        * "Besonders wichtig ist dabei nicht nur die Ausbildung der Ärzte, sondern auch die Aufklärung der Patienten. Professor Torsten Zuberbier fordert für Deutschland ebenfalls einen sofortigen Aktionsplan."

Tierisches Eiweiß
-----------------
* siehe Vegan/ -> Gesundheit
* siehe Vegan/more.md
* ...

Tiergesundheit
--------------
* siehe tiere-artgerecht.md

Gesundheit von Arbeitern
------------------------
* Was ist der Nachteil von Massentierhaltung?
    * http://www.foodispower.org/factory-farm-workers/
    * http://www.foodispower.org/slaughterhouse-workers/

Aspartam
--------
* Wer stellt Aspartam her, wo ist es drin und welche Nachteile hat es? -> https://de.wikipedia.org/wiki/Aspartam#Gesundheitsfragen

Lobbyismus
----------
* siehe pharma.md

In anderen Ländern
------------------
* siehe Kaffee/
* siehe Kakao/

Big data
--------
* siehe dort

Medizinprodukte
---------------
* 2018
    * "Dutzende Patienten in Deutschland haben von 2010 an eine neuartige Bandscheibenprothese aus Plastik"
        * https://www.tagesschau.de/inland/implantfiles/cadisc-101.html
        * #ImplantFiles
* 2017
    * ["Kontrolle ist gut, mehr Kontrolle besser"](http://www.tagesschau.de/ausland/medizinprodukte-103.html)
        * "Erst waren Brustimplantate undicht, dann haben HIV-Tests nicht funktioniert: Immer wieder sorgen mangelhafte Medizinprodukte für Schlagzeilen. In Zukunft sollen sie sicherer werden - dafür hat die EU einen ausgeklügelten Plan."
    * **Qualität und Sicherheit von Medizinprodukten**
        * Zentrales Register, wo Mängel von Ärzten gemeldet werden. Beispiel: Schweden.
        * Vorteil: Man bekommt Überblick, welche Firmen Qualität liefern und spart dadurch Geld (Beispiel: teure neue Hüftgelenke).
        * "Die ersten Register in den skandinavischen Ländern entstanden infolge eines Medizinskandals: In den 70er-Jahren hatten skandinavische Orthopäden sogenannte Christiansen-Hüftprothesen einige Tausend Mal implantiert, bevor sie erkannten, dass durchschnittlich sechs Jahre nach der Implantation jede dritte Prothese gewechselt werden musste. Daraufhin gründeten Schweden und Finnland 1979 die weltweit ersten **Endoprothesenregister**. Das Resultat: In Schweden sank die Zahl der jährlichen Wechseloperationen auf die Hälfte. Angesichts solcher Zahlen hält es auch SPD-Gesundheitsexpertin Carola Reimann für dringend notwendig, Register für Medizinprodukte einzuführen." aus DLF-Beitrag 2012: ["Geprüft und doch nicht immer sicher - Kritik an Kontrollen von Medizinprodukten"](http://www.deutschlandfunk.de/geprueft-und-doch-nicht-immer-sicher.724.de.html?dram:article_id=100437), siehe [Wiki](https://de.wikipedia.org/wiki/Endoprothesenregister) über Situation in D.
