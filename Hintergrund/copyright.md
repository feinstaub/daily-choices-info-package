Gedanken zum Copyright / Urheberrecht
=====================================

<!-- toc -->



<!-- tocstop -->

Sammlung:

* Wikipedia: https://de.wikipedia.org/wiki/Urheberrecht
* Definition Urheberrecht: http://www.netzdurchblick.de/definition_urheberrecht.html
* Freie Inhalte: http://www.netzdurchblick.de/freie-inhalte.html
* Plagiate und Zitate: http://www.netzdurchblick.de/plagiateundzitate.html
* http://questioncopyright.org/
    * englischsprachige Einstiegsseite zum Hintergrund der Reform-Notwendigkeit des aktuellen Copyright-Rechts
    * "Our mission is to provide advocacy and practical education to help cultural producers embrace open distribution.": http://questioncopyright.org/about
* Beispiele:
    * Disney-Gesetze, siehe Internet
    * Seit 1997: [Kraftwerk vs. Pelham](http://www.zeit.de/kultur/musik/2016-05/kraftwerk-moses-pelham-sampling-verfassungsgericht-urteil), bis derzeit 2017
        * also 20 Jahre, das Copyright sollte einfacher sein
* Video: ["Copyright is Brain Damage | Nina Paley | TEDxMaastricht"](https://www.youtube.com/watch?v=XO9FKQAxWZc), 2015, 19 min
