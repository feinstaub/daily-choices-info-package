Bekannte Marken namhafter Hersteller
====================================

<!-- toc -->

- [Einleitung](#einleitung)
  * [Schwarzbuch Markenfirmen](#schwarzbuch-markenfirmen)
  * [Beispiel Nestle](#beispiel-nestle)
  * [Beispiel Bayer](#beispiel-bayer)
  * [Beispiel Coca-Cola](#beispiel-coca-cola)
  * [Beispiel BP](#beispiel-bp)
  * [siehe Kleidung / Zara](#siehe-kleidung--zara)
  * [Toxic 100 Index](#toxic-100-index)
  * [Filme](#filme)
  * [Bücher](#bucher)
- [ARD-Markencheck](#ard-markencheck)
  * [Milchpulver-Werbung als Baby-Nahrung](#milchpulver-werbung-als-baby-nahrung)
- [Der Goldene Windbeutel](#der-goldene-windbeutel)
- [Armut als Markt](#armut-als-markt)
  * [Das Geschäft mit dem Hunger](#das-geschaft-mit-dem-hunger)
- [Interessante Aussagen](#interessante-aussagen)
  * ["Aber das ist doch deren Geschäftsmodell"](#aber-das-ist-doch-deren-geschaftsmodell)
  * ["Wer groß ist, steht halt im Fokus"](#wer-gross-ist-steht-halt-im-fokus)

<!-- tocstop -->

Einleitung
----------
Es ist nicht alles Gold, was glänzt.

Nur, weil etwas bekannt ist, kann man nicht darauf schließen, das alles mit rechten Dingen zugeht.

Wer Probleme hat, sich emotional von gewissen Marken zu lösen (um diese dann auch aktiv zu meiden), dem sei als Einführung dieses Buch empfohlen

### Schwarzbuch Markenfirmen
* Sachbuch: [Schwarzbuch Markenfirmen](https://de.wikipedia.org/wiki/Schwarzbuch_Markenfirmen) von Klaus Werner-Lobo und Hans Weiss
    * Webseite/Blog: https://www.markenfirmen.com/
    * Buch 2016: [Nach der Empörung - Was tun, wenn wählen nicht mehr reicht](https://www.markenfirmen.com/2016/03/07/leseprobe-nach-der-empoerung/)
    * Interview mit Telepolis 2014:
        * [Seite 1](https://www.heise.de/tp/features/Die-Macht-der-Konsumenten-hat-eine-problematische-Seite-3367948.html)
        * [Seite 2](https://www.heise.de/tp/features/Die-Macht-der-Konsumenten-hat-eine-problematische-Seite-3367948.html?seite=2)
            * "Die viel beschworene "Macht der Konsumenten" hat aber auch eine problematische Seite: Wenn heute manche von sogenannten demokratischen Entscheidungen an der Supermarktkasse sprechen, dann heißt das doch, dass jene mehr Macht und Einfluss haben die über mehr Geld verfügen, solche Konsumentscheidungen zu treffen. Das ist eine Fortsetzung des neoliberalen Diskurses und damit des Problems. Mich erstaunt es ein bisschen, dass immer nach den Möglichkeiten gefragt wird, die wir als Konsumenten haben - und nie nach denen, die wir als Bürger, als Citoyens demokratischer Länder haben."
                * Bewertung: stimmt, aber deswegen sind im ersten Schritt auch alle Personen mit ausreichendem Einkommen gefragt, sich entsprechend zu verhalten (siehe tägliche Konsum-Entscheidungen)
            * "Die eigene Komfortzone zu verlassen und sich für ökologische, soziale und demokratische Rechte einzusetzen, und zwar überall wo wir die Möglichkeit dazu haben - durch respektvollen und solidarischen Umgang mit weniger Privilegierten, durch zivilgesellschaftliches Engagement, die Unterstützung sozialer Bewegungen oder auch durch gewaltfreien Widerstand, wenn Konzerne und Regierungen Umwelt, Menschen und Demokratie mit Füßen treten. Im Schwarzbuch Markenfirmen zeigen wir anhand konkreter Beispiele, dass vor allem kreative Protestformen, die durchaus auch humorvoll sein können, besonders wirksam und nachhaltig sind."

### Beispiel Nestle
* Vittel - Leuten in Frankfreich wird das Wasser abgegraben
    * https://utopia.de/nestle-wasser-vittel-90266
* Marke: San Pellegrino
* ...
* https://de.wikipedia.org/wiki/Nestl%C3%A9#Kritik
    * Babynahrung
    * Kinderarbeit, Menschenhandel und Zwangsarbeit
    * Trinkwasser
        * https://de.wikipedia.org/wiki/Bottled_Life_%E2%80%93_Nestl%C3%A9s_Gesch%C3%A4fte_mit_dem_Wasser
    * ...

### Beispiel Bayer
* 2019:
    * "Ärger um Monsanto-Liste - Bayer bittet um Entschuldigung - Bayer bittet um Entschuldigung für umstrittenes Verhalten der Konzerntochter Monsanto. Die französische Justiz ermittelt, weil diese zwecks Einflussnahme eine Liste von Kritikern geführt haben soll.", tagesschau 2019
        * Dokumente von 2016
        * französische Journalistin auf der Liste, "muss erzogen werden", private Infos gesammelt, Beeinflussung empfohlen
        * Argumentationen und Twittervorschläge, um Glyphosat in ein gutes Licht zu rücken
        * franz. Umweltministerin galt laut einer Monsanto-Kommunikationsagentur als "nicht beeinflussbar", daher "kaltstellen"
        * "solche Listen ohne das Wissen der Beteiligten zu führen, ist in F. strafbar"
* 2018:
    * ["Neonikotinoide - Frankreich verbietet "Bienenkiller""](https://www.tagesschau.de/ausland/neonikotinoide-frankreich-101.html), 2018
        * https://de.wikipedia.org/wiki/Bayer_AG#Kritik_und_Skandale, 2018

            4.1 Medikamente
                4.1.1 Verkauf HIV-kontaminierter Blutprodukte
                4.1.2 Starke Nebenwirkungen von Lipobay
                4.1.3 Antibabypille Yasminelle
            4.2 Menschenrechte und Umweltschutz
                4.2.1 Menschenrechtsverletzungen
                4.2.2 Tierversuche
                4.2.3 CO-Pipeline
                4.2.4 Umweltverschmutzung
                4.2.5 Klimaschutz
            4.3 Preismanipulationen und -absprachen
                4.3.1 Aspirin-Kartell
                4.3.2 Kautschuk-Kartell
                4.3.3 Preismanipulationen zu Lasten der US-Sozialkassen

### Beispiel Coca-Cola
* Websuche "coca cola lobby zucker"
    * https://www.srf.ch/news/schweiz/suesse-macht-die-zuckerlobby-im-parlament
    * https://www.dzw.de/coca-cola-report-zuckergetraenke-sind-fluessige-krankmacher - FoodWatch Coca-Cola-Report
    * https://www.absatzwirtschaft.de/erkauftes-wohlwollen-vergraetzen-pepsi-und-coca-cola-ihre-kunden-91445/
        * "die Getränkeriesen Pepsi und Coca-Cola Einfluss auf die US-Gesundheitspolitik und griffen
            tief in den Geldtopf, um rund 100 Gesundheitsorganisationen mit Sponsoring zu “unterstützen“."

### Beispiel BP
* Websuche "bp lobbying"
    * https://www.theguardian.com/business/2019/mar/22/top-oil-firms-spending-millions-lobbying-to-block-climate-change-policies-says-report
        * https://influencemap.org/report/How-Big-Oil-Continues-to-Oppose-the-Paris-Agreement-38212275958aa21196dae3b76220bddc
    * https://unearthed.greenpeace.org/2019/03/12/bp-lobbied-trump-climate-methane-obama/
        * "The oil giant claims to be a leader on tackling methane emissions but successfully lobbied against two major regulations tackling the problem"

### siehe Kleidung / Zara
* ...

### Toxic 100 Index
* https://de.wikipedia.org/wiki/Toxic_100_Index
    * "Der Toxic 100 Index ist eine Rangliste der 100 größten Luftverschmutzer in den USA unter internationalen Industrieunternehmen"

### Filme
* Film: We feed the world (2005) - 7,6

* Film: ["10 Milliarden – Wie werden wir alle satt?"](https://de.wikipedia.org/wiki/10_Milliarden_%E2%80%93_Wie_werden_wir_alle_satt%3F), Valentin Thurn, 2015, 107 min
    * ...
        * ...
        * fossile Düngervorräte (reichen ca. 50 Jahre)
        * Pestizide
        * ...
    * 55 min: cultured meat
        * aber geht es auch ohne Fleischproteine?
        * die Frage, die der Entwickler aufgreift, ist seltsam (Supermarkt in 20 Jahren: Wahl zwischen zwei gleich schmeckenden Produkten etc.)
            * seltsam deswegen, weil die Wahl heute schon da ist
            * "wir haben die Technologie; wir sollten es machen" (die Frage, ob es unbedingt "Fleisch" sein muss, wird zunächst gar nicht gestellt)
        * TODO: Wo kommen die Vitamine her?
        * Man könne auch aufhören Fleisch zu essen, aber das sei unwahrscheinlich
            * Nein, das hieße die Menschen weiterhin nicht richtig aufzuklären
        * Fazit: ... und unerschwinglich für die Armen ...
    * 1 h 2 min: Chicago, größte Agrarbörse
        * bezahlbare Nahrungsmittel
        * ...
        * Börsenguru wird ausfällig
        * ...
        * allerdings logisch klingt das schon:
            * die Bauern müssen eine vernünftig hohe Entlohnung für ihre Arbeit bekommen
            * zumindest im westlichen Wirtschaftssystem
            * vs. Börsenhandel ist 16x so groß wie tatsächlicher Warenwert
    * 1 h 9 min: Transition Town
        * lokal angebaute Lebensmittel
        * kleingranular intensiv ist die effektivste Nutzung der Landfläche
            * dank Handarbeit ist eine intensivere Bewirtschaftung als per Industrie möglich
                * billig ist die Industrie nur, weil Arbeitskräfte viel Geld kosten
                    * Frage: aber Bodenfläche kostet auch Geld (Pacht), oder?
                * in Entwicklungsländern ist Arbeitskraft genug vorhanden; nur Boden ist knapp
    * 1 h 16 min: Malawi
        * Gemüse wird im Fluss gewaschen
        * ...
        * Lösung: mehr Unabhängigkeit und eigenes Wirtschaften bei der Ernährung
            * todo: Grundeinkommenfilm, wo einem afrikanischen Dorf Menschen ein gewisses Einkommen gewährt wurde und dadurch die lokale Wirtschaft in Gang kam, von denen viele profitierten
    * 1 h 23 min: Urban Farming Milwaukee
        * in armen Vierteln, wo frisches Gemüse vorher für viele zu teuer war
        * Basketballprofi
        * auch in Amerika sind Menschen unterernährt
        * guter Boden ist wichtig; viele Würmer
        * Aquaponik: Abfälle der Fische werden von Pflanzen aufgenommen
    * 1 h 28 min: Solidarische Landwirtschaft in D.
        * ...
    * 1 h 28 min: Incredible Edible Todmorden
        * "Wenn du eine Aktion starten willst, dann am besten mit deinen eigene Händen, deinem eigenen Geld und deinem eigenen Platz"
        * z. B. vor einem Krankenhaus alle giftigen Zierpflanzen raus und dafür Essbares angepflanzt
    * video error

* Film: [Let's make money (2008)](https://de.wikipedia.org/wiki/Let%E2%80%99s_Make_Money) (COPIED)
* Film: Food, Inc
* Film: Zeit der Kannibalen
* Film: [Plastic Planet (2009)](https://de.wikipedia.org/wiki/Plastic_Planet), Plastik Planet

* Doku: "The True Cost - Der Preis der Mode (2015)" - 7,7

* "Blue Gold: World Water Wars (2008)" - 7,7
    * "Wars of the future will be fought over water as they are over oil today, as the source of human survival enters the global marketplace and political arena."
* "Abgefüllt (2009)" - 7,3
    * "Examines the role of the bottled water industry and its effects on our health, climate change, pollution, and our reliance on oil."
* "Bottled Life - Das Geschäft mit dem Wasser (2012)" - 6,7

* "A Plastic Ocean (2016)" - 7,9
* "Monsanto, mit Gift und Genen (2008)" - 8,1

### Bücher
* Sachbuch: [Öl - Das blutige Geschäft (2010)](https://www.perlentaucher.de/buch/peter-maass/oel.html) von Peter Maass
    + "Ohne Öl läuft nichts; es ist der Garant unseres Wohlstands und unserer Mobilität. In vielen Förderländern hingegen herrschen Elend und Gewalt. Wer über den Ölhahn bestimmt, diktiert das blutige Geschäft. Wer in der Nähe der Ölquellen lebt, ist der ökologischen Katastrophe ausgeliefert. Peter Maass zeigt, wie der begehrte Rohstoff die Länder straft, die ihn besitzen, wie er jene korrumpiert, die mit ihm handeln, und die Welt verwüstet, die nach ihm dürstet."
    * Spielfilm: [Syriana (2005)](https://www.themoviedb.org/search/movie?query=Syriana)

ARD-Markencheck
---------------
### Milchpulver-Werbung als Baby-Nahrung
* Stand 2018
    * ["Das zynische Geschäft mit Säuglingsnahrung"](http://www.spiegel.de/wirtschaft/service/saeuglingsnahrung-danone-und-nestle-bewerben-ihre-produkte-in-entwicklungslaendern-massiv-mit-folgen-a-1208128.html), 2018
        * "Muttermilchersatz ist ein Milliardengeschäft [...]. Sie verkaufen ihre Produkte gezielt in Entwicklungsländern - und gefährden damit nach Ansicht von Wissenschaftlern die Gesundheit Hunderttausender Kinder."
        * "Internationaler Kodex der WHO wird ignoriert"
        * "Das Marketing wirkt"
        * "Aktion gegen den Hunger [...] Lebensmittelkonzerne unter Druck setzen wollen, die "aggressiven Marketingstrategien" für ihre Säuglingsnahrung zu stoppen und den Milchkodex vollständig umzusetzen"
    * ["Nestlé hat wieder Ärger wegen Babymilch"](https://utopia.de/nestle-hat-wieder-aerger-wegen-babymilch-78607/), 2018
        * "wegen Zusatzstoffen und eines mehr als fragwürdigen Werbeversprechens"
* Stand 2015
    * https://www.daserste.de/information/ratgeber-service/markencheck/der-nestle-check-2-folge-3-102.html
        * inkl. Kurzvideo
        * WHO-Milchkodex:
            * Keine Werbung für Milchpulver für Neugeborene
            * Keine kostenlosen Proben an Mütter verteilen
            * Keine Firmengeschenke an Gesundheitspersonal
        * Werbung:
            * Im ARD-Interview [...], dass der Konzern Müttern empfehle, „dass Stillen oberste Priorität hat“. Milchpulver sei nur als Ersatz gedacht, falls eine Frau nicht stillen könne. Auf den Werbeplakaten sieht das aber anders aus. Die Kaufanreize sind beruflicher Erfolg und Intelligenz: Die Flaschenmilch soll Kinder schlauer machen, versprechen die Slogans.
            * Zugang zu sauberem Wasser gibt es dort aber nicht. Auch die Hitze ist ein Problem: In der angerührten Milch bilden sich schnell Bakterien, die die Mütter trotzdem noch verfüttern.
            * Viele wissen nicht, dass Milch schnell schlecht wird oder dass das Wasser aus den verunreinigten Brunnen vor allem für Kinder eine große Gefahr ist. Aufgeklärt wird über die Risiken von Flaschenmilch bis heute nicht ausreichend, erst recht nicht von Seiten der Verkäufer von Milchpulver.

Der Goldene Windbeutel
----------------------
* https://www.foodwatch.org/de/informieren/werbeluegen/2-minuten-info/
* siehe werbung.md

Armut als Markt
---------------
* https://www.3sat.de/page/?source=/dokumentationen/190161/index.html, 2016
    * "Lebensmittelkonzerne entdecken Schwellen- und Entwicklungsländer - Der große Hunger ist vorbei. In den Märkten Europas und Nordamerikas verzeichnen die großen Lebensmittel-Konzerne kaum noch Wachstum. Unternehmen wie Nestlé und Unilever vertreiben deswegen zunehmend Produkte in Afrika oder Brasilien.
    Weltweit gibt es laut Schätzungen der Konzerne etwa drei Milliarden Konsumenten mit geringem Einkommen - ein riesiges Verkaufspotential. Allerdings lassen sich an ärmere Familien nur kleine Packungsgrößen gut verkaufen. Denn diese Menschen leben förmlich von der Hand in den Mund. Sie können täglich nur das ausgeben, was sie gerade verdient haben. Deshalb reicht der Verdienst nur für den Kauf von Kleinstpackungen. Dabei sind diese anteilig viel teurer als große."
    * "Nestlé packt für die neuen Kunden Markenprodukte wie "Maggi", "Alete" oder "Nescafé" in kleinere Portionen, reichert sie je nach Land und Bedarf mit Extra-Nährstoffen an und bringt sie zu vermeintlich geringerem Preis quasi bis vor die Haustür."
    * "Auch Unilever setzt auf Billig- oder Miniversionen seiner Produkte."
    * "In den Favelas stehen Fertiggerichte an erster Stelle auf dem Speiseplan von ärmeren Familien."
    * "Durch **industrialisiertes Essen** hat sich in Brasilien die Kalorienaufnahme in den letzten 20 Jahren verdoppelt. Das hat fatale Folgen. Denn gerade die Armen werden übergewichtig und leiden an Diabetes."
    * Doku "Das Geschäft mit der Armut", 2018

### Das Geschäft mit dem Hunger
* http://www.fr.de/wirtschaft/entwicklungslaender-das-geschaeft-mit-dem-hunger-a-765532, 2013
    * "Konzerne wie Coca-Cola oder Nestlé beanspruchen eine immer größere Rolle im Kampf gegen die Unterernährung für sich. Hilfswerke sind alarmiert - der Kampf gegen den Hunger soll nicht zum Geschäft verkommen."
        * Warum nicht?
    * "Tatsache sei, dass Saatgutkonzerne und Supermarktketten in Entwicklungs- und Schwellenländern die Bauern auf ihre Produkte verpflichten und Kleinhändler verdrängen."
    * "Wenn global agierende Nahrungsmittelkonzerne mit Kleinbauern Lieferverträge abschließen, sei das für die Farmer oft kein fairer Deal und risikoreich, argumentiert Studienautor und Misereor-Experte Benjamin Luig. Aufgrund der Marktmacht werde zum Beispiel oft nur ein winziger Bruchteil des Konsumentenpreises an die Erzeuger weitergegeben."
    * "Zudem forderten die Konzerne oft hohe Standards beim Anbau, die für die Bauern mit immensen Kosten verbunden seien."
    * Krass: "Der Vertragsanbau könne die Ernährungssicherheit sogar gefährden, erklärt Luig mit Hinweis auf eine Feldstudie aus Kenia, wo den beteiligten Landwirten der Mischanbau von Gemüse für den eigenen Verkauf und Verbrauch vertraglich untersagt wurde."
    * "Beim Bemühen der Privatwirtschaft, Kleinbauern in ihre Wertschöpfungskette zu integrieren, werden nach Ansicht von Misereor die strukturellen Ursachen von Hunger ebenso ausgeblendet wie der Zusammenhang zwischen den proklamierten „guten Taten“ von Konzernen und ihrem eigentlichen Geschäftsmodell."
    * "Misereor und das Forum für Umwelt und Entwicklung fordern stattdessen eine umfassende Strategie zur Stärkung der kleinbäuerlichen Landwirtschaft. Entwicklungsländer sollten sich bei ihrer Ernährungspolitik in erster Linie von unabhängigen Institutionen wie der der Weltgesundheits- oder Welternährungsorganisation beraten lassen – und nicht von den Vorgaben der Konzerne leiten lassen."

Interessante Aussagen
---------------------
### "Aber das ist doch deren Geschäftsmodell"
* Nur, weil ein Unternehmen sich ein vom Gesetz her erlaubtes Geschäftsmodell ausgesucht hat, heißt noch nicht, dass es auch ethisch vertretbar ist.
    * Beispiel Facebook etc.

### "Wer groß ist, steht halt im Fokus"
* Nur gibt es daneben auch viele große Unternehmen, die nicht negativ auffällig sind.
