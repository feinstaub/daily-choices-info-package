Öl
==

<!-- toc -->

- [Einstieg](#einstieg)
- [Gas Flaring](#gas-flaring)

<!-- tocstop -->

Einstieg
--------
* Buch: Öl

Gas Flaring
-----------
* World Bank
    * https://www.worldbank.org/en/programs/gasflaringreduction
        * "Billions of cubic meters of natural gas is flared annually at oil production sites around the globe. Flaring gas wastes a valuable energy resource that could be used to support economic growth and progress. It also contributes to climate change by releasing millions of tons of CO2 to the atmosphere."

    * "Increase in Global Gas Flaring", 2019
        * https://www.worldbank.org/en/news/press-release/2019/06/12/increased-shale-oil-production-and-political-conflict-contribute-to-increase-in-global-gas-flaring
        * "Increased Shale Oil Production and Political Conflict Contribute to Increase in Global Gas Flaring"
        * "Newly released estimates from satellite data show global gas flaring increased by 3% in 2018
            to 145 billion cubic meters (bcm), which is equivalent to the total annual gas consumption of Central and South America."
        * "takes place because of technical, regulatory, and/or economic constraints"

* 2017: "Gas flaring in the Niger Delta ruins lives, business"
    * https://www.dw.com/en/gas-flaring-in-the-niger-delta-ruins-lives-business/a-41221653

* Video: The Age Of Stupid, 2009, 90 min
    * https://www.youtube.com/watch?v=awVbLg59tR8
    * ...
    * 25:30 min...

