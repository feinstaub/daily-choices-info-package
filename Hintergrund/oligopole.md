Hintergrund: Nachteile von Oligopolen und Monopolen
===================================================

<!-- toc -->

- [Große Markenfirmen allgemein](#grosse-markenfirmen-allgemein)
- [Beispiele aus der Geschichte](#beispiele-aus-der-geschichte)
  * [Großkonzerne gegen Eisenbahn](#grosskonzerne-gegen-eisenbahn)
- [Steuertricks von Großkonzernen](#steuertricks-von-grosskonzernen)
- [Lebensmittelbereich](#lebensmittelbereich)
- [Softwarebereich](#softwarebereich)

<!-- tocstop -->

Große Markenfirmen allgemein
----------------------------
* siehe [Markenfirmen](marken-multinational.md)

Beispiele aus der Geschichte
----------------------------
### Großkonzerne gegen Eisenbahn
* Doku: ["Die Erdzerstörer: Sind wir Menschen die Bösen? | Doku | ARTE"](https://www.youtube.com/watch?v=yXYYWVAAKRc), 2019, Dokumentarfilm von Jean-Robert Viallet (F 2019, 99 Min)
    * "Kompromissloser Blick auf die vergangenen 200 Jahre des Industriekapitalismus"
        * "in Zusammenarbeit mit den Wissenschaftshistorikern Christophe Bonneuil und Jean-Baptiste Fressoz"
    * ...
    * ca. 30 min: kaufen Straßenbahngesellschaften von 45 amerikanischen Städten / Schiene
    * 33:20 min: wollen die Stadtverwaltungen nicht verkaufen --> Bestechung, Dienste der Unterwelt
        * "innerhalb von 10 Jahren zerstören die Industriellen 10.000e km Straßenbahnverbindungen"
            * Ersatz durch Busse, "um Absatzmärkte für ihre eigene Domänen zu schaffen" (Automobil und Erdöl)
    * 33:50: "das Ende der amerikanischen Straßenbahnen ist kein historisches Detail. Es ist ein Meilenstein. Ein Symbol für ungebrochenes Fortschrittsdenken."
        * Geschäft ist wichtiger als Umwelt
    * ...
    * 1929: Bankenkrise in Deutschland
    * 1933: Übernahme eines armen Landes (ärmer als England und Frankreich)
        * Rede: "Wir müssen - so oder so - auch den letzten Mann in Deutschland, der überhaupt tätig sein will, wieder in eine Arbeit bringen"
        * 37 min: Henry Ford war auch anti-semitisch? (todo)
        * Autobahn bauen (für den Krieg)
        * Gelände mit großen Mengen von Dynamit ebnen
        * Verbindung von Krieg und Öl
        * (1943 https://de.wikipedia.org/wiki/Sportpalastrede)
    * 40 min: Amerika: zur Kriegsvorbereitung wird die industrielle Produktion massiv hochgefahren. Vorbote für die Zukunft
        * 42:30 min: Dupont entwickelte für Krieg Nylon. Jetzt werden damit riesige Fischernetze möglich. -> industrieller Fischfang
            * Ortungsgeräte für Krieg werden nun Fischschwärme aufspüren. -> Beitrag zur restlosen Ausbeutung der Meeresresourcen
    * Atombombe
        * inklusive Experiment mit Hiroshima und Nagasaki
        * "Die Atomkraft ist fortan das perfekte Symbol für die menschliche Hybris"
        * krasse Vorschläge, was man mit der neuen Kraft alles machen kann, z. B. die Polkappen abschmelzen, Berge abtragen, Erz abbauen, Wüste beleben, Klima verändern
        * 52:30 Plowshare-Werbung
            * Plan u. a.: Panama-Kanal schaffen mit 300 Atombomben
            * tatsächlich 27 Atom-Explosionen zur zivilen Nutzung
        * 54:35 Programm Nr. 7 )sow. Plowshare), noch zerstörerischer (fast 150 Atomexplosionen)
    * ca. 1950: "die große Beschleunigung" beginnt (aus Sicht der Anthropzän-Leute)
        * Beginn von abc und Massenkonsum für Millarden Menschen
    * Levitt-Fertighäuser (40/50er Jahre?)
        * Bauen einer Menge identischer Häuser (17.000 innerhalb von 2 Jahren)
        * "bald können hier Familien ihr ruhiges, standardisiertes und glückliches Leben führen"
        * Neues Stadtmodell: Lewittown: Vorstadtsiedlungen mit Verkehrsanbindung
        * so günstig wie nie (auch dank Kreditgarantien): eigenes Haus für den Arbeiter billiger als Wohnung in der Stadt
        * damals verpasst (Amerika und die Welt): die von der Regierung geförderten (weil Öl sparen wegen Krieg) Solarhäuser
            * Entwicklerin eine Frau; 75 % Selbstversorgung
            * Universitäten entwickeln solarbetriebenen Durchlauferhitzer
            * Architekten sehen Solar als Ideal
            * nichts scheint dem Solarzeitalter entgegenzustehen
            * Marketingkampagnen für die immer neusten Elektrogeräte zur Erleichertung des Haushalts von General Electric und Kohlekraftwerbsbetreibern
            * Deals mit den Bauträgern, damit schon alles für Elektro vorgesehen ist und die Käufer zum Anschluss an die Stromnetze gezwungen werden
            * => Aus für Solarenergie
    * 1h 5min: Kalter Krieg: Wettrennen der Systeme
        * der jeweiligen Bevölkerung muss ein gutes Leben gewährleistet werden
        * Amerika: Autos für alle; jeder 6. Erwerbstätige direkt oder indirekt für die Automobilindustrie
        * Eisenhower: baut massig Straßen; Interstate-Highways; für 50 Millarden Dollar (sehr teuer; Begründung: Dezentralisierung etc.)
    * 1h 9min: Plastik-Werbung
    * Frankreich/Europa "tritt der Kultur des Konsumismus bei; samt ihren Ikonen und Tempeln"
    * Anfang 50er Jahre zwei Bücher mit hoher Auflage mit Warnsignalen bezüglich Ausbeutung von Amerika und Erde von Spitzen der Naturforschung (Wiliam Vogt, Fairfield Osborne)
        * Kampf gegen die Natur soll beendet werden
        * Mäßigung aber von Führungseliten nicht vereinbar mit Wettlauf gegen kommunistischen Block
        * Präsident Truman und Payley-Ausschuss zur Sicherung der Rohstoffe
            Ergebnis: Die ganze Welt soll ausgebeutet werden, um die Rohstofflieferungen sicherzustellen
    * Kalter Krieg und Fortschrittwettlauf => Ausbeutung der südlichen Länder erreicht neues Niveau
        * auch Russland macht bei seinem Land mit
            * Austrocknung des Aralsees
            * Kohleverschmutzung
            * Radioaktive Verseuchung
        * Rohstoffvorräte Afrikas, des mittlern Ostens und Lateinamerika werden innerhalb von Jahrzehnten leergeräumt
            * Westen: Lebensstandard steigt stetig
            * dort: Lebensstandard sinkt plus Umweltschäden = "ungleicher ökologischer Austausch"
    * Zusammenfassung: Übergang ins Anthropzän mit Fortschritt der Rüstungsindustrie, zivile Absatzmärkte, Ausbeutung der armen Länder, sondern auch...
    * 1h 16: Landwirtschaft / Gentechnik
        * Chemie
        * Pestizide
        * industrielle Stickstoffdüngerproduktion
        * ausgewählte Sorten, die Pestiziden und Düngemitteln standhalten und homogen genug für mechanische Ernte
        * Amerika und Rockefeller Stiftung bringen industrielle Landwirtschaft nach Mexiko, um Investitionen dort vor einer drohenden Hungersnot zu schützen
        * Rockefeller-Programm beginnt mit Hybrid-Mais; dann weitere Saatgut-Kontrolle: Weizen, Bohnen, Reis, Viehfutter
        * späte 50er Jahre
            * Indien steckt in einer großen Lebensmittelkrise
            * Instabilität Gefahr für den Westen
                * Indiens relativ freie Gesellschaft muss erfolgreich sein, weil andere Länder sonst das Modell des Kommunismus wählen würden
            * Gebündelte Kräfte zur Unterstützung der indischen Regierung zur Transformation der Landwirtschaft
                * alte Selbstversorger-Landwirtschaft in intensive und industrielle Landwirtschaft umwandeln
        * Agronom Borlock (bei Dupont): Erfolg mit Weizenzüchtung in Mexiko / Rockefeller-Stiftung
            * nun beauftragt dasselbe in Indien zu machen
            * Der Weizen funktioniert sehr gut: mit Düngemitteln und Bewässerung; inkl. Pestizide
            * Indischer Staat fördert das alles: Abhängigkeit von Chemie und Geräten (die hauptsächlich aus Amerika und Europa stammten)
            * 1970: Friedensnobelpreis; "ganz Asien bekennt sich nach und nach zur industriellen Landwirtschaft"
            * Gewinner sind die Landwirte, die zu Unternehmern wurden
                * es wurde nun mehr exportiert; weniger alle im eigenen Land ernährt
                * "Ökonomen und Historiker behaupten, dass man mit den gleichen Mitteln eine bessere Lebensmittelversorgung der Inder hätte sicherstellen können, wenn man auf die Entwicklung landwirtschaftlicher Kleinbetriebe gesetzt hätte"
        * 1h 25min: Chemische Keule und weltweiter Anbau immer gleicher Spitzensorten
            * diese Landwirtschaft: verheerende Auswirkungen auf die Natur
            * 1000e traditionell angebaute Sorten sind ausgestorben
            * drastischer Anstieg des Wasserverbrauchs, Düngemitteln und Pestiziden, Auslaugung bis totaler Sterilisation des Bodens, fossiler Energieverbrauch (Landwirtschaftsmaschinen, Düngemittel, Nahrungsmitteltransformation(?))
        * Beginn 19. Jh. Westeuropa: Verbrauch von einer Kalorie => 5 - 6 Lebensmittelkalorien
            * heute 1 verbrauchte Kalorie => 0,7 Lebensmittelkalorien
            * => energetisches Verlustgeschäft durch die Industrialisierung
    * 70 Mio Liter Agent Orange über Vietnam
        * ohne Rücksicht auf Bevölkerung
    * 1972 liefert Apollo-17 Fotografie des Erdballs, Symbol für abschlossene, endliche Welt
        * Club of Rome gibt Studie beim MIT in Auftrag: "The limits to growth"
            * mit Hilfe von Computern
            * 13 Zukunftsszenarien und Warnung vor Zusammenbruch im 21. Jahrhundert wegen Rohstoffmangel, Umweltverschmutzung und Überbevölkerung
            * erstmalige Formulierung der Grenzen des Wachstums von den Eliten des Kapitalismus
    * 1973, 1978: zwei Ölkrisen
        * 1h 31min: Rede von Jimmy Carter über die Grenzen der menschlichen Identität durch Konsum und materielle Güter (anstelle von dem, was der Mensch tut)
        * Berater fordern, das Land in die erneuerbare Energiewende zu führen
            * Solarzellen auf dem weißen Haus
            * auf der Höhe der Zeit; Menschen fordern das auch
        * tausende Prozessakten von vergangenen Industrieumweltschäden füllen die Schränke
            * z. B. Chemieunternehmen entschädigten jedes Jahr ihre Nachbarn für Ernteverluste und Zerstörung ihrer Umwelt
                * Die Modernisierer wussten also schon recht genau, was sie tun!
                * Auch die Ärzte damals schrieben der Umwelt einen entscheidenden Beitrag zur Gesundheit zu
                ' Anreiner machten gegen die Fabriken und ihre Verschmutzung mobil
            * ab ca. 1815: "zwei Jahrhunderte lang schlug man unaufhörlich Jahrzehnt für Jahrzehnt Alarm"
                * in den 70er Jahren dachte viele, es würde sich was ändern
                    * "Government is not the solution. Government is the problem"; Industrielobby organisiert sich gegen die ökologische Bewegung
                        * Thatcher, England
                        * Reagan, USA
                        * mit Boost durch Finanzindustrie, weitere Beschleunigung bis heute
    * seit 200 Jahren
        * Wettlauf, über Kontinente, der immer tiefere Gräben der Ungleichheit gräbt
        * "Konsum als Wirtschaftsmotor und Mittel gegen sozialen Aufruhr"
        * 20 % der Weltbevölkerung lebt diesen Standard und produziert damit die meisten Umweltschäden
        * auch Solarzellen brauchen Bergbauprodukte; digitale Infrastruktur frisst Energie
            * ähnlich wie damals: "sollten Wälder und Klima durch die Kohle gerettet werden"
        * Planet in letzter Sekunde vor uns selbst retten?

Steuertricks von Großkonzernen
------------------------------
* Doku: ["Steuern sparen wie Großkonzerne - ein Experiment"](https://www.youtube.com/watch?v=FjfoNLaE1Hc), 45 min, NDR, 2017, anschaulich
    * mit Cum-Ex, den Big Four, Apple, Amazon, Gerhard Schick, Lobbyismus, ehemaligen Steuerfahndern (die entlassen wurden), einem Whistleblower in Luxemburg, Modell IKEA, Niederlande ein Land der Briefkastenfirmen, selber eine gründen, Interview mit EU-Kommission, große Konzerne drohen abzuwandern, wenn sie kein Sonder-Ruling bekommen, anschaulichen Erklärungen, Steuersparmodell vorher mit dem Finanzamt abklären lohnt sich ab einem Gewinn von 10 Mio EUR, Grundgebühr dafür 20 T EUR, Interviews über Steuergerechtigkeit werden abgelehnt, Lösung ist nicht, dass auch die kleinen Leute mit Steuerbetrug anfangen, sondern dass man Steuertricks nicht wissentlich zulässt, bei keinem.
* ["Cum-Ex: Der größte Steuerraub in der deutschen Geschichte"](http://www.zeit.de/2017/24/cum-ex-steuerbetrug-steuererstattungen-ermittlungen), 2017
    * "Über Jahrzehnte plünderten Banker, Berater und Anwälte den deutschen Staat aus. Niemand verhinderte den Raubzug. Doch dann kam ihnen eine Frau auf die Spur."
    * http://www.zeit.de/wirtschaft/2017-05/cumex-skandal-steuern-verlorene-gelder-deutschland
        * "Davon Schaden, der durch juristisch umstrittene Steuertricksereien (Cum-Cum) entstanden ist: mindestens 24,6 Mrd. €"
        * "Davon durch mutmaßlich organisierte Kriminalität (Cum-Ex): mindestens 7,2 Mrd. €"

Lebensmittelbereich
-------------------
* Artikel: [Kartellamt warnt - Aldi und Co. nutzen Marktmacht aus](https://www.handelsblatt.com/unternehmen/handel-konsumgueter/kartellamt-warnt-aldi-und-co-nutzen-marktmacht-aus/10748874.html), Handelsblatt, 2014, ::aldi
    * "Die großen Lebensmittelhandelskonzerne Edeka, Rewe, Aldi und die Schwarz-Gruppe mit den Lidl-Märkten und Kaufland verdrängen nach einer mehrjährigen Untersuchung des Bundeskartellamts immer stärker ihre Wettbewerber."
    * "Es drohe eine weitere Verschlechterung des Wettbewerbs, der bereits jetzt zu 85 Prozent von diesen Unternehmen dominiert werde"
    * "Nach den Ergebnissen kaufen die Handelskonzerne anders als häufig behauptet **ganz überwiegend ihre Produkte im Inland ein**."
        * "Für diese Verhandlungen hätten die Einzelhändler „bereits jetzt einen gravierenden Vorsprung gegenüber
            ihren mittelständischen Konkurrenten und **genießen strukturelle Vorteile, die sie in den Verhandlungen mit den Herstellern nutzen können**“"
            * welche?
                * https://www.bundeskartellamt.de/Sektoruntersuchung_LEH.pdf?__blob=publicationFile&v=7, 460 Seiten
                    * ...
                    * "Als  Nachfragemacht  bezeichnet  man  in  sehr  allgemeiner  Form  die  Fähigkeit  eines  Unternehmens,  einseitig  die  Bezugskonditionen  für  die  von  ihm  eingesetzten  Vorprodukte  zu  seinen  Gunsten  gewinnsteigernd  zu  beeinflussen."
                    * ...
                    * "Insbesondere die drei Nachfrager  Edeka,  Rewe  und  Schwarz  Gruppe  weisen  diese  hohen  Beschaffungsmengen  bei  dem  Einkauf   von   Markenartikeln   auf.   Sie   verfügen   darüber   hinaus   über   weitere   Größen-   und Finanzkraftvorteile  im  Vergleich  zu  ihren  Wettbewerbern  und  sind  damit  in  der  Lage,  gezielt  und  erfolgreich Preiswettbewerb gegen ressourcenschwächere Wettbewerber zu führen und so auch ihre Position  auf  den  Absatzmärkten  zu  stärken.  Für  die  Hersteller  von  Markenprodukten  haben  diese  Händler     aufgrund     ihrer     hohen     und     steigenden     Absatzmengen     im     Hinblick     auf     die     Marktdurchdringung  die  Funktion  eines  „gatekeepers“  inne. "
        * "Dieser Verhandlungsmacht könnten selbst Hersteller bekannter Marken ausgesetzt sein, soweit es ihnen de facto an Ausweichalternativen für den Absatz ihrer Produkte fehle"
    * "Mit ihren **Eigenmarken** machten die Händler **zunehmend Druck in den Verhandlungen über die Einkaufskonditionen**.
        Einwände der Konzerne [...] seien nun empirisch widerlegt."
    * "Besonders stark ist Edeka dem Kartellamt zufolge im Markt platziert."
        * "Bei den Eigenmarken habe zwar Aldi eine herausragende Stellung"
    * siehe auch Sachbuch: Karen Duve - Die Ernährungsdiktatur

* "Milch: So zockt der Handel die Bauern ab" - https://www.agrarheute.com/markt/milch/milch-so-zockt-handel-bauern-ab-561557, 2019, Kommentar
    * "Der Lebensmittelhandel nutzt seine Marktmacht brutal aus. Das zeigen die jüngsten Trinkmilchabschlüsse."
    * Lebensmitteleinzelhandel (LEH) hebt Preise für Milch, aber gibt sie nicht weiter
        * "Das zusätzliche Geld landet in den Kassen des Handels."
        * "bei Biomilch gab es eine Nullrunde. Teilweise mussten die Biomilchhersteller gar mit einem Minus von 2 Cent/l den Verhandlungstisch verlassen." / zu Hypothese ::aldi sei gut für ::bio
    * "Einheitliche Preiserhöhungen wirken wie abgesprochen"
        * "Von einer fairen Partnerschaft kann hier keine Rede sein. Und die Politik schaut machtlos zu. Die angekündigte Verschärfung des Wettbewerbsrechts auf EU-Ebene sind reine Lippenbekenntnisse. Reine Papiertiger."

* https://edition.cnn.com/interactive/2019/05/business/aldi-walmart-low-food-prices/index.html, 2019
    * "Aldi’s lasting impact: Lower prices and fewer grocers"
        * "Although huge competitors can reduce prices to compete with Aldi, regional supermarkets are getting squeezed by the grocery price war.", ::aldi, ::arbeitsplätze
        * "Aldi and Lidl will be a significant disrupting force in the US, threatening smaller regional supermarket chains and forcing larger players to cut prices"
        * "With smaller grocers disappearing, there’s probably room for both Walmart and Aldi to pick up the pieces"

Softwarebereich
---------------
* siehe z. B. [IT-an-Schulen](../Hintergrund/IT-an-Schulen.md)
