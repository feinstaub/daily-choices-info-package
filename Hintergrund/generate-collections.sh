#!/bin/bash

TARGET=Hintergrund/buch-und-film-liste.md

INCLUDE=\*.md
EXCLUDE=buch-und-film-liste.md

echo "Schreibe TARGET=$TARGET..."

cat Hintergrund/buch-und-film-liste.header.md > $TARGET

echo "Sachbücher" >> $TARGET
echo "----------" >> $TARGET
grep -r --include=$INCLUDE --exclude=$EXCLUDE "Sachbuch:" | sort >> $TARGET
echo "" >> $TARGET

echo "Sonstige Bücher" >> $TARGET
echo "---------------" >> $TARGET
grep -r --include=$INCLUDE --exclude=$EXCLUDE "Buch:" | sort >> $TARGET
echo "" >> $TARGET

echo "Filme" >> $TARGET
echo "-----" >> $TARGET
grep -r --include=$INCLUDE --exclude=$EXCLUDE "Film:" | sort >> $TARGET
echo "" >> $TARGET

echo "Dokus" >> $TARGET
echo "-----" >> $TARGET
grep -r --include=$INCLUDE --exclude=$EXCLUDE "Doku:" | sort >> $TARGET
echo "" >> $TARGET

echo "Videos" >> $TARGET
echo "------" >> $TARGET
grep -r --include=$INCLUDE --exclude=$EXCLUDE "Video:" | sort >> $TARGET
echo "" >> $TARGET

echo "Zahlen" >> $TARGET
echo "------" >> $TARGET
grep -r --include=$INCLUDE --exclude=$EXCLUDE "Zahlen:" | sort >> $TARGET
echo "" >> $TARGET

# Ersetze:
#   'Hintergrund/unsortiert.md:'
# durch:
#   '* [../Hintergrund/unsortiert.md](Hintergrund/unsortiert.md):'
sed -i 's|^\(.*\.md\):|* [ref](../\1):|g' $TARGET

# Ersetze:
#   ':    Buch:' oder 'Sachbuch' oder ': Film:' oder ': Video:' oder 'Zahlen:'
# durch
#   ':'
sed -i 's/:.*\(Buch\|Sachbuch\|Film\|Doku\|Video\|Zahlen\):/:/g' $TARGET

# Vertausche Buch/Filme/etc. und Referenz:
sed -i 's/\(\[ref.*.md)\): \(.*\)/\2 (\1)/g' $TARGET

echo "DONE"
