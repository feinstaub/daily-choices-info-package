Kolonianismus
=============

<!-- toc -->

- [Kolonianismus](#kolonianismus)
  * [Intro](#intro)
  * [Reifen-Doku](#reifen-doku)
  * [Völkermord an den Herero und Nama](#volkermord-an-den-herero-und-nama)
  * [Spanische Kolonisation Lateinamerikas](#spanische-kolonisation-lateinamerikas)
  * [(Post)kolonialismus und Globalgeschichte](#postkolonialismus-und-globalgeschichte)
  * [Imperialismus](#imperialismus)
  * [Homophobie in Afrika](#homophobie-in-afrika)
  * [Beispiele von Ausrottung von Urvölkern](#beispiele-von-ausrottung-von-urvolkern)

<!-- tocstop -->

Kolonianismus
-------------
### Intro
* https://de.wikipedia.org/wiki/Kolonialismus

* 2019: "Von Menschen und Herrenmenschen - Kolonialismus und seine Folgen", ZDF
    * ...
    * ...

* "Deutschland in Afrika - Der Kolonialismus und seine Nachwirkungen", 2005
    * http://www.bpb.de/internationales/afrika/afrika/58870/deutschland-in-afrika?p=all
    * todo

* https://de.wikipedia.org/wiki/Liste_der_Aufst%C3%A4nde_in_den_deutschen_Kolonien "Liste der Aufstände in den deutschen Kolonien"

* indigene Bevölkerung

### Reifen-Doku
* siehe "Reifen-Doku"

### Völkermord an den Herero und Nama
* siehe erinnerung

### Spanische Kolonisation Lateinamerikas
* siehe erinnerung

### (Post)kolonialismus und Globalgeschichte
* "Dekolonisation im 20. Jahrhundert", https://www.bpb.de/geschichte/zeitgeschichte/postkolonialismus-und-globalgeschichte/219139/dekolonisation-im-20-jahrhundert, 2016
    * TODO

### Imperialismus
* https://de.wikipedia.org/wiki/Kolonisation#Zeitalter_des_Imperialismus
    * "Der Imperialismus bezeichnet das Weltmachtstreben insbesondere der europäischen Großmächte."
    * "Parallel zur Intention, die als „unterentwickelt“ bezeichneten Völker vor allem Afrikas zu
    beherrschen, wurde die Ideologie des Sozialdarwinismus in Verbindung mit wissenschaftlich unhaltbaren Theorien menschlicher Rassen konstruiert."
    * https://de.wikipedia.org/wiki/Imperialismus

### Homophobie in Afrika
"Die meisten homophoben Gesetze sind ein direktes Erbe des Kolonialismus" (siehe dort: amnesty)

### Beispiele von Ausrottung von Urvölkern
* Vergangenheit:
    * siehe Buch: https://de.wikipedia.org/wiki/Arm_und_Reich_(Diamond)
        * "So wurde etwa das Inka-Reich in kurzer Zeit von nur 160 Spaniern vernichtet"
        * "die von den Europäern eingeschleppten Krankheiten waren, die oftmals über 90 Prozent der einheimischen Bevölkerung auslöschten"
* Gegenwart:
    * [Arte-Doku zu Tiermythen](https://www.arte.tv/de/videos/050830-003-A/tiermythen/), 2017
        * "Jahrtausende lang haben in Amazonien die uralten Geschichten, der Glaube an die Magie und das einzigartige Wissen über Tiere und Pflanzen, ein Gleichgewicht zwischen Mensch und Natur bewahrt. Silvio beginnt sich zu engagieren, denn er weiß: Der Mensch zerstört den Lebensraum des Botos und damit auch die Lebensgrundlage der Ureinwohner. Genau das versucht der 38-Jährige zu verhindern."
        * Welsfischerei (in Südamerika?): Delfine werden zerhackt und als Köder verwendet
        * Stadt (welche?) frisst sich in den Amazonas
        * Einwohner wissen nicht, wie man sich ohne Tiere ernähren kann
