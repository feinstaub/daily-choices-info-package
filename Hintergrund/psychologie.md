Psychologie
===========

<!-- toc -->

- [Psychologists for Future](#psychologists-for-future)
  * [Anleitung zum Unbesorgtsein](#anleitung-zum-unbesorgtsein)
  * [YT](#yt)
- [Sokrates-Methode](#sokrates-methode)
  * [Intro](#intro)
  * [The Socratic Method with Trevor Noah](#the-socratic-method-with-trevor-noah)
  * [Telefonakquise](#telefonakquise)
  * [Gesprächsführung](#gesprachsfuhrung)
- [Zeitdruck, rein monetäres Denken => Gewalt](#zeitdruck-rein-monetares-denken--gewalt)
- [Keine Zeit trotz zeitsparender Technik?](#keine-zeit-trotz-zeitsparender-technik)
- [Umweltpsychologie](#umweltpsychologie)
  * [Inbox 2020 - Why People Take Things Out of Context](#inbox-2020---why-people-take-things-out-of-context)
  * [Inbox 2020 - Wettbewerb, Is Competition Human Nature?](#inbox-2020---wettbewerb-is-competition-human-nature)
  * [Inbox 2020 - Emotionen als Anstoßgeber](#inbox-2020---emotionen-als-anstossgeber)
  * [Professor](#professor)
  * [Zeitschrift](#zeitschrift)
- [Weisheiten aus der Glücksforschung](#weisheiten-aus-der-glucksforschung)
  * [Langweile](#langweile)
- [Verschiedenes](#verschiedenes)
  * [Werbung](#werbung)
  * [Phasen der Erkenntnis unbequemer Wahrheiten](#phasen-der-erkenntnis-unbequemer-wahrheiten)
  * [Klein anfangen](#klein-anfangen)
  * [Warum ist sich für Gutes zu engagieren immer so anstrengend?](#warum-ist-sich-fur-gutes-zu-engagieren-immer-so-anstrengend)
  * [Action for Happiness](#action-for-happiness)
  * [Negatives nicht ausblenden / Blinde Flecken](#negatives-nicht-ausblenden--blinde-flecken)
  * [Desinformation, PLURV und Umgang mit Verschwörungsmythen](#desinformation-plurv-und-umgang-mit-verschworungsmythen)
- [Liste von kognitiven Verzerrungen](#liste-von-kognitiven-verzerrungen)
  * ["Dunning-Kruger-Effekt"](#dunning-kruger-effekt)
- [Transparenz/Sichtbarkeit hat positive Auswirkungen](#transparenzsichtbarkeit-hat-positive-auswirkungen)
  * [Beispiel: Bilder auf Tabakprodukten](#beispiel-bilder-auf-tabakprodukten)
  * [Beispiel: Bedingungen in der Tierprodukt-Herstellung](#beispiel-bedingungen-in-der-tierprodukt-herstellung)
  * [Beispiel: Hygieneampel bei Restaurants](#beispiel-hygieneampel-bei-restaurants)
  * [Beispiel: Zutatenliste bei Lebensmitteln](#beispiel-zutatenliste-bei-lebensmitteln)
- [Inbox 2020](#inbox-2020)
  * [Jordan B. Peterson](#jordan-b-peterson)
- [Inbox 2019](#inbox-2019)
  * [How to spot a liar | Pamela Meyer](#how-to-spot-a-liar--pamela-meyer)
  * [Personality](#personality)

<!-- tocstop -->

Psychologists for Future
------------------------
* https://twitter.com/Psychologists4F
    * ...
    * #NationalitätMensch

### Anleitung zum Unbesorgtsein
* "Unsere 7 Tipps für ein unbesorgtes Leben im Angesicht der #Klimakatastrophe."
* https://twitter.com/Psychologists4F/status/1230081601444425728
* "1. Denke stets: Wenn xy nicht darüber spricht, wird es "so schlimm nicht sein"."
* "2. Rege dich über die Botschafter*innen auf!"
* "3. Erinnere dich! Früher wurde alles gut!"
* "4. Leugne!"
* "5. Vermeide!"
* "6. Die machen das schon!"

### YT
* "Rede Psy4F Würzburg", 2020
* Channel: https://www.youtube.com/channel/UCs6nhXdezhVJ6I_MXUXh2_g
    * Politikpsychologe Prof. Dr. Thomas Kliche im Interview
        * "Muss Verzicht immer mit Leid verbunden sein?"
        * "Gewohnheiten verändern - Welche Techniken gibt es?"
        * "Bewusstsein für die Klimakatastrophe unterstützen - Was kann man machen?"

Sokrates-Methode
----------------
### Intro
* siehe Ed Winters
* TODO: https://de.wikipedia.org/wiki/Sokratische_Methode
    * ...
* TODO: https://opensource.com/open-organization/17/5/better-it-socratic-method, "How Socrates taught me to talk to developers"
    * "A structured series of probing questions can uncover your team's guiding cultural assumptions.", ::kultur
    * ...

### The Socratic Method with Trevor Noah
* https://www.youtube.com/watch?v=lgXqaS08ZqM - "How To Win An Argument Without Making Enemies", 2019, 13 min, Sokrates, ::komm
    * ...
    * sub communication: not here for a fight
        * calm tone
        * slow things down (e.g. repeat to overcome interruption; take a deep breath)
        * inflection
    * ...

### Telefonakquise
* "So meistern Sie die Telefonakquise: Leitfaden und Tipps", https://www.fuer-gruender.de/wissen/unternehmen-fuehren/akquise/telefonakquise/
    * TODO
* "How to create a sales call script", https://blog.close.com/how-to-create-a-sales-phone-script-free-template

### Gesprächsführung
* "Psychologie der Gesprächsführung: Wer fragt, der führt!", https://www.akademie-weinheim.de/seminare/seminar-beschreibung/2056/
    * "Kommunikation und Gesprächsführung gehört zu den wichtigsten Kompetenzen einer Führungskraft.
        In diesem intensiven Workshop schärfen Sie Ihr Profil, lernen gute Fragen zu entwickeln, führen konstruktivere Dialoge und erreichen besser Ihre Ziele."
    * "Persönlichkeitstypen im Gespräch / Die Welt der Fragen / Basisfragen und systemische Fragen / Stärkung von Empathie und Verständnis /
        Perspektiven wechseln / Gesprächspartner besser einschätzen / Wirksamere Gespräche führen /
        Kompetenzraster: Selbstreflexion / Menschen abholen und motivieren / Weichmacher und Killerphrasen /
        Emotionsregulierung / Übungen und Rollenspiele / Intensiver Austausch in der Seminargruppe"

Zeitdruck, rein monetäres Denken => Gewalt
------------------------------------------
* 2020: "Traumatische Geburt - Gewalt im Kreißsaal | WDR Doku", 2020, https://www.youtube.com/watch?v=OrK2wnS_8Xo, 45 min
    * "Die Entbindung wurde künstlich eingeleitet, dauerte 3 Tage - und endete in einem Kaiserschnitt, bei dem die Ärzte operierten, ohne die Wirkung der Narkose abzuwarten"
    * "Seitdem bestimmen Flashbacks, Alpträume und Panikattacken Sabrinas Alltag."
    * "Auch ihr Mann und ihre Mutter leiden unter Sabrinas psychischen Ausnahmezustand."
    * Hebamme: "Doch im Kreißsaal erlebte sie immer wieder, dass Frauen nicht zugehört wird und sie nicht gefragt werden.
        Dass sie mangelhaft aufgeklärt und in routinierte Abläufe gezwungen werden.
        Dass viele am Ende die Geburt ihres Kindes sogar als Misshandlung, Nötigung oder Vergewaltigung beschreiben."
    * https://de.wikipedia.org/wiki/Kristeller-Handgriff
    * unnötige Interventionen, nur wegen Zeitdruck, "zuwenig Aufklärung, zuviel ökonomisches Denken"
    * Wo bleibt der Mensch?

Keine Zeit trotz zeitsparender Technik?
---------------------------------------
* Doku: ["Speed - Auf der Suche nach der verlorenen Zeit"](https://www.youtube.com/watch?v=offrOwIL3rM), 2012
    * Warum wird alles immer schneller und was kann man dagegen tun?
* Doku: ["Schluss mit Schnell"](https://www.youtube.com/watch?v=QHMG2XwPLyY), ARTE F, 2014, 1h 25 min
    * Ankündigung von konkreten Menschen, die sich dem "Diktat der Dringlichkeit" widersetzen; neue fruchtbare Beziehung mit der Zeit entdecken
    * [Hartmut Rosa](https://de.wikipedia.org/wiki/Hartmut_Rosa), Soziologe und Politologe
        * 15. August 1965 in Lörrach, "lehrt an der Friedrich-Schiller-Universität Jena"
        * erster Schritt zur Besserung: Bewusstwerdung der gesellschaftlichen und psychologischen Vorgänge, welche systemischen Zwänge erzeugt werden, was das alles mit Selbstbestimmung zu tun hat (oder dem Gefühl deren Verlustes)
        * "Zeitsoziologische und modernetheoretische Untersuchungen bilden die Basis seiner Habilitationsschrift „Soziale Beschleunigung. Die Veränderung der Temporalstrukturen“. Die „technische beziehungsweise ökonomisch induzierte Beschleunigung“ zeigt sich in der rasanten Entwicklung der Technik im 19./20. Jahrhundert und der sozialen Beschleunigung der Menschen. Die Geschichte der Moderne sei gleichzeitig die Geschichte von Beschleunigung. Aufgrund des Zeitgewinns durch technischen Fortschritt entstehe eine Zeitnot und kein Zeitgewinn."
    * kam das vor?
        * "neue Form der Gewalt"
        * "Kolonialisierung der menschlichen Zeit - ökonomische Zeit"? / Kolonialismus
    * https://www.arte.tv/de/videos/048763-000-A/schluss-mit-schnell/
        * "Die globalisierte Beschleunigung hat uns alle fest im Griff. Verantwortlich für diese Geschwindigkeit ist die unkontrollierte Entwicklung von Wissenschaft, Technik und Wirtschaft. Wir sind in einem Zustand permanenten Zeitdrucks. Doch überall auf der Welt verweigern sich immer mehr Menschen dem allgegenwärtigen Stress. Eine Ode an das selbstbestimmte Leben. "
        * "Immer schneller, immer effizienter, immer rentabler – was haben wir aus der Zeit gemacht? Die Zeit scheint sich dem allgemeinen Maß des Geldes nicht mehr entziehen zu können. Wir sind in die Ära der Beschleunigung eingetreten, in die Ära der Norm gewordenen Unverzüglichkeit.
        Aber zu welchem Preis? Im Finanzwesen und in der Hightech-Branche führt der immer größere Zeitdruck zu ökologischen, wirtschaftlichen und sozialen Katastrophen."
        * "Als Gegenmodell zum Wettlauf um Zeit und Rentabilität könnten diese Alternativen beispielhaft für die Welt von morgen sein. Im Grunde sind sie die praktische Umsetzung der kritischen Analysen von Philosophen, Soziologen, Wirtschaftswissenschaftlern und Forschern wie Pierre Dardot, Rob Hopkins, Geneviève Azam und Bunker Roy."
    * related:
        * ["Wichtig oder Dringlich"](extras.springer.com/2011/978-3-642-13719-8/Data/Wichtig%20oder%20dringlich.pdf)
            * A-B-C-D-Prio
* Doku: ["Gutes Leben – ohne Konsum?"](https://www.youtube.com/watch?v=JnEWVgEM38U), 2017, ARD, 30 min
    * Familie zieht in eine Jurte
    * kein Schuldenverhältnis
    * Tiny House, 25 qm auf Rädern für 26 T EUR
    * TODO
* Video: ["Faulsein — Anleitungen zum Müßiggang"](https://www.youtube.com/watch?v=VNwBAyRO9qM), 35 min, 2016, Philosophie/Lebenskunst
    * Österreichischer Fotograf
        - ...
    * Anselm Grün
        - ...
    * Augustin-Verkäufer
        - ...
    * todo
* Video: ["Wer glücklich ist kauft nicht - Vortrag Prof. Hüther"](https://www.youtube.com/watch?v=b-zV8aGDCTE), 2014, Soziologie, (Verantwortung des Einzelnen)
    * [Prof. Hüther](https://de.wikipedia.org/wiki/Gerald_H%C3%BCther) - Hirnforscher
    * 5:10 min: So wie wir aktuell denken und handeln beeinflusst unsere Mitmenschen um uns herum. Diese Denk- und Verhaltensmuster geben wir auch an unsere Kinder weiter. "Und so bestärken wir uns nun schon seit einigen Generationen in der Einstellung, dass man als Einzelner kurzfristig am besten vorankommt, wenn man sich aus allen Verbundenheiten, Verantwortlichkeiten und Verpflichtungen herauslöst; wenn man rücksichtslos und blind für alles, was man dabei zerstört, seine eigenen Ziele verfolgt. Und die Vorstellung, jeder sei seines eigenen Glückes Schmied und alles sei machbar ist deshalb zu einem festen Bestandteil unseres Weltbildes, unseres Menschenbildes und unseres eigenen Selbstbildes - jedenfalls der meisten Menschen - in unserem abendlichen Kulturkreis geworden." (siehe auch Hüther & Spannbauer 2012, S. 7 (?))
    * todo
    * Gerald Hüther
        * Hirmforscher, Vorstand Akademie für Potentialentfaltung
        * Zitate aus Amnesty-Journal-Interview 1/2017
            * "Man muss wieder Subjekt werden, damit man die Welt verändern kann."
            * "Wir werden dann zu Menschen, wenn wir aufhören, andere Menschen als Objekt zu benutzen."

Umweltpsychologie
-----------------
siehe auch ::Umweltkommunikation

### Inbox 2020 - Why People Take Things Out of Context
* siehe eisenstein

### Inbox 2020 - Wettbewerb, Is Competition Human Nature?
* https://charleseisenstein.org/video/is-competition-human-nature/, 7 min

### Inbox 2020 - Emotionen als Anstoßgeber
* https://www.ews-schoenau.de/energiewende-magazin/zur-sache/das-gefuehl-entscheidet/
    * "Die Fakten zur Klimakrise sind bekannt – doch kaum jemand ändert sein Verhalten. Umweltpsychologen erkunden nun die Rolle von Emotionen als Anstoßgeber."
    * ...
    * ""Wir müssen stärker vermitteln, dass ökologische Alternativen auch irre toll sein können."
        Prof. Gerhard Reese, Umweltpsychologe, Uni Koblenz-Landau"
        * "'Ein Herz für den Klimawandel' - Prof. Gerhard Reese beim #59 Science Slam Berlin", 13 min
            * Reverse Psychology: "hat da mal drei Schritte mitgebracht, wie wir das Paris Agreement vielleicht sogar noch boykottieren können."
    * ...

### Professor
* https://www.nwzonline.de/oldenburg-kreis/bildung/umweltpsychologe-im-nwz-interview-raus-aus-dem-hamsterrad-und-hin-zur-nachhaltigkeit_a_50,4,997412445.html, 2019
    * "Wie mit den eigenen psychischen Ressourcen das Umweltverhalten verbessert werden kann, erklärt Prof. Dr. Marcel Hunecke an diesem Dienstag, 19. März, im Ganderkeseer Rathaus. Er hat den Begriff der Umweltpsychologie geprägt und steht ein für ein nachhaltiges Bewusstsein der eigenen Umwelten."
    * "Es geht um die Wechselwirkung zwischen Psyche und Umwelt. Zur Umwelt gehören sowohl bebaute, physikalische Umwelten als auch soziale Umwelten. Umweltpsychologie ist eine lange etablierte Teildisziplin der Psychologie, in der das Thema Nachhaltigkeit stark an Bedeutung gewonnen hat. Mittlerweile scheint fast überall angekommen zu sein, dass sich in Bezug auf unseren Ressourcenverbrauch etwas ändern muss."
    * Hamsterrad
        * "Das Hamsterrad steht für die Summe an Anforderungen und Belastungen, denen wir alltäglich ausgesetzt sind. Um dies zu ändern, müssen wir unseren Lebensstil ändern. Hierzu zählt auch, auf Dinge zu verzichten, bei denen wir erst einmal das Gefühl haben, es wird uns etwas weggenommen. Mein Ziel ist es, Menschen dadurch positiv zu einem nachhaltigen Lebensstil zu motivieren, dass sie einen freiwilligen Verzicht nicht als Mangel, sondern als Gewinn wahrnehmen. Eine Veränderung des eigenen Verhaltens kann eine Einschränkung von Flugreisen, die Reduktion des Fleischkonsums oder auch der Wechsel zu einer ökologisch-ethischen Bank sein."
    * sechs psychische Ressourcen
        * "Ich habe sechs Ressourcen für einen nachhaltigen Lebensstil aus einer Liste von etwa 20 bekannten Ressourcen aus der Psychologie identifiziert, die sich gezielt fördern lassen: Selbstwirksamkeit, Selbstakzeptanz, Achtsamkeit, Genussfähigkeit, Sinnkonstruktion und Solidarität."
            "Dabei ist es auch wichtig, dass unterschiedliche gesellschaftliche Handlungsfelder vorhanden sind, zum Beispiel Schulen oder Unternehmen, in denen diese psychischen Ressourcen gefördert werden können."
    * Stress?
        * "Die psychische Ressource Achtsamkeit ist sehr stark mit Ansätzen zur Stressbewältigung verbunden."
    * "Vortrag basiert auf meinem Buch „Psychologie der Nachhaltigkeit“ aus dem Jahr 2013."
    * Politik!
        * "Wichtig ist, dass eine nachhaltige Entwicklung aber nicht nur auf einer individuellen, sondern auch auf einer politischen Ebene umgesetzt werden muss. Wenn sich die Verhältnisse nicht ändern, kann der Einzelne auf Dauer auch nicht viel ausrichten."

* Mehr zu dieser Forschung
    * http://www.report-psychologie.de/nc/heft/archiv/?tx_rparchive_pi1%5Barticle%5D=650&tx_rparchive_pi1%5Baction%5D=show&tx_rparchive_pi1%5Bcontroller%5D=Article&cHash=32fd43a2106a42a91e9d997c88294967 - "Mal eben die Welt retten …"
    * https://www.fh-dortmund.de/de/fb/8/personen/lehr/hunecke/index.php

### Zeitschrift
* https://www.umweltpsychologie.de
    * ...

Weisheiten aus der Glücksforschung
----------------------------------
* todo

### Langweile
* "Der Psychologe Leon Windscheid erklärt bei Markus Lanz warum unser Hirn Langeweile braucht und wie gefährlich es ist, Langeweile zu unterdrücken."
    * https://www.youtube.com/watch?v=8mHZGak1z64, 2017, 3 min
        * Gedanken wandern, höhere Gehirnaktivität, mehr Kreativität

Verschiedenes
-------------
### Werbung
siehe werbung.md

### Phasen der Erkenntnis unbequemer Wahrheiten
* Unkenntnis
* -> Gewahrwerdung ("Aber man muss doch was tun")
* -> Feststellen, dass es nicht so einfach ist
* -> Resignation
    * "Die Welt ist schlecht und es ist sowieso zu spät"
    * "Ich würde ja gerne, ich kann aber nichts tun"
* -> Erkenntnis ("Aber eigentlich _will_ ich mich nicht ändern müssen")
    * hier bleiben viele stehen
* -> "Ich probiers mal mit einer Veränderung"
    * Zitat: "Das wenige das du tun kannst, ist viel"
* todo: Quelle

### Klein anfangen
* Die eigene Meinung passt sich den eigenen Handlungen an (Quelle: ...)
    * Kleine Schritte erzeugen eine Wirkung.

### Warum ist sich für Gutes zu engagieren immer so anstrengend?
* Fragen:
    * Ist es nicht so, dass der Mensch von der Biologie her eigennützig und bequem ist?
    * Wenn ich nun im Supermarkt korrekt einkaufen möchte, muss ich immer aktiv nach den richtigen Sachen suchen. Das ist anstrengend. Einfacher wäre es doch, einfach zugreifen zu können.
    * Der Geist muss also immer gegen die Biologie arbeiten?
* Schnellantworten:
    * bequem: ja
    * eigennützig: nein, denn der Mensch ist auch ein inhärent soziales Wesen
        * will nicht gern allein sein
        * ist rational (oder kann es zumindest)
    * immer aktiv nach Sachen suchen: ja, das muss so sein, aber man kann es einfacher oder schwieriger gestalten
    * Wo ein Wille ist, da ist auch ein Weg. Alles Gewöhnungssache. Der Mensch ist ein Gewohnheitstier.

### Action for Happiness
* http://www.actionforhappiness.org/
    * http://www.actionforhappiness.org/friendly-february
        * http://www.actionforhappiness.org/media/644562/friendly_february.jpg - Friendly Action Calendar
        * http://www.actionforhappiness.org/media/625566/kindness_calendar.jpg - Kindness Calendar

### Negatives nicht ausblenden / Blinde Flecken
* Relativierung der eigenen Perspektive durch Gewahrwerdung der [Blinden Flecken](saubere-weste-blinde-flecken.md).

### Desinformation, PLURV und Umgang mit Verschwörungsmythen
siehe wissenschaft.md

Liste von kognitiven Verzerrungen
---------------------------------
https://de.wikipedia.org/wiki/Liste_von_kognitiven_Verzerrungen

...

### "Dunning-Kruger-Effekt"
* https://de.wikipedia.org/wiki/Dunning-Kruger-Effekt
    * "in der Fachliteratur unübliche Begriff"
    * "bezeichnet die kognitive Verzerrung im Selbstverständnis inkompetenter Menschen, das eigene Wissen und Können zu überschätzen."

Transparenz/Sichtbarkeit hat positive Auswirkungen
--------------------------------------------------
### Beispiel: Bilder auf Tabakprodukten
* Vorbild und Vorreiter: Australien
* 2017: "Schockbilder" zeigen in Deutschland Wirkung: die Steuereinnahmen von Tabakprodukten gingen stark zurück.

### Beispiel: Bedingungen in der Tierprodukt-Herstellung
* Vorschlag zur Diskussion: Einführung von realitätsnahen Bildern auf Kuh-Milch-Packungen oder Produkten, die Kuh-Milch enthalten.

### Beispiel: Hygieneampel bei Restaurants
* ... nicht umgesetzt ...

### Beispiel: Zutatenliste bei Lebensmitteln
* ...

Inbox 2020
----------
### Jordan B. Peterson
* Web
    * https://en.wikipedia.org/wiki/Jordan_Peterson
        * https://en.wikipedia.org/wiki/12_Rules_for_Life, 2018, Bestseller
            * "you are left with a hardline self-help manual of self-reliance, good behaviour, self-betterment and individualism"
            * "some of his critics might be surprised to find much of the advice he offers unobjectionable, if old-fashioned: he wants young men to be better fathers, better husbands, better community members."
    * https://www.theguardian.com/science/2018/feb/07/how-dangerous-is-jordan-b-peterson-the-rightwing-professor-who-hit-a-hornets-nest
        * https://de.wikipedia.org/wiki/Identit%C3%A4tspolitik - Identitätspolitik
    * https://www.smh.com.au/world/north-america/right-winger-not-me-says-alt-right-darling-jordan-peterson-20180417-p4za14.html
        * controversial: "Right-winger? Not me, says alt-right darling Jordan Peterson"
        * climate change: "Most of the global warming posturing is a masquerade for anti-capitalists to have a go at the Western patriarchy. That’s partly why the climate change thing for me is a contentious issue, because you can’t trust the players. You can’t trust the data because there is too much ideology involved."
* YT
    * from "Professor Jordan Peterson on climate change and climate policy at the Cambridge Union", https://www.youtube.com/watch?v=pBbvehbomrY, 6 min, 2018
        * defeatist?
        * recommends https://de.wikipedia.org/wiki/Bj%C3%B8rn_Lomborg
            * "Für Überraschung sorgte Lomborg [...] im August 2010 [...] Es sei Tatsache, dass es eine globale Erwärmung gebe,
            dass sie vom Menschen verursacht werde und dass etwas dagegen getan werden müsse.
            Dazu schlug Lomborg die Einführung einer Steuer auf CO2-Emissionen vor."
                * https://www.zeit.de/2010/42/U-Interview-Lomborg?page=1
                    * ...
                    * "Die Frage war: Wo verspricht Hilfe den größten Erfolg? Beim Klimawandel jedenfalls nicht.
                        Hunger dagegen können wir sehr effizient bekämpfen. Und wer heute Abend hungrig ins Bett geht,
                        den interessiert die Temperatur in 100 Jahren nicht. "
                    * ...
    * over "Jordan B. Peterson | Full interview | SVT/TV 2/Skavlan", https://www.youtube.com/watch?v=_iudkPi4_sY, 38 min, 2018
        * interesting!
        * ...
        * These: je gleicher die Chancen von Mann und Frau, desto größer scheinen die biologischen Unterschiede durch
        * ...
    * to "Jordan Peterson on The Necessity of Virtue", https://www.youtube.com/watch?v=gwUJHNPMUyU, 50 min, 2011
        * "illustrates the necessity of virtue both for the individual and for society at large"
        * todo
    * and "Dr. Jordan B. Peterson On The Impact Of the Radical Left", https://www.youtube.com/watch?v=dOmJx8mTnm8, 45 min
        * 5 min: too much hierarchy is bad; radical left = all hierarchy is bad; and that's bad

Inbox 2019
----------
### How to spot a liar | Pamela Meyer
* https://www.youtube.com/watch?v=P_6vDLq64gE, 19 min, 2011
    * ...
    * (stellenweise etwas zu einfach)
    * ...
    * Besser hinschauen, besser zuhören => dadurch sich befreien an einer Lüge teilzunehmen
        * den Weg einschlagen, selber ein wenig mehr explizit zu werden, denn dann
            signalisiert man jeden um einen herum: "My world, our world, it's going to be an honest one.
            My world is going to one where truth is strengthened and falsehood is recognized and marginalized."

### Personality
* https://www.ted.com/talks/brian_little_who_are_you_really_the_puzzle_of_personality
    * incl. transcript
    * ...later
