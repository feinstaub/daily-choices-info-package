Sexismus
========

<!-- toc -->

- [Die Göttliche Ordnung - Diskriminierung nach Geschlecht](#die-gottliche-ordnung---diskriminierung-nach-geschlecht)
  * [Frauenrechte](#frauenrechte)
  * [Bibel-Stelle: Eure Weiber lasset schweigen](#bibel-stelle-eure-weiber-lasset-schweigen)
- [Gewalt gegen Frauen](#gewalt-gegen-frauen)
  * [2020](#2020)
  * [2019](#2019)
- [Misogynie](#misogynie)
  * [Frauenmord](#frauenmord)
- [Menschenrechte für die Frau](#menschenrechte-fur-die-frau)
  * [Terre des Femmes](#terre-des-femmes)
- [Systemischer Sexismus](#systemischer-sexismus)
  * [Equal Pay Day](#equal-pay-day)
- [Aktuelle Missstände](#aktuelle-missstande)
  * [Beschneidung / Genital-Verstümmelung](#beschneidung--genital-verstummelung)
- [Inbox](#inbox)
  * [Ist es radikal, Jungen beizubringen, nicht zu vergewaltigen?, 2020](#ist-es-radikal-jungen-beizubringen-nicht-zu-vergewaltigen-2020)
  * [The Alicia Project](#the-alicia-project)

<!-- tocstop -->

Die Göttliche Ordnung - Diskriminierung nach Geschlecht
-------------------------------------------------------
### Frauenrechte
* Film: http://www.goettlicheordnung.de, 2017
    * Trailer (deutsch statt schwiezerdütsch)
        * "Wir kämpfen für die Befreiung der Frauen." - "Ich fühle mich aber nicht unfrei" - "Aha. Wegen Frauen wie Ihnen geht's nicht vorwärts!"
            * an injustice anywhere...
        * "Ich finde auch vieles ungerecht und ich mach deswegen nicht so ein Aufstand"
        * "Die Männer machen die Gesetze. Aber betroffen von diesen Gesetzen sind wir auch."
        * "...weil es eigentlich glasklar ist, wie himmelschreiend ungerecht das ist"
            * an injustice anywhere...
                * (verschändnisvoll: Schlachthäuser sind aber nicht aus Glas)
                * (realistisch: selbst wenn, müssen Menschen auf die Straße, sonst passiert nichts)
    * http://www.goettlicheordnung.de/downloads/Schulmaterial_DIE-GOETTLICHE-ORDNUNG.pdf
        * "der erste Spielfilm über das Schweizer Frauenstimmrecht und dessen späte nationale Einführung 1971."
        * "setzt all den Menschen ein Denkmal, die damals für gleiche politische Rechte gekämpft haben, sowie all jenen, die sich auch heute für Gleichberechtigung und Selbstbestimmung engagieren."
        * S. 4: Wahlplakate gegen und für das Frauenstimmrecht
        * ...
    * http://www.goettlicheordnung.de/#section4-point
        * "1990 ließ der Kanton Appenzell Innerrhoden als letzter seine Einwohnerinnen zur Wahl gehen, nach einem Urteil des Bundesgerichts."

* Abtreibung?
    * https://www.tagesschau.de/ausland/gesetzeslage-abtreibungen-ueberblick-101.html, 2019
        * Gesetzeslage - "So regeln Staaten die Abtreibung"
    * Alice Schwarzer: "Abtreibung ist ein Menschenrecht!"

### Bibel-Stelle: Eure Weiber lasset schweigen
* https://bibeltext.com/1_corinthians/14-34.htm
    * "Eure Weiber lasset schweigen unter der Gemeinde; denn es soll ihnen nicht zugelassen werden, daß sie reden, sondern untertan sein, wie auch das Gesetz sagt."

Gewalt gegen Frauen
-------------------
### 2020
* ...

### 2019
* "Massenprotest gegen Gewalt an Frauen", https://www.tagesschau.de/ausland/demos-frauen-gewalt-101.html, 2019
    * "Allein in diesem Jahr wurden in Frankreich bereits mindestens 116 Frauen von ihrem Partner oder Ex-Partner getötet. Zehntausende Demonstranten gingen heute im ganzen Land für einen besseren Schutz von Frauen auf die Straße."
    * "Gewalt gegen Frauen ist Alltag"

Misogynie
---------
"Misogynie (zu griechisch misos „Hass“, und gyne „Frau“) ist ein abstrakter Oberbegriff für soziokulturelle Einstellungsmuster der geringeren Relevanz bzw. Wertigkeit von Frauen oder der höheren Relevanz bzw. Wertigkeit von Männern. Sie wird sowohl von Männern als auch von Frauen selbst über die psychosoziale Entwicklung verinnerlicht (Sozialisation, Habitualisierung) und stellt die Erzeugungsgrundlage für den hierarchisierenden Geschlechtshabitus von Männlichkeit und Weiblichkeit dar."
(https://de.wikipedia.org/wiki/Misogynie)

### Frauenmord
* https://de.wikipedia.org/wiki/Femizid
* tritt in verschiedenen Kulturkreisen auf
    * z. B. "Femizid in Lateinamerika und der Begriff Feminicidio"
        * https://de.wikipedia.org/wiki/Femizid_in_Lateinamerika
            * "betrifft die besondere Häufung von geschlechtsspezifischen Tötungen von Frauen in den spanisch- und portugiesischsprachigen Ländern Amerikas"
    * z. B. Türkei: https://www.derstandard.de/story/2000119220516/aktivistinnen-kritisieren-frauenfeindliches-klima-in-der-tuerkei
        * http://kadincinayetlerinidurduracagiz.net/uber-uns („Wir werden Frauenmorde stoppen!“)
        * Tradionelles Frauenbild / Rollenbild => Hass gegen unabhängige Frauen => Gewalt (haupts. d. Ehem. u. Partn.)
        * außerdem: "Staatsführung macht Stimmung gegen LGBQTI => (gleiches Prinzip) => Gewalt"

Menschenrechte für die Frau
---------------------------
### Terre des Femmes
* [Terre des Femmes – Menschenrechte für die Frau e. V. ](https://de.wikipedia.org/wiki/Terre_des_Femmes)
    * "sich für ein gleichberechtigtes und selbstbestimmtes Leben von Mädchen und Frauen weltweit einsetzt."
    * u. a. kritische Auseinandersetzung mit Religion, wenn sie Frauenrechte negativ beeinflusst
        * ["Kopftuch bis zur Burka im Islam – nur eine Frage der Deutung?"](https://www.frauenrechte.de/online/gne/1750-kopftuch-bis-zur-burka-im-islam-nur-eine-frage-der-deutung), 2014
            * ...
            * "Mein Fazit ist, dass diese muslimisch-männliche Vorschrift der Reizbedeckung von Frauen nichts mit Religion und nichts mit weiblicher Identität, aber um so mehr mit Sexismus zu tun hat, und daher unserem Grundgesetz widerspricht."
            * ...
        * ["Die Verschleierung von Mädchen aller Altersstufen – ein zunehmendes Phänomen in vielen Schulen und sogar in Kindergärten – steht für eine Diskriminierung und Sexualisierung von Minderjährigen."](https://www.frauenrechte.de/online/themen-und-aktionen/gleichberechtigung-und-integration/kinderkopftuch/3338-terre-des-femmes-unterschriftenaktion-den-kopf-frei-haben), 2018
            * https://www.tagesschau.de/inland/kopftuch-143.html
                * "Ich bin 1963 in Istanbul geboren. Meinen Eltern war es fremd, dass auf den Kopf eines Kindes ein Kopftuch gehört. So bin ich aufgewachsen."
            * todo: Fragen...

Systemischer Sexismus
---------------------
### Equal Pay Day
* todo: Video mit Kindern, warum Frauen weniger verdienen als Männer

Aktuelle Missstände
-------------------
### Beschneidung / Genital-Verstümmelung
* https://de.wikipedia.org/wiki/Weibliche_Genitalverst%C3%BCmmelung
    * "Weibliche Genitalverstümmelung (englisch female genital mutilation, kurz FGM), weibliche Genitalbeschneidung (englisch female genital cutting, kurz FGC)"
    * "Diese Praktiken werden von den Ausübenden überwiegend aus der Tradition heraus begründet."
    * "Es wird geschätzt, dass [...] jährlich etwa drei Millionen Mädchen, meist unter 15 Jahren, eine Genitalverstümmelung erleiden."
    * "Sowohl internationale staatliche Organisationen wie die Vereinten Nationen, UNICEF, UNIFEM und die Weltgesundheitsorganisation (WHO) als auch nichtstaatliche Organisationen wie Amnesty International, Terre des Femmes oder Plan International wenden sich gegen die Genitalbeschneidung und stufen sie als Verletzung des Menschenrechts auf körperliche Unversehrtheit ein, auf die mit dem Internationalen Tag gegen weibliche Genitalverstümmelung, der seit 2003 jährlich am 6. Februar stattfindet, aufmerksam gemacht werden soll."
* Film: Wüstenblume: https://de.wikipedia.org/wiki/W%C3%BCstenblume
* Fadumo Korn
    * https://de.wikipedia.org/wiki/Fadumo_Korn
        * " gebürtige Somalierin, die seit 1979 in Deutschland lebt und durch ihre 2004 erschienene Autobiografie Geboren im großen Regen und ihren Einsatz gegen die Beschneidung weiblicher Genitalien Bekanntheit erlangte"
        * "Im April 2011 bekam sie die Verdienstmedaille der Bundesrepublik Deutschland verliehen."
    * NALA e.V.
        * https://nala-fgm.de/
        * "Bildung statt Beschneidung!"
        * "NALA setzt sich insbesondere gegen weibliche Genitalbeschneidung,
            Female Genital Mutilation (FGM) bzw. Genitalverstümmelung in afrikanischen Ländern,
            aber auch in Deutschland und weltweit ein."
        * "NALA bedeutet in der Sprache der Kisuaheli die Löwin.
            Es steht aber auch für nachhaltig, aktiv, lebensnah und aufklärend."
* Aktueller Stand
    * 2020:
        * 68.000 Frauen und Mädchen in D. sind von FGM betroffen
        * Bundesweite Hilfetelefon "Gewalt gegen Frauen"

Inbox
-----
### Ist es radikal, Jungen beizubringen, nicht zu vergewaltigen?, 2020
* https://sz-magazin.sueddeutsche.de/freie-radikale-die-ideenkolumne/vergewaltigung-aufklaerung-jungen-88318, 2020
    * Kolumne "Freie Radikale" von Teresa Bücker
    * "Wir müssen einräumen, dass auch die eigenen Söhne zu Tätern werden können. Denn die beste Prävention gegen sexualisierte Gewalt ist es, Jungen dabei zu unterstützen sich frei von gefährlichen Männlichkeitsnormen zu entwickeln."
    * "Erziehungsberechtigte müssen damit umgehen, dass nahezu alle männlichen Jugendlichen von Pornos darin beeinflusst werden, wie sie über Geschlechterrollen beim Sex nachdenken und auch über Einvernehmlichkeit."
        * siehe auch
            * https://fightthenewdrug.org/3-reasons-why-watching-porn-is-harmful/, 2019
                * "Let’s Talk About Porn. Is It As Harmless As Society Says It Is?"
                * "It took decades for society to believe the science that proved **smoking cigarettes was harmful**,
                    and we are learning a similar lesson with porn in our world today."
                    * https://fightthenewdrug.org/porn-industry-uses-tobacco-industry-tactics-to-hide-the-truth/
                    * https://fightthenewdrug.org/3-lies-society-tells-about-porn/
                * "Porn can change and rewire a consumer’s brain."
                * "A porn habit can dramatically escalate into unexpected territory."
                * "Porn can become an obsessive compulsion, or even an addiction."
                * "Porn can alter a consumer’s sexual tastes."
                * "Similar to a drug, porn can affect a consumer’s brain."
                * "Porn can damage your sex life and sexual health."
                * "Porn is full of toxic lies."
                    * "Porn also makes it look like no matter what a man does, the woman likes it even though so many of the sex acts shown in porn are degrading, painful or violent."
                * ...
                * "Porn has a dirty little secret: not all explicit content is produced consensually."
                * "Porn is inseparably connected to sex trafficking."
                * "Porn is connected to violence."
                * "Why this matters"
                    * ..., "Get educated and fight against an industry that is tangibly harming individuals, relationships, and society."

            * "Let’s Talk Porn | Maria Ahlin | TEDxGöteborg", 2019
                * https://www.youtube.com/watch?v=DBTb71UzPmY, 18 min
                    * taboo
            * https://www.npr.org/2019/05/25/723192364/what-we-dont-talk-about-when-we-talk-about-porn
            * https://minnesotachildrensalliance.org/training/porn-lets-talk-about-it/
                * "Kids are exposed to pornography at high rates due to free and accessible porn websites. On these websites, children can easily confuse violent, misogynistic, and degrading themes as normal sexual behavior, and most of the content does not convey a culture that requires consent."
    * (todo)

### The Alicia Project
* "Kinderpornografie: Wie eine 13-Jährige ihre Entführung überlebte", 2020
    * https://www.svz.de/deutschland-welt/xl/Alicia-Kozakiewicz-spricht-im-Interview-offen-ueber-Missbrauch-id28520842.html
    * https://www.fr.de/panorama/kinder-machen-fehler-nutzen-taeter-aus-13807223.html
        * "Trotz der Metoo-Bewegung werden Mädchen heutzutage hypersexualisiert. Viele Mädchen denken deshalb, dass es ihre wichtigste Aufgabe sei, sexy zu sein."
* http://www.aliciaproject.org/
    * "Pornhub at New York Fashion Week??", 2020
        * https://www.aliciakozak.com/post/pornhub-at-new-york-fashion-week, 8 min
            * "Pornhub, the incredibly popular, and problematic, adult video website,
                had its adult "stars" walk in New York Fashion Week as a "feminist statement.""
            * "I am NOT kink-shaming and I am not saying that porn is inherently bad.
                It’s the kind of porn that is readily available today that is the issue."
