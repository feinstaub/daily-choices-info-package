Niko Paech
==========

<!-- toc -->

- [2020](#2020)
- [2019](#2019)
- [2015](#2015)
- [2012](#2012)

<!-- tocstop -->

2020
----
* FRAGE: Die These ist, dass jegliches Wachstum in Solar und Wind auch nicht gut ist und der Verbrauch stetig steigt.
    * Aber: der pro-Kopf-Verbrauch sinkt doch (z. B. sparsamere Motoren etc.). Klar, es wird durch Rebound mehr verbraucht.
    * Ist also nicht Wind und Solar das Problem, sondern die Nachfrageseite?
        * bzw. Wind/Solar nicht besser als Kohle?

* Andere Wachstumskritik:
    * Video: "Unboxing Capitalism - Wie wir unter 1,5 Grad bleiben": https://www.youtube.com/watch?v=JSGuy3LA-d0, 35 min, 2020, Now Collective
        * "Gerade für junge Menschen halten wir es für unheimlich wichtig, eine fundierte und strukturierte Kritik am Kapitalismus zu entwickeln,
            welche sich gegen personalisierte Kritik und Verschwörungs[mythen] abgrenzt! Wenn wir dazu etwas beitragen können, ist das super! :)"
        * ...
        * ... todo
        * Markt und Konkurrenz erzwingt Wachstum => stimmt möglicherweise nicht; siehe paech Jung&Naiv 2019
        * ...

* "Niko Paech über Post-Wachstums-Ökonomie, Barbarei & Nachhaltigkeit - Jung & Naiv: Folge 405", 2019, https://www.youtube.com/watch?v=9DKN_GRzLUY, 1 h
    * ...
    * ...
    * ...
    * ca. 15:00 Kapitalismus und Wachstum trennen
        * Unternehmer können sich auch mit Status quo begnügen;
        * auch Konkurrenz => Wachstumszwang stimmt nicht unbedingt; es kommt auf die Charakteristik der Märkte an
        * auch in nicht-kapitalistischen Staaten will man wachsen
        * Grund ist eher der Aufbruch aus dem Mittelalter in die Moderne ist die Wachstumsursache
            * Freiheitsverständnis; frei sein wollen von Schicksalsschlägen (Adel, Wetter, ...); frei sein von Knappheit => Steigerungsdynamiken
    * 20:00 parlamentarische Demokratie: Geschenke verteilen und Wahlen gewinnen oder andersrum
        * Ziel ist schon, soziale Gerechtigkeit zu schaffen (wenn auch die Unterschiede zunehmen)
        * Menschen sozial emanzipieren, durch mehr Geld, mehr Infrastruktur, mehr Technik
    * ca. 21:00 Wachstum = Res.verbrauch
        * z. B. Fläche
        * Wohnraumbedarfssteigerung
            * https://www.umweltbundesamt.de/daten/private-haushalte-konsum/wohnen/wohnflaeche#zahl-der-wohnungen-gestiegen, 2019
                * Zahl der Eigenheime gestiegen
                * Single-Haushalte
    * 24:00 Bodenmoratorium (hatten die Grünen mal im Programm in den 80ern)
        * Effektivste Wachstumsbremse = keine neuen Flächen mehr versiegeln dürfen
        * Neues Zeitalter wird anbrechen wo wir versiegelte Fläche wieder reaktivieren, renaturieren
    * 25:00 Teile von Flughäfen und Autobahnen abbauen
        * Mobilität (Warenverkehr, motorisierter Individualverkehr, Flugreisen) und Digitalisierung sind Sargnägel der Zivilisation
            * direkte ökol. Gründe und: Einschränkung von Kommunikation und Transport => Wachstum wird gebremst
    * ca. 28:00 woher kommt das Recht physikalische Leistungen in Anspruch zu nehmen, die in keinem Verhältnis zur eigenen Arbeit steht
        * ...
        * Barbarei?
        * ...
        * Wo setzen wir die Grenze, was ein Mensch mit Recht beanpruchen kann? Diese Debatte muss geführt werden.
            * wenn wir wissen, was die Konsequenzen sind bzw. wenn alle das tun
            * Alternative: nur eine Elite darf das und hält die anderen (per Zwang) davon ab, das auch zu tun (weil sonst der Planet unbewohnbar wird)
            * Nichts verbieten, aber Fragen stellen.
    * ...
    * ... TODO
    * ...
    * "Nicht derjenige ist am glücklichsten/souveränsten?, der am meisten hat, sondern derjenige, der am wenigsten braucht."
    * ...
    * ...

* https://www.asta-giessen.de/vortrag-von-prof-dr-niko-paech-rebellieren-fuer-eine-wirtschaft-ohne-wachstum-warum-und-wie/, 2020, 45 min
    * "Vortrag von Prof. Dr. Niko Paech: Rebellieren für eine Wirtschaft ohne Wachstum: Warum und Wie?"
    * ...
    * ...
    * ...

* "Kann man durch Konsumverzicht das Klima retten?", 2019, 4 min, https://www.youtube.com/watch?v=X0KFQnIcX94
    * Keine Urlaubsreisen mit dem Flugzeug mehr.
    * Flugzeug = die größte Umweltzerstörung, die man heute als einzelner legal ausführen kann
    * oft purer Luxus
    * ...
    * ...

2019
----
* Impuls-Video: arte, Schluss mit Wachstum: https://www.arte.tv/de/videos/086089-015-A/schluss-mit-wachstum/, 2019, Niko Paech, 4 min
    * Postwachstumsökonomie
    * Vorleben: demokratisch, friedlich, freiheitlich, freiwillig, humorvoll
    * Verantwortung übernehmen; nicht nur an Technik, Wissenschaft oder Staat zu delegieren / ::techniküberschätzung
    * ...
    * Leben entrümpeln vom Wohlstandsmüll
    * ...
    * mit 20 statt 40 Wochenstunden sind in der Nachbarschaft umschauen, um Gemeinschaften aufzubauen, die nicht nur zum Spaß da sind, sondern um unsere Bedürfnisse zu befriedigen
    * ...
    * prägnante Zusammenfassung
    * siehe auch
        * "Upcycling vs. Konsumwahn": https://www.arte.tv/de/videos/086089-013-A/upcycling-vs-konsumwahn/, 2019

* Video: https://www.youtube.com/watch?v=0xR2JeOpzug, "Prof. Dr. Niko Paech: Wege zur Postwachstumsökonomie", Dez 2018 Hochschule Augsburg
    * ...
    * ...
    * TODO: ...
    * ...
    * ...
    * 25:00 Warum grünes Wachstum nicht geht
        * Objektorientierung: nur Objekte werden als effizient, nachhaltig, umweltfreundlich bezeichnet
            * vs. Menschen
        * ...
        * Es gibt keine "nachhaltigen" Einzelhandlungen / Produkte / Projekte / Technologien.
            * Es gibt nur nachhaltige Lebensstile.
        * "Seit ich nur noch Bio-Fleisch esse, habe ich absolut kein schlechtes Gewissen mehr und auch die Arbeit macht mir Spaß"
        * Ablasshandel von damals wird heute wieder (immer noch) kultiviert
            * "Isoliert betrachtete Handlungen können kontraproduktiv sein: Je mehr Kompensationsmasse, desto mehr ruinöse Praktiken lassen sich damit symoblisch kompensieren, also legitimieren"
                * (todo: Beispiel auch z. B. Altpapier statt Papier aus Frischfasern?)
        * "Nicht Symbole, sondern Summe aller Handlungen zählt"
            * https://www.co2online.de
            * https://uba.co2-rechner.de/de_DE/ - Umweltbundesamt
                * Deutscher Durchschnitt: 11 t / Jahr
                * ich (Treiber: Heizung): 7 t / Jahr (Ziel: 2,5 t)
        * "Produkte führen ein Doppelleben"
            * Beseitigung von Knappheiten
            * Botschaft übermitteln (ich bin ein Weltretter; Beispiel HessNatur-T-Shirt)
    * 29:45: 2,5 t CO2 / Jahr wäre derzeit globalisierungstauglich und gerecht: 80 Jahre alt => 200 t insgesamt
        * Mobilität haut stark rein, insb. Flugreisen
        * 1x FRA/NewYork und zurück als Flugreise: ca. 4 t CO2
    * 31:00: Intellektuelle, Bildung: "Umweltbewusstsein - so haben wir lange geglaubt - sei ein Garant für umweltfreundliches Handeln"
        * UBA-Studie: das Umweltbewusstsein ist so hoch wie nie
        * man empört sich auch immer mehr über ausbleibende Klimapolitik
        * dadurch ist 1980 eine Partei entstanden, "die diesem Umweltbewusstsein Ausdruck verleiht: die nannte man damals die Grünen und frecherweise nennt man sie heute immer noch so"
        * 32:00: Forschergruppe Wahlen:
            * bestätigt das intellektuelle Problem (kein Grünen-Bashing; nur ein Befund)
                * Grünenwähler fliegen am meisten, während einige Wähler anderer Parteien noch nie in einem Flugzeug saßen"
                * Hier ist das Ablassdenken am Werk.
                    * siehe Mittelalter
    * "Anthropologische, soziale und ökologische Logiken des Ablasses"
        * ...
        * moralischen Überschuss erwirken / Guthaben sammeln, um sich Sünden zu leisten
        * ...
    * 35:35: Geschichte vom Jungen, der das Stehlen von Omas Apfelkuchenstück gebeichtet hatte
        * 20 Vater Unser
        * 40 Vater Unser => noch ein Stück
        * Vater Unser = Passivhaus, Atmosfair, Einkauf im Ökoladen
        * Die Kirche damals hat das eingenommene Geld - im Ggs. zu den Adligen - (auch) für gute Dinge verwendet: Schulen, Bildung, Straßen etc.
            * Kompensationsleistung erbringen, um Schuld/Schade zu tilgen: gibt es schon länger
                * Ablass-Ökonomik
                * => "Kulturtechnik, die sozialen Frieden stiftet"
                * Streicheleinheiten, oder auch Reparationszahlungen eines Staates
                * "soziale Stabilität und Gerechtigkeit sind darauf angewiesen"
    * 38:00: Dilemma: wir leben in einer Wissensgemeinschaft
        * "Der gebildete Mensch ist der Produktionsfaktor."
        * Der gebildete Mensch hat durchschnittlich das höchste Einkommen und auch das höchste Umweltbewusstsein.
            * Das Einkommen wird u. a. durch kosmopolitische Lebensstile verdient.
                * TODO: Menschen FRAGEN: ist das so? Könntest du weiterhin dein Einkommen erzielen, ohne Flugreisen, SUV und ständig neue Elektronik?
            * "Der Flughafen ist der Ersatz für den Hochofen, die Lidl-Kasse und das Fließband geworden"
        * Dilemma für den gebildeten Menschen: "Er - mit seinem Umweltbewusstsein - weiß wie sonst niemand - was er oder sie tut"
            * "wenn man SUV fährt, 160 qm Wohnfläche hat, seinen Kindern jedes Quartal ein neues Smartphone kauft, und um den Planeten jettet"
            * (=> ganz wichtig: Empathie/Verständnis anwenden, siehe Eisenstein, blame&shame vermeiden)
        * => "Kognitive Dissonanz infolge einer invasiven Lebensführung" / schlechtes Gewissen
        * => "Bedarf an symbolischen Kompensationshandlungen" / SUV-Fahrer: grüner Garten, handwerklich gestaltet?
        * => "Additiver Konsum an "grünen" Gütern als effiziente Therapie"
        * => "Entstehung einer "Green (Washing) Industry"
        * "Nachhaltigkeitsschickaria als Verkörperung der extremen Kluft zwischen theoretischer und empirischer Nachhaltigkeit"
    * 40:10: "Je mehr grüne Ausgleichssymbole (z. B. Bioprodukte)
            oder kompensatorische Einzelhandlungen (z. B. punktuellen Engagement im Gemeinschaftsgarten)
            vorhanden sind, desto mehr verantwortungslose Praktiken lassen sich damit ausgleichen"
        * und das ohne kogn. Dissonanz => "Strukturkonservatismus und Lernresistenzen"
        * Grünes Wachstum (Wachstum im Bereich der "nachhaltig"eren Produkte) kann also sogar den gegenteiligen Effekt haben
            * relevant ist nur die gesamte Lebensführung, die unter 2,5 t CO2 / Jahr bleibt
                * (vs. jedes kleine bisschen ist schon was)
            * "wachstumskritische Konzeption" notwendig
        * (Man muss wirklich aufpassen, dass man darauf nicht reinfällt. Das geht vielen (gebildeten) Menschen so.)
    * 42:00
        * TODO
        * ... Nachfrageseite anpassen (Suffizienz)
        * ... Angebotsseite anpassen (Subsistenz, entkommerzialisieren, kürzere Produktionsketten...)
            * (derzeit geht es hin zu immer längeren Ketten)
            * weniger kaptitalintensiv, dafür mehr arbeitsintensiv
            * Industrie zurückbauen (NICHT abschaffen, sondern schrumpfen)
        * "20 h "normale" Erwerbstätigkeit"
        * "20 h "marktfreie" Versorgungszeit"
        * = die "kleine Vollbeschäftigung" (Wuppertal-Institut)
        * 44:00
            * Es geht nicht um den Sudan oder Equador, sondern um Europa: vor der eigenen Haustür kehren
            * Wer sagt, dass Vollbeschäftigung eine 40-h-Woche sein muss?
                * Das steht nicht in der Bibel oder sonst wo
        * 45:30: Vorschlag einer Nebenökonomie
            * Unternehmensnetzwerk für IT (Update Software, Instandhaltung Geräte etc.)
                => gleicher digitaler Komfort und Innovation mit der Hälfte der Produktionsaufwände, weil die Geräte doppelt so lange genutzt werden können
            * Netzwerk für Möbel
            * Netzwerk für Kleidung etc.
        * 46:30: Die marktfreie Zeit nutzen, um Dinge selber zu machen
            * Bildungssystem sollte nicht nur "Konsumdeppen" erzeugen, sondern Menschen,
                die in der Lage sind eine Schraube in die Wand zu drehen, ein Brot zu backen oder die Fahrradkette zu ölen
            * Nutzungsdauer verlängern durch Reparaturen in allen Nichen der Gesellschaft
                * nicht nur Repaircafes
                * sondern auch Schulen, Hochschulen, Volkshochschulen, Kindergärten, Kirchen
            * Teilen
                * z. B. (eine gute) Waschmaschine mit 5 Menschen
                    * => viel weniger Industrieproduktion nötig
                * => Man kommt nicht nur mit weniger Verbrauch, sondern auch mit **weniger Geld** aus!
            * Kommunikation: Denn wie kommuniziert man die Transformation den Menschen...
                * das ist eine **soziale Frage**!
            * ... siehe Folie

* Artikel: „Wir brauchen einen Aufstand der Handelnden“ - https://www.deutschlandfunk.de/oekonom-zu-klimaschutz-wir-brauchen-einen-aufstand-der.694.de.html?dram:article_id=454447, 2019
    * "Eine wirksame CO2-Steuer müsste nach Ansicht des Umweltökonomen Niko Paech den Menschen Urlaubsflüge, Fleischkonsum, Autofahren und übermäßigen Konsum madig machen. Dafür gebe es keine Mehrheit, sagte er im Dlf. Die Befürworter könnten aber einfach damit anfangen, Handlungsmuster zu verändern."
    * ...
    * ... TODO
    * "Abschied von der 40-Stunden-Woche"
        * "Also die Wirtschaft muss dann nicht kollabieren, wenn wir langsam, aber sicher den Rückbau der Industrie gestalten können. Ich spreche von einer sogenannten Postwachstumsökonomie, und die würde damit ihren Anfang nehmen können, dass wir uns verabschieden vom 40-Stunden-Arbeitswochenmodell."
            * "Würden wir die wöchentliche Arbeitszeit und damit auch das Einkommen – das muss ich ganz klar sagen, alles andere wäre Scharlatanerie", ...
            * "dann könnten wir langsam aber sicher die Mobilitäts- und vor allem die Konsum- und Wohnraum- und Digitalisierungsnachfrage senken und damit eine wirklich prägnanten Klimaschutzeffekt erzielen"
            * "und könnten dies unter Einhaltung von Zielen der globalen Gerechtigkeit auch hinbekommen"
                * "dass wir keine Arbeitslosigkeit haben, sondern dass wir einfach sozusagen ein geringeres Arbeitsniveau haben, auf dem aber dann sozusagen alle Menschen auch ein Auskommen haben."
        * "Nehmen wir mal an, wir hätten eine 30- oder 20-Stunden-Woche und das würde funktionieren, wir würden dann die Wirtschaft dergestalt umbauen, dass die freigestellte Zeit, über die die Menschen verfügen, [...] benutzt,
            um ergänzend zu einem nicht mehr so hohen Geldeinkommen eigene Leistungen zu erbringen,
            zum Beispiel im Nahrungsmittelanbau, in der Reparatur der Güter
            und drittens, ganz wichtig, in der gemeinschaftlichen Nutzung.
            Wenn wir also Autos gemeinschaftlich nutzen, Rasenmäher, Werkzeuge und so weiter und andere Dinge,
            dann reduzieren wir den Bedarf an industrieller Produktion,
            auch an Transporten und so weiter, dann hätten wir wirklich einen prägnanten Effekt."

* https://www.nzz.ch/international/nico-paech-dann-geht-in-gottes-namen-unter-ld.1518768, 2019
    * "«Dann geht in Gottes Namen unter»: Der Ökonom Niko Paech hat radikale Ansichten darüber, wie die Welt zu retten ist
        – aber er will niemanden zu seinem Glück zwingen"
    * "In einer Welt, wie sie sich Niko Paech vorstellt, würden alle nur noch zwanzig Stunden pro Woche arbeiten
    und 75 Prozent der Flughäfen wären geschlossen.
    Die Gesellschaft würde sich auf Kontingente verständigen: Diplomaten, Journalisten, Merkel und der Papst würden weiterfliegen.
    Die meisten anderen Bürger eher nicht, oder nur noch selten.
    50 Prozent der Autobahnen würden ebenfalls geschlossen,
    denn sie sind laut Paech «Lebensadern der Zerstörung»."
    * ...
    * ... todo
    * ...

* "Ökonom zu Klimaschutz - „Wir brauchen einen Aufstand der Handelnden“"
    * https://www.deutschlandfunk.de/oekonom-zu-klimaschutz-wir-brauchen-einen-aufstand-der.694.de.html?dram:article_id=454447, 2019
    * ...

* "Ökonom Niko Paech zum Klimaschutz - „Wir müssen unseren Lebensstil ändern“"
    * https://www.deutschlandfunkkultur.de/oekonom-niko-paech-zum-klimaschutz-wir-muessen-unseren.1008.de.html?dram:article_id=459353, 2019
    * "Wir müssten unseren Lebensstil dergestalt ändern, dass wir eine Wirtschaft ohne Wachstum meistern können. Es gibt zu einem kleinen Anteil bereits Menschen, die in modernen Industriegesellschaften diesen Lebensstil praktizieren."
    * "Einen nachhaltigeren Lebensstil zu verbreiten, in den Bildungseinrichtungen, in Gesellschaft und Politik, sei eine zentrale Aufgabe."
    * "Die Unterscheidung zwischen Grundbedürfnissen und dekadentem Luxus ist wichtig."
        * "So sei beispielsweise Urlaub „etwas Wichtiges.“ Aber er sei „nicht daran gekoppelt, dass man allzu viel CO2-Emissionen damit verursacht“."

2015
----
* https://www.einfachbewusst.de/2015/02/interview-niko-paech/
    * "„So sparen wir Zeit, Geld, Raum und ökologische Ressourcen.“"
    * Lesenswertes Interview
    * Buchempfehlungen:

    Niko Paech: Da gibts viele … hier nur einige, die mir spontan einfallen.
    „Plenitute“ von Juliet Schor
    „Vorwärts zur Mäßigung“ von Hans-Christoph Binswanger
    „Die Macht der Bedürfnisse“ von Marianne Gronemeyer
    „Wohlstand ohne Wachstum“ von Tim Jackson
    „Wirtschaft jenseits von Wachstum“ von Herman Daly
    „Selbstbegrenzung“ von Ivan Illich
    „Small is beautiful“ von Ernst Friedrich Schumacher
    „Es reicht! Abrechnung mit dem Wachstumswahn“ von Serge Latouche
    „Selbst denken: Eine Anleitung zum Widerstand“ von Harald Welzer
    „Urban Gardening: Über die Rückkehr der Gärten in die Stadt“ von Christa Müller
    „The transition handbook“ von Rob Hobkins

* Video: "Niko Paech, Umweltökonom" - https://www.youtube.com/watch?v=-NXy7bqSzVU, BR, ARD, 2015, 45 min
    * ...
    * "Wir haben ein ökologisches und ein soziales Problem."
        * "Denn wenn klar ist, dass die Begrenztheit des Planeten es nicht erlaubt, jeden beliebigen Lebensstil zu praktizieren, dann haben wir ein Verteilungsproblem"
    * ...
    * Probleme mit industrieller Landwirtschaft: Energie, Dünger, Böden auslaugen, Umwelt vergiften => Ende dieses Systems, die Menschen zu ernähren, ist absehbar
        * zu lange Wege, Zentralisierung, wenig Autonomie, einige wenige, nicht krisenfest
    * ...
    * Frage: "Wie kommt man denn nun da hin?"
        * ...
    * ...
    * ... todo
    * ...

* https://www.einfachbewusst.de/neu-hier/
    * "Hier dreht sich alles um Minimalismus, Nachhaltigkeit und vegane Ernährung im Alltag und beim Wandern."
    * https://www.einfachbewusst.de/2015/01/einfaches-leben/
        * "8 Probleme, die ein einfaches Leben lösen kann"
            * Geldsorgen, Hohe Arbeitsbelastung, Zeitmangel, Platzmangel, Umweltprobleme,
                Schnelllebigkeit und Reizüberflutung, Fremdbestimmung, Unzufriedenheit
            * ...
    * https://www.einfachbewusst.de/2014/07/weitere-tipps-minimalistischer-leben/
        * "25 weitere Tipps, wie Du minimalistischer leben kannst"
        * "1. Jage nicht dem Perfekten hinterher, sonder streben nach dem für Dich Optimalen."
        * "11. Lebe einen Tag lang ohne Energie und Elektrizität."
        * "12. Bitte Deine Geburtstagsgäste keine Geschenke mitzubringen."
        * "17. Konzentriere Dich auf nur eine Sache."
        * "19. Was hat Dich in letzter Zeit existenziell glücklich gemacht?"
        * TODO

2012
----
* https://www.tagesspiegel.de/politik/wachstumskritiker-niko-paech-warum-die-menschen-experimentieren-muessen/7431092-4.html
    * "Sehe ich aus wie ein Hippie?"
