Gegen Kinderarbeit
==================

<!-- toc -->

- [Gegen Kinderarbeit](#gegen-kinderarbeit)
- [Gegen Kindesmissbrauch](#gegen-kindesmissbrauch)
- [Politik](#politik)
  * [Gerd Müller / Entwicklungsminister](#gerd-muller--entwicklungsminister)

<!-- tocstop -->

Gegen Kinderarbeit
------------------
* Siegel und Zertifikate: https://www.aktiv-gegen-kinderarbeit.de/gegenmassnahmen/siegel-und-zertifikate/
* Firmenliste (Firmen und Marken auf Kinderarbeit überprüft): https://www.aktiv-gegen-kinderarbeit.de/firmen/firmenliste
* Übersicht: https://www.aktiv-gegen-kinderarbeit.de/kampagne/
* Was kann man dagegen tun?
    * neben politischer Aktivität, siehe z. B. Kakao
* Sachbuch: ["Benjamin Pütter: Kleine Hände – großer Profit Kinderarbeit – Welches ungeahnte Leid sich in unserer Warenwelt verbirgt"](https://www.randomhouse.de/Paperback/Kleine-Haende-grosser-Profit/Benjamin-Puetter/Heyne/e518817.rhd), 2017
    * [Leseprobe](https://www.randomhouse.de/leseprobe/Kleine-Haende-grosser-Profit-Kinderarbeit-Welches-ungeahnte-Leid-sich-in-unserer-Warenwelt-verbirgt/leseprobe_9783453604407.pdf)
    * Autor bereist seit 37 Jahren Länder, in denen Kinderarbeit besonders verbreitet ist
    * "»Wahrheit sagt uns, wie die Welt beschaffen ist, Moral, wie sie sein sollte.« Susan Neiman"
    * "eine Welt, in der der Profit Einzelner über das Wohl von mehr als 170 Millionen arbeitenden Kindern gestellt wird."
    * "wie die Welt sein sollte, wie jeder Einzelne sie verändern und dazu beitragen kann, dass eine Welt ohne Kinderarbeit Wirklichkeit wird"

Gegen Kindesmissbrauch
----------------------
* ["*Gewalt gegen Kinder gestiegen - Alltag, in allen sozialen Schichten"](http://www.tagesschau.de/inland/kinder-gewaltopfer-elysium-101.html), 2017, tagesschau
    * "Gewalt gegen Kinder ist Alltag in Deutschland, berichten die Kinderschützer. Auch wenn die Zahl der angezeigten Fälle in der Kriminalstatistik stabil geblieben ist. 12.000 bis 14.000 Opfer würden pro Jahr erfasst."
    * "Das Dunkelfeld liege bei einer Million, sagt Kathinka Beckmann, Kinderschutzexperten an der Uni Koblenz."
    * "Taten, die in allen sozialen Schichten begangen werden, berichtet Beckmann. Sie blieben oft unentdeckt, weil Ansprechpartner fehlen und die Jugendämter schlecht ausgestattet sind."

Politik
-------
### Gerd Müller / Entwicklungsminister
* 2020
    * Buch: Umdenken - Überlebensfragen der Menschheit
        * https://shop.murmann-verlag.de/item/umdenken-gerd-mller
    * "Krisengewinner stärker besteuern"
        * "Entwicklungsminister Müller will Steuervermeider und "Krisengewinner" wie Amazon stärker in die Pflicht nehmen.
        Von der deutschen EU-Ratspräsidentschaft erwartet er entsprechende Schritte"
        * https://www.tagesschau.de/inland/mueller-amazon-101.html
        * "Denn es gibt Krisengewinner unglaublichen Ausmaßes."
        * "Hungerkrise in Folge der Pandemie"
            * "Die Europäische Union habe noch keinen einzigen zusätzlichen Euro für die Bekämpfung der Pandemie und der Hungerkrise zur Verfügung gestellt,"
        * "Es sei immer wieder fatal, beklagt Müller, dass erst die Bilder durch die Medien gehen müssten, bevor eine "Bettelkonferenz" durchgeführt werde und dann dauere es ein weiteres halbes Jahr, bis Soforthilfe geleistet werde."
        * "Finanztransaktionssteuer gegen Not und Hunger"
* https://www.diplomatisches-magazin.de/artikel/entwicklungsminister-dr-gerd-mueller-der-klimaschutz-ist-die-ueberlebensfrage-der-menschheit/
    * Klimaschutz wichtig
    * Aber auch Wachstumsmärkte in Afrika
* 2019
    * https://www.handelsblatt.com/politik/deutschland/entwicklungsminister-im-interview-gerd-mueller-ausbeuterische-kinderarbeit-muss-ausgeschlossen-sein/24088056.html?ticket=ST-3395295-dXUafb7ycWjUPMLx0hgH-ap3
        * "„Ausbeuterische Kinderarbeit muss ausgeschlossen sein“"
        * "Entwicklungsminister Gerd Müller sieht auf Reisen in Asien und Afrika überall durch Färbemittel verschmutzte Flüsse – und Kinder, die auf Plantagen arbeiten müssen. Für durchschlagende Verbesserungen sei die freiwillige Selbstverpflichtung deutscher Unternehmen nicht wirksam genug."
        * "Die Verbraucher in Deutschland akzeptieren es längst nicht mehr, wenn am Anfang der Lieferkette Kinder für uns arbeiten müssen und Hungerlöhne gezahlt werden."
            * "Offenbar sehen sich aber viele Firmen überfordert, in ihren weit verzweigten Lieferketten die Einhaltung der Standards zu kontrollieren."
        * ...
        * "Ein Kilo Kaffee kostet zehn bis zwölf Euro in Deutschland. Nur 50 Cent kommen davon bei den Bauern an. Davon können die Familien auf den Plantagen doch nicht leben."
    * https://www.zeit.de/politik/deutschland/2019-12/kinderarbeit-entwicklungsminister-gerd-mueller-lieferkettengesetz-csu
        * "plant im Kampf gegen Kinderarbeit ein Lieferkettengesetz für den Fall, dass große Unternehmen die Möglichkeit zur freiwilligen Selbstverpflichtung nicht ausreichend wahrnehmen."
        * "Die Rechte der Kinder nach der UN-Kinderrechtskonvention seien längst nicht Wirklichkeit. "Nicht in indischen Steinbrüchen, nicht in den Textilfabriken Asiens und auch nicht in den Kobaltminen im Kongo", sagte Müller. "Wir dürfen hier nicht länger wegsehen.""
        * "Für eine gesetzliche Regelung, die Unternehmen bei Schäden an Mensch und Umwelt haftbar machen könnte, hatte sich zuletzt die Initiative Lieferkettengesetz ausgesprochen."
