Die große Transformation
========================

<!-- toc -->

- [Inbox 2020](#inbox-2020)
  * [Luisa Neubauer](#luisa-neubauer)
  * [Schneidewind](#schneidewind)
  * [Personal Change](#personal-change)
  * [Greta Thunberg](#greta-thunberg)
  * [Selbstverbrennung (Buch)](#selbstverbrennung-buch)
- [Welt im Wandel – Gesellschaftsvertrag für eine Große Transformation](#welt-im-wandel-%E2%80%93-gesellschaftsvertrag-fur-eine-grosse-transformation)
- [Weiteres](#weiteres)
  * [Great Transition](#great-transition)
  * [Starke Reden - XR](#starke-reden---xr)
  * [XR](#xr)

<!-- tocstop -->

Inbox 2020
----------
### Luisa Neubauer
* "Luisa Neubauer kritisiert Friedrich Merz | Markus Lanz am 30.06.2020", https://www.youtube.com/watch?v=zA2j5_3NV4s, 30 min
    * an die Unternehmensvertreterin: welche Forderungen stellen Sie an die Politik, damit wir
        innerhalb der planetaren Grenzen wirtschaften zu können?
        (nicht nur wir sparen hier mal was ein und verwenden LEDs)
* YT: "Interview Luisa Neubauer, 20.07.2020 (dt.)", 45 min,
    * Oft wird versucht die Naturwissenschaft und die Ökonomie gegeneinander auszuspielen.
        * (Du sagst also, die Naturwissenschaft liegt falsch? Nein? Könnte es sein, dass sich die ökonomischen Theorien an die Realität anpassen müssen?)
    * 8:45: Dem Verbraucher kann man nicht alle Verantwortung geben in einer Zeit, wo überall irgendwo eine Menschenrechtsverletzung drinsteckt.
    * ...
* Buch_: Luisa Neubauer und Alexander Repenning: Vom Ende der Klimakrise – Eine Geschichte unserer Zukunft, Tropen Verlag, Stuttgart 2019
* YT: "Post für Luisa Neubauer – Sarah Bosetti | WDR", Comedy und Satire
    * "Männer wie Friedrich Merz fühlen sich sichtlich gestört, wenn Frauen es wagen zu sprechen. Sarah Bosetti hat deshalb eine Bitte an die Klimaaktivistin Luisa Neubauer, die zusammen mit Merz bei Markus Lanz saß: nimm ein bisschen Rücksicht. "Die meisten Herren fühlten sich sehr dadurch in ihren erotischen Fantasien gestört, dass Du gesprochen hast.""
* YT: "11. ZEIT Wirtschaftsforum 2019 – Umweltaktivistin Luisa Neubauer im Gespräch", 30 min
    * zu Greta auf der Klimakonferenz: "die **Klarheit**, in der sie da Menschen anspricht und Situationen adressiert,
        einen **Mut voraussetzt**, den ich ganz lange vermisst hatte [...] das war inspirierend"
    * wie es zum Streik kam auf der Konferenz und später in Berlin (Antwort auf Einladung an Mitstreiker: ne du ist kurz vor Weihnachten keine Zeit)
        * ein Streik ist ein Nichts-Tun
    * ...

### Schneidewind
* Buch: Schneidewind - todo - Einführung in die Kunst des gesellschaftlichen Wandels

### Personal Change
* yt comment: "It's always **easiest to adopt the opinion** that requires no personal change or sacrifice."
    * https://www.youtube.com/watch?v=0Zo2DWW_fhc, hallam, 2020, 19 min, World Web Forum
        * https://worldwebforum.com/about/
            * ... radical change ...
        * Cambridge Analytica, 2020
            * https://worldwebforum.com/project/we-also-had-talks-with-swiss-companies/
            * https://www.heise.de/newsticker/meldung/Cambridge-Analytica-Eine-gefaehrliche-im-Dunkeln-arbeitende-Industrie-4641565.html
    * ...
    * confessions
    * "values don't mean shit" without courage to put them into action
    * ...
    * the outliers...
    * ...
    * do something out of the box
    * ...
    * courage is not about winning success; it is about failure and humiliation
    * ...
    * we do it again: willingly let our government take advantage of African and other 3rd world people so that
        we can have our yearly flights and other cheap stuff
    * ...
    * hunger strike etc.; maybe not successful, but it might give you self-respect
        (when you are on your death bed and then yes I did what I knew I could have done)

### Greta Thunberg
* "Greta Thunberg | Averting a Climate Apocalypse | Davos 2020 | Extinction Rebellion" - https://www.youtube.com/watch?v=OGokqK7iBd4, 8 min
    * ...
    * politics, banks and businesses
    * This has to be done _now_: stop all investing in extraction and ... of fossil fuels
        * (BlackRock letter: words, words, words, but they continue)
        * this might sound like a heavy demand and naive but it is the required minimum

### Selbstverbrennung (Buch)
Hyperenergiegesellschaft oder Nachhaltigkeitskultur?

Welt im Wandel – Gesellschaftsvertrag für eine Große Transformation
-------------------------------------------------------------------
* Gutachten 2011: https://de.wikipedia.org/wiki/Welt_im_Wandel_%E2%80%93_Gesellschaftsvertrag_f%C3%BCr_eine_Gro%C3%9Fe_Transformation
    * WBGU
    * "Der historische Normalfall sei bisher gewesen, eine Richtungsänderung erst als Reaktion auf Krisen und Katastrophen vorzunehmen.
        Dies gelte es zu vermeiden, und stattdessen „einen umfassenden Umbau aus Einsicht, Umsicht und Voraussicht“ anzutreiben."
    * "Der Gesellschaftsvertrag kombiniere eine Kultur der Achtsamkeit (aus ökologischer Verantwortung) mit einer Kultur der Teilhabe
        (als demokratische Verantwortung) sowie einer Kultur der Verpflichtung gegenüber zukünftigen Generationen (Zukunftsverantwortung)"
    * ...
    * ...

Comic dazu: http://www.die-grosse-transformation.de

PDF: https://www.wbgu.de/fileadmin/user_upload/wbgu/publikationen/hauptgutachten/hg2011/pdf/wbgu_jg2011.pdf, 448 Seiten

Zitate:

* S. 3: "Nach Einschätzung des Beirats ist anspruchsvoller glo-baler Klimaschutz auch ohne Kernenergie möglich"
    * "wenn zugleich die gewaltigen Potenziale zur Effi-zienzsteigerung  ausgeschöpft  werden  und  die  **nicht nachhaltigen Lebensstile**,  insbesondere  in  den  Indus-trie- und Schwellenländern, **gesellschaftlich problematisiert** werden."
        * TODO: welche Lebensstile?
* S. 7: Globale Kooperation, Gestaltender Staat, Pioniere des Wandels, Graphik
    * Heute = 2011
    * 10 J. = 2020
    * 30 J. = 2040 (bis dahin sollte die Dekarbonisierung abgeschlossen sein: wir haben noch 20 J.)
    * Achtung Rebound
* S. 10: "Konkrete Handlungsoptionen: Zehn  transformative Maßnahmenbündel"
    * S. 14ff: "Bündel 7: Klimaverträgliche Landnutzung voranbringen"
        * "Stopp der Entwaldung und Übergang zur nachhaltigen Waldwirtschaft"
        * "Klimaverträgliche  Landwirtschaft  fördern"
        * "Klimaverträgliche Ernährungsweisen fördern"
            * "Als rasch umzusetzende Maßnahmen empfiehlt der WBGU **vermehrte Aufklärungsarbeit**
            * in Kombination mit der **Kennzeichnung von Umweltwirkungen auf der Verpackung**.
            * **Kantinen der öffentlichen Hand sollten als Vorbild ein bis zwei fleischfreie Tage pro Woche einlegen.**
            * Die  EU-**Subventionen zur Stützung der Tierproduktion sollten rasch abgebaut werden.**"
* ...

https://www.dnr.de/sozial-oekologische-transformation/info-quellen/transformationsdebatte/wgbu/?L=892

* "Die Gruppe der G20 müsse somit:"
    * "CO2-Emissionen bis 2050 auf null abzusenken"
    * "Finanzierungsmodelle"...
    * "Klimaschutz- und Nachhaltigkeitspolitik **als globales Gerechtigkeitsprojekt verstehen.**"
    * "Nationalismus und autoritäre Bewegungen zurückdrängen. "

Weiteres
--------
### Great Transition
https://de.wikipedia.org/wiki/Great_Transition, 2002

### Starke Reden - XR
* ...

### XR
siehe xr-extinction-rebellion.md
