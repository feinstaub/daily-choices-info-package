Medien
======

<!-- toc -->

- [Inbox](#inbox)
  * [2021: George Monbiot about Murdoch, Double Down News](#2021-george-monbiot-about-murdoch-double-down-news)
  * [2020: Rezo über die Presse](#2020-rezo-uber-die-presse)
  * [2020: Murdoch, Macht](#2020-murdoch-macht)
  * [2018: Medienkonsum](#2018-medienkonsum)
- [BILDblog](#bildblog)
- [Rolle in der Demokratie](#rolle-in-der-demokratie)
  * [Machtmissbrauch](#machtmissbrauch)

<!-- tocstop -->

Inbox
-----
### 2021: George Monbiot about Murdoch, Double Down News
* YT: "How Britain Could Become a Failed State | George Monbiot", 2021, 10 min, DDN Double Down News
    * plutocracy
    * big money controlling media

### 2020: Rezo über die Presse
* https://netzpolitik.org/2020/die-zerstoerung-der-presse/

### 2020: Murdoch, Macht
* https://www.tagesschau.de/ausland/australien-murdoch-klima-101.html
    * "Auch im Zeichen der Buschbrände können sich Premier Morrison und die Murdoch-Medien aufeinander verlassen.
    Sie förderten Australiens Kohle-Industrie und spielten den Klimawandel herunter. Doch dagegen regt sich nun Widerstand."

### 2018: Medienkonsum
IST-Situation:

* 2018: Die Deutschen konsumieren durchschnittlich 9 Stunden Fernsehen/Radio etc. Der höchste Wert bisher.
    * ...todo... quelle


BILDblog
--------
* https://bildblog.de/
    * Eigenangabe:
        * "unabhängiges, journalistisches Internetangebot, das sich seit Juni 2004 kritisch mit der deutschsprachigen Presselandschaft auseinandersetzt"
        * "zeigen wir tagesaktuell sachliche Fehler, Sinnentstellendes und bewusst Irreführendes in den Berichterstattungen auf"
    * todo wo? Vergleich verschiedener Medien?
    * https://bildblog.de/126266/wenn-ich-reichelt-hier-sehe-habe-ich-den-eindruck-dass-er-in-einer-art-kriegszustand-lebt/, 2020
        * "Wallraff: “Abschießen, vernichten” waren zu meiner Zeit bei “Bild” Alltagsbegriffe.
            Es ging oft darum, Menschen “fertig zu machen”.
            “Bring die Sau zur Strecke!” Es gab auch keine   Trennung zwischen Berichterstattung und Meinung."

Rolle in der Demokratie
-----------------------
Informationsbeschaffung, Meinungsbildung, ...

* Artikel: ["Meinungsbildung und Kontrolle der Medien"](https://www.bpb.de/gesellschaft/medien-und-sport/medienpolitik/172240/meinungsbildung-und-kontrolle-der-medien?p=all), 2016
    * ...
    * "Bisher wird die besondere Beachtung des Fernsehens gegenüber anderen Medientypen an drei Kriterien festgemacht, die maßgeblich von der Rechtsprechung des Bundesverfassungsgerichts geprägt worden sind"
        * Breitenwirkung, Aktualität, Suggestivkraft
    * "Tabelle 2: Anteile der Medienkonzerne am Meinungsmarkt 2. HJ 2015"
    * "Tabelle 3: Übersicht über Arten des Medieneinflusses"
        * ...
    * ...
    * "Instrumentelle Aktualisierung"
    * ...

* ["Wie beeinflussen Medien unser Leben?"](https://schulzeug.at/deutsch/eroerterungen/wie-beeinflussen-medien-unser-leben/)

* siehe demokratie.md

### Machtmissbrauch
* siehe Film "Spotlight"
* siehe Film "Gelobt sei Gott" (2018)
* siehe BILD
