Hintergrund: Warum ist Privatsphäre wichtig
===========================================

<!-- toc -->

- [Inbox 2020](#inbox-2020)
- [Beispiel Facebook](#beispiel-facebook)
- [Wenn die Verkäuferin eine App wäre](#wenn-die-verkauferin-eine-app-ware)
- [Technik und Privatsphäre in der Schule](#technik-und-privatsphare-in-der-schule)
- [Entwicklungen zu Privatsphäre](#entwicklungen-zu-privatsphare)
- [Risiken für Benutzer](#risiken-fur-benutzer)
- [Unkontrolliertes Datensammeln](#unkontrolliertes-datensammeln)
- [Freie Software](#freie-software)
- [Staatliche Überwachung](#staatliche-uberwachung)
- [Handlungsoptionen für mehr Privatsphäre](#handlungsoptionen-fur-mehr-privatsphare)

<!-- tocstop -->

Inbox 2020
----------
* https://www.youtube.com/watch?v=9YgBcFCrH_E - "Das kleine Fernsehspiel: Operation Naked (09.02.2016 ZDF)"
    * siehe auch: Doku "Die geheime Macht von Google | Reportage | Das Erste", 2014, 45 min

Beispiel Facebook
-----------------
* siehe z. B. ["Get your loved ones off Facebook."](http://www.salimvirani.com/facebook/) and WhatsApp
    * Beispiel: eigenen Facebook-Data-Export anschauen:
        * Previous Names!
        * Timeline: alles auf der Pinnwand!
        * Photos
        * Friends, inklusive Removed Friends!
        * Messages: alle Nachrichten von allen an alle!
        * Ads: Advertisers with your contact info

Wenn die Verkäuferin eine App wäre
----------------------------------
* Video: [Wenn die Verkäuferin eine App wäre (Versteckte Kamera)](https://www.youtube.com/watch?v=wHo755bxByI)
    * "Dieser Frage ist das dänische Verbrauchermagazin Taenk"
    * hochgeladen von Stiftung Warentest: https://www.test.de/Messenger-Apps-Ein-Aussenseiter-schlaegt-WhatsApp-Co-4884453-0/
        * bekannte freie alternativen waren nicht im Test

Technik und Privatsphäre in der Schule
--------------------------------------
* didacta DIGITAL
    * ["Datenschutz - Finger weg vom Klassenchat - Datenschutz in der Schule"](https://www.didacta-digital.de/lernen-lehren/finger-weg-vom-klassenchat-datenschutz-in-der-schule), 2017
        * WhatsApp aus Datenschutzsicht nicht zulässig
    * ["Datenschutz - Fehlendes Problembewusstsein für Persönlichkeitsrechte von Kindern"](https://www.didacta-digital.de/digitale-kompetenz/fehlendes-problembewusstsein-fuer-persoenlichkeitsrechte-von-kindern)
        * "Sehr viele Erwachsene haben ein fehlendes Problembewusstsein in Bezug auf die Persönlichkeitsrechte von Kindern, wenn es um die Veröffentlichung von Informationen oder Bildern über Soziale Medien wie WhatsApp, Facebook oder Instagram geht. Das geht aus einer Umfrage im Auftrag des Deutschen Kinderhilfswerks hervor."
        * "Ein großer Teil der Erwachsenen macht sich anscheinend keine Gedanken über die Risiken, dass Bildmaterial von ihren Kindern in falsche Hände gerät. Auch scheint es nur eine knappe Mehrheit zu interessieren, welche Rechte sie dabei an die Plattformen abgeben."
* Riot.im (https://about.riot.im/) geeignet?
    * Frage zu Riot.im "permission.READ_CONTACTS - Explicit privacy explanation?" - https://github.com/vector-im/riot-android/issues/1776

Entwicklungen zu Privatsphäre
-----------------------------
* http://www.tagesschau.de/ausland/china-leihfahrraeder-101.html, 2017
    * "Die Leihfahrrad-Szene erlebt in China einen Boom. Allein in Shanghai sind rund eine Million Leihräder unterwegs. Den Anbietern geht es vor allem um die Daten der Kunden. Das Geschäftsmodell lockt internationale Investoren."
* https://www.heise.de/newsticker/meldung/c-t-empfiehlt-Raus-aus-den-US-Clouds-3685596.html
    * "c't empfiehlt: Raus aus den US-Clouds"
    * siehe User-Comments, siehe [unpassende Werbung](https://gnusocial.de/attachments/16ebd8e6e37324ae767ab3544e109936d61c56fdbce1c012dbdd50de16a05ad9.jpg)

Risiken für Benutzer
--------------------
* 2017: https://vizzzion.org/blog/2017/09/privacy-software/
    * "The more data that is collected, the bigger the risk of Identity Theft becomes"
    * "More collected data means that decisions will be made for the user based on skewed or incomplete information (imagine insurance policies)"
    * "Collected data may end up in the hands of oppressive regimes, posing risks to the user when travelling, or even at home"
    * "Blackmail"
    * "User's most private secrets may end up in the wrong hands"

Unkontrolliertes Datensammeln
-----------------------------
* ["Attac fordert wirksame Regulierung der Digitalökonomie - Datensammelwut fördert Steuervermeidung und gefährdet Demokratie"](http://www.attac.de/index.php?id=394&no_cache=1&tx_ttnews%5Btt_news%5D=9145), 07.04.2017

Freie Software
--------------
* https://stallman.org - "What's bad about: ..."

Staatliche Überwachung
----------------------
* todo: snowden-Zitat über den Beschaffenheit von Grundrechten
* ["Diese Unternehmen durchleuchten ihre Mitarbeiter"](https://www.welt.de/wirtschaft/article136863126/Diese-Unternehmen-durchleuchten-ihre-Mitarbeiter.html), welt.de, 2015

Handlungsoptionen für mehr Privatsphäre
---------------------------------------
* Allgemein: [Freie Software](../FreieSoftware)
* Bei Nichtvertrauen in den Mobiltelefonhersteller
    * LinageOS: https://lineageos.org/ ([Freie Software](../FreieSoftware))
* Bei Nichtvertrauen in den eigenen Internet-Anbieter
    * VPN
        * siehe z. B. https://blogs.gnome.org/mcatanzaro/2017/04/13/on-private-internet-access/ (Private Internet Access)
* ...
