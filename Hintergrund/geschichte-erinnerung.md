Geschichte Erinnerung
=====================

<!-- toc -->

- [Schlimme Zustände anderswo - Beispiel: Favelas in Brasilien](#schlimme-zustande-anderswo---beispiel-favelas-in-brasilien)
- [Drogenkrieg in Mexiko / Frauenmord](#drogenkrieg-in-mexiko--frauenmord)
- [2003-2019...: Darfur-Konflikt](#2003-2019-darfur-konflikt)
- [1994: Völkermord in Ruanda](#1994-volkermord-in-ruanda)
- [Jugoslawienkriege 1991–1995 und Kosovokrieg 1998–1999](#jugoslawienkriege-1991%E2%80%931995-und-kosovokrieg-1998%E2%80%931999)
- [1976–1983: Argentinische Militärdiktatur](#1976%E2%80%931983-argentinische-militardiktatur)
- [1964–1985: Militärdiktatur in Brasilien und VW](#1964%E2%80%931985-militardiktatur-in-brasilien-und-vw)
- [1940-1980 Apartheid](#1940-1980-apartheid)
- [194x: NS: Massaker von Distomo](#194x-ns-massaker-von-distomo)
- [194x: NS: Porajmos - Völkermord an den europäischen Roma](#194x-ns-porajmos---volkermord-an-den-europaischen-roma)
- [1941-1945: Holocaust](#1941-1945-holocaust)
- [1904: Völkermord an den Herero und Nama](#1904-volkermord-an-den-herero-und-nama)
- [Armenien](#armenien)
- [Sklaverei](#sklaverei)
- [Spanische Kolonisation Lateinamerikas / Entdeckung Amerikas](#spanische-kolonisation-lateinamerikas--entdeckung-amerikas)
- [1100-1200: Kreuzzüge](#1100-1200-kreuzzuge)
- [1100-1700: Inquisition](#1100-1700-inquisition)
- [Wikinger](#wikinger)
- [Vietnamkrieg](#vietnamkrieg)

<!-- tocstop -->

### Schlimme Zustände anderswo - Beispiel: Favelas in Brasilien
* Menschen leben im und vom Müll wohlhabender Menschen
* wird gern verdrängt

### Drogenkrieg in Mexiko / Frauenmord
* https://de.wikipedia.org/wiki/Drogenkrieg_in_Mexiko
    * "bewaffneter Konflikt zwischen Staat, mexikanischer Bevölkerung und Drogenkartellen"
    * "In einigen Landesteilen Mexikos haben die Kartelle das Gewaltmonopol des Staates faktisch außer Kraft gesetzt."
    * "Der Krieg hat von 2006, als das Militär systematisch im Inland eingesetzt wurde, bis Juni 2018 zwischen 200.000 und 250.000 Menschen das Leben gekostet."
    * Frauenmord: siehe Misogynie

### 2003-2019...: Darfur-Konflikt
* Darfur ist eine Region im Westen des Sudan (https://de.wikipedia.org/wiki/Darfur)
    * "Die Republik Sudan ist ein Staat in Nordost-Afrika mit Zugang zum Roten Meer."
* https://de.wikipedia.org/wiki/Darfur-Konflikt
    * "nach anderen Angaben starben durch den Konflikt bis 2019 rund 400.000 Menschen"
    * "Ein Grund für frühere Konflikte war die geografische Lage Darfurs als ein Zentrum für den Sklavenhandel"
    * unter anderem "Ethnische Konflikte als Ursache"
        * "Ad-Du’ain ist durch ein Massaker am 27. März 1987 an Dinka bekannt geworden" (https://de.wikipedia.org/wiki/Ad-Du%E2%80%99ain)
    * sehr komplex
* gary

### 1994: Völkermord in Ruanda
* Film: [Hotel Ruanda (2004)](https://de.wikipedia.org/wiki/Hotel_Ruanda)
* https://de.wikipedia.org/wiki/V%C3%B6lkermord_in_Ruanda
* 1.000.000 Tote
* gary

### Jugoslawienkriege 1991–1995 und Kosovokrieg 1998–1999
* Serbien, gary
* ...

### 1976–1983: Argentinische Militärdiktatur
* https://de.wikipedia.org/wiki/Argentinische_Milit%C3%A4rdiktatur_(1976%E2%80%931983)
    * "Der erste Junta-Chef General Jorge Rafael Videla sagte zu Beginn der Diktatur: „Es müssen so viele Menschen wie nötig in Argentinien sterben, damit das Land wieder sicher ist.“"
    * "kam es zu bürgerkriegsähnlichen Zuständen mit Staatsterror (ca. 30.000 Opfer)"
    * "Zustimmung der USA"
    * "Vorwürfe gegen die deutsche Regierung"

### 1964–1985: Militärdiktatur in Brasilien und VW
* https://de.wikipedia.org/wiki/Geschichte_Brasiliens#Milit%C3%A4rdiktatur_(1964%E2%80%931985)
    * "1964 putschte das Militär, unterstützt durch verdeckte Operationen des US-Geheimdienstes CIA"
    * "staatlicher Mord, staatliche Todesschwadronen, Folter und Verschwindenlassen von Oppositionellen an der Tagesordnung"
* 2019: Geschichtsklitterung: https://www.tagesschau.de/investigativ/swr/volkswagen-brasilien-diktatur-101.html

### 1940-1980 Apartheid
* https://de.wikipedia.org/wiki/Apartheid
    * "Als Apartheid wird eine geschichtliche Periode der staatlich festgelegten und organisierten sogenannten Rassentrennung in Südafrika, inklusive Südwestafrika, bezeichnet. Sie war vor allem durch die autoritäre, selbsterklärte Vorherrschaft der „weißen“, europäischstämmigen Bevölkerungsgruppe über alle anderen gekennzeichnet."
    * siehe "Mandela"
* siehe auch FUCK WHITE TEARS

### 194x: NS: Massaker von Distomo
* https://de.wikipedia.org/wiki/Massaker_von_Distomo

### 194x: NS: Porajmos - Völkermord an den europäischen Roma
* https://de.wikipedia.org/wiki/Porajmos
    * "bezeichnet den Völkermord an den europäischen Roma in der Zeit des Nationalsozialismus.
        Er bildet einen Höhepunkt der langen Geschichte von Diskriminierung und Verfolgung.
        Die Zahl der Opfer ist nicht bekannt. Nach unterschiedlichen Schätzungen liegt sie innerhalb einer großen Spannbreite, ist jedoch sechsstellig."

### 1941-1945: Holocaust
* https://de.wikipedia.org/wiki/Holocaust
    * "5,6 bis 6,3 Millionen europäischen Juden"
* gary

### 1904: Völkermord an den Herero und Nama
* Kolonialismus
* https://de.wikipedia.org/wiki/V%C3%B6lkermord_an_den_Herero_und_Nama
    * "Der durch Existenzängste geschürte Aufstand begann im Januar 1904 mit dem Angriff der Ovaherero unter Samuel Maharero auf deutsche Einrichtungen und Farmen."
    * "Der größte Teil der Herero floh daraufhin in die fast wasserlose Omaheke-Wüste.
    Trotha ließ diese abriegeln und Flüchtlinge von den wenigen dort existenten Wasserstellen verjagen,
    so dass Tausende Herero mitsamt ihren Familien und Rinderherden verdursteten."
    * "Vernichtungsbefehl mitteilen: „Die Herero sind nicht mehr Deutsche Untertanen.
    […] Innerhalb der Deutschen Grenze wird jeder Herero mit oder ohne Gewehr, mit oder ohne Vieh erschossen,
    ich nehme keine Weiber und keine Kinder mehr auf, treibe sie zu ihrem Volke zurück oder lasse auch auf sie schießen.“"
    * ...
    * ...

### Armenien
* gary
* ?

### Sklaverei
* todo: Jahr
* gary

### Spanische Kolonisation Lateinamerikas / Entdeckung Amerikas
* todo: Jahr
* Kolonialismus
* https://de.wikipedia.org/wiki/Kolonisation#Spanische_Kolonisation_Lateinamerikas
    * "Der berühmteste Entdecker wurde Christoph Kolumbus"
    * "die Einheimischen selbst wurden versklavt und als billige Arbeitskräfte ausgebeutet"
    * "Aufgrund deren hoher Sterblichkeit – zum einen durch die brutale Ausbeutung, zum anderen durch eingeschleppte Krankheiten –
        wurden sie aber bald durch aus Afrika verschleppte Sklaven ersetzt"
        * siehe https://de.wikipedia.org/wiki/Geschichte_der_Sklaverei
            * siehe oben Film Belle
            * ...
* Entdeckung Amerikas: Laut Arte-Doku (welche?) eigentlich von den Wikingern entdeckt.
    Danach kamen nochmal die Europäer, dann verstärkt mit Mord und Totschlag.

### 1100-1200: Kreuzzüge
* https://de.wikipedia.org/wiki/Kreuzzug
* gary, the crusades

### 1100-1700: Inquisition
* https://de.wikipedia.org/wiki/Inquisition
    * ...
* gary, Religionsgruppen

### Wikinger
* todo: Jahr
* Laut Arte-Doku (welche?) auch Menschenhandel etc.

### Vietnamkrieg
* ...
