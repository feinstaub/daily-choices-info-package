Umwelt- und Naturschutz
=======================

<!-- toc -->

- [Success Stories](#success-stories)
  * [2021](#2021)
  * [pending](#pending)
- [Human supremacism](#human-supremacism)
  * [Mögliches Statement](#mogliches-statement)
  * [Quellen](#quellen)
  * [Human rights without human supremacism, 2017](#human-rights-without-human-supremacism-2017)
  * [AV](#av)
  * [Tierschutz im Unterricht - Tierschutzbeauftragte Hessen](#tierschutz-im-unterricht---tierschutzbeauftragte-hessen)
  * [Vegan, aber Urvölker](#vegan-aber-urvolker)
  * [Mögliche Vorurteile gegen Veganismus](#mogliche-vorurteile-gegen-veganismus)
- [Alternativen zur Grasnutzung](#alternativen-zur-grasnutzung)
  * [Graspapier](#graspapier)
- [Alternativen zu Leder](#alternativen-zu-leder)
  * ["Holzschuhe" aus Furnier, TU Dresden, 2020](#holzschuhe-aus-furnier-tu-dresden-2020)
- [Inbox 2020](#inbox-2020)
  * [Traditionalismus](#traditionalismus)
- [Anfang 2020](#anfang-2020)
  * [Umweltschützer / Umweltverbände / (Tierschützer)](#umweltschutzer--umweltverbande--tierschutzer)
  * [Naturschutz und vegan, Hilal Sezgin](#naturschutz-und-vegan-hilal-sezgin)
  * [Naturschutzgruppen](#naturschutzgruppen)
  * [Hey, Weidefleisch ist tier- und umweltfreundlicher](#hey-weidefleisch-ist-tier--und-umweltfreundlicher)

<!-- tocstop -->

Success Stories
---------------
### 2021
* Eier: https://berlin.nabu.de/umwelt-und-ressourcen/oekologisch-leben/essen-und-trinken/huehnereier/ - "Das beste Ei ist immer noch die Alternative dazu."

### pending
* Eier: https://www.bund.net/bund-tipps/detail-tipps/tip/kein-ei-mit-der-3/
* Fleisch etc.: https://www.nabu.de/umwelt-und-ressourcen/oekologisch-leben/essen-und-trinken/natur/10169.html

Human supremacism
-----------------
### Mögliches Statement
* (zu Supremacy vergleiche https://de.wikipedia.org/wiki/White_Supremacy, „weiße Vorherrschaft“, „Überlegenheit der Weißen“)
* Statement:
    * Wir lehnen es ab, dass die faktische Überlegenheit bzw. Vorherrschaft der Menschen
        bei der Bewertung unseres Verhaltens gegenüber anderen Lebewesen (und der Natur als Ganzes)
        als zulässige Rechtfertigungsgrundlage hergenommen werden kann.
    * ...und damit andere gesellschaftlich akzeptierte Werte und Tugenden außer Kraft gesetzt werden
        * z. B. Ablehnung des Rechts des Stärkeren oder des Rechts des Intelligenteren
        * Empathie, Mitgefühl
        * Barmherzigkeit
        * Gesellschaftliche Grundwerte sind z. B., siehe https://www.bpb.de/izpb/8449/wirtschaftspolitik-und-gesellschaftliche-grundwerte
            * Freiheit
            * Gerechtigkeit
            * (Sicherheit)
            * (Fortschritt)
        * https://de.wikipedia.org/wiki/Allgemeine_Erkl%C3%A4rung_der_Menschenpflichten - "Allgemeine Erklärung der Menschenpflichten"
            * friedlich, freundlich, verständnisvoll, hilfsbereit (siehe auch Goldene Regel)
            * dem eigenen Gewissen unterworfen
            * Solidarität
            * (Selbstverteidigung im Falle eines Angriffs ist erlaubt)
            * Wahrhaftigkeit und Toleranz
            * Gleichwertigkeit aller Menschen
            * Ehrfurcht vor dem Leben
    * Dazu zählt auch Ablehnung von "Traditionalismus". Damit sind Handlungen gemeint, die innerhalb unserer allgemein
        akzeptierten gesellschaftlichen Werte nur dadurch gerechtfertigt werden können, dass man sich auf Tradition beruft.
* Beispiele, die wir daher ablehnen:
    * Das Töten von Ringeltauben (nicht gefährdete Art) außerhalb von Notsituationen, so wie hier praktiziert:
        https://jung-jaeger.eu/taubenjagd-im-winter/.
    * ...weitere...

### Quellen
* https://www.newstatesman.com/politics/staggers/2019/04/human-supremacism-why-are-animal-rights-activists-still-orphans-left, 2019
    * "Human supremacism: why are animal rights activists still the “orphans of the left”?"
    * "by Will Kymlicka"
    * ...
    * "And a growing body of social science evidence confirms that **many groups in society continue to be dehumanised**"
        * growing body of social science evidence: siehe unten
        * "not in the literal sense that they are not seen as members of the species Homo sapiens, but rather in the sense that they are seen as less likely to possess the “distinctly human” qualities that allegedly distinguish us from animals."
        * "**Specifically, outgroups today are seen as driven by basic instincts we share with animals** – say, pleasure or fear – but as less likely to display “distinctly human” feelings, such as remorse or gratitude. This leads to discrimination and prejudice.
        But it also renders outgroups vulnerable to violence, since it is assumed that, **like animals, they need to be controlled through force.**"
    * "But there is now **growing interest in bridging the chasm.**
        One reason is that **supremacism is difficult to defend in our post-Darwin world**.
        Darwin’s theory of evolution showed that humans and other animals are continuous
        with respect to our interests and capacities."
        * "A second reason for overcoming the gap is the recognition on the left that animal agriculture is an environmental catastrophe"
    * "But perhaps the most **important reason for building bridges** is that **human supremacism may not be an effective strategy for fighting dehumanisation after all**.
        There is growing evidence that shows that a belief in human supremacy and species hierarchy aggravates,
        rather than alleviates, the problem of dehumanisation. **The more people believe that humans are superior to animals**,
        **the more likely they are to dehumanise immigrants, women, and racial minorities.**"
        * "Interestingly, the **link between species hierarchy and dehumanisation is causal, not just correlational**.
            For instance, when participants in studies are given a newspaper story reporting on evidence for
            human superiority over animals, the outcome is the expression of greater prejudice against human outgroups."
        * ...
        * "In short, reducing the status divide between humans and animals helps to reduce
            prejudice and helps to strengthen belief in equality amongst human groups."
    * "This suggests that human supremacism is not only unnecessary to counteract dehumanisation, but is in fact counterproductive."
        * ...

    * growing body of social science evidence:
        * https://www.annualreviews.org/doi/abs/10.1146/annurev-psych-010213-115045, "Dehumanization and Infrahumanization", Haslam, 2014
            * eine Bestandsaufnahme

    * growing evidence:
        * https://www.newscientist.com/article/mg21628950-400-the-link-between-devaluing-animals-and-discrimination/, 2012
            * "The link between devaluing animals and discrimination"
            * (anerkannt: https://de.wikipedia.org/wiki/New_Scientist)
            * "By Gordon Hodson and Kimberly Costello"
            * "**Our feelings about other animals have important consequences for how we treat humans**"
            * "“AUSCHWITZ begins whenever someone looks at a slaughterhouse and thinks: they’re only animals,” wrote the philosopher and social commentator Theodor Adorno, reflecting on the Holocaust. He argued that humans degrade, exploit and wilfully murder “under-valued” other people once they are considered to be animal-like. Genocides can happen, therefore, when we think of members of other groups – outgroups – to be considerably less human than ourselves. This process unleashes a raft of negativity as the moral protections normally afforded to humans are cast aside."
            * (pay wall)
        * "Exploring the roots of dehumanization: The role of animal—human similarity in promoting immigrant humanization", 2009
            * **Kimberly Costello, Gordon Hodson**
            * Study 1: "As predicted, beliefs that animals and humans are relatively similar were associated with greater
                immigrant humanization, which in turn predicted more favorable immigrant attitudes"
            * Study 2: "perceptions of animal—human similarity were experimentally induced through editorials highlighting
                similarities between humans and other animals or emphasizing the human—animal divide"

### Human rights without human supremacism, 2017
* https://www.tandfonline.com/doi/abs/10.1080/00455091.2017.1386481
    * Will Kymlicka, "Philosophy Department, Queen’s University, Kingston, Canada"
        * Author of "Zoopolis: A Political Theory of Animal Rights"
    * "Early defenders of the Universal Declaration of Human Rights invoked species hierarchy: human beings are owed rights because of our discontinuity with and superiority to animals. Subsequent defenders avoided species supremacism, appealing instead to conditions of embodied subjectivity and corporeal vulnerability we share with animals. In the past decade, however, supremacism has returned in work of the new ‘dignitarians’ who argue that human rights are grounded in dignity, and that human dignity requires according humans a higher status than animals. Against the dignitarians, I argue that defending human rights on the backs of animals is philosophically suspect and politically self-defeating."

    * TODO: read more
        * "From human rights to sentient rights", 2013, Cochrane
        * "Animal rights is a social justice issue", 2015, Jones
        * "‘Simply in virtue of being human’? A critical appraisal of a human rights commonplace", 2018, Fasel
        * "Are Human Rights Universal or Culturally Relative?", 2016, Le
        * "Moving Beyond Arbitrariness: The Legal Personhood And Dignity Of Non-Human Animals", 2009, ...

### AV
* https://www.anonymousforthevoiceless.org/about-us
    * "Anonymous for the Voiceless is against all forms of non-human animal exploitation and promotes a clear vegan message.
        As we **fight against human supremacism**, we do not tolerate discrimination, bullying or harassment of any kind within
        our organisation. We expect anyone representing us to be respectful and inclusive towards people of all racial,
        political, religious, cultural and any other groups."

### Tierschutz im Unterricht - Tierschutzbeauftragte Hessen
* https://tierschutz.hessen.de/%C3%BCber-uns/tierschutzunterricht-schulen-wieso
    * "Tierschutzunterricht an Schulen - wieso?"
    * "Oberstes Ziel ist es, junge Menschen dafür zu sensibilisieren, Tiere als fühlende und leidensfähige Mitgeschöpfe zu begreifen."
    * "Die gegenwärtige Lebenssituation von Tieren im Verantwortungsbereich des Menschen kann bewusst gemacht, hinterfragt und Änderungsmöglichkeiten können diskutiert werden."
    * "Tierschutz im Unterricht kann **einen wichtigen Beitrag zur mitfühlenden und verantwortungsvollen Lebenshaltung sowie zur Gewaltfreiheit in allen Lebensbereichen leisten**."
* Urteilsdatenbank: https://tierschutz.hessen.de/%C3%BCber-uns/datenbank-zur-recherche-von-tierschutzrechtsf%C3%A4llen
* "Tierschutz geht alle an" https://tierschutz.hessen.de/%C3%BCber-uns/emp%C3%B6rung-mitleid-gen%C3%BCgen-nicht
    * "Empörung & Mitleid genügen nicht!"
    * ...
    * "Den Fleischkonsum einzuschränken fällt gar nicht so schwer, wie Sie vielleicht glauben. Die Devise lautet: Weniger, aber dafür aus **artgerechter** Tierhaltung."
    * ...
* "Entwicklung eines Geflügelschlachtmobils" für Hessen, siehe schlachten.md

### Vegan, aber Urvölker
- Beim Veganismus geht es zunächst darum, vor der eigenen Haustür zu kehren. Gesellschaftlich und individuell.
    Wir leben in einer Industriegesellschaft, in der Tierprodukte nur noch aus Gewohnheit, Bequemlichkeit, Tradition oder Geschmack konsumiert werden.
    Das ist mit den Grundwerten, die wir uns als Gesellschaft erarbeitet haben, nicht vereinbar und muss schnellstens aufhören.
- Unsere Kultur beinhaltet das Weltbild der "Human Supremacy" (analog zur White Supremacy).
    Wir fühlen uns uns der Natur und den Tieren so dermaßen überlegen, dass wir es gar nicht für notwendig erachten,
    Rechenschaft über unser Tun abzulegen. Es gilt das Recht des Stärkeren.
    Und das wird auch gegenüber Kulturen ausgeübt, die nicht an die Human Supremacy glauben.
    Nämlich die Urvölker, die wir vernichtet haben und gerade dabei sind, die letzten Reste auch noch zu vernichten.
- siehe auch "Veganismus gleich Humanismus"

### Mögliche Vorurteile gegen Veganismus
* ...entkräften mit dem Verweis darauf, dass die Grundhaltung, die zu Vegansimus führt, die Ablehnung
    von Human supremacism ist, der - siehe oben - begrüßenswerte Effekt hat und außerdem deckungsgleich
    mit den Werten der meisten Umweltschützer:innen ist.

Alternativen zur Grasnutzung
----------------------------
Man muss Gras nicht unbedingt immer nur als Futtermittel für Stalltiere hernehmen.
Es gibt auch andere Nutzungsmöglichkeiten.

### Graspapier
* 2020:
    * http://www.graspapier.de/heu-lieferant-werden/
        * Heu-Lieferant werden
        * "Wollen Sie für uns als Systempartner Heu in Ihrer Region produzieren?"

    * Zusammen mit Wissenschaftler der Uni Bonn, siehe Video
        * https://printelligent.de/umweltfreundlich-drucken-mit-graspapier/, mit Video_: 5 min, 2017
            * ...

    * https://diegrasdruckerei.de/
        * "Ein grüner Gruß von Ihrer Graspapier-Druckerei aus Stuttgart"
        * "Vegan und klimaneutral auf Graspapier drucken"
        * "Nutzen Sie Graspapier, das aus heimischen Wiesen gewonnen wird!"
        * "Für die Entwicklung der besonders nachhaltigen Papieralternative aus Grasfasern gewann creapaper den KfW Award Gründen 2017"
        * ...
        * mit Video_ YT: "Scheufelen Graspapier Deutsche Version", 2017, 3 min
            * ...

    * YT: "graspapier start green film", 2017, 2 min
        * ...

* 2018
    * https://www.creapaper.de/
        * Frage: Grasflächen für Papier anstelle für Rinder?
* 2018: Graspapier (http://www.graspapier.de/) - Papier statt Milch und Fleisch?
    * siehe Erklärfilm: im Vergleich zu Holzzstellstoff:
        * viel kürzere Transportstrecken (weil Holz oft aus dem Ausland kommt)
        * weniger Wasserverbrauch
        * weniger Energieverbrauch
        * viel weniger Chemie

Alternativen zu Leder
---------------------
"Leder ist ein Naturprodukt". Holz auch.

### "Holzschuhe" aus Furnier, TU Dresden, 2020
* "Holzschützender Wirkstoff aus Pflanzenzellen – für Schuhe aus flexiblem Furnier"
    * https://tu-dresden.de/ing/der-bereich/news/holzschuetzender-wirkstoff-aus-pflanzenzellen-fuer-schuhe-aus-flexiblem-furnier
* https://www.sg-veneers.com/produkte/nuo-weltweit-erstes-weiches-holz.html
    * "NUO. Holz so weich wie Leder."
    * "NUO ist weich wie Leder und geschmeidig wie Stoff – dabei aus echtem Holz gefertigt, vegan und nachhaltig.
        Die angenehme Haptik und die Schönheit des Materials überzeugen und begeistern sofort. "
    * verschiedene Gravuren
    * mit Video, 1 min

Inbox 2020
----------
### Traditionalismus
* Nicht alle liebgewonnenen Methoden von damals sind für die neue Zeit zu gebrauchen.
    * Beispiel NABU-Vortrag zur Biodiversität:
        Mit der Versiegelung etc. aufhören (so wie es damals war) reicht nicht.
        Wir müssen aktiv gegensteuern und regenerieren.
        Siehe auch Permakultur-Vortrag zur Nachhaltigkeit.

Anfang 2020
-----------
### Umweltschützer / Umweltverbände / (Tierschützer)
* Ideen zum Fleischatlas 2018:
    * **Anstelle tierische Eiweiße** als hochwertiger gegenüber Pflanzeneiweißen darzustellen (s. S. todo), könnte man auch folgendes machen:
        * erst die bekannten, gesunden und von der DGE empfohlenen pflanzlichen Eiweißquellen auflisten (Hülsenfrüchte etc.) und dann tierische Eiweiße. Dann könnte man in dem Insektenbeitrag einfach das Fleisch von Säugetieren durch das von Insekten ersetzen.
* Tierwohl, Umwelt, Gesundheit und Gerechtigkeit:
    * siehe vegan
    * siehe Bio
    * werteorientierte Betrachtung, rationale Methoden, Fokus auf _hier_ (also Industrieländer), Vorbild sein, Moralvorstellungen haben sich weiterentwickelt
    * Bequemlichkeit, Gewohnheit, Tradition, Geschmack
        * Zitat: "Statt einer Leitkultur forderte er "Leitwerte", die nicht Traditionen aus Faulheit zur Tugend erheben." (https://www.heise.de/newsticker/meldung/re-publica-Sascha-Lobo-plaediert-fuer-offensiven-Sozial-Liberalismus-4039557.html), 2018
    * vegane Veranstaltung hilft Menschen zu zeigen, wie leicht man sich ganz praktisch auch mal einen Tag ohne Tierprodukte ernähren kann
        * wichtig für das Ziel einer Fleischreduktion
    * Wenn es darum geht, Geld für Tierschutz auszugeben, sollte das vor allem in **Bildungsarbeit** investiert werden
        * Wenn die Menschen verstehen, dass Tiere mehr Wert sind als Objekte, geht der Rest automatisch
* Tierwohl-Ergänzungen zum blinden Fleck des Schlachtens:
    * siehe [Schlachten](../Vegan/schlachten.md) / Bio
* **Natürlichkeit** vs. aktuelle auf Leistung gezüchtete Qualrassen (vor allem bei Huhn und Milch-Kuh)
* FRAGE: Welche Tiere in der Nutztierhaltung sind notwendig?
    * siehe "Sind Tiere für eine nachhaltige Landwirtschaft unbedingt notwendig?"
    * Sicht der Tiere?
* HINTERGRUND: Es gibt eine Lobby, die sich gegen die Interessen der Allgemeinheit stellt
    * z. B. dokumentiert auf http://agrarlobby.de
        * ["Systematisches Tierleid in den Ställen Deutschlands wichtigster Agrarlobbyisten"](http://agrarlobby.de/verbaende/die-lobby-gegen-tiere/)
            * [aktuelle Videoaufnahmen](http://www.lobby-gegen-tiere.net)
        * ["Bauernverband interpretiert Klimaschutz"](http://agrarlobby.de/2016/04/22/bauernverband-interpretiert-klimaschutz/)
            * Zitat Lobby: "Bekenntnis der Politik zur Tierhaltung in Deutschland. Ohne die Haltung von Wiederkäuern wie Rindern, Schafen und Ziegen sei der Erhalt und die Nutzung von rund 5 Millionen Hektar Grünland in Deutschland nicht möglich."
            * Bewertung: "Er spricht sich zudem für die Tierhaltung generell aus, erwähnt dann aber nur Rinder, Schafe und Ziegen – für das exportorientierte Wachstum ist aber auch die industrielle Schweine- und Hühnerhaltung zentral, die natürlich mitnichten auf Grünland stattfindet."
        * ["Tierwohllabel: 70 Mill. Euro Werbung"](http://agrarlobby.de/2017/01/20/tierwohllabe-70-mill-werbung/)
            * "Zum Vergleich: für eine Kampagne gegen Lebensmittelverschwendung gab das Ministerium 2,8 Millionen Euro aus"
            * Link zu [Fortschrittsversprechen](http://agrarlobby.de/propaganda/fortschrittsversprechen/)
                * "Heute besser als früher" -> Einschätzung:
                    * "Die These selber ist je nach Vergleichsobjekt wahr oder falsch. Kühen geht es wohl in Laufställen besser als angebunden. Aber Hühnern geht es wohl im Hinterhof besser als im Massenmaststall. Hinzu kommen die Veränderungen der Tiere durch Zucht, die sie unabhängig von den Haltungsbedingungen heute körperlich beeinträchtigen. Außerdem gibt es die Veränderung in der Masse: Heute leiden viel viel mehr Tiere unter der menschlichen Nutzung als jemals zuvor."
                    * "Für die Frage, ob die heutige Nutzung ok ist, ist es unwichtig, ob sie für die Tiere besser oder schlechter ist als früher. Menschen haben Tiere in der Nutztierhaltung schon immer brutal ausgebeutet und auf ihre Bedürfnisse nicht geachtet. Daran hat sich nichts geändert. Nur weil es immer schlimmer geht, ist die gegenwärtig übliche Praxis nicht gerechtfertigt."
                * Einschätzung zu "Initiative Tierwohl"
                * siehe auch [fortschrittsversprechen](../Hintergrund/nachhaltigkeit.md -> Fortschrittsversprechen)
        * [Mythen](http://agrarlobby.de/propaganda/mythen/)
            * ["Nur Tiere, die sich wohlfühlen und gesund sind, bringen auch wirtschaftlichen Erfolg für den Betrieb."](http://agrarlobby.de/propaganda/mythen/tierwohl-und-leistung/)
                * "[...] Das heißt aber nicht, dass es in seinem wirtschaftlichen Interesse ist, jedes Einzeltier bestmöglich gesund zu halten – auch mit einem Stall mit vielen kranken Tieren lässt sich gutes Geld verdienen. Hinzu kommt, dass Gesundheit und Wohlbefinden nicht dasselbe sind: Auch wenn ein Tier körperlich gesund ist, heißt das nicht, dass es ihm auch gut geht."
        * http://agrarlobby.de/propaganda/abwertung-der-kritik
        * http://agrarlobby.de/propaganda/kinder-jugendliche/
            * "Eine zentrale Zielgruppe für die Öffentlichkeitsarbeit der Agrarlobby sind Kinder und Jugendliche."
* Förderung der lokalen Wirtschaft:
    * siehe Freie Software
    * siehe Bio
* Elektroschrott-Reduktion:
    * siehe Freie Software
* Gülle-Problem, Grundwasser etc.:
    * siehe vegan
* Klimaschutz
    * siehe vegan
* Konsumantreibende Werbung reduzieren
    * siehe Freie Software
* Vorreiter-Beispiele: BMUB, **BUND-Jugend**
    * [BMUB-Catering 2017](http://www.bmub.bund.de/bmub/aufgaben-und-struktur/catering-bei-veranstaltungen-des-bmub/)
    * BUND-Jugend-Hauptversammlungen vegan (und bio?)
    * Projekt "otto konsumiert nachhaltig" vom BUND Sachsen-Anhalt
        * http://bund-sachsen-anhalt.de/index.php?id=1108
        * inkl. Ernährungsformen vegetarisch und vegan
            * verlinkt auf: https://www.magdeburg-vegan.de
                * https://www.magdeburg-vegan.de/category/videos/
                    * Was tun mit alten Bananen
                * https://vegan-tv.de
                    * Video: ["Mein Weg zum TINY HOUSE in Deutschland"](https://www.youtube.com/watch?v=1QgnxuiA1DI)
                        * ...
                        * Rechtliche Situation in Deutschland: Abmessungen, wo abstellen?, Wasser, Strom etc.
                        * follow up:
                            * http://tiny-houses.de/
                            * ["Ihr wollt ein Tiny House? Das müsst ihr wissen, bevor ihr euch ein Minihaus kauft"](http://www.businessinsider.de/plant-ihr-ein-tiny-house-in-deutschland-das-muesst-ihr-wissen-bevor-ihr-euch-ein-minihaus-kauft-2016-3), 2016

### Naturschutz und vegan, Hilal Sezgin
* Video: "The End of Meat - Hilal Sezgin - Mit Tieren leben (DE)", 2017
    * https://vimeo.com/240162622, 30 min
    * in Frankfurt aufgewachsen
    * ...
    * Relation zum Naturschutz
        * ...
        * vegan heißt auch bescheidener sein
        * weniger Raum zu beanspruchen (für Wohnen, für die Auslagerung von Hobbies, Produktion etc.)
        * für Natur und Tiere
        * ...
        * "vegane Fahrweise": langsam bei Wildwechsel
    * ...
    * Frankfurter Zoo: Menschenaffengehege und davor ein Spiegel mit "das gefährlichste Raubtier" (der Mensch)
        * blanke Ironie
    * größtes Problem: **"wir sehen nicht mit den Augen, sondern mit dem Kopf"**
        * wir sehen ein Stück Fleisch auf dem Teller, aber nicht das Tier
        * wir sagen, wir seien das größte Raubtier und sperren Affen ein
        * wir lieben Pferde und reiten Rennen mit ihnen
        * das alles können wir Menschen
        * ...
        * immer wieder werden die Konzepte angepasst, um sich den Status Quo schönzureden
            * der Begriff "Massentierhaltung" rüttelt mittlerweile nicht auf, denn man kauft ja schließlich immer woanders ein
        * sehen, aber nicht sehen
    * 9 min
    * Auch Veganer können/müssen auf diese Weise verdrängen
        * Ledersofa, Medikamente, etc. etc., so ist unsere Gesellschaft eben
        * allgemeiner menschlicher Selbstschutz
        * Beispiel: beim Schaf mit Schädelfraktur das Schmerzmittel vergessen
            * Härte vs. beim Mensch: tut weh, aber muss jetzt sein, muss er durch
    * **Paternalismus**
        * z. B. gegenüber Kindern: das musst du jetzt tun, in 20 Jahren wirst du mir danken
    * Gewalt: früher: Frau schlagen und lieben ging ohne Probleme zusammen
    * **Zuneigung und Hierarchie (Liebe und Gewalt)** können gut zusammengehen, wenn wir so gelernt haben
    * 14 min
    * Paternalismus gegenüber Tieren bei Haustierhaltung
    * ...
    * ...

### Naturschutzgruppen
Fragen:

* Wenn jeder zum Grillen **Wildbret** in dieser Menge nehmen würde, wäre dann genug für alle da?
* Wenn wir Fleisch reduzieren sollten (was ja eine zentrale Forderung ist) und es unmöglich erscheint,
    auch nur eine Veranstaltung im Jahr mal ganz ohne auszurichten, wie soll man das begründen?
* Ist es überhaupt möglich, eine Veranstaltung ohne Tierprodukte auszurichten, und dennoch alle zufrieden sind?
* Wenn nicht einmal wir es schaffen, eine einzige Veranstaltung im Jahr auch ganz ohne Tierprodukte auszurichten, wie kann dann eine Fleischreduktion in der Breite funktionieren?

Hinweise:

* **Privilegierungsargument**: Dass wir heute hier mit Wild- und Weidefleisch grillen können, wird dadurch ermöglichst, dass andere Menschen in der Region, die dieselbe Menge an Tierprodukten für sich beanspruchen, diese aus Massentierhaltung beziehen. Damit hat die Situation etwas Abgehobenes und Elitäre an sich.

Tipps:

* Bisher auf dem Flyer: "Kuchenspenden für die Veranstaltung sind sehr willkommen"
    * Besser 1: "Kuchenspenden für die Veranstaltung sind willkommen; besonders vegane Rezepte mit Zutaten in Bio-Qualität sind gerne gesehen. Aber alles andere ist auch in Ordnung"
    * Besser 2: "Kuchenspenden für die Veranstaltung sind sehr willkommen; bitte mit Zettel versehen, ob bio und vegan oder nicht."
* Auf dem Flyer steht: "Für die Verköstigung der Besucher kümmern wir uns"
    * Das Angebot wird vom Verein komplett Bio und Vegan gestaltet.
    * Tipp: Diesen Umstand nicht laut kommunizieren. Die Gäste werden das Angebot gerne annehmen, weil sie froh sind, wenn es überhaupt etwas gutes zu essen gibt.
    * Hinweis zur Beruhigung: Es ist üblich, dass der Gastgeber bestimmt, was angeboten wird.
    * Fallstrick: Nicht in Schwarz-Weiß-Denkmuster verfallen. Einfache Begründung:
        * Nur weil man als Naturschutzgruppe und Gastgeber vorbildlich bio und veganes Essen anbietet, heißt das nicht, dass den Gästen und anderen Leuten irgendwas vorgeschrieben wird. Denn zuhause und anders wo kann jeder (auch die Naturschützer) tun, lassen und essen, was er will.
        * Jede Mahlzeit zählt.
    * Tipp: Die veganen Rezepte parat haben. Denn die Erfahrung zeigt, dass viele Leute neugierig und dann begeistert von den bio-veganen Kuchen sind. Falls sie überhaupt merken, dass da keine Eier oder Milch drin ist.
        * Beispiel: veg. Schokokuchen, veg. Nusskuchen, Erdbeerkuchen mit veganem Bodenrezept
* Praktisches Beispiel 2017: Kasseler(?) Umwelttage bieten nur vegetarisch und veganes Essen an.
    * auch hier gilt: Keinem Gast wird irgendwas vorgeschrieben. Jeder sollte mal für ein paar Stunden auf Wurst verzichten können, oder einfach woanders sich die Dosis besorgen.

### Hey, Weidefleisch ist tier- und umweltfreundlicher
* Video: ["Hey Veganer, Weidefleisch ist tier- und umweltfreundlicher."](https://youtu.be/qJiTJimZ9QA), 2018, 10 min
    * ...
    * "Und vielen scheint es dabei auch zu reichen, dass ethisch überlegenes Weidefleisch existiert oder existieren könnte um das mehr oder weniger unbewusst aufs Fleischessen allgemein zu übertragen und um dann weiter weitgehend BELIEBIGE Tierprodukte zu konsumieren."
    * ...
    * "Und wenn es wirklich um die Verminderung von Tierleid gehen sollte, dann sucht man nicht nach dem vermeintlich tierfreundlichsten Fleisch sondern nach dem tierfeundlichsten Nahrungsmittel. Und das ist dann kein Fleisch."
