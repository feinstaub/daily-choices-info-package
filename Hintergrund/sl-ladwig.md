Bernd Ladwig
============

<!-- toc -->

- [2020](#2020)
  * [Video: Gerechtigkeit für Tiere | Sternstunde Philosophie](#video-gerechtigkeit-fur-tiere--sternstunde-philosophie)
  * [Video: Umweltschutz und Tierrechte | Suhrkamp espresso #21](#video-umweltschutz-und-tierrechte--suhrkamp-espresso-%2321)
  * [Ethik und Schlachten - Philosoph Bernd Ladwig: Wir schulden Tieren etwas](#ethik-und-schlachten---philosoph-bernd-ladwig-wir-schulden-tieren-etwas)
  * [Das Fleisch gehört uns nicht](#das-fleisch-gehort-uns-nicht)
- [2015](#2015)
  * [„Ein großes, saftiges Schnitzel“](#%E2%80%9Eein-grosses-saftiges-schnitzel)

<!-- tocstop -->

2020
----
### Video: Gerechtigkeit für Tiere | Sternstunde Philosophie
* YT: "Gerechtigkeit für Tiere | Sternstunde Philosophie | SRF Kultur", 2020, 60 min
    * Philosoph Bernd Ladwig im Gespräch mit SRF-Moderatorin Barbara Bleisch
    * "Die gesellschaftliche Einstellung zu Tieren schwankt zwischen inniger Liebe und Ausbeutung. Manche haben einen Namen und werden als Haustiere verwöhnt, andere haben eine Nummer und werden unter leidvollen Bedingungen gehalten, um dann nach einigen Wochen oder Monaten getötet zu werden. Diese Ausbeutung der Tiere ist tief in der menschlichen Lebenspraxis und Kultur verankert. Der Philosoph Bernd Ladwig fordert deshalb eine politische Wende in der Debatte."

* Fragestellungen
    * TODO

### Video: Umweltschutz und Tierrechte | Suhrkamp espresso #21
* YT: "Umweltschutz und Tierrechte | Suhrkamp espresso #21"
    * https://www.youtube.com/watch?v=AVQx8IFKO48&t=366s - »Politische Philosophie der Tierrechte«
        * **Wie verhilft man jemandem zu seinen Rechten, der keine Stimme hat?**

### Ethik und Schlachten - Philosoph Bernd Ladwig: Wir schulden Tieren etwas
* https://www.deutschlandfunknova.de/beitrag/philosoph-ueber-das-toeten-von-tieren-wir-schulden-tieren-etwas, 2020
    * "Aber er geht weiter: Tiere wollen leben. Und das heißt, dass wir Menschen kein Recht haben, sie zu töten.
        Zumindest nicht wir, die wir im zivilisatorischen Überfluss leben.
        Wir können einfach in den Supermarkt gehen und und pflanzliche Produkte kaufen, von denen wir uns gesund ernähren können.
        Und genau das sollten wir auch tun."

### Das Fleisch gehört uns nicht
* "Das Fleisch gehört uns nicht - Warum politischer Veganismus der Auftakt zu einer humanen Gesellschaft sein könnte."
    * https://www.neues-deutschland.de/artikel/1138706.das-fleisch-gehoert-uns-nicht.html, 2020
    * todo

2015
----
### „Ein großes, saftiges Schnitzel“
* "Was gibt uns das Recht, andere Spezies aufzuessen? Nichts, findet Moralphilosoph und FU-Professor Bernd Ladwig. Trotzdem vermisst er als Veganer manchmal die Schnitzel seiner Kindheit."
    * https://furios-campus.de/2015/06/22/ein-grosses-saftiges-schnitzel/, 2015
    * ...
    * "Was braucht es, um Menschen dazu zu bringen, kein Fleisch mehr zu essen?"
        * "Ich bin überzeugt, dass Menschen aus rein moralischer Einsicht handeln können, weil sie verstehen, dass etwas falsch ist."
        * ...
