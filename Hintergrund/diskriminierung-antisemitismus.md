Antisemitismus
==============

<!-- toc -->

- [Einleitung](#einleitung)
- [Manifestationen](#manifestationen)
- [Holocaust](#holocaust)
  * [Beispiel 2019](#beispiel-2019)
- [Aufklärung](#aufklarung)

<!-- tocstop -->

Einleitung
----------
Judenhass

* 2020, tagesschau
    * Worte, Vorurteile
        * direkt: "Auch heute noch ist der Einfluss der Juden zu groß" (15 % ganz oder teilweise)
        * indirekt: "Viele Juden versuchen aus der Vergangenheit des Dritten Reiches heute ihren Vorteil zu ziehen." (26 % ganz oder teilweise)
        * wegen Israel-Politik: "Bei der Politik, die Israel macht, kann ich gut verstehen, dass man etwas gegen Juden hat" (40 % ganz oder teilweise)
    * Taten:
        * Kriminalstatistik: Die meisten Taten kommen aus dem rechten Spektrum.
        * hohe Dunkelziffer
        * Steigerung 2019->2020
    * Studie:
        * ca. 50 % in den letzten Jahren antisemitische Erfahrungen
        * ca. 40 % denken an Auswandern

Manifestationen
---------------
* Anschläge auf Synagogen
* 2020: Verschwörungsmythen in Zeiten Corona

Holocaust
---------
Leugnung, Relativierung, Instrumentalisierung

### Beispiel 2019
* https://extinctionrebellion.de/blog/extinction-rebellion-deutschland-verurteilt-instrumentalisierung-des-holocaust-und-verhalten-von-roger-hallam/
    * "Extinction Rebellion Deutschland verurteilt Instrumentalisierung des Holocaust und Verhalten von Roger Hallam"
    * https://extinctionrebellion.de/blog/extinction-rebellion-was-wir-aus-hallam-lernen-k%C3%B6nnen/
        * "Rebecca und Tino von XR ziehen ihr Fazit, nach einem Gespräch mit einem Holocaustforscher"
        * ...

Aufklärung
----------
* im Theater
* ...
