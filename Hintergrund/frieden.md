Frieden
=======

<!-- toc -->

- [Inbox](#inbox)
  * [2020](#2020)

<!-- tocstop -->

siehe ::frieden

Inbox
-----
### 2020
* "The Power of Direct action with Peter Tatchell and Jane Tallents | Extinction Rebellion"
    * "Interview - **Angie Zelter** - Dismantling Weapons of Mass Destruction", https://www.youtube.com/watch?v=TNt89Qoxumc, 2020, 30 min
        * ...
        * peace service by women
        * ...
    * "LESSONS FOR LIFELONG ACTIVISM  # 1 - Angie Zelter - Wednesday 4th February 2015 – Bradford University, https://docs.google.com/document/d/1HJZH7qcjw3nLUj1RsTYDdqjMzmBetbp11cyopHUwuHI
    * https://de.wikipedia.org/wiki/Trident_Ploughshares
        * http://tridentploughshares.org/

* vs. Militär: #WorldPeace4Climate #Fridays4Worldpeace #Rebel4WorldPeace
