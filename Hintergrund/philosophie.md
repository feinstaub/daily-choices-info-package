Philosophie
===========

<!-- toc -->

- [Liebe zur Weisheit](#liebe-zur-weisheit)
  * [Filme](#filme)
- [Inbox 2020](#inbox-2020)
  * [Harari](#harari)
- [Philosophie der Nachhaltigkeit](#philosophie-der-nachhaltigkeit)
- [Natur als Wert an sich](#natur-als-wert-an-sich)
  * [Vorreiterunternehmen](#vorreiterunternehmen)
- [Verantwortung](#verantwortung)
- [Menschenrechte](#menschenrechte)
- [Lebensrechte anderer Lebenwesen](#lebensrechte-anderer-lebenwesen)
  * [Vegan](#vegan)
  * [Eine neue christliche Tierethik](#eine-neue-christliche-tierethik)
- [Freie Software](#freie-software)
- [Humanismus](#humanismus)
  * [Veganer Humanismus](#veganer-humanismus)
  * [Veganismus als neue Aufklärungsbewegung](#veganismus-als-neue-aufklarungsbewegung)
  * [Portale](#portale)
  * [Für Helfer / "Gutmenschen"](#fur-helfer--gutmenschen)
- [Geschichte](#geschichte)

<!-- tocstop -->

Liebe zur Weisheit
------------------
* https://de.wikipedia.org/wiki/Philosophie

### Filme
* http://www.philfilms.utm.edu/ - Philosophical Films
    * Matrix
    * ...

Inbox 2020
----------
* "PHILOSOPHY - Ethics: Killing Animals for Food [HD]", 9 min

### Harari
* siehe auch geschichte.md
* "Yuval Noah Harari: "21 Lessons for the 21st Century" | Talks at Google", https://www.youtube.com/watch?v=Bw9P_ZXWDJU, 2018, 60 min
    * ...
    * Die Veränderung wird allgegenwärtig sein
        * eigene Identität ist weniger ein Haus, als ein ein Zelt
            * YT comment: "Sounds like a perfect recipe for an explosion of mental illness.. “reinventing yourself/identity fluidity”. Harari’s vision describes what seems to be a pretty dismal future of economic precarity—moving our tent of identity around like beleaguered refugees.. would like to see him and Zizek debate about this."
        * Vorbereiten sich im Laufe des Lebens mehrfach neu zu erfinden
    * ...
    * Konzept aus dem 18./19. Jahrhundert
        * Freier Wille
            * Determinismus vs. Randomness und Kombinationen
            * "der Kunde hat immer Recht" als Verkörperung des freien Willens; als letzte Rechtfertigung für Unternehmen
        * heute: löst sich auf, durch Determinismus
    * ...
    * Zwei verschiedene Arten von Gott/Götter
        1. Das Mysterium
        2. Der konkret sagt (und vorschreibt), was richtig und falsch ist (wie sollen sich das eine Geschlecht kleiden)
            (von 1. nach 2.: weil die Wissenschaft die Ursache des Urknall nicht verstehen kann, muss diese Hexe hier jetzt brennen)
    * Spiritualität vs. Religion
        * Der Weg der großen Fragen vs. Konkrete Antworten / Vorschriften
    * ...
    * unique times: today, great philosophical questions (should the owner of the car die or two little kids?)
        must be tackled by engineers who build the self-driving car and the answer WILL have real consequences
    * ...
    * modern society needs/is built on (large) corporations (= a fiction created by the sharmans called layers)
    * ...
    * nations, cooperations, banks: stories/fiction humans have created
        * humans are real
        * stories are not
        * stories were created to serve human needs (and not vice versa)
    * ...
    * problem fake news is created by the architecture / model of the news market
        * you get exciting news for free in exchange for human attention
            * excitement and attention; more competitors for attention; no room for truth
        * Lösung: irgendwie high quality news mit Geld zu verbinden
    * Q: How to best solve global problems?
        * A: most important thing is "to change to public conversation and focus it on the global problems"
            * "if people focus on local problems they don't see the need for effective global cooperation"
            * repeat the three biggest problems that everybody is facing (nucelar war, climate change and technological disruption)
    * ...
    * **compassion is important**
        * our ability to shape ourselves (manipulate humans) is rising (früher: education etc.); heute: technology
            * => Optimierung auf Effektivität, Intelligenz und schnelle Entscheidungen (workers, soldiers etc.)
                (you don't have one day, you have to decide in half an hour)
                Gefahr, dass compassion das ist, was dafür wegfällt (weil Unternehmen und Regierungen das für irrelevant oder sogar problematisch halten)
        * sich selber kennenlernen (z. B. Wut und Ärger über andere schadet einem selber mehr => compassion gegenüber anderen hilft auch einem selber)
    * meditation: helps to focus on really important things
        * don't grab things

Philosophie der Nachhaltigkeit
------------------------------
* siehe "Philosophie der Nachhaltigkeit - Es ist nicht egal, was du tust"

Natur als Wert an sich
----------------------
Stichworte: Naturschutz, Living Planet View, ganzheitlich, ..., ::natur

* siehe Eisenstein
* Vorteile:
    * "02.04.2020 Schulze: Weltweiter Naturschutz kann Risiko künftiger Seuchen verringern"
    * ...

### Vorreiterunternehmen
* "Power aus der Sojabohne - Firma Taifun in Freiburg", 2019, https://www.ardmediathek.de/ard/player/Y3JpZDovL3N3ci5kZS9hZXgvbzExNTM1MDg/power-aus-der-sojabohne-firma-taifun-in-freiburg, 30 min
    * ...

Verantwortung
-------------
* siehe wirksamkeit-des-einzelnen.md / Die Verantwortlichkeit der Intellektuellen

Menschenrechte
--------------
* siehe Humanismus
* siehe menschenrechte.md
* siehe demokratie.md
* siehe wirksamkeit-des-einzelnen.md

Lebensrechte anderer Lebenwesen
-------------------------------
### Vegan
* siehe dort
* nicht vergessen: "soweit praktisch möglich" (in der industrialisierten Welt ist sehr viel praktisch möglich)

### Eine neue christliche Tierethik
* siehe dort

Freie Software
--------------
* siehe dort

Humanismus
----------
* https://de.wikipedia.org/wiki/Humanismus

### Veganer Humanismus
* ["Manifest des Veganen Humanismus"](https://www.fink.de/katalog/titel/978-3-7705-5989-3.html), 2015
    * "Was ist für uns ein Übel? Mit Sicherheit alles, was wir den Tieren antun: sie erzwungen hilflos aufziehen, sie schmerzvoll transportieren, sie in Experimenten quälen, sie in Todesangst nicht selten betäubungslos töten. Was ist das Böse? Das Übel, das Anderen angetan wird. Unser Umgang mit den Anderen bildet im Fall der Tiere ein verwerflich böses Verhalten."
    * "Was war bisher Humanismus? Unbedingter Vorrang menschlicher Interessen, einschließlich der Nutzung und Vernutzung der Tiere."
    * "Was soll Veganer Humanismus sein? Ein für Menschen und Tiere gerechtes und wohltätiges Ende aller Tiernutzung. Anstatt den Tieren unwirksam Rechte zu verleihen, läuft Veganer Humanismus auf eine ebenso erfüllbare wie unbedingte Pflicht hinaus, die Tiere aus aller Nutzung durch die Menschen zu entlassen."
    * Rezeption:
        * ["Vom anthropozentrischen zum veganen Humanismus"](https://hpd.de/artikel/12214), 2015
            * siehe User-Comments
                * ... todo

* ["Humanismus und Veganismus"](https://hcri.de/veganismus), 2014
    * "Welche Gemeinsamkeiten kann es zwischen Humanismus und Veganismus geben?
        In manchen humanistischen Strömungen steckt latent noch immer jener Speziesismus, der den Menschen in den
        Mittelpunkt stellt, ihn nicht aber als Teil der gesamten Lebenswelt betrachtet."
    * ...
    * Thesen
        * "Empathie und Kooperation fördern das Zusammenleben"
            * ...
        * "Fähigkeiten wollen ausgeübt werden"
            * ...
        * "Ernstnehmen der Fähigkeiten und Bedürfnisse aller"
            * ...
        * "Zu einem anthropokosmischen Paradigma"
            * ...
        * "Zukunft sicherstellen"
            * ...
        * "Ökologische Verantwortung"
            * ...
        * "Gemeinsame Motive"
            * ...
        * "Ein »Humanismus 2.0«"
            * ...
        * "Vegane Kommunikation erforschen"
            * ...

* todo: andere Philosophen fragen

### Veganismus als neue Aufklärungsbewegung
* ["Verleihung des Peter-Singer-Preises 2018 - Veganismus als neue Aufklärungsbewegung"](https://hpd.de/artikel/veganismus-neue-aufklaerungsbewegung-15690), 2018
    * "Peter-Singer-Preis für Strategien zur Tierleidminderung"
    * "Preisträger ist der Australier Philip Wollen, der vom Spitzenbanker zum Tierrechtsaktivisten wurde und weltweit zahlreiche wohltätige Projekte initiierte"
        * yt: "Philip Wollen : Animals Should Be Off The Menu debate | Subtitles in 18 languages", 2012
        * Video: ["Phillip Wollen Speech - Why CHANGE is Imperative *GRAPHIC TRUTH*"](https://www.youtube.com/watch?v=iRdOCaCKGXY), 2018, 13 min, engl.
            * "Former investment banker turned activist drops some truth as to why we now need to consider some of our unconscious choices."
            * ...
            * Zahlen: "If we kill at the same rate we would be wiped out in 12 days"
            * ...
            * "What made me decide to leave the world of lobsters and lear jets"
            * ...
            * 5:15: "Die Analphabeten des 21. Jahrhunderts werden nicht die sein, die nicht lesen und schreiben können, sondern, die, die nicht lernen, verlernen und neu lernen können."
            * Ahimsa
                * https://de.wikipedia.org/wiki/Ahimsa
                    * "Gewaltlosigkeit – eines der wichtigsten Prinzipien im Hinduismus, Jainismus und Buddhismus. Es handelt sich um eine Verhaltensregel, die das Töten oder Verletzen von **Lebewesen** untersagt bzw. auf ein unumgängliches Minimum beschränkt."
                * im Denken, Reden und Handeln
            * ...
    * Singer: "fordert, dass die Interessen nicht-menschlicher ebenso wie jene menschlicher Tiere Berücksichtigung finden müssen."
    * "Wollen lebte als erfolgreicher Spitzenbanker und hatte es bis zum Vizepräsidenten der Citibank gebracht, bis er nach einem Schlachthofbesuch an seinem 40. Geburtstag zum Vegetarier und später zum Veganer wurde und sich entschloss, sein gesamtes Vermögen zugunsten wohltätiger Zwecke einzusetzen."
    * "einsetzen für die Verminderung von Tierleid sowie für die Umwelt, Kinder, Jugendliche und Todkranke"
    * Stefan Bernhard Eck
        * siehe denkweise.md
    * Ben Moore
        * "10 % als kritische Masse" --> siehe denkweise.md
    * "Wenn es um Leiden gehe, so Singer, spiele die Spezies des Leidenden keine Rolle."
        * "Dass dies von weiten Teilen der Menschheit heute noch immer anders gesehen werde, sei, so betonte Singer, eines der wichtigsten und am stärksten vernachlässigten moralischen Probleme seit dem Ende der Sklaverei."
    * " vor allem Projekte fördere, die **möglichst effektiv sowohl Tieren als auch Menschen helfen.** "
    * "Abwandlung des berühmten Zitats von Victor Hugo, dass nichts so machvoll ist, wie eine Idee, deren Zeit gekommen ist, dass **nichts so zerstörerisch sei, wie eine Idee, deren Zeit vergangen sei.** "
        * "Denn, so Wollen, Fleischverzehr und Massentierhaltung bedeuteten nicht nur für unzählige Tier-Individuen entsetzliches Leid, sie hätten auch katastrophale Folgen für die Umwelt, für Menschen und Volkswirtschaften."
    * "Veganer, die sich all jener Probleme bewusst seien und versuchten, auf einen gesellschaftlichen Fleischverzicht hinzuwirken, betrachtet Wollen daher als Vorkämpfer einer neuen, überaus notwendigen Aufklärungs-Bewegung."
    * User-Comments
        * These: "Warum wird hier immer eine **künstliche Grenzlinie zwischen tierischem und pflanzlichem leben** gezogen?"
            * Antwort:
                * "Sie haben recht, die Grenzziehung ist willkürlich. Allerdings: wer nicht sieht bzw. nicht sehen will, dass die Grenzziehung zwischen Mensch und Tier ebenso willkürlich aber obendrein viel absurder ist als die zwischen Tieren und Pflanzen, der ist noch immer auf dem "Der Mensch ist die Krone der Schöpfung"-Trip."
                * "Tiere, vor allem Säugetiere (die ja den größten Teil unserer sogenannnten Nutztiere ausmachen) sind dem Säugetier Mensch viel näher als Pflanzen selbst den niedrigsten Tieren."
                * "Wir können nicht existieren ohne irgendeinen biologischen Schaden zu verursachen. Das ist Fakt. Wer jedoch aus der Tatsache, dass wir uns zumindest von Pflanzen ernähren müssen, den Schluß zieht, dass man dann ja auch gleich Tiere essen kann, der hat leider nichts von dieser ganzen Thematik begriffen."
                    * "Die Erzeugung von Tiernahrung verursacht nämlich ein Vielfaches an Pflanzenleid ! (Wie eigentlich allgemein bekannt ist.)"
                    * "Dass die Leute immer dann ihr Herz für Pflanzen entdecken, wenn es um ihren eigenen Fleischkonsum geht. Wow, wie empathisch !(Das Tierleid ist ihnen dann aber immer zweitrangig. Wie erbärmlich !)"
        * https://graslutscher.de/ich-lass-mir-doch-nicht-das-rauchen-mit-kindern-im-auto-verbieten/
        * These: "Jeder weiß um die Umstände und Folgen der Massentierhaltung und die meist unschönen Zustände in den Schlachthöfen. Das hieraus aber direkt die moralische Pflicht zum Veganismus folgen soll, ist absurd."
            * Antwort:
                * "[...] Und ich glaube nicht, daß jedem Einzelnen klar ist, was alles getan wird, damit wir Milch oder Ei auf dem Frühstückstisch haben. Da fehlt in jedem Fall die Aufklärung."
        * These: "Es gibt Millionen von Tierarten, **die allermeisten sind dazu da, von anderen gefressen zu werden** "
            * Antwort:
                * "Dies stimmt so sicher nicht. Evolutionäre Prozesse verlaufen nicht zielgerichtet um irgendetwas hervorzubringen oder gar zu produzieren.
                * "Informierte Menschen können sich allerdings entscheiden zwischen in Schlachtereien ‚unschön produzierter’ Nahrung und [...] Kost, für deren Herstellung kein Blut fließen muss."
        * Müller:
            * "muss ich doch meine Verwunderung über die missionarischen Einstellungen der Artikelschreiberin und der meisten Kommentatoren ausdrücken"
            * "mehrheitlich rationale Menschen sind, was ich häufig von religiösen, esoterischen und vegan lebenden Leuten nicht sagen kann"
            * Angst: "ist es eines Tages wohl so, dass ich die Schnake, die mich vor dem Einschlafen um die Nachtruhe bringt, nicht mehr mit der elektrischen Fliegenklatsche "erledigen" darf, sondern sie vorsichtig fangen muss um sie dann in die Freiheit zu entlassen."
                * Antwort:
                    * "Anscheinend sind Ihnen Begründung, Zweck und **Funktionsweise der Ethik** noch nicht recht klar. Wenn Sie nicht vermeidbarerweise leiden und berechtigt sein wollen, eigene und fremde Interessenverletzungen zu beanstanden, müssen Sie sich natürlich auch Ihrerseits bestmöglich leidvermeidend verhalten, und zwar nicht nur Menschen, sondern ALLEN empfindungsfähigen Wesen gegenüber"
                    * " **Statt sich von abwegigen Zukunftsvisionen blockieren zu lassen**, beginnen Sie doch einfach an irgendeinem Ihnen naheliegenden Punkt damit, sich der direkten und indirekten Folgen Ihres Handelns und Unterlassens, sowie Ihrer systemische Mitverantwortung für die verschiedensten Mißstände auf der Erde bewußt zu werden."
                    * " **Als Folge des Nachdenkens darüber**, wie Sie Ihren Anteil an ihnen minimieren können, **sollte sich Ihnen im Laufe der Zeit die Notwendigkeit etlicher Verhaltenskorrekturen aufdrängen**. **Eine davon wird (hoffentlich eher früher als später) die Entscheidung sein, den Konsum tierlicher Produkte einzustellen.** "
            * "Veganismus ist eine weitere Religion auf diesem Planeten. Es wird nicht die Letzte sein die der Mensch hervorbringt um sich besser zu fühlen."

### Portale
* [Humanistischer Pressedienst](https://hpd.de)
* http://who-is-hu.de

### Für Helfer / "Gutmenschen"
* ["Gutmensch trifft Flüchtling"](https://hpd.de/artikel/gutmensch-trifft-fluechtling-14941), 2017
    * SEHR LESENSWERT
    * "Was passiert, wenn sogenannte "Gutmenschen" mit Flüchtlingen arbeiten und die Dinge nicht so glatt laufen wie erhofft?"
    * "Ich erinnere mich an die Zeit der Flüchtlingswelle im Jugoslawienkrieg. Jemand aus unserer damaligen Gruppe meinte, die Arbeit mit den Asylsuchenden bringe einen manchmal in Gefahr, ausländerfeindlich zu werden."
    * "Entidealisierung der Flüchtlinge"
        * "Stellen Sie sich vor, Sie und Ihre Nachbarn müssten wegen Bürgerkrieg, Hunger, Terror fliehen. Da wäre die hilfsbereite Familie Meier, die allerdings entsetzlich fromm ist. Dann der rechthaberische Herr Müller, [...]"
    * "Opfer sein ist kein charakterliches Qualitätsmerkmal."
    * "Flüchtlingsarbeit ist ein Geschäft auf Gegenseitigkeit. Aber nicht in dem Sinn: Ich gebe meinen gesammelten Idealismus und dafür erwarte ich, dass meine bürgerlich-mitteleuropäischen Erwartungen hinsichtlich Dankbarkeit und Wohlverhalten erfüllt werden, sonst bin ich beleidigt."
        * "Sondern: Ich gebe ein begrenztes Engagement, begrenzte Zeit und erwarte, dass die andere Seite sich an Absprachen hält und ihren Teil beiträgt."
        * ...
        * "Keine Erwartungen an Flüchtlinge zu haben, alles zu verstehen, alles zu akzeptieren, führt zu nichts außer zu Frust bei den Helfern."
        * "Auf Selbstüberforderung und Selbstüberschätzung folgt Zynismus. Wenn man dann auf dem harten Boden der Realität landet, wird das zu Unrecht den unwilligen, unfähigen Flüchtlingen angelastet."

Geschichte
----------
* siehe geschichte.md
