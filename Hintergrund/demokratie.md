Demokratie
==========

<!-- toc -->

- [Allgemein](#allgemein)
- [Persönliches Engagement für eine bessere Welt](#personliches-engagement-fur-eine-bessere-welt)
  * [10 Dinge](#10-dinge)
  * [Kausalität / Die Nachfrage bestimmt das Angebot](#kausalitat--die-nachfrage-bestimmt-das-angebot)
  * [Pessimisten](#pessimisten)
  * [Zuviele Baustellen?](#zuviele-baustellen)
  * [Zitate für mehr Engagement](#zitate-fur-mehr-engagement)
  * [Verantwortung](#verantwortung)
- [Elemente](#elemente)
  * [Meinungsfreiheit - so gut reagiert Merkel](#meinungsfreiheit---so-gut-reagiert-merkel)
  * [Mündigkeit](#mundigkeit)
  * [Solidarität](#solidaritat)
- [Was gefährdet die Demokratie?](#was-gefahrdet-die-demokratie)
  * [Populismus vs. Demagogie](#populismus-vs-demagogie)
- [Wählen gehen / Wahl](#wahlen-gehen--wahl)
  * [Das Wahlrecht als ein hart erkämpftes Gut](#das-wahlrecht-als-ein-hart-erkampftes-gut)
  * [Kleinpartei / Splitterpartei](#kleinpartei--splitterpartei)
  * [Bausteine: Freie Presse](#bausteine-freie-presse)
  * [Bewusstsein](#bewusstsein)
- [Baustein: Ziviler Ungehorsam](#baustein-ziviler-ungehorsam)
  * [2019](#2019)
  * [Allgemein](#allgemein-1)
  * [Einwände](#einwande)
- [Baustein: Staatsgewalt](#baustein-staatsgewalt)
- [Die Stützen der Gesellschaft](#die-stutzen-der-gesellschaft)
  * [Einleitung](#einleitung)
  * [Auf Reisen](#auf-reisen)
  * [Firmen: Vorbild Frosch beim Thema Müll](#firmen-vorbild-frosch-beim-thema-mull)
  * [Probleme mit digitalen Angeboten](#probleme-mit-digitalen-angeboten)
  * [Kantinen](#kantinen)
- [Krieg und Frieden](#krieg-und-frieden)
  * [Kleinwaffen, Kriegseinsätze](#kleinwaffen-kriegseinsatze)
  * [Waffenexporte, Rüstungsexporte](#waffenexporte-rustungsexporte)
- [Menschenrechte](#menschenrechte)
- [Politik von unten](#politik-von-unten)
- [Diskussion](#diskussion)
  * [Konservativ heißt...](#konservativ-heisst)
  * [Sorgen der Mittelschicht aus der Nachkriegsgeneration](#sorgen-der-mittelschicht-aus-der-nachkriegsgeneration)
- [Frieden](#frieden)
  * [Galtung](#galtung)
  * [Theodor Ebert](#theodor-ebert)
  * [Inbox](#inbox)
- [Inbox](#inbox-1)
  * [2020 - Rhetorik in Haushaltsdebatte - Merkels Antwort](#2020---rhetorik-in-haushaltsdebatte---merkels-antwort)
  * [2020 - Wahrheit](#2020---wahrheit)
  * [2019](#2019-1)

<!-- tocstop -->

Allgemein
---------
* ...

* [Wer in der Demokratie schläft...](http://www.huffingtonpost.de/tobias-afsali/wer-in-der-demokratie-schlaeft-geht-waehlen_b_9381806.html), 2016
    * "Trotz einer extremen Leistungsverdichtung in der Schule, der Lehre oder an der Uni engagieren sich Jugendliche gerne ehrenamtlich und wenden ihre kostbare Freizeit für diese wichtige Arbeit auf."

* [Digitalcourage e.V.](https://digitalcourage.de)
    * [BigBrotherAwards](https://bigbrotherawards.de)
        * [Beispiel 2017](https://vimeo.com/216301925), z. B. erste paar Minuten (Wege von Außendienstmitarbeitern lückenlos überwachen)

* [Digitale Gesellschaft e. V.](https://digitalegesellschaft.de)

* [Prof. Rainer Mausfeld: Die Angst der Machteliten vor dem Volk](https://www.youtube.com/watch?v=Rk6I9gXwack), 2016
    * gefunden auf: https://kenfm.de/kenfm-zeigt-zensur/
        * gefunden auf: 2017: https://de.wikipedia.org/wiki/Benutzer:Fonero
            * https://de.wikipedia.org/wiki/Wie_viel_Bank_braucht_der_Mensch%3F_Raus_aus_der_verr%C3%BCckten_Finanzwelt
            * https://de.wikipedia.org/wiki/Benutzer:Fonero#Landwirtschaft
            * https://de.wikipedia.org/wiki/Benutzer:Fonero#Lebensmittel
            * https://de.wikipedia.org/wiki/Benutzer:Fonero#Chemikalien
            * https://de.wikipedia.org/wiki/Benutzer:Fonero#Dokumentarfilme
            * https://de.wikipedia.org/wiki/Benutzer:Fonero#B%C3%BCcher
                * Die Pestizid-Lüge – Wie die Industrie die Gesundheit unserer Kinder aufs Spiel setzt (2018), siehe Bio/
            * https://de.wikipedia.org/wiki/Benutzer:Fonero#Werbung
                * Es geht auch bewusst ohne Palmöl: https://www.youtube.com/watch?v=ANMoTfG8ogY, 40 sek
    * "Prof. Rainer Mausfeld ist eine Koryphäe, wenn es um das Enttarnen von Eliten-Macht geht. Sein aktueller Vortrag „Die Angst der Machteliten vor dem Volk“ hilft dem einzelnen, die Ohnmacht zu überwinden, die jeden beschleicht, wenn er versucht, im Alleingang das System zu durchschauen. Die Chance auf Veränderung beginnt im Kopf. Wissen ist der Schlüssel. Mehr Wissen bei den Massen."
    *   "Die Metapher von den Schafen und dem Hirten: Die Beziehung zur Demokratie von unten und von oben betrachtet"
    * Demokratie-Begriff wird oft auf Wahlen reduziert; siehe aber auch nächster Abschnitt
    * Kritik an Psychologen, die ihr Wissen dazu verwenden, um Soft-Power-Techniken zu entwickeln, mit denen unwissende Menschen manipuliert werden
    *   "Eliten und ihre Herrschaftstechniken: Psychische und physische Machtausübung - Basisideologie von Soft-power als Rechtfertigungsideologie"
    *   "Volk und Elite: Warum das Volk zum eigenen Wohle belogen werden soll"
    *   "Affektive und kognitive Mentalvergiftung, um Kritik am Zentrum der Macht zu verunmöglichen - Falschwörter und Denuziationsbegriffe"
    *   "Meinungsmanipulation: Aktualindoktrination - Propaganda in den Medien. Tiefenindoktrination - Schulsysteme im Sinne der Herrschaftsideologie"
    * TODO: weiter ab 60 Minuten
    * http://de.wikimannia.org/Moralische_%C3%84quivalenz
        * https://de.wikipedia.org/wiki/Amerikanischer_Exzeptionalismus#cite_note-15
    * https://de.wikipedia.org/wiki/Rainer_Mausfeld
        * Kritik an [Weißen Folter](https://de.wikipedia.org/wiki/Wei%C3%9Fe_Folter)

Persönliches Engagement für eine bessere Welt
---------------------------------------------
### 10 Dinge
![](img/IMG_201608-ZEIT-1.jpg)
![](img/IMG_201608-ZEIT-2.jpg)
![](img/IMG_201608-ZEIT-3.jpg)

todo

* Tipp: nicht nur wählen gehen
* Veränderungen vorleben
* siehe auch "Philosophie der Nachhaltigkeit - Es ist nicht egal, was du tust"

* Weiteres:
    * 1x/Jahr den eigenen Abgeordneten anschreiben

### Kausalität / Die Nachfrage bestimmt das Angebot
Beispiele, wo bei Umweltschutzthemen erst die Nachfrage immer mehr zunahm und dann das Angebot nachzog.

* 2018: Bio-Nachfrage => Leute kaufen immer mehr bio, auch überregional
    => Jetzt fangen die Politiker an zu überlegen wie man den regionalen Erzeugern helfen kann,
    auf Bio zuzustellen, damit der Bürger auch verstärkt regional bio einkaufen kann

### Pessimisten
* Veränderungen zum besseren erreicht man, indem man Problemfelder erkennt und benennt und dann konkrete und positive Lösungswege aufzeigt und selber lebt
* Dass es immer Menschen gibt, die einerseits die erkannten Probleme nicht wahr haben wollen (denial) und andererseits an den Lösungen etwas auszusetzen haben (pessimistic), ist klar.
* Wer aufgibt, kann schon von vornerein nichts erreichen.

### Zuviele Baustellen?
* Je nach Alter, Zeit und Lebensumständen z. B. jedes Jahr _eine_ neue Sache in Angriff nehmen.
* Es gibt wenige gebildete und gesunde Bürger, die _nie_ für ihre Gesellschaft Zeit haben, wenn sie ehrlich sind.

### Zitate für mehr Engagement
* "Wenn du denkst, wir könnten die Welt nicht verändern, bedeutet das nur, dass du keiner von denen bist, die es tun werden." -- Jacque Fresco
    * https://de.wikipedia.org/wiki/Jacque_Fresco
    * https://en.wikiquote.org/wiki/Jacque_Fresco
* [Paul Watson](https://sputniknews.com/voiceofrussia/2012_07_15/Paul-Watson-People-should-have-a-license-to-have-children/)
    * "I think that everybody has the capacity to make a difference. Just look at the virtues of courage, imagination, and passion. Passion is the most important, but what people really need to do is look at their own talents and abilities and put those to work to make us a better world, whether you’re a writer, or a journalist, or a lawyer, a teacher, an artist. Just make a better contribution in your own world to make us a better world."
* "Einen guten Konsumenten erkennt man daran, was er wählt, wenn er für die Wahl nicht persönlich zur Verantwortung gezogen wird" (frei nach ?)
* "Wenn jeder an sich denkt, ist an alle gedacht", oder?

### Verantwortung
* siehe auch wirksamkeit-des-einzelnen.md / Die Verantwortlichkeit der Intellektuellen
* Doku: ["Essen retten mit "kreuz und quer" - orf"](https://www.youtube.com/watch?v=-zIRTeIEjqA), 2014, gegen Lebensmittelverschwendung, Food-Sharing
    * ...
    * 32:50: Franz Küberl, Caritas Graz-Seckau: "Die Freiheit für den Einzelnen ist rascher gewachsen als die Fähigkeit des Einzelnen diese Fähigkeit auch mit Verantwortung auszufüllen. Wenn es Freiheit gibt, gibt es auch mehr Verantwortung für mich, mich gut einzuklingen; selbst beginnen solidarisch zu sein; und nicht nur auf eine Anordnung von oben zu warten, dass ich jetzt solidarisch sein müsste."
    * Bio-Werkstatt
        * seit 2017 geschlossen: https://www.facebook.com/pg/biowerkstatt.wien/about/
        * Stand 2019:
            * Rohgenuss: http://www.rohgenuss.at/i-tell-you-a-raw-story/
            * Iss mich! Bio-Catering: https://www.issmich.at/
    * ...

Elemente
--------
### Meinungsfreiheit - so gut reagiert Merkel
* "Gut gekontert: So cool erklärt Merkel einem AfD-Politiker Meinungsfreiheit – Video", 2019, 6 min
    * https://www.volksverpetzer.de/social-media/merkel-konter/
    * ruhig und souverän

### Mündigkeit
* https://digitalcourage.de/digitale-selbstverteidigung/digitale-muendigkeit
    * "Digitale Mündigkeit heißt nicht, immer perfekt zu sein"
* https://de.wikipedia.org/wiki/M%C3%BCndigkeit_(Philosophie)
    * "beschreibt das innere und äußere Vermögen zur Selbstbestimmung und Eigenverantwortung"
    * "Mündigkeit ist ein Zustand der Unabhängigkeit"
    * "Sie besagt, dass man für sich selbst sprechen und sorgen kann"
    * siehe auch Emanzipation
    * Kant:
        * "Aufklärung ist der Ausgang des Menschen aus seiner selbst verschuldeten Unmündigkeit. Unmündigkeit ist das Unvermögen, sich seines Verstandes ohne Leitung eines anderen zu bedienen. Selbstverschuldet ist diese Unmündigkeit, wenn die Ursache derselben nicht am Mangel des Verstandes, sondern der Entschließung und des Mutes liegt, sich seiner ohne Leitung eines anderen zu bedienen"

### Solidarität
* siehe z. B. https://www.unteilbar.org
    * https://www.unteilbar.org/sogehtsolidarisch/
    * https://www.ende-gelaende.org/news/aufruf-unteilbar-durch-die-krise/

Was gefährdet die Demokratie?
-----------------------------
* Gleichgültigkeit
* Gefühlte Ohnmacht des Einzelnen
* Verletzung der Menschenrechte
* ...

### Populismus vs. Demagogie
* TODO

Wählen gehen / Wahl
-------------------
### Das Wahlrecht als ein hart erkämpftes Gut
* http://www.bpb.de/apuz/255954/waehlen-gehen
    * "Die einfachste Möglichkeit, sich demokratisch einzubringen, ist wählen zu gehen. So vertraut das klingt, so wenig sind allgemeine, unmittelbare, freie, gleiche und geheime Wahlen eine Selbstverständlichkeit: Unser Wahlrecht ist ein hart erkämpftes Gut, dessen Wert manchmal unterschätzt wird."

### Kleinpartei / Splitterpartei
* ["Warum es sich lohnt, Kleinparteien und Einzelkandidaten zu wählen"](http://mnementh.blogsport.de/2009/09/23/warum-es-sich-lohnt-kleinparteien-und-einzelkandidaten-zu-waehlen), 2009
* ["Die Mär von den 'verlorenen Stimmen'"](http://www.heise.de/tp/artikel/31/31189/1.html), 2009
    * "Wie ein Votum für kleine Parteien und unabhängige Kandidaten den politischen Willensbildungsprozess beeinflusst"
    * "rationalere Alternative zum Wahlboykott"
* aufgelöst: http://nein-idee.de/nicht-wahlen-bringt-nichts, 2012

### Bausteine: Freie Presse
* Film: [Spotlight (2015)](https://www.themoviedb.org/movie/314365-spotlight), 2015, "The true story of how the Boston Globe uncovered the massive scandal of child molestation and cover-up within the local Catholic Archdiocese, shaking the entire Catholic Church to its core."
    * "They knew and they let it happen! It could've been you, it could've been me, it could've been any of us."

* siehe medien.md

### Bewusstsein
* siehe diskriminierung.md / "injustice anywhere"
* siehe wirksamkeit-des-einzelnen.md / "You assist an evil system most effectively"

Baustein: Ziviler Ungehorsam
----------------------------
### 2019
* "Philosoph zu „Extinction Rebellion“ - Robin Celikates: Klimaprotest ist „nicht antidemokratisch“", 2019
    * https://www.deutschlandfunk.de/philosoph-zu-extinction-rebellion-robin-celikates.911.de.html?dram:article_id=461210
    * ...

* "Medienwissenschaftlerin über Protest - Ziviler Ungehorsam gehört zur Demokratie", 2019
    * https://www.deutschlandfunknova.de/beitrag/protest-ziviler-ungehorsam-gehoert-zur-demokratie
    * "Ziviler Ungehorsam bewegt sich legal im demokratischen Rahmen und kann sie sogar beleben, meint Medienwissenschaftlerin Theresa Züger"
    * Humboldt Institut für Internet und Gesellschaft
    * "Ziel zivilen Ungehorsams: Gesetze und politische Maßnahmen verändern"
    * "Wenn man sich die **historischen Beispiele** des zivilen Ungehorsams anguckt, kann man erstmal festhalten,
        dass er große Erfolge für die Demokratien verbuchen konnte und für mehr Gerechtigkeit in den Gesellschaften gesorgt hat."
        * Martin Luther King: "I think we all have a moral obligation to obey just laws and to disobey unjust laws, because noncooporation with evil is as much a moral obligation as is cooperation with good."
    * mit Sanktionen rechnen
        * "Natürlich haben wir Angst. Wir sind ganz normale Menschen und viele von uns waren davor nie politisch aktiv, aber wir denken halt: Ok, es muss was gemacht werden. Uns läuft die Zeit davon." (Hannah Elshorst, Sprecherin von "Extinction Rebellion" in Deutschland)
    * "eentscheidende Abgrenzung zwischen zivilem Ungehorsam und Randale:
        Ziviler Ungehorsam ist inklusiv, will möglichst alle Bürger in den Dialog miteinbeziehen"

### Allgemein
* [Ziviler Ungehorsam](https://de.wikipedia.org/wiki/Ziviler_Ungehorsam)

### Einwände
* "Man muss doch die Regeln einhalten": ja, aber was, wenn uns die Regeln kollektiv gegen die Wand fahren?
    (und auch im Jetzt schreiende Ungerechtigkeiten erzeugen?)
    * Der Einwand basiert möglicherweise darauf, dass
        1) kein Bewusstsein dafür herrscht, dass wir nicht nachhaltig und ungerecht wirtschaften
        2) dass Ungerechtigkeit schon ok ist / Recht des Stärkeren
        3) dass es ok ist, wenn man im Hier und Jetzt die Möglichkeiten von zukünftigen Menschen einschränkt
        4) ein sehr starkes Rechtsbewusstsein und Sinn für persönliche Freiheit (ggf. ohne Rücksicht auf die Freiheit anderer)
        5) Angst vor Chaos

Baustein: Staatsgewalt
----------------------
* "POL1Z1STENS0HN a.k.a. Jan Böhmermann - Ich hab Polizei (Official Video) | NEO MAGAZIN ROYALE ZDFneo"

Die Stützen der Gesellschaft
----------------------------
### Einleitung
Die Stützen der Gesellschaft sind gut situierte Personen und Organisationen, die über eine überdurchschnittliche Fähigkeit der Informationsbeschaffung und -auswertung verfügen.

Diese Gruppe ist wie keine andere in der Lage nachhaltigen/ethischen Konsum vorzuleben und in ihrem Umfeld zu erleichtern. Diese aktive Vorbildfunktion hat Auswirkungen auf den Rest der Gesellschaft inklusive anderer Länder, die sich an unserem Lebensstil ein Beispiel nehmen.

Sammlung von Beispielen, bei denen es um Aspekte ethischer Prinzipien bei den eigenen Handlungen geht, die etwas mit Konsum zu tun haben.

### Auf Reisen
Positive Beispiele: auf Reisen

* Deutsche Bahn
    * 2018: ["Deutsche Bahn mit neuem Vegan-Angebot"](https://albert-schweitzer-stiftung.de/aktuell/deutsche-bahn-neues-vegan-angebot)

### Firmen: Vorbild Frosch beim Thema Müll
* Video ["Im Zeichen des Froschs Mainzer Firmen recyceln Plastik-Verpackungen"](https://www.swr.de/natuerlich/nachhaltige-reinigungsmittel/-/id=100810/did=19670660/nid=100810/bjgbx0/index.html), SWR, 2017, 6 min

### Probleme mit digitalen Angeboten
* todo: Beispiele von Krankenkassen, Banken und anderen öffentlichen Einrichtungen, die bestimmte Angebote verstärkt via Google oder Apple anbieten und damit zur Verletzung der [Privatsphäre](privatsphaere.md) der Bevölkerung beitragen.
* ...

### Kantinen
* siehe [kantine](kantine.md)

Krieg und Frieden
-----------------
::frieden

### Kleinwaffen, Kriegseinsätze
Wirtschaftsinteressen sind wichtig, sollten aber nicht über allem stehen.

* Warum sind Geschäfte mit Waffen zu vermeiden?
    * Artikel: [Kleinwaffen - die wahren Massenvernichtungswaffen](http://sicherheitspolitik.bpb.de/konventionelle-waffen/hintergrundtexte-m5/kleinwaffen-die-wahren-massenvernichtungswaffen) (Bundeszentrale für politische Bildung, 2011)

* [Deutsche Rüstungsexporte und Kindersoldaten - Kleinwaffen in Kinderhänden](http://www.kindersoldaten.info/kindersoldaten_mm/downloads/Publikationen/Studie_+Kleinwaffen+in+Kinderh%C3%A4nden/Studie_+Kleinwaffen+in+Kinderh%C3%A4nden.pdf), 2017, Studie im Auftrag von tdh und anderen
    * alles spricht gegen Export von Kleinwaffen in Dauerkonfliktländer mit Kindersoldaten
    * die Bundesregierung genehmigt die Ausfuhren trotzdem
    * siehe auch https://www.tdh.de/kleinwaffen

* Warum ist die Weiterentwicklung von modernem Kriegsgerät zu vermeiden?
    * Meldung: [Trierer Bischof: Bewaffnete Drohnen senken Gewalt-Schwelle](https://daserste.ndr.de/panorama/aktuell/drohnen115.html) (24.09.12)
    * Sachbuch: [J. Weizenbaum - Kurs auf den Eisberg](http://www.zvab.com/advancedSearch.do?title=%22Kurs+auf+den+Eisberg%22) (1988) über die Verantwortung des Einzelnen
    * Meldung: [Human Rights Watch fordert Komplettverbot von Killerrobotern](http://www.heise.de/newsticker/meldung/Human-Rights-Watch-fordert-Komplettverbot-von-Killerrobotern-2633796.html) (2015)
    * Unbemannte Drohnen: Filme, die zeigen wie es solchen Drohenpiloten geht
        * Doku: National Bird (2016)
            * junge Frau, die Image-Analystin war
                * u. a. den ganzen Tag Ziele und nach dem Schuss Leichenteile identifizieren
                * Papa merkt an wie schlimm es ist zu sehen, dass seine Tochter keine adäquate Behandlung bekommt
            * Airforce-Recruitement-Videos
            * Einordnung der offiziellen Aussage, dass man das beste versucht, keine Zivilisten zu erwischen
            * Einordnung der Politik-Aussage, mit Drohnen präzise, quasi chirurgische Eingriffe machen zu können
            * makes war easy and convenient
            * Papierkram einer Hausdurchsuchung von einem, der beim Film etwas erzählt hat
            * Interviews mit zivilen Opfern in Afghanistan, u. a. Junge mit Beinprothese
            * Blog-Eintrag zum Film 2017: https://events.ccc.de/2017/05/20/film-uber-den-geheimen-drohnenkrieg-national-bird/
            * Deutschlandfunk-Artikel 2016: http://www.deutschlandfunkkultur.de/dokumentarfilm-ueber-drohnenkrieg-bei-manchen-ist-es-so.2168.de.html?dram:article_id=345927
        * Spielfilm: Good Kill – Tod aus der Luft (2014)
            * "Major Thomas Egan ist Pilot der U.S. Air Force und steuert von einer Basis in der Nähe von Las Vegas Drohnen überall auf der Welt. Seine Kollegen bezeichnen ihre Arbeit als den besten Job der Welt, weil sie tagsüber Krieg führen und abends bei ihren Frauen zu Hause Bier trinken und grillen können." (https://de.wikipedia.org/wiki/Good_Kill_%E2%80%93_Tod_aus_der_Luft)
            * "Als er bei einem Drohneneinsatz im Jemen Kinder tötet, äußert er Zweifel an den Einsätzen gegenüber seiner neuen Copilotin Vera Suarez."
    * Film über Auswirkungen von Waffenexporten
        * Film: Meister des Todes (2015), Spielfilm
            * ["Deutsche Waffen tauchen in vielen Krisengebieten der Welt auf, entgegen den angeblich restriktiven staatlichen Kontrollmechanismen."](http://www.daserste.de/unterhaltung/film/themenabend-waffenexporte/film/index.html), ARD
            * Lieferung an Mexiko, obwohl wegen Menschenrechtsverletzungen nicht den Anforderungen des Kriegswaffenkontrollgesetzes entspricht
            * Whistleblower wird verfolgt; Familie bedroht
    * 2017: https://www.heise.de/newsticker/meldung/Horrorvideo-soll-Verbot-autonomer-Waffen-voranbringen-3889950.html
        * Video: ["Slaugherbots" von Future of Life Institute](https://www.youtube.com/watch?v=HipTO_7mUOw), 2017, über autonome Waffensysteme
        * http://autonomousweapons.org
        * Daniel Suarez - 3. Buch

* Kriegseinsätze sollten nur aus humanitären Gründen erfolgen und ohne wirtschaftlichen Interessen geleitet sein
    * Positive Beispiele:
        * [Info über Blauhelme](https://de.wikipedia.org/wiki/Friedenstruppen_der_Vereinten_Nationen) (inkl. Kritik)
    * Lange Liste der negativen Beispiele:
        * [PTBS - Posttraumatische Belastungsstörung](http://de.wikipedia.org/wiki/Posttraumatische_Belastungsst%C3%B6rung)
        * [Agent Orange / Vietnam](https://de.wikipedia.org/wiki/Agent_Orange), siehe auch https://de.wikipedia.org/wiki/Bayer_AG
    * Hintergrundinfos von Menschen mit Erfahrung:
        * Video: [Johannes Clair - Ehemaliger Soldat über "Afghanistan und Kampfeinsatz"](https://www.youtube.com/watch?v=SjfuyKMgI7s) (2015)

### Waffenexporte, Rüstungsexporte
* 2019: "Allein in der ersten Hälfte dieses Jahres lieferte Deutschland Rüstungsgüter im Wert von mehr als einer Milliarde Euro an die Jemen-Kriegsallianz - und das trotz bestehender Exportbeschränkungen." (https://www.tagesschau.de/wirtschaft/exporte-ruestung-101.html)
* 2017: ["Deutsche Rüstungskonzerne umgehen Exportregeln"](http://www.tagesschau.de/multimedia/video/video-365777.html)
    * Petition von terres des hommes: https://weact.campact.de/petitions/stoppt-waffenexporte
    * Beispiel RM
        * "Da der Genehmigungsprozess für den Rüstungsexport in Deutschland mit großen Risiken behaftet ist, da die Geschäfte vom Wohlwollen der jeweiligen Regierung abhängig sind, wickelt [...] große Deals, wie die mit Saudi-Arabien auch über Tochterunternehmen ab. Diese haben ihren Sitz z.B. in Italien [...] oder Österreich [...]. Rüstungsgüter, die in anderen Ländern produziert werden, unterliegen nicht der deutschen Rüstungsexport-Kontrolle."](https://de.wikipedia.org/wiki/Rheinmetall#Unternehmensbereich_R%C3%BCstung), "Den Kern der Organisationsstruktur bilden die drei Divisionen Weapon und Munition (deutsch: Waffe und Munition), Electronic Solutions (deutsch: elektronische Lösungen) sowie Vehicle Systems (deutsch: Fahrzeug-Systeme)"
        * Startseite zum Nachhaltigkeitsbericht: "So ganzheitlich denkt RM" (https://www.rheinmetall.com/de/rheinmetall_ag/home.php)
* DLF-Feature 2015-02-14, 18:40 - 19:00 (Link?):
    - Mit Kleinwaffen werden die meisten Menschen getötet (Massenvernichtungswaffen)
    - Kleinwaffen werden oft schlecht gesichert gelagert
    - Kleinwaffen landen früher oder später immer in kriminellen Kreisen
    - Kleinwaffe: Pistole bis Panzerfaust (was eine Person tragen kann)
    - Kleinwaffen sind robust und langlebig
    - Die USA kontrolliert zumindest ihre gelieferten Waffen
        (die am Ende meist gegen die USA eingesetzt werden, Beispiel Stingerraketen)
    - D. darf eigentlich nur an Nato-Partner exportieren.
        Weil die aber ihr Budget gekürzt haben, werden mittlerweile
        immer mehr Ausnahme-Exporte in Drittländer genehmigt (auf Drängen der Rüstungsindustrie)
        Dort haben die Waffen eigentlich nichts zu suchen.

Menschenrechte
--------------
siehe menschenrechte.md

Politik von unten
-----------------
* ["Bürgerliche Sammelbewegung: #aufstehen oder #liegenbleiben?"](https://neue-debatte.com/2018/08/06/buergerliche-sammelbewegung-aufstehen-oder-liegenbleiben/)
    * "Die fehlende Nähe der bürgerlichen Linken zu den ökonomisch benachteiligten Schichten bekommt Gesicht."
    * "Den Bürgerinnen und Bürgern müsse zugehört werden, steht auf der Webseite www.aufstehen.de und, dass Flaschen sammeln keine Lösung sein darf."
    * "Doch ein radikaler politischer Ansatz übersteigt die Vorstellungskraft der Bürgerlichen, die noch einigermaßen gut leben im System, auch wenn sie merken, dass es ihnen langsam aber sicher ans Leder geht."
    * ?
* https://neue-debatte.com/2018/09/10/aufstehen-oder-zusammenwirken/
    * #Aufstehen vs. basisdemokratischen 7er-Ansatz
    * https://neue-debatte.com/2018/09/02/politik-und-leben-verbinden/
        * "Sie wissen eigentlich: Abwarten ist tödlich! Doch sie sind so vereinzelt, dass ihre Machtlosigkeit inzwischen zur zwangsweisen Tatenlosigkeit führt."
        * Wirksamkeit des Einzelnen
* https://de.wikipedia.org/wiki/Proletariat
    * ... Probleme ernst nehmen ... auch in der Kommunikation ... den Leuten geht es tatsächlich schlechter als anderen ... alles andere ist Schönreden

Diskussion
----------
### Konservativ heißt...
* ["Gastkommentar von Christian Nürnberger: Was heißt heute noch „konservativ“?"](https://www.wormser-zeitung.de/panorama/aus-aller-welt/gastkommentar-von-christian-nurnberger-was-heisst-heute-noch-konservativ_19231413)
    * "Konservativ sein heißt, immer erst 20 Jahre zu spät einzusehen, dass die anderen recht hatten."

* todo: siehe auch allgemein-zielgruppen.md

### Sorgen der Mittelschicht aus der Nachkriegsgeneration
* Ausdruck der Angst/Sorgen
    * "Einwanderung in die Sozialkassen"
        * Kampfbegriff
        * Was ist mit den vielen "faulen Inländer", die keine Arbeit finden wollen?
        * Wo sollen diese Menschen hin?
        * Europäische Lösung nötig. Gerechte Verteilung oder Abschottung?
        * siehe "Gutmensch trifft Flüchtling"
    * "Schneller Abschieben; bei Kriminalität direkt in sichere Herkunftsländer; nicht in unsere Gefängnisse"
        * ...
    * "So wird alles zerredet"
        * ...
* Sorgen ernst nehmen (!= Konsequenzen teilen)
    * siehe z. B. https://de.wikipedia.org/wiki/Brigitte_Bardot#Verbindung_zur_franz%C3%B6sischen_Rechten
    * Werte in Gefahr?
        * Welche?
        * Nur durch Kultur X?
        * Wenn es Kultur X nicht gäbe, wäre dann alles in Ordnung?
        * Welche Akteure der Kultur 0 arbeiten ebenfalls am Verfall der Werte?
    * Wer ist für das Einwandern einer Kultur Y verantwortlich?
        * die Einwanderer selbst?
        * die Politik?
        * die Wirtschaft?
            * z. B. hochgebildete Arbeitskräfte; sind diese auch unerwünscht?
                * wie trennt man das?
                * was ist Minderleistern der Kultur 0?
    * "Der Islam ist nicht integrierbar und gefährdet unsere Werte, weil er moralisch Jahrzehnte zurückliegt"
        * ...
        * Schlimm, keine Frage. Was ist die Lösung?
        * Wie kann man sich da persönlich engagieren?
        * Welche Werte genau?
        * Rhetorik:
            * darauf achten, ob die Sachebene verlassen wird und auf die Personenebene gegangen wird
                * z. B. der Argument des Politikers der Partei X lässt man nicht gelten oder ist verdächtig, wegen der wahrgenommenen Gesinnung der Partei, die nicht zur eigenen passt
* "Der Anteil der Muslime in Deutschland wird weiter stark zunehmen, auch wenn die Flüchtlingszahlen zurückgehen"
    * https://www.cicero.de/innenpolitik/Muslime-Anzahl-Deutschland-Europa-Studie-Pew-Research, 2018
        * "Bleibt etwa der Zuzug von Muslimen auf dem sehr hohen Niveau der Jahre 2010 bis 2016, so würde ihr Anteil an der Gesamtbevölkerung in Deutschland von derzeit 6,1 Prozent auf 20 Prozent bis zum Jahr 2050 steigen"
            * Kabarett: dann müssen die halt mit dem Klimawandel zurechtkommen
            * siehe auch https://de.wikipedia.org/wiki/Kollaps_(Buch)
                * Ursache Nr. 3: "Feindliche Nachbarn"
        * "Aufgrund der vergleichsweise hohen Fertilitätsrate muslimischer Frauen wird der Anteil der Muslime in Europa auch ohne Einwanderung steigen, von derzeit 4,9 auf 7,4 Prozent."
    * Lösungen:
        * Religion und Staat noch stärker trennen?
        * eine Politik der wenigen Kinder fahren (auch wegen der Nachhaltigkeit)?
* "Alles, was links der Partei C ist, führt zu DDR-Verhältnissen"
    * ...
    * Das heißt, die Partei xyz ist alternativlos?
    * Vertritt die Partei C überhaupt christliche Werte?
    * Das heißt, egal was die anderen Parteien vorschlagen, sie werden nie wählbar sein?
    * Warum dann nicht mit Gleichgesinnten in der Partei xyz engagieren?
        * Bringt auch nichts
    * Wenn es nichts besseres gibt, dann ist die Wahl doch schon gefallen, oder?
    *
    * ...
* "Welche Lösung hast du?"
    * Jeder tut das, was er in seinem Rahmen tun kann.
    * Das große Ganze im Blick behalten.
    * Mehr an die Gemeinschaft denken und nicht nur an sich und sein nächstes Umfeld.
    * Dinge, die einfach umzusetzen sind, auch umsetzen und nicht warten bis man für alles eine Lösung hat.
    * Probleme ehrlich benennen. Nachsichtig mit den Schwächen der Menschen sein. Gesetzesverstöße konsequent ahnden.
    * An die Schwachen denken.
    * Auch mal die Perspektive der Opfer einnehmen.

Frieden
-------
siehe ::frieden

### Galtung
* https://www.galtung-institut.de/en/
    * Prof. Dr. Galtung - Keep Calm and Study Peace
    * https://de.wikipedia.org/wiki/Gewaltdreieck nach Galtung
        * "Wechselwirkungen zwischen struktureller, personaler und kultureller Gewalt"
        * Strukturelle Gewalt
            * ...
            * "Dass Menschen morden und verletzen ist genauso auf personale und strukturelle Gewalt zurückzuführen, wie dass bestimmte soziale Gruppen unterdrückt werden und dass beispielsweise Lebenschancen ungleich verteilt sind. Weitere Beispiele sind inhumane Lebensbedingungen, unzureichende Kontrollinstanzen und mangelhafte soziale Absicherung."
            * https://de.wikipedia.org/wiki/Strukturelle_Gewalt
                * "Strukturelle Gewalt ist die vermeidbare Beeinträchtigung grundlegender menschlicher Bedürfnisse"
                * "Unter Strukturelle Gewalt fallen alle Formen der Diskriminierung,
                die ungleiche Verteilung von Einkommen, Bildungschancen und Lebenserwartungen,
                sowie das Wohlstandsgefälle zwischen der ersten und der Dritten Welt."
                * " Strukturelle Gewalt werde von den Opfern oft nicht einmal wahrgenommen, da die eingeschränkten Lebensnormen bereits internalisiert seien."
                * Bertholt Brecht
                    * "„Es gibt viele Arten zu töten. Man kann einem ein Messer in den Bauch stechen, einem das Brot entziehen, einen von einer Krankheit nicht heilen, einen in eine schlechte Wohnung stecken, einen durch Arbeit zu Tode schinden, einen zum Suizid treiben, einen in den Krieg führen usw. Nur weniges davon ist in unserem Staat verboten.“"

### Theodor Ebert
* Theodor Ebert
    * https://de.wikipedia.org/wiki/Theodor_Ebert_(Politikwissenschaftler)
        * "maßgeblich an der Entwicklung des Konzepts der Sozialen Verteidigung als Alternative zur Kriegführung beteiligt"
            * https://de.wikipedia.org/wiki/Soziale_Verteidigung
                * "ist der Schwerpunkt nicht die Verteidigung eines Territoriums, sondern der Strukturen der Zivilgesellschaft gegen
                militärische Übergriffe eines anderen Landes (oder gegen die Handlungen von Putschisten)"
                * "Aufgebaut wurde dabei unter anderem auf den Grundlagen des von Henry David Thoreau entwickelten zivilen Ungehorsams
                und dem gewaltfreien Widerstand Mahatma Gandhis und Martin Luther Kings"
                * "Soziale Verteidigung geht von dem Wertegrundsatz aus, dass Menschenleben und Strukturen einer Gemeinschaft wichtiger
                sind als politisch-historische Einflusssphären"
                * "Es wird nicht der Angriff bestraft, sondern die Besatzung."
                * "Soziale Verteidigung basiert auf folgenden Grundhaltungen:"
                    * "jeder Mensch wird als Mensch geachtet, also auch der Gegner;"
                    * ...
                    * "der Glaube, dass jeder Mensch veränderungsfähig ist;"
                    * ...
                * Empfohlene Verhaltensweisen:
                    * "Während militärische Verteidigung in der Regel den Eintrittspreis für einen Aggressor möglichst hoch gestalten will,
                    beschert die sich gewaltlos verteidigende Bevölkerung einen hohen Aufenthaltspreis."
                    * ...
                * Beispiele, Möglichkeiten und Grenzen: ...

### Inbox
* 2020: https://www.friedenskooperative.de/

Inbox
-----
### 2020 - Rhetorik in Haushaltsdebatte - Merkels Antwort
* "So trocken reagiert Merkel auf Weidels Erklärung zur Spendenaffäre",
    https://www.youtube.com/watch?v=rZv6nlvVwTg&feature=emb_rel_end, 2018, 2 min

### 2020 - Wahrheit
* aus einem Artikel von Michel Friedman Mai 2019 zu Corona
    * "Die Wahrheit ist dem Menschen zumutbar, behauptet [...] Schriftstellerin Ingeborg Bachmann.
        Mit dem Wort zumutbar beschreibt sie die Herausforderung und Kompliziertheit, oft auch
        den Schmerz und die Unbequemlichkeit der Wahrheit.
        Wahrheiten verändern sich [mit der Zeit].", Sie ist dynamisch.
        Es gibt keine ewigen absoluten Wahrheiten.
    * "Wahrheit ist nicht vom Glauben bestimmt, sondern vom Wissen und dem Nachdenken darüber."
    * "Wer heute behauptet, die Sonne drehe sich um die Erde, ist ein Lügner" (damals war das anders)
    * Andre Gide sagt: "Vertrauen Sie denen, die die Wahrheit suchen, und misstrauen sie denen, die sie gefunden haben."
    * "Die Wahrheit suchen ist Wesenskern der Wissenschaft und Wissenschaftler"
    * Corona: "permanenter öffentlicher Erkenntnisprozess"
        * offen mit der Lücke und der Schwäche umgehen können,
            "um erneut einen weiteren Schritt zu gehen, die Voraussetzung für intelligentes Handeln ist"
    * Schwurbler-Proteste: "Die bisher unsichtbaren Menschenverachter, Demagogen, Lügner"
    * Wahrheit ist dem Menschen zumutbar ..."Die Suche nach ihr ist das wahre Geschenk des Menschseins"

### 2019
* Buchtipp: Ian McEvan - The Cockroach
* https://blog.gls.de/gls-treuhand/right-livelihood-award-fuer-mutige-menschen/, 2019
    * "Die schwedische Right Livelihood Foundation unterstützt mutige, visionäre Menschen mit dem „Alternativen Nobelpreis“."
    * "Was haben Greta Thunberg, Davi Kopenawa, Aminatou Haidar, Guo Jianmei, Edward Snowden, Hermann Scheer, Johan Galtung und Vandana Shiva gemeinsam?
        Sie alle gehen mutig und visionär globale Probleme wie Klimakrise, Zerstörung von Lebensräumen, Ungerechtigkeit und Menschenrechtsverletzungen an"
