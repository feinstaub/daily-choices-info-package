Denkweise
=========

siehe auch allgemein-zielgruppen.md

<!-- toc -->

- [Beispiele](#beispiele)
  * [Die Stärke entwickeln, nein zu sagen](#die-starke-entwickeln-nein-zu-sagen)
  * [Was ich tue und was die anderen tuen](#was-ich-tue-und-was-die-anderen-tuen)
  * [Abgeklärtheit](#abgeklartheit)
- [Motivation für Aktive](#motivation-fur-aktive)
  * ["Warum gibt es eigentlich keine Elternbewegung gegen den Klimawandel?"](#warum-gibt-es-eigentlich-keine-elternbewegung-gegen-den-klimawandel)
  * ["Ich glaube, die Gesellschaft kann es schaffen"](#ich-glaube-die-gesellschaft-kann-es-schaffen)
  * [DieOption.at](#dieoptionat)
  * [Scheinbar unüberwindbare Probleme / Herausforderungen](#scheinbar-unuberwindbare-probleme--herausforderungen)
  * [Wichtigkeit der vielen 'kleinen Leute'](#wichtigkeit-der-vielen-kleinen-leute)
- [The Lifelong Activist](#the-lifelong-activist)
- [Nicht-hinterfragte Glaubenssätze](#nicht-hinterfragte-glaubenssatze)
  * [Der Mensch](#der-mensch)
  * [Wirtschaft](#wirtschaft)
  * [Engagement für eine gute Zukunft / Nachhaltigkeit](#engagement-fur-eine-gute-zukunft--nachhaltigkeit)
  * [Landwirtschaft](#landwirtschaft)
  * [Mensch-Tier-Beziehung](#mensch-tier-beziehung)
  * [Vermeintliche Ohnmacht des Einzelnen](#vermeintliche-ohnmacht-des-einzelnen)
  * [Anspruchshaltung / Normalität / Was ist normal? / Glück / Zufriedenheit](#anspruchshaltung--normalitat--was-ist-normal--gluck--zufriedenheit)
  * [Bewertung des Zustandes der Welt](#bewertung-des-zustandes-der-welt)
  * ["Aber irgendein *-ismus steht vor unseren Toren (oder ist schon unter uns) und das ist das Ende"](#aber-irgendein--ismus-steht-vor-unseren-toren-oder-ist-schon-unter-uns-und-das-ist-das-ende)
  * ["Aber die Politik ist schuld / muss zuerst was tun"](#aber-die-politik-ist-schuld--muss-zuerst-was-tun)
  * ["Aber ich mach' doch schon ganz viel X" / "Wir machen schon viel" / "Wir haben schon viel gemacht"](#aber-ich-mach-doch-schon-ganz-viel-x--wir-machen-schon-viel--wir-haben-schon-viel-gemacht)
  * ["Aber kann man da überhaupt noch Spaß haben?"](#aber-kann-man-da-uberhaupt-noch-spass-haben)
  * ["Aber man kann doch alleine eh nichts ändern"](#aber-man-kann-doch-alleine-eh-nichts-andern)
  * ["Aber eigentlich ist doch die Überbevölkerung an allem Schuld"](#aber-eigentlich-ist-doch-die-uberbevolkerung-an-allem-schuld)
- [Siehe auch](#siehe-auch)

<!-- tocstop -->

* https://www.duden.de/rechtschreibung/Geisteshaltung
    * Synonyme: "Denkweise, Einstellung, Gesinnung, Ideologie, Mentalität, Standpunkt, Überzeugung, Weltanschauung"
* https://en.wikipedia.org/wiki/Mindset

Beispiele
---------
### Die Stärke entwickeln, nein zu sagen
* Video: [Interview mit Richard Stallman über IoT](https://www.youtube.com/watch?v=AAP4N3KyLmM), 2018, 17 min, engl.
    * "We have to develop the strength to say no"
    * (see FreieSoftware/README.md)

### Was ich tue und was die anderen tuen
* Video: [Interview mit Richard Stallman über IoT](https://www.youtube.com/watch?v=AAP4N3KyLmM), 2018, 17 min, engl.
    * ...
    * 5:15: "Can one person stop this desater?"
        * The answer is no unless other people join in.
        * Is not claiming to have the powers of a superman.
        * Bewusstsein schaffen und hoffen, dass andere mitmachen, sich zu wehren
        * But your question assumes that is not good enough. Either I am so strong that I can defeat the whole thing or the case is lost completely.

* Video: [Interview mit Richard Stallman 2017](https://www.youtube.com/watch?v=jUibaPTXSHk), 2017, 30 min, engl.
    * siehe ../FreieSoftware/README.md

### Abgeklärtheit
* Buch: "Aus kontrolliertem Raubbau" von Kathrin Hartmann
    * S. 377 "Alternativen statt Lösungen"
        * S. 386
            * https://de.wikipedia.org/wiki/Naomi_Klein
                * Buch_: No Logo
                * Buch_: Die Entscheidung: Kapitalismus vs. Klima
            * "illusorisch"
            * Vorwurf des "Klimawandel-Missbrauchs", um auf die Probleme von Kapitalismus und Wachstum aufmerksam zu machen
        * S. 390
            * "Wenn diese Utopie Wirklichkeit werden soll, geht das nur, wenn dafür etwas anderes wegräumt - und das wird sich zur Wehr setzen"
            * Ulrich Brand
                * Ursache von vielen Problemen ist das Profit- und Gewinnprinzip
        * S. 392
            * Kleinbauern in Bangladesh führen ein Leben, das sie so noch viele Generationen weiterleben könnten (wenn nicht die Natur um sie herum kaputt gemacht wird)
            * "Änderung der imperialen Lebensweise"
                * drastisch weniger Energieverbrauch, kein Hyperkonsum, weniger tierische Produkte
                * nicht nur auf individueller Ebene, sondern die Strukturen müssen sich mitändern
            * gesellschaftliche Veränderungen herbeiführen
            * das große Ganze betrachten und nicht nur Einzelaspekte
            * "Ungerechtigkeit und Leid nicht einfach hinnehmen,
                sondern tief zu empfinden und wütend zu werden
                **statt zynisch und abgeklärt mit den Achseln zu zucken**,
                das ist für diese Veränderung unbedingt nötig"
        * S. 393
            * bessere Tütensuppe mit besserem Palmöl
        * S. 394
            * Hoffnung trotz Beobachtung von struktureller Gewalt auf den Reisen (siehe Galtung)
            * "Weil die Menschen [dort] mit einer so großen Leidenschaft und Kraft, Liebe und Solidarität,
            Phantasie, Klugheit und Mut **ganz selbstverständlich Widerstand** gegen die Zumutung
            einer totalitären Gesellschafts- und Weltordnung leisten.
            Weil sie nicht an pragmatische 'Lösungen' glauben, sondern an bedingungslose Gerechtigkeit.
            Für uns alle."
            * "Nichts von alldem habe ich gespürt, wenn ich mit [...] den nüchternen und überheblichen Technokraten,
            denen ich bei Interviews oder auf den vermeintlichen Weltrettungskongressen begegnet bin
            und deren **lähmender, geistesvernichtender monotoner Ökonomen-Lyrik** ich gelauscht habe,
            hinter der sich immer nur Empathielosigkeit, Verachtung und Aggression verborgen haben."

Motivation für Aktive
---------------------
### "Warum gibt es eigentlich keine Elternbewegung gegen den Klimawandel?"
* https://blog.gls.de/der-wirtschaftsteil/wirtschaftsteil-301-technik-klimafragen/, 2018
    * "Aus meiner Sicht müssten — wenn wir es mit den Kindern und ihrer Zukunft wirklich ernst meinten — alle Eltern der westlichen Welt gemeinsam die massivste und unüberhörbarste Bewegung gegen den Klimawandel bilden, die die Welt je gesehen hat. Und dabei so viel brachiale Wirkung auf die Politik erzeugen, dass der Ausstieg aus den fossilen Brennstoffen eine Frage von Monaten ist. Aber genau das passiert nicht. Warum nicht?"
        * https://kaffeeundkapital.de/2018/07/18/eltern-ihre-kinder-und-der-mangel-an-brachialer-bewegung-zur-rettung-der-welt/
            * ...
        * "Die Antwort auf diese Frage ist dabei ziemlich klar, sie ist intellektuell, politisch und menschlich frustrierend. Es ist eine Antwort auf verschiedenen Ebenen, und die deprimierendste davon ist die der aktuellen Politik, Denn die ist eben vor allem Politik- und Lösungs-Theater, ein Phantasma der Machbarkeit und Beherrschbarkeit, das sich in Begriffen wie „Obergrenze“ abbildet."
            * http://www.spiegel.de/kultur/gesellschaft/klimawandel-die-katastrophe-haette-verhindert-werden-koennen-a-1221528.html
            * Antwort, warum: http://www.spiegel.de/wissenschaft/natur/klima-streit-im-duerresommer-ueberhitzt-kommentar-a-1222518.html
                * "Die Debatte steckt in einem Dilemma: Nur wer sich mit Hysterie nach vorne drängelt, erntet Beachtung."
                * ["More bad news: The risk of neglecting emotional responses to climate change information."](http://psycnet.apa.org/record/2007-06263-003), 2007
                * ["Doom and Gloom: The Role of the Media in Public Disengagement on Climate Change"](https://shorensteincenter.org/media-disengagement-climate-change/), 2018
                    * ...

### "Ich glaube, die Gesellschaft kann es schaffen"
* https://blog.gls.de/bankspiegel/next-organic-foerdert-innovationen/, 2018

### DieOption.at
* http://www.dieoption.at - "Alternativen für die Zukunft" - positive Beispiele
* ["Die besten Dokus zu Zukunft und Alternativen"](https://www.dieoption.at/doku-tipps/)
* ["Die besten Spielfilme über Zukunft und Alternativen"](https://www.dieoption.at/spielfilm-tipps/)

### Scheinbar unüberwindbare Probleme / Herausforderungen
* siehe "Siehe auch"
* z. B. Zu viel achtlos weggeworfener Müll/Kippen auf dem Boden?
    * Positive Beispiele: todo: Vorbild Singapur (?): hohe Geld-Strafen bei Fehltritten => Stadt ist blitzeblank.

### Wichtigkeit der vielen 'kleinen Leute'
* siehe philosophie.md / "Veganismus als neue Aufklärungsbewegung"
    * Stefan Bernhard Eck, Mitglied des Europäischen Parlaments
        * "Die **Verflechtungen von Agrarlobby und Wirtschaft mit der Politik** führten dazu, dass sich für Tiere und Umwelt kaum etwas Positives bewirken lasse."
        * "Statt sich mit Petitionen an Entscheider zu wenden, hält Eck daher **Aktivismus und kreativen Widerstand für zielführender, ja Widerstand ist für ihn angesichts der geschilderten Situation sogar Pflicht**. "Man kann sich auf die Politik nicht verlassen", so Eck, "darum ist es wichtig, dass viele 'kleine Leute' sich für eine Sache einsetzen.""
    * "Ben Moore, Professor für Astrophysik und Direktor des Zentrums für theoretische Astrophysik und Kosmologie der Universität Zürich"
        * "Moore ging in seinem Vortrag unter anderem der Frage nach, warum trotz der erdrückenden Menge von rationalen Argumenten, die für einen Fleischverzicht sprechen, **selbst hochgebildete Menschen** nicht bereit seien, auf Fleisch zu verzichten."
            * "Statt vorgeschobener Argumente wie "Weil es mir eben schmeckt", vermutet Moore eine andere Motivation: Weil die meisten Menschen **nicht einer Minderheit anhören wollen**."
            * "bekräftigte seine These mit sozialpsychologischen Untersuchungen, die einen **Bevölkerungsanteil von 10 % als kritische Masse** für das allgemeine Durchsetzen einer neuen Idee betrachten."

The Lifelong Activist
---------------------
* http://lifelongactivist.com/
* ["A Day in the Life of a Successful Activist"](http://lifelongactivist.com/part-v-managing-your-relationship-with-others/36-a-day-in-the-life-of-a-successful-activist/)
* ["How Successful People View (and Use) Time"](http://lifelongactivist.com/part-ii-managing-your-time/2-how-successful-people-view-and-use-time/)

Nicht-hinterfragte Glaubenssätze
--------------------------------
In der Diskussion mit Menschen begegnet man immer wieder bestimmten Aufassungen und Sichtweisen, die auf impliziten Annahmen beruhen.

Auflistung möglicherweise tief sitzender Glaubenssätze, die nicht hinterfragt werden (wollen), die aber positiven Handlungen entgegenwirken.
Dazu mögliche Fragen, mit denen diese Annahmen explizit gemacht werden können; als Vorbereitung zur Hinterfragung und Reflexion.

### Der Mensch
* "Die meisten Leute sind egoistisch"
    * todo: Egoismus-Artikel
        * siehe z. B. Sitzplatz in der Bahn vs. Anonymität der Stadt
        * der Mensch ist erfolgreich durch gegenseitige Rücksichtnahme

* "Der Mensch ist von Natur aus egoistisch"
    * falsch, todo: Egoismus-Artikel
        * Reduktion auf Biologie und Berücksichtigung von Soziologie und Kultur des Menschen

* "Menschen ändern sich nicht", "Die Leute werden sich nicht ändern"
    * Es gibt genug Gegenbeispiele dafür, dass sich Menschen im Laufe ihres Lebens entwickeln und nicht stehenbleiben.
    * Zum Beispiel Menschen, die irgendwann merken, wie egoistisch sie bisher gehandelt haben und sich ändern.

* "Der Einzelne kann nichts tun"
    * siehe unten

* "Der Einzelne kann nichts bewirken"
    * siehe unten

### Wirtschaft
* "Regulierung ist schlecht"
    * FRAGE: Wie würde ein Zusammenleben ohne Regeln funktionieren?
        * (das ist übrigens keine Anarchie, weil das Herrschaftslosigkeit bedeutet; die Frage wäre, wer dann für die Einhaltung der Regeln sorgt)
    * Das Wirtschaftsleben ist voller sinnvoller Regeln. Im Einzelfall ist zu prüfen, ob eine Regel Sinn macht oder nicht.

* "Der Markt kann für Gerechtigkeit sorgen"
    * FRAGE: Auch für die, die wenig Geld haben und dadurch für den Markt unsichtbar sind?

* "Steuern sind schlecht. Wir bezahlen zuviel."
    * FRAGE: Was sind uns korruptsfreie staatliche Leistungen wert? (dem Geld steht ja auch Leistung gegenüber)

* "Privat ist besser als Staat, weil effizienter"
    * FRAGE: Was ist mit regelmäßigen Skandalen in privaten Großunternehmen? Ist das gut für das Gemeinwohl?

* "Die unsichtbare Hand sorgt sich für das Gemeinwohl"
    * "Wenn alle egoistisch handeln, dann geht es allen gut"
    * FRAGE: Wenn jeder an sich selbst denkt, ist dann an alle gedacht? (nein, siehe Egoismus-Artikel; siehe die "Allmende-Herausforderung")
    * Auch dieses Konzept benötigt Regeln, an das sich alle halten müssen

* "Nur, was man in Geld messen kann, hat einen Wert"
    * (Geld als Selbstzweck)
    * FRAGE: Welche erstrebenswerten Werte außer Geld gibt es noch?

* "Wachstum um jeden Preis"
    * siehe wachstum.md

### Engagement für eine gute Zukunft / Nachhaltigkeit
* "Es ist schon zu spät etwas zu ändern"
    * "Es ist nun besser, nur noch unsere eigenen Interessen zu berücksichtigen"
    * FRAGE: Wer ist "uns"? Von wem hängt die Erfüllung unserer Interessen ab?
    * siehe auch "Nachhaltigkeit aus der Not heraus"

* "Andere sollen was tun"
    * Aber: "Sei du selbst die Veränderung..."
    * FRAGE: Was würdest du gerne verändert sehen?
    * siehe Egoismus

* "Gefühl der Bevormundung bei Vorschlag neuer Regeln" (oder Moralvorstellungen)
    * TODO: Artikel, Es ist sinnvoll, die Regeln, die unser friedliches Zusammenleben ermöglichen, regelmäßig auf den Prüfstand zu stellen und bei Bedarf anzupassen.
    * Ganz ohne Regeln geht es nicht. Sie dienen dem Gemeinwohl.
    * Die Freiheit des einen endet da, wo die Freiheit des anderen verletzt wird
    * Beispiel: realistische Bepreisung von Tierprodukten oder Ölprodukten

* "Was immer schon so war, ist gut"
    * FRAGE: Gilt das auch für X, Y oder Z?
        * z. B. Abwesenheit von Mobilität, Strom, Büchern und Internet, Gleichberechtigung, Rechtsstaat, ...
    * heute andere Rahmenbedingungen als früher

* "Natürlichkeit ist gut", "Die Natur ist gut"
    * Naturalistischer Fehlschluss, ...

* "Das Recht des Stärkeren"
    * siehe Egoismus; kann aus egoistischer Sicht auch hinten losgehen, denn keiner ist überall stark

* "Diktatur des Jetzt", "Status quo soll beibehalten werden"
    * FRAGE: Ist das gerecht?
    * FRAGE: Glaubst du, wenn man sich heute nicht anstrengt, dass mann morgen alles so bleibt wie es heute ist?
    * Angst vor Veränderung
    * Aber auch Unsicherheit bezüglich Auswirkungen von heutigen Entscheidungen. Man kann nicht wissen, welche Entscheidung für die Zukunft besser gewesen wäre (z. B. Atomkraft).
    * Lieber nichts tun bzw. den Dingen ihren Lauf lassen?
        * Kommt dann automatisch eine gute Lösung zustande?
            * z. B. Spaltung zwischen Arm und Reich: Laufen lassen? Ohne gemeinsame Werte wie Gerechtigkeit wäre das Ergebnis fatal.

* "Wir machen doch schon was"
    * Vergleich mit anderen Ländern, z. B. Investition in Schienenverkehr
        * https://www.tagesschau.de/wirtschaft/bahn-schienenbilanz-101.html - "Deutschland gibt deutlich weniger Geld für die Infrastruktur aus als andere Nationen."

### Landwirtschaft
* "Gentechnik ist der beste Weg"
    * FRAGE: Ist es vielleicht nur der bequemste Weg, um sich mit anderen Lösungsmöglichkeiten, die jetzt schon durchgeführt werden können, aber Anstrengungen erfordern, nicht beschäftigen zu müssen?

### Mensch-Tier-Beziehung
* "Bestimmte - von uns ausgedeutete Tierarten - sind weniger wert als andere"
    * FRAGE: Wie ist das zu rechtfertigen?

* "Tiere sind für die Nutzung durch den Menschen da"
    * FRAGE: Wer sagt das? Gilt hier das Recht des Stärkeren?

### Vermeintliche Ohnmacht des Einzelnen
* Aussage: "Aber im Vergleich zum Ganzen trage ich (tragen wir) nur einen sehr geringen Teil bei"
    * FRAGE: Wer konkret müsste dann nach dieser Logik anfangen, etwas zu tun? Kennst du Personen und Gruppen, die von sich sagen würden, sie verbrauchen/verantworten mehr als "nur das Nötigste"?
* siehe auch "Philosophie der Nachhaltigkeit - Es ist nicht egal, was du tust"
* siehe auch klimawandel.md
* siehe auch wirksamkeit-des-einzelnen.md

### Anspruchshaltung / Normalität / Was ist normal? / Glück / Zufriedenheit
* Feststellung 1: ["Die Welt wird besser, nicht schlechter"](https://www.dieoption.at/die-welt-wird-besser-nicht-schlechter/), 2018
* Feststellung 2: Das, was als normal empfunden wird, ändert sich
* Feststellung 3: Der Anspruch an ein erfülltes Leben wird durch verschiedene Akteure zu einem Konsumdenken verkehrt
* Folge 1: in vielen Fällen ist es angebracht, die für viele objektiv bessere Realität mit der empfundenen Normalität abzugleichen, um einen Zustand der Zufriedenheit zu erzeugen.
* Folge 2: Das heißt nicht, dass jetzt schon alles gut ist oder automatisch alles weiterhin besser wird, wenn man nichts tut
    * siehe geschichte.md
    * siehe kinderarbeit.md

Beispiele:

* Zweck von Urlaub ist Entspannung: Kann man diese Entspannung auch ohne Flugreisen erreichen?
* Zweck von Essen und Trinken ist Ernährung und Genuss: siehe ../Kaffee

### Bewertung des Zustandes der Welt
* siehe geschichte.md

### "Aber irgendein *-ismus steht vor unseren Toren (oder ist schon unter uns) und das ist das Ende"
* Was genau hat das mit der persönlichen nachhaltigen Lebensweise zu tun?
* Drüber nachdenken, wie man sich im persönlichen Umfeld dieses Themas annehmen kann.

### "Aber die Politik ist schuld / muss zuerst was tun"
* Mit einer guten Politik lebt es sich in der Tat leichter.
* Aber sollte man deswegen mit eigenen Handlungen abwarten, bis sich die Poltik (zufällig, irgendwann) ändert?
* Sich informieren wie Demokratie funktioniert: https://de.wikipedia.org/wiki/Demokratie, siehe demokratie.md, vermeintliche Ohnmacht des Einzelnen

### "Aber ich mach' doch schon ganz viel X" / "Wir machen schon viel" / "Wir haben schon viel gemacht"
* Wenn X die Mülltrennung ist, dann man in der Tat vielleicht mal darüber nachdenken, einen Schritt weiterzugehen.
    * siehe z. B. ["Der Kauf von Bio-Lebensmitteln oder eine gute Mülltrennung wiegen das nicht auf."](https://www.umweltbundesamt.de/presse/pressemitteilungen/wer-mehr-verdient-lebt-meist-umweltschaedlicher)
* Jeder ist auf einer Reise im Leben und immer mal wieder kann man sein eigenes Handeln im Sinne des Gemeinwohls hinterfragen und weiterentwickeln

### "Aber kann man da überhaupt noch Spaß haben?"
* klar, warum nicht?

### "Aber man kann doch alleine eh nichts ändern"
* siehe z. B. http://der-artgenosse.de/hey-veganer-ihr-koennt-eh-nichts-aendern (7 Minuten)
    * Hauptthema ist die vegane Lebensweise. Der Beitrag ist aber auch auf andere Themen übertragbar.
* siehe [Blinde Flecken](../hintergrund/saubere-weste-blinde-flecken.md), die vielleicht einen kleinen Schubs geben können, bei sich selber anzufangen
* siehe oben

### "Aber eigentlich ist doch die Überbevölkerung an allem Schuld"
* Problemsuche bei anderen:
    * Websuche: "Warum bekommen Leute in armen Ländern so viele Kinder"
        * "Geburtenkontrolle - Sechs Kinder sind drei zu viel - Ruanda will seine Bevölkerungsexplosion eindämmen: mit einer Drei-Kind-Politik.", [taz.de, 2007](http://www.taz.de/!5198781/)
        * http://www.gutefrage.net/frage/warum-haben-arme-leute-in-indien-afrika-usw-so-viele-kinder
            * wegen mangelnder Bildung
* siehe https://www.drawdown.org/solutions/women-and-girls/family-planning
* siehe fortpflanzung.md

Siehe auch
----------
* psychologie.md
* nachhaltigkeit.md
* klimawandel.md
* demokratie.md
