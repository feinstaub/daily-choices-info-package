Chemische Verschmutzung
=======================

<!-- toc -->

- [Langzeitaufbewahrung, Langzeitschutz](#langzeitaufbewahrung-langzeitschutz)

<!-- tocstop -->

Langzeitaufbewahrung, Langzeitschutz
------------------------------------
* ["Video: Dem Giftmüll auf der Spur"](https://www.daserste.de/information/wissen-kultur/w-wie-wissen/videos/Dem-Giftmuell-auf-der-video-100.html), 22.06.19 | 14:13 Min
    * "Millionen Tonnen gefährlicher Abfälle werden Jahr für Jahr in Salzbergwerken eingelagert. Experten befürchten, dass der Giftmüll – entgegen den Beteuerungen von Bergwerksbetreibern und Behörden – auf lange Sicht nicht sicher verwahrt ist."
