Saisonschluss 2019
==================

sl schlaue leute

<!-- toc -->

- [1/3](#13)
- [2/3](#23)
- [3/3](#33)

<!-- tocstop -->

1/3
---

"Klima, Kommentare und kleinere Katastrophen: Die Sommer sind heiß, die Gletscher schmelzen schneller als gedacht, eine weltweite Jugendbewegung treibt die Politiker vor sich her. In seinem dreiteiligen essayistischen Jahresrückblick stellt Mathias Greffrath fest, dass alles ungut mit allem zusammenhängt.
Von Mathias Greffrath", "Mathias Greffrath, Jahrgang 1945, ist Soziologe und Journalist"

https://www.deutschlandfunk.de/rueckblick-und-ausblick-saisonschluss-1-3.1184.de.html?dram:article_id=464237 - auch als Audiobeitrag

* "Klima ist kein Problem unter anderen, sondern ein totales soziales Phänomen, ein Epochenphänomen.
    Mobilität, Ernährung, Energie, Arbeit, Wachstum, Migration, Frieden – kaum ein Lebensbereich, der nicht vom Klimawandel berührt wird. / ::frieden
    Totalsichten bezeichnen historische Bruchlinien und zielen auf Veränderungen
    – aber ebensogut können sie Passivität erzeugen, in Ideologisierung, religiöser Überhöhung oder Feindbildzuschreibung münden."

* Verweis auf https://www.deutschlandfunk.de/carl-amery-global-exit-die-kirchen-und-der-totale-markt.730.de.html?dram:article_id=101743
    * "Carl Amery: Global Exit – Die Kirchen und der totale Markt", 2002
        * "Ich glaube an Gott; und ich glaube an den freien Markt"
        * "Frömmigkeit und die feste Überzeugung, ein freier Markt ohne jeglichen regulierenden oder kontrollierenden Eingriff des Staates
            sei das Beste für die Menschheit, gehören in den USA – und nicht nur dort – oft zusammen", ::freiheit
        * "Die biosphärische Krise [ist die] bedeutendste Herausforderung der Menschheit seit der Sintflut."
        * ...

* "Das Jahr begann mit einem teuren Konsens. Noch 20 Jahre Frist für Strom aus Kohle und dafür 40 Milliarden Subventionen für den Umbau. So beschloss die Kohlekommission; das heißt: mehr als eine Million pro Arbeitsplatz."

* "Dies ist das Jahr, nach dem niemand, [...] sagen kann: er habe es nicht gewusst."

* Greta Thunberg, ::ökofaschismus
    * "Ich bin sicher: wir werden noch viel mehr Pathos und viel mehr Inszenierung erleben – und brauchen. Denn wir haben den Klimawandel nicht vor uns, wir sind mittendrin."
    * "Es gibt viele Arten der Verleugnung.
        - Es gibt die einfache Ignoranz, mit der die AfD wirbt.
        - Es gibt die verlogene Ignoranz, mit der Donald Trump dröhnt, er halte den Klimawandel für eine Erfindung, und zwar der Chinesen.
        - Es gibt die Pathologisierung: das Mädchen ist krank, wie Friedrich Merz es befand.
        - Historiker bemühten die Kinderkreuzzüge des Mittelalters, um die gut informierten Forderungen der Schüler wegzuerklären,
        - psychiatrische Laien diagnostizierten Greta Thunberg,
        - Soziologen sorgten sich mehr als um die Atmosphäre um Dieselpendler und Ölheizungsbesitzer, die über Nacht zu Gelbwesten mutieren könnten.
        - Der Philosoph Wolfram Eilenberger setzte noch eins drauf: die massenhafte Mobilisierung für eine Konsumwende sei bedrohlicher als die „völkischen Untergangsszenarien“ vom rechten Rand, und er sehe schon einen ökofaschistischen Kontroll-Staat am Horizont, der eines Tages Fleischfresser oder SUV-Fahrer „im Namen des Lebens...ausmerzen“ könnte."
    * "Was treibt gestandene Intellektuelle zu solch schrillen, wütenden Befürchtungen angesichts ernsthafter, unironischer, gut informierter Schüler?"
        "Ist es die unangenehme Wahrheit, dass größere Veränderungen anstehen als die der Treibstoffart, dass schon bald auf mehr verzichtet werden muss als nur auf SUVs und viel zu große Wohnungen und viel zu billige Nackensteaks, dass mehr zu Ende geht als nur das Kohlezeitalter, dass es nicht nur um nur Grenzwerte geht, sondern um die Verteidigung ganz anderer Grenzen?"

    * "„Es gibt keine Alternative zum Fortschritt“, schrieb der Publizist Thomas Schmid in der Welt, Greta Thunberg gehöre zu jener „altbekannten“ Bewegung, die Rückkehr zu archaischen Verhältnissen predige, ja, sie sei zum Angriff auf das „gesamte Universum der westlichen Welt“ angetreten, denn zu diesem Universum gehöre „nun einmal ein Wille zum … Wohlstand im eindeutig materiellen Sinne“."

    * "Fridays for Future sich die Parents for Future, die Lehrer for Future, Omas for Future, Scientists for Future, Economists und Unternehmer und Ingenieure for Future"

    * "Und es verabschiedete ein Klimapaket, dessen Forderungen selbst vom Institut der deutschen Wirtschaft und vom Münchner ifo-Institut als zu lasch empfunden wurde. Und selbst das, so sagt es ein Ministerialbeamter, wäre nicht möglich gewesen ohne die Freitags-Schüler und diese autistische Schülerin aus Stockholm. Hätten Sie das für möglich gehalten?"

* "Dies ist das Jahr, in dem Angela Merkel zur tragischen Figur wurde"
    * ...

* Wissenschaft und Politik
    * "Follow The Science – Thunbergs Slogan trifft ins Herz des neuzeitlichen Glaubens: Wissenschaft"
    * "Mit der Physik kann man nicht diskutieren, das stimmt.
        Aber über die Konsequenzen der physikalischen Wahrheiten muss man streiten.
        Da geht es nicht um Fakten, sondern um unser Verhältnis zur Welt.
        Um Alternativen.
        Um Aufbruch oder Beharren. Um Bewahren oder Zerstören."
    * zwei Weltsichten
        * „Sachverständigenrates zur Begutachtung der gesamtwirtschaftlichen Entwicklung“: „Aufbruch zu einer neuen Klimapolitik“
            * "Die einzige Wunder- und Universalwaffe – und die einzige Empfehlung der Wirtschaftsweisen -
                ist eine Erhöhung der Prämie, die Wirtschaft und Private für die Verschmutzung der Atmosphäre zahlen sollen: der CO2-Preis."
            * "Es ist das Dokument einer Ökonomischen Wissenschaft, in deren Modellen Rohstoffe, Arbeit, Stoffströme – kurzum, die reale materielle Welt mit ihrer Physik und Chemie nur als Geldgrößen vorkommen."
        * „Sachverständigenrates für Umweltfragen“ zur „Demokratischen Legitimation von Umweltpolitik“
            * "enthält mehr Welt"

* Die Welt wird nicht untergehen, aber...
    * "wir hätten, so das Gutachten, die Wahl zwischen einem „Verwüstungsanthropozän“ oder „holozänartigem Anthropozän“"
    * "Wo wir anlanden, das wird davon abhängen, ob wir die planetaren Belastungsgrenzen für CO2- und Stickstoffbelastung, Süßwasserverbrauch, Ozeanversäuerung, Landnutzung überschreiten, jenseits derer die Reparatursysteme unberechenbar werden – das populärste Beispiel ist der westantarktische Eisschild, dessen völliges Abschmelzen ab einem theoretisch, aber nicht zeitlich exakt bestimmbaren Kipppunkt nicht mehr aufzuhalten ist – und die Ozeane um fünf Meter steigen lassen würde. Und das ist nur die Westantarktis."
    * "Ein „holozänartiges Anthropozän“ zu bewahren, also annähernd die Welt, die wir kennen, das bedürfte eines hohen Grades aktiver menschlicher Einwirkung, Planung, Gestaltung. Und einer radikalen Änderung unserer Lebensweisen."

* Vor 33 Jahren
    * "„Unsere Generation und die unserer Kinder und Enkel – so schrieb es vor 33 Jahren der Zoologe Hubert Markl,
        der damals Präsident der Deutschen Forschungsgemeinschaft war – werden zu tätigen Zeugen einer gewaltigen Umwälzung des Lebens auf unserer Erde.
        Vor unseren Augen, unter unseren Händen geht eine erdgeschichtliche Epoche zu Ende,
        die viele Jahrmillionen Bestand hatte. Nur blinder Stumpfsinn könnte sich dieser Tragik verschließen.
        Was bevorsteht, ist ebenso klar erkennbar wie bitter.“"
        * "Hubert Markl sprach vom biologischen „Holocaust“, gar vom Ende des Neozoikums –
            der Erdperiode, die vor 60 Millionen Jahren begann und in der die Fauna und Flora entstanden,
            mit der wir jetzt noch leben, er kritisierte unsere „parasitäre Lebensweise“,
            und appellierte an unsere Verantwortung: „Natur“ sei nun zur „Kulturaufgabe“ geworden, und zwar zur globalen, und zwar endgültig."
        * "Warnungen gab es schon vor 50 Jahren"
            * ...
            * "Seither haben sich die CO2-Emissionen der menschlichen Gattung noch einmal vervierfacht"
            * "Ein Epochenbruch steht bevor. Wie sieht so etwas aus? Vielleicht muss man bis zum letzten großen Klimaereignis der Erdgeschichte zurückgehen, / ::klimawandelfolgen
                um zu erklären, was uns da widerfährt, warum es nicht reicht, den CO2-Ausstoß kostenpflichtig zu machen.
                Um zu begreifen, was uns hindert, dem zu folgen, was wir wissen."

* "Die Erd-Saison, die jetzt zu Ende geht, begann in der neolithischen Revolution"
    * "Übergang des homo sapiens von einer nomadischen zur sesshaften Lebensweise"
    * "Das Leben wurde sicherer, aber auch anstrengender – [..]
        Ackerbau und Viehzucht erforderten regelmäßige, systematische Bearbeitung der Natur, Kalendersysteme, Bewässerungswissen, das Menschheitswissen
        und die Herrschaft über die Natur wuchsen, es entstanden Siedlungen und Städte, und damit militärisch gestützte Herrschaftssysteme,
        Baukunst, Priesterkasten, das Geldsystem; die Arbeitsteilung wuchs,
        damit die **soziale Schichtung und Ungleichheit**. Die Menschheit nahm zu, in neun Jahrtausenden von vier auf 50 Millionen."
    * Verweis auf "Film der Woche: „Parasite“ – Ungehemmte Gier nach Luxus"
    * "Jean‑Jacques Rousseau in seinem Diskurs über die Ungleichheit:"
        * "Der erste, der ein Stück Land mit einem Zaun umgab und auf den Gedanken kam zu sagen
            ‚Dies gehört mir‘ und der Leute fand, die einfältig genug waren,
            ihm zu glauben, war der eigentliche Begründer der bürgerlichen Gesellschaft [...]
            ihr seid verloren, wenn ihr vergesst, dass zwar die Früchte allen, die Erde aber niemandem gehört."
    * "Neue Eigentums- und Herrschaftsformen"
        * "der Monotheismus entstand:
            eine Erlösungsreligion, die keine Naturgötter und keine Gottkönige mehr kannte,
            sondern deren anonyme Stifter einen Weltenlenker und Gesetzgeber ersannen, der transzendent, gestaltlos und unerforschlich war.
            Weswegen auch die Herrschenden und die Reichen den Gesetzen unterworfen waren, wenigstens im Prinzip."
    * "**Das Recht** wurde heilig –
        und das schuf die ideelle Grundlage – nun, nicht für Demokratie – wohl aber für den **Beginn einer Milderung der Gewalt durch Gesetze**.
        Gesetze, die auch über den Mächtigen standen – weswegen Raum für Propheten frei wurde."
        * "Thora zum Thema Eigentum gesagt war: dass die Kluft zwischen Arm und Reich schändlich sei,
            dass alle sieben Jahre die Schulden erlassen gehören
            und alle fünfzig Jahre neu verteilt werden soll, was im Prinzip allen gehört."
        * "Jesus Christus verschärfte diesen Prozess der Zivilisation:
            die Zumutung des Gesetzes wanderte in die Herzen und ins Gewissen ein.
            Nicht das äußerliche Befolgen der Regeln, sondern die Verinnerlichung ihres Prinzips sollte den Frieden bringen." (vs. internationale Großkonzerne)

* "Extraktionskapitalismus hat die Grenzen erreicht"
    * "heute hat die profitgetriebene Ausweitung der Kapitalzone die Lebenserwartung und den Güterwohlstand auch im globalen Süden gesteigert,
        aber sie unterminiert zunehmend die politische Gestaltungsmacht der Nationalstaaten."

* "Jonathan Safran Foer: Ein Symptom des Phänomens Wachstum", ::wachstum
    * Buch: Wir sind das Klima, Jonathan Safran Foer, ::klimawandel, ::wirksamkeit-des-einzelnen
    * "es geht nicht darum, Diesel durch Wind zu ersetzen, und dann ist alles gut.
        Klima ist kein Problem unter anderen, sondern das spürbarste Symptom eines „totalen sozialen Phänomens“.
        Und das heißt Wachstum."
    * "Wie wir leben, sind wir der Klimawandel, aber die globalisierte Menschheit ist kein WIR.
        **Die Lasten und die Wohltaten sind ungleich verteilt, national wie global**,
        und **die Erderwärmung wird diese Ungleichheit noch steigern.**"
    * "Der doppelte Wachstumszwang – der Konsumenten und der profitabhängigen Kapitale – verschärft die Auseinandersetzungen
        um die Rohstoffreserven der Erde und der Ozeane: an Öl, an Kupfer, an Mangan, an seltenen Erden, Lithium, Phosphor, ja an Sand.
        Die „große Regression“ hat begonnen."
    * "Ein Jahrhundert lang wurde die **Gerechtigkeitsfrage in den kapitalistischen Industriegesellschaften durch Wachstum neutralisiert**"
        * ... (d. h. wichtige Gerechtigkeitsthemen stehen nicht auf der Agenda)
        * ...
    * "Daran aber wird klar, dass Demonstranten und Politiker zu kurz springen,
        wenn sie den Kampf gegen den Klimawandel zum alles überwölbenden globalen Großthema erklären.
        Noch einmal: nicht der Klimawandel ist das Problem, er ist das massive Symptom des Grundproblems: Wachstum."
    * "die Frage, wie eine Wirtschaft, deren Funktionieren auf Wachstum beruht,
        **in einen stationären Zustand gebracht werden kann, wird nicht einmal ernsthaft gestellt an unseren Wirtschaftsfakultäten.**"
    * "bedürfte einer global wirksamen Politik. Und das macht „einen Grad transnationaler Zusammenarbeit nötig,
    der die in der internationalen Arena bis heute geübte Praxis bei weitem übersteigt""
        * Jürgen Habermas
            * "Zweitausend Seiten, das Resümee eines lebenslangen Denkens über vernünftige Freiheit, auf denen er, ausgehend von jener Achsenzeit am Ende der neolithischen Umwälzung, die allmähliche Verwandlung einer Gesetzesreligion mit Erlösungsversprechen in eine säkulare Philosophie der menschlichen Autonomie und des Fortschritts im Bewusstsein der Freiheit rekonstruiert."
            * ...
* "Wohlstandsregionen müssten mehr Gleichheit ertragen"
    * "zeitgemäße Formulierung des kategorischen Imperativs in einer globalisierten und gefährdeten Erde,
        in der die Energiefrage, das demokratische Gleichheitspostulat und der materielle Glücksanspruch verbunden sind,
        die hatte Angela Merkel 2010 in gefunden: Jedem Erdenbürger das Recht auf den gleichen Anteil an der Atmosphäre.
        Das Postulat **ist mit Gründen nicht zu bestreiten, sondern nur mit Macht und mit Mauern zu ignorieren**.
        Zu Ende gedacht, heißt es: Ein „holozänartiges Anthropozän“ erfordert viel Technik und eine neue, global geltende
        und exekutierbare Rechtsordnung des gemeinsamen Eigentums aller Erdenbürger an den Gütern der Erde – Wasser, Boden, Luft, Naturschätzen."
    * "Das ist zwar vorstellbar, aber in den Wohlstandsregionen der Welt müssten die Bürger ihre Ansprüche auf die Erde reduzieren,
        **ihre Schulden gegenüber der Natur und den ehemaligen Kolonien anerkennen**,
        was praktisch hieße: mehr Gleichheit ertragen.
        Und an dieser Wegmarke hilft Argumentieren nicht weiter.
        Hier beginnt, mit den Worten des katholischen Ökologen Carl Améry, das Reich der religiösen Regungen,
        „**wo solche Einsichten in Schuld und Last nicht als Minderung, sondern als Mehrung unserer Menschlichkeit empfunden werden**“."
    * "Und hier kann auch die Vernunft des Philosophen nicht weiterhelfen.
        Habermas schreibt: „Erst im Akt der Bindung unserer Willkür an die aus praktischer Vernunft gewonnene Einsicht,
        erfüllt sich das richtige moralische Urteil im autonomen Handeln.“
        Aber „nichts und niemand“ zwinge uns zu solcher Bindung.
        ie Suche nach einem Ausweg aus der Wachstumsfalle, die nicht auf technische Willenslenkung à la China oder facebook setzen will,
        kann sich – so endet das große Buch – auf keine Gewissheit stützen.
        Es hängt – so Habermas – „von unserem Selbstverständnis ab, ob wir als Individuen ...oder als Bürger und Politiker ...
        in Situationen der Wahrnehmung unausweichlicher Probleme
        uns selbst und gegenseitig die Spontaneität vernünftiger Freiheit sowohl zutrauen wie zumuten.“"
    * "Uns etwas zuzumuten – dafür waren in der alten Welt die Propheten zuständig.
        Seit einem halben Jahrhundert erheben Wissenschaftler, Philosophen, Theologen, Nobelpreisträger und Religionsoberhäupter
        ihre mahnenden Stimme gegen die Weltzerstörung."
        * "Aber alle diese, „Posaunen der Propheten“, so Carl Amery, selbst ein Prophet, „brachten nicht einmal die Vorwerke des Techno-Systems“ zum Wanken."
        * "So hat auch die ebenso ökologische wie ökumenische Botschaft des Papstes in seiner Enzyklika Laudato si,
            so haben seine scharfen Attacken auf den „Imperialismus des internationalen Finanzkapitals“, in diesem Land wenig bewirkt."
        * "Aber es ist vielleicht ein Unterschied, ob es ein 82-Jähriger sagt oder ob eine Sechzehnjährige einfach stur weiter sitzen bleibt,
            so der Münsteraner Weihbischof Lohmann mit einem schmunzelnden Blick nach oben.
            Und nicht nur er, sondern auch die Bischöfe von Berlin, Hildesheim und Münster haben Greta Thunberg als Prophetin apostrophiert."
    * "Es geht einiges zu Ende: die goldenen Jahre der letzten großen Konjunktur,
        die Dominanz des euro-amerikanischen Kapitalismus,
        **die Illusion einer unendlich verwertbaren Natur**.
        Ja, das „gesamte Universum der westlichen Welt“." (siehe oben)

* "Zeiten, in denen es darauf ankommt"
    * "Eine alte Ordnung löst sich auf, und eine neue hat noch keine Konturen.
        Das sind Zeiten, in denen die Menschen „mehr als sonst eine grundlegendere Diagnose erwarten und ganz besonders bereit sind,
        sie aufzunehmen und zu erproben, wenn sie nur einigermaßen annehmbar sein sollte“.
        So schrieb es der große Ökonom John Maynard Keynes im Intervall zwischen der Weltwirtschaftskrise von 1929 und dem Beginn des Weltkriegs.
        Es sind Zeiten, in den auch die Monster der Vergangenheit auftauchen und die Drohung des „Verwüstungsanthropozäns“ Gestalt annimmt.
        **Zeiten, in denen es darauf ankommt, wie viele Millionen einzelne sich verhalten,**
        **wohin sie sich bewegen, was sie wählen, wie hartnäckig sie bleiben – unter dem offenen Himmel der Geschichte.**
    Kein Zweifel, die Sache ist groß.
    Aber macht sie das nicht auch …reizvoll?"

2/3
---
https://www.deutschlandfunk.de/rueckblick-und-ausblick-saisonschluss-2-3.1184.de.html?dram:article_id=464789

"Arbeitsplätze, Algorithmen und Alternativen:
Mit dem Wissen über die Probleme wächst die Furchtsamkeit der Politik.
Braucht die Welt einen neuen, furchtloseren Politikertyp?"

3/3
---
https://www.deutschlandfunk.de/rueckblick-und-ausblick-saisonschluss-3-3.1184.de.html?dram:article_id=464820

"Über die Seele im Zeitalter von Epochenwenden [...].
Wenn moralische Leitplanken und politische Loyalitäten nicht mehr als Orientierungshilfe dienen,
greift dann die Realitätsverweigerung?"

* ...
* ...
* ...

* Buch: Ihr habt keinen Plan, 2019, ::klimawandel, ::lösungsweg
    * "Ihr habt keinen Plan, darum machen wir einen! 10 Bedingungen für die Rettung unserer Zukunft - Mit einem Vorwort von Harald Lesch"
    * https://www.randomhouse.de/Paperback/Ihr-habt-keinen-Plan-darum-machen-wir-einen/Der-Jugendrat-der-Generationenstiftung/Blessing/e568381.rhd
        * "Acht Autoren und Aktivisten, Mitglieder des Jugendrates Generationen Stiftung, warnen nicht nur vor den Gefahren, denen sich die heutigen 14- bis 25-Jährigen ausgesetzt sehen. In genau recherchierten Beiträgen, die mit den Erkenntnissen anerkannter Wissenschaftler abgeglichen sind, stellen sie konkrete Forderungen, nehmen uns alle in die Verantwortung und entwerfen eine Vision, die die Kraft hat, Generationen zu vereinen."
        * Bewertungen:
            * "»Während die Erwachsenen noch übers Schuleschwänzen lamentierten, haben die Jungen ihre Hausaufgaben gemacht – und die ihrer Kritiker gleich mit. Herausgekommen ist ein kluges und kontroverses Buch [...] Wir brauchen radikal neue Entwürfe unseres Zusammenlebens auf diesem Planeten! Die Zeit läuft ab, aber die Lösungen sind da. Dieses Buch zeigt viele davon auf. Lesen Sie es, und lesen Sie es schnell.« Frank Schätzing"
            * "Der Jugendrat der Generationen Stiftung nimmt sich schonungslos zehn Themen an, bei denen man gerne die Augen verdreht und denkt "Ach, nicht schon wieder." Klimaschutz, Alternativen zum Kapitalismus, soziale und globale Gerechtigkeit, Frieden, um nur ein paar zu nennen. Die Lösungswege sind nachvollziehbar. Sie sind nicht bequem, das ist wohl der Grund für den heftigen entgegengebrachten Widerstand. Was ich sehr angenehm finde: es ist kein Buch von Gutmenschen oder Moralaposteln. Es ist ein pragmatischer, schonungsloser Blick auf das, was geändert werden muss."
    * https://www.freitag.de/produkt-der-woche/buch/ihr-habt-keinen-plan-darum-machen-wir-einen
        * TODO! inkl. Videos
