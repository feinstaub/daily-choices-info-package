Charles Eisenstein
==================

<!-- toc -->

- [2021](#2021)
  * [Trust Your Heart](#trust-your-heart)
  * [Connecting to "Them"](#connecting-to-them)
- [2020](#2020)
  * [us and them; evil people?](#us-and-them-evil-people)
  * ["How Much Good Can One Person Really Do?", 2020](#how-much-good-can-one-person-really-do-2020)
- [...sort...](#sort)
  * [What if we survive?](#what-if-we-survive)
  * [A New Story of Climate Change, father-son](#a-new-story-of-climate-change-father-son)
  * [Video about his new book "Climate: A New Story", 15 min](#video-about-his-new-book-climate-a-new-story-15-min)
  * [Verschiedenes](#verschiedenes)

<!-- tocstop -->

2021
----
### Trust Your Heart
* YT: "Trust Your Heart.", 2020, 6 min
    * want to create a better world
    * know that it exists
    * but the concept for change cannot be grasped by the mind only
    * no roadmap to change
    * ...
    * do things for no other reason but what makes you come alive
        * is not all the time the same (but might be); life is dynamic
    * we are in service for life
    * "the current civilizational setup is in service to death"
    * we convert all that is alive around us into products
    * ...
    * yearning: I am here to serve, to give, to contribute

### Connecting to "Them"
* YT: "Connecting to "Them"", 2020, 8 min
    * ...
    * 4:30: other person: defenses are overcome by superior evidence and logic; that is not how anyone changes their mind
        * you have to change the conditions; the beliefs
        * totality of that persons experiences
        * if you want the beliefs, the story to change, you have to offer different experiences
    * ...
    * ...
    * "what's it like to be you?"

2020
----
### us and them; evil people?
* YT: Why People Take Things Out of Context; https://www.youtube.com/watch?v=BBUNvh5kqU0, 2020, 5 min, ::frieden
    * **Dehumanize the enemy**
    * evil people?
    * tactics of war
    * ...

### "How Much Good Can One Person Really Do?", 2020
siehe wirksamkeit-des-einzelnen.md

...sort...
----------
* Video: "**Regenerative Agriculture**: A Solution to Climate Change", https://www.youtube.com/watch?v=_P31w8E_5Zc, 2 min, 2020, ::permakultur
    * see also
        * embedded video on front page, three methods
            * No-Till farming
            * Regenerative Grazing
            * Agro-Forestry --> siehe auch "Food Forest"
        * Video about importance of **soil regeneration**: "Charles Eisenstein Keynote from Regenerative Earth Summit 2019, Boulder, CO", https://www.youtube.com/watch?v=9KiUGLfzDqE, 2020, 40 min
    * less effective? no!
    * What we need:
        * **More labor in agriculture (1950: 10 %, 2020: 1 %, The Future: 50 %)**
        * Relationship to land
            * ...
            * does not fit into an industrial model
            * ...
    * see also "Charles Eisenstein - Why We Need Community To Live In the New Story", https://www.youtube.com/watch?v=SYgjuMtGQtc, 5 min
        * die ganze Zeit unter Wasser sein und dann nach oben kommen und Luft schnappen
            * aber da ist keiner und wieder unter gehen
            * dann nochmal, und dann ist da plötzlich jemand
            * usw.
        * **community**
        * ...
        * Buch: https://en.wikipedia.org/wiki/The_Art_of_Loving, Die Kunst zu lieben, "populäres gesellschaftskritisches Werk des Sozialpsychologen Erich Fromm", 1956
            * https://de.wikipedia.org/wiki/Die_Kunst_des_Liebens (Hörbuch)
                * "Dem Werk liegt Fromms Sichtweise zugrunde, nach der Liebe Wissen und aktives Bemühen erfordert"
                * "Der Liebe müsse der höchste Stellenwert im Leben eingeräumt werden, vor Erfolg, Prestige, Geld und Macht."
                * "das Bewusstsein der Trennung der Menschen untereinander die Quelle aller Ängste und Schuldgefühle"
                * "Diskrepanz zwischen dem in der zeitgenössischen Gesellschaft aufgrund dieser Getrenntheit bestehenden Konformitätsbedürfnis und der gleichzeitig behaupteten Individualität der Gesellschaftsmitglieder"
                * "Die durch Konformität erreichte Einheit sei eine Pseudo-Einheit"
                * "Nur die Liebe eines reifen Menschen wahrt die eigene Integrität und Individualität.
                    Eine solche Liebe kann niemals auf Leidenschaft als treibender Kraft beruhen, sondern muss auf freiem Willen basieren."
                * ...
                * "Verfall der Liebe in der zeitgenössischen kapitalistischen Gesellschaft"
                    * ...
                * ... TODO
                * ...
        * see also "TAMERA, Healing Biotope 1", https://www.tamera.org/
            * "WE WORK FOR A GLOBAL **SYSTEM CHANGE** - from war to **peace**, from exploitation to cooperation, from fear to trust"
            * https://www.tamera.org/healing-of-love/
                * https://de.wikipedia.org/wiki/Dieter_Duhm
                    * "Psychologe und Buchautor. Duhm wurde vor allem als Organisator kommuneartiger Projekte bekannt"
                    * https://de.wikipedia.org/wiki/Otto_Muehl
                        * https://de.wikipedia.org/wiki/Aktionsanalytische_Organisation
                        * Gerichtsverfahren und Verurteilung
                    * https://de.wikipedia.org/wiki/Psychodrama
                    * https://de.wikipedia.org/wiki/Sabine_Lichtenfels
                        * "deutsche Autorin, Friedensaktivistin, Theologin, Mitgründerin des Friedensforschungszentrums Tamera in Portugal / ::frieden
                            und Mit-Inspiratorin des ZEGG, eine der 1000 Frauen für den Friedensnobelpreis 2005"
                        * "Buch mit dem Titel „Rettet den Sex. Ein Manifest von Frauen für einen neuen sexuellen Humanismus“"
                        * https://www.ezw-berlin.de/downloads/Materialdienst_10_1989.pdf
                            * gewaltfreie Erde
                            * "„Sexuali-tät ist das Thema Nummer Eins. Das gilt für die ganze menschliche Geschichte, für die Gesellschaft, für die Kultur und für das Wohlbefinden jedes Menschen. Eros ist umstellt von Lüge und falscher Moral, von Angst und Resignation, von Haß und Gewalt. Weltweit. Wenn dieses Thema nicht gelöst wird, bleibt alles andere auch ungelöst und unerlöst. Der Eros ist die Quintessenz zwischen Mann und Frau, das Lebenselixier, aus dem das Gute kommt oder das Böse. Hier entscheidet sich, ob auf der Erde Krieg oder Friede sein wird.""
                            * "Sexualität und Liebe sind nicht dasselbe, können es aber werden."
                            * "Liebe und Eifersucht gehören nicht zusammen"
                            * ... ...
            * http://www.loveschoolfilm.com, "LOVE SCHOOL - A visionary film about Tamera Research Village"
                * eros is missing, "a peace culture that celebrates eros"
                * https://de.wikipedia.org/wiki/Freie_Liebe
                * http://www.ianmack.com/sacred-economics/
                    * Video: https://www.youtube.com/watch?v=EEZkQv25uEs, "Sacred Economics with Charles Eisenstein - A Short Film", 2012, 12 min, ::wachstum
                        * ...
                        * ... separate from each other ... Dauer-Krise
                        * Zitat: "If you do not change direction you may end up where you are heading", Lao Tsu?, Richard Stallman
                        * ...
                        * Life is a gift
                            * "we didn't earn any of the things that keep us alive or that make life good", air, being born, a plant that provides food, the sun
                                => "people have that inborn gratitude"
                            * **gift economy** vs. money economy
                                * "**You cannot have community as an addon to a monetized life.** You have to actually need each other."
                            * Money often stops people from doing things.
                                * What am I called to do?
                                * setup big gardens for homeless people to take care of?
                                * cleanup a toxic waste site?
                                * what beautiful thing would you do?
                                * and why is it impractical to do these things?
                                * why isn't there money in those things?
                        * The shift
                            * ...
                            * ... every crises more severe than the former, our choice at which point the change will happen
                        * Adulthood
                            * ...

* Video: "Exiting the Matrix", https://charleseisenstein.org/video/exiting-the-matrix/, 2019, https://youtu.be/QQdKbpeRe-Y
    * "I was always a very unwilling participant in The Matrix, even going back to grade school."
    * ...
    * ...
    * "So I’d like to encourage you to abide in that space of unknowing for a while until a **new story** comes to you
        that really resonates with who you want to become, that isn’t preying on your fear,
        but **that is inviting you into service to something** that’s really beautiful to you.
        And I’m not even saying you’ll recognize it right away. Probably you’ll have to have a few false starts."

    * "Charles Eisenstein - Buddha at the Gas Pump Interview", 2017
        * ...
        * ca. 19 min: our societies way of dealing with problems is to find the cause and then politics goes to war against this force, e.g.
            * crime -> crimimals -> lock them up
            * terrorism -> terrorists -> kill them
            * disease -> germs -> kill the germs
            * climate change -> greenhouse gases -> cut greenhouse gases
            * agriculture
            * medicine
            * lock something up, suppress something, control something vs. **complex web of causes that we ourselves are part of**
        * ...

* "**Our New, Happy Life? The Ideology of Development**", https://charleseisenstein.org/essays/7061-2/, 2018
    * ...
    * "Like in 1984, there is something deceptive in these arguments that so baldly serve the established order."
    * "In fact, I share the fundamental optimism of Kristof and Pinker that humanity is walking a positive evolutionary path.
        For this evolution to proceed, however, it is **necessary that we acknowledge and integrate the horror, the suffering**,
        and the loss **that the triumphalist narrative of civilizational progress skips over.**"
    * "**metrics-based evaluations**, while seemingly objective, bear the **covert biases** of those who decide what to measure,
        how to measure it, and what not to measure.
        They also **devalue those things** which we **cannot measure** or that are intrinsically unmeasurable. Let me offer a few examples.
            * "Nicholas Kristof celebrates a decline in the number of people living on less than two dollars a day."
                * "Well, every time an indigenous hunter-gatherer or traditional villager is forced off the land
                    and goes to work on a plantation or sweatshop, his or her cash income increases from zero to several dollars a day."
                    * FRAGE: wurde das in der Statistik berücksichtigt?
                * "Most had lived largely outside the money economy."
            * human health, ...
            * literacy -> "destruction of oral traditions and even the extinction of entire non-written languages")
    * ... todo ...


* "**Extinction and the Revolution of Love**", https://charleseisenstein.org/essays/extinction-and-the-revolution-of-love/, 2020
    * ...
    * "That is because they recognize that **the challenge facing humanity is not “How do we sustain business-as-usual using carbon-neutral fuels?”**
        **Business-as-usual is not OK**, and switching fuels will not make it so.
        Like the anti-war radicals of the 1960s, like the anti-globalization protestors of the 90s,
        like the Occupy Wall Street occupiers, they do not aspire to modest reforms.
        They know that modest reforms do not reach deep enough.
        They recognize, whether consciously or not, that **ecocide is a feature and not a bug of the current socioeconomic system.**"
    * ...
    * ... TODO
    * ...
    * "The fossil-fuel based system [...] is woven into every facet of modern life, from medicine to agriculture to transport, manufacturing, and housing.
        Every activist must understand that a demand to get off fossil fuels is a demand to change everything,
        and that **this demand is impossible to fulfill**.
        Its goal is not impossible; a change in everything is what we are here to serve.
        But it cannot be realized as a demand, because there is **no one with the power to fulfill it**."
    * even with only regenerative power and other technology, business-as-usual is infeasible
        * https://www.counterpunch.org/2019/09/17/what-is-energy-denial/, "**What is Energy Denial?**", by Don Fitz, 2019
            * "first Earth Day of 1970"
            * "environmentalism has gone mainstream"
            * popular during the early Earth Days: “Reduce, Reuse, Recycle.”
            * "unspoken phrase of today’s Earth Day is “Recycle, Occasionally Reuse, and Never Utter the Word ‘Reduce.’”", ::genügsamkeit
                * "The concept of possessing fewer objects and smaller homes has surrendered to the worship of ecogadgets."
            * “Live simply so that others can simply live”
            * "Long forgotten are the modest lifestyles of Buddha, Jesus and Thoreau."
            * "The very **idea of re-imagining society so that people can have good lives**
                as they use less energy has been consumed by visions of the infinite expansion
                of solar/wind power and the oxymoron, “100% clean energy.”"
                    * "“clean energy danger denial.”"
            * "Obviously, fossil fuels must be replaced by other forms of energy.
                But those energy sources have such negative properties that **using less energy** should be the beginning point,
                the ending point and occupy every in-between point on the path to sane energy use."
            * "The 15 Unstated Myths of Clean, Renewable Energy"
                * ...
    * "By framing anything as a demand, **we entrench existing political power relationships.**"
        * "A demand implies a threat: “Do as I say – or else!” [...] that someone is unable to fulfill is to make them an adversary."
            * "Movements that do this tend to shrink over time, not grow.", ...
        * "Progress is won through a fight, a struggle for domination.
            Can we not see that the same domination mentality underlies civilization’s ecocide? Another kind of revolution beckons."
        * "There is a certain comfort in establishing a set of enemies as the key to solving a crisis.
            We replace a goal we don’t know how to achieve (changing everything)
            with one we do (toppling a leader, overthrowing a government, seizing political power)"
            * ...
            * "find ourselves in the engine room, we will discover we are just as incapable of throttling the engine as its previous occupant was."
        * "None of this is to say we should just give up and go home. [...] Authentic hope is not a distraction from reality,
            it is the **premonition of a possibility**. [...] we have to **step outside conventional problem-solution vicious circle**,
            in which each solution generates the same problem in another guise.
            The conventional diagnosis of the climate change problem is itself part of the problem, and so,
            therefore, are the solutions that come of it.
            Stepping outside of it, we may arrive at different demands and, more importantly,
            **ways that address the crisis that lie outside the mentality of demanding altogether.**"
    * "The incapacity of our leaders to make significant changes mirrors the incapacity of the public."
        * working class: "Appealing to personal virtue to persuade people to use less, burn less, ride less,
            is pointless when they inhabit a system that requires them to use, burn, and ride just to survive."
    * ...
    * ... todo
    * ...
    * "... That is called inclusivity. It is the gateway to a revolution of love."
    * "This is not the kind of revolution where we sacrifice some beings for “the cause” of saving the world"
        * "what has been othered, excluded, and devalued more than nature herself?"
        * "To value nature’s beings in terms of carbon,
            a measurable quantity subject to the customary cost-benefit analyses,
            is not a very big departure from valuing her beings in terms of money."
        * "What is devalued when we count carbon? What is not counted? Well, ecosystems for one."
            * "To scale up “green energy” technologies such as solar panels, batteries, wind turbines, and electric vehicles
                would require a vast expansion of mining.
                Does the reader understand what a major mining operation looks like?"
                * "and other minerals. Each takes a bite out of forests and other ecosystems, poisons water tables"
                * "The other main renewable energy technologies – hydro and biomass – are, when produced at industrial scale,
                    perhaps even more ecologically horrific than mining, leaving dislocated people and destroyed ecosystems.
                    This cannot be what we environmentalists have in mind: to convert Earth’s biota into fuel and her rivers into power plants."
                * "Those who care about this earth, I beg of you: be careful what you ask for"
                    * ...
        * "It is time to take a stand for a transition more profound than can be encompassed in carbon metrics."
            * "The deeper causes of climate change are identical to the deeper causes of most of the violence, injustice, and ecological harm on Earth."
            * "Some say that cause is capitalism, but the former socialist countries were just as rapacious as capitalist countries, if not more so."
        * "I propose that the root cause of ecocide is the world-story of modern civilization.
            I call it the Story of Separation:
            the story that holds me separate from you,
            humanity separate from nature,
            spirit separate from matter,
            and soul separate from flesh;
            that holds full beingness and consciousness to be the exclusive province of the human being,
            whose destiny is therefore to rise to domination over the mechanical forces of nature
            to impose intelligence onto a world that has none.
            The Story of Separation embeds capitalism-as-we-know-it.
            It scaffolds all of our systems.
            It mirrors the psychology that has adapted to those systems.
            Each – story, system, and psychology – perpetuates the others."
            * "The first demand of Extinction Rebellion is that the government tell the truth about climate change,
                but does it even know the truth?
                Who is prepared to tell the truth that Earth is alive?
                That the cause of ecological degradation lies in the deepest stories that civilization tells itself?
                Who is prepared to tell the truth of what the crisis therefore asks of us –
                total transformation, an initiation into a new kind of civilization?"
        * ...
        * ... todo
        * ...
        * Video: "The Biotic Pump: How Forests Create Rain", https://www.youtube.com/watch?v=kKL40aBg-7E, 2019, 3 min
        * ... and other examples of interlocked processes
        * "The phrase “disruption of ecosystems” sounds scientific compared to “harming and killing living beings.”
            But from the **Living Planet view**, it is the latter that is more accurate."
            * "A forest is not just a collection of living trees – it is itself alive.
                The soil is not just a medium in which life grows; the soil is alive. So is a river, a reef, and a sea."
            * "Just as it is a lot easier to degrade, to exploit, and to kill a person when one sees the victim as less than human,
                so too it is easier to kill Earth’s beings when we see them as unliving and unconscious already.
                The clearcuts, the strip mines, the drained swamps, the oil spills,
                and so on are inevitable when we see Earth as a dead thing, insensate, an instrumental pile of resources."
            * "**Our stories are powerful.**
                If we see the world as dead, we will kill it.
                And if we see the world as alive, we will learn how to serve its healing."
        * Living Planet vs. academic climate conferences, rooms with identical chairs
            * "Nature exists only in representation, and Earth seems alive only in theory, and probably not at all."
            * "What is real, in that world, is the numbers.” How ironic, given that numbers are the extremity of abstraction."
                * "With problems defined by numbers, the “realistic” mind seeks to solve them by the numbers too."
                * "Then I would order up more of this one and less of that one, offsetting jet travel with tree planting,
                    compensating for wetlands destruction here with solar panels there, to meet a certain greenhouse gas budget.", siehe auch paech
                * "traditional environmental issues such as wildlife conservation, saving the whales,
                    or cleaning up toxic waste get short shrift in the climate movement. “Green” has come to mean “low-carbon.”"
                * "We imply that “sustainability” means the sustaining of society as we know it, but with non-fossil fuel sources."
        * "The real threat to the biosphere is actually worse than most people, even on the left, understand"
            * "Earth is approaching death by organ failure."
            * "ten percent world"
        * "It would sure be nice to be able to blame all of that on a single cause,
            i.e. climate change. Then we could operate in the familiar territory of reductionism."
            * "Not knowing what to do is uncomfortable. [...] But not knowing is a lot better than thinking, falsely, that we know."
                * siehe auch The Big Short (Mark Twain-Zitat?)
        * "4. New Priorities"
            * ...
            * ...
            * "To perform regeneration effectively, we cannot rely on scalable formulas. Each place is unique. [...]
                Quantitative science can be part of developing this knowledge,
                but it cannot substitute for the close, qualitative observation
                of farmers and other local people who interact with the land every day through generations."
            * ...
            * ... todo ...
            * ...

* **Living Planet View**: https://charleseisenstein.org/video/the-core-themes-of-climate-a-new-story/, 6 min, 2019
    * "because I think that **current** policy and rhetoric and thinking about climate change is still heavily influenced by what I call the ‘geo-mechanical’ view,
    **which sees Earth as this fantastically complicated, wonderful machine.** That way of thinking leads us to believe that if we could tinker with the air/fuel mixture of our diesel engine, we could get it running properly."
    * "But I don’t think that that’s enough motivation to do the courageous things we have to do in order to change the direction of civilization.
        **Courage does not come from fear, really. Courage comes from love.**"
    * "When we see Earth as a Living Planet, then we understand that, actually, **even if we cut greenhouse emissions to zero**,
        if we continue to degrade the organs and tissues of our living earth, the planet will still die a death of a million cuts."
    * 4 "top priorities", siehe unten, dort eingearbeitet

### What if we survive?
* https://charleseisenstein.org/program/series/climate/
    * ...
    * Video: "**What if we survive** - Charles Eisenstein", 2019, 5 min, https://www.youtube.com/watch?v=5bwkwsO2E3s
        * ...
        * "What if we can survive? What kind of world are we summoning?" (alles in Beton und die Verschmutzung durch Filter rausfiltern?)
        * Die wichtige Frage ist nicht: "Was müssen wir tun, um (nur) zu überleben?"
            **sondern: "In was für einer Welt wollen wir leben?"**
        * die Kraft unserer Entscheidungen, die Kraft unserer Geschichten anerkennen
        * Die jungen Menschen wollen wissen, warum sie hier sind (nicht nur um einen Lebensunterhalt zu verdienen und "zu überleben").

### A New Story of Climate Change, father-son
* "A New Story of Climate Change - Charles Eisenstein at New Frontiers" - https://www.youtube.com/watch?v=MHlnAH-vHzg, 2018, 30 min
    * ...
    * ... TODO: gute Einleitung. It is not about being more clever about the things we are doing right now
        * We are being offered a new view and thinking.
    * ...
    * 3:30 **father-son-example**
        * ...
        * "oh you're right, I better take care of him"
        * ...
    * ...
    * ...
    * ...

### Video about his new book "Climate: A New Story", 15 min
* https://charleseisenstein.org/
    * “What we observe to be happening in the world says as much about ourselves as it does about the world.
      It reveals what we think is important, significant, valuable, and sacred,
      and what is irrelevant or useless too. Put another way, what we see reveals how we see.”
        "From my book, Climate: A New Story."
    * Video about his new book "Climate: A New Story", 15 min
        * andere Probleme sollten nicht außer Acht gelassen werden
            * Böden
            * chemische Verschmutzung
            * Abfall
            * Sonnencreme vs. Korallen
        * **Top priorities** / https://charleseisenstein.org/video/the-core-themes-of-climate-a-new-story/, siehe auch oben
            * 1. Protect and preserve... what is left of places that are in a pristine or close to prestine condition:
                * primary rain forests: Amazonas, Kongo, ..., wetlands, areas of oceans (this is not seen in a CO2 reductionist lens); conservation; protection
            * 2. Regeneration / Restore and heal... everything that has been damaged (soil, water, forest); regenerative agriculture; bring health back; regeneration
                * **create marine preserves that cover at least half of the oceans!**, no fishing, no interference there
                * ...
            * 3. Stop dumping poison into the world
                (toxic waste, pharaceutical waste, agriculture runoff, pesticides, sunscreen, oestrogene from anti-baby pill, antibiotics)
            * 4. Cut fossil fuel use; planet is wounded and therefore cannot handle the rapid rise of ff very well;
                side-effect of 1..3 (mine, drill, frack, pipeline, risk of oil spills)
            * and peace (::frieden)
                * template of war
                    * war on nature; overcoming of the wild
                    * part of ecoside
                    * im Krieg gibt es nur ein Ziel: den Feind zu besiegen; Umwelt ist egal
                        * war machine / war economy has different priorities than the healing of the world
        * 6:30 "Where do we put our priorities?"
            * "If we hold conflicting agendas as an individual, we create conflicting results"
            * "We have to decide what do we serving here."
            * "As long as we incinerating so much of our energy fighting each other, distrusting each other, distrusting each other, hating each other...
                we never come into the coherence necessary to serve the world because we an in service of something else"
            * "Service to life", "We have to come together as a species", "We have to unify"
        * 8:20 interconnection; what we do to the world/to others, we do to ourselves
        * "without peace there will be no healing"
            * https://en.wikipedia.org/wiki/Peaceworker
            * https://peaceworkersus.org/
                * Buch: "Waging Peace: Global Adventures of a Lifelong Activist - Authors: David Hartsough"
                    * "David Hartsough knows how to get in the way! He has used his body to block Navy ships headed for Vietnam and trains loaded with munitions on their way to El Salvador and Nicaragua. He has crossed borders to meet “the enemy” in East Berlin, Castro’s Cuba, and present-day Iran. He has marched with mothers confronting a violent regime in Guatemala and stood with refugees threatened by death squads in the Philippines.
                    Waging Peace is a testament to the difference one person can make. Hartsough’s stories inspire, educate, and encourage readers to find ways to work for a more just and peaceful world. Inspired by the examples of Mahatma Gandhi and Martin Luther King Jr., Hartsough has spent his life experimenting with the power of active nonviolence. It is the story of one man’s effort to live as though we were all brothers and sisters."
                    * todo
                    * Video
                        * "when people overcome their fear, and they realize that they have one another and they are willing to commit to non-violence and to struggle for liberation, there is nothing stopping them"
                        * money? "we have the money; we just have to decide what to do with it"
                            * war on terrorism? (which creates more terror)
                            * or schools, libraries, job training... which keep people and the youth from forming crimial gangs?
            * more on peace - siehe ::frieden; Galtung; Ebert
        * 9:30 "stop seeing someone as the enemy"
            * "and really try to understand them"
            * "and ask: what is it like to be you?"
            * "then you waging peace", "outbreak of peace anywhere contributes to the field of peace"
            * "why did you do this thing?"
                * "What are the conditions that brought you to this?"
                * "What are the conditions that let you drive this SUV, to fly that airplane, to build that oil well?"
                    * not: judging, not: making them bad people
                * "what system are you lodged in, what pressures are you under, what story do you living?"
                    * "that's called peace, asking that sincerely"
                * still you can fight, have a lawsuite or direct action; but at least you are not defaulting to fighting
                    * "What's the thing to go to war on?"
        * 12:00 any act of peace is an act of healing
        * also peace with youself; be compassionate with yourself
            * ...why...?
            * "loving questions"
            * "they are based on there must be something"
            * "I am somebody who loves the world so much; who loves life so much; ..."
                * what if all the other people you condemn are the same?
                    * "that is holding a story that invites people into it; holding a story of peace; of a healed world"
                    * "that includes the story of who people are;
                        it is not to ignore/to dismiss the thing that are causing harm, that people are doing;
                        but that's the belief: this is not really who you are"
                        * "not as an ideology, but to try to see it (and maybe you don't see it), but you can look for it"
                            * "that's peace; to look for it"
        * 16:00 "the global climate, and the social climate, and the psychic climate, and the internal climate, all of these are related"
            * "the war on nature/self/each other are all related"
            * peaceworker, love servant, servant of life, service of others ("that reminds me I am not crazy")
                * "That inspires me, yeah I can do it, too"
                * "When I see a brave person: thank you for showing me what's possible" (Dankbarkeit)
                    * "for showing what a human being can be.
                    Maybe I can do it too.
                    Even if I never see you, I can feel you. We can all fell each other.
                    That's how we build the field."

### Verschiedenes
* https://de.wikipedia.org/wiki/Charles_Eisenstein
    * Occupy-Bewegung

* YT: "Occupy Wall St. – NYC ReEnvisioning Money", https://charleseisenstein.org/video/occupy-wall-st-nyc-reenvisioning-money/, 2011
    * ...

* https://charleseisenstein.org/about/
    * "My quest had an emotional dimension as well. From an early age I sensed a wrongness in the world. Sitting in a classroom doing worksheets, part of me rebelled."
    * "This perception, abetted by a growing awareness of ecological devastation and social injustice, prevented me from whole-heartedly embracing a normal career."
    * "I didn’t know what I was searching for, but I knew that none of the usual options life presents a Yale graduate attracted me."
    * "I spent most of my 20s there, educating myself broadly (though not at all rigorously – it was more through osmosis) in Eastern spiritual traditions."
    * "I also read voraciously: books on health, nutrition, globalization, spirituality, physics, and biology."
    * "In my late 20s I entered what was to be a long period of intensifying crisis.
        It started when all my professional work became intolerable.
        It became excruciating to do work I didn’t care about.
        Even though a million reasons told me why it was irresponsible, impractical, and foolish to quit, I eventually could not make myself do it anymore."
    * ...

Articles:

* https://charleseisenstein.org/courses/unlearning-for-change-agents/
    * "We are all activists, whether we are called to discover a new story or disrupt a current one."

* https://charleseisenstein.org/courses/living-in-the-gift/
    * "Personal and collective transition from an age of scarcity and separation, to an age of abundance, community, and gift."

* https://charleseisenstein.org/essays/opposition-to-gmos/
    * "Opposition to GMOs is Neither Unscientific nor Immoral", 2018
    * ...

* https://www.goodreads.com/en/book/show/560130.The_Yoga_of_Eating
    * Weston A. Price Foundation
        * https://www.westonaprice.org/health-topics/vegetarianism-and-plant-foods/the-ethics-of-eating-meat-a-radical-view/, 2002
            * :(, see also comments
        * https://www.westonaprice.org/
            * picture of a family: "They’re happy...because they eat butter!
                They also eat plenty of raw milk, cream, cheese, eggs, liver, meat, cod liver oil, seafood,
                and other nutrient-dense foods that have nourished generations of healthy people worldwide!"
        * https://charleseisenstein.org/essays/old-fashioned-healthy-lacto-fermented-soft-drinks-the-real-real-thing/, 2003
