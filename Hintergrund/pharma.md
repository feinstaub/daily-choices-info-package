Pharma
======

<!-- toc -->

- [Hintergrund](#hintergrund)
  * [Werbung und Lobby 2010](#werbung-und-lobby-2010)
  * [Pharma-Sponsoring 2020](#pharma-sponsoring-2020)
  * [Antibiotika-Resistenzen](#antibiotika-resistenzen)
  * [Patente](#patente)
  * [Psychopharmaka für Zootiere](#psychopharmaka-fur-zootiere)
  * [Tierversuche](#tierversuche)

<!-- tocstop -->

Hintergrund
-----------
### Werbung und Lobby 2010
* Buch: ["Caroline Walter, Alexander Kobylinski - Patient im Visier - Die neue Strategie der Pharmakonzerne"](http://www.suhrkamp.de/buecher/patient_im_visier-caroline_walter_46305.html), 2010, suhrkamp
    * "Pharmafirmen manipulieren und gefährden mit irreführender und illegaler Werbung Kranke wie Gesunde. Sie versprechen tödlich Erkrankten Heilung, wo es keine gibt. Sie reden Gesunden ein, dass sie krank seien. Studien werden geschönt, Risiken verschwiegen. Das Buch enthüllt erstmals, mit welch skrupellosen Methoden die Pharmaindustrie Krankmach-Kampagnen lancieren, und berichtet über das Schicksal von Patienten, die in die Fänge der Pharmafirmen geraten."
    * Journalisten, Undercover-Recherche, "aufgedeckt, wie sich Medien, Ärzte und Politiker zu Komplizen der Pharmabranche im Geschäft mit der Ware Gesundheit machen."
    * Buchbesprechung auf Deutschlandfunk: [Verdeckte Werbung der Pharmalobby](http://www.deutschlandfunk.de/verdeckte-werbung-der-pharmalobby.1310.de.html?dram:article_id=194166)
    * Rezension: http://gutepillen-schlechtepillen.de/buchtipp-patient-im-visier/
    * Alzheimer, Multiple Sklerose, Diabetes

### Pharma-Sponsoring 2020
* "Wenn sich Ärzte verführen lassen", 2020
    * https://www.tagesschau.de/investigativ/ndr-wdr/anwendungsbeobachtungen-123.html
    * "Eine Studie zeigt erstmals, dass Ärzte Medikamente häufiger verordnen, für deren Beobachtung sie Geld von Pharmafirmen bekommen. Kritische Mediziner fordern, diese Anwendungsbeobachtungen "endlich abzuschaffen"."
    * "Eine Art von legaler Korruption"
    * "Risiko für die Patienten"
    * User Comment
        * "Daß Sie Sachkenntnis in die Debatte bringen wollen, ist an sich lobenswert.
        Aber das wollen ausgerechnet mit dem Hinweis auf zwei Webseiten erreichen,
        wo es von Public-Relations-Geschwurbel nur so wimmelt? Ist das wirklich Ihr Ernst?
        Von Medizin habe ich überhaupt keine Ahnung, dafür umso mehr von Public Relations.
        Wir könnten uns ja austauschen: Ich lerne von Ihnen Medizin, Sie von mir PR.
        Was Sie hier posten, ist nämlich nichts anderes als PR. Solche Texte gibt es
        auch als Vorlage, nennt sich im PR-Sprech "Argumentationshilfen"."

### Antibiotika-Resistenzen
* siehe antibiotika.md, Doku "Der unsichtbare Feind – Tödliche Supererreger aus Pharmafabriken"

### Patente
* 2018: R ["hat ein neues Medikament gegen Multiple Sklerose entwickelt, das 33.000 Euro kostet. Für 3000 Euro gibt es ein vergleichbares Medikament. Deutsche Ärzte dürfen es MS-Patienten aber nicht mehr verordnen."](http://www.tagesschau.de/wirtschaft/roche-ms-101.html), Kontraste-Recherche

### Psychopharmaka für Zootiere
* siehe Zoo

### Tierversuche
* ...
