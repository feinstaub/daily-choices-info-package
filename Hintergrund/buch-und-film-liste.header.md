Buch- und Film-Liste
====================

(Automatisch generiert mit generate-collections.sh)

Externe Filmelisten
-------------------
### videoproject
* https://www.videoproject.com/Everything-Connects.html
    * aus https://www.videoproject.com/kids-can-save-the-planet.html
* Into Eternity: https://www.videoproject.com/Into-Eternity.html
    * "The world’s nuclear power plants have generated an estimated 300,000 tons of high-level radioactive waste that must be safely stored for 100,000 years or more. Every year, they generate another 12,000 metric tons of high-level waste."
* ...

### kinderworld.org
* "The best videos about veganism and animal rights", https://www.kinderworld.org/videos/
    * Happy Animals
    * Animal Wisdom
    * Ethics
    * Health
    * Our Planet
        * "Planet Earth - As we eat our way to Extinction", animal agriculture is leading cause...
    * ...
* https://www.kinderworld.org/get-involved/
    * "Create the change you want to see in the world"

<!-- toc -->



<!-- tocstop -->


















