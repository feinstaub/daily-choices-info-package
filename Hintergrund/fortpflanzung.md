Fortpflanzung
=============

<!-- toc -->

- [Inbox 2020](#inbox-2020)
  * [Paul Ehrlich](#paul-ehrlich)
- [Inbox 2019](#inbox-2019)
  * [Buch: Kinderfrei statt Kinderlos](#buch-kinderfrei-statt-kinderlos)
  * [Zahlen](#zahlen)
  * [Gesellschaft](#gesellschaft)
- [Kinderwunsch](#kinderwunsch)
  * [2020](#2020)
  * [2018](#2018)
- [Biologie](#biologie)
  * [Fortpflanzung](#fortpflanzung)
  * [Reproduktion](#reproduktion)
- [Bildung](#bildung)
  * [Ungeplante Schwangerschaften durch fehlendes Wissen](#ungeplante-schwangerschaften-durch-fehlendes-wissen)
- [Nachhaltigkeit](#nachhaltigkeit)
  * [drawdown.org - Frauenrechte](#drawdownorg---frauenrechte)
  * ["Most effective individual steps"](#most-effective-individual-steps)
  * [Club of Rome](#club-of-rome)

<!-- tocstop -->

Inbox 2020
----------
### Paul Ehrlich
* https://en.wikipedia.org/wiki/Paul_R._Ehrlich, Paul R. Ehrlich
    * "Optimum Human Population Size (1994)"
        * "In this paper, the Ehrlichs discuss the 'optimal size' for human population, given current technological realities. They refer to establishing "social policies to influence fertility rates."" (https://en.wikipedia.org/wiki/Paul_R._Ehrlich#Optimum_Human_Population_Size_(1994))
        * https://en.wikipedia.org/wiki/Optimum_population
            * ...
            * "Preservation of biodiversity"
            * -> "the estimation of optimum population was to be roughly around 1.5 billion to 2 billion people"
    * "Ehrlich has argued that humanity has simply deferred the disaster by the use of more intensive agricultural techniques, such as those introduced during the Green Revolution. Ehrlich claims that increasing populations and affluence are increasingly stressing the global environment, due to such factors as loss of biodiversity, overfishing, global warming, urbanization, chemical pollution and competition for raw materials.[38] He maintains that due to growing global incomes, reducing consumption and human population is critical to protecting the environment and maintaining living standards, and that current rates of growth are still too great for a sustainable future."
    * https://en.wikipedia.org/wiki/Population_Connection
        * "advocates for improved global access to family planning and reproductive health care"

Inbox 2019
----------
### Buch: Kinderfrei statt Kinderlos
* Buch: ["Verena Brunschweiger - Kinderfrei statt kinderlos - Ein Manifest"](https://www.buechner-verlag.de/buch/kinderfrei-statt-kinderlos/), 2019
    * "begibt sich als Soziologin und Philosophin, aber vor allem als feministische und ökologische Aktivistin mitten hinein in die Tabuzone unseres gesellschaftlichen Konsenses, der sich ein Lebensglück ohne Kinder nur schwer vorstellen kann. Sie setzt sich kritisch mit dem pronatalistischen Dogma auseinander, das Politik, Kultur und Alltag durchdringt und sich in die Tiefenschichten unseres Denkens, Fühlens und Wünschens eingeschrieben hat. Sie zeigt, wer von diesem Konsens profitiert, und dass er nicht für Geschlechtergerechtigkeit in unserer Gesellschaft sorgen wird."
    * "Ihr Fazit: Deutschland braucht eine echte Frauenpolitik, keine unreflektierte pronatalistische Bevölkerungspolitik!"

### Zahlen
* https://ourworldindata.org/world-population-growth-past-future, 2019
    * "Two centuries of rapid global population growth will come to an end"
    * https://ourworldindata.org/world-population-growth

### Gesellschaft
* Bill Maher: "I didn't reproduce day"

Kinderwunsch
------------
### 2020
* "Ist es radikal, auf leibliche Kinder zu verzichten?", 2020
    * https://sz-magazin.sueddeutsche.de/freie-radikale-die-ideenkolumne/leibliche-kinder-alternativen-88347
    * "Man muss ein Kind nicht selbst austragen oder zeugen, um es zu lieben. Im Gegenteil:
        Es bringt sogar Vorteile, wenn sich mehr als zwei Erwachsene zu einer Familie zusammenfinden."
    * "Die kulturelle Norm, dass eine Frau ohne Kinder nicht vollständig ist, ist sogar so stark,
        dass die bewusste Entscheidung gegen eigene Kinder von manchen Menschen als politischer Protest verstanden wird."
    * "Warum entscheidet man sich eher gegen Kinder, weil man glaubt, die Belastung sei zu hoch,
        als nach neuen Familienformen zu schauen, in denen sich mehr als ein oder zwei Erwachsene um Kinder kümmern?"
    * "In der gegenwärtigen Struktur der Erwerbsarbeit und dem Mangel an Kinderbetreuungsmöglichkeiten,
        wären Familien mit mehr sorgenden Erwachsenen eine sinnvolle Lösung dafür, dass es sowohl Kindern als auch Erwachsenen in den Familien gut geht."
    * (...)
* https://sz-magazin.sueddeutsche.de/frauen/richtiges-alter-fuer-kinder-87870, 2019
    * "Na, wann kommt das erste Kind?"
        * "Fast jeder jungen Frau wird diese Frage gestellt – auch wenn sie nicht schwanger ist oder plant,
            schwanger zu werden. Das Schlimmste daran: An der Antwort sind die Fragenden gar nicht interessiert."
        * ...

### 2018
Frage: Welche Gründe werden hervorgebracht, warum eine Person Kinder haben möchte? Ist der Kinderwunsch biologisch/instinktiv bedingt?

* [Kinderwunsch](https://de.wikipedia.org/wiki/Kinderwunsch)
    * "Als Kinderwunsch bezeichnet man den Wunsch (oder das Bedürfnis) von Menschen Kinder zu haben."
    * "Geschichte und Soziologie"
        * "Der Kinderwunsch in seiner heutigen Form wird in der psychologischen Fachliteratur **als ein Phänomen der Moderne diskutiert**, da erst seit der Möglichkeit einer wirksamen Empfängnisverhütung eine Trennung von Sexualität und Mutter- bzw. Vaterschaft möglich wurde. Erst die Wahlfreiheit zwischen Elternschaft und gewollter Kinderlosigkeit im Rahmen der Lebensplanung ermöglichte die Möglichkeit eines Wunsches."
    * "Motive"
        * "Die Motive für einen Kinderwunsch sind vielfältig. Manche Autoren argumentieren, dass der Kinderwunsch eines Menschen **weder biologisch noch instinktiv bedingt** ist."
        * "Ein Kinderwunsch kann **selbst-, partner- oder paarbezogen, normativ oder sozial bedingt** sein."
        * "Die Ansicht, dass Kinder zu einer Familie gehören, stellt ein **normativ geprägtes Motiv** dar, der Wunsch der eigenen Eltern nach Enkeln ein soziales."

* [Familienplanung](https://de.wikipedia.org/wiki/Familienplanung)
    * "Als Familienplanung werden Maßnahmen von Paaren bezeichnet, die Zahl und den Zeitpunkt der Geburt von Kindern zu planen."
    * **"Für die Familienplanung ist die Trennung von Sexualität und Fortpflanzung entscheidend geworden"**
    * "Familienplanung als Menschenrecht"
        * "Jedem Paar wird das Grundrecht zugestanden, frei und verantwortlich über die Zahl seiner Kinder und den zeitlichen Abstand der Geburten zu entscheiden."
        * "Doch auch heute hat nicht jedes Paar die Möglichkeit, über die Möglichkeiten der Familienplanung selbst zu entscheiden."
    * "Der Einfluss von Wertewandel und Lebensplanung"
        * "Genauso entscheidend wie die Rahmenbedingungen des Staates sind der kulturelle, der soziale und der religiöse Hintergrund"
    * siehe auch [Künstliche Befruchtung](https://de.wikipedia.org/wiki/K%C3%BCnstliche_Befruchtung)

* [Ursachen von Kinderlosigkeit](https://de.wikipedia.org/w/index.php?title=Kinderlosigkeit#Ursachen_von_Kinderlosigkeit)
    * "Soziale Ursachen"
        * "Veränderte Geschlechterbeziehungen und Lebensverhältnisse"
        * "Schwierige Vereinbarkeit von Familie und Beruf"
        * "Finanzielle Belastung und sozialer Status"
    * **"Gedanken über die Zukunft"**
        * Sorgen um die Zukunft
            * "Furcht vor einer zunehmenden individuellen Existenzgefährdung im Rahmen der Globalisierung"
            * "Umgekehrt sehen einige Ökonomen im Fehlen der Angst vor der Zukunft einen Grund für die verbreitete Kinderlosigkeit"
            * "Streben nach einer Verbesserung des Lebens auf der Erde"
                * [VHEMT](https://de.wikipedia.org/wiki/Voluntary_Human_Extinction_Movement)
    * "Religiöse Erwägungen"
    * "Ungewollte Kinderlosigkeit"
* [Kinderlosigkeit](https://de.wikipedia.org/w/index.php?title=Kinderlosigkeit)
    * "Vorurteile über Kinderlose"
    * siehe auch https://de.wikipedia.org/wiki/Adoption
    * ["Bewusst kinderlos Warum es okay ist, keine Kinder zu wollen"](https://www.ksta.de/ratgeber/familie/kinderlos-gluecklich-interview-sarah-diehl-1257814), 2014

Biologie
--------
### Fortpflanzung
* https://de.wikipedia.org/wiki/Fortpflanzung
    * "Fortpflanzung ist die Erzeugung neuer, eigenständiger Nachkommen eines Lebewesens."

### Reproduktion
* https://de.wikipedia.org/wiki/Reproduktion#Biologie
    * "Bei Lebewesen spricht man von Vermehrung, von vegetativer Vermehrung oder von generativer Vermehrung, geschlechtlicher Fortpflanzung und Fortpflanzung."

Bildung
-------
### Ungeplante Schwangerschaften durch fehlendes Wissen
* siehe drawdown.org
* "Viele Schwangerschaften sind nicht geplant, manche auch ungewollt. Jährlich entscheiden sich in Deutschland knapp 100.000 Frauen für einen Schwangerschaftsabbruch." (https://www.profamilia.de/erwachsene/ungewollt-schwanger.html)
* "Jede zehnte Frau in Deutschland wird mindestens einmal im Leben ungewollt schwanger. Das hat eine repräsentative Umfrage ergeben. Gründe für eine ungewollte Schwangerschaft sind unter anderem Unwissen." (https://www.9monate.de/leben-familie/beziehung-sexualitaet/ungewollt-schwanger-id114329.html)
* ["Weltweit fast jede zweite Schwangerschaft ungeplant"](https://www.aerzteblatt.de/nachrichten/91608/Weltweit-fast-jede-zweite-Schwangerschaft-ungeplant), 2018
    * "Weltweit werden jedes Jahr fast 100 Millionen Frauen ungewollt schwanger"
    * "Das bedeutet nicht unbedingt, dass Frauen in Europa umsichtiger sind."
        * "Auch in den entwickelten Regionen der Erde sind 46 % aller Schwan­ger­schaften ungewollt."
        * "In den Entwicklungsländern waren es zuletzt 43 %."
        * "Aus dem Rahmen fällt hier Lateinamerika, wo 69 % aller Schwangerschaften ungewollt sind."
    * "Ungewollte Schwangerschaften sind nicht nur von medizinischem Interesse, weil illegale Schwangerschaftsabbrüche häufig die Gesundheit der Frauen gefährden. Auch die Kinder, die nach einer ungewollten Schwangerschaft geboren werden, erleiden Nachteile. So kam in einer US-Studie zu dem Ergebnis, das Mütter ihre ungewollten Kinder seltener stillen."
* ["Österreichischer Verhütungsreport 2015"](http://verhuetungsreport.at/2015/ungewollt-schwanger)
    * "Beinahe jede zweite Frau (48 %) war in ihrem Leben bereits in einer Situation, in der sie einen Schwangerschaftstest durchgeführt hat, weil sie vermutete, ungewollt schwanger zu sein. Bei 31 % war dies einmal der Fall, bei 17 % mehrmals."
* ["Ungewollt schwanger: Zwei Drittel haben verhütet"](https://sciencev1.orf.at/science/news/74446), 2010
    * "Hauptgrund für das häufige Versagen der Kontrazeption ist nach Angaben der Forscher die falsche oder fehlerhafte Anwendung ihrer Methoden."
    * "wünschen sich die Autorinnen der Studie mehr Einfühlungsvermögen der Ärzte, welche die Verhütungsmethoden verschreiben"
* ["Schwangerschaftsabbrüche bei Teenagern im europäischen Vergleich"](https://www.uni-landau.de/kluge/Beitraege_zur_S.u.S/europa.pdf), 2002
    * "Weltweit bringen nach einem UNICEF-Bericht jedes Jahr 15 Millionen Frauen im Teenageralter ein Kind zur Welt."
    * "Ungefähr zehn Prozent der Schwangerschaftsabbrüche, die in der Welt durchgeführt werden, entfallen auf die Altersgruppe der 15- bis 19-jährigen Frauen."
    * Europa: "Meistens tut sich die Politik der noch zu nennenden Staaten schwer, die häufigsten Ursachen zu erkennen und Strategien für eine Problemlösung zu entwickeln."

Nachhaltigkeit
--------------
### drawdown.org - Frauenrechte
* Rank 6 and 7
    * ["Family Planning"](https://www.drawdown.org/solutions/women-and-girls/family-planning)
        * "Securing women’s right to voluntary, high-quality family planning around the world would have powerful positive impacts on the health, welfare, and life expectancy of both women and their children. It also can affect greenhouse gas emissions."
        * "225 million women in lower-income countries say they want the ability to choose whether and when to become pregnant but lack the necessary access to contraception."
            * "The need persists in some high-income countries as well, including the United States where 45 percent of pregnancies are unintended."
                * Situation in Deutschland/Europa? siehe unten
        * "Honoring the dignity of women and children through family planning is not about governments forcing the birth rate down (or up, through natalist policies). Nor is it }}}about those in rich countries, where emissions are highest, telling people elsewhere to stop having children."
    * ["Educating Girls"](https://www.drawdown.org/solutions/women-and-girls/educating-girls)

### "Most effective individual steps"
* 2019: NOTE: though the result is ok, the study has methodical flaws! (ZEIT Wissen)
* siehe nachhaltigkeit.md
* 2017: ["The most effective individual steps to tackle climate change aren't being discussed"](https://phys.org/news/2017-07-effective-individual-tackle-climate-discussed.html), Institute of Physics
    * Infographik: ![](https://3c1703fe8d.site.internapcdn.net/newman/csz/news/800/2017/themosteffec.jpg)
    * "The four actions that most substantially decrease an individual's carbon footprint are: eating a plant-based diet, avoiding air travel, living car-free, and having smaller families."
    * User comments: Reply to "others should start" and "there is only little effect": "More broadly, what are you arguing when you say these are all pointless changes? That unless _one change_ solves _all the problems_, it's not worth doing? Because of course there's only so much one person can do. Changing one's single impact is a nessicary part of _everyone_ changing their impact."
* Auch hierzulande kann man den ökologischen Fußabdruck minimieren:
    * https://www.populationboom.at
        * https://www.populationboom.at/population-facts-fussabdruck.html
            * "Ökologischer Fussabdruck und Weltbevölkerung"
                * „Wenn einem der Umweltschutz tatsächlich am Herzen liegt, muss man den Ressourcenverbrauch der Industrieländer vermindern,
                statt sich über die Familiengröße fremder Frauen in fernen Ländern Gedanken zu machen”
                * Land-Grabbing
                * «Überzählig sind immer die Anderen»
        * https://www.populationboom.at/population-facts-ernaehrung.html
            * "„Überbevölkerung” als Frage der Verteilungsgerechtigkeit"
            * "„Überbevölkerung” als Argument pro Gentechnik"
            * "„Überbevölkerung” als Ausrede für das politische Versagen"
        * [Mythos Überbevölkerung - Verblüffende Fakten zur Welt von morgen Doku in HD](https://www.youtube.com/watch?v=cRBBH72j8ao), zdf info, 2016, 45 min
            * Ist das was? Graphik von Schadstoffausstoß von arm und reich.
    * Altersvorsorge?
        * [Werbung: Mein Junge, mein Stolz, ...](https://www.youtube.com/watch?v=KxkBqnkkP40)
        * [5 Hinweise](http://www.brigitte.de/aktuell/gesellschaft/entscheidungshilfe--5-hinweise--dass-du-dir-gut-ueberlegen-solltest--ob-du-kinder-bekommst-10018168.html), Brigitte, siehe auch User-Comments
    * ["Ein Kind - oder lieber doch nicht?"](http://www.brigitte.de/liebe/beziehung/kinderwunsch--ein-kind---oder-lieber-doch-nicht--10111000.html), 2016?, Brigitte
        * "Gedanken, um die die Gefühle zu sortieren"
    * https://stallman.org/articles/children.html
        * Verlinkung interessanter Artikel
            * ["Worried about the planet? Avoid that extra kid"](https://www.treehugger.com/culture/if-you-really-want-help-planet-dont-have-another-kid.html), 2017
                * "“No textbook suggested having fewer children as a way to reduce emissions, and only two out of ten mentioned avoiding air travel. Eating a plant-based diet was presented in the form of moderate-impact actions such as eating less meat, even though a completely plant-based diet can be 2 to 4.7 times more effective at reducing greenhouse gas emissions than decreased meat intake."
            * ["Want to fight climate change? Have fewer children"](https://www.theguardian.com/environment/2017/jul/12/want-to-fight-climate-change-have-fewer-children), 2017
                * "Next best actions are selling your car, avoiding flights and going vegetarian"
                * mit Graphik
            * ["The Mother of All Questions"](https://harpers.org/archive/2015/10/the-mother-of-all-questions/), 2015
        * "Natalist Pressure"
            * ...
            * http://www.art.net/studios/hackers/hopkins/Don/text/rms-vs-doctor.html, 1993
        * https://rewire.news/article/2011/08/25/i-population-problem-0
            * http://grist.org/article/2010-03-30-gink-manifesto-say-it-loud-im-childfree-and-im-proud (über Dinge reden, hilft)
        * https://www.theguardian.com/commentisfree/2016/may/24/marriage-kids-children-relationship-suffers-research
        * http://www.marieclaire.com/culture/a22189/i-regret-having-kids/
        * ["Richard Stallman on Children"](https://www.youtube.com/watch?v=RbArXHuzbrI), 16 s
    * Studie 2017
        * ["Weniger Kinder - besseres Klima"](http://www.hr-inforadio.de/programm/themen/studie-weniger-kinder---besseres-klima,co2-kinder-100.html), HR-Info, 2017
            * ["Wertschätzend leben macht glücklicher"](http://www.hr-inforadio.de/programm/themen/himmel-und-erde-wertschaetzend-leben-macht-gluecklicher,wertschaetzung-100.html), 2018
        * ["Klein-Klein beim Klimaschutz"](http://www.sueddeutsche.de/wissen/klimaschutz-es-darf-nicht-wehtun-1.3668264), 2017, süddeutsche
            * "Was kann jeder Einzelne tun, um die Erderwärmung zu begrenzen? Die meisten Empfehlungen beschränken sich auf Maßnahmen, die nur wenig bringen. Echte Einschnitte zu fordern, traut sich fast niemand."
            * "Wer aufs Auto verzichtet, keine Fernreisen macht und kein Fleisch isst, bewirkt wirklich etwas"
            * "Wer mag schon dafür werben, auf Fleisch zu verzichten?"
        * [Vorschlag eines Philosophen - Babysteuer gegen den Klimawandel?](http://www.rp-online.de/panorama/wissen/bekommt-weniger-kinder-aid-1.6230291), 2016
            * User-Comment: "Man sollte lieber eine Sterilisationsprämie in Form einer garantiertem Mindestrente einführen"
            * Buch: ["Toward a Small Family Ethic"](http://www.springer.com/us/book/9783319338699#aboutBook), 2016, von Travis N. Rieder, PhD
                * "How Overpopulation and Climate Change Are Affecting the Morality of Procreation"
        * ["Weniger fliegen, weniger Kinder kriegen"](http://www.klimaretter.info/forschung/nachricht/23405-weniger-fliegen-kein-auto-weniger-kinder-kriegen), 2017
    * http://www.ksta.de/ratgeber/familie/kinderlos-gluecklich-interview-sarah-diehl-1257814
    * ["Paul Watson: “People should have a license to have children”"](https://sputniknews.com/voiceofrussia/2012_07_15/Paul-Watson-People-should-have-a-license-to-have-children), 2012
    * ["True Detective - Rust & Martin Car Conversation Scene"](https://www.youtube.com/watch?v=A8x73UW8Hjk), 2014, 5 min
        * siehe auch "Better Never to Have Been" von [David Benatar](https://en.wikipedia.org/wiki/David_Benatar), Antinatalismus
        * siehe auch ["Soll man Kinder kriegen oder nicht?"](https://www.heise.de/tp/features/Soll-man-Kinder-kriegen-oder-nicht-3375074.html), Telepolis, 2015
    * siehe [fortpflanzung](../Hintergrund/fortpflanzung.md)

* Aktueller Zustand / Entwicklungen
    * 2017 - Wohnen in Hongkong - http://www.tagesschau.de/ausland/weltspiegel-hongkong-101.html - Video
    * "Weltkarte von Umweltkonflikten": http://www.wbgu.de/fileadmin/user_upload/wbgu.de/templates/dateien/veroeffentlichungen/hauptgutachten/jg2007/wbgu_jg2007_ex02.pdf

* Weiterer Denkanstoß mit positiver, humaner Grundhaltung: http://www.vhemt.org (The Voluntary Human Extinction Movement)
    * ["These EXIT times"](http://www.vhemt.org/TET1.pdf), 1991
        * "Volunteers to improve Quality of Earth's health"
        * "May - is a wish, not a command. Freedom of choice"
        * "We - means all of us, not just them. Unity. No enemies."
        * "Live - is what we have a right to do until we die."
        * "Long - ... Good health and social security"
        * "And - ..."
        * "Die - ..."
        * "Out - ... Respect for all life"
        * "Les Talk 'Man to man'"
            * "The most important decision for men today is whether or not we accept responsibility for preventing pregnancy and the demise of life on planet Earth."
        * "Path of  progressive awareness"
            * "Pre-awareness, shock, denial, hopeful/hopeless anger, hopeful/hopeless acceptance, vehemence"
        * "What do you say when they ask you...?"
            * "Some say If you're not part of the solution, you're part of the problem. Actually, we're both problem and solution. ..."
        * "By decreasing Births or increasing Deaths, there'll be less More. Add morality to math and you'll get VHEMT."
    * http://www.vhemt.org/biobreed.htm#babies
    * http://www.vhemt.org/whybreed.pdf
    * 2019:
        * ["Alex, a 24-year-old French man, wants to save the planet by not having children"](https://www.euronews.com/2019/01/11/members-of-vhemt-have-pledged-not-to-have-children-to-save-the-world)
            * Social Worker
        * ["The Voluntary Human Extinction Movement wants you to rethink having children"](https://www.abc.net.au/news/2018-08-05/voluntary-human-extinction-movement/10071036), 2018
            * 5-Minuten-Radio-Interview mit Les Knight
            * "the movement is designed to make people think twice about having children"
            * "If people really think about it before they procreate, they're likely not to — more and more people are choosing to not procreate these days."
            * "The odds on our success are about the same as the odds on us taking care of 10 billion people in the future."
            * "The idea is to not produce more than we already have, and that helps us to be able to take care of the people who are already here"
        * ["The Voluntary Human Extinction Movement Is Both Anti-Republican and Anti-Death"](https://www.inverse.com/article/14056-the-voluntary-human-extinction-movement-is-both-anti-republican-and-anti-death), 2016, later
    * 2018:
        * VHEMT in Portugal: http://lesuknight.blogspot.com/2018/01/vhemt-in-portugal.html
        * See user comments in https://www.theguardian.com/world/2018/jun/20/give-up-having-children-couples-save-planet-climate-crisis
            * about personal sacrifices and "give up" arguments
        * ["The Voluntary Human Extinction Movement wants you to rethink having children"](http://www.abc.net.au/news/2018-08-05/voluntary-human-extinction-movement/10071036)

### Club of Rome
* ["Geld für weniger Kinder? Der falsche Ansatz!"](https://www.clubofrome.de/single-post/1-kind-politik-club-of-rome)
