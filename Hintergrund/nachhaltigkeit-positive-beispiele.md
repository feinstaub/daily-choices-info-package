Positive Beispiele
==================

<!-- toc -->

- [Futurzwei](#futurzwei)
- [Konzeptwerk Neue Ökonomie (KNÖ) - Zukunft für alle](#konzeptwerk-neue-okonomie-kno---zukunft-fur-alle)
  * [Stell dir das Jahr 2048 vor](#stell-dir-das-jahr-2048-vor)

<!-- tocstop -->

Futurzwei
---------
von https://www.youtube.com/watch?v=FpiV6ldAhjA:

* z. B. österreischer Schuhfabrikant Heini Staudinger
    * Chef verdiene am wenigsten und die Person mit der schlechtesten Arbeit das meiste. Stimmt das?
    * https://de.wikipedia.org/wiki/Heinrich_Staudinger
        * Waldviertler
        * Firmengrundsätze:
            * 1. Scheiß di ned au! 2. Bitte, sei ned so deppat! 3. Orientiere dich an der Liebe
    * https://www.youtube.com/watch?v=IJ58EF2fg0s - "Rapunzel Events: Vortrag Heini Staudinger, "Über die Liebe, den Mut, die Wirtschaft und das Leben"", 2018
        * todo
    * https://www.fluter.de/wie-funktioniert-gemeinwohl-oekonomie
        * „Unser Wirtschaftssystem macht die Welt kaputt“, referiert er mahnend: „Je erfolgreicher, desto zerstörerischer.“
        * "bezieht selbst eigenen Angaben nach keinen festen Lohn" (klar, ist ja auch Entrepreneur)
* "das geht doch nicht" -> Futurzwei erzählt Geschichten darüber, was geht
    * Haben verlernt, über unsere eigenen Handlungsspielräume Rechenschaft abzulegen. Einfacher ist es zu sagen: ich kann nichts machen.
* Bringt Geschichten erzählen überhaupt was?
    * die meisten Menschen machen das was die meisten Menschen machen
        * ist prinzipiell auch sinnvoll
    * die Geschichten handeln immer von Ausnahmen. Der Übergang von "geht nicht" zu "geht doch".
        * wichtig, weil es Wege aufzeigt, die etwas mit **Autonomie** zu tun haben

https://futurzwei.org/magazin

* „Wir müssen uns immer konkreter ausmalen, wie alles auch ganz anders sein kann!“
    * https://futurzwei.org/article/1297 - Konzeptwerk Neue Ökonomie (KNÖ), siehe unten
        * "Mit dem Projekt „Zukunft für alle“ will das Konzeptwerk Neue Ökonomie (KNÖ) ein positives Zukunftsbild entwickeln,
            **das über die kapitalistische Wachstumsgesellschaft hinaus geht und dabei konkret vorstellbar ist.**"

* Weitere Beispiele:
    * ...

Konzeptwerk Neue Ökonomie (KNÖ) - Zukunft für alle
--------------------------------------------------
* https://konzeptwerk-neue-oekonomie.org/zukunft-fuer-alle/

### Stell dir das Jahr 2048 vor
* Videoliste auf YT: https://www.youtube.com/playlist?list=PLEIbShhFHYQ0vsMiMapn01yV5WP2LBVl8 - "Zukunft für alle – gerecht. ökologisch. machbar."
