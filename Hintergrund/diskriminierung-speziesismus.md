Speziesismus
============

<!-- toc -->

- [Definition](#definition)
- [Anerkannte Institutionen](#anerkannte-institutionen)
  * [Deutscher Ethikrat](#deutscher-ethikrat)
  * [Deutscher Ethikrat - Achtung des Tierwohls in der Nutztierhaltung, 2020](#deutscher-ethikrat---achtung-des-tierwohls-in-der-nutztierhaltung-2020)
  * [Deutsches Referenzzentrum für Ethik in den Biowissenschaften](#deutsches-referenzzentrum-fur-ethik-in-den-biowissenschaften)
  * [Institut für Theologische Zoologie - Ethik des Lebendigen - Vom Umgang mit Nutztieren](#institut-fur-theologische-zoologie---ethik-des-lebendigen---vom-umgang-mit-nutztieren)
- [Inbox](#inbox)
  * [2020 - Persona Non grata - The Great Ape Project](#2020---persona-non-grata---the-great-ape-project)
- [Vorreiter-Akteure](#vorreiter-akteure)
  * [Friederike Schmitz](#friederike-schmitz)
- [Kritische Betrachtung des Speziesismus](#kritische-betrachtung-des-speziesismus)
- [hpd](#hpd)
  * [hpd-Rezension: Grundlegende Informationen zum Veganismus](#hpd-rezension-grundlegende-informationen-zum-veganismus)
- [Einwände](#einwande)
  * [Analogie Hund](#analogie-hund)
- [Human supremacism](#human-supremacism)

<!-- tocstop -->

Definition
----------
"Speziesismus (aus Spezies (= Art) und -ismus) bezeichnet die moralische Diskriminierung von Geschöpfen ausschließlich aufgrund ihrer Artzugehörigkeit. Dies schließt ein, dass das Leben oder das Leid eines Lebewesens nicht oder weniger stark berücksichtigt wird, weil es nicht einer bestimmten Spezies, wie etwa der Spezies des anatomisch modernen Menschen (Homo sapiens), angehört."
(https://de.wikipedia.org/wiki/Speziesismus)

Anerkannte Institutionen
------------------------
### Deutscher Ethikrat
* https://de.wikipedia.org/wiki/Deutscher_Ethikrat
    * "Der Deutsche Ethikrat (Vorläufer von Juni 2001 bis Februar 2008: Nationaler Ethikrat) ist ein unabhängiger Sachverständigenrat, der „die ethischen, gesellschaftlichen, naturwissenschaftlichen, medizinischen und rechtlichen Fragen sowie die voraussichtlichen Folgen für Individuum und Gesellschaft verfolgt, die sich im Zusammenhang mit der Forschung und den Entwicklungen insbesondere auf dem Gebiet der Lebenswissenschaften und ihrer Anwendung auf den Menschen ergeben“."

* (Aufgabe aller anderen Akteure und Institutionen: dieses wahrzunehmen, zu reflektieren und Handlungen einleiten)

### Deutscher Ethikrat - Achtung des Tierwohls in der Nutztierhaltung, 2020
* https://www.ethikrat.org/mitteilungen/2020/ethikrat-fordert-staerkere-achtung-des-tierwohls-in-der-nutztierhaltung/
    * ...
    * "Die Bedingungen von Zucht, Haltung und Verwertung einschließlich der Tötung von Nutztieren müssen **mit guten Gründen gerechtfertigt** werden."
        * (Schauen wir uns doch mal die Zucht und Tötung an: ...)
    * "Aus dem Respekt vor dem Leben von Tieren folgt darüber hinaus, dass **generell acht- und sparsam mit tierlichem Leben umgegangen wird**."
        * "Dieser Grundsatz wird verletzt, wenn bestimmte Nutztiere allein aufgrund ihrer geringeren ökonomischen Erträge pauschal aussortiert und vernichtet werden."
    * "Die im **Tierschutzgesetz** festgelegten grundsätzlichen Schutzstandards stehen mit den in der Stellungnahme dargelegten ethischen Anforderungen in Einklang bzw. lassen sich zumindest in diesem Sinne verstehen."
        * "Tierwohlorientierte Vorgaben des Tierschutzgesetzes dürfen **nicht** im Wege der Interpretation
            oder der untergesetzlichen Konkretisierung **unterlaufen** werden."
        * todo: Beispiel Mannheim
    * "mit transparenten Beteiligungsstrukturen, die Tiere und ihre berechtigten Belange angemessen "repräsentieren".
        Institutionalisierte Interessenkonflikte und einseitige Besetzungen sind zu vermeiden."
    * "**Nutztierbasierte Produkte** sind in ihrer besonderen **Wertigkeit** anzuerkennen.
        Ferner sind **Ersatzprodukte zu stärken**. Die zunehmende **Nachfrage von Konsumenten**
        nach pflanzenbasierten Fleischersatzprodukten ist als **indirekter Beitrag zum Tierwohl zu begrüßen.**"
    * "Die Aufgabe, die moralisch gebotene Achtung des Tierwohls praktisch umzusetzen, **betrifft unsere gesamte Gesellschaft**.
        Um sie zu bewältigen, genügt es nicht, allein an die Verantwortung der Konsumenten zu appellieren.
        Vielmehr sind **alle relevanten Akteure in einen ergebnisorientierten Diskurs einzubinden**."
        * todo: NABU, BUND etc. als gesellschaftliche Vorreiter in dieser Debatte
    * "Eine ethisch vertretbare Nutztierhaltung ist in erster Linie eine Frage verantwortlicher Regulierung.
        Die Rolle der Politik besteht darin, einen angemessen strukturierten Transformationsprozess zu gestalten.
        Dabei ist sicherzustellen, dass **die erwartbaren Lasten, die ein solcher Strukturwandel mit sich bringt**,
        **fair verteilt werden**."
        * todo: Vorschläge von Vorreitern sind zu unterbreiten

* Vollständige Stellungnahme: https://www.ethikrat.org/fileadmin/Publikationen/Stellungnahmen/deutsch/stellungnahme-tierwohlachtung.pdf
    * ...
    * ...
    * S. 47
        * "c.Mögliche  Alternativen"
            * "**Kurz- bis mittelfristig wirksam ist die Förderung fleischfreier Ernährungsoptionen im Alltagsleben**,
                um dem Einzelnen ein stärkertierwohlorientiertes Konsumverhaltenzu erleichtern.
                **Eine Schlüsselstellung nimmt hier die Gemeinschaftsverpflegung zum Beispiel in Kitas oder Werkskantinen ein**.
                Soweit diese schon jetzt eine Essensauswahl anbieten, sollten sie verpflichtet sein,
                immer auch eine fleischfreie Option anzubieten."
            * Transparenz: "Für Verbraucher sollten **Herkunft und Herstellungsbedingungen tierlicher Produkte** nicht nur
                bei „Rohzutaten“, sondern auch bei verarbeiteten Produkten **sowie in Mensen und Kantinen klar erkennbar sein**."
    * ...
    * ...

* topagrar: https://www.topagrar.com/management-und-politik/news/ethikrat-fordert-staerkere-achtung-des-tierwohls-in-der-nutztierhaltung-12087490.html
    * "Der Deutsche Ethikrat fordert erhebliche Reformen, um künftig Mindeststandards eines unter ethischen Gesichtspunkten akzeptablen Umgangs mit Nutztieren zu erreichen."

### Deutsches Referenzzentrum für Ethik in den Biowissenschaften
* [Deutsches Referenzzentrum für Ethik in den Biowissenschaften, drze](http://www.drze.de/im-blickpunkt/tierversuche-in-der-forschung/kernfragen-der-ethischen-diskussion), abgerufen 2020
    * "Kernfragen der ethischen Diskussion - Der moralische Status von Tieren und Menschen"
        * Ausgangspunkt ist die "Frage, ob Tierversuche ethisch vertretbar sind"
        * "1. Tiere haben keinen genuinen moralischen Status: sie sind nicht um ihrer selbst willen schützenswert"
            * "[...] Gegenwärtig halten es zahlreiche Autoren für plausibler, anzunehmen, die Schädigung empfindungsfähiger Lebewesen sei als solche und gegenüber diesen selbst moralisch bedenklich. Vor diesem Hintergrund argumentieren sie, **die angemessene Behandlung von Tieren sei eine Frage der Gerechtigkeit, nicht der Barmherzigkeit**."
        * "2. Tiere haben einen eigenen moralischen Status "
            * "2.1 Tiere und Menschen haben einen vergleichbaren moralischen Status"
                * ...
            * "2.2 Der moralische Status von Tieren ist dem moralischen Status von Menschen nachgeordnet"
                * "Die Theorie, Tiere hätten zwar einen eigenen moralischen Status und ihnen gegenüber bestünden demnach direkte moralische Pflichten, ihr moralischer Status sei jedoch prinzipiell dem moralischen Status von Menschen nachgeordnet, wird gelegentlich als Doppelstandardtheorie bezeichnet."
                * "Obwohl der Doppelstandard in gewisser Weise schwer zu begründen ist (er entgeht beispielsweise nicht dem Speziesismusvorwurf, s. o.) entspricht dieses Modell wohl weitestgehend dem Alltagsverständnis von einem angemessenen Verhältnis zwischen Menschen und Tieren."
                    * Das ist also aktueller Alltag. Dennoch kann man damit keine Zirkusse und Zoos rechtfertigen, siehe unten
                * "Hiernach wären Tiere zwar um ihrer selbst willen schützenswert, ihre Interessen (an Schmerzfreiheit, Lebenserhaltung usw.) wären jedoch - falls sie mit menschlichen Interessen in Konkurrenz träten - nachrangig."
                * "Insgesamt ergäbe sich hieraus die Pflicht, zumindest dann Rücksicht auf Tiere zu nehmen, wenn dadurch keine gravierenden menschlichen Interessen verletzt würden."
                    * Ist Unterhaltung (Zirkus und Zoo) ein gravierendes menschliches Interesse?
                    * Oder ein Kuhmilch-Eis- oder Kuh-Milch-Schoko-Nachtisch?

### Institut für Theologische Zoologie - Ethik des Lebendigen - Vom Umgang mit Nutztieren
* gelöschtes Seminar (vor 2018):
    * https://klosterstift-heiligengrabe.de/seminare/theologie-kirche/ethik-des-lebendigen
    * Vom "Institut für Theologische Zoologie"
        (http://www.theologische-zoologie.de/neuigkeiten/neuigkeiten-detail/ethik-des-lebendigen-vom-umgang-mit-nutztieren/ toter Link).
        ...
        Es werden mehrere Jahrtausende von Liebe nötig sein, um den Tieren ihre Dienste an uns zu vergelten.
        (Christian Morgenstern)
    * Wenn Tiere uns dienen, und wir den Tieren – wären wir in einer lebendigen Kommunikation verbunden.
        Realität aber ist eine industrielle Tierhaltung, die Tiere als Nahrungsware züchtet, beherrscht,
        vermarktet. Daran ist nichts mehr lebendig, nichts mehr ethisch.

Inbox
-----
### 2020 - Persona Non grata - The Great Ape Project
* The Great Ape Project
    * https://de.wikipedia.org/wiki/Great_Ape_Project
        * "bestimmte Grundrechte, die derzeit dem Menschen vorbehalten sind, auch für die anderen Mitglieder
            der Familie der Hominidae (Menschenaffen (englisch) Great Apes) – also Schimpansen, Gorillas und Orang-Utans –
            zu fordern, darunter das Recht auf Leben und der Schutz der individuellen Freiheit."
    * https://www.greatapeproject.de
        * https://www.facebook.com/Great-Ape-Project-180644385376233/
        * Buch: Colin Goldner - "Lebenslänglich hinter Gittern -
            Die Wahrheit über Gorilla, Orang Utan & Co in deutschen Zoos"
            * siehe auch hpd

* https://www.deutschlandfunkkultur.de/unter-menschen-affen-und-vampiren-persona-non-grata.3720.de.html?dram:article_id=474397
    * "Außerdem zeigen sie Verhaltensweisen, die sie als individuelle Personen erkennen lassen.
        Wer aber als Person gilt, der genießt auch bestimmte Rechte.
        Das Great Ape Project fordert drei Grundrechte für Menschenaffen:
        das Recht auf Leben,
        den Schutz ihrer individuellen Freiheit
        und das Verbot der Folter.
        Was macht eine Person eigentlich aus? Ist der Menschenaffe wenigstens persona non grata?"

Vorreiter-Akteure
-----------------
### Friederike Schmitz
* 2019: "WDR 5 Funkhausgespräche: Menschenrechte für Tiere?"
    * https://www1.wdr.de/radio/wdr5/sendungen/funkhausgespraeche/funkhausgespraeche1722.html
    * ...
    * ...
* https://friederikeschmitz.de/
    * ["Staat beschönigt Tierhaltung für Schulen"](https://friederikeschmitz.de/staat-beschoenigt-tierhaltung/), 2018
        * ...
        * gute Beispiele
        * ...
        * siehe auch https://mensch-tier-bildung.de, spenden-atlas.md
* https://friederikeschmitz.de/meine-landwirtschaft-der-zukunft/, 2018
    * "1. Utopie"
        * "Kein Tier wird mehr gefangengehalten, verletzt, in seinen Bedürfnissen eingeschränkt oder getötet, nur damit wir Kuhmilch trinken, Eier oder Fleisch essen können. Wir Menschen ernähren uns gesund rein pflanzlich"
        * "Ja, wir müssen Vitamin B12 künstlich zuführen. Aber bevor Sie das jetzt unnatürlich finden, denken Sie daran, dass zur Zeit die Nutztiere das Vitamin als Futterzusatz bekommen"
        * "Natürlich sind noch ein paar Haushühner, Hausschweine und Rinder übrig. Die leben in größtmöglicher Freiheit auf einigen Lebenshöfen, wo sie gut versorgt werden und wo Menschen Ausflüge hin unternehmen, um sie zu beobachten und mit ihnen auf freiwilliger Basis zu interagieren. Die Tiere müssen nicht um ihr Leben fürchten, je nach Kapazitäten des Lebenshofes können sie auch Nachwuchs bekommen, diesen selbst versorgen und Familienleben genießen."
        * "Wir bauen alle Nahrungsmittel ökologisch an, häufig nach agrarökologischen und Permakultur-Prinzipien und natürlich weitgehend ohne den Dung von Tieren"
        * "Außerdem haben wir die Methoden verbessert, auch menschliche Ausscheidungen gefahrlos wieder aufs Feld zu bringen; insbesondere Stickstoff und Phosphor aus dem Urin sind wichtig für die nachhaltige Kreislaufwirtschaft"
        * "Der Anbau der Nahrungsmitteln ist weitgehend lokal und in überschaubaren Gemeinschaften auf Solidarprinzip organisiert."
    * "2. Gründe"
        * "dass es nicht in Ordnung ist, sie für unsere Ernährung zu verletzen, einzusperren und zu töten – mindestens dann, wenn es auch anders geht."
        * "der Tierarzt wird zum Beispiel nur geholt, wenn es sich wirtschaftlich noch lohnt."
        * "Und natürlich wird jedes so genannte Nutztier gegen seinen eigenen Lebenswillen gewaltsam getötet, sobald das eben wirtschaftlich sinnvoll ist."
        * "Aus meiner Sicht müssen wir auch aus Umwelt- und Klimagründen aus der Tierhaltung aussteigen."
        * "Und an der Idee, dass die richtige Weidehaltung von Kühen das Klima schütze, ist neuesten Studien zufolge auch kaum etwas dran. Ohne Nutztiere können wir auf weniger Land mehr Nahrung für Menschen erzeugen."
        * "Und warum kleine Gemeinschaften mit Konsens-Entscheidungen auf Solidarprinzip?"
            * "Weil kapitalistische Verhältnisse und die Steuerung über mächtige Konzerne und den Markt zu den zentralen Ursachen dafür gehören, dass so viel Naturzerstörung und Ausbeutung stattfindet."
            * "Es ist illusorisch, in Anbetracht dieser Grundstruktur darauf zu hoffen, dass die Leute in relevanter Zahl durch Aufklärung in Zukunft “ethischer” konsumieren werden."
            * "Dafür sind die Leute in der heutigen Gesellschaft mit zu viel anderem beschäftigt und wahrscheinlich auch zu eigennützig."
    * "3. Mindestforderungen und Schritte"
        * "Also wirklich die Menge reduzieren, nicht immer nur über die Umstellung auf Bio reden."
        * "Es müssen dazu alternative und attraktive Möglichkeiten für die jeweiligen Landwirt_innen geschaffen werden, die sich ja zur Zeit immer nur als Verlierer sehen."
        * "Die pflanzliche Ernährung muss also massiv gefördert werden – dafür können wir alle uns auf vielen Ebenen einsetzen, und auch „die Politik“ kann über Steuerregelungen, Förderungen, Infokampagnen und vieles mehr einiges tun."
        * ...
        * "In jedem Fall denke ich, dass wir nur dann eine Chance haben, tatsächlich wirksam etwas zu ändern, wenn sich viele Leute von unten organisieren – wie es in den Bürgerinitiativen ja schon geschieht. Genau davon brauchen wir viel, viel mehr. Wir alle, die wir uns eine gerechtere und schönere Welt wünschen, für Menschen und auch für Tiere und die Natur, wir müssen da selber mit anpacken. Wir dürfen uns nicht auf die Parteien, schon gar nicht auf die Wirtschaft verlassen. Wir müssen selber den Wandel voranbringen – sowohl durch Protest und Widerstand, als auch durch Aufbau von Alternativen."

* "Veganismus - "Man darf sich nicht für Grausamkeit entscheiden können"", 2017
    * https://www.zeit.de/entdecken/2016-12/veganismus-vegetarische-ernaehrung-lobby-tierrechte-tierhaltung
    * "Beide kämpfen dafür, dass wir alle bald vegan leben. Sebastian Joy vertraut auf die Vernunft der Konsumenten, Friederike Schmitz will die Revolution. Ein Streitgespräch"
    * knapp 1000 Kommentare
        * "man sollte durchaus über häufigkeit von fleischkonsum, herkunft und qualität statt quantität diskutieren, aber entspannt, ohne zeigefinger und ideologie."
            * Welche Ideologie? Was ist das? Und warum?

* siehe auch "konkurrenz-von-trog-und-teller"
* Videosammlung: https://friederikeschmitz.de/video/
    * Doku: ["Gier auf Tier: Die scheinbar unstillbare Lust auf tierische Nahrungsmittel ZDF Scobel"](https://www.youtube.com/watch?v=p75ttktrsUk), 2017, 1 h
        * Vorstellung des Films "The End of Meat"
        * Berichte von Ist-Zuständen und Entwicklungen in der Tierhaltung
        * Verbrauch in D. leicht gesunken; Produktion steigt stetig
        * Entwicklungsländer holen im Fleischkonsum im weiter auf
        * Preisdruck durch Billigproduktion auf Kosten von Tieren und Menschen
        * viel Export von Schweinefleisch nach China
        * Mitverursacher: die großen Handelsketten
        * ...
        * Erfolgreicher Slogan einer Marketing-Agentur: "Fleisch ist ein Stück Lebenskraft"
            * hinterfragt...
        * im Gespräch:
            * Friederike Schmitz
            * Prof. Dr. Harald Grethe von der Humboldt-Universität Berlin: ["Weniger Tierhaltung, weniger Fleisch und Milliarden für Umbau"(https://www.topagrar.com/news/Home-top-News-Prof-Grethe-Weniger-Tierhaltung-weniger-Fleisch-und-Milliarden-fuer-Umbau-3816636.html), 2016
            * Frage: Brauchen wir Fleisch?
            * ...
            * Zahlen: Marktanteil von ökologisch erzeugtem Schweine-, Hühner- und Rindfleisch (Koppelprodukt zur Milch) bei unter 2 %.
                * vs. öffentliche Wahrnehmung über Ausmaß vs. Bio = gut für Tiere
            * Verteilung der Tierkrankheiten (Produktionskrankheiten) ist bei konv. und bio ähnlich. Auch bei großen und kleinen Betrieben ist die Tiergesundheit ähnlich verteilt. Formales Platzangebot ist nicht maßgeblich für Tiergesundheit und Tierwohl.
            * ...
        * Ausflug zum Gülleausbringen bei einer der größten Schweinmastanlagen Deutschlands in Tornitz
            * Umweltfolgen etc.; Gegenüberstellung von Hundekot
            * Reporter werden gleich fotographiert
            * Bürgerinitiative gegen Ausbau der Mastanlagen
            * Bilder aus Ställen von Animal Rights Watch (krass enge Boxen). Klage. Verfahren eingestellt. Förderung durch Politik.
            * Gülle verseucht Trinkwasser mit giftigem Nitrat
            * ...
            * Massenhafter Antibiotika-Einsatz bei der Hühnerhaltung
                * Das Robert-Koch-Institut will sich dazu nicht äußern! "Die Frage sei zu politisch." ?!
            * ...
        * ...
    * Vortrag an einer Uni: „Von der Tierethik zur Tierpolitik: Argumente und Handlungsmöglichkeiten“ - Hamburg 2018
        * https://www.konsumentenbund.de/
        * Effektiver Altruismus: https://nachhaltigkeit.unisg.ch/de/studentischesengagement/ea-at-hsg
    * ...
    * Wenn alle (oder auch nur einige) negativen Aspekte der modernen Tierhaltung eingepreist würden, dann wären die Produkte wesentlich teurer und der Markt würde dafür sorgen, dass davon wesentlich weniger konsumiert würde.
    * ...

Kritische Betrachtung des Speziesismus
--------------------------------------
* drze: http://www.drze.de/im-blickpunkt/tierversuche-in-der-forschung/module/speziesismus
    * ...

* https://utopia.de/ratgeber/speziesismus-was-hinter-dem-begriff-steckt/, 2020
    * ...
    * https://www.peta.de/speziesismus
        * ... gute Erklärung? ...
    * https://www.cicero.de/kultur/tierethik-tiere-haben-keine-rechte/58419, 2004
        * ...
        * "wenn für Tiere dasselbe Grundrecht auf Leben wie für Menschen gefordert wird"
            * (wird denn 'dasselbe' Grundrecht gefordert? Oder ein auf Tiere angepasstes?)
            * "Bei dieser Forderung werden nämlich immer wieder gleich zwei äußerst wichtige relevante Tatsachen übersehen"
                * "Erstens leben Tiere immer in der Gegenwart bzw. unmittelbaren Zukunft und haben nicht annähernd
                    das weitreichende Überlebens- oder Weiterlebensinteresse, das wir Menschen haben.
                    * (was ist mit Kleinkindern, die auch in der Gegenwart leben?)
                * "Und zweitens würden unsere zahlreichen Nutztiere – anders als die Wildtiere –
                    ja gar nicht erst zur Welt kommen ohne die menschliche Nachfrage nach Fleisch."
                    * (wieso ist das relevant?)
        * "Ich kann beim besten Willen nicht erkennen, warum ich es für unmoralisch halten soll,
            Fleisch zu essen – _sofern_ die Tiere bereits zum Fleischverzehr erzeugt,
            artgerecht gehalten sowie schmerzlos getötet wurden."
            * (was bedeutet hier 'sofern'?)
            * https://de.wikipedia.org/wiki/Norbert_Hoerster, 1937
                * "deutscher Jurist und Philosoph", "lehrte von 1974 bis 1998 Rechts- und Sozialphilosophie an der Universität Mainz"
                * "Den Begriff der Menschenwürde als Kriterium der Ethik lehnt Hoerster ab, da dieser Begriff eine Leerformel sei, mit der sich beliebige Werte verbinden lassen.
                    Stattdessen vertritt er eine Interessenethik, wonach nicht die Würde, sondern die elementaren Interessen der Menschen
                    (und bis zu einem gewissen Grad der Tiere) zu schützen sind."
                * "von 2004 bis 2011 im Beirat der Giordano-Bruno-Stiftung."
                    * Wikipedia: "Grundrechte für Menschenaffen (2011)"
                * taz: "Ethiker zu tierleidfreier Ernährung: „Menschen dürfen Tiere essen“", 2016
                    * ...

* http://www.uffl.org/pdfs/vol27/UFL_2017_Lu.pdf
    * "Is Speciesism Like Racism and Sexism?", 2017
    * "I argue that once we properlyunderstandwhatmakes racismand sexismwrongwe will see that “speciesism” is fundamentally disanalogous to those injustices.
        Unlike racism and sexism, the acknowledgment of the moral value of all human beings reflects a true
        judgment about human nature as the rational grounds for the moral dignity of persons."
    * ...todo... (schauen, ob es da um Personen geht und ob Tiere auch als Personen gelten können)
    * ...
    * Conclusion
        * ...
        * "In theend, speciesismisnotonlynotlikeracismorsexism;itisaconsequentoftheveryrecognition oftheonlypossible (natural) grounds for moral status."

* https://cpb-us-w2.wpmucdn.com/campuspress.yale.edu/dist/7/724/files/2016/03/Whats-Wrong-with-Speciesism-299h6xu.pdf
    * "What’s Wrong with Speciesism? (Society of AppliedPhilosophy Annual Lecture 2015)"
    * ...
    * "But I do know this. Our inclination to treat humans as though we are special is no mere prejudice.
        Despite what Singer says, there is a significant philosophical view at workhere — one worthy of careful further investigation."

* https://fewd.univie.ac.at/fileadmin/user_upload/inst_ethik_wiss_dialog/Pluhar__E._1988._Speciesism._A_Form_of_Bigotry_or_a_Justified_View.pdf
    * "SPECIESISM: A FORM OF BIGOTRY OR AJUSTIFIED VIEW?", 1988
    * ...

* http://www.bbc.co.uk/ethics/animals/rights/speciesism.shtml
    * todo

hpd
---
### hpd-Rezension: Grundlegende Informationen zum Veganismus
* ["Grundlegende Informationen zum Veganismus – in aufklärerischer Weise"](https://hpd.de/artikel/grundlegende-informationen-zum-veganismus-aufklaererischer-weise-15959), 2018
    * "Der bekannte Ernährungswissenschaftler Claus Leitzmann legt auf der Basis neuerer Forschungen eine kurze Einführung und Überblicksdarstellung zum "Veganismus" vor. Der Autor präsentiert dabei die wichtigsten Informationen auf engem Raum und korrigiert dabei so manches weit kursierende Vorurteil zum Thema."
    * ...
    * siehe auch Über den Autor
    * siehe auch User-Comments für interessante Diskussion über Ethik und Moral
        * ...
        * "Es gibt einen Kreis moralischer Akteure. Nichtmenschliche Tiere gehören nicht in diesen Kreis."
            * "- Auch menschliche Säuglinge, Kleinkinder und geistig schwer Behinderte gehören nicht in diesen Kreis. Dennoch sind sie ethische Objekte und zwar schlicht deshalb, weil sie - wie auch ein Großteil nichtmenschlicher Tiere - leidensfähig sind."
            * "Menschenrechte sind für Menschen, und auch nur für die, die die Menschenrechte anderer achten -unabängig davon, ob diese anderen Tiere nutzen oder nicht."
                * "- Das ist falsch. Die **Menschenrechte** gelten bedingungslos - sogar für die grausamsten Verbrecher."

Einwände
--------
### Analogie Hund
* "Es ist doch schon gut, wenn ich weniger Fleisch kaufe und nur Bio."
    * Das ist ein Forschritt; gut gemacht. Nur ist da noch großes Potential. Beispiel:
    * Wenn ich früher meinen Hund täglich geschlagen habe und nun nur noch 1x / Woche.
        Was würde wohl die Nachbarin sagen?

Human supremacism
-----------------
siehe dort
