Notizen zu Wissenschaft
=======================

<!-- toc -->

- [Gute wissenschaftliche Praxis](#gute-wissenschaftliche-praxis)
- [Qualität](#qualitat)
- [Desinformation](#desinformation)
  * [Desinformation, PLURV und Umgang mit Verschwörungsmythen](#desinformation-plurv-und-umgang-mit-verschworungsmythen)
  * [2020: Die Überzeugungstricks von Sucharit Bhakdi analysiert](#2020-die-uberzeugungstricks-von-sucharit-bhakdi-analysiert)
  * ["Die Wissenschaft ist sich uneins"](#die-wissenschaft-ist-sich-uneins)
  * [Buch](#buch)
  * [Homöopathie](#homoopathie)

<!-- tocstop -->

Gute wissenschaftliche Praxis
-----------------------------
* Gute wissenschaftliche Praxis, Werte etc.
    * Wissenschaft: https://de.wikipedia.org/wiki/Wissenschaft
    * wissenschaftliche Methode: https://de.wikipedia.org/wiki/Wissenschaft#/media/File:Wissenschaftliche_Methode.svg
    * Werte: https://de.wikipedia.org/wiki/Wissenschaft#Werte_der_Wissenschaft

Qualität
--------
* 2017: Beispiel wie bestimmte Geld-Konstrukte die Qualität verschlechtern können, indem falsche Anreize gesetzt werden
    * Verlage publizieren gegen Geld (Distribution durch Open Access dann frei) => bei der Eingangskontrolle herrscht ein Interessenskonflikt, der von schwarzen Schafen genutzt wird (minimale oder keine Eingangskontrolle, damit viel Geld reinkommt)
    * siehe http://faktenfinder.tagesschau.de/fachzeitschriften-fake-news-101.html, 2017, "Wie konnte ein Nonsens-Text [...] den Weg in eine wissenschaftliche Fachzeitschrift finden?"

Desinformation
--------------
### Desinformation, PLURV und Umgang mit Verschwörungsmythen
* https://www.klimafakten.de/meldung/p-l-u-r-v-dies-sind-die-haeufigsten-desinformations-tricks-von-wissenschafts-leugnern
    * "P-L-U-R-V: Dies sind die häufigsten Desinformations-Tricks von Wissenschafts-Leugnern"
    * mit Infographik
    * mehr: https://skepticalscience.com/PLURV-Taxonomie-und-Definitionen.shtml

* https://www.volksverpetzer.de/hintergrund/corona-fake-reaktion/, 2020

* siehe auch nachhaltigkeitskommunikation.md

### 2020: Die Überzeugungstricks von Sucharit Bhakdi analysiert
* https://www.volksverpetzer.de/analyse/bhakdi-ueberzeugungstricks/

### "Die Wissenschaft ist sich uneins"
* siehe z. B. https://de.wikipedia.org/wiki/Oregon-Petition
    * "Die Oregon-Petition ist die gebräuchliche Bezeichnung für eine Erklärung gegen das Kyoto-Protokoll als Teil der Klimaschutzpolitik, die vom Oregon Institute of Science and Medicine (OISM) 1999 herausgegeben wurde."
    * "Die Petition wird als eine politische Desinformationskampagne betrachtet"
    * "Daher gilt die Oregon-Petition als typisches Beispiel für die seit den 1970er Jahren zunächst von der Tabakindustrie eingesetzte Strategie, falsche Experten für eigennützige Zwecke anzuführen."
    * "führen die Petition noch heute als vermeintlichen "Beleg" dafür an, dass es unter Klimaforschern keine Einigkeit über die Existenz der menschengemachten globalen Erwärmung gebe."
    * "Die Petition dient zudem als Beispiel für weitere ähnliche Aktionen."

### Buch
* Buch: [Der Tollhauseffekt](https://www.dgs-franken.de/medien/tollhauseffekt/), 2018
    * "Der Tollhauseffekt handelt vom Spannungsfeld zwischen Politik und Wissenschaft, wobei es vor allem um die klimaforschende Wissenschaft geht. Das Buch beschäftigt sich mit den Manipulationen sogenannter Klimaskeptiker, deren Pseudo – und Antiwissenschaft, die, allzu oft als seriöse Wissenschaft verkleidet, anerkannte Erkenntnisse und physikalische Tatsachen leugnet und zu untergraben versucht."

### Homöopathie
* "Globuli-Lehre an Unis und in Apotheken - Medizinstudierende fordern Ende der Sonderregeln für Homöopathie", 2020,
    * https://medwatch.de/2020/06/04/medizinstudierende-fordern-ende-der-sonderregeln-fuer-homoeopathie/
    * **medwatch**
        * https://de.wikipedia.org/wiki/MedWatch_%28Online-Magazin%29, siehe auch Aluminium
    * Positionspapier: https://www.bvmd.de/fileadmin/user_upload/Grundsatzentscheidung_2020-05_Homo%CC%88opathie.pdf
* Kritik: https://de.wikipedia.org/wiki/Hom%C3%B6opathie#Kritik_an_der_Hom.C3.B6opathie
* 2020: YT: "Homöopathie - Sanfte Medizin oder Hokuspokus? | Doku | ARTE", https://www.youtube.com/watch?v=vfRbyRdiTE4
    * ...
    * Psychologe: Studie mit Placebos, wo der Patient weiß, dass es sich um ein Placebo handelt
        * z. B. Rückenbeschwerden: diese Salbe enthält keinen chemisch wirksamen Stoff, schmieren Sie sie dennoch regelmäßig auf => positver Effekt nachweisbar
        * Die Wirksamkeit von Placebos sind wissenschaftlich gut erforscht und sollten somit zukünftig systematisch in der Behandlung eingesetzt werden (derzeit: kaum)
    * ...
* Lobbyarbeit: https://de.wikipedia.org/wiki/Hom%C3%B6opathie#Hom.C3.B6opathie-Lobbyarbeit
    * ["Schmutzige Methoden der sanften Medizin "](http://www.sueddeutsche.de/wissen/homoeopathie-lobby-im-netz-schmutzige-methoden-der-sanften-medizin-1.1397617), 2012
        * "Wie viele Forscher vor ihm auch kam er zu dem Ergebnis: Die Zuckerkügelchen wirken nicht besser als eine Scheinpille."
