Wachstum
========

<!-- toc -->

- [Inbox](#inbox)
  * [2020 Politik, Preise, Technik als Allheilmittel, Wachstum, ::gerechtigkeit](#2020-politik-preise-technik-als-allheilmittel-wachstum-gerechtigkeit)
  * [2019](#2019)
  * [2019: Digitalsteuer](#2019-digitalsteuer)
- [Problembewusstsein](#problembewusstsein)
  * [Inbox 2019 / Suffizienz](#inbox-2019--suffizienz)
  * [Grenzen des Wachstums, 1972](#grenzen-des-wachstums-1972)
  * [Wir sind dran, 2017](#wir-sind-dran-2017)
  * [Attac](#attac)
  * [polyp-Cartoons](#polyp-cartoons)
- [Politik](#politik)
- [Lösungswege](#losungswege)
  * [Wuppertal Institut](#wuppertal-institut)
- [Rebound-Effekt / Backfire](#rebound-effekt--backfire)
- [Verschiedenes](#verschiedenes)
  * [Wachstum auf Kosten der Schweine](#wachstum-auf-kosten-der-schweine)

<!-- tocstop -->

Inbox
-----
### 2020 Politik, Preise, Technik als Allheilmittel, Wachstum, ::gerechtigkeit
Lehren aus Corona

* https://www.klimareporter.de/international/vorbeugen-ist-besser-als-heilen
    * "Viel Zeit ist vertan worden, aber ein treffliches afrikanisches Sprichwort besagt:
        "Der beste Moment, einen Baum zu pflanzen, war vor Jahrzehnten, der zweitbeste ist heute.""
    * "Verzwickt, schmerzhaft und teuer wird es werden, aber lange nicht so wie weitere Lösungsverschleppung."
    * "Covid-19 enthält Lehren für die Begrenzung der Erderhitzung, zum Beispiel folgende fünf."
        * "Fakten zählen"
        * "Hohe staatliche Kompetenz und Kapazität sind unabdingbar"
        * "Die Erderhitzung ist nur politisch in den Griff zu bekommen"
        * "Preise müssen Kosten reflektieren"
        * "Technologische Innovation ist kein Allheilmittel", ::techniküberschätzung

### 2019
* ["20 Jahre Cittàslow - Der Erfolg der langsamen Städte"](https://www.tagesschau.de/ausland/cittaslow-101.html), 2019
    * "McDonald's ist tabu - und das Gemüse kommt direkt vom Feld in die Küche: Die Cittàslow-Bewegung will Städte lebenswerter machen und ihre Einzigartigkeit erhalten. Eine italienische Gemeinde zeigt, wie es geht."
    * https://de.wikipedia.org/wiki/Citt%C3%A0slow
        * "1999 in Italien gegründet wurde. Sie wurde inspiriert von der Slow-Food-Bewegung. Hauptziele sind die Verbesserung der Lebensqualität in Städten und das Verhindern der Vereinheitlichung und Amerikanisierung von Städten, in denen Franchise-Unternehmen dominieren."
    * http://www.cittaslow.org/

### 2019: Digitalsteuer
* "Frankreich verabschiedet Digitalsteuer", 2019
    * https://www.tagesschau.de/ausland/frankreich-digitalsteuer-usa-101.html
    * https://www.heise.de/newsticker/meldung/Frankreich-verabschiedet-nationale-Digitalsteuer-4468108.html

Problembewusstsein
------------------
### Inbox 2019 / Suffizienz
* ["Suffizienz – Der Weg in eine nachhaltige Gesellschaft?"](https://kaufnix.net/suffizienz-der-weg-in-eine-nachhaltige-gesellschaft/)
    * "Wer die Zukunft der Menschheit sichern will, muss weiteres Wirtschaftswachstum verhindern. Um Klimaschutz zu erreichen, muss Suffizienz statt Wachstum zur persönlichen, politischen und ökonomischen Prämisse werden."
    * "Effizienz, Konsistenz und Suffizienz können nur dann zu einer nachhaltigen Entwicklung beitragen, wenn sie als komplementäre Strategien betrachtet werden."
        * Konsistenz = Anderes produzieren, Kreislaufwirtschaft
        * Suffizienz = Verhaltensänderung, weniger produzieren und konsumieren
    * "Problematisch wird das erst dann, wenn die gesteigerte Effizienz zu noch mehr Konsum und Wachstum führt. Eine solche Entwicklung bezeichnet man als Rebound-Effekt."
    * "Während ökonomische Effizienz in der Regel Kosteneffizienz bedeutet, dient ökologische Effizienz vornehmlich dem Erhalt natürlicher Ressourcen."
    * "Die junge Klimaaktivistin Greta Thunberg hat eines richtig erkannt: „Anstatt nach Hoffnung zu suchen, suchen Sie nach Handlungsmöglichkeiten. Dann – und nur dann – wird die Hoffnung kommen.“"
    * ["Die Wachstumslüge"](https://www.sueddeutsche.de/politik/aussenansicht-die-wachstumsluege-1.4345588), Febr. 2019
        * "Ein mustergültiges Beispiel dafür ist der kürzlich veröffentlichte Abschlussbericht der sogenannten Kohlekommission. Ganze 149-mal kommt darin das Wort "Wachstum" vor, davon 128-mal in direktem Zusammenhang mit "Wohlstand" und/oder "Innovation", immer in positivem Kontext, nie kritisch reflektiert. Kein einziges Mal finden sich dagegen die Wörter "Suffizienz", "Anthropozän" oder "Verzicht"."

### Grenzen des Wachstums, 1972
* https://de.wikipedia.org/wiki/Die_Grenzen_des_Wachstums
    * https://en.wikipedia.org/wiki/David_Attenborough
        * "The growth in human numbers is frightening. I've seen wildlife under mounting human pressure all over the world, and it's not just from human economy or technology. Behind every threat is the frightening explosion in human numbers. I've never seen a problem that wouldn't be easier to solve with fewer people – or harder, and ultimately impossible, with more."
    * https://de.wikipedia.org/wiki/Kenneth_Ewart_Boulding
        * "entwickelte unter anderem den Begriff der Soziosphäre"
        * Zitat: „Jeder, der glaubt, exponentielles Wachstum kann andauernd weitergehen in einer endlichen Welt, ist entweder ein Verrückter oder ein Ökonom.“

### Wir sind dran, 2017
* Buch: ["Wir sind dran. Club of Rome: Der große Bericht"](https://www.randomhouse.de/Buch/Wir-sind-dran-Club-of-Rome:-Der-grosse-Bericht/Ernst-Ulrichvon-Weizsaecker/Guetersloher-Verlagshaus/e529351.rhd), von Ernst Ulrich von Weizsäcker, Anders Wijkman, "Was wir ändern müssen, wenn wir bleiben wollen. Eine neue Aufklärung für eine volle Welt", 2017
    * ...
    * S. 141 ?: Wachstum => erfordert Automatisierung => erzeugt Arbeitlosigkeit (todo)

### Attac
* https://www.jenseits-des-wachstums.de/index.php?id=76248
    * "Es war einmal… das Wachstum. - Es hat geholfen, extreme Armut und existenziellen Mangel zu überwinden. Heute ist das Streben nach ›Immer mehr‹ jedoch ein Selbstzweck, der in reichen Ländern mehr schadet als nützt. Die fixe Idee des ›ewigen Wachstums‹ zerstört Mensch und Natur. Sie ist eine Dinosaurier-Ideologie – und Dinosaurier sterben aus."
    * inkl. Cartoon (get yourself a proper job)
        * mehr Cartoons auf https://polyp.org.uk/consumerism_cartoons/sscon1.html
    * https://www.jenseits-des-wachstums.de/startseite/ mit Kurzfilm von Attac und Heinrich-Böll-Stiftung

### polyp-Cartoons
artwork.md

Politik
-------
* Angesichts der aufgezeigten Grenzen des Wachstums: was kann man tun, damit die Politik nicht weiter unreflektiert auf Wachstum setzt?
* **Wie kann eine ehrliche Kommunikation mit dem Bürger aussehen, die das Jetzt einschränkt, um in Einklang mit dem Morgen zu sein?**
    * Wie kann die Politik hier neue Impulse setzen?
* Siehe auch denkweise.md/Glaubenssätze; vermeintliche Ohnmacht des Einzelnen: wirksamkeit-des-einzelnen.md

Lösungswege
-----------
### Wuppertal Institut
* https://wupperinst.org/themen/wohlstand/
    * "Ungehemmtes Wirtschaftswachstum und die Übernutzung der Ressourcen lassen sich technisch längst nicht mehr in den Griff bekommen. Neue Konsummuster müssen her und soziale Innovationen, die die Entwicklung des Wohlstands von der ökologischen Belastung entkoppeln."
    * https://wupperinst.org/leistungen/
        * Ernst Ulrich von Weizsäcker
            * Umweltschädliches Verhalten teurer machen
    * https://wupperinst.org/abteilungen/nachhaltiges-produzieren-und-konsumieren/
        * "Unsere Vision ist die einer nachhaltigen, ressourcenleichten Wirtschaft und Gesellschaft, in der Produkte und Dienstleistungen hohe Lebensqualität bieten, global oder vor Ort fair produziert werden und die Umwelt schonen.
        Jeder Mensch verbraucht dann 8 Tonnen Natur im Jahr, statt 40 Tonnen in Deutschland
        oder statt andererseits 2 bis 4 Tonnen wie in den Entwicklungsländern Burundi, Ruanda oder Bangladesch"
    * https://wupperinst.org/a/wi/a/s/ad/4501/ - "Streitpunkt Wachstum"
        * "vorsorgeorientierte Postwachstumsposition"
        * "Die Konferenz "Herausforderungen Wachstumsunabhängigkeit" lotet zu diesem Zweck am 5. November 2018 in Berlin politische Implikationen aus."
        * "Das Wohlergehen weltweit hängt davon ab, ob es gelingt, die ökologischen Grenzen des Planeten einzuhalten."
        * "schlagen die Autoren ein vorsorgeorientiertes Vorgehen vor: Wachstumsabhängigkeit müsse möglichst gemindert und so der Wachstumsvorbehalt abgeschwächt werden, der ambitionierte umweltpolitische Vorschläge bisher oft ausbremst"
        * "Drei Forderungen an die Politik"
            * "umweltschädliche Effekte von Produktion und Konsum insbesondere durch marktbasierte Instrumente internalisiert werden"
            * "neue Pfade der gesellschaftlichen Entwicklung auszuloten"
            * "sollte es sich die Politik zu einem zentralen Ziel machen, zu prüfen, wie gesellschaftliche Institutionen und Prozesse unabhängiger vom Wachstum werden können"

Rebound-Effekt / Backfire
-------------------------
* https://de.wikipedia.org/wiki/Rebound-Effekt_(%C3%96konomie)
    * "Mit Rebound-Effekt (englisch für Abprall- oder Rückschlageffekt) werden in der Energieökonomie mehrere Effekte bezeichnet, die dazu führen, dass das Einsparpotenzial von Effizienzsteigerungen nicht oder nur teilweise verwirklicht wird. Die Effizienzsteigerung sorgt dafür, dass der Verbraucher weniger Ausgaben hat und deshalb weitere Produkte erwerben kann. Führt die Effizienzsteigerung gar zu erhöhtem Verbrauch (das heißt zu einem Rebound-Effekt von über 100 Prozent), spricht man von Backfire."
    * "Transformations-Effekt: Technische Effizienzsteigerungen verändern das Konsumverhalten, was sich auf Infrastrukturen, soziale Normen und so weiter auswirkt. Wird beispielsweise der Verkehr effizienter, verändern sich Siedlungsstrukturen, kleine Läden verschwinden und Einkaufszentren entstehen, was schließlich wieder zu einem bestimmten Verkehrsverhalten zwingt."
    * "Mentaler Rebound-Effekt: Verschiedentlich führen Einsparungen durch effizientere Technologien zur moralischen Selbstlegitimierung (Moral licensing) von zusätzlichem Konsum. Steigen Autofahrer auf ein gasbetriebenes Fahrzeug um, können diese mit gutem Gewissen mehr Gas geben oder auch weitere Strecken zurücklegen. Ein Teil der möglichen Einsparungen wird somit durch einen höheren Verbrauch kompensiert."
    * "Als Lösungsstrategie wird die Kopplung von Effizienz-Förderung und Abgaben auf die Nutzung einer Ressource bevorzugt. Durch schrittweise Erhöhung der Abgaben könnten Rebound-Effekte teilweise bis vollständig vermieden werden."
    * "Häufig beobachtet wird ein Zeit-Rebound-Effekt: So führen schnellere Verkehrsverbindungen dazu, dass weitere Strecken zurückgelegt werden; zeitsparende Haushaltsgeräte wie Waschmaschinen verändern die sozialen Standards (es wird mehr gewaschen usw.)."

Weitere Beispiele:

    * 2018: ["Kohlendioxid-Belastung Mehr Autos, mehr PS, mehr CO2"](https://www.tagesschau.de/wirtschaft/autoverkehr-umweltbelastung-101.html)
        * "Der Autoverkehr in Deutschland belastet die Umwelt zunehmend mit Kohlendioxid. Das liegt vor allem an vielen Neuwagen mit immer leistungstärkeren Motoren - aber auch an der puren Zahl der Autos."

    * Glühbirne: https://www.gluehbirne.ist.org/rebound.php

Schlussfolgerung? - Reine Steigerung der Effizienz ist nicht der ganze Weg.

Verschiedenes
-------------
### Wachstum auf Kosten der Schweine
* siehe deutsche Megaschweineställe, deren Produkte in den wachstumsorientierten Export gehen
