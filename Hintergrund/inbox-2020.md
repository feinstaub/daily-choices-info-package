Inbox 2020 (unsortiert)
=======================

<!-- toc -->

- [2020](#2020)
  * [Gesellschaft: Nicht die Sprache verroht](#gesellschaft-nicht-die-sprache-verroht)
  * [Arte-Dokus](#arte-dokus)
  * [Zufällige oder robuste Freiheit? / Personal/public freedom](#zufallige-oder-robuste-freiheit--personalpublic-freedom)
  * [fefe, ccc](#fefe-ccc)
  * [ccc](#ccc)
  * [Nachhaltigkeit und Wachstum](#nachhaltigkeit-und-wachstum)
  * [Auf den Nachhaltigkeitszug aufspringen](#auf-den-nachhaltigkeitszug-aufspringen)
  * [Soja als Tierfutter / Sojaanbau und Regenwald](#soja-als-tierfutter--sojaanbau-und-regenwald)
  * [Sonstiges](#sonstiges)

<!-- tocstop -->

2020
----
### Gesellschaft: Nicht die Sprache verroht
* "Nicht die Sprache verroht, sondern ihre Benutzer", Juli 2020
    * https://de.wikipedia.org/wiki/Ernst_Osterkamp, Germanist

### Arte-Dokus
* TODO: "Dick, dicker, fettes Geld", über menschengemachtes Übergewicht / Fettleibigkeit
    * https://www.arte.tv/de/videos/083970-000-A/dick-dicker-fettes-geld/, 2020
        * "Mediziner sprechen von einer Zeitbombe: Bis 2030 ist die Hälfte der Weltbevölkerung übergewichtig oder fettleibig."

* "Eine wohlgenährte Welt?"
    * "Du bist, was du isst? Mit vier Dokumentationen nimmt THEMA unsere Essgewohnheiten unter die Lupe."
    * https://www.arte.tv/de/videos/RC-019407/eine-wohlgenaehrte-welt/
    * "Anders essen - Das Experiment", 2018
        * https://www.arte.tv/de/videos/086137-000-A/anders-essen-das-experiment/

* TODO: https://www.arte.tv/de/videos/083310-000-A/ueberwacht-sieben-milliarden-im-visier/
    * "Überwacht: Sieben Milliarden im Visier", 2020

* TODO Arte-Doku_: "Osteopathie - Heilen mit den Händen | Doku | ARTE", 2020
    * ...

* TODO Arte-Doku_: "Erinnerungen - Wie wir uns irren (Doku, arte)", 2016, 50 min
    * https://www.youtube.com/watch?v=Nl9ZUMLjrBw
    * ...

* DONE:

* Arte-Doku: "Online Handel - Alles retour | ARTE Re:", 2020, 30 min, über Retouren
    * https://www.youtube.com/watch?v=wgSMMUN3mtg

### Zufällige oder robuste Freiheit? / Personal/public freedom
* "How Do You Know If You're Truly Free? | Philip Pettit | TEDxNewYork", https://www.youtube.com/watch?v=1rTEOU67zCo, 14 min, 2016, ::freiheit, ::demokratie
    * https://de.wikipedia.org/wiki/Philip_Pettit
        * "Philip Noel Pettit ist ein irischer Philosoph und Politikwissenschaftler"
    * ...
    * Geschichte von Nora und ihrem sanftem Herrscher Torvald (gentle master)
        * Zufällige oder **robuste Freiheit**
            * zufällig: Freiheit basiert auf dem aktuellen Willen eines anderen (oder einer Entität) und kann jederzeit wieder weggenommen werden
    * ...
    * 5 min: in order to have freedom you need a **law** and a **culture** that backs you up
    * ...
    * e.g. money elite controls the government => Anhängig von dieser Elite => no robust freedom
    * ...
    * 8 min: e.g. freundlichte Kolonialmacht
        * laws that grant personal freedoms are not robustly in place
    * robust/public freedom
        * people must control laws
        * by an electoral democracy
        * and three more things
            * 1. protection of certain minorities (religious, cultural) against the majority culture
            * 2. protection of majorities; against elites (e.g. money elite)
                * Beispiel unvorteilhafter Einfluss auf **Steuer- oder Umweltgesetzgebung**
            * 3. contestatory democracy
                * people can contest (anzweifeln, hinterfragen), what the government is proposing and doing, (auch ::unternehmenskultur?)
                * e.g. by NGOs
                    * **price of democracy is eternal vigilance (Wachsamkeit, Aufmerksamkeit)**
    * 11:00 Test for...
        * **personal/private freedem**: anderen in die Augen schauen, ohne Grund zu Angst oder Ehrerbietung (deference)
        * **public freedem**: wie fühle ich mich, wenn ein Gesetz in meinem Bereich Nachteile bringt?
            * ...
    * 12:00 Public freedom: means control, means freedom over the law, control over the law (analog Freie Software: Kontrolle über den Code, ::free_sw)
        "we as people share equally in shaping that law"
    * **libatarian fallacy/oversight**: "only the market makes us free", "that government and law is always an intrusion". On the contrary, ... Demokratie ist wichtig dabei
        * ...
        * Gegenbeispiel: China: freier Markt, aber die Leute kontrollieren das System nicht
    * YT comment:
        * "You can feel as free as you want, but you might not actually be free.
        Whereas you might feel not free, while living in a free country.
        This is not about feelings, this is about actual freedom."

### fefe, ccc
* https://blog.fefe.de/
* "36C3 - Das nützlich-unbedenklich Spektrum", https://www.youtube.com/watch?v=31xA9p3pYE4, Dez 2019, fefe, 1 h, Gedankengang
    * wollte nie Code für Medizingeräte oder Atomkraftwerke schreiben
        * ist doch ganz einfach: wenn die, die ihre Grenzen kennen, es nicht tun, dann machen es halt die anderen
    * "Problem: Programmieren lernt man explorativ"
        * "Explorative Verfahren kennen die Grenzen nicht", "Die zu finden ist ja gerade das Ziel"
        * "Ich habe es dann verstanden, wenn ich die Aufgabe implementiert habe" (Code ist dann nicht gut, aber nur so lernt man)
        * Alle arbeiten immer an den Grenzen des für sie gerade noch Machbaren
        * Das heißt, die Sachen, die in Produktion gehen, ist genau das, was der Typ gerade noch verstanden hat.
            * (nicht die abgehangenen, gut verstandenen Sachen)
        * Die wenigsten haben ihre Tools und Abhängigkeiten wirklich im Griff.
    * Bessere mit Komplixtät umgehen (neue Programmiersprache etc.) => bestehendes besser machen? Nein, komplexere SW bauen.
    * Gradient Descent, so sind wir halt
        * z. B. Proj.mgmt: "Jetzt haben wir schon so viel investiert, jetzt können wir nicht zurück"
        * KEIN Gradient
            * Security
                * "Man merkt nicht, wenn man zuweit gegangen ist, bis es zu spät ist"
            * auch: "Daten rausgeben" (an Facebook etc.)
            * Komplexität
                * Kontrollverlust merkt man erst Jahre später
                * Zurückrollen geht nicht
                * Kosten werden an unsere Kunden und unser zukünftiges Selbst **externalisiert**.
    * ab ca. 10 min: Harmlose Software
        * ...
        * Beispiel Browser-Addons: nützlich oder harmlos? -> man wählt nützlich, z. B. Flash-Plugin
            * Chroma wählt "Sicherheit" und blockt sogar Adblocker
    * Sein Security-Alltag
        * Bug Severity; nur noch wichtige Bugs werden gefixt ("später fixen" => nie); Bugtracker sind riesige Datenendlager; Bugwelle
            * Leute schreiben 'wichtig' oder 'security' dran
        * Beispiel Mozilla-Bugtracker: viele Bugs (besser als IE, wo der Tracker nicht öffentlich ist)
        * Bugs schließen
            * neue JS-Engine schließt alle Bugs der alten Engine => sieht so aus, als hätte man was geschafft
            * Automatisch schließen! nach einer bestimmten Zeit der Inaktivät bzw. Untätigkeit
        * => Firmen haben bugfreien Code nicht mal als abstraktes Ziel; Bugs verwalten, statt schließen
            * Bugs werden oft nur mit Exploit anerkannt
        * Mitigations: Bugs offen lassen, aber Exploit schwieriger machen
            * Exploits werden viel teurer
        * Erfahrung
            * Reaktive Security geht nicht
            * beim Entstehen schon mitdenken
    * ca. 25 min Idee
        * Devs sollen explorativ lernen können, aber nicht direkt am Produkt, weil das dann schlecht wird
            * Zeit nötig (aber bisher will keiner dafür Geld ausgeben)
            * im Projekt: innovativ sein _was_ du tust, aber nicht _wie_
    * 30 min: Beispiel, wie ein Code-Smell-Detektor scheitert: zuviele Warnungen, anpassen, verteilen, "nur false positives",
        eine Weile weiterlaufen lassen damit das Projekt nicht als gescheitert gilt
    * legacy code: wenn zu krass, laufen die Devs weg, auch wenn sie bezahlt werden
    * Tipps:
        * alle Compilerwarnungen anschalten, durchlesen und wegmachen
    * findet viel Antivirus Schlangenöl ("hilft manchmal, vielleicht, gegen doofe Angreifer")
    * Q&A
        * Enterprise-Umfeld? -> die Score ist nicht dafür gedacht, denn im Enterprise-Umfeld
            entscheidet der Geldgeber darüber, wofür du deine Zeit verwendest.
            Einfach so Bugs beheben geht nicht. Es gibt ein großes Backlog.
            Speziell in agile-Projekten wird jede freie Minute rausoptimiert.
            => Man muss über Open-Source kommen. Dort anfangen, dann schwappt das rüber.

### ccc
* "36C3 - Verkehrswende selber hacken", https://www.youtube.com/watch?v=WhgRRpA3b2c, Dez 2019, robbi5 ubahnverleih
    * gute Argumente für öffentlichen Personennahverkehr (ÖPNV)
        * u. a. private Anbieter sind nur da, wo sich Gewinn machen lässt bzw. gehen weg, wenn es sich nicht lohnt (Beispiele)
    * Beispiel: Mobilitäts-Startups: dort geht es nicht um Nachhaltigkeit, sondern um Hype und möglichst viel Mobilität in die eigene App zu bekommen für Monopol
    * Beispiel: Bike-Sharing-Schloss
        * Spec geholt, App geschrieben, später gingen damit Schlösser im realen Einsatz auf
    * Problem, wenn jeder alles für sich macht (Verkehrsverbünde, Scooter-Anbieter): viele Anbiet mit je eigener Apps, obwohl man z. B. nur das nächste Fahrzeug in der Nähe haben will
    * API ohne Auth mit Kundendaten entdeckt => Pressebericht
    * "Wenn es Doku gibt, bauen die Leute plötzlich Anwendungen"
    * Pos. Beispiel: Stadt Los Angeles entwickelt einheitlichen Standard für Daten, die Scooter-Anbieter für jedes Fahrzeug liefern müssen
    * ...

* "36C3 ChaosWest: Datacenter - was wir wissen sollten", https://www.youtube.com/watch?v=7Q3Lzn6ZCmw, Dez 2019, Günter Eggers
    * Stromausfall/Blackout: Handynetz 30 min, Festnetz (= Voiceover IP) direkt aus?
    * Kühlung Rechenzentrum: wenn es ein Konzept gibt (z. B. Fernwärme), dann nur ca. 1 % der Wärme; Rest geht in die Luft
        * (mit nur 50 % könnte man 10.000 Gebäude heizen)
        * Für Fernwärmenetze (Aufbau, Wartung) braucht es gute (Kommunal-)Politik
    * Stromverbrauch in Rechenzentren in D. steigt stetig (obwohl wir ein Reduktionsziel haben)
    * Ausblick: Zahlen 2018:
        * Rechenzentren (Markt) in D. und Europa wachsen stetig (25 % p. a.) (=> Stromverbrauch steigt exponentiell mit)
        * hauptsächlich (80 %) durch die großen Hyperscaler (= große Cloudanbieter) getrieben.
    * Digitale Souveränität (falls politisch erwünscht): einige Punkte offen

### Nachhaltigkeit und Wachstum
* https://www.tagesschau.de/wirtschaft/corona-klima-101.html
    * "So kann sogar die Luftfahrtindustrie in Zukunft nachhaltig sein." really?
    * "Das wirtschaftliche Wachstum ankurbeln und das Klima retten, Claudia Kemfert ist zuversichtlich, dass beides gelingen kann.
        Denn Wachstum sei nur dann schädlich, wenn es auf das reine Bruttoinlandsprodukt reduziert werde und Klimaschäden
        nicht berücksichtigt würden. Dazu müsse der Klimaschutz eben auch als systemrelevant eingestuft werden."

### Auf den Nachhaltigkeitszug aufspringen
* BlackRock-Letter
    * Regulierung wird antizipiert; aber gehandelt erst, wenn die Gesetze da sind
    * Mit welchen Mitteln bekommt es ein Unternehmen hin, vorher freiwillig Umweltvorreiter zu werden?
        * Beispiele sammeln von anderen Unternehmen (Vaude, BMW?)
            * Welcher Spirit und welche Werte sind vorhanden?
        * Liegt es an der Führung?

### Soja als Tierfutter / Sojaanbau und Regenwald
* Lobby-Text: https://www.dvtiernahrung.de/aktuell/faqs-oder-haeufige-fragen-und-vorurteile/haeufige-irrtuemer-zu-tierfutter.html
    * ist ja alles nicht so schlimm

* https://www.transgen.de/lebensmittel/1049.futtermittelimporte-europa-sojabohnen-gentechnik.html
    * Graphik "Die Welt der Sojabohnen: Die großen Handelsströme, Stand 2017"
        * Zahlen: über 30 Mio t Soja gehen von Amerika nach Europa
    * "Europa produziert zu wenig eiweißreiche Futterpflanzen für seine Nutztiere und ist deswegen auf die Einfuhr großer Mengen an Sojabohnen angewiesen. Und die sind im Regelfall „mit Gentechnik“: In den wichtigsten Erzeugerländern in Nord- und Südamerika werden fast nur noch gentechnisch veränderte Sorten angebaut. Weltweit liegt der Gentechnik-Anteil an der Sojaproduktion bei knapp achtzig Prozent."

* https://www.wwf.de/themen-projekte/landwirtschaft/ernaehrung-konsum/fleisch/soja-als-futtermittel/ zu Fleischkonsum, Jahr ?
    * "Soja wird in Deutschland bzw. Europa kaum angebaut. Das meiste wird importiert. Über 80 Prozent des nach Deutschland importierten Sojas stammt aus Südamerika."
    * "Bei unseren Großeltern kam Fleisch in der Regel nur einmal die Woche auf den Tisch. Heute essen die Deutschen täglich oder mehrmals pro Woche Fleisch."
    * "Im Schnitt etwas über 1 Kilogramm pro Woche. Das ist doppelt so viel Fleisch wie vor 100 Jahren."
    * "Und das ist doppelt so viel, wie Ernährungsexperten aus gesundheitlichen Gründen empfehlen."
    * "Bislang stammt nur ein verschwindend geringer Anteil des Fleisches, das wir kaufen, aus ökologischer und artgerechter Tierhaltung. Bei Rindfleisch sind dies 4 Prozent, bei Schweinefleisch 0,5 Prozent und bei Geflügelfleisch 0,8 Prozent. Soja findet hier bei der Fütterung dieser Tiere deutlich weniger Verwendung."

### Sonstiges
* https://www.fluxfm.de/5vor12/
