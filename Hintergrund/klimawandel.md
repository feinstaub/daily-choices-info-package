Der Klimawandel
===============

<!-- toc -->

- [Allgemein: Der Generationenvertrag](#allgemein-der-generationenvertrag)
  * [Das Generationenmanifest](#das-generationenmanifest)
  * [Gerechtigkeit](#gerechtigkeit)
  * [Hintergrund](#hintergrund)
- [Lösungen von drawdown.org](#losungen-von-drawdownorg)
- [Politische Möglichkeiten](#politische-moglichkeiten)
  * [CO2-Preis](#co2-preis)
- [Indizien](#indizien)
  * [2020](#2020)
  * [2019 - wie sieht Klimawandel aus?](#2019---wie-sieht-klimawandel-aus)
  * [2018](#2018)
  * [2017](#2017)
- [Wie funktioniert Wissenschaft?](#wie-funktioniert-wissenschaft)
- [Aussagen und Fragen](#aussagen-und-fragen)
  * [Die Welt geht unter; Weltuntergangspropheten gab es schon immer](#die-welt-geht-unter-weltuntergangspropheten-gab-es-schon-immer)
  * [Panikmache vs. positives Weltbild und Gottvertrauen / nichts vorschreiben lassen](#panikmache-vs-positives-weltbild-und-gottvertrauen--nichts-vorschreiben-lassen)
  * [Es gibt keine Lösung. Also gibt es auch kein Problem.](#es-gibt-keine-losung-also-gibt-es-auch-kein-problem)
  * [Schau dir doch die Leute an; was wäre wenn](#schau-dir-doch-die-leute-an-was-ware-wenn)
  * [(Was ist denn nun die Lösung?)](#was-ist-denn-nun-die-losung)
  * [Protestieren bringt nichts / Demonstranten stören mich und andere](#protestieren-bringt-nichts--demonstranten-storen-mich-und-andere)
  * [Eigene Wirksamkeit in Frage gestellt, dann lieber eigennützig?](#eigene-wirksamkeit-in-frage-gestellt-dann-lieber-eigennutzig)
  * [Der Demonstrant als Feindbild](#der-demonstrant-als-feindbild)
  * [Wir haben alles aufgebaut; die jungen Leute sind dekadent](#wir-haben-alles-aufgebaut-die-jungen-leute-sind-dekadent)
  * [Unser Konsum wird doch immer effizienter](#unser-konsum-wird-doch-immer-effizienter)
  * [Demokratie und Freiheit fällt nicht vom Himmel](#demokratie-und-freiheit-fallt-nicht-vom-himmel)
  * [Wissenschaftsleugner](#wissenschaftsleugner)
  * [Climate of Concern, a 1991 educational film produced by Shell](#climate-of-concern-a-1991-educational-film-produced-by-shell)
  * [Der globale CO2-Anstieg](#der-globale-co2-anstieg)
  * ["Deutschland hat nur einen Anteil von 2 % am weltweiten CO2-Ausstoß"](#deutschland-hat-nur-einen-anteil-von-2-%25-am-weltweiten-co2-ausstoss)
  * ["Das Klima hat sich schon immer geändert"](#das-klima-hat-sich-schon-immer-geandert)
  * ["CO2 ist kein Klimagas"](#co2-ist-kein-klimagas)
  * [Allgemein](#allgemein)
  * ["Andere sind schuld"](#andere-sind-schuld)
  * ["CO2 bringt Pflanzen schneller zum Wachsen"](#co2-bringt-pflanzen-schneller-zum-wachsen)
  * ["Warum sollten ausgerechnet wir Vorbild sein?"](#warum-sollten-ausgerechnet-wir-vorbild-sein)
  * [Brückenlösung Atomkraft?](#bruckenlosung-atomkraft)
- [Verschiedenes](#verschiedenes)
  * [Zielkonflikt: Naturschutz vs. Klimawandel](#zielkonflikt-naturschutz-vs-klimawandel)
  * [Sammlung zu Kohlestrom und Kohleabbau](#sammlung-zu-kohlestrom-und-kohleabbau)
  * [Videos](#videos)
- [Aktiv werden](#aktiv-werden)
  * [Ende Gelände](#ende-gelande)
  * [Klimawende](#klimawende)
  * [Weitere](#weitere)

<!-- tocstop -->

Allgemein: Der Generationenvertrag
----------------------------------
* siehe auch hoffnung.md

### Das Generationenmanifest
* September 2017:
    * [ZEIT-Artikel zum Generationenmanifest](http://www.zeit.de/news/2017-09/07/klima-studie-deutschland-verfehlt-eindeutig-klimaschutzziel-07072402)
    * Startseite: https://www.generationenmanifest.de/
        * 10 Forderungen
        * [Videos](https://www.generationenmanifest.de/videos/) mit Statments
            * "jeder der Kinder hat, ..."
        * [Personen](https://www.generationenmanifest.de/wer-wir-sind/)
    * Video: [Bundespressekonferenz zur Vorstellung des Generationenmanifests](https://www.youtube.com/watch?v=Ng56MWOIBNA), Phönix, ab ca. 5 min für 10 min
        * In der deutschen Verfassung ist zwar Umweltschutz als Staatsziel enthalten, aber nicht ein Generationenvertrag (also heute so handeln, dass die nächste Generation die gleichen Chancen hat). Mit der Aufnahme der Generationengerechtigkeit ins Grundgesetz wird sie einklagbar.
        * Schnellnhuber: "die Diktatur des Jetzt": heute wird zugunsten der jetzigen Generation alles was geht, verwertet und verbraucht; Müll etc. auf künftige Generationen abladen

### Gerechtigkeit
::gerechtigkeit

* 2020: ""#Klimagerechtigkeit ist kein Zustand, sondern der Kampf gegen die gesellschaftlichen Strukturen, die #Klimaungerechtigkeit verursachen.""
    (https://twitter.com/MuellerTadzio/status/1245965667188252672)
* ...

### Hintergrund
* Sachbuch: "Selbstverbrennung" von Hans Joachim Schellnhuber, Thema: Klimawandel
    * S. 27
    * S. 29: Artensterben
    * Abb. 1: Champagnerschale der globalen Ungleichheit
    * S. 66: Irrwege wie die 1980er Jahre Spekulation einer bevorstehenden Eiszeit (-> immer noch Futter für Wissenschaftsverdreher)
    * Abb. 15: Komplexität
    * S. 191: Erdöl/Erdgas sind Früchte des Meeres (pflanzliches und tierisches Plankton)
    * S. 195: Methan, Milchkuh
    * S. 219: 17./18. Jh. neuartiger Konsumrausch,
        * Aufstieg Großbritanniens zur Weltmacht, drei Antriebe: Piraterie, Sklavenhandel, Corioliskraft
        * 'Empire' von Nial Ferguson, 2004
        * Ende des 16. Jh "das Staatsfreibeutertum etabliert"
    * S. 223: Handelsdreieck, Sklavenschiffe, Moral vs. Profit
    * S. 238: Hyperenergiegesellschaft?, Abb. 29
    * S. 239: keine Leibeigenschaft, aber Zwang, sich in Konkurrenz zu Maschinen für Hungerlöhne zu verdingen
    * S. 241: Erdöl wird Treiber der "stürmischen industriellen Überholvorgänge"
    * S. 242/243:
        * "Das Hochsteigen des Öls in die Herzkammer der Weltwirschaft" -> Kap. 16 (todo)
        * Ursache wirtschaftliche Verknappung
            * **schwindendes Holz** => fossile Kohle
                * todo: wann und wie war das genau?
            * **Schwund der Walbestände** => Erdöl
                * Fischbein ist biegsam und dauerhaft => Miederindustrie
                    * Walfang wurde aggressiv intensiviert
                * Tran (Walfett, Polaröl), aus Fettgeweben von Walen gewonnen
                    * erster flüssige Brennstoff, der in nennenswertem Umfang erschlossen werden konnte
                    * Öllampen
                    * Herstellung von Nahrungsmitteln, Arzneien
                    * Produktion von Sprengstoff
                        * Alfref Nobel wird reich damit
                    * Schmiermittel
                * Mitte 19. Jh. 900 Fangschiffe erlegen bis zu 10.000 Wale jährlich
                    * 1860: Erfindung der Harpune: Beschleunigung
                    * nördliche Gewässer keine Wale mehr => Pazifik und Indischer Ozean
                    * 1930er Jahre Höhepunkt der Abschlachtung
    * ...
    * ...
    * Verschiedenes
        * http://water.org
        * siehe transformation.md

Lösungen von drawdown.org
-------------------------
* [Summary of Solutions by Overall Rank](https://www.drawdown.org/solutions-summary-by-rank)
    * ...
* Sector Summaries, https://www.drawdown.org/solutions
    * Electricity Generation
    * Food
    * Buildings and Cities
    * Land Use
    * Transport
    * Materials

Politische Möglichkeiten
------------------------
### CO2-Preis
* ["Energiewende - Bundesrechnungshof watscht Wirtschaftsministerium ab"](https://www.tagesspiegel.de/politik/energiewende-bundesrechnungshof-watscht-wirtschaftsministerium-ab/23123606.html), 2018
    * "Die Energiewende wird schlecht gemanagt, kritisiert der Bundesrechnungshof in einem neuen Bericht und schlägt einen CO2-Preis vor."
* https://de.wikipedia.org/wiki/CO2-Steuer

Indizien
--------
* Mit dem Handeln zu warten, bis der Klimawandel endgültig bewiesen ist, wäre töricht. Denn bei einem derartig komplexen System heißt das: die Veränderungen sind eingetreten.
* Zwischen "nicht belegt" und "endgültig bewiesen" liegt eine große Bandbreite (vor allem ab 99 %). Die verfügbaren wissenschaftlichen Indizien sprechen eine deutliche Sprache.

### 2020
* "Alpenrausch im Klimawandel - Der Ausverkauf der Berge | SWR Doku", https://www.youtube.com/watch?v=jq-m-qlwQXA, 2020, 45 min
    * "Durch den Klimawandel sind viele Skigebiete nicht mehr schneesicher.
        70 bis zu 90 Prozent der Pisten müssen dabei ständig künstlich beschneit werden."
    * "Um den Skizirkus trotz Klimawandel in Gang zu halten, schrecken viele Betreiber auch vor illegalen Baumaßnahmen am Berg nicht zurück."
    * "Einheimische protestieren gegen Eingriffe in die Natur"
        * "Sie kämpfen gegen den Bau von riesigen Speicherseen zur Beschneiung der Pisten und gegen geplante Skischaukeln,
            die bislang naturbelassene Hänge überbauen würden.
            Deren Betreiber und Investoren schrecken beim Bau oft auch nicht vor illegalen Machenschaften zurück."
    * ...

* "Uno-Ozeanbeauftragter über Klimawandel - "Wir müssen anfangen, wie in einem Kriegszustand zu denken""
    * https://www.spiegel.de/wissenschaft/mensch/peter-thomson-uno-ozeanbeauftragter-vergleicht-klimarisiko-mit-kriegszustand-a-9d22f870-e1a8-4695-b2fa-23c978569093#
    * Peter Thomson
    * "Jetzt befinden wir uns in einer Art Appeasement-Phase"

* Lobby-Aktivitäten
    * https://correctiv.org/top-stories/2020/02/04/die-heartland-lobby/ - Heartland Institute
        * EIKE
    * https://netzpolitik.org/2019/wir-veroeffentlichen-das-framing-gutachten-der-ard/

### 2019 - wie sieht Klimawandel aus?
::klimawandelfolgen

* Buch: "David Wallace-Wells: Die unbewohnbare Erde", 2019
    * https://www.daserste.de/information/wissen-kultur/ttt/sendung/ttt-25082019-david-wallace-wells-100.html
    * https://www.sonnenseite.com/de/tipps/die-unbewohnbare-erde.html
    * ...

* Buch: Silent Spring / Der stumme Frühling, 1962, Rachel Carson
    * https://de.wikipedia.org/wiki/Der_stumme_Fr%C3%BChling
    * Chemikalien (noch kein Klimawandel)

### 2018
* ["Klimasünder kommen vor Gericht"](https://www.heise.de/newsticker/meldung/Klimasuender-kommen-vor-Gericht-3952173.html), 2018

### 2017
* ["Deutsche Forscher in Peru - den Meeren geht der Sauerstoff aus"](http://www.tagesschau.de/ausland/klimawandel-meere-101.html), 11.04.2017, Tagesschau
    * "Die ersten Erkenntnisse sind nicht wirklich beruhigend."
    * "von Peru kommt 10 % des weltweiten Fischereiertrags"
* US-Klimabehörde: https://www.heise.de/newsticker/meldung/Im-Jahr-2016-brachen-etliche-Klimarekorde-3797940.html

Wie funktioniert Wissenschaft?
------------------------------
siehe wissenschaft.md

Aussagen und Fragen
-------------------
::cdugen

### Die Welt geht unter; Weltuntergangspropheten gab es schon immer
* "Die Welt geht unter" ist eine Zusammenfassung. Es kommt auf die Details an; Gründe; detaillierte Prognosen; ::klimawandelfolgen
    * Was waren bei deiner Aussage in der Vergangenheit die _Gründe_ für eine eventuelle Untergangsprognose?
* Gründe sind essentiell.
* Rahmenbedingungen können sich ändern.

* Wir können viel **aus der Geschichte lernen**?:
    * Harte Arbeit
        * (Hätte man damals gedacht, dass das römische Groß-Reich untergeht? (ist nicht ganz klar))
        * Dekadenz?
        * Völker, die **hart arbeiten**, stehen besser da?
            * heute: sollen alle noch mehr ins Büro, denn dort lässt sich objektiv am besten Geld verdienen?
                * indem dort organisiert wird, wie und was andere arbeiten sollen
        * Gruppen mit gutem Militär sehen besser da?
        * Gibt es was Griffiges mit Umwelt?
    * Seit 200 Jahren Industrialisierung: die komplette Art zu leben umgekrempelt (nicht immer schmerzfrei)
        * Die Ressourcen der Erde (inkl. Fläche und Nutzung als Müllhalde) bis zum letzten Winkel erschlossen
        * Die Welt dreht sich **immer schneller**
            * Was sagt uns das auf eine potentiell nächste Umkrempelung?
            * Können wir absehen, welche Effekte diese Wirtschaftsart der Beschleunigung auf uns in den nächsten Jahrzehnten hat?
        * Indigene Völker ausgerottet
            * diese lebten nachhaltiger, weil sie keine Technik hatten
        * Artensterben
            * heute gleiche Geschwindigkeit wie vor Millionen von Jahren beim letzten Massensterben.
            * Sollte das nicht zu denken geben?
        * Extraktion läuft immer präziser und schneller, um Wohlstand durch Wachstum zu erreichen.
            * Was machen die Menschen, wenn die Ressourcen aufgebraucht sind?
            * Ist überhaupt eine Ende abzusehen? (um die Rente zu finanzieren?)

* https://www.daserste.de/information/wissen-kultur/ttt/sendung/fridays-for-future-108.html, user comment, 2019
    * "Weltuntergang - Natürlich werden wir nicht den Untergang der Welt bewirken. Aber den Untergang unserer lebenswerten Welt haben wir so gut wie besiegelt. Natürlich ist unsere Welt noch nie untergegangen. Schon garnicht durch selbsterzeugte Katastrophen. Aber wenn man zB den 30 jährigen Krieg als eine der selbst bewirkten Katastrophen betrachtet; er war der Untergang für 2/3 Menschen in der Mitte Europas. **Für diese Menschen macht es keinen Unterschied ob die ganze Welt untergeht, oder nur die ihre.** Und für die Überlebenden und ihre Nachkommen war die Welt nie wieder die gleiche. Aber der große Unterschied zwischen diesen Katastrophen besteht darin, solch ein Krieg läuft sich tot, wenn das Menschenmaterial verbraucht, alle Städte niedergebrannt und alle Vorräte aufgefressen sind. Der Klimawandel gewinnt dann erst an Fahrt. Eine globale Erwärmung führt zu noch schnellerer Erwärmung und diese Kettenreaktion ist unumkehrbar. Und deshalb ist Panikmache legitim und alles andere dumm."

### Panikmache vs. positives Weltbild und Gottvertrauen / nichts vorschreiben lassen
* aka (Wie sagen wir es den Kindern; Wir haben doch noch Zeit; Ökowahn) ::ökofaschismus
    * Wie erkläre ich es den Kindern?
        * Analogie?: lieber Opa raucht. Besorgte Angehörige wollen, dass er aufhört. Was nun?

* Analogien
    * Klima- und Umwelt entwickelt sich so und so. Die Verzögerung ist erheblich. Wir müssen JETZT handeln.
        * Glaub ich nicht.
        * Oder: vergewissern und handeln
    * Ein Doktor diagnostiziert Krebs.
        * Schock. Glaub ich nicht?
        * Anderen Arzt suchen? solange bis einer (von vielen) sagt, dass alles in Ordnung ist und die bereits sichtbaren Symptome alle möglichen Ursachen haben, außer Krebs
    * Das Haus wird langsam unterspült.
        * Prognose von Bauexperten: das Haus stürzt in 2 Jahr ein, wenn nichts gemacht wird
        * Reaktion 1: so lange die Experten abklappern, bis man einen gefunden hat, der kein Problem sieht
        * Reaktion 2: den Jahresurlaub streichen und das Fundament ausbessern.
            * Das ist aber echt unangehm und außerdem weiß ich nicht, wie man das Fundament ausbessert.
            * Also vorsichtshalber das Problem an sich abstreiten.
                * Es wird schon gut gehen (Gottvertrauen).
                * Noch steht das Haus ja und innen wackelt auch nichts. Also was soll schon dran sein an der Unterspülungsbehauptung.
    * Ein Hausdach bekommt Löcher.
        * Leute sagen, man sollte was machen, aber man will lieber seinen Garten bestellen.
        * Leute sagen, es wird nicht besser. Aber das Wasser ist noch nicht in der Wohnung zu sehen (das Mauerwerk saugt sich von innen voll)
        * Sollte man drauf vertrauen, dass es bisher in der eigenen Lebzeit mit dem Dach noch nie Probleme gab?

* Glauben vs. Akzeptieren
    * 2020: Nicht "Glauben an den menschengemachten Klimawandel", sondern
        **akzeptieren**, dass die Ursachen und Auswirkungen, die die Wissenschaft über den Klimawandel herausgefunden hat, zur Wirklichkeit unserer Welt gehören.

* **Wütend, wenn eigene Argumentation ins Wanken gerät**
    * Warum?
        * Angst vom Glauben abgebracht zu werden?
            * Warum eigentlich, wenn der Glauben stark und gefestigt ist?
            * Sollte mit jeder Situation zurecht kommen, oder?
            * Wenn nicht... dann starkes Zeichen dafür wie wichtig die richtigen Worte sind.
        * Angst, dass die "kommunistischen Gedankengänge" ansteckend sein könnten?

* **Glauben an Gott und den freien Markt / die Lenkungskraft des Geldes**
    * Grundannahme: der freie Markt bietet maximale ::Freiheit für den Einzelnen.
        Jeder kontrollierende oder regulierende Eingriff wird als Angriff auf die Freiheit des Bürgers verstahnden
    * Kann der absolute Markt und das Geld, auf das wir uns - offensichtlich - bedingungslos verlassen, alle Probleme lösen?
    * Was ist mit Freiheit und Bürgerrechte?
        * Werden diese durch den Markt und Geldstreben erstritten und erhalten?
    * Was ist mit gerechter Verteilung?
        * (ok klar, wer keine gut bezahlte Arbeit findet, hat es nicht besser verdient)
    * Was ist mit Werten wie Liebe, Barmherzigkeit, Bescheidenheit und Zusammenhalt von Menschen (Solidarität, gegenseitig helfen)
        ... kann das durch Markt und Geld erwirkt und aufrechterhalten werden?
    * Wenn nein, woher kommt dann das bedingungslose Vertrauen?
        vielleicht daher, dass die vorigen Systeme nicht zu so einem rasanten Wohlstand von Vielen (aber auf Kosten
        von einigen wenigen - und in anderen Teilen der Welt) führte?
    * Was, wenn jemand politisch vorschlagen würde, dass dem **Markt und dem Geld neue Grenzen gesetzt** werden sollen? ::freiheit, ::ökofaschismus, "Beispiele von guten Einschränkungen"...
        * Abgelehnt? Also ist Markt und Geld (siehe Großkonzerne) doch in Ordnung?
        * **Beispiele von Gesetzen und Regelungen**, die wir als Gesellschaft akzeptiert haben, **obwohl** sie Einzelne einschränken
            * Gurtpflicht, Anschnallpflicht
            * FCKW raus aus Kühlschränken
            * Leibeigenschaft
            * Männer, die Frauen nicht mehr vorschreiben dürfen, welchen Beruf sie wählen dürfen
            * Rauchverbot in Cafes und Restaurants
                * Symmetrie-These (kein Fleisch berücksichtigen sei symmetrisch zu Fleisch berücksichtigen
                    * ("Hast du ein Problem damit, aus Spaßgründen an einer ansonsten unnötigen Tiertötung beteiligt zu sein?")
                    * Gegenbeispiel: Besuch einer Raucherbar
            * Tabakwerbeverbot          vs. Freiheit
            * Straßenverkehrsordnung    vs. Freiheit
                * **Alkohol am Steuer**     vs. Freiheit
                * Tempolimits
                * Handyverbot am Steuer
                * Zebrastreifen, Einbahnstraße
            * Rauchen und Alk in der Schwangerschaft    vs. Freiheit und Komfort
            * Autoverbot an bestimmten Stränden
            * (Rauchen auf der Terasse, wenn Kinder anwesend sind)
            * Zuckerreduktion (noch im Übergang)
            * -> für nötige Änderungen wegen Klima benötigt man die Anerkennung der aktuellen Forschung ("Klimaerkenntnis)
                * wie beim Tabak
    * Ist Gerechtigkeit überhaupt Ziel der Religion?
        * Oder eher, wer viel hat, der hat es sich verdient? Und alle anderen sind selber Schuld?
    * Marktglauben und Fokussierung auf Geld zersetzt den Glauben?
        * ja? -> was nun?

### Es gibt keine Lösung. Also gibt es auch kein Problem.
* (weil Menschen so oder so sind; weil die Politik so oder so ist; weil die Welt so oder so ist)
* Klären: Sollte man Problemanalysen davon abhängig machen, ob man bereits eine Lösung sieht oder nicht?
    * Generell oder nur bei komplizierten Themen?

### Schau dir doch die Leute an; was wäre wenn
* Die denken nur an sich; die fliegen alle dauernd in Urlaub.
    * Fragen dazu:
        * Kann die Religion hier vielleicht weiterhelfen?
        * Wieviele gute Christen sind unter den Vielfliegern?
        * Führt der Gedanke an "die Leute" zur Hilflosigkeit/zum Untergang?
            * Wenn ja, warum sollte man nichts tun?
        * Wer sind denn "die Leute"? Alle, außer man selber?
        * Was würde passieren, wenn "die Leute" dasselbe machen würden wie man selber?
            * noch mehr aufbauen und zubauen?
            * weniger fliegen? weniger konsumieren?
                * wer kurbelt dann sonst die Wirtschaft an?
                * wer ist dann Schuld, wenn es wirtschaftlich bergab geht? Die Leute oder die Anforderungen des Wachstums?


### (Was ist denn nun die Lösung?)
(* erstens: ist das Problem klar?)
* zweitens: wirklich keine Lösung?
    * Was schon probiert?
        * Wie oft? (Klappt denn sonst alles gleich beim ersten Mal?)
    * Welche Erfahrungen gesammelt?
    * Mit anderen über das Problem, für das man eine Lösung sucht, reden
    * Das Problem regelmäßig auf die Agenda setzen
    * Expertenmeinungen einholen
        * (Politiker sind hier derzeit unbedingt eine zuverlässige Quelle)
    * wenn selber keine Zeit, dann drauf vertrauen, dass andere wissen wie man das Problem angeht
        * nicht meckern, ohne sich zu informieren
    * die richtigen Fragen stellen
    * noch nichts gefunden? Ist das Problem (was war das nochmal?) nun weg? Oder einfach zu groß?

### Protestieren bringt nichts / Demonstranten stören mich und andere
* Doch, siehe Geschichte (Protestforscher, todo: WDR5-Runde)
* Einige wenige protestieren und opfern ihre Zeit; viele profitieren.
    * **Warum sollte ich mich engagieren, wenn der Erfolg nicht 100% garantiert ist?**
    * Ich hab doch im Garten genug zu tun und mit Kinder hüten und Familie pflegen.
        * Jeder trägt seinen Teil für die Gesellschaft bei.
        * **Jeder ist wichtig.** Jeder wie er kann (Gesundheit)
        * Ab einem gewissen Komfortlevel (Putzfrau, jährliche Flugreisen und sonstige Umweltbelastungen wie Skifahren; gute Gesundheit)
            kann man sich fragen, ob der Komfort zugunsten von (noch mehr) Familie oder Gesellschaft eingelöst werden sollte.
            * Komfortlevel mit **Chancen** (nicht Zustand, das hängt vom Engagement ab) von anderen Menschen vergleichen
        * Zumindest: gegenseitig verstehen und anerkennen:
            * Familienpflege ist wichtig, **aber es ist nicht alles**, was für eine gute Gesellschaft zählt
            * Umwelt ist wichtig, aber es ist nicht alles, was für eine gute Gesellschaft zählt
            * Menschen, die den Entscheidern auf die Finger schauen oder bei Pfusch protestieren sind wichtig, aber es ist nicht alles, was für eine gute Gesellschaft zählt
* Siehe: "Was ist denn nun die Lösung?"
    * Sind die Politiker (hauen sich die Taschen voll) die Lösung? --> anscheinend nicht ganz
    * Ist die Wirtschaft die Lösung (hauen sich die Taschen voll)? --> anscheinend nicht ganz
    * **Bessere Lösung?**
        * Nein? Dann warum Menschen auf der Straße abwerten, obwohl es historisch gesehen oft zum Erfolg führte?
            * Abwertung ist aber verständlich, denn protestierende Menschen wurde schon immer zu Lebzeiten von ihren Mitmenschen abgeurteilt.
                * siehe z. B. Umfragen zu Martin Luther King Protesten
            * Muss das aber heute immer noch so sein?

* **Demonstranten brechen Regeln** / Ziviler Ungehorsam
    * Ist das Anliegen verständlich?
    * Können die protestierenden Schüler etwas dazu, dass die Lehrer sie nicht ins Klassenbuch eintragen?
        * Ändert das etwas am Anliegen?

* Erwartungsmanagement für die Lösungsfindung. Es geht nie alles gleich sofort.

### Eigene Wirksamkeit in Frage gestellt, dann lieber eigennützig?
* siehe ohnmacht des einzelnen
* Ja, die eigennützige Variante bringt Vorteile.
    * Möglichst viel für sich sichern, um bei der Krise gewappnet zu sein.
    * (Nur was ist dieses "viel"? Materielles? Oder Beziehungen?)
    * Was aber, wenn das alle so machen? -> **Beschleunigung** der Probleme
* ...
    * Ist das eigene Tun das einzig Wahre? Was wäre, wenn jeder so wäre? -> Vielfalt nötig -> anerkennen))

### Der Demonstrant als Feindbild
* Der Feind im eigenen Land?
* Welche Bilder von Demonstranten werden gezeigt/bekommt man zu sehen?
    * Junge, männliche Hippies, die rumschreien (a bunch of crusties)?
    * Wie sähe die Wahrnehmung aus, wenn andere Bilder gezeigt würden?
        * Frauen mit und ohne Kindern?
        * normal und durchschnittlich aussehend?
        * Ältere Menschen?
        * friedlich und freundlich?
        * Christen?
        * Menschen wie du und ich?
            * Sogar aus der selben Familie?

* Erkenntnis und Bescheidenheit, dass man nicht alles alleine lösen kann und Hilfe anderer braucht
    * die Umwelt als Lebensgrundlage / kaputte Umwelt entzieht Freiheiten, ::freiheit
    * die **Politiker, um einen gerechten Rahmen zu gestalten**
        * wenn die Politiker nicht das Allgemeinwohl (inkl. Zukunft) im Blick halten (attestiert durch Wissenschaft),
            ist es die Pflicht des demokratischen, freiheitlichen Bürgers, diese an ihre Pflicht zu erinnern
            (nicht jeder hat das Zeug zum Politiker, aber jeder kann verstehen, was los ist; was die Wissenschaft uns sagt)
            (manchmal brauchen die Politiker einen kleinen Tritt von unten; Protest ist förderlich für Demokratie)

### Wir haben alles aufgebaut; die jungen Leute sind dekadent
* **Die Lösungen von gestern** haben Wohlstand gebracht; sind aber Ursache der Probleme von heute (Umweltprobleme und krasse Ungleichheit).
    * Die Lösungen von gestern (Flächen immer weiter zubauen; Wachstum; zunehmende Fixierung auf Geld und Geiz)
        können nicht die Lösungswege der Probleme von heute sein.
    * Oder wie siehst du das?
        * Interesse die Lösungsfindung der jungen Leute konstruktiv zu begleiten?
    * Welche Probleme siehst du?
        * Ich-Fixierung
            * Wie würdest du das lösen?
        * Flüchtlinge
            * Wären nach perfekter Integration die Umwelt- und Gemeinschaftsprobleme (z. B. Ich-Fixierung) gelöst?
    * Lösung durch Religion?

### Unser Konsum wird doch immer effizienter
* ...z. B. durch Auslagerung der Umweltkosten nach China
* Die Verbesserung Produktions- und Entsorgungs-Technik kann mit der Geschwindigkeit der Steigerung des Verbrauchs nicht mithalten.
    * Wegen Wachstum brauchen wird jedes Jahr ein noch höheres BIP als zuvor.
* Technik ist schon da; muss nur noch intelligent genutzt werden (Verbrauch senken; Wiederverwenden; Reparieren)
    * Reparieren statt neu produzieren
    * Welcher freie Markt- und Geldmechanismus greift hier?
        * Keiner? Sollte man so etwas dennoch lösen dürfen oder heißt es gleich wieder "Verbotspartei"?

### Demokratie und Freiheit fällt nicht vom Himmel
* ...
* Wer für den Erhalt seiner Freiheit nicht kämpft, wird sie schleichend verlieren.
* ...

### Wissenschaftsleugner
* Args
    * "Klimawandel hat es schon immer gegeben"
    * "Man sollte offener darüber sprechen, ob der Mensch wirklich dafür verantwortlich ist"
    * "Angst haben macht kein Sinn. Richte dir ein schönes Leben ein. In 20 Jahren wird es hier weiterhin schön sein"
* Mögliche Reaktionen
    * Neumayer-Forschungsstation in der Antarktis: "Forscher gehen für Klimaschutz aufs Eis", https://www.spiegel.de/wissenschaft/natur/polarstern-forscher-demonstrieren-fuer-den-klimaschutz-a-1298864.html, 2019
        * "demonstrierten Wissenschaftler am Freitag für mehr Klimaschutz"
        * Das sind unsere Wissenschaftler.
        * Alfred-Wegener-Instituts (AWI)

* https://www.climaterealityproject.org/blog/climate-denial-machine-how-fossil-fuel-industry-blocks-climate-action, 2019
    * "The Climate Denial Machine: How the Fossil Fuel Industry Blocks Climate Action."
    * ...

### Climate of Concern, a 1991 educational film produced by Shell
* https://thecorrespondent.com/6285, Artikel 2017
    * "But despite this clear-eyed view of the risks, the oil giant has lobbied against strong climate legislation for decades."
* https://www.youtube.com/watch?v=vTlYYlRN0LY, 30 min
    * da kommt eigentlich alles vor, was bezüglich Klimawandel auch heute noch wichtig ist
        * inkl. wiss. Konsens, Landwirtschaft, mögliche Klimaflüchtlinge, ..., mögliche Alternativen wie Solar und Wind etc.
        * sehr teuer alles; am besten also die vorhandene Energie besser nutzen und Verschwendung vermeiden
        * ...
        * positives Beispiel Dänemark, das erfolgreich Fernwärme einsetzt, um Energie besser zu nutzen
        * ...

### Der globale CO2-Anstieg
* ["Der globale CO2-Anstieg: die Fakten und die Bauernfängertricks"](https://scilogs.spektrum.de/klimalounge/der-globale-co2-anstieg-die-fakten-und-die-bauernfaengertricks/), 2017
    * "Die CO2-Konzentration ist seit Beginn der Industrialisierung von 280 ppm (dem Wert der vorangegangenen Jahrtausende des Holozäns) auf inzwischen 405 ppm angestiegen."
        * https://de.wikipedia.org/wiki/Holoz%C3%A4n - "begann ca. 9700 Jahre vor Christus mit der Erwärmung der Erde am Ende des Pleistozäns"
            * https://de.wikipedia.org/wiki/Pleistoz%C3%A4n - "Es begann vor etwa 2,588 Millionen Jahren"
    * "Der Zyankali-Cocktail"
        * "Ein Mann bietet Ihnen auf einer Party einen Cocktail mit etwas Zyankali an. Sie lehnen entrüstet ab, doch der Mann versichert es sei ganz harmlos: schließlich läge der Zyankali-Anteil in ihrem Körper nach diesem Drink nur bei 0,001 Prozent! Das könne wohl kaum schädlich sein! Den Wissenschaftlern, die behaupten, schon 3 mg/kg Körpergewicht (also 0,0003 Prozent) Zyankali seien tödlich, sei nicht zu trauen. Fallen Sie darauf rein? Würde die Welt-Redaktion diesen Cocktail trinken?"
    * ["7. Nur 3 Prozent des CO2-Ausstoßes sind menschengemacht"](https://www.focus.de/wissen/klima/tid-8638/diskussion_aid_234329.html) - nicht ganz verstanden

### "Deutschland hat nur einen Anteil von 2 % am weltweiten CO2-Ausstoß"
* ja, siehe [Emissionen nach Ländern](https://de.wikipedia.org/wiki/Liste_der_gr%C3%B6%C3%9Ften_Kohlenstoffdioxidemittenten#Nach_L%C3%A4ndern)
    * und ist damit Stand 2011 auf Platz 6 (nach China, USA, Indien, Russland und Japan)
    * also doch ganz schön viel und damit einer der Topkandidaten sich anzustrengen, etwas zu ändern (ansonsten ist das nur langweiliges Fingerpointing)
* zum Vergleich: Anteil an der Weltbevölkerung
    * https://de.wikipedia.org/wiki/Weltbev%C3%B6lkerung#Die_bev%C3%B6lkerungsreichsten_Staaten
        * Platz 16: "Deutschland Deutschland: 83 Mio. (1,1 %)"
    * [CO2-Ausstoß pro Kopf und Land](https://de.statista.com/infografik/9658/laender-mit-den-hoechsten-co2-emissionen-pro-kopf/)
        * 2015: die blauen Kreise
    * ["Pro-Kopf-CO2-Emissionen nach ausgewählten Ländern weltweit im Jahr 2015"](https://de.statista.com/statistik/daten/studie/167877/umfrage/co-emissionen-nach-laendern-je-einwohner/)
        * Hier ist Deutschland auf Platz 11 der Liste.
    * siehe Berechnung: ![](data/co2-land-bevölkerung.png)
    * siehe auch graslutscher unten
* "Damit kann Deutschland auch bei eigenen null Prozent nicht die Welt retten"
    * Richtig; aber soll man daraus schließen? ("Verantwortung Hochverbrauchsländer")
        * **Diese Argumentation verkennt**, dass der globale Schadstoffeintrag aus der Summe von vielen kleinen Teilen besteht. So könnte jeder auf seinen minimalen Beitrag verweisen, um - obwohl noch Potential vorhanden ist - zu rechtfertigen, selber nichts zur Lösung beitragen zu müssen. Dadurch wird der summierte Schadstoffeintrag aber nicht weniger und alle haben den Schaden (vor allem die Schwächeren der Gesellschaft und zukünftige Generationen). Richtig wäre, dass jeder **jetzt** nach seinen Möglichkeiten seinen Beitrag leistet und man gemeinsam an einem Strang zieht. Die Länder, die überdurchschnittlich viel pro Kopf ausstoßen, sollten dabei mehr tun als andere.

### "Das Klima hat sich schon immer geändert"
* [XKCD Earth Temperature Timeline](https://xkcd.com/1732/)

### "CO2 ist kein Klimagas"
* https://de.wikipedia.org/wiki/Treibhauseffekt#Treibhausgase
* https://www.klimafakten.de/behauptungen/behauptung-der-co2-anstieg-ist-nicht-ursache-sondern-folge-des-klimawandels

### Allgemein
* http://www.pik-potsdam.de/~stefan/alvensleben_kommentar.html
* check:
    * Wenn der Klimawandel kein Thema ist, was ist dann ein weiteres dringendes Umweltthema?
    * Was ist mit anderen konkreten Schadstoffen? Was leistet die Wissenschaft da?
    * Leistet die Wissenschaft überhaupt was?
    * Sind Umweltthemen überhaupt wichtig für uns Menschen?
    * Welche konkreten Themen sind wichtig?

### "Andere sind schuld"
Quelle: graslutscher, 2016-11-29
Themen: Umwelt / Erneuerbare Energien / Kaffee

Kommentar zum FAZ-Artikel http://www.faz.net/aktuell/wirtschaft/energiepolitik/iea-bericht-zahl-der-erneuerbaren-energietraeger-steigt-14497162.html

    "Wieso sollen wir denn die Umwelt schützen, wenn die Chinesen viel mehr Schadstoffe ausstoßen?

    1. Weil auch wir Deutschen sie überproportional zerstören. Wir emittieren 2,4% der weltweiten Treibhausgase, sind aber nur 1% der Weltbevölkerung. Und da ist nicht mal eingerechnet, dass für unseren "Hunger" nach Soja-Tierfutter in Südamerika eine Fläche so groß wie Rheinland-Pfalz von Regenwald freigehalten wird, wir Tonnen von Kaffeebohnen von sonst wo verbrauchen und diverse Ozeanriesen auf den Weltmeeren rumschippern, um billige Klamotten aus Asien in unsere Primark- und H%M-Filialen zu schwemmen.

    2. China baut massiv seine Kapazitäten für erneuerbare Energien aus, so dass der weltweite Ausbau leztes Jahr erstmals den Ausbau von Kohle-Kapazitäten überholt hat. Konkret wurden im Jahr 2015 in China 60 Gigawatt Kraftwerksleistung in Form von Photovoltaik und Windkraft installiert. Zum Vergleich: Das ist ein Drittel der gesamten deutschen Kapazität (inkl. Kohle, Atom und EE). Windkraft- und Solarkraftwerke kommen in Deutschland zusammen auf 72 Gigawatt - China hat also allein im Jahr 2015 83% der gesamten deutschen EE-Leistung installiert, für die wir hier Jahrzehnte benötigt haben.

    Ja, die Chinesen emittieren viel mehr, aber sie tun auch mehr dagegen. Kein Grund für uns, sich zurückzulehnen."

### "CO2 bringt Pflanzen schneller zum Wachsen"
* ...aber nur bis zu einer gewissen Grenze und außerdem sinkt dann der Nährstoffgehalt. Quelle?

### "Warum sollten ausgerechnet wir Vorbild sein?"
* weil wir es uns leisten können
* weil wir nicht von der Ölproduktion leben
* weil z. B. China schon wesentlich mehr macht als wir
    * z. B. bei Elektro-Autos gibt der Staat wesentlich ambitioniertere Vorgaben (http://www.tagesschau.de/multimedia/video/video-280415.html, 19.04.2017)

### Brückenlösung Atomkraft?
* siehe z. B. Schiffsinhaber-Erzählung, siehe wirksamkeit-des-einzelnen.md / Die Verantwortlichkeit der Intellektuellen
* siehe [Strom](../Strom]

Verschiedenes
-------------
### Zielkonflikt: Naturschutz vs. Klimawandel
* siehe [Strom](../Strom]

### Sammlung zu Kohlestrom und Kohleabbau
* siehe [Strom](../Strom]

### Videos
* Video: ["Was hat mein Essen mit dem Klima zu tun"](https://www.youtube.com/watch?v=COiCdy9opLw), 3 min, 2013, vom Bundeszentrum für Ernährung
    * ...
    * Schädlichkeit tierischen Produkten für das Klima
    * Wegwerfproblematik
    * Regional und Saisonal
    * Vorteile von Bio-Landwirtschaft (Obst und Gemüse)
        * Bei Bio-Tierprodukten ist umstritten, ob sie das Klima schonen
        * "ansonsten: Bio ist prima fürs Klima"
* Video: ["Schlau ist, wer Gutes tut - einfache Tipps für mehr Umweltschutz"](https://www.youtube.com/watch?v=61vpAfjsdGc), 2 min, 2012, Deutsche Bahn
    * u. a. Schädlichkeit Fleischkonsum
* 2018: [3 Videoclips mit Badesalz](https://www.youtube.com/results?search_query=badesalz+energiewende)

Aktiv werden
------------
... und/oder Aktive unterstützen

### Ende Gelände
https://www.ende-gelaende.org/

### Klimawende
* https://www.klimawende.org
    * "Klimawende von unten"
    * "Beratung - Als HerausgeberInnen des Leitfadens für die Klimawende von unten hoffen wir natürlich, Dich mit den von uns vorgestellten Erfolgsbeispielen und Handlungsempfehlungen begeistert und inspiriert zu haben. Aber in der Fülle der Möglichkeiten ist es nicht ganz einfach, den richtigen Ansatzpunkt zu finden und zu konkretisieren. Dabei wollen wir Dich gerne unterstützen!"

### Weitere
* XR
* FFF
