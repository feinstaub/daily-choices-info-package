Spenden-Atlas
=============

Ideen für zielgerichtetes und effektives Spenden für gemeinnützige Zwecke.

<!-- toc -->

- [Hinweise](#hinweise)
- [terres des hommes](#terres-des-hommes)
- [Amnesty International](#amnesty-international)
  * [Hintergründe: Fluchtursachen und Folgen für die Menschen](#hintergrunde-fluchtursachen-und-folgen-fur-die-menschen)
- [LobbyControl](#lobbycontrol)
  * [Lobbypedia](#lobbypedia)
  * [Astroturfing](#astroturfing)
  * [Country_Branding](#country_branding)
  * [Weiteres](#weiteres)
- [Campact](#campact)
- [foodwatch](#foodwatch)
  * [Aufklärung über Lobbyismus](#aufklarung-uber-lobbyismus)
  * [Tierwohl](#tierwohl)
- [KDE](#kde)
- [Albert Schweitzer Stiftung für unsere Mitwelt](#albert-schweitzer-stiftung-fur-unsere-mitwelt)
- [Animal Equality](#animal-equality)
- [Attac](#attac)
- [Weitere](#weitere)
  * [Amadeu Antonio Stiftung - für Zivilgesellschaft und demokratische Kultur](#amadeu-antonio-stiftung---fur-zivilgesellschaft-und-demokratische-kultur)
  * [Digitalcourage](#digitalcourage)
  * [Finanzwende](#finanzwende)
  * [Stiftung bridge](#stiftung-bridge)
  * [Bewegungsstiftung](#bewegungsstiftung)
  * [FIfF](#fiff)
  * [Germanwatch](#germanwatch)
  * [Bonventure](#bonventure)
  * [Wikimedia](#wikimedia)
  * [netzpolitik.org](#netzpolitikorg)
  * [Soko Tierschutz](#soko-tierschutz)
  * [ARIWA - Animal Rights Watch](#ariwa---animal-rights-watch)
  * [Menschen für Tierrechte](#menschen-fur-tierrechte)
  * [Rettet den Regenwald e.V.](#rettet-den-regenwald-ev)
  * [BILDblog](#bildblog)
  * [Survival International](#survival-international)
  * [Verein Mensch Tier Bildung e.V.](#verein-mensch-tier-bildung-ev)
  * [Das Peng! Kollektiv](#das-peng-kollektiv)
  * [Graslutscher](#graslutscher)

<!-- tocstop -->

Hinweise
--------
Worauf man bei der Auwahl der Organisation, der man spenden möchte, achten kann:

* Freiwilliges Reporting nach den Standards der [Initiative Transparente Zivilgesellschaft](https://www.transparency.de/Initiative-Transparente-Zivilg.1612.0.html)
* [DZI-Spenden-Siegel](http://www.dzi.de/spenderberatung/das-spenden-siegel/)

terres des hommes
-----------------
* Ziel: Weltweites Engagement für Kinderrechte, z. B. Einsatz gegen Kinderarbeit und gegen Kindersoldaten
    * siehe z. B. https://www.tdh.de/kleinwaffen
        * [Forderungen und Ziele](https://www.tdh.de/was-wir-tun/themen-a-z/kleinwaffen/forderungen-und-ziele/): "Rüstungsexporte müssen grundsätzlich gestoppt werden. Die bestehenden Rüstungskontrollgesetze reichen nicht aus."
    * 2018: "Die Linken beantragen ein grundsätzliches Exportverbot von Kriegswaffen und die Grünen ein striktes Rüstungsexportkontrollgesetz."
* Hinweis: Gegen Kinderarbeit kann man sich auch speziell bei jedem Schokoladenkauf stark machen. Inkl. dem Hinweis, wenn irgendwo Schokolade angeboten wird, unter deren Herstellung vermutlich mit Kinderarbeit zustandekam.
* [Kinderarbeitsreport 2015 - Kinderarbeit in der Thai Shrimp-Industrie](https://www.tdh.de/fileadmin/user_upload/inhalte/04_Was_wir_tun/Themen/Kinderarbeit/Shrimps-Studie/Zusammenfassung_Shrimp-Studie.pdf)
* [Webseite](https://www.tdh.de)

Amnesty International
---------------------
* Ziel: Weltweites Engagement für Menschenrechte, z. B. das Rechte auf freie Meinungsäußerung, Recht auf Privatsphäre etc.
* Besonderheit: nimmt kein Geld von staatlichen Einrichtungen an, um unabhängig zu sein, da Menschenrechtsverletztungen oft von staatlichen Gesetzen ausgehen.
* Besonders effektiv: Briefe gegen das Vergessen
* Regelmäßige Print-Publikation: Amnesty Journal
    * Aktuelle Situationsberichte
    * Film- und Buchtipps
* [Webseite](https://www.amnesty.de)
    * Spenden statt Schenken: https://www.amnesty.de/spenden-statt-schenken
* Themenauswahl
    * 2019, Neue Herausforderungen:
        * Kommunikation / Digitalisierung
        * Zivilgesellschaft stärken
        * Klimawandel
    * 2019:
        * "Facebook and Google’s pervasive surveillance poses an unprecedented danger to human rights" - https://www.amnesty.org/en/latest/news/2019/11/google-facebook-surveillance-privacy/
            * ...
            * "Facebook and Google must not be allowed to dictate how we live online. It is time to reclaim this vital public space for everyone rather than a few powerful unaccountable companies in Silicon Valley."
            * ...
            * "We called out Facebook and Google but still need them. That's exactly the problem.", https://www.amnesty.org/en/latest/news/2019/11/we-called-out-facebook-and-google-but-need-them/
    * 2017: Rüstungsexporte, gute Geschäfte mit Waffenverkäufen
        * z. B. Waffenmesse IDEX in Abu Dhabi
        * Umgehung von Exportrichtlinien
        * automatisierte / autonome Waffensysteme
        * Asylpolitik
    * Homophobie
        * Gegen Verfolgung und Diskriminierung von Homosexuellen in afrikanischen Ländern,z . B. Kamerun: https://www.amnesty.de/2014/2/6/lgbti-afrika
        * „Die meisten homophoben Gesetze sind ein direktes Erbe des Kolonialismus“ (https://www.amnesty.de/presse/2013/6/25/homophobie-afrika-nimmt-zu)
        * Schüren von Hass, Beispiel Brasilien 2017, https://de.wikipedia.org/wiki/Homosexualit%C3%A4t_in_Brasilien
        * siehe auch:
            * ["Undercover in der Konversionstherapie"](https://www.youtube.com/watch?v=p0efVz8qbpw), 2019
                * "Als Konversionstherapien bezeichnet man verschiedene Verfahren, in denen Homosexuelle und andere LGBT*-Menschen durch Anwendung verschiedener Methoden vermeintlich ihre sexuelle Präferenz hin zur Heterosexualität ändern können."
    * Rassismus
        * weltweit und in Deutschland
        * ["Als ob ich keine eigene Meinung hätte"](https://www.amnesty.de/journal/2017/april/als-ob-ich-keine-eigene-meinung-haette)
            * u. a. beleidigende Twitterbots
        * Aufmerksammachen auf (versteckten) Rassismus
    * Frauenrechte
    * Freedom of Speech = Meinungsfreiheit
        * Faktische Schranken: https://de.wikipedia.org/wiki/Meinungsfreiheit
        * Film: [Free Speech Fear Free](https://www.youtube.com/watch?v=zhXlkp7u0O8), 2016, todo, ein Film über die Meinungsfreiheit
            * https://www.themoviedb.org/movie/459163-free-speech-fear-free
            * Video: [2017‐05‐21 ‐ ttt ‐ Das Ende der Redefreiheit ‐ Die Dokumentation 'Free Speech, Fear Free'](https://www.youtube.com/watch?v=S1WJK2CUFoo), 5 min
                * Freiheit insgesamt wird im Namen der Sicherheit eingeschränkt
            * [Does Free Speech Offend You?](https://www.youtube.com/watch?v=9vVohGWhMWs), 5 min, 2015
            * [Two big reasons my generation hates free speech](https://www.youtube.com/watch?v=ErKrYhbHbzg)
    * Grundrecht auf Privatsphäre
        * z. B. 2017: "Mit europäischer Spähsoftware werden auf der ganzen Welt Proteste unterdrückt, Journalisten verfolgt und das Internet illegal überwacht"
    * Forderungen nach mehr Transparenz und Konsequenz im deutschen Waffenexportgesetz
        * z. B. https://www.amnesty.de/journal/2017/april/ausser-kontrolle
        * https://www.amnesty.de/allgemein/kampagnen/haende-hoch-fuer-waffenkontrolle
    * Aufmerksam machen auf unsinngige Gewalt
        * z. B. [Film über den Krieg mexikanischer Kartelle](https://www.amnesty.de/journal/2017/april/teuflische-freiheit), 2017, "La libertad del diablo"
    * Aufklärung, wie schädlich Rohstoff-Förderung für die Quell-Länder ist
        * z. B. 2017 gute Nachricht: "Als erstes Land der Welt hat El Salvador den Abbau von Gold verboten"
            * siehe auch http://www.zeit.de/wirtschaft/2016-01/schiedsgerichte-el-salvador-demokratie-schutz-trinkwasser-gold
                * "El Salvador sitzt auf reichen Goldvorkommen – und will sie im Boden lassen, um sein Trinkwasser zu schützen. Der Investor klagt [vor einem Schiedsgericht]: eine Kraftprobe für die junge Demokratie."
            * https://amerika21.de/2017/04/173123/el-salvador-mineralien-abbau
                * "In einer historischen Entscheidung hat das Parlament von El Salvador mit 69 von 82 Stimmen ein Gesetz verabschiedet, mit dem der Abbau von Mineralien in El Salvador verboten wird."
    * Menschenrechte
        * Zustände in Flüchtlingslagern
    * Rechte intergeschlechtlicher Kinder
    * Aufmerksammachen auf Staaten, in denen ein Klima der Angst herrscht
        * wo von Staatseite gegen oppositionelle Stimmen vorgegangen wird und wie
        * z. B. gegen kritische Medien oder bestimmte Bevölkerungsgruppen
    * https://www.amnesty.de/allgemein/und-was-hat-das-mit-mir-zu-tun
        * "Und was hat das mit mir zu tun? - Zehn Gründe, warum dir die Menschenrechte nicht egal sein können", siehe wirksamkeit-des-einzelnen.md

### Hintergründe: Fluchtursachen und Folgen für die Menschen
* Sachbuch: ["Bekenntnisse eines Menschenhändlers - Das Milliardengeschäft mit den Flüchtlingen"](https://www.kunstmann.de/buch/andrea_di_nicola-bekenntnisse_eines_menschenhaendlers-9783956140297/t-0/), 2015
    * "Illegale Immigration – ein Milliardengeschäft"
    * "Die Autoren haben entlang der Hauptrouten illegaler Immigration recherchiert und lassen die neuen Menschenhändler selbst sprechen: Anwerber und Skipper, Vermieter illegaler Unterkünfte, Geldhändler."

LobbyControl
------------
* Ziel: versteckten Lobbyismus, der sich gegen das Allgemeinwohl richtet, aufdecken.

* 2020
    * Masthuhn-Lobby-Professor aufgedeckt; Uni distanziert sich
    * Google-Lobby-Machenschaften
* 2019
    * Forderung Lobbyregister (schon seit längerem)
    * Beispiel Einladung Mitgliederversammlung: "Bitte teilen Sie uns mit, wenn Sie vegan essen möchten oder wenn Sie Unverträglichkeiten oder Allergien haben"
* 2017 Beispiele:
    * gegen Lobby-Werbung in der Schule
    * Cum/Ex-Steuerskandal
* Bekannte Print-Publikation
    * LobbyPlanet Berlin
* [Webseite](https://lobbypedia.de)

### Lobbypedia
* Entwicklung und Pflege der [Lobbypedia](https://lobbypedia.de)
    * Beispiele: https://lobbypedia.de/wiki/Microsoft, https://lobbypedia.de/wiki/Google
    * Beispiel: https://lobbypedia.de/wiki/Facebook, verlinkt z. B. auf ["Facebooks Schmutzkampagne gegen Google"](http://www.faz.net/aktuell/wirtschaft/netzwirtschaft/google/facebook-schmutzkampagne-gegen-google-1639096.html)
    * Erklärung von Lobby-Methoden: siehe nächste Abschnitte

### Astroturfing
* https://lobbypedia.de/wiki/Astroturfing
    * mit Fallbeispielen, z. B. [Monsanto / Astroturfing](https://lobbypedia.de/wiki/Astroturfing#Astroturfing_im_Internet:_Monsanto_und_die_Bivings_Group), Braunkohle etc.
    * [AstroTurf-Film](http://www.larrikinfilms.com/films/astro-turf-wars/), 2010

### Country_Branding
* https://lobbypedia.de/wiki/Country_Branding#Jersey

### Weiteres
* https://lobbypedia.de/wiki/Deep_lobbying, vor allem an Schulen
* [Lobbyismus an Schulen](https://lobbypedia.de/wiki/Lobbyismus_an_Schulen)
    * Dies ist auch sonst immer wieder Thema, u. a. weil Goolge und Microsoft immer stärker ins Klassenzimmer vorrücken wollen

Campact
-------
* Ziel: für eine progressive Politik streiten
    * https://www.campact.de/campact
        * "Unsere Kampagnen treiben sozialen, ökologischen und demokratischen Fortschritt voran – für eine Welt, in der alle Menschen ihre Freiheit gleichermaßen verwirklichen können."
        * Intro-Video
* Bekannt durch Online-Appelle vor wichtigen politischen Entscheidungen
* Große Medienreichweite z. B. durch aufblasbare Figuren (Motorsäge wegen Palmöl) am Ort des Geschehens (z. B. Hauptversammlung einer großen Bank)
    * großes Medieninteresse aufgrund der vielen Unterschriften der Online-Teilnehmer
* [Webseite](https://www.campact.de) (aktuelle Kampagnen)

foodwatch
---------
* Ziel: am IST-Zustand "Was wir essen, entscheiden nicht wir selbst. Die Verbraucherinnen und Verbraucher in Europa müssen machtlos zuschauen, wie die Nahrungsmittelindustrie der Politik die Spielregeln diktiert." etwas zum Positiven verändern
    * siehe http://www.foodwatch.org/de/ueber-foodwatch/mission/
    * "Alle Menschen haben ein Recht auf ausreichend Nahrung!"
    * "Wir haben ein Recht auf gesunde Produkte!"
    * "Wir haben ein Recht auf transparente Information!"
    * z. B. soll der Verbraucher erfahren dürfen, ob die Früche im Jogurth aus der Region oder China stammen
        * ob Gentechnik eingesetzt wurde, wie die Milchkühe gehalten wurden
        * nur durch dieses Wissen hat man Einfluss
* Mittel
    * E-Mail-Kampagnen
    * Aufdeckung von [Verbrauchertäuschung](http://www.foodwatch.org/de/informieren/verbrauchertaeuschung/)
        * [abgespeist Werbelügen](http://www.foodwatch.org/de/informieren/werbeluegen/2-minuten-info/) mit konkreten Fällen
        * [Wahl zum Goldenen Windbeutel](http://www.foodwatch.org/de/informieren/goldener-windbeutel/2-minuten-info/)
* Finanzierung: nur über private Spenden
* [Webseite](https://www.foodwatch.org)
* 2020: Nachverfolgung des Wilkes-Wurst-Skandals
* 2019: Nutriscore
* 2018: "Die Verantwortung wird auf die Verbraucher abgeschoben"

### Aufklärung über Lobbyismus
* Newsletter September 2018
    * "Wie groß der Einfluss der Lobbyisten tatsächlich ist, lässt sich an einigen erschreckenden Beispielen nur erahnen:"
        * "Die Wissenschaftler der Deutschen Gesellschaft für Ernährung (DGE) haben im Auftrag der Bundesregierung Qualitätsstandards für eine gute Ernährung in Kindergärten entwickelt. Aber: Das Ernährungsministerium hat sie danach nicht einfach veröffentlicht, sondern erst einmal dem Lobbyverband der Lebensmittelwirtschaft zur Stellungnahme gegeben – und die Standards daraufhin allen Ernstes abgesenkt! Die Lobbyisten freuten sich, dass sie „zahlreiche Inhalte“ noch „verbessern“ konnten. **Konkret: Künstliche Aromen, Geschmacksverstärker, Schmelzkäse oder Süßstoffe blieben in den Kitas weiter „erlaubt“. Und das entgegen dem Rat der unabhängigen Wissenschaftlerinnen und Wissenschaftler!** "
        * "Eine Milliarde Euro (!) hat die Lebensmittelindustrie EU-weit investiert, um die von zahlreichen Verbraucherinnen und Verbrauchern gewünschte Ampelkennzeichnung für Lebensmittel zu verhindern"
* z. B. Coca-Cola, siehe zucker-README.md

### Tierwohl
* https://www.foodwatch.org/de/informieren/versteckte-tiere/mehr-zum-thema/hintergrund/
* https://www.foodwatch.org/de/informieren/versteckte-tiere

KDE
---
* Ziel: ["A world in which everyone has control over their digital life and enjoys freedom and privacy."](https://dot.kde.org/2016/04/05/kde-presents-its-vision-future)
* [Manifest](https://manifesto.kde.org)
* https://userdatamanifesto.org - ["KDE signs the User Data Manifesto 2.0 and continues to defend your freedom"](http://blog.lydiapintscher.de/2015/09/03/kde-signs-the-user-data-manifesto-2-0-and-continues-to-defend-your-freedom)

Albert Schweitzer Stiftung für unsere Mitwelt
---------------------------------------------
* Vegan Taste Week
* siehe auch https://animalcharityevaluators.org/donation-advice/recommended-charities/, 2017

Animal Equality
---------------
* https://www.animalequality.de
    * https://de.wikipedia.org/wiki/Animal_Equality
        * "Die Organisation wurde 2006 unter dem Namen Igualdad Animal (span. für "Animal Equality") in Madrid gegründet"
    * Fleisch und Fisch: https://www.animalequality.de/essen/fleisch
    * Bildungsarbeit: https://www.animalequality.de/was-wir-tun
    * 2018: Doku über https://de.wikipedia.org/wiki/Mattanza
        * "traditionelle Thunfischjagd vor den Küsten Siziliens und Sardiniens"
* https://loveveg.de/
    * Erste Schritte, Rezepte, Gründe, Gesundheit, Tipps, Blog
    * Berühmt und vegan
* https://animalcharityevaluators.org/donation-advice/recommended-charities/, 2017
    * "Top Charities"
    * https://animalcharityevaluators.org/
        * "Learn how you can be a more effective advocate"
        * Video: ["Dan Pallotta: Unsere Denkweise über Wohltätigkeit ist grundverkehrt"](https://www.youtube.com/watch?v=bfAzi6D5FpM), 2013, 19 min
            * Markttheorie: z. B. für bestimmte benachteiligte oder nicht vom Glück begünstigte Gruppen gibt es keinen Markt
                * dafür benötigt man den Non-Profit-Sektor und Philantropie
                * für Leute, für die es keinen Markt gibt
            * Problem 1: Compensation
                * Veranschaulichung:
                    * Wenn jemand 1 Mio EUR verdient mit Computerspielen (die keinem helfen), ist er ein Geschäftsheld
                    * Wenn jemand 1 Mio EUR verdient, indem er versucht Malaria zu heilen (also anderen zu helfen), finden viele das anstößig
                * Zahlen von Gehältern
            * Problem 2: Advertising and Marketing
                * ...
            * ... todo
* News
    * 2018:
        * Studie: https://www.animalequality.de/neuigkeiten/neue-studie-fleischkonsum-ernaehrung-umwelt
    * ["Neues Gesetz in Luxemburg: Tiere haben eine Würde"](https://www.animalequality.de/neuigkeiten/neues-gesetz-luxemburg-tiere-haben-eine-wuerde), 2018
        * "Nach der Schweiz ist Luxemburg damit das zweite Land, das Tieren eine schützenswerte Würde zuspricht."
        * "Tiere dürfen nicht mehr aus rein wirtschaftlichen Gründen getötet werden"
        * "Tiere gelten nicht mehr als "Sache", sondern als Lebewesen."
    * Tierärztin: [„Die Tiere verstehen im Schlachthof, dass sie betrogen wurden“](https://www.animalequality.de/neuigkeiten/interview-tieraerztin-schlachthof), 2018
        * "Fehlbetäubung?" - "Es gab so gut wie immer einen Grund, dass es nicht richtig funktioniert hat."
    * https://www.animalequality.de/neuigkeiten/videokampagne-gegen-mcdonalds-times-square-new-york, 2018

Attac
-----
* Themen
    * "Globalisierung geht ganz anders – Mensch und Natur vor Profit"
        * https://www.attac.de/was-ist-attac/
    * Globalisierung
        * https://www.attac.de/themen/globalisierung/
            * "Der trügerische Glaube an "die Märkte""
    * Welthandel
    * Finanzmärkte & Steuern
    * Natur & Umwelt
    * Arbeit & Soziales
    * Privatisierung
    * Demokratie
    * Europa
    * Internationale Solidarität
    * Krieg & Frieden
    * Flucht & Migration
* Auswahl
    * ["Positionen der wichtigsten Parteien zu TTIP, CETA & Co"](https://www.attac.de/kampagnen/handelsabkommen/aktionen/abgeordnetenaktionen/parteipositionen/), 2014
    * Steuervermeidung: Artikel [Steuertricks internationaler Konzerne](http://www.attac.de/index.php?id=394&no_cache=1&tx_ttnews[tt_news]=8580), attac.de, 16.02.2016
    * Unkontrolliertes Datensammeln: ["Attac fordert wirksame Regulierung der Digitalökonomie - Datensammelwut fördert Steuervermeidung und gefährdet Demokratie"](http://www.attac.de/index.php?id=394&no_cache=1&tx_ttnews%5Btt_news%5D=9145), 07.04.2017
    * [Milchpulver](http://www.attac-netzwerk.de/ag-welthandelwto/milchpulver/hintergrund/)
    * Dezentralität:
        * Artikel: [Risiko einer Bankenkrise wie 2008 ist hoch](http://www.attac.de/index.php?id=394&no_cache=1&tx_ttnews[tt_news]=8593), 23.02.2016
        * siehe auch Ernährung / Was ist der Nachteil von Monopolen und Oligopolen?

Weitere
-------
### Amadeu Antonio Stiftung - für Zivilgesellschaft und demokratische Kultur
* "Die Amadeu-Antonio-Stiftung will eigenen Angaben zufolge eine demokratische Zivilgesellschaft zu stärken, die sich konsequent gegen Rechtsextremismus, Rassismus und Antisemitismus wendet. Der Name der Stiftung erinnert an den Vertragsarbeiter Amadeu Antonio Kiowa, der im November 1990 von rechtsextremen Skinheads in der brandenburgischen Stadt Eberswalde zu Tode geprügelt worden ist. Der 28-jährige Angolaner war eines der ersten Todesopfer rassistischer Gewalt nach der Wiedervereinigung." (http://faktenfinder.tagesschau.de/inland/kita-broschuere-101.html)
* Gründung 1998 (https://www.amadeu-antonio-stiftung.de/wir-ueber-uns/)
* [Handreichung: "Ene, mene, muh – und raus bist du! Ungleichwertigkeit und frühkindliche Pädagogik"](https://www.amadeu-antonio-stiftung.de/w/files/pdfs/kita_internet_2018.pdf)
    * Gegendarstellung des verzerrenden BILD-Echos: https://www.amadeu-antonio-stiftung.de/aktuelles/2018/klarstellung-es-geht-um-das-kindeswohl-und-nicht-um-blonde-zoepfe/

### Digitalcourage
* Ziel: todo
* 2020
    * Digitale Selbstverteidigung: übersichtliche Einführung von Privatsphären-Methoden und -Tools
* ...
    * https://digitalcourage.de/themen/facebook
        * mit Tipps zum Umgang für Organsisationen: https://digitalcourage.de/themen/facebook/facebook-eine-grundsatzentscheidung
        * Hintergrund: https://digitalcourage.de/themen/facebook/hintergrund-zu-kommerziellen-datenkraken
    * [Elektronische Gesundheitskarte](https://digitalcourage.de/themen/elektronische-gesundheitskarte)
        * Umfangreiches Statement
        * Alternativen zur geplanten/beschlossenen Cloud-basierten Lösung
            * z. B. Gesundheitsdaten von Arzt zu Arzt senden mittels PGP; sehr günstig
        * [Video 2015](https://vimeo.com/127256587), 18 min
    * https://netzpolitik.org/tag/digitalcourage/

### Finanzwende
* https://www.finanzwende.de, siehe dort

### Stiftung bridge
* [Webseite](https://www.stiftung-bridge.de)
* [Ziel](https://www.stiftung-bridge.de/warumbridge.html), Bürgerrechte in der digitalen Gesellschaft
* Mittel: Finanzierung von Projekten
* gegründet von der [Bewegungsstiftung](https://www.bewegungsstiftung.de)

### Bewegungsstiftung
* [Webseite](https://www.bewegungsstiftung.de)
* legt Stiftungsgelder [ethisch-nachhaltig](https://www.bewegungsstiftung.de/geldanlage.html) an
* Ziel und Mittel: "fördert soziale Bewegungen, die sich für Ökologie, Frieden und Menschenrechte einsetzen mit Zuschüssen und Beratung"
* Besonderheit: [Protestsparen](http://www.protestsparen.de)

### FIfF
* Forum InformatikerInnen für Frieden und gesellschaftliche Verantwortung e.V., ::frieden
* [Webseite](https://www.fiff.de)
* [Themen](https://www.fiff.de/themen)
* Videos und Vorträge z. B. [#FIfFKon16 – in.visible systems](https://2016.fiffkon.de/)
    * "in Kooperation mit dem Zentrum für Technik und Gesellschaft (ZTG), der Fachgruppe Informatik und Ethik der GI und dem CCC"
    * "In einer digitalisierten Gesellschaft untergraben unsichtbare Systeme die individuelle Selbst- und demokratische Mitbestimmung. Doch nicht nur das, die Manipulation von Denken und Handeln ist zur treibenden Kraft der IT-Entwicklung geworden."
    * z. B. Video: Vortrag zu [Facebook-Problemen](https://media.ccc.de/v/fiffkon16-4001-begr_ung), 8 min
    * z. B. Video: Vortrag ["Kleine Geschichten verborgener Technik"](https://media.ccc.de/v/fiffkon16-4002-kleine_geschichten_verborgener_technik)
        * "Interessierte oder gar versierte Nutzerinnen und Nutzer sind in der Schönen Neuen Welt nicht erwünscht"
    * z. B. Video: Vortrag zur [Cyberpeace Kampagne](https://media.ccc.de/v/fiffkon16-4013-cyberpeace#video), 30 min
    * z. B. Video: Vortrag ["Sie haben den Nutzen der Technik noch nicht rational erkannt! - Biometrie"](https://media.ccc.de/v/fiffkon16-4016-sie_haben_den_nutzen_der_technik_noch_nicht_rational_erkannt_biometrie_verstehen_und_akzeptieren), 45 min
        * Herstellung von Transparenz um zu überzeugen, dass z. B. biometrische Systeme unverzichtbar sind
        * fragwürdige Wachstums- und Akzeptanzstudien
* siehe auch http://pothos.blogsport.eu/other-works/

### Germanwatch
* ...

### Bonventure
* INSM Watchblog

### Wikimedia
* ...

### netzpolitik.org
* ...

### Soko Tierschutz
https://www.soko-tierschutz.org

Aktuelle Recherchen nach dem Motto: wir nehmen die Verantwortlichen, die in der Tierindustrie arbeiten, beim Wort und schauen regelmäßig nach, inwieweit die versprochenen Verbesserungen umgesetzt werden.

* 2018
    * "Die Kadaverfarm von Demker will vor Gericht 08/12/2018"
        * [stern TV](https://www.youtube.com/watch?v=PZ03THd-G6I), 2018, Milchviehgroßbetrieb in Sachsenanhalt, 9 min
            * Videoaufnahmen von tagelang tot rumliegenden Kühen
            * zuständigen Behörden machen trotz Mitteilung nichts
                * als sie Tage später schauen, wird alles als in Ordnung dokumentiert
                * laut Fotoaufnahme tage später liegt die Kuh aber noch herum
            * Videoaufnahmen von Kälbchen, dass vom Kotschieber vor sich hergeschoben wird
            * Abnehmer der Milch darf aus rechtlichen Gründen nicht genannt werden
                * stellt Trinkmilch, Sahnepulver und Babynahrung her
            * ...
    * Kurz-Video über die Zustände in einem Niedersächsischen Schlachtbetrieb
        * Rausziehen vom LKW mit der Seilwinde
        * Anlieferung von Kadavern (kriminell)
    * ["Ich kaufe nur bei meinem Metzger um die Ecke"](https://www.youtube.com/watch?time_continue=166&v=vizlGlACiyQ), 3 min
        * der Schlachthof des Vertrauens ("Landmetzgerei") wurde nach dokumentierten krassen Verstößen gegen das Tierschutzrecht geschlossen
        * Es geht um das Schlachten von Kühen aus der Milchindustrie
        * Was passiert dort, wo keiner hinschaut?
        * [ARD-FAKT-Reportage](https://www.youtube.com/watch?v=hXwkem9vgi4), 5 min
            * dem Amt ist wiederholt nichts aufgefallen
            * damit solche Zustände möglich werden, muss eine ganze Kette von Akteuren entsprechend wegschauen
    * ["Mut für mehr Zivilcourage: Die Initiative SOKO Tierschutz und Taina Gärtner sind die Preisträger*innen des diesjährigen Panter Preises."](http://www.taz.de/Panter-Preis-2018/!167237/)
        * Mülln, "SOKO Tierschutz e. V.", https://www.youtube.com/watch?v=uVs7JfEshXM, 3 min, taz, 2018, **Zivilcourage**
            * Undercover-Aktivist: "Jede Grausamkeit, die man sich ausdenken kann, wird leider gemacht."
            * Grausamkeit der Zeitersparnis wegen
            * Undercover-Aktivist stellt fest: "Jede Grausamkeit, die man sich ausdenken kann,
                wird leider gemacht.
                Es gibt wenig, was mich wirklich noch schocken würde.
                Einfach: weil es passiert alles und man gewöhnt sich daran
                und man stumpft ab, weil dieses System ist einfach auf unglaubliche Grausamkeit aus.
                Die Leute wollen vielleicht nicht grausam sein, aber es wird halt irgendwann normal.
                Es ist so - es ist mein Job.
                Das meiste, was grausam ist, ist halt, weil es zeitsparend sein muss.
                Dann wird eben ein Ferkel nicht mehr betäubt, sondern man schlägt es einfach
                solange auf den Boden, bis es tot ist - oder vermeintlich tot ist. Das spart Zeit."

### ARIWA - Animal Rights Watch
http://www.ariwa.org

* 2018
    * TV-Beiträge: http://www.ariwa.org/ueber-uns/ueber-uns/tv-beitraege.html
        * "Vergessen im Stall - 26.03.2019 | 6 Min." - http://mediathek.daserste.de/FAKT/Vergessen-im-Stall/Video?bcastId=310854&documentId=61505226
            * "Rinder dürfen ganzjährig angebunden werden. Schon 2016 wollte der Bundesrat diese Form der Anbindehaltung verbieten. Doch bis heute scheitert das Vorhaben an den Bauernverbänden."
        * "Schlachtfehler: http://www.ariwa.org/aktivitaeten/1739-grausame-schlachtfehlerq-auch-im-bio-schlachthof-nebenan.html
            * Video_: "Grausamer Alltag im Schlachthof nebenan"
    * http://www.ariwa.org/aktivitaeten/aufklaerung/aktionenarchiv/1634-2018-05-30-15-16-24.html
        * Berichte von "Großdemonstrationen für die Schließung aller Schlachthäuser 2018"

* http://www.ariwa.org/aktivitaeten/aufklaerung/videos.html
    * http://www.ariwa.org/aktivitaeten/aufklaerung/videos/1842-woher-kommen-die-eier-in-kuchen-keksen-und-co-video.html, 3 min
    * http://www.ariwa.org/aktivitaeten/aufklaerung/videos/1456-die-wahrheit-hinter-der-heilenq-welt-bei-bioland-video.html, 5 min
    * Elterntiere: Eierproduktion für die Hühnermast
        * http://www.ariwa.org/aktivitaeten/aufklaerung/videos/1382-2017-03-28-15-51-30.html, 4 min
            * "Bilder aus fünf Elterntierbetrieben von Europas größtem Anbieter von Masthuhnküken. Elterntiere werden dazu benutzt, Eier zur Mastkükenproduktion zu legen und sind einer bisher kaum thematisierten Mehrfachbelastung ausgesetzt. Hohe Reproduktion bei gleichzeitig unnatürlicher Gewichtszunahme führen zu besonders hohem Tierleid."
        * http://www.ariwa.org/aktivitaeten/aufgedeckt/recherchearchiv/1381-2017-03-28-15-14-47.html
            * Hungern: "Die Tiere müssen über 23 Stunden am Tag Hunger leiden. Denn auch die Eltern der Masthühner sind genetisch auf unnatürliches Fleischwachstum gezüchtet. Dürften sie ihren Hunger stillen, so würden sie stark übergewichtig, ihre Reproduktionsleistung würde sinken"
            * "In Elterntierhaltungen werden weibliche Hühner und männliche Hähne zusammen gehalten, damit sie sich gegenseitig befruchten. Immer wieder springen die Hähne auf die Hennen und fügen ihnen schmerzhafte, blutende Wunden zu. Krankenbuchten, in denen verletzte und kranke Tiere separiert werden könnten, gibt es in keiner der Anlagen."
            * "Nach etwa 14 Monaten werden die Tiere getötet – dann nimmt ihre Legeleistung ab und sie sind nicht mehr rentabel. Jede sechste Henne und jeder dritte Hahn verenden jedoch vorzeitig."
            * "Die Eltern der Masthühner werden genau wie diese als bloße Produktionseinheiten und nicht als Lebewesen angesehen."
            * "Die Zukunft gehört einer rein pflanzlichen Ernährung und einer nachhaltigen, bio-veganen Landwirtschaft."

### Menschen für Tierrechte
* https://www.tierrechte.de/wp-content/uploads/2019/10/leitbild.pdf
    * "Die Menschen für Tierrechte – Bundesverband der Tierversuchsgegner e.V. vertreten in einer pluralistischen Gesellschaft Wertefür eine Kultur der Tierrechte."
    * "Wir sind Menschen verschie-denen Alters, unterschiedlicherweltanschaulicher Herkunft und kommen aus unterschied-lichen Berufsgruppen.Gemeinsam ist uns, die Tierrechte zu achten"
    * ...
    * "Die Situation der Tiere in unserer Gesellschaft und der Umgang mit ihnen
        sind ein Teil und Spiegelbild einer wirtschaftlichen Verwertungsmaschinerie
        und des vorherrschenden **Gewaltprinzips**"
        * "Tiere haben aufgrund ihrer biologischen Ausstattung keine Möglichkeit, ihre Interessen einzufordern und zu verteidigen."
    * ...
    * "Wir versuchen, Andersdenkende zu überzeugen, sie ernst zu nehmen und ihre Situation zu verstehen.
        Wir sind ungeduldig, weil Tiereweiterhin im Elend leben"
    * ...
* https://www.tierrechte.de/2018/02/19/tierschutz-ist-ein-gesamtgesellschaftlicher-bildungsauftrag/
    * Interview mit **Dr. Madeleine Martin**
    * "Tierschutz ist ein **gesamtgesellschaftlicher Bildungsauftrag**.
        Es geht im Kern um die ethische Frage: **Wie gehe ich mit Schwächeren oder mit Andersartigen um?**"
    * ...
* Bio-vegane Landwirtschaft: https://www.tierrechte.de/category/themen/bio-vegane-landwirtschaft/
* https://www.tierrechte.de/category/themen/tierrechte/missstaende-beim-vollzug/
    * "**Trotz Staatsziel Tierschutz** und einem umfangreichen Tierschutzrecht schützt unser Rechtsstaat die Tiere nicht zuverlässig."
* https://www.tierrechte.de/2019/08/16/keine-einzelfaelle-repressalien-gegen-amtstieraerzte/
    * "Keine Einzelfälle: Repressalien gegen Amtstierärzte"

### Rettet den Regenwald e.V.
* https://www.regenwald.org/
* https://de.wikipedia.org/wiki/Rettet_den_Regenwald

### BILDblog
* siehe medien.md

### Survival International
* https://de.wikipedia.org/wiki/Survival_International
    * "Wir helfen indigenen Völkern ihr Leben zu verteidigen, ihr Land zu schützen und ihre Zukunft selbst zu bestimmen."
* https://www.survivalinternational.de/kolonialer-naturschutz
    * "Naturschutz dekolonisieren - Indigene Völker sind die besten Naturschützer"
* https://www.survivalinternational.de/artikel/3533-vegan-und-naturschutz
    * "Ich bin Veganerin und Tierliebhaberin – aber ich sehe Naturschutz kritisch", 2018
    * ...
* https://www.survivalinternational.de/kampagnen/diegro%C3%9Fegr%C3%BCnel%C3%BCge
    * "Die große grüne Lüge"

### Verein Mensch Tier Bildung e.V.
* https://mensch-tier-bildung.de/uber-uns/
    * "wurde 2015 von Wissenschaftler_innen, Pädagog_innen und ehrenamtlich Engagierten in Berlin gegründet."
    * "Wir möchten die Teilnehmenden in unseren Bildungsangeboten dazu ermutigen und befähigen, sich gesellschaftskritisch mit dem bestehenden Mensch-Tier-Verhältnis sowie dessen Bedingungen und Folgen auseinanderzusetzen. Als Teil der Gesellschaft ist das Mensch-Tier-Verhältnis von Menschen gemacht und damit veränderbar. Es ist aus unserer Sicht nicht isoliert zu betrachten, sondern steht in engem Zusammenhang mit anderen gesellschaftspolitischen Fragestellungen."
* https://mensch-tier-bildung.de/uber-uns/personen/
* https://mensch-tier-bildung.de/uber-uns/foerderung/
* Impressionen: https://mensch-tier-bildung.de/impressionen/
* ["Workshops und Projekttage für Schulen"](https://mensch-tier-bildung.de/bildungsangebote/workshop-projekttag-tiere-landwirtschaft-schule/)
* https://mensch-tier-bildung.de/tierschutz-im-unterricht-schule/
    * "Gefühle ernst nehmen und ethische Einstellungen unterstützen"

### Das Peng! Kollektiv
* 2020: https://pen.gg/de/campaign/klingelstreich/ - "klingelstreich beim Kapitalismus"
    * "Wie wir die Chefs der deutschen Industrie anriefen und ihre Unternehmen regulierten"
    * "„Wir brauchen Zielvorgaben der Politik“"
    * "Die Fleischwirtschaft wäre theoretisch in der Lage, auf 100% veganes Essen umzustellen."

### Graslutscher
* https://graslutscher.de/arte-filmemacher-wollen-die-klimakrise-jetzt-durch-verzicht-auf-windkraft-und-e-autos-loesen/
* https://graslutscher.de/unterstuetzer/
* https://www.patreon.com/graslutscher
