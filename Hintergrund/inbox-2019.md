Inbox 2019 (unsortiert)
=======================

<!-- toc -->

- [Bio](#bio)
  * [Pestizide](#pestizide)
  * [Probleme beim Zuckerrübenanbau?](#probleme-beim-zuckerrubenanbau)
  * [Vegan für Großküchen](#vegan-fur-grosskuchen)
  * [Vegan von Bio-Pionieren](#vegan-von-bio-pionieren)
  * [Engagement](#engagement)
- [Vegan](#vegan)
  * [Umwelt](#umwelt)
  * [Anlass Fastenzeit](#anlass-fastenzeit)
  * [Fragen für Einsteiger](#fragen-fur-einsteiger)
  * [Für Einsteiger](#fur-einsteiger)
  * [Philosophie](#philosophie)
  * [Auswirkungen von Billig-Schweinefleisch](#auswirkungen-von-billig-schweinefleisch)
  * [Fernsehen](#fernsehen)
  * [Argument, Debatte](#argument-debatte)
  * [Beispiele](#beispiele)
  * [Smartphones](#smartphones)
- [Nachhaltigkeit / Umwelt](#nachhaltigkeit--umwelt)
  * [Wie schaffen wir die Agrarwende?](#wie-schaffen-wir-die-agrarwende)
  * [Ruinöse Viehpreise](#ruinose-viehpreise)
  * [Veränderungsprozesse einleiten](#veranderungsprozesse-einleiten)
  * [Auswirkungen auf Kinder](#auswirkungen-auf-kinder)
  * [Philosophie](#philosophie-1)
- [Free Software / Datenschutz / Kontrolle](#free-software--datenschutz--kontrolle)
  * [Für Einsteiger](#fur-einsteiger-1)
  * [Mobilfunk: wetell, 5G-Kritik](#mobilfunk-wetell-5g-kritik)
  * [Public money, public code](#public-money-public-code)
  * [Bildung: Programmieren vs. kritisches Denken](#bildung-programmieren-vs-kritisches-denken)
  * [Reparieren, Second-Hand, Elektronik-Schrott, Nachhaltigkeit](#reparieren-second-hand-elektronik-schrott-nachhaltigkeit)
  * [Privatsphären-gefährdende Unternehmen](#privatspharen-gefahrdende-unternehmen)
  * [Geschäftsmodelle](#geschaftsmodelle)
  * [NextCloud](#nextcloud)
  * [Digitaler Konsum](#digitaler-konsum)
- [Wirtschaft](#wirtschaft)
  * [Die Große Geldflut](#die-grosse-geldflut)
  * [Paketdienste](#paketdienste)
- [Gemeinnützigkeit](#gemeinnutzigkeit)
- [Gerechtigkeit](#gerechtigkeit)
  * [Ungerechtigkeit fördert Kriminalität](#ungerechtigkeit-fordert-kriminalitat)
  * [Weniger Ungerechtigkeit](#weniger-ungerechtigkeit)
- [Lärm](#larm)
  * [Ist es zu laut bei uns? Der Lärm und seine Folgen](#ist-es-zu-laut-bei-uns-der-larm-und-seine-folgen)

<!-- tocstop -->

Bio
---
### Pestizide
* NABU-Wanderausstellung "Irrweg Pestizide"
    * siehe Begleitheft
        * Buch über Pestizid-Wirkung auf Kinder
        * Wirkung auf Tiere
        * ...
    * ...

### Probleme beim Zuckerrübenanbau?
Anscheinend Ja:

    * https://ackergifte-nein-danke.de/news/365-pestizide-vermeiden-beim-zuckerruebenanbau.html
    * https://www.mellifera.de/blog/bienen-schuetzen/expertenworkshop-zuckerruebe.html

### Vegan für Großküchen
* ["Vegan und bio bereichern die Großküche"](https://www.oekolandbau.de/grossverbraucher/betriebsmanagement/betriebswirtschaft/biospeiseplanung/vegan-und-bio-bereichern-die-grosskueche/)
* ["Trend vegane Kost - für Großküchen"](https://www.oekolandbau.de/grossverbraucher/betriebsmanagement/betriebswirtschaft/biospeiseplanung/trend-vegane-kost-fuer-grosskuechen-machbar/)
    * "Vegane Küche bedeutet für Christian Kolb, BioSpitzenkoch aus Bad Vilbel, weniger Verzicht als vielmehr eine echte Alternative und spannende Herausforderung"
    * "Es darf auch mal ersetzt werden", mit Tabelle
    * ...
    * "Vegane Kost ist im Kommen und wird nicht nur von jungen ernährungs- und umweltbewussten Menschen gelebt. Viele Einrichtungen der Gemeinschaftsverpflegung sehen in der veganen Küche einen attraktiven Weg, auf die steigende Anzahl an Allergien, beispielsweise auf Milchprodukte, zu reagieren."

### Vegan von Bio-Pionieren
* https://www.vegorganic.de/ueber-uns/

### Engagement
* Der Maulwurf, der Licht ins Dunkle bringt. Oder (Marketing): sieht nur die Maulwurfshügel auf einem grasgrünen Rasen, wo eigentlich Blumen- und Wildkräuterwiese sein sollte inkl. Maulwurfshügel.

Vegan
-----
### Umwelt
* https://animalequality.de/blog/weniger-treibhausgase-durch-vegane-ernaehrung/
    * https://www.spiegel.de/wissenschaft/mensch/veganer-sparen-jaehrlich-zwei-tonnen-treibhausgase-a-1264577.html, 2019
        * "Ernährung und Klimaschutz - Jeder Veganer spart jährlich zwei Tonnen an Treibhausgasen"
        * "Der weltweit wachsende Fleischkonsum schadet dem Klima. Wer ausschließlich Pflanzenkost isst, belastet die Umwelt erheblich weniger. Dies belegen neue Zahlen der Universität Oxford."
            * weltweit wachsend => unsere Vorbildfunktion nutzen!
        * "Pro Kopf und Jahr produzieren die Deutschen durchschnittlich elf Tonnen Treibhausgase. Wer vegan lebt, reduziert seine Bilanz laut Poore um zwei Tonnen jährlich, bei ansonsten gleichbleibendem Lebensstil. Das entspricht, je nach Berechnungsgrundlage, in etwa acht Economy-Class-Flügen zwischen London und Berlin."
        * "Ergebnisse veröffentlichten sie bereits 2018 in der Fachzeitschrift "Science". In ihrem Papier kommen die beiden zu dem Schluss, dass selbst Tierprodukte mit einer verhältnismäßig guten Ökobilanz - Eier oder Geflügel - Umwelt und Klima stärker belasten als Pflanzenkost."
* https://www.economist.com/briefing/2018/10/13/why-people-in-rich-countries-are-eating-more-vegan-food
    * u. a. Graphik, die zeigt wieviel wertvolle Nahrung man allein dadurch verliert, dass man die Pflanzenproteine zunächst an Tiere verfüttert, bevor sie in den menschlichen Konsum gehen
* Vorurteil «Wer Soja isst, zerstört den Regenwald»
    * NZZ: https://www.nzz.ch/panorama/montagsklischee/soja-wird-hauptsaechlich-fuer-tierfutter-produziert-1.18335485, 2014
        * "Die weltweite Soja-Produktion steigt und steigt. Doch der Grund dafür ist nicht unser erhöhter Tofu- und Soja-Latte-Konsum. Soja wird heute fast ausschliesslich für die Fütterung von Schlachttieren angebaut."
    * Lobby-Seite: https://www.dvtiernahrung.de/aktuell/faqs-oder-haeufige-fragen-und-vorurteile/haeufige-irrtuemer-zu-tierfutter.html
* Graphik: https://www.quarks.de/umwelt/landwirtschaft/so-eindeutig-sind-nutztiere-in-der-ueberzahl/, 2019
    * Zahlen: "Viehzüchter produzieren massig Tierbestände. Gleichzeitig sinkt die Zahl der wilden Säugetiere. Das Ergebnis: Über 90% aller Säugetiere leben, um geschlachtet zu werden."
    * "Es gibt etwa 15 Mal so viele Rinder, Schafe und Co. auf unserem Planeten wie wilde Säugetiere."
    * Zahlen: Säugetiere weltweit: 100 Mt Nutztiere, 3 Mt Landsäugetiere, 2 Mt Vögel

### Anlass Fastenzeit
* ["Caro Cult unterstützt Vegan-Kampagne"](https://www.ardmediathek.de/ard/player/Y3JpZDovL21kci5kZS9iZWl0cmFnL2Ntcy8wOGZhZDFkMy1hMTI5LTQ4NTEtOGE3NC05MjNmNjYwMTBiNzA/), ARD, 2019
    * "Während der Fastenzeit nur vegan essen: Viele Prominente machen mit. Paul McCartney, Nena oder Caro Cult sind bei der Kampagne der 12-jährige Aktivistin Genesis Butler dabei."

### Fragen für Einsteiger
* Was haben Haus- und Nutztiere gemeinsam?
* Welche Bedürfnisse sind festzustellen? (insbesondere Selbsterhaltung / Lebenswillen)
    * siehe lifespan-cc.jpeg
    * siehe https://friederikeschmitz.de/meine-landwirtschaft-der-zukunft/, siehe beispiele.md
        * "der Tierarzt wird zum Beispiel nur geholt, wenn es sich wirtschaftlich noch lohnt."
        * "Und natürlich wird jedes so genannte Nutztier gegen seinen eigenen Lebenswillen gewaltsam getötet, sobald das eben wirtschaftlich sinnvoll ist."
* Welche Unterschiede in der Behandlung sind festzustellen?
    * Sind diese in den Bedürfnissen begründet?
    * Warum nicht?
    * siehe "Was ist Dis­kri­mi­nie­rung?"
* Ideologie-Einwurf
    * siehe "Wo ist die Ideologie?"
    * Beispiel: "Wir schützen Tiere ohne Ideologie."
        * Was heißt das genau?
        * Andersrum: In welchem Bereich und aufgrund welcher Ideologie werden die heute immer noch Tiere wider besseren Wissens extrem tierunfreundlich behandelt?
* Fragen von Einsteigern
    * Also keine Tierhaltung mehr?
        * https://friederikeschmitz.de/meine-landwirtschaft-der-zukunft/, siehe beispiele.md

### Für Einsteiger
* Für **starke Männer**
    * [Diese Omi sagt wo's langgeht - "When you're vegan AF"](https://peertube.mastodon.host/videos/watch/423ff81c-0c89-4e3f-90d3-dbda11a9edb8)
        * warning - explicit language all over the place
        * Turducken
        * yt: https://www.youtube.com/watch?v=gDirubjWFk4 - Tofucken

* Videos on https://www.kinderworld.org
    * ["The Secret Marketing Weapon of Meat, Dairy, and Eggs"](https://www.kinderworld.org/videos/talks/secret-marketing-weapon-meat-dairy-eggs/), 7 min
        * "British actress Kate Miles acting as Kate Cooper" (https://medium.com/@victoriahalina/these-days-its-way-too-easy-to-bs-your-way-to-the-top-1438db565aed?)
        * Instant Mix Cakes from the industry
        * 1. "Progress"
        * 2. ...
        * Secret weapon: you
            * "**The power of willful ignorance** cannot be overstated. ... This is **systemized cruelty on massive a scale**
            and we [the marketing people] only get away with it because everybody is prepared to look the other way."
    * ["Dairy is Scary"](https://www.kinderworld.org/videos/dairy-industry/dairy-is-scary-erin-janus/), how milk is produced
    * ["Feminism and Veganism"](https://www.kinderworld.org/videos/talks/feminism-veganism/), 2018, 6 min
        * about rape
    * ... weitere Videos ...

* Psychopaths examples...
    * Video: "Norm" - A Short Film - https://www.youtube.com/watch?v=poxl0K9UrP0, 12 min, 2017, "the norm"
    * https://www.youtube.com/watch?v=Ag0c930-FNQ, Ed Winters, "Meat Eater Decides to Challenge Vegan Activist", 2020, 25 min, debate a nihilist
        * "Humans and non-human animals are resources..."
        * YT comments: "Isn't it mind blowing listening to a true psychopath by definition"
            "He only doesn’t kill other people because it’s against the law."
            "He also said the reason he doesn't eat people is because they might sue him, whereas cows can't file lawsuits."
            "Yep im sure he would be doing plenty of raping at that campus if there weren't laws against it"
            "he may have empathy. I'm not convinced he doesn't have that quality. ..."
            "Like Paul McCartney said "You can judge a mans character by how he treats animals""
            "Maybe he's just capable of turning on and off the psychopath switch, which I think you could make a better argument for that. Like a Nazi soldier that comes home and treats his kids & wife with love, but he's still a Nazi at the end of the day."
            "nihlism isn't an argument. it's admitting defeat."
                "The argument for nihilism is among the hardest and most frustrating for me to hear."
            "People who blindy follow societys laws without questioning it are the most dangerous type of people"
            "I think 1 sentence sums up the whole conversation, he says "I don't think I could live without milk", everything he says before that is just a justification for him wanting to continue eating dairy."

* Wer ist PETA? / Darmstadt
    * https://www.galileo.tv/video/wer-steckt-hinter-peta/, 2015, ProSieben

* ["Vegan 2018 - Der Film"](https://www.youtube.com/watch?v=bTeqU6_Jryc), Plant based news, Nov 2018, 60 min
    * ...
    * BBC
    * tradition
    * goodall: what you as individual do each day _is_ affecting what is going on in the world each day
    * good summary of cubes and activists which film industry standard
        * -> many people only then realize that change is necessary (and could ruin this industry)
    * ...
    * Milch
        * Oatly-Werbung: "It's like milk but made for humans."
            * https://www.plantbasednews.org/post/its-like-milk-but-made-for-humans-oatly-campaign-uk
            * https://www.thedrum.com/news/2018/10/17/after-angering-swedish-dairy-industry-oatly-brings-controversial-ad-campaign-the-uk
    * ...
    * Ed: most things in animal industry is not transparent. -> Why? -> Why not make it transparent?
        * many people still don't know!
    * ...

### Philosophie
* ["What vegans and non-vegans alike don’t get about veganism, and why dialogue between two sides often results in unnecessary confrontation"](https://medium.com/ephemeral-living/what-vegans-and-non-vegans-alike-dont-get-about-veganism-c60913a8ca5b), 2017
    * What
        * ..., "Alignment with this doctrine is solely what makes something vegan, not anything else."
        * ...
        * "Others can be second-hand leather, fur, and down. This is because those instances of procurement do not result in the oppression of an animal."
        * "As veganism is only a principle, one becomes a vegan by merely agreeing with the principle, despite differing application. There exists times where it is necessary to harm an animal, in order to prevent harm to yourself — which affects application, but not belief. However, by having that belief, one is likely to attempt avoiding making that same violation again in the future. Such as packing more vegan food next trip so one doesn’t need to procure non-vegan food, or cleaning up your kitchen properly so one doesn’t need to kill more pests."
        * ...
        * "Like any movement, veganism unfortunately is incorrectly associated with the common and not-so-common individual practices of its followers, rather than by the actual doctrine of it."
        * ...
    * Violations
        * "There is no disagreement that while animals are actively enslaved, that striving to improve their conditions is fantastic for the animal, improves the conscience of the farmer, and the quality of the produce. The stance that the vegan doctrine takes however is that we should strive to not violate the animal in the first place."
        * "The most common violations are the killing for meat, the sexual abuse for breast milk, and the concentration camps for eggs"
        * "However, what all these necessity pleas fail to realise is **that necessity doesn’t make a violation no longer a violation**. Even despite the violation now having some justifications to its necessity, the violation still exists and should still be avoided."
        * ...
        * "These violations also come at an expense to ourself, as violations of our own value of kindness and compassion, and to our own belief in civil liberty and reciprocity."
            * "Veganism resolves these particular instances of cognitive dissonance through consumer choice and education, refined corporate practices, and government regulations."
        * "However, necessary violations are still violations, and a violation is still something that should be aimed to be avoided. Killing against a victim’s will is bad, it is a violation of someone else’s agency. Killing in self defence can be justified, however despite the justification, a violation has still occured, and it should still be a situation to avoid occuring in the first place."
        * ...
        * "The idea that necessity eliminates violations, seems to stem from a naive belief that one is, or even can be, violation free. But one who has killed, even when justified, is still a killer. No amount of neccessity surrounding a violation removes the fact that the violation still occurred. All that the necessity states is that the violation was justified, not that it did not occur. Being violation free in the face of a violation is wishful and delusional thinking, a longing for a sense of purity that is nonexistent."
    * Neccessity
        * "Since the dawn of mankind, we’ve created endless beliefs and rationalisations to provide comfort for the ceaseless angst that unavoidable violations play on our conscience, often with religious practices. Or we downright ignore it, never resolving our dissonance, becoming quick to trigger."
        * "As technology and society progresses, less moral violations remain necessary, updating our belief systems accordingly."
        * "Now, for the first time in history, a nutritionally complete/comparable selection of plant products are available to the average supermarket consumer, regardless of the consumer’s season and budget — making veganism now enter the public consciousness, as oppressing animals wanes into the unncessary."
        * "Saying the prayer before slaughter/eating “thank you God for forgiving my sin in taking this animal’s life so that I can live and not die from starvation” hardly applies when the animal was killed mass-market by someone else so you can enjoy their flesh in an airplane at 35,000 ft flying in the sky — there is no unavoidable necessity of starvation in that circumstance — yet the legacy of those rational delusions sputter onward."
        * "Do those justifications ever make that atrocity okay? No. Should atrocities always be sought to be avoided? Yes."
    * The Messenger
        * "This isn’t to say we should drop everything we do and become a vegan or a jain, it is to say we should educate ourselves on all exposed violations and identify which are ultimately avoidable and passionate in our own lives. One can hate the messenger or their delivery, but to ignore their valid points just because one hates the messenger, is a behaviour that is unvirtuous by any standard."

### Auswirkungen von Billig-Schweinefleisch
* Arte-Doku 2017, Jens Niehuss, 1h 30min
    * https://www.arte.tv/de/videos/064368-000-A/armes-schwein-fettes-geschaeft/
        * "Deutschland ist europaweit der günstigste Anbieter von Schweinefleisch"
        * "in Agrarfabriken mit mindestens 10.000 Tieren. Ganze fünf Arbeiter braucht es dazu."
        * "exportieren die Konzerne rund 50 Prozent des Fleisches nach Europa und in die ganze Welt."
        * Billig-Löhner werden aus Osteuropa über Subunternehmen nach D. gelockt und ausgebeutet
        * "die Gülle bleibt in Deutschland. [...] und einen hohen Preis hat: Durch Gülle verseuchte Böden und Trinkwasserbrunnen, über Steuergelder bezahlte EU-Strafzölle für fehlende Umweltauflagen, hoher Antibiotika-Einsatz in der Massentierhaltung"
        * ab ca. 2005 seit Seehofer dafür gesorgt, dass die Industrialisierung der Schweineproduktion von Großkonzernen massiv subventioniert wird
        * Beispiel Frankreich, wo dortige Konzerne mit der Billigkraft von D. nicht mehr mithalten können und reihenweise schließen
        * ...
    * https://www.youtube.com/watch?v=PhrPEcDPcVI

### Fernsehen
* Zapp-App
    * April 2019: WDR: Münsteranrer Tierrechtler (Pelze, Tierhaltung, Tierschutz im Grundgesetz, Rechtsprofessorin in Heidelberg)
    * April 2019: SWR: Patricia Kopietz kennt sich mit den unschönen (und versteckten) Details der Jagd aus / vegan

### Argument, Debatte
* ["IntelligenceSquared Debates: Don't Eat Anything With A Face"](https://www.youtube.com/watch?v=OCcJq56ZMJg), 2013
    * voting by audience pre and post the debate: vegan arguments clearly win

* ["More Doctors Smoke Camels Than Any Other Cigarette"](https://www.youtube.com/watch?v=gCMzjJjuxQI)

### Beispiele
* ["I ate a vegan diet for 3 years | Here's what I learnt"](https://www.youtube.com/watch?v=XEMfTs_S9CE), 2019
    * Bodybuilder

### Smartphones
* "Smartphones Are Killing The Planet Faster Than Anyone Expected -
    Researchers are sounding the alarm after an analysis showed that buying a new
    smartphone consumes as much energy as using an existing phone for an entire decade."
    * https://www.fastcompany.com/90165365/smartphones-are-wrecking-the-planet-faster-than-anyone-expected, 2018

Nachhaltigkeit / Umwelt
-----------------------
### Wie schaffen wir die Agrarwende?
* Doku: ["Wie schaffen wir die Agrarwende?"](https://www.youtube.com/watch?v=mu4RWL2cXHg), ARTE, 2019, 50 min, TODO
    * Beschreibung
        * "Die industrielle Landwirtschaft stellt zwar unsere Ernährung sicher und hat Gemüse, Obst und Fleisch billiger gemacht. Immer deutlicher aber bekommen wir ihre negativen Folgen zu spüren. Ein Jahr lang begleitet die Dokumentation Landwirte, die Alternativen zur industriellen Landwirtschaft suchen. Und sie fragt, welche Rolle wir Verbraucher und EU-Subventionen dabei spielen."
        * "Welche Alternativen gibt es zur industriellen Landwirtschaft? Ist „Bio für alle“ möglich? Oder lässt sich auch die konventionelle Landwirtschaft in entscheidenden Punkten nachhaltiger gestalten? Große Hoffnungen setzen Wissenschaftler auch auf die nach ökologischen Prinzipien wirtschaftende neue Anbaumethode Permakultur. Kann sie einen entscheidenden Beitrag zu einer Landwirtschaft der Zukunft leisten? Ein Jahr lang begleitet die Dokumentation Landwirte in Deutschland und Frankreich, die nach Alternativen suchen."
        * "Sven Wilhelm aus dem Renchtal im Schwarzwald hat seinen Gemüseanbau auf bio umgestellt. In der Umstellungsphase braucht er einen langen Atem, um zu überleben."
        * "Michael Reber aus Schwäbisch-Hall geht einen Zwischenweg: Statt immer mehr Geld für Mineraldünger und Spritzmittel auszugeben, versucht er, mit speziellem Humusdünger die Bodenfruchtbarkeit auf seinen Äckern zu erhöhen."
        * "Und in der Normandie beschreiten Perrine und Charles Hervé-Gruyer mit der Permakultur ein Experiment, das trotz Verzichts auf Chemie die Produktivität deutlich steigert."
        * "Es zeigt sich aber auch: Ohne eine Änderung der Subventionsregeln der EU und des Kaufverhaltens der Verbraucher ist eine Agrarwende nicht zu schaffen."
    * ...
    * Bodenfruchtbarkeit wichtig für Produktivität
        * synth. Dünger und Pestizide kontraproduktiv
    * ...
    * 44:20: "Wir könnten ohne mehr zu zahlen alle von Bio ernähren. Wir müssten nur weniger wegschmeißen"
        * Unsere Essgewohnheiten verhindern derzeit auch die Agrarwende
        * Schöne Darstellung, für was welche Agrarfläche verwendet wird
            * "Weltweit steht pro Einwohner für die Prod. von Lebensmitteln eine Fläche von einem Fußballfeld zur Verfügung"
            * 6 % für Obst und Gemüse
            * 66 % für Tierhaltung
            * 28 % für Getreide
                * davon 40 % für Viehfutter
            * => hoher Fleisch-Flächenverbrauch geht auf Kosten von Obst, Gemüse und Getreide, ::flächenfraß
            * Nötig ist eine Fleischkonsumreduktion von 60 %
    * ...

### Ruinöse Viehpreise
* https://www.spiegel.de/wirtschaft/unternehmen/deutschland-kaelber-kosten-inzwischen-unter-neun-euro-a-1295665.html

### Veränderungsprozesse einleiten
* http://www.senckenberg.de/root/index.php?page_id=19005&preview=true
    * https://de.wikipedia.org/wiki/Racing_Extinction
        * https://www.youtube.com/watch?v=MwxyrLUdcss
    * Vortrag: "Warum handeln wir nicht, obwohl wir so viel wissen?"
        * "Die Trägheit unserer mentalen Infrastruktur und die Verlockung, weiter in der Komfortzone zu bleiben, machen das vorhandene Wissen praktisch irrelevant."
        * "Wie lassen sich die dringend notwendigen gesellschaftlichen Veränderungsprozesse einleiten und befördern?"

### Auswirkungen auf Kinder
* https://www.heise.de/tp/features/Kinder-unter-Stress-4301494.html, 2019
    * "Kids in den Ballungsräumen kämpfen zunehmend mit chronischen Erkrankungen - und das liegt nicht nur an der Stadtluft"
    * "Mehr und mehr fehlen Schonräume in den Städten, wo Spielplätze lukrativen Wohnprojekten zum Opfer fallen."

### Philosophie
* ["Eine überschätzte Spezies | ARTE"](https://www.youtube.com/watch?v=nLEtkmVUMfU), arte, 2019, 30 min
    * "Die Menschheit ist nur ein verschwindend geringer Teil des Universums und bei weitem nicht so einzigartig wie wir denken. Von den unendlichen Weiten des Universums bis zu mikroskopisch kleinen Einheiten, von den Ökosystemen der Erde zum menschlichen Körper, macht jede Episode dieser Animationsserie Dinge begreiflich, die unsere Vorstellungskraft sprengen. Denn der Mensch ist nicht der Nabel der Welt."
    * ...
    * "dass der Mensch ein Wettkämpfer ist. Geboren, um sich selbst und andere zu übertreffen"
    * ...
    * Mikroorganismen im Körper / Darm
    * ...
    * "Er meint alles zu beherrschen und beherrscht nicht einmal sich selbst"
    * ...

Free Software / Datenschutz / Kontrolle
---------------------------------------
### Für Einsteiger
* Beitrag: ["Angeklickt: Steuerfinanzierte Software – sollten Behörden-Programme uns allen gehören?"](https://www1.wdr.de/mediathek/video/sendungen/aktuelle-stunde/video-angeklickt-steuerfinanzierte-software--sollten-behoerden-programme-uns-allen-gehoeren-100.html), WDR, 2019
    * "Software, die für oder in Behörden, Universitäten und öffentlichen Einrichtungen entwickelt wird, wird von Steuergeldern bezahlt. Nur dass die, die Steuern zahlen, nichts von der Software haben. Die Initiative "Freier Code für freie Bürger" will das ändern und fordert: Diese Art von Software soll generell OpenSource sein."
* Doku: ["Software-Rebellen: Die Macht des Teilens"](https://www.youtube.com/watch?v=BxJyDkIqv5Q), arte, Dokumentation von Philippe Borrel, F 2017, 56 Min
    * "wirft einen resolut optimistischen Blick auf die aktuellen Entwicklungen und zeigt konkrete Anwendungsfälle von Open-Source-Lösungen in Medizin, Landwirtschaft, Bildung und Industrie. Anonyme Entwickler und bekannte Persönlichkeiten aus Indien, den USA und Europa erklären, wie sie mit freier Software die virtuelle und auch die reale Welt zum Positiven verändern."
    * "Ziel der Anhänger der freien Software ist es, das gesamte Wissen der Menschheit freizugeben. Warum soll eine Minderheit zu ihrem alleinigen Vorteil über den Inhalt der Programmcodes entscheiden?"
        * (so einfach ist es leider nicht, weil auch die Frage beantwortet werden muss, wie damit Geld zu verdienen ist; aber eben nicht primär; gefährlich sind große proprietäre Plattformen, weniger einzelne kleine proprietäre Programme; möglicherweise ist das Denken im aktuellen Wirtschaftssystem nicht ganz kompatibel zu den ethischen Grundwerten; vor allem: in der Praxis führen proprietäre Komponenten in einem Softwaresystem meistens in eine unerwünschte Sackgasse, wenn es darum gehen soll das meiste für den User herauszuholen; das heißt freie SW ist die Basis, um das herum sich die Wirtschaft entwickelt sollte)
    * Karen Sandler, Anwältin, Freedom Software Conservancy
        * https://en.wikipedia.org/wiki/Karen_Sandler
    * Richard Stallman
        * Software hat ähnliche Macht wie Gesetze
        * weigert sich eine elektronischen Schlüssel zu verwenden, weil das dem System seine Identität mitteilt. Daher hat er einen normalen Schlüssel für sein Büro
        * Freiheit und Gemeinschaft der Nutzer soll respektiert werden
            * Gegenteil: Nutzer werden isoliert und hilflos zurückgelassen
            * => Entwickler hat Machtposition gegenüber dem Nutzer. Das ist eine Ungerechtigkeit
            * Wer als Nutzer Freiheit erlangen will, der weigert sich proprietäre Software zu nutzen
    * James Boyle, Jura-Professor
    * Herve le Crosnier, Experte für Digitale Technologien
    * Richard Stallman Vortrag auf Französisch
        * hat kein Mobil-Telefon
        * Thema Menschenrechte
    * Pierre-Yves Gosset, Direktor Framasoft
        * "Proprietäre Software = Fertiggericht / Freie Software = Möglichkeit selber zu kochen"
            * Gewinn-orientierte Unternehmen möchten natürlich, dass wir Fertiggerichte kaufen
        * "Freie Software = Open Source + ethische und soziale Werte"
    * Beispiel Nante: in dieser Schule wird mit freier SW gearbeitet
        * "Bienvenue a l'atelier partage - Install'party Linux"
            * Thomas Bernadi, PING, Verein für allgemeine EDV-Bildung
            * Reparieren = Verstehen
                * (--> RepairCafe)
    * "Wissenskapitalismus"
    * Ecole 42
        * Aufgabe: Teile der C-Library neu schreiben: https://github.com/fwuensche/libft
            * https://cdn.intra.42.fr/pdf/pdf/775/libft.en.pdf
        * (überall steht Apple rum)
        * https://en.wikipedia.org/wiki/42_(school)
    * Mozilla Foundation
        * Amerika
        * Indien, Bangalore
            * hat einen Day-Job und abends arbeitet er für Mozilla. So kann er ruhig schlafen, weil er seinen Beitrag leistet
    * Lionel Maurel, La Quadrature du Net
        * OpenStreetMap
        * Open Hardware
    * Handprotese
        * Verein My Human Kit
    * Scality
        * "Nicht der Quellcode, sondern die Nutzergemeinde macht den eigentlich Wert der freien Software aus"
    * Open License
        * freie Software in proprietäre einbetten, ohne weitere Folgen
    * David Bollier, Commons Strategist Group
        * Kapital will sich Gemeingut aneignen
        * Instrument: geistiges Eigentum
            * alleinige Kontrolle über Gene, Saatgut, Kreativität und Informationen zu erlangen
    * "Mit diesen neuen Formen der Wissensprivatisierung wird auf allen Gebieten experimentiert, um die Grenzen der proprietären Logik immer weiter zu verschieben"
    * Marc Oshima, Areofarms
        * Landwirtschaft
        * Regalanbau, um verseuchten Boden nicht nutzen zu müssen
        * Nährlösung und Anbauverfahren patentiert bzw. Betriebsgeheimnis
            * (vs. kleinbäuerlicher Ansatz, in dem jeder Mensch ermächtigt ist, selber seine Nahrung anzubauen)
        * Kapitalgeber wie Goldman Sachs
        * wollen disruptiv sein
            * (Wachstum verlangt immer mehr Disruption)
    * Gegenpunkt: Kleinbauern in Indien
        * Ziel: freien Zugang zu Saatgut erhalten
        * https://www.evangelische-akademie.de/extras/download/__/file/3447/

            Die „Sieben Todsünden der modernen Welt“ nach Mahatma Gandhi
            Pleasure without conscience     -       Genuss ohne Gewissen
            Knowledge without character     -       Wissen ohne Charakter
            Politics without principle      -       Politik ohne Prinzipien
            Commerce without morality       -       Geschäft ohne Moral
            Wealth without work             -       Reichtum ohne Arbeit
            Science without humanity        -       Wissenschaft ohne Menschlichkeit
            Worship without sacrifice       -       Religion ohne Opferbereitschaft
    * Joseph E. Stiglitz, Nobelpreis Wirtschaftswissenschaften 2001
        * ...
    * Genf, Sitz der Weltorganisation für Geistes Eigentum, WIPO
        * James Love, Direktor Knowlegde Ecology International
        * Direktor: Innovation findet zumehmend im Wissensbereich statt, Innovation ist Wettbewerbsvorteil, Innovation braucht Grundlage für Investitionen, dafür braucht es die wirtschaftlichen Rahmen
            * (dass dabei ein Großteil der Menschen die Kontrolle über Gemeingut verliert, wird nicht gesagt)
        * Stallmann
            * "Geistiges Eigenum: konfuse Verallgemeinerung von Dingen, die nicht vergleichbar sind.", Geschäftsgeheimnisse sind in Ordnung
                * keine Patente im Bereich Landwirtschaft, Medizin und Software
                * Medizin: Blockade der Generika-Produktion => Medikamente für viele Menschen teurer als nötig
    * 36:30
    * Diabetiker
        * wünscht sich mehr Kontrolle über die Technologie, die ihn am Leben hält
        * Open Insulin, openinsulin.org
            * nur 3 große Hersteller, Preise sind stark gestiegen
            * Reinigung kompliziert; die einfachen Verfahren sind patentiert
            * Suche nach Alternativen
    * USA, Landwirt mit Mähdrescher
        * Software (Mähdrescher) Saatgut unter Kontrolle von Großkonzernen
        * Fernwartung möglich, aber separater Vertrag mit Rechnung; eigene Reparatur nicht erlaubt
        * Kevin Kenney, Bewegung "Recht auf Reparatur"
        * Recht auf die Reparatur der eigenen Maschinen soll Gesetz werden
        * Abgeordnete sagt zu Patenten auf geistiges Eigentum: "mit einem Fuß auf der Bremse wird es keine Innovation geben"
            * weil ja keiner weiß, was die Zukunft bringt
    * Bangalore
        * durch die Strategie der Standortverlagerungen und 10mal günstigeren Gehältern ist die Stadt sehr schnell gewachsen
        * 12 Mio Einwohner, 1/3 lebt trotzdem noch in Slums
        * Bildungszentrum von und mit freier Software, Gemeinschaftliches EDV-Zentrum Ambedkar (AC3)
            * "freie SW als allgemein zugängliches Emanzipationsinstrument"
            * LibreOffice, TuxPaint
            * hier wird alles mögliche erklärt, was in den staatlichen Schulen nicht gelehrt wird
    * Zeiten des Klimawandels: Lösungen werden sich nicht im Wachstum finden lassen; siehe regionale Ansätze
    * Atelier Paysan
        * OpenSource-Geräte für die Landwirtschaft
        * Gemüse-Bäuerin
        * "frei heißt nicht gratis; man braucht Geld um etwas zu bewegen"
    * "Die Zivilgesellschaft ist nicht nur eine Konsumgesellschaft, sondern kann auch produktiv sein"
        * Freie Software ist ein Beispiel dafür, dass Menschen aus der Zivilgesellschaft sich organisieren können, um hochwertige Produkte zu schaffen
    * 2018?! Förderungsgebiet Notre-Dame-des-Landes
        * ...

* ["Linux statt Windows: Einfach ausprobieren und bei Gefallen wechseln!"](https://www.heise.de/newsticker/meldung/Linux-statt-Windows-Einfach-ausprobieren-und-bei-Gefallen-wechseln-4399670.html)

### Mobilfunk: wetell, 5G-Kritik
* https://wetell-change.de/
    * https://www.startnext.com/wetell
        * https://www.diagnose-funk.org/themen/mobilfunk-versorgung/5g
    * https://wetell-change.de/2019/03/26/5g/
        * "Es geht um die Frage: Warum brauchen wir heute schon drei Mobilfunknetze? Und in Zukunft vielleicht sogar vier? Ist nicht der Ausbau und die gemeinsame Nutzung von einem einzigen Mobilfunknetz, genannt „national Roaming“, viel effizienter?"

### Public money, public code
* [" MAlt project - Taking back control using open software "](https://home.cern/news/news/computing/malt-project)
    * https://www.heise.de/newsticker/meldung/CERN-wechselt-von-Microsoft-zu-Open-Source-Software-4447421.html
* ["Expert Brochure to modernise public digital infrastructure with public code"](https://fsfe.org/campaigns/publiccode/brochure.html)
* München / Limux
    * https://www.br.de/radio/bayern2/sendungen/zuendfunk/fail-muenchen-und-microsoft-ein-schwerer-ausnahmefehler-100.html
    * https://www.deutschlandfunkkultur.de/christian-ude-ueber-it-sicherheit-und-den-einfluss-von.1008.de.html?dram:article_id=412109
* Klage gegen Zwangsmigration: https://www.heise.de/newsticker/meldung/Windows-Zwangsmigration-Strafanzeige-gegen-niedersaechsische-Landesregierung-4309953.html, 2019

### Bildung: Programmieren vs. kritisches Denken
* ["Digitalismuskritik - Fauler Zauber"](http://taz.de/Digitalismuskritik/!168081/)
    * "Apple Education twitterte im Frühjahr 2018 etwa Folgendes: »Wenn Sie Programmieren unterrichten, bringen Sie Ihren Schülern gleichzeitig kritisches Denken und das Lösen von Problemen bei.« Dabei wird stures Auswendiglernen, das Routinelernen, mit kreativem, individuellem Denken verwechselt, ein Klassiker, der aber im Zeitalter der Wissensgesellschaft gefährlich ist, die nicht nach der Logik der Industriegesellschaft läuft. Coden hilft beim kritischen Zweifeln und Finden origineller Lösungen ungefähr so gut wie Rosenkranzbeten gegen Rückenbeschwerden."
    * "Joseph Weizenbaum, das kritische Gewissen der Informatik, hat das 1972 vorhergesehen: »Der meiste Schaden, den der Computer potenziell zur Folge haben könnte, hängt weniger davon ab, was der Computer tatsächlich kann oder nicht kann, als vielmehr von den Eigenschaften, die das Publikum dem Computer zuschreibt.«"

### Reparieren, Second-Hand, Elektronik-Schrott, Nachhaltigkeit
* https://www.backmarket.de
    * insbesondere Smartphones
* https://faircomputer.ch
    * "Reduce E-Waste! Take a Faircomputer. - Ein Occasions-Laptop von Faircomputer hilft die Lebenszeit eines Laptops zu verlängern. Damit setzt Du ein Statement für die nachhaltige Nutzung von elektronischen Geräten."

### Privatsphären-gefährdende Unternehmen
* ["DSGVO: Datenschützer untersucht EU-Verträge mit Microsoft"](https://www.heise.de/newsticker/meldung/DSGVO-Datenschuetzer-untersucht-EU-Vertraege-mit-Microsoft-4367881.html), 2019
* ["Aral Balkan: I was wrong about Google and Facebook: there’s nothing wrong with them (so say we all)"](https://ar.al/2019/01/11/i-was-wrong-about-google-and-facebook-theres-nothing-wrong-with-them-so-say-we-all/), 2019
    * eine Reihe von Beispielen inkl. Apple

### Geschäftsmodelle
* https://media.ccc.de/v/froscon2018-2219-open_source_darf_auch_was_kosten
* https://fsfw-dresden.de/2018/08/funding-floss.html

### NextCloud
* NextCloud perspective for the new year: https://karlitschek.de/2019/01/2018-and-2019/

### Digitaler Konsum
* ["Why our screens make us less happy | Adam Alter"](https://www.youtube.com/watch?v=0K5OO2ybueM), 10 min
    * auch Steve Jobs empfahl, Kindern möglichst spät ein Pad zu geben
    * stopping cues

Wirtschaft
----------
### Die Große Geldflut
* ["Die Große Geldflut: Wie Reiche immer reicher werden"](https://www.youtube.com/watch?v=5e4qAergE3Q), SWR-Doku, 2018
    * "Seit Jahren drücken die Notenbanken dieser Welt die Zinsen, um verschuldete Staaten zu stabilisieren, Banken zu retten und Wachstum anzukurbeln. Eine noch nie dagewesene Geldflut führt zu neuen Blasen."
    * "Die Geldvermehrung kann nicht ewig weitergehen"
    * ... TODO

### Paketdienste
* "Systematischer Irrsinn - Die Paketsklaven" - https://www.youtube.com/watch?v=O9OWVsuG_Xk - 2012
    * ... todo

Gemeinnützigkeit
----------------
* https://www.hephata.de

Gerechtigkeit
------------
### Ungerechtigkeit fördert Kriminalität
* https://www.fhoev.nrw.de/fileadmin/user_upload/Pfeiffer_Farren_2018.pdf
    * "Ungerechtigkeit fördert Kriminalität. Fairness lohnt sich."
    * ...

### Weniger Ungerechtigkeit
* "Weniger Ungerechtigkeit!“ statt „Was ist Gerechtigkeit?“ –Mit Amartya Senzu einer praxisnahen Gerechtigkeitstheorie."
    * https://edoc.ub.uni-muenchen.de/17007/1/Wagner_Michael.pdf

Lärm
----
### Ist es zu laut bei uns? Der Lärm und seine Folgen
* MDR-Doku: https://www.mdr.de/wissen/dokumentationen/video-368384_zc-99e06b6a_zs-8a16bf16.html "Ist es zu laut bei uns? Der Lärm und seine Folgen", 45 min,
    * "Lärm ist nicht nur lästig und laut, sondern zunehmend auch ein Krankheitsmacher. Studien in Mitteldeutschland belegen das immer wieder. Doch was bedeutet das im Einzelfall. Die Doku begibt sich auf Spurensuche."
    * Studie in Jena
    * Hauptsache LKWs als Ursache
    * ...
* Weitere Dokus: https://www.mdr.de/wissen/dokumentationen/index.html
