unsortiert
==========

Unsortierte Informationen und Diskussionsanregungen zu alternativen, gegenwarts- und zukunftsgerichteten Mindsets aus heutiger Zeit und der Geschichte.

<!-- toc -->

- [Politik](#politik)
- [EDV](#edv)
  * [Kommunikation](#kommunikation)
  * [Betriebssysteme](#betriebssysteme)
  * [Software](#software)
  * [Wahlcomputer](#wahlcomputer)
- [Landwirtschaft / Ernährung](#landwirtschaft--ernahrung)
  * [Supermarkt-Macht](#supermarkt-macht)
  * [Weiteres](#weiteres)
- [Gesellschaft](#gesellschaft)
  * [Chauvinismus](#chauvinismus)
  * [Lichtverschmutzung](#lichtverschmutzung)

<!-- tocstop -->

Politik
-------
Warum sind Geschäfte mit Waffen zu vermeiden? --> MOVED
Warum ist die Weiterentwicklung von modernem Kriegsgerät zu vermeiden? --> MOVED
Kriegseinsätze sollten nur aus humanitären Gründen erfolgen und ohne wirtschaftliche Interessen geleitet sein

EDV
---
### Kommunikation

Beispiele von Unternehmen, die nicht-werbefinanzierte Dienste für Privatkunden anbieten:

* https://posteo.de
* https://mailbox.org/

Hintergrundinfos:

* [Posteo, Mailbox.org, Tutanota, and ProtonMail compared, 2015](http://www.admin-magazine.com/Archive/2015/26/Posteo-Mailbox.org-Tutanota-and-ProtonMail-compared)
* [TKÜV ab 10.000 Teilnehmern, seit ca. 2002](https://de.wikipedia.org/wiki/Telekommunikations-%C3%9Cberwachungsverordnung)
* [Untangling the SaaSS Issue from the Proprietary Software Issue](http://www.gnu.org/philosophy/who-does-that-server-really-serve.en.html)
* E-Mail ist ein offener Standard, der dezentrale Implementierungen zulässt
* [Mozilla Thunderbird](https://de.wikipedia.org/wiki/Mozilla_Thunderbird)
* [Backup](http://www.freefilesync.org/)

Diskussionspunkte:

* "Daten sind die neue Währung"
* Vor- und Nachteile von Big Data
    * ["Big Data in 3 Minuten erklärt"](https://www.youtube.com/watch?v=uH813u7_b0s), 2014, was besseres?
    * ["Big Data, Big Opportunities for Marketing"](https://www.youtube.com/watch?v=xJfP_o_fANA), IBM, 2013
    * im Gesundheitsbereich: [ted-talk](https://www.youtube.com/watch?v=0Q3sRSUYmys), 2014
* Cloud ("just other people's computers"): Komfort vs. Daten in der Cloud sind immer für Hackerangriffe verfügbar

### Betriebssysteme

Beispiele von Betriebssystemen, die nach dem Prinzip der [Freien Software](https://de.wikipedia.org/wiki/Freie_Software) funktionieren:

* [openSUSE](https://www.opensuse.org/)
* [Debian](http://www.oreilly.com/openbook/debian/book/ch01_01.html)

Hintergrundinfos:

* [Warum? (1)](http://de.wikipedia.org/wiki/George_Orwell)
* [Warum? (2)](https://www.google.de/search?q=microsoft+und+nsa&ie=utf-8&oe=utf-8&gws_rd=cr&ei=JxRCVcOOK5TiasGYgVA#q=microsoft+und+nsa)
* [Warum? (3)](http://de.wikipedia.org/wiki/1984_%28Roman%29)
* [Warum? (4)](http://de.wikipedia.org/wiki/%C3%9Cberwachungsstaat)
* https://sfconservancy.org/
* [EFF](https://de.wikipedia.org/wiki/Electronic_Frontier_Foundation)

### Software

* [LibreOffice](https://de.libreoffice.org/)

### Wahlcomputer

* [Wahlcomputer - Einführung des CCC](http://wahlcomputer.ccc.de/)
* [Comic "Der Wahlschrank"](http://wahlcomputer.ccc.de/presse/bildmaterial/der-wahlschrank/)
* [Der Wahlstift aus Hamburg](http://wahlcomputer.ccc.de/presse/bildmaterial/wahlstift/)


Landwirtschaft / Ernährung
--------------------------
### Supermarkt-Macht
* http://www.supermarktmacht.de/
    * http://www.supermarktmacht.de/marktmacht/
    * http://www.supermarktmacht.de/preiskampf/ (u. a. Qualität des Endprodukts vs. Produktionsprozess)
    * http://www.supermarktmacht.de/ausbeutung/
    * http://www.supermarktmacht.de/uber-uns/
* http://www.sagneinzumilch.de/
* Sachbuch: Karen Duve - Anständig essen
* Sachbuch_: Karen Duve - Die Ernährungsdiktatur / COPIED
* Film: [Walmart: The High Cost of Low Price](http://www.bravenewfilms.org/walmartmovie), 2005, [https://www.youtube.com/watch?v=RXmnBbUjsPs](full movie)

Was ist der Nachteil von Monopolen und Oligopolen?

* siehe [Monopole und Oligopole](../Hintergrund/oligopole.md)

* ARD-Doku "Europas dreckige Ernte" 2018:
    * "In Italien und Spanien haben wir immer wieder gehört: die deutschen Supermarktketten seien die größten Preisdrücker und Mitschuld an der Ausbeutung"

### Weiteres

Positive Beispiele:

* Pro/Contra-Bio-Lebensmittel?
    * https://de.wikipedia.org/wiki/Bio-Lebensmittel
    * (["Die Uni Kassel richtete vor 20 Jahren einen Studiengang Ökologische Landwirtschaft ein – der erste dieser Art weltweit"](http://www.fnp.de/rhein-main/Keine-Oeko-Spinner-mehr;art801,1459232)) (23.06.2015, fnp)

Gesellschaft
------------
* Filme die über Finanzkrise ab 2007: (COPIED)
    * [Inside Job](https://de.wikipedia.org/wiki/Inside_Job), 2010
    * [Der große Crash – Margin Call](https://de.wikipedia.org/wiki/Der_gro%C3%9Fe_Crash_%E2%80%93_Margin_Call), 2011
    * [The Big Short](https://de.wikipedia.org/wiki/The_Big_Short_%28Film%29), 2015
    * https://de.wikipedia.org/wiki/Finanzkrise_ab_2007#Dokumentationen_und_Spielfilme
* Film: https://de.wikipedia.org/wiki/American_History_X
* Film: [Alphabet (2013)](https://de.wikipedia.org/wiki/Alphabet_%28Film%29), u. a. "Verkürzung des Lebens auf die Ökonomie“
* Film: [Colonia Dignidad (2015)](https://de.wikipedia.org/wiki/Colonia_Dignidad_%E2%80%93_Es_gibt_kein_Zur%C3%BCck)
    * Pispers: [Bis neulich (2007), Teil 1 u 2](https://www.youtube.com/watch?v=AuUUQ0_SO00), [Teil 2 - Syrien, Afghanistan, Wandschrank](https://www.youtube.com/watch?v=vHygky3_JcI)
    * https://de.wikipedia.org/wiki/Colonia_Dignidad
    * https://de.wikipedia.org/wiki/Putsch_in_Chile_1973
    * https://de.wikipedia.org/wiki/Amnesty_International
* Buch: Daemon [Paperback] [Jan 07, 2010] Suarez, Daniel
* Buch: Freedom (TM) [Mass Market Paperback] [Jan 04, 2011] Suarez, Daniel
* Sachbuch: "Das Imperium der Schande: Der Kampf gegen Armut und Unterdrückung", von Jean Ziegler
* COPIED: Film: Die Bucht - The Cove (DVD 2009)
* Buch: Die Letzten ihrer Art [Paperback] [Nov 01, 1992] Adams, Douglas and Carwardine, Mark

### Chauvinismus
* Überblick: https://de.wikipedia.org/wiki/Chauvinismus
* Greifbare Beispiel, Video: http://www.ardmediathek.de/tv/Zapp/Chauvinismus-Erfahrungen-aus-Redaktione/NDR-Fernsehen/Video?bcastId=3714742&documentId=47311014, NDR, 2017, 5 min

### Lichtverschmutzung
* todo: Berichte von 2017 und 2018
