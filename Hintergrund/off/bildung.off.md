bildung / zurückgestellt
========================

...seriös? ...Randmeinungen...

### (Digitalisierung, 2019)
* "Wie die Digitalisierung unsere Kinder verblödet - Psychiater spricht Klartext! (Michael Winterhoff)" - https://www.youtube.com/watch?v=zzLM3CrfYm0, 90 min
    * Achtung, Polemik, siehe https://de.wikipedia.org/wiki/Michael_Winterhoff#Rezeption
        * nicht empfehlenswert
    * ...
    * ... ist das was?
    * Weisheit
    * Psyche
    * Zeit für sich selber haben
    * ...
    * Sozialstaat: der Stärkere ist für den Schwächeren mitverantwortlich
    * ...
    * Polemik gegen freiheitliches Lernen und Baselräume
    * ...
    * yt comments
        * "Kinder brauchen Beziehung und nicht unsinnige Do-it-yourself- Arbeitspläne"
    * https://rpp-institut.org/ueber-rpp/
        * Personen
            * Raphael M. Bonelli
            * Michael Winterhoff
            * Manfred Spitzer
            * https://de.wikipedia.org/wiki/Institut_f%C3%BCr_Religiosit%C3%A4t_in_Psychiatrie_und_Psychotherapie
                * ...
        * Video_: "Von der digitalen Demenz zur Smartphone-Pandemie (Manfred Spitzer)"
            * ...
            * disruptiv
                * Sozialdarwinismus
                * neue Technologie einführen
                * "mach mit oder du bist weg"
            * ...
            * https://de.wikipedia.org/wiki/Manfred_Spitzer
                * Kritik: "Der Neurologe Hans-Peter Thier bezweifelt, dass es den Sachverhalt „digitale Demenz“ überhaupt gebe"

    * https://de.wikipedia.org/wiki/Michael_Winterhoff
        * "deutscher Kinder- und Jugendpsychiater, Psychotherapeut und Autor. Seine Werke erzielen hohe Auflagen und stoßen auf entsprechend große mediale Resonanz,
        in der Fachwelt jedoch auf verbreitete Kritik"
            * "demagogischen Duktus des Erstlings Winterhoffs Warum unsere Kinder Tyrannen werden"
        * https://de.wikipedia.org/wiki/J%C3%BCrgen_Kaube
