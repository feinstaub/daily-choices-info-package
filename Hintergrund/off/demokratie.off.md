...Randmeinungen...

### Probleme mit repräsentativer Demokratie
* Symptome
    * Viele sind unzufrieden mit dem, was "die da oben" entscheiden.
    * Man hat das Gefühl, auch die "eigene" Partei macht nicht das, was richtig wäre.
    * Auf oberster Ebene regiert Geld und Geld-Lobby.
    * Im lokalen Engagement hat man es mit Vetternwirtschaft und sonstigem Mist zu tun.
        * => vernünftige Leute werden abgeschreckt; übrig bleibt der Rest.
    * Die wollen uns doch nur abzocken (immer nur neue Steuern, neue Abzocke)
* Frage:
    * Muss das so sein? Geht es auch besser?
* Mögliche Antworten (aus Sicht UK)
    * "Our Spectator Democracy Costs the Earth | Dr Gail Bradbrook | Extinction Rebellion" - https://www.youtube.com/watch?v=3XdWlet6hB4, 30 min, 2019, Russia Today
        * Statements, warum repräsentative Demokratie nicht funktioniere und nie funktioniert habe
        * Probleme mit repräsentativer Demokratie: Verbindung zur Basis fehle
        * ... ... ...
        * What to say to pragmatic conservatives with "getting things done" mentality (vs. love and harmony and ethics)
            * IPCC climate change is on the run
            * Milton Friedman - https://www.forbes.com/sites/stevedenning/2013/08/01/milton-friedman-and-the-fallacy-of-good-intentions/#1950d80152c6
                * “Concentrated power is not rendered harmless by the good intentions of those who create it.”
                    * "Friedman was referring, of course, to government, but his quote has turned out to be prophetic critique of his own legacy in the private sector"
                * "has led to concentrated power in the hands of the few, along with large harm for many"
                    * "a supposed goal of maximizing of shareholder value which has in practice undermined long-term shareholder value"
                    * "the creation of “bad profits” that have destroyed many individual firms"
                        * https://www.forbes.com/sites/stevedenning/2013/07/22/how-modern-economics-is-built-on-the-worlds-dumbest-idea/#211464187e6f
                    * "a highly concentrated banking sector that is “too big to fail” (or to jail) despite rampant illegality and continuance of the very practices that caused the financial meltdown in 2008"
                    * "large-scale off-shoring of manufacturing that has destroyed whole sectors of the U.S. economy"
                        * (lange Lieferketten vs. Verantwortung)
            * Milton Friedman: if power is distributed via free market, no one has superiour power and no harm will be done
        * Parlamente
            * Es finden Diskussionen statt, aber man kann mit den Diskutanten z. B. keine Graphiken teilen, um Dinge zu visualisieren
                * siehe auch lokale Sitzungen, wo die Leute in Anzügen an runden Tischen sitzen
            * andererseits: vielleicht kann man sich so besser konzentrieren?
        * Twitter user feedback
            * Hongkong vs. Gilet Jaunes in Frankreich
                * https://de.wikipedia.org/wiki/Gelbwestenbewegung
                    * seit Ende 2018, wie geht es weiter?
        * democracy? -> "the vast majority of people are not engaged at all; just simply not involved"
            * everyone should be feeded in to make decisions (-> Bürgerbeteiligung?)
            * Book: Flatpack Democracy 2.0
        * Mehr von Spiritualität, Trauer etc. benötigt
            * sieht man kaum (noch?)
            * Gegen-Beispiel Beerdigung, wo die Menschen sich hinterher befreit und besser fühlen)
            * problem with patriarchic thinking
            * Fakten vs. emotionale Teilnahme
            * Kultur
                * bei uns derzeit: viele narzistische Elemente im Konsumerismus (ich und meine Wünsche, jetzt) ::narzismus, ::konsum
                * woanders:
                    * Sinn für Ahnen/Vorfahren und Sinn für die nächsten 7 Generationen
                    * man ist im Fluss des Lebens; nur eine Weile hier; wenn man weg ist, wird man geehrt für das, was man getan hat
                        * (sieht man viel in Religionen)
                    * man sieht zu, was man für seine Kinder tun kann; das hätten wir nicht in unserer Kultur
                    * Teil es Prozesses ist über Tod und Sterben nachzudenken
                        * kann sehr befreiend sein sich vorzustellen bald zu sterben; weil es darum geht, was man jetzt tut
                            * im Ggs. zu so lang leben wie möglich und mit möglichst viel Stuff
        * need to stop the harm and repairing it

