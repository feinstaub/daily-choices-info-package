Zielgruppen, Haltungen, Bewusstsein
===================================

Annahme: gemeinsames Ziel sind gemeinwohlorientierte Handlungsweisen.

<!-- toc -->

- [Umweltschützer / Umweltverbände / (Tierschützer)](#umweltschutzer--umweltverbande--tierschutzer)
- [Tierheime / Haustiere](#tierheime--haustiere)
- [Handelsketten](#handelsketten)
- [Bildende](#bildende)
- [Öffentlich finanzierte Schulen](#offentlich-finanzierte-schulen)
- [Bürger](#burger)
  * [Allgemeines Bewusstsein](#allgemeines-bewusstsein)
  * [Sicherheitsbewusssein](#sicherheitsbewusssein)
  * [Natur-erhaltendes Bewusstsein](#natur-erhaltendes-bewusstsein)
- [Politik](#politik)
  * [Allgemein](#allgemein)
  * [Grün](#grun)
  * [Konservativ / Werte](#konservativ--werte)
- [Stützen der Gesellschaft](#stutzen-der-gesellschaft)
- [Kantine / Mensa](#kantine--mensa)
- [Spiritualität](#spiritualitat)

<!-- tocstop -->

Umweltschützer / Umweltverbände / (Tierschützer)
------------------------------------------------
* siehe umwelt-und-naturschutz.md

Tierheime / Haustiere
---------------------
Fragen:

* Kann man auch ein Schwein als Haustier haben?
* Warum (mit welcher Begründung) darf man hier Hundefleisch nicht essen, obwohl das in anderen Ländern kein Problem ist?
    * https://de.wikipedia.org/wiki/Hundefleisch
        * Bild: "Hundefleisch-Gericht, Guilin, China mit einem Hundeschwanz garniert."
        * "für Deutschland seit Mai 2010 aus § 22 Abs. 1a der Tierische Lebensmittel-Hygieneverordnung (Tier-LMHV).[21] Experten halten zumindest die Strafbewehrung dieses Verbots aber aus verfassungsrechtlichen Gründen für bedenklich."
            * http://www.zis-online.com/dat/artikel/2016_2_984.pdf
                * "Sind das Inverkehrbringen von Hyänensteaks, die Labradorentrecôtegewinnung zum Zwecke des Verzehrs in mittelbarer Täterschaft und die Anstiftung zum versuchten Verkauf gedünsteter Pavianohren nach deutschem Recht strafbar? Ja, dem ist tatsächlich so."
                * ...
                * "Es ist offenkundig, dass solcherlei Strafgesetzgebung in Konflikt mit dem Bestimmtheitsgrundsatz nach Art. 103 Abs. 2 GG geraten kann."
                * ...
        * Olympischen Sommerspiele in Seoul: "Auch FIFA-Präsident Sepp Blatter unterstützte ein Schlachtverbot von Hunden. Die Reaktionen auf koreanischer Seite waren diesmal allerdings weniger nachgiebig: Bemühungen um eine Legalisierung – auch mit dem Argument, dass dies der beste Weg zur Bekämpfung grausamer Schlachtmethoden sei – wurden verstärkt, und einige Nationalisten empörten sich über die Arroganz des Westens. Die Kontroverse zwischen Befürwortern der Legalisierung und Gruppen, die ein vollständiges Verbot anstreben, dauert derzeit in Südkorea noch an."
    * https://de.wikipedia.org/wiki/Katzenfleisch
* Wie unterscheiden sich Schwein und Hund?
* Ist es überhaupt machbar, ein Fest ohne Tierprodukte ausrichten, und dass trotzdem alle satt werden?
* siehe auch rc

Handelsketten
-------------
* ...

Bildende
--------
* Konzernunabhängige Computerbildung ohne Werbung:
    * siehe Freie Software
* Wo kommt unser Essen her / Landwirtschaft:
    * siehe Bio
    * siehe vegan
* Umweltpädagogik:
    * siehe Umweltschützer

Öffentlich finanzierte Schulen
------------------------------
* günstige Rechner- und Softwareausstattung für den Unterricht:
    * siehe Freie Software
* Werbefreiheit:
    * siehe Freie Software
* Kosten-Reduktion:
    * längere Verwendung älterer Hardware: siehe Freie Software
    * Sparen von Softwarelizenzkosten: siehe Freie Software
    * beim Schulessen, bei gleichzeitiger Gesundheitsförderung: siehe vegan
* Gesundheit beim Schulessen:
    * siehe Bio
    * siehe vegan

Bürger
------
### Allgemeines Bewusstsein
* Video: ["Die Tricks der Fleischpanscher - Wie aus Wasser und Abfall Wurst wird"](https://www.zdf.de/dokumentation/zdfinfo-doku/die-fleischpanscher-wie-aus-wasser-wurst-wird-100.html), ZDF, 2018, 30 min
    * "Industriell hergestellte Fleisch- und Wurstwaren können mit Proteinen aus Schlachtabfällen gepanscht, gestreckt und gefärbt werden, ohne dass dies bei Lebensmittelkontrollen auffällt."
    * "die Deutsche Landwirtschafts-Gesellschaft (DLG) eine gepanschte Wurst aus solchen Proteinen und Fleischabfällen mit dem silbernen DLG-Preis prämiert"
    * Separatorenfleisch, 3mm-Fleisch
        * https://www.lgl.bayern.de/lebensmittel/warengruppen/wc_07_fleischerzeugnisse/et_separatorenfleisch.htm
* ["Ställe werden zu selten kontrolliert"](https://albert-schweitzer-stiftung.de/aktuell/staelle-zu-selten-kontrolliert), 2018

### Sicherheitsbewusssein
* Sicherheit und Privatsphäre wichtig:
    * siehe Freie Software im Haushalt und auf dem Smartphone
    * siehe [Warum Privatsphäre wichtig ist](../Hintergrund/privatsphaere.md)

### Natur-erhaltendes Bewusstsein
* Haltung
    * "Die meisten Umweltfragen werden von der Regierung adäquat behandelt."
    * "Wir Bürger sollten uns nicht zuviele Sorgen machen."
    * "Zuviel Umweltschutz schadet der Wirtschaft und ist nicht sozialverträglich. Von daher gute Balance aktuell."
    * "Es gibt eine Vielzahl von Problemen. Umwelt ist nur eins davon und sollte nicht überbewertet werden."
    * "In der Summe geht es bei uns sehr gerecht zu."
    * "Industrielobby? Die Umweltlobby ist doch genauso schlimm und setzt immer wieder unsinnig hohe Grenzwerte durch."
    * "Unsere Technik ist in der Summe gut für alle Menschen."
    * "Großkonzerne sind wichtiger Teil unserer Wirtschaft"
    * "Nichts überstürzen; wenn wir zu schnell eine bestimmte Richtung einschlagen, könnte sich das später als falsch erweisen"
    * siehe denkweise.md
* Annahmen
    * Ausgewogene Umweltthemen sind im Gegensatz zu medialen Überskandalisierung einfach weniger präsent
    * Wissenschaftlicher Sachstand nicht bekannt oder nicht verstanden
* FRAGEN dazu
    * Welche aktuellen Umweltprobleme sind dir präsent?
    * Welche Umweltschutzorganisationen kennst du?
        * Welche Interessen haben sie (z. B. Geld und die Bevölkerung schikanieren)?
    * Welche zivilgesellschaftlichen Organisationen kennst du?
        * Ist es gut sich für das Allgemeinwohl zu engagieren?
    * Wie stark sollte sich die Gesellschaft um Schwächere kümmern (Solidarprinzip)?
    * Was ist die zunehmende Spaltung von arm und reich zu sehen?
    * Wie tragen Großkonzerne, deren Ziel Gewinnmaximierung und Shareholdervalue sind, zum Allgemeinwohl bei?
    * Glaubst du an den Trickle-Down-Effekt?
        * https://de.wikipedia.org/wiki/Trickle-down-Theorie
            * "bezeichnet die These, dass Wirtschaftswachstum und allgemeiner Wohlstand der Reichen nach und nach durch deren Konsum und Investitionen in die unteren Schichten der Gesellschaft durchsickern würden"
            * "John Kenneth Galbraith wies darauf hin, dass man die Trickle-down-Theorie zu seiner Jugendzeit als horse and sparrow theory bezeichnete: „Wenn man einem Pferd genug Hafer gibt, wird auch etwas auf die Straße durchkommen, um die Spatzen zu füttern“,[4] woher im Deutschen auch die Bezeichnung Pferdeäpfel-Theorie rührt. Seit etwa 1980 fand sie insbesondere unter Neoliberalen wieder viele Anhänger und prägte die Steuerpolitik von Regierungschefs und Staatsoberhäuptern wie Ronald Reagan, Margaret Thatcher oder jüngst Donald J. Trump."
            * "David Stockman, Ronald Reagans Chefberater in Wirtschaftsfragen, geprägt. Er sah die angebotsorientierte Wirtschaftspolitik als Teil einer langen Tradition der Ökonomie, wonach das Laissez-faire nicht nur jenen helfe, die gut im Markt platziert sind, sondern allen, auch den Ärmsten."
            * "Namhafte Wirtschaftswissenschaftler bestreiten die Gültigkeit der Theorie. Paul Krugman äußerte 2008: „Wir warten auf diesen Trickle-down-Effekt nun seit 30 Jahren – vergeblich.“[6] Ähnlich bezweifelte Joseph Stiglitz 2012, dass „[…] an der sogenannten Trickle-down-Theorie […] auch nur ein Quäntchen Wahrheit“ wäre."
            * "2013 stellte Papst Franziskus in einem apostolischen Schreiben fest, dass die Trickle-down-Theorie ein „undifferenziertes, naives Vertrauen auf die Güte derer aus[drückt], die die wirtschaftliche Macht in Händen halten, wie auch auf die sakralisierten Mechanismen des herrschenden Wirtschaftssystems.“ (Evangelii Gaudium, Nr. 54)[10]"
    * "undifferenziertes, naives Vertrauen"...
        * Was sagt die Geschichte, wie Menschen und Organisationen, die zu großer Macht gekommen sind, damit umgegangen sind?
    * Welche Probleme existieren in der aktuellen Art der Landwirtschaft / Mobilität / Konsum?
        * Welche Maßnahmen werden ergriffen?
        * (Was ist z. B. mit Tierwohl und Hochleistungszüchtungen der konv. Landwirtschaft?)
        * Was gegen den Klimawandel tun?
            * ("5 untrügliche Anzeichen dafür, dass jemand vom Klimawandel keine Ahnung hat und unsere Zeit verschwendet – Teil 3: Immer mit der Ruhe, lass erst mal die Gebäude dämmen" - https://graslutscher.de/5-untruegliche-anzeichen-dafuer-dass-jemand-vom-klimawandel-keine-ahnung-hat-und-unsere-zeit-verschwendet-teil-3-immer-mit-der-ruhe-lass-erst-mal-die-gebaeude-daemmen/, 2019)
                * "[...] bevor die Ergebnisse der Europawahl feststanden, also bevor CDU und SPD einen historischen Denkzettel erhielten, indem Millionen ihrer Wähler zu den Grünen übergelaufen sind. [...] dass womöglich die Verlierer der Wahl eine 180-Grad-Kehrtwende hinlegen und sich bei Klimaschutzmaßnahmen gegenseitig zu übertreffen versuchen. Ja, in der Rückschau schon reichlich naiv, denn auch nach diesem eindeutigen Signal sind die Lieblingsmaßnahmen in dieser Sache nach wie vor **proaktives Abwarten und intensives Hoffen.**"
                * "Viel lieber wäre mir, wenn alle Parteien erkennen würden, wie elementar wichtig dieses Thema ist und untereinander nur um den besten Weg zu einem CO2-neutralen Land streiten würden. Ich weiß, viele tun so, als wäre das bereits der Fall, aber hier kommt meine Einleitung ins Spiel: Die meisten Akteure wissen offenbar gar nicht, was notwendig ist, um die Erderwärmung aufzuhalten"
                * "Wollen wir das 1,5-Grad-Ziel einhalten, dann müssen ALLE Sektoren ihre Treibhausgasemissionen drastisch senken, es ist hier kein Ablasshandel möglich in dem Sinne, dass wir den einen Sektor dekarbonisieren, um dann in unserem Lieblingssektor weiter folgenlos Mineralöl zu verbrennen."
                * "Unser Strom entsteht zu 60 Prozent aus fossilen Brennstoffen, unsere Autos fahren zu 95 Prozent mit Mineralöl und unsere Häuser werden zu 90 Prozent mit Öl und Gas beheizt. Zusammen gerechnet haben wir damit 14 Prozent der Energiewende umgesetzt."
                * "Diese fixe Idee, weiter dicke Benzinautos zu fahren, weil irgendwo im Land ein bisschen an der Gebäudedämmung geschraubt wird, ist vollkommen absurd und kindisch. Wenn es in meinem Haus durchs Dach regnet, dann sehe ich zu, möglichst schnell alle Löcher zu stopfen"
                * Ergebnisse des Klimakabinetts 2019, u. a. (welt.de)
                    * "Einschränkungen sollen die Bundesbürger möglichst nicht verordnet bekommen: „Wir wollen den Menschen nicht vorschreiben, was sie essen sollen“, sagte etwa die Sprecherin von Klöckner auf die Frage zur Notwendigkeit eines sinkenden Fleischkonsums. Verkehrsminister Scheuer betonte, er wolle „erlauben, erleichtern und ermöglichen und nicht verbieten, verteufeln und verteuern“."
                * "Hierzu fand dann noch eine denkwürdige Pressekonferenz statt, in der die zuständige Sprecherin allen Ernstes behauptete, es seien gar keine zwingenden Vorschriften notwendig, weil in der Bevölkerung ja bereits ein hohes Bewusstsein für den Klimaschutz bestünde."
                    * "Und als nächstes schaffen wir dann die Strafverfolgung ab, weil in der Bevölkerung so ein hohes Bewusstsein dafür besteht, dass man eigentlich nett zu seinen Mitmenschen sein soll?"
                * "Es ist ziemlich infam, hier so zu tun, als hätte die Bevölkerung es allein in der Hand, während die Regierung Rahmenbedingungen setzt, durch die klimaschonendes Verhalten unkomfortabel und teuer ist."
                    * z. B. teure Zugtickets und teure Pflanzenmilch
                * Flugtaxis
                    * https://www.klimareporter.de/verkehr/taxis-gehen-in-die-luft
                        * "Zurückhaltung auch beim ADAC. "Flugtaxis können für einzelne attraktiv sein, werden aber keinen nennenswerten Beitrag zur Lösung der urbanen Verkehrsprobleme leisten", meint Stefan Gerwens, Verkehrschef bei dem Autoclub. Sie wären dann eher ein Nischen-Transportmittel, etwa für Geschäftsleute, die ohne Stau vom Flughafen zum Termin in die Innenstadt fliegen wollen."
                * "Selbst wenn die Deutsche Autoflotte im Schnitt klimafreundlicher pro Auto wird, bringt das überhaupt nichts, wenn sich die Entwicklung fortsetzt und auch in den kommenden 10 Jahren zusätzliche 6 Millionen Autos zugelassen werden. Zudem ist das fraglich, wenn im Mai 2019 32 Prozent mehr SUVs und 22,8 Prozent mehr Geländewagen zugelassen wurden als im Mai 2018."
                * "Die Idee, einfach weniger zu verbrauchen, rennt gegen das Glaubensbekenntnis der CDU, die unter „Erhaltung der Schöpfung“ offenbar eher das Schützen der Aktienkurse versteht als das ihrer Lebensgrundlage."
                    * "Ich weiß, bei den Worten „weniger verbrauchen“ verfallen einige Menschen in harte Schnappatmung, aber weniger CO2-Emissionen heißt nicht automatisch weniger Lebensqualität: [...]"
                * "Teil 2: **Klimaschutz? Nein, das ist nicht sozialverträglich**!": https://graslutscher.de/5-untruegliche-anzeichen-dafuer-dass-jemand-vom-klimawandel-keine-ahnung-hat-und-unsere-zeit-verschwendet-teil-2-klimaschutz-nein-das-ist-nicht-sozialvertraeglich
                    * "eine rührselige Geschichte von einer Berliner Oma erzählte, die ihre Enkel in Köln besuchen will und das nicht könnte, wenn Flüge zu teuer wären."
                    * "Irgendjemand erklärt, dass man sozialverträglich vorgehen müsse und lehnt deswegen so gut wie alle vorgeschlagenen Maßnahmen ab, durch die auch nur ein Produkt teurer oder ein Arbeitsplatz unsicher werden könnten."
                        * "schon länger ein beliebter rhetorischer Kniff von Leuten, denen arme Menschen über komplette Legislaturperioden vollkommen egal sind, die aber immer dann aus dem Hut gezaubert werden, wenn es auf einmal ernst wird."
                            * "Wer so argumentiert, hat zwei zentrale Aspekte nicht kapiert oder ignoriert sie bewusst:"
                                * "CO2-Steuern kann man so gestalten, dass die dadurch eingenommenen Beträge wieder an die Bevölkerung zurückgezahlt werden, man spricht dabei von einer aufkommensneutralen Steuer."
                                    * Beispiel Schweiz
                                        * "Den Anteil, der von der Bevölkerung entrichtet wird, zahlt der Staat jedes Jahr über die Krankenkassen gleichmäßig an die Bürger zurück."
                                    * "Menschen, die sich klimaschonend verhalten, haben in diesem System sogar einen finanziellen Vorteil im Vergleich zum Ist-Zustand. Der geht zu Lasten derer, die sich eher klimaschädlich verhalten (hier recht gut dargestellt)." (z. B. Leute mit hohem Einkommen und damit höherem Verbrauch)
                                        * https://www.youtube.com/watch?v=BYm7Ngo7ORU
                                            * Arme Leute kriegen sogar noch was raus
                                        * "Der Witz ist nämlich, dass wirklich arme Menschen gar nicht über die Mittel verfügen, vergleichsweise viel CO2 zu emittieren."
                                * "Eine fortschreitende globale Erwärmung ist [nicht] sozialverträglich"
                                    * Nahrungsmittel werden teurer (für alle, egal ob arm oder reich)
                                    * Extremwetterereignisse nehmen zu
                                    * "Wir können jetzt also schön gar nichts machen und uns alle gegenseitig auf die Schultern klopfen, wie dolle sozialverträglich wir doch unterwegs sind, nur wird uns das dann in ein paar Jahrzehnten so was von um die Ohren fliegen, dass wie in der Rückschau wie die asozialste (im Wortsinn) Generation aller Zeiten aussehen. Oh, sagte ich „uns“? Sorry, mein Fehler, das fliegt natürlich nicht uns um die Ohren, vielmehr werden die kommenden Generationen Krisen mit einer sozialen Sprengkraft erleben, gegen die die Weltwirtschaftskrise von 1929 wie ein Kindergeburtstag wirkt."
                                    * "Es werden dann ein paar Milliarden Menschen mehr auf der Erde leben, gleichzeitig wird der Phosphor zur Neige gehen (ein entscheidender Bestandteil in heutigem Pflanzendünger)"
                                        * https://www.leibniz-gemeinschaft.de/forschung/junge-leibniz-wissenschaftler-im-interview/phosphor/ (Jahr?)
                                            * der Einzelne?
                                                * "Den Verzehr von Fleisch reduzieren, da die Fleischproduktion sehr viel Dünger verbraucht. Allgemein sollte eine Landwirtschaft unterstützt werden, die so wenig mineralischen Phosphor wie möglich einsetzt und geschlossene Phosphorkreisläufe anstrebt."
                                    * "Wer also auf die Idee kommt, unsere heutige Untätigkeit als sozialverträglich zu bezeichnen, ist krass unwissend. Oder ein Lügner."

Politik
-------
### Allgemein
* Unabhängige IT/Datenverarbeitung:
    * siehe Freie Software
* Umwelt, Gesundheit und Gerechtigkeit:
    * siehe vegan
    * siehe Bio
* Unterstützung demokratischer Strukturen:
    * siehe Freie Software
    * siehe Bio
* Förderung der lokalen Wirtschaft:
    * siehe Freie Software
    * siehe Bio
* Kosten-Reduktion:
    * siehe Freie Software
    * siehe Bio
    * siehe vegan
* Datenschutz / Privatsphäre:
    * siehe Freie Software
    * siehe [Warum Privatsphäre wichtig ist](../Hintergrund/privatsphaere.md)

### Grün
* Unabhängige IT/Datenverarbeitung:
    * siehe Freie Software
* Tierwohl, Umwelt (inkl. Klimaschutz), Gesundheit und Gerechtigkeit:
    * siehe Bio
    * siehe vegan
* Strategie:
    * in der eigenen Partei vorleben und als Vorbild fungieren: bio, vegan

### Konservativ / Werte
* 2020: in Verbindung setzen mit konservativen Werten:
    **_nicht_ Gewohnheitserhalt**
    sondern Erhalt der wirklichen Werte; welche sind das?
        Ehrlichkeit, Familie, Freiheit (für möglichst viele), Gerechtigkeit (landes- und weltweit und über Generationen hinweg)
        weitere? (FRAGEN: was sind konservative Werte? Und welche Rolle spielt dabei die Flächenversiegelung?)
* Förderung des Mittelstandes:
    * siehe Freie Software
* Unabhängige IT/Datenverarbeitung:
    * siehe Freie Software
* Umwelt und Gesundheit:
    * siehe Bio
    * siehe vegan
* Vertretung konservativer Grundwerte wie Gerechtigkeit und Demokratie:
    * siehe Freie Software
    * siehe vegan
    * siehe Bio
* Verringerung der Erpressbarkeit:
    * durch Unabhängigkeit in der Energieversorgung: siehe EE
    * Reduktion der Lebensmittelimporte: siehe vegan, siehe Bio
    * in der IT: siehe Freie Software

Stützen der Gesellschaft
------------------------
* Gut situierte Organisationen und Personen, die über eine überdurchschnittliche Fähigkeit der Informationsbeschaffung und -auswertung verfügen.
    * => Mehr Verantwortung für das eigene Handeln?

Kantine / Mensa
---------------
* [kantine](../Hintergrund/kantine.md)

Spiritualität
-------------
* Buch_: ["The World Peace Diet: Eating for Spiritual Health and Social Harmony"](https://www.goodreads.com/book/show/359642.The_World_Peace_Diet)
