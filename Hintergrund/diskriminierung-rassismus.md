Rassismus
=========

<!-- toc -->

- [Amerika (ehemalige Europäer)](#amerika-ehemalige-europaer)
  * [Ein amerikanischer Held - Die Geschichte des Colin Kaepernick, 2020](#ein-amerikanischer-held---die-geschichte-des-colin-kaepernick-2020)
  * [Michael Che's Civil Rights Update, 5 min, !](#michael-ches-civil-rights-update-5-min-)
  * [John Oliver on institutional racism, Police](#john-oliver-on-institutional-racism-police)
  * [Whites learn how American justice works which blacks already known (Trevor Noah), 2019](#whites-learn-how-american-justice-works-which-blacks-already-known-trevor-noah-2019)
  * [Film - Just Mercy](#film---just-mercy)
  * [Film - Belle](#film---belle)
  * [Film - I Am Not Your Negro](#film---i-am-not-your-negro)
  * [Film - American History X](#film---american-history-x)
  * [Doku - Mississippi's War: Slavery and Secession | MPB](#doku---mississippis-war-slavery-and-secession--mpb)
- [The Freedom Riders History (1961)](#the-freedom-riders-history-1961)
  * [Martin Luther King Jr.](#martin-luther-king-jr)
  * [Freedom Riders](#freedom-riders)
  * [Bernard Lafayette Jr. ! / Relationships and Non-Violent Protest](#bernard-lafayette-jr---relationships-and-non-violent-protest)
  * [Childism](#childism)
  * [Selma](#selma)
  * [Filme](#filme)
  * [Mighty Times: The Children's March](#mighty-times-the-childrens-march)
  * [Injustice anywhere...](#injustice-anywhere)
  * [Analogie von Extinction Rebellion](#analogie-von-extinction-rebellion)
- [Alltagsrassismus](#alltagsrassismus)
  * [Betroffene schildern Alltagserfahrungen, 2018](#betroffene-schildern-alltagserfahrungen-2018)
  * [Der alltägliche Rassismus - Schwarze in Deutschland](#der-alltagliche-rassismus---schwarze-in-deutschland)
  * [Beispiele](#beispiele)
  * [Gesellschaft](#gesellschaft)
  * [Opferrolle / empfindlich](#opferrolle--empfindlich)
  * [vs. Rechtsextremismus](#vs-rechtsextremismus)
  * [Weiteres](#weiteres)
- [Postive Beispiele](#postive-beispiele)
  * [Widerspruch gegen Rassismus - Mainz 05, 2020](#widerspruch-gegen-rassismus---mainz-05-2020)
  * [arte-Doku über Independent Office for Police Conduct, UK](#arte-doku-uber-independent-office-for-police-conduct-uk)
- [Black Lives Matter](#black-lives-matter)
  * [All lives matter?](#all-lives-matter)
- [Afrika](#afrika)
  * [FUCK WHITE TEARS by Annelie Boros (Germany 2016)](#fuck-white-tears-by-annelie-boros-germany-2016)
  * [Apartheid](#apartheid)
- [Rechtsextremismus](#rechtsextremismus)
  * [Umgang im öffentlichen Diskurs / soll-man-mit-neonazis-reden](#umgang-im-offentlichen-diskurs--soll-man-mit-neonazis-reden)
  * [Widersprechen](#widersprechen)
  * [Wie wird man zum Neonazi?](#wie-wird-man-zum-neonazi)
- [Verschiedenes](#verschiedenes)
  * [in der Philosophie](#in-der-philosophie)

<!-- tocstop -->

Amerika (ehemalige Europäer)
----------------------------
### Ein amerikanischer Held - Die Geschichte des Colin Kaepernick, 2020
* https://www.arte.tv/de/videos/086146-000-A/ein-amerikanischer-held/, 50 min
    * "2016 ging der amerikanische Footballer Colin Kaepernick bei der Nationalhymne, die vor jedem Spiel gesungen wird, auf die Knie. Es war ein Akt des Protests gegen rassistische Polizeigewalt, der das Land in eine Debatte um Rassismus und nationale Identität stürzte. Der Dokumentarfilm erzählt von Kaepernicks Aufstieg zur Ikone einer Protestbewegung und ist heute aktueller denn je."
    * schöne Heldenmusik
    * Bedeutung der Nationalhymne
    * "Er wurde einer der bekanntesten Sportler der USA – und beschloss, das zu nutzen,
        um ein Zeichen gegen rassistische Polizeigewalt zu setzen.
        2016 ging er während der Hymne auf die Knie. Für die einen war Kaepernick ein Held, für die anderen ein Vaterlandsverräter."

### Michael Che's Civil Rights Update, 5 min, !
* https://www.youtube.com/watch?v=AeN_SVoJet0, 2020, 5 min
    * ...seen the news... in the past 400 years?
        * (a rough patch - "If you have or go through a rough patch, you have a lot of problems for a time.")
    * brother is a cop
    * Black Lives Matter
        * "not matters more than you; just matters", "Matters", "Just Matters"
            * "That's where we're starting the negotiations."
            * can't agree on that shit? WTF is less than matters? Black lives exist?
        * equal rights?
        * fight for civil rights, just civil, not even equal, just be civil
            * https://de.wikipedia.org/wiki/B%C3%BCrgerrecht
                * "Unter Bürgerrechten versteht man im Allgemeinen nur solche Rechte,
                    die sich auf das **Verhältnis zwischen Bürger und Staat** beziehen,
                    weniger auf das Verhältnis von Einwohnern des Staates untereinander."
        * "They don't tell you black lives don't matter. That's not the argument."
            * "They hit you with this slick shit"
            * "All lives matter", "Really? Semantics?"
            * ("Do you love me?" "What you are talking about. I love everybody.")
            * get over with... slavery 400 years ago, ..., police shooting 2 weeks ago
            * 9/11, "all buildings matter" T shirt

* "Michael Che - Thoughtful Racism", https://www.youtube.com/watch?v=37G0D6s0J8s, 2015, 7 min, !
    * ...todo...
    * ...
    * gay marriage? what's the problem? Yeah, what will they do next is marriage with animals!
        * Probably, but what's the problem. I eat animals. So let others f... them, that's your business.
           I am pretty sure I am doing the worst thing to it. Let's see what if I was a goat...

* "Michael Che - Lying on Your Résumé, Paying Taxes & The History of Sexting", https://www.youtube.com/watch?v=6AhAi0NfWbs, 2018, 7 min
    * ...

### John Oliver on institutional racism, Police
* https://www.youtube.com/watch?v=Wf4cea5oObY, "Police: Last Week Tonight with John Oliver (HBO)", 2020, 33 min
    * ...

### Whites learn how American justice works which blacks already known (Trevor Noah), 2019
* https://youtu.be/ldZJx5irpiQ?t=364 - "Best of Between the Scenes 2019 | The Daily Show"
    * obstruction of justice
    * mother and cookie analogy
    * case of a black who was arrested because he resisted arrest
    * https://www.youtube.com/watch?v=MdcEyCgp0RY - "Funniest Donald Trump Moments Of 2019 | The Daily Show With Trevor Noah"
        * https://www.theguardian.com/us-news/2019/apr/03/trump-claims-father-born-germany-false-fred-trump
            * "Donald Trump has wrongly claimed his father was born in Germany, again"

### Film - Just Mercy
* Trailer: https://www.youtube.com/watch?v=GVQbeG5yW78
* über Vorurteile, systemischen Rassismus im amerikanischen Justizsystem und Todesstrafe
* basiert auf wahrer Geschichte
* "“Just Mercy” follows young lawyer Bryan Stevenson and his history-making battle for justice.", ::gerechtigkeit

### Film - Belle
* ...inkl. Sklavenhandel..., siehe auch Kolonialismus

### Film - I Am Not Your Negro
* gezeigt auf arte
* https://de.wikipedia.org/wiki/I_Am_Not_Your_Negro
    * "ist ein Dokumentarfilm von Raoul Peck (Drehbuch, Regie). Das unvollendete Manuskript Remember This House von James Baldwin (1924–1987), gesprochen von Schauspieler Samuel L. Jackson, ist die Grundlage für Pecks filmische Collage aus Ausschnitten der Medienberichterstattung vor allem der zweiten Hälfte des 20. Jahrhunderts, in der er dem weißen Rassismus in der amerikanischen Gesellschaft nachspürt."
    * ...

### Film - American History X
* = name of the private history lesson for one of the protagonists
* ...

### Doku - Mississippi's War: Slavery and Secession | MPB
* https://www.youtube.com/watch?v=U3CFD2RRF80, "FULL DOCUMENTARY: Mississippi's War: Slavery and Secession | MPB", 2014, 1 h
    * todo: ist das was?

The Freedom Riders History (1961)
---------------------------------
### Martin Luther King Jr.
* https://de.wikipedia.org/wiki/Martin_Luther_King
* https://www.nzz.ch/feuilleton/martin-luther-king-wurde-vor-90-jahren-geboren-ld.1451179, 2019
    * todo
* https://www.deutschlandfunk.de/todestag-von-martin-luther-king-seine-botschaft-ist.1773.de.html?dram:article_id=383006, 2017
    * "In Zeiten politischer Spaltung halten dort viele Kings berühmte „Traum“-Rede noch für aktuell."
    * "Stephon Ferguson trägt die Reden des Bürgerrechtlers Martin Luther King vor."
        * z. B. https://www.youtube.com/watch?v=Yj6Y6H2yu8g, 5 min
    * "Das ist jetzt wichtiger als je zuvor in der Geschichte unseres Landes. So viel geschieht: Wir haben einen neuen Präsidenten, es gibt so viel Spaltung, so viel Böses und Hass."
    * "„Wenn man sich die ‚Ich habe einen Traum‘-Rede heute anhört, ist sie immer noch aktuell.
    Die gleichen Themen, die gleichen Probleme. Es ist ein wenig besser geworden,
    aber viele Probleme sind einfach nur verdeckt. Es gibt sie noch – sie äußern sich nur anders.“"
    * "„Ja, es gibt Grund zur Sorge. Wir brauchen mehr Menschen, die wie Dr. King sind,
    die bereit sind, aufzustehen und etwas zu tun. Menschen, die Unrecht nicht akzeptieren
    und ihr Leben dem Kampf dagegen widmen. Ich möchte, dass Menschen, die hören wie ich
    die Rede vortrage, sich inspiriert und motiviert fühlen, etwas für die Gesellschaft zu tun.“"
* Buch_: "Ich habe einen Traum", ?
* Rede I have a dream: https://www.youtube.com/watch?v=vP4iY1TtS3s

* "Roger Hallam - Why Public Disruption is Necessary - Extinction Rebellion", 2019, 17 min
    * https://www.youtube.com/watch?v=pckkpXBfqXM, 17 min
    * was "most hated man" back then, siehe Umfragen:
        * https://www.crmvet.org/docs/60s_crm_public-opinion.pdf - Umfrageergebnisse von 1961ff
            * z. B. "Do you approve or disapprove of what the 'Freedom Riders' are doing? - 22% Approve 61% Disapprove 18% No Opinion"
            * z. B. Denken Sie, da sind vor allem Kommunisten daran beteiligt?
    * ...
    * 16:00 Duty, Tradition, Legacy
        * "What do you want to leave? What is the legacy of this generation? The destruction of the next generation?"

### Freedom Riders
* Video: ["The Freedom Riders History"](https://www.youtube.com/watch?v=1zBY6gkpbTg), 5 min, 2010
    * ungerechte Rassengesetze und ziviler Ungehorsam
    * "In the spring of 1961, black and white civil rights activists rode buses to protest the segregationist policies of the Deep South"
    * Je tiefer sie in den Süden kamen, desto feindlicher wurde die Stimmung und gewalttätige Angriffe folgten
        * (Die Angreifer waren Menschen, die sich sicher waren, das Richtige zu tun)
        * (außerdem gab das Gesetz - zumindest ein Teil davon - eine Rechtfertigung)
    * Auch weiße Befürworter wurden angegriffen, teilweise aus dem Hinterhalt
    * jail-no-bail-tactic

### Bernard Lafayette Jr. ! / Relationships and Non-Violent Protest
* Video: ["Relationships and Non-Violent Protest: Bernard Lafayette Jr. at TEDxEmory"](https://www.youtube.com/watch?v=AiWhMEr-Vhc), 2012, 21 min
    * (from https://www.youtube.com/watch?v=5oOvjbCDh0Y - "Animal Liberation Conference, California - REVIEW")
    * https://en.wikipedia.org/wiki/Bernard_Lafayette - Nashville, Selma, Martin Luther King Jr.
        * TODO: Film: Selma, ...
    * It is about **love** (noch bevor es non-violence genannt wurde)
        * ...
        * (siehe auch "Die Kunst zu lieben" von Erich Fromm)
        * ...
    * https://de.wikipedia.org/wiki/Agape - Liebe ohne Bedingung oder so
        * not give only to gain
        * ...
        * self-regenerative
    * Nashville
    * Kontrolliere deine Gefühle, um die Situation zu kontrollieren
    * ...
    * Racism is based on **childism**
        * Gib einem Kind 20 Dollar. Erwartet man ein vernünftiges Umgehen mit dem Geld?
        * Beispiel Afrikaner aus Sicht des Westens oder Frauen
        * "skin color is just a convenient landmark to separate those who one wants to exploit"
    * ...
    * **Beispiel** wie er selber angegriffen wurde
        * "unexpected but genuine behaviour"
        * fighting on the inside; maintain your own dicipline; fight fear
        * if he had his assailants background he had probably behaved the same
    * ...

### Childism
* https://en.wikipedia.org/wiki/Childism
    * "Childism can refer either to advocacy for empowering children as a subjugated group
        or to prejudice and/or discrimination against children or childlike qualities"

* https://happinessishereblog.com/we-need-to-talk-about-childism/, 2016
    * Friseur-Beispiel
    * Beispiel: Nach Essen fragen
    * ...
    * ...

* https://www.psychologytoday.com/us/blog/child-in-mind/201201/understanding-childism-are-we-prejudiced-against-children, 2012
    * "Understanding Childism: Are We Prejudiced Against Children?"
    * defined as "a prejudice against children on the ground of a belief that they are property and
        can (or even should) be controlled, enslaved, or removed to serve adult needs."

### Selma
* todo Film

### Filme
* The Green Book

### Mighty Times: The Children's March
* https://en.wikipedia.org/wiki/Mighty_Times:_The_Children%27s_March
* https://www.zinnedproject.org/materials/childrens-march, todo
* Video: "Mighty Times: The Children's March", https://www.youtube.com/watch?v=BT-QkNkMZjk, 40 min, Birmingham
    * ...
    * ...
    * under the current governor the city was close to a police state; white tank
    * ...
    * https://en.wikipedia.org/wiki/George_Wallace%27s_1963_Inaugural_Address
        * "In the name of the greatest people that have ever trod this earth, I draw the line in the dust and toss the gauntlet before the feet of tyranny, and I say segregation now, segregation tomorrow, segregation forever."
    * ...
    * King in jail
        * James Bevel
            * https://en.wikipedia.org/wiki/James_Bevel
                * "called a father of voting rights, the strategist and architect of the 1960s Civil Rights Movement"
                    * civil rights = Bürgerrechte
        * 9:00 Nashville
            * "the stark reality of non-violence being met with violence was shocking and scary and it drew kids in by the hundreds"
        * hard work to keep the protest peaceful
        * free from jail
            * 11:00 who want's to go to jail? - nobody stands up except for the children
    * 12:45 D-Day planning
        * secret planning
    * 18:00 many arrests; even have to take school buses
        * 20:15 pictures of black kids arrested
        * day 1: almost 1000 arrests
    * 23:00 Hochdruck-Wasserwerfer gegen die Kinder (tat weh)
        * 24:15 heard the firemen yelling: knock the niggers down
    * 25:15 let the dogs out at the children; on the leash
        * aber dennoch: die Leute werden reihenweise gebissen
    * 26:10 day 2: 1200 arrests
    * 28:30 day 3: 4000 arrests
        * rats in prison, dry bread as food
        * every child should be interrogated, e.g. a 9 years old girl was asked: who told you to march etc.
        * images of children in jail; also singing
        * why are you here? - teedom (could not say freedom)
    * 30:30 immer wieder Gewalt von weiß und schwarz
        * Kennedy sagt: bitte erinnert euch, dass Gewalt immer noch mehr Gewalt bringt
    * 34:15 city of Birmingham "faced a state of collapse", 3000 students marched downtown, "the police had been made helpless"
    * 36:00 Kennedy on televion: equal rights and equal opportunities
        * 5 month later Kennedy was killed
        * Civil Rights Act was passed in 1964
        * violence, e.g. 16th Street Baptist Church with four kids killed
        * 5 years etc. later: King was killed
    * children are the heros

### Injustice anywhere...
https://www.goodreads.com/quotes/631479-injustice-anywhere-is-a-threat-to-justice-everywhere-we-are

    “Injustice anywhere is a threat to justice everywhere. We are caught in an inescapable network of mutuality, tied in a single garment of destiny. Whatever affects one directly, affects all indirectly.”

    ― Martin Luther King Jr., Letter from the Birmingham Jail

### Analogie von Extinction Rebellion
* ...siehe Vortrag...

Alltagsrassismus
----------------
### Betroffene schildern Alltagserfahrungen, 2018
* "Rassismus: Betroffene schildern Alltagserfahrungen", https://www.tagesschau.de/multimedia/video/video-387243.html, 2018, 3 min
    * 3 Mitarbeiter von ARD berichten von ihren Erfahrungen
    * begleitet ihn seit Grundschulalter
        * einziger Schwarzer; andere bezeichnen ihn als Neger oder Negerkuss
        * passiert auf verschiedenen Ausprägungsstufen
        * ist Alltag
    * als gute Leistung ausgelegt, dass sie ihre Muttersprache so gut beherrscht
        * kann gut tanzen, weil sie habe es ja im Blut
        * ist den meisten nicht bewusst, wenn sie etwas Rassistisches sagen oder tun
        * oft wird Erklärung, dass etwas rassistisch war, als falsch abgewiegelt
    * Salamipizza können wir nicht bestellen
        * Taxifahrer fragt Kollegen, ob sie eigentlich deutsch verstehe
        * die Leute meinen es meistens nicht böse; denken nicht drüber nach
        * wichtig ist, es zu erklären, wo das Problem liegt

### Der alltägliche Rassismus - Schwarze in Deutschland
* https://www.zdf.de/politik/frontal-21/der-alltaegliche-rassismus-lang-100.html, 12 min, 2020
    * "protestieren Menschen weltweit gegen Rassismus, auch hierzulande.
        Denn viele Schwarze Deutsche erfahren ihr Leben lang immer wieder Diskriminierung, Ausgrenzung und manchmal auch Gewalt."
    * TODO

### Beispiele
* Anekotisch
    * Was ist dein Mädchenname? Passkontrolle im Zug an der Grenze Öst./D. mit der Feststellung des Kontrolleurs "ist ne Deutsche"
    * Kinder bekommen wollen in einer Beziehung schwarz / weiß: die Kindheit unseres Kindes wird sich von einer unterscheiden; das muss dir bewusst sein
    * Paar mit zwei Söhnen; wird unterschiedlich erzogen; z. B. zieh dir keinen Kapuzenpulli an
    * Ab ca. 5 - 6 Jahren: Kinder werden sich bewusst

### Gesellschaft
* https://de.wikipedia.org/wiki/Alltagsrassismus
    * ...
    * https://de.wikipedia.org/wiki/Alltagsrassismus#Diskursanalytische_Untersuchung
        Die Einwanderer sind dazu eingeladen worden, in unser Land zu kommen.
        Sie verursachen den Niedergang der Stadt bzw. der Nachbarschaft.
        Sie (die eingewanderten Arbeiter) arbeiten hart.
        Sie nehmen unsere Häuser in Beschlag.
        Sie missbrauchen unser System der sozialen Sicherheit und leben von Sozialhilfe.
        Sie müssen sich unseren Normen und Regeln anpassen.
        Sie haben andere Lebensgewohnheiten, Sitten und Gebräuche.
        Erziehung sollte nur in unserer Sprache stattfinden.
    * https://de.wikipedia.org/wiki/Duisburger_Institut_f%C3%BCr_Sprach-_und_Sozialforschung
        * https://www.diss-duisburg.de/
* https://www.amadeu-antonio-stiftung.de/w/files/pdfs/ich_habe_nichts_2.pdf, 2007, »Ich habe nichts gegen Juden, aber...«
    * "die **gruppenbezogene Menschenfeindlichkeit**. Dieser Begriff mag sehr sperrig sein, aber er umfasst das gesamte Syndrom von Einstellungen,
        die Menschen deshalb abwertet, weil sie einer bestimmten Gruppe angehören:
        Einwanderer, Juden, Schwarze, Sintiund Roma, Homosexuelle, Muslime, Obdachlose oder Frauen."
    * "Stimmt jemand z. B. rassistischen Klischees zu, dann ist die Wahrscheinlichkeit hoch, dass er auch anderen Gruppen gegenüber feindlich werden kann."
    * ...
    * ...
    * https://www.bpb.de/politik/extremismus/rechtsextremismus/214192/gruppenbezogene-menschenfeindlichkeit, 2015, "Gruppenbezogene Menschenfeindlichkeit"
        * ...
        * "Es fällt allerdings auf, dass nahezu kulturübergreifend das Geschlecht, das Alter,
            die Religion, die ethnisch-kulturelle Herkunft und vielfach auch die sexuelle Orientierung
            und eine Behinderung als Merkmale herangezogen werden, um Ungleichwertigkeiten zu begründen."
        * ...

### Opferrolle / empfindlich
* https://www.tagesspiegel.de/wissen/alltagsrassismus-in-deutschland-wenn-der-hass-krank-macht/25295914.html, 2019
    * **"Den Opfern wird unterstellt, sie seien empfindlich"**
    * "Von Rassismus oder Antisemitismus betroffene Personen registrieren demnach oft eine Marginalisierung, die von den Vertretern der Mehrheitsgesellschaft heruntergespielt wird."

### vs. Rechtsextremismus
Rassismus ist nicht gleich Rechtsextremismus

### Weiteres
* https://www.bpb.de/politik/extremismus/rechtsextremismus/262065/zur-verbreitung-des-antisemitismus-in-deutschland, 2017
    * "Zur Verbreitung des Antisemitismus in Deutschland: Empirische Forschungsbefunde und methodische Probleme"
    * "Zahlreiche sozialwissenschaftliche Studien der jüngeren Vergangenheit haben gezeigt,
        dass sich Antisemitismus nicht nur in der extremen Rechten, sondern auch in der Mitte der Gesellschaft findet."
    * "Umstritten ist allerdings, inwieweit damit auch auf rechtsextreme Einstellungen in der Mehrheitsgesellschaft geschlossen werden kann."
* https://www.bpb.de/dialog/194569/offensichtlich-und-zugedeckt-alltagsrassismus-in-deutschland
    * ...
* https://www.hr-inforadio.de/programm/themen/rassismus-im-alltag-hat-viele-gesichter,alltagsrassismus-102.html, 2020
    * ...

Postive Beispiele
-----------------
### Widerspruch gegen Rassismus - Mainz 05, 2020
* https://www.volksverpetzer.de/bericht/mainz-05-rassistisch/
    * "So cool reagiert Mainz 05 auf ein rassistisches Ex-Vereinsmitglied"

### arte-Doku über Independent Office for Police Conduct, UK
* https://www.arte.tv/de/videos/085431-000-A/feindbild-polizei/, "Feindbild Polizei - Gewalt und Gegengewalt ohne Ende?", 2020, NDR
    * "Über Monate liefern sich in Frankreich Gelbwesten-Demonstrierenden Straßenschlachten mit der Polizei. Deutsche Polizei-Gewerkschaften beklagen seit Jahren die zunehmende Gewalt. Die Polizei ist zum Feindbild geworden. Der Dokumentarfilm zeigt, wie der Personalabbau der letzten Jahre, der Einfluss von Social Media und eine repressive Polizeitaktik die Gewaltspirale befeuern."
    * ...
    * "59 Polizisten haben sich 2019 in Frankreich das Leben genommen. Immer mehr Aufgaben, Stress und Gewalt setzen auch Polizisten in Deutschland unter Druck."
    * "Mit mehr Kommunikation statt Repression – so wollen zumindest Einsatzpolizist Jan-Patrick Huke und seine Hundertschaft in Hannover bei einer Demonstration am Rande eines AfD-Parteitages vorgehen. Dass einige seiner Kollegen mit der AfD sympathisieren, kann er nicht verstehen."
    * "Laut einer Umfrage unterstützt aber nur eine geringe Zahl an Innenministerien eine vom Bund der Kriminalbeamten geforderten Studie zu extremistischen Einstellungen in den eigenen Reihen. Vor allem die fehlende juristische Verfolgung von Polizeigewalt kostet Vertrauen."
        * "Anders in England und Wales. Die Dokumentation bietet Einblicke in die Arbeit des Independent Office for Police Conduct in Birmingham. Leiter Derrick Campbell und seine 150 Mitarbeiter ermitteln unabhängig beim Verdacht von Polizeigewalt. Die Arbeit des IOPC hat das Vertrauen der Gesellschaft in die Polizei gestärkt, so Campbells Überzeugung. "
    * https://www.deutschlandfunkkultur.de/arte-doku-feindbild-polizei-bei-polizeigewalt-fehlt-die.2168.de.html?dram:article_id=478551
        * "Sparpolitik führt zu mehr Polizeigewalt"
        * "Unabhängige Behörden wichtig"

Black Lives Matter
------------------
### All lives matter?
* **All plates matter**: https://www.youtube.com/watch?v=NtAAeyswlHM, 2016, 2 min

* Kritik: https://de.wikipedia.org/wiki/Black_Lives_Matter#%E2%80%9EAll_Lives_Matter%E2%80%9C

* "Why you should stop saying “all lives matter,” explained in 9 different ways", 2016
    * https://www.vox.com/2016/7/11/12136140/black-all-lives-matter
        * "all houses matter"
        * get my fair share
            * "TL;DR: The phrase "Black lives matter" carries an implicit "too" at the end;
            it's saying that black lives should also matter. Saying "all lives matter"
            is dismissing the very problems that the phrase is trying to draw attention to."
        * "all plates matter"
        * “There are other diseases too”
            * "Do people who change #BlackLivesMatter to #AllLivesMatter
                **run thru a cancer fundraiser going "THERE ARE OTHER DISEASES TOO"**"
        * “I too have felt loss”
            * "WTF is the impulse behind changing #BlackLivesMatter to #AllLivesMatter.
            **Do you crash strangers' funerals shouting I TOO HAVE FELT LOSS?**"
        * ...

* https://www.goodhousekeeping.com/life/a32745051/what-black-lives-matter-means/, 2020

Afrika
------
### FUCK WHITE TEARS by Annelie Boros (Germany 2016)
* https://www.youtube.com/watch?v=zbI0IGZwMCc, 25 min
    * "Annelie Boros comes to Cape Town to make a film about the student protests. When she gets here, the black students turn her away: they don't want her, as a white person, to make a film about this Black protest movement. The filmmaker wants to understand why and shows us a film about a film that couldn't be made, because she is white. It's about centuries-old divides and power structures in South Africa, 20 years after the end of Apartheid."

### Apartheid
* siehe erinnerung

Rechtsextremismus
-----------------
### Umgang im öffentlichen Diskurs / soll-man-mit-neonazis-reden
* https://www.bpb.de/politik/extremismus/rechtsextremismus/232223/toralf-staud-soll-man-mit-neonazis-reden (todo)
    * "Das Kalkül dabei ist klar: Wenn man sie zu Gesprächen zulässt, können Rechtsextreme ihre Positionen in die Debatten einspeisen – und sie wirken schnell als legitimer Teil des demokratischen Meinungsspektrums. Dabei sind Demokraten in öffentlichen Diskussionsveranstaltungen strukturell im Nachteil, warnt der politische Bildungsreferent Andreas Hechler: "Das sattsam bekannte Parolenspringen kann man nur verlieren."
    -> s: "Die oft kurzen, zugespitzten, demagogischen Aussagen von Rechtsextremen können meist nur durch komplexe und komplizierte
    Antworten widerlegt werden – was in öffentlichen Veranstaltungen mit begrenzter Zeit und Publikumsaufmerksamkeit schwierig ist."

### Widersprechen
* Simone Rafael, Amadeu Antonio Stiftung, https://www.bpb.de/politik/extremismus/rechtsextremismus/232137/simone-rafael-rassismus-widersprechen (todo)
    * s: "plädiert dafür, im schriftlichen Kontext
        die **Äußerungen nicht unbewertet stehen zu lassen**, das kann man auch in Diskussionen immer wieder machen - die
        menschen- und demokratiefeindlichen Dinge benennen, sich dagegen positionieren und dann weitermachen mit den
        lösungsorientierten, sinnvollen Gesprächen. Nicht immer müssen es lange Erläuterungen und Versuche zum Widerlegen sein..."

* https://www.bpb.de/politik/extremismus/rechtsextremismus/232141/klaus-peter-hufer-argumente-wirken
    * Klaus-Peter Hufer: "Ich bin überzeugt von der Wirkung und der Bedeutung von Argumenten"
    * s: "Mit Holocaust-leugnenden "Hardcore-Nazis" redet Prof. Klaus-Peter Hufer nicht – um sie
        **nicht salonfähig zu machen**. In den meisten anderen Fällen aber führt sein unerschütterlicher Glaube
        an die Aufklärung dazu, dass er das Gespräch mit Neonazis und Mitläufern aufnimmt und ihnen mit Argumenten entgegentritt."

### Wie wird man zum Neonazi?
* "Wie wird man zum Neonazi? Aussteiger trifft Extremismus-Experte | DISKUTHEK", stern, 2020, 30 min, TODO
    * https://www.youtube.com/watch?v=cJ5gqzhQImk
        * "Christian Weißgerber ist Ex-Neonazi, Kulturwissenschaftler und Philosoph. Über seine Zeit in der Szene hat er das Buch “Mein Vaterland!” geschrieben."
        01:30 – Leitfrage 1: Wie wird man Neonazi?
        08:25 – Leitfrage 2: Sind die neuen Nazis gefährlicher?
        24:37 – Leitfrage 3: Wie kann der Ausstieg gelingen?

Verschiedenes
-------------
### in der Philosophie
* https://www.heise.de/tp/features/Der-verschwiegene-Rassismus-der-Philosophen-3363965.html, 2014, Patrick Spät
    * Achtung, zur Einordnung siehe Experten-Kommentare im User-Forum
