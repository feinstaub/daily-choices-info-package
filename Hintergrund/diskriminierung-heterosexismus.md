Heterosexismus
==============

<!-- toc -->

- [Homophobie](#homophobie)
  * [Homosexuellen-Verfolgung in der NS-Zeit, Emil Haab](#homosexuellen-verfolgung-in-der-ns-zeit-emil-haab)
  * [§175](#%C2%A7175)
  * [Prominente Beispiele: Billie Jean King](#prominente-beispiele-billie-jean-king)
  * [Aus dem Kolonianismus](#aus-dem-kolonianismus)

<!-- tocstop -->

"Heterosexismus wertet Homo-, Bi- und Intersexuelle sowie Transgender als „unnormal“
ab und stellt ihnen Heterosexualität als überlegene oder auch einzig natürliche Form gegenüber."
(https://de.wikipedia.org/wiki/Heterosexismus)

Homophobie
----------
### Homosexuellen-Verfolgung in der NS-Zeit, Emil Haab
* https://www.tagesschau.de/inland/homosexualitaet-nationalsozialismus-101.html, 2020
    * "Unzählige Männer wurden damals drangsaliert und getötet. Eine Reportage über den Buchhändler Emil Haab aus der Pfalz."
    * "Experten schätzen, dass in dieser Zeit etwa 100.000 Menschen aufgrund ihrer sexuellen Orientierung drangsaliert wurden.
        Etwa 50.000 wurden verurteilt, bis zu 10.000 Menschen ermordet."
    * "Sie seien **Staatsfeinde**, würden das gesellschaftliche System gefährden und die Jugend verderben."
        * (klingt nach Trump, siehe z. B. Kaepernick)
    * "In der NS-Diktatur wurde dieses Bild über die Presse an die Bevölkerung weitergegeben.
        Das führte immer wieder zur Hatz gegen Homosexuelle. Teils wurden sie in Selbstjustiz durch die Straßen getrieben.
        So war damals die Zeit."

### §175
* tagesschau: "Der lange Kampf gegen §175 - Wegen des "Schwulen-Paragrafen" 175 wurden 64.000 Menschen verurteilt."

### Prominente Beispiele: Billie Jean King
* https://en.wikipedia.org/wiki/Billie_Jean_King
    * Film: The Battle of the Sexes

### Aus dem Kolonianismus
* siehe diskriminierung-kolonianismus.md
