Inbox 2018 (unsortiert)
=======================

<!-- toc -->

- [Vegan](#vegan)
- [Käfighaltung (für alle Tiere beenden) - EU-Petition](#kafighaltung-fur-alle-tiere-beenden---eu-petition)
- [Filme](#filme)
- [Dokus](#dokus)
  * [Westlich](#westlich)
  * [Welt](#welt)
- [Frage: Kann man Fleisch von alten Tieren essen?](#frage-kann-man-fleisch-von-alten-tieren-essen)
- [Frage: Was ist ein Hybrid-Huhn?](#frage-was-ist-ein-hybrid-huhn)
- [Frage: Wieviel Fleisch fressen die Katzen in Deutschland?](#frage-wieviel-fleisch-fressen-die-katzen-in-deutschland)
- [Medien](#medien)
- [Bio](#bio)
  * [Ökologische und konventionelle Landwirtschaft - Ökolandbau: Ist „Bio“ wirklich nachhaltiger?](#okologische-und-konventionelle-landwirtschaft---okolandbau-ist-%E2%80%9Ebio-wirklich-nachhaltiger)

<!-- tocstop -->

Vegan
-----
* ["How could veganism change the world? | The Economist"](https://www.youtube.com/watch?v=hwoL6hWd4l0), 2018, 6 min
    * ...
    * "Interest in vegan food has been booming across the rich world. A major study has put the diet to the test - analyzing an imagined scenario in which the world goes vegan by 2050. If everybody went vegan by 2050 we estimated that food-related greenhouse gas emissions could be reduced by 3/4."
    * see YT comments
    * https://de.wikipedia.org/wiki/The_Economist
        * "The Economist [ðɪ ɪ'kɒnəmɪst] ist eine britische Wochenzeitung mit den Schwerpunkten internationale Politik und Weltwirtschaft. Charakteristisch sind ihre liberale Prägung und die globale Berichterstattung"

Käfighaltung (für alle Tiere beenden) - EU-Petition
---------------------------------------------------
* https://info.endthecageage.eu/faqs-de-DE/
    * Schöne Graphik: "Wie viele Nutztiere werden in der Europäischen Union in Käfigen gehalten?", inkl. Aufteilung cage und cage-free

Filme
-----
* The Superior Human? (2012), 80%
    * https://www.themoviedb.org/movie/366208-the-superior-human
    * https://www.youtube.com/watch?v=mqT82oGeax0
* A River of Waste: The Hazardous Truth About Factory Farms (2009), 50%
    * https://www.themoviedb.org/movie/209928-a-river-of-waste-the-hazardous-truth

Dokus
-----
### Westlich
* ["Die Macht der Konzerne | Doku | ARTE"](https://www.youtube.com/watch?v=1TpcpvzSkzM), F 2016, 90 min
    * ...

### Welt
* ["Bangladesch: Im Bordell von Daulatdia | ARTE Reportage"](https://www.youtube.com/watch?v=nbmtIAHCIgY), 2017, 25 min
    * ganzer Stadtteil
    * ...
    * Oradexon, Medikament eigentlich für die Tiermast gedacht, wird schlanken Frauen empfohlen, damit sie fülliger werden
    * ...

Frage: Kann man Fleisch von alten Tieren essen?
-----------------------------------------------
* ["Darf Fleisch von Tieren die an Altersschwäche gestorben sind in Deutschland verkauft werden?"](https://www.gutefrage.net/frage/darf-fleisch-von-tieren-die-an-altersschwaeche-gestorben-sind-in-deutschland-verkauft-werden), 2012
    * "Such mal nach "Txogitxu", das ist Fleisch von sehr alten Kühen die nah am Ende der natürlichen Lebenserwartung waren, ca. 17 Jahre."
* [Artikel über Txogitxu](http://schlaraffenwelt.de/die-fette-alte-kuh-das-beste-fleisch-der-welt-ein-erfahrungsbericht/), 2015

Frage: Was ist ein Hybrid-Huhn?
-------------------------------
* https://www.demeter.de/sites/default/files/richtlinien/demeter-richtlinien_erzeugung_gefluegelhandbuch.pdf
    * "In der ökologischen Legehennenhaltung werden momentan überwiegend die gleichen Hybridherkünfte wie in der konventionellen Landwirtschaft eingesetzt."
    * "Züchtungsziel ist das Zweinutzungshuhn. Für die wirtschaftliche Hühnerhaltung ohne Kükentöten braucht es geeignete Zweinutzungsrassen, Linien oder Gebrauchskreuzungen mit ausreichender Fleisch- und Legeleistung. Hybriden sind nicht generell abzulehnen, aber die Züchtung gehört in gemeinnützige Hand."
        * "Das Lohmann Dual Huhn ist ein Hybrid-Zweinutzungshuhn."
    * Listen mit Hühnerrassen
    * "Linienzucht, Linienkreuzung, Hybridzüchtung - Linien sind getrennt vermehrte Zuchtstämme innerhalb einer Rasse. Werden solche Linien gekreuzt, weisen die Kreuzungen eine höhere  Leistungsfähigkeit auf als die Elterntiere. Dieser so gennannte Heterosis-Effekt [...] ist größer bei weniger eng verwandten Linien."
        * "Der größte Vorteil der Gebrauchskreuzungen oder auch Hybridhühner ist eindeutig die hohe Legeleistung."
        * "Somit kann auch im Ökolandbau mit Hybridhühnern eine hohe Wirtschaftlichkeit erreicht werden"
        * "Ein Nachteil der Hybridzüchtung ist, dass die Linienzüchtung unter konventionellen Bedingungen stattfindet."
            * "Die Züchtung findet jedoch weiterhin unter konventionellen Bedingungen in Käfigen und mit künstlicher Besamung statt."
    * Rassehühner
        * ... "Nachteilig für die Wirtschaftlichkeit ist die geringere Legeleistung im Vergleich zu Hybridhennen"
        * "Die Legepersistenz vieler Rassetiere ist zudem nicht so stabil wie bei den Hybridhennen, eine spontane Mauser sowie Leistungsabfall nach den ersten 4 Monaten von max. 70 % auf 50 % ist üblich."
        * "Zudem ist auch die Eigewichtsentwicklung häufig heterogen und es muss mit einem vermehrten Anfall von S Eiern und später aufgrund geringerer Schalenfestigkeit vermehrt mit Brucheiern gerechnet werden."
        * "eventuell ungünstig für die Vermarktung"

Frage: Wieviel Fleisch fressen die Katzen in Deutschland?
---------------------------------------------------------
* (Zusammenfassung: Haustiere fressen längst nicht mehr nur, das was übrig bleibt, sondern stehen in direkter Konkurrenz zur menschlichen Nahrungsversorgung)
* (Zusammenfassung: Wenn die Menschen weiterhin fleischfressende Haustiere halten, könnten problemlos alle vegan essen)
* Warum die Frage?
    * Hunde kann man vegan ernähren. Katzen nicht.
        * Pflanzenfresser-Haustiere (siehe unten): Kaninchen, Meerschweine oder Vögel
    * zu oben: Könnte man also nicht also das Fleisch von alt gestorbenen Landwirtschaftstieren nicht einfach an die Haustiere verfüttern?
* Zahlen: In Deutschland lebten im Jahr 2014 ca. 12 Millionen Katzen
    * https://talendo.ch/de/karriere-mag/2015/11/18/wie-viele-katzen-gibt-es-in-deutschland
* Zahlen: In den USA entfallen auf Hunde und Katzen 25 bis 30 Prozent des gesamten Fleischkonsums
    * https://www.derstandard.de/story/2000062228005/oekologischer-pfotenabdruck-eines-katzen-und-hundestaats-berechnet
        * "der gesamte Kalorienverbrauch der amerikanischen Katzen und Hunde dem der Bevölkerung Frankreichs"
        * http://www.scinexx.de/wissen-aktuell-21733-2017-08-04.html
* http://www.scinexx.de/wissen-aktuell-21733-2017-08-04.html
    * "Die rund 163 Millionen Hunde und Katzen in den USA konsumieren jährlich so viele Kalorien wie die gesamte Bevölkerung Frankreichs – oder wie 60 Millionen Amerikaner."
        * Grobe Rechnung: Amerika 320 Mio Einwohner, Deutschland 80 Mio Einwohner
            * 80 / 320 * 60 = 15
            * Das heißt, wenn 15 Millionen Menschen in Deutschland vegan essen würden, dann könnte man davon alle Hunde und Katzen in Deutschland ernähren.
    * Tierfutter-Problem
        * "Denn dieses enthält keineswegs nur die Fleischabfälle, die der Mensch ohnehin nicht essen könnte und die sozusagen übrig sind."
        * "Stattdessen ist gerade bei teureren Marken oft Fleisch enthalten, das problemlos auf unseren Tellern landen könnte"
        * "Konkurrenz zur menschlichen Nahrungsversorgung"
        * "Hund und Katze tragen dadurch dazu bei, dass zusätzliche Hühner, Schweine oder Rinder geschlachtet werden müssen."
        * "Ein Hund muss kein Steak fressen, er kann genauso gut Tierprodukte fressen, die für einen Menschen nicht verwertbar sind"
    * "Eine weitere Möglichkeit wäre es natürlich, einfach mehr Pflanzenfresser als Haustiere zu halten – Kaninchen, Meerschweine oder Vögel. "
* http://www.deutschlandfunkkultur.de/josef-h-reichholf-haustiere-die-unbekannten-lebewesen-daheim.950.de.html?dram:article_id=406078
    * "Trotzdem ist der Fleischbedarf von Hunden und Katzen riesig. Allein in den USA verschlingen sie so viel Fleisch wie die französische Bevölkerung. Und dennoch bricht Josef H. Reichholf für die Haustiere eine Lanze, weil sie für das seelische Wohlbefinden vor allem einsamer Menschen wichtig sind.

Medien
------
* RT, ehemals Russia Today: https://de.wikipedia.org/wiki/RT_(Fernsehsender)

Bio
---
### Ökologische und konventionelle Landwirtschaft - Ökolandbau: Ist „Bio“ wirklich nachhaltiger?
* https://www.laborpraxis.vogel.de/oekolandbau-ist-bio-wirklich-nachhaltiger-a-702936/
    * "Ökolandbau ist kein Leitbild für global nachhaltige Landwirtschaft"
    * ABER: "Eben so wenig kann die industrielle Landwirtschaft mit ihrem hohen Einsatz von Chemikalien als Modell für Nachhaltigkeit dienen."
    * Anmerkung:
        * also doch Ideen vom Ökolandbau abschauen?
    * Gleiche Studie
        * https://biooekonomie.de/nachrichten/oekolandbau-allein-reicht-nicht-aus
        * ["nicht das Patentrezept"](https://www.bioland.de/im-fokus/meinung/detail/article/nein-der-oekolandbau-ist-nicht-das-patentrezept.html)
    * http://www.biodiversity.de/produkte/interviews/Matin_Qaim, 2018
        * einige interessant Thesen, aber auch Aussagen, die stutzig machen
            * "Erstens, eine rein vegane Ernährung ist ernährungswissenschaftlich nicht zu empfehlen." --> FALSCH
            * "Drittens, die Biolandwirtschaft ist auf organischen Dünger angewiesen, der vor allem aus der Tierhaltung kommt. Dieser Dünger würde aber wegfallen, wenn alle Menschen Veganer wären, denn dann gäbe es auch keine Tierhaltung mehr."
                * Was ist mit der Übergangslösung: Tiere für Dünger, aber ohne Schlachten?
                * Oder siehe Schmitz: menschliche Exkremente für eine Kreislaufwirtschaft?
    * Schöne Gegendarstellung: https://www.heise.de/tp/features/Kann-der-Oeko-Landbau-Europa-ernaehren-4313239.html?seite=all
* https://www.bauernverband.de/wie-nachhaltig-ist-der-oeko-landbau
    * ?
* siehe auch "Kann Bio die Welt ernähren?"
