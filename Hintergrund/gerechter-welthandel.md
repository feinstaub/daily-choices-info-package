Freihandelsabkommen
===================

<!-- toc -->

- [2020](#2020)
  * [Netzwerk Gerechter Welthandel](#netzwerk-gerechter-welthandel)
- [Problembewusstsein](#problembewusstsein)
  * [Inbox 2019](#inbox-2019)

<!-- tocstop -->

2020
----
### Netzwerk Gerechter Welthandel
* https://www.gerechter-welthandel.org/mitglieder/
    * BUND, GLS, ...
* entstanden aus gegen CETA und TTIP
* umfassendes Material zu CETA: https://www.gerechter-welthandel.org/material/ceta/

Problembewusstsein
------------------
### Inbox 2019
* CETA
    * https://www.ceta-im-bundesrat.de/Offener_Brief_an_die_GRUENEN
    * https://www.ceta-im-bundesrat.de
    * Mail
        * "Die Fleischproduktion und den internationalen Fleischhandel zu steigern, ist Ziel des CETA (EU-Kanada) wie auch des JEFTA (EU - Japan) und des zZ verhandelten Freihandelsabkommens mit Mercosur (EU - Lateinamerika)."
            * https://www.tagesschau.de/wirtschaft/freihandel-eu-mercosur-101.html, 2019
                * "Freihandel mit Mercosur - Industrie euphorisch, Bauern wütend"
    * https://www.abl-ev.de - Arbeitsgemeinschaft bäuerliche Landwirtschaft e.V. (AbL)
        * 2019: Kurzfilm zu den Folgen billiger Milchexporte
            * https://www.abl-ev.de/apendix/news/details/?tx_ttnews%5Btt_news%5D=2354&cHash=aff61831628868a79ddb5dd85032b44c
            * "Die Stellschrauben der Europäischen Agrarpolitik (GAP) führen zu einer Intensivierung in der Tierhaltung in Deutschland. Die deutsche Bundesregierung treibt diese Politik massiv voran. Besonders hart betroffen sind Milchhöfe, aber auch schweinehaltende Betriebe."
        * 2019: "Gute GRÜNdE gegen CETA" Offener Brief
        * 2017:
            * "Europäische und kanadische Studie kritisiert CETA - Anlässlich der in Kürze anstehenden Entscheidungen über das Freihandelsabkommen CETA (Comprehensive Economic and Trade Agreement) beleuchten kanadische und europäische ExpertInnen der Zivilgesellschaft in einer neuen Studie die umstrittensten Aspekte des Abkommens. Sie kommen zu dem Schluss, dass das Abkommen in seiner derzeitigen Form das Allgemeinwohl beider Seiten des Atlantiks gefährdet. Die Studie benennt u.a. Gefahren im Bereich Konzernklagerechte, Landwirtschaft und Energiepolitik."
                * https://power-shift.de/
        * "Stand: August 2016 - Hintergrundpapier - CETA – Der Versuch Landwirtschaft weiter zu globalisieren, bäuerliche
Märkte zu zerstören und Gentechnik hoffähig zu machen"
            * "[...] Denn der sicherste Schutz unserer Standards und unserer bäuerlichen Landwirtschaft ist kein CETA, kein TTIP und keine anderen schädlichen Handelsabkommen. Die Agrarpolitik muss dringend das Höfe-Sterben stoppen. Statt Exportorientierung braucht es eine Qualitätsoffensive. Das bedeutet artgerechte Tierhaltung und gentechnikfreie Fütterung, Stärkung des Anbaus von heimischen
Eiweißfutter, das Recht auf Nachbau von Saatgut, keine Gentechnik durch die Hintertür – weder durch neue Gentechnik-Verfahren oder Aufweichen der Nulltoleranz, eine Reduzierung des Pestizid- und Mineraldüngereinsatzes und vieles mehr."

* JEFTA
    * Mail
        * "Beim JEFTA stimmte 2018 nur die Hälfte der Grünen im EU Parlament gegen dieses Freihandelsabkommen"
    * https://www.topagrar.com/management-und-politik/news/freihandelsabkommen-mit-japan-eu-wird-vor-allem-bei-schweinefleisch-profitieren-9565262.html
    * "„Es wird deutlich, dass die EU ihren Handel mit Japan intensiviert und im Agrar- und Ernährungsbereich Produktionssteigerungen zu erwarten sind“, sagt Agrarökonomin Dr. Janine Pelikan vom Thünen- Institut für Marktanalyse. In der EU werde die Produktion am stärksten im Sektor Schweinefleisch steigen."
    * "In Deutschland wird der Export von Milchprodukten größere Bedeutung haben. Hier könnte die Produktion um bis zu 0,5 % steigen."
