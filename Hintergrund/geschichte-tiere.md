Geschichte Tiere
================

<!-- toc -->

- [Historische Beispiele von Ausrottungen von Tierarten durch den Menschen](#historische-beispiele-von-ausrottungen-von-tierarten-durch-den-menschen)
  * [Wandertaube](#wandertaube)
  * [Dodo](#dodo)
  * [Insekten](#insekten)
  * [Sonstige](#sonstige)
  * [Aktuelles Artensterben](#aktuelles-artensterben)
  * [Aktuelle Ausrottung von Arten: Das Pangolin](#aktuelle-ausrottung-von-arten-das-pangolin)

<!-- tocstop -->

Historische Beispiele von Ausrottungen von Tierarten durch den Menschen
-----------------------------------------------------------------------
### Wandertaube
* ["Von Milliarden auf Null: Vor 100 Jahren starb die Wandertaube aus"](http://www.spektrum.de/news/von-milliarden-auf-null-vor-100-jahren-starb-die-wandertaube-aus/1304131), 2014
    * "Im 19. Jahrhundert waren Wandertauben wahrscheinlich die häufigsten Vögel der Erde - wenige Jahrzehnte später starben sie aus."
    * "Die Luft war im wahrsten Sinne mit Tauben gefüllt. Das mittägliche Licht beschattet wie bei einer Sonnenfinsternis. Kot regnete vom Himmel, ein bisschen so wie schmelzender Schnee. Und das kontinuierliche Rauschen der Schwingen lullten meine Sinne ein … Noch vor Sonnenuntergang erreichte ich Louisville, 55 Meilen von Hardensburgh entfernt. Die Tauben zogen immer noch in unverminderter Zahl – und das dauerte noch drei weitere Tage an."
    * "Vogeljagd | Riesige Vogelschwärme verdunkelten im 19. Jahrhundert den Himmel im US-amerikanischen Nordosten – und erleichterten den Jägern den Abschuss der Wandertauben."
    * "Einen weiteren und letztlich fatalen Schub erlebte die Wandertaube, als die Europäer die Neue Welt eroberten und dabei die einheimische Bevölkerung durch Krieg und Krankheiten dezimierten."
    * "Ausweichräume für die Taubenschwärme verschwanden, ihre Brutkolonien wurden leichter zugänglich und durch Straßen und Eisenbahnlinien erschlossen – und das erlaubte plötzlich die industrielle Verwertung der Tiere. Millionenfach wurden sie abgeschossen und das Fleisch zu den Ballungszentren an der Küste transportiert. Ein einzelner Jäger soll tatsächlich in einem einzigen Jahr – 1878 – drei Millionen Vögel vermarktet haben."
* https://de.wikipedia.org/wiki/Wandertaube#Bestandsentwicklung_und_Aussterben
    * "Die Wandertaube ist für die menschliche Ernährung exzessiv bejagt worden. Im Jahr 1855 verkaufte ein Händler an einem einzigen Tag 18.000 Stück. Der Vogelmaler John James Audubon (1785–1851) hat das massenhafte Töten bei der Jagd auf die Tiere beschrieben"

### Dodo
* https://de.wikipedia.org/wiki/Dodo#Aussterben

### Insekten
* wegen Pestizid-Einsatz und Landschaftsausräumung

### Sonstige
* ["Diese Tiere wurden vom Menschen ausgerottet"](http://www.tierfans.net/p/hdejqp/diese-tiere-wurden-vom-menschen-ausgerottet/3)

### Aktuelles Artensterben
* wegen Landwirtschaft: ["Humans are driving one million species to extinction"](https://www.nature.com/articles/d41586-019-01448-4), 2019
    * "Landmark United Nations-backed report finds that agriculture is one of the biggest threats to Earth’s ecosystems"
* [WWF-Bericht: Schwere Zeiten für Koala und Seepferdchen](http://www.faz.net/aktuell/gesellschaft/tiere/wwf-das-groesste-artensterben-seit-dem-ende-der-dinosaurier-15359814.html), 2017
    * "Mehr als 25.000 Tier- und Pflanzenarten sind laut einer WWF-Jahresbilanz bedroht – vor allem vom Menschen."
    * Genannt sind auch einige wenige positive Entwicklungen. So richtig Grund zur Freude scheint nicht aufzukommen.
* [Liste der neuzeitlich ausgestorbenen Säugetiere](https://de.wikipedia.org/wiki/Liste_der_neuzeitlich_ausgestorbenen_S%C3%A4ugetiere)
* [Liste ausgestorbener Tiere und Pflanzen](https://de.wikipedia.org/wiki/Liste_ausgestorbener_Tiere_und_Pflanzen)
* siehe auch Planetare Grenzen (Planetary Boundaries)
* siehe auch Bio

### Aktuelle Ausrottung von Arten: Das Pangolin
* z. B. 2019: https://www.tagesschau.de/ausland/pangolin-111.html
