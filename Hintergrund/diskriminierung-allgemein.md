Diskriminierung
===============

<!-- toc -->

- [Material zur Bewusstseinsbildung](#material-zur-bewusstseinsbildung)
  * [CosmicSkeptic](#cosmicskeptic)
- [Gary Yourofsky](#gary-yourofsky)
  * [Main Speech](#main-speech)
  * [Getting involved, one thing we need to have in common: Peace](#getting-involved-one-thing-we-need-to-have-in-common-peace)
  * [Andere Ausschnitte - one group thinks they are special - warum töten nicht ok ist](#andere-ausschnitte---one-group-thinks-they-are-special---warum-toten-nicht-ok-ist)
- [Definition Diskriminierung](#definition-diskriminierung)
- [The Freedom Riders History (1961)](#the-freedom-riders-history-1961)
- [Die Göttliche Ordnung - Diskriminierung nach Geschlecht](#die-gottliche-ordnung---diskriminierung-nach-geschlecht)
- [Mehr](#mehr)
  * [Intersektionalität](#intersektionalitat)
  * [Angst vor Gerechtigkeit](#angst-vor-gerechtigkeit)
  * [Kleine Geschichte der Diskriminierung](#kleine-geschichte-der-diskriminierung)
  * [Antisemitismus](#antisemitismus)
  * [Alltagsrassismus](#alltagsrassismus)
  * [Umweltrassismus](#umweltrassismus)

<!-- tocstop -->

::gerechtigkeit

Material zur Bewusstseinsbildung
--------------------------------
* Kurzes
    * Für junge Menschen: https://www.menschenrechte.jugendnetz.de/material-projektideen/glossar/diskriminierung/
    * "the norm", siehe dort
* Längeres
    * Frauenrechte: Die göttliche Ordnung (Berufswahl, Wahlrecht)
    * Rassentrennung (...)
    * Es gibt doch offensichtliche Unterschiede (nur halt keine relevanten)
    * Analogie zu Haus- und Nutztieren
* Respekt
    * Analogie: eine Person respektieren und dafür "nur" einmal die Woche schlagen (anstelle täglich)
        * das war früher ok, aber heute würde man sagen: gar nicht mehr
        * (Fr.witze)
        * analog Tiere einmal die Woche essen
* **Lehren**
    * Was lehren uns die überwundenen Missstände aus der Vergangenheit und aktuelle Missstände in anderen Gesellschaften?
        * => 1. Wir waren damals in diesen Punkten auch nicht so weit.
             2. Selbstkritisch prüfen, ob vielleicht aktuelle Punkte vorhanden sind, die wir noch nicht erkannt haben
                (siehe systemischer Rassismus, Sexisms, Speziesismus)

### CosmicSkeptic
Alex O'Connor

* https://www.youtube.com/watch?v=C1vW9iSpLLk - "A Meat Eater's Case For Veganism", 2019, 25 min
    * (made a good deal of yt commentors think or go vegan)
    * Buch_: Peter Singer, Animal Liberation
    * Short Story: Ursula Le Guin, The Ones Who Walk Away From Omelas, 1973
        * todo: https://libcom.org/files/ursula-k-le-guin-the-ones-who-walk-away-from-omelas.pdf
        * https://de.wikipedia.org/wiki/The_Ones_Who_Walk_Away_From_Omelas
            * "Die Zentralidee, die des „Sündenbocks“, der für das Glück der Vielen leiden muss"
            * "Der Text wird oft in Lehreinrichtungen verwendet und dann entweder als Kritik an der „ersten Welt“ präsentiert,
            die, in ihrem Wohlstand, das ferne Leid anderer billigend hinnimmt, oder als Warnung vor einer utilitaristischen Sicht des Menschen"
    * Überwindung des Rassismus: **Schwarze und weiße Menschen haben nicht deswegen gleiche Rechte, weil sie in allen Aspekten gleich sind.**
        Es gibt deutliche Unterschiede: Hautfarbe, kultureller Hintergrund, sonstiges in der DNA.
        **Sie haben deswegen gleiche Rechte, weil es KEINE Unterschiede in den RELEVANTEN Aspekten gibt.**
        => Tiere: sind eine andere Spezies und dies ist auch kein relevanter Unterschied, wenn es darum geht, ob wir sie für unseren Geschmack töten sollten
    * Menschen höheren moralischen Wert? -> ja, z. B. weil sie viel weiter in die Zukunft planen können
        * Beispiel: Factory farming mit Hühnern läuft ganz anders ab, als wenn man dort Menschen zur Fleischproduktion verwenden würde. Weil letztere wissen sehr viel deutlicher was los ist.
        * (Was ist eigentlich ein moralischer Wert?)
    * **Analogien**, Beispiel einsame Insel: du und ein Mensch und ein Schwein. Wen würdest du essen? Aha, also ist Schweinehaltung für Essen ok.
        * zweite Situation: du und ein junger Mensch und ein alter schwacher Mensch... Aha, man kann also...
        * Menschen können gerne höheren moralischen Wert haben als Schweine,
            aber das allein rechtfertigt noch nicht Töten und Essen der Schweine in der Situation, in der wir uns im Alltag befinden
            * dazu Situation: entscheide zwischen 'dem Retten eines Schafes' und 'einem Kind ein Stück Schokolade geben'
                => **Wenn nun einer das Schaf rettet heißt aber nicht, dass er das Schaf höher wertet als das Kind.**
                **Aber es heißt: das Leben des Tieres ist mehr wert als die kurzfristige Freude des Menschen.**
        * Andere Versionen:
            * Gerade dabei dem Kind, was bei der Mutter steht, ein Eis zu kaufen. Wenn das Eis nicht rechtzeitig
                geliefert wird, wird das Kind weinen. Gleichzeitig entsteht eine Situation, wo ein Schwein eingeklemmt
                ist und Sie - wenn Sie Sie jetzt handeln - das Schwein retten können. Was würden Sie tun?
                Wenn Schwein retten, bedeutet das, dass für Sie Schweine wichtiger sind als Ihr eigenes Kind? (nein)
            * Rassismus:
                * Eine Gruppe von ausländischen Flüchtlingskindern ist gerade bereit, die von einer Radio-Aktion
                    organisierte Feuerwehrtruppe in Empfang zu nehmen, die eine Menge von Eis mitgebracht hat.
                    Plötzlich werfen die Feuerwehrleute das Eis auf den Boden und rennen weg. Um einen Brand
                    in einem Mehrfamilienhaus zu löschen, der gerade gemeldet wurde.
                    Ist die Feuerwehr rassistisch? (nein)

* Video: https://www.youtube.com/watch?v=gcVR2OVxPYw - "It's Time To Go Vegan | Animal Rights Speech 2019 | Alex O'Connor, Tel Aviv", 2019, 28 min

* https://www.youtube.com/watch?v=9wRknbX8d-M - "Are You Worth More Than a Tree? Dennis Prager Response" ::cdugen

* https://www.youtube.com/watch?v=VhcOPICugz4 - "Rationality Rules – Debunked"
    * can morality be derived out of rational thinking as pure facts, this is objective?
    * or is morality entirely subjective and has no basis an analytical and empirical facts?
    * Hume: you cannot come from "is" statements to an "ought" statement
    * do "axiomatic oughts" exist?

Gary Yourofsky
--------------
### Main Speech
* "It is NEVER OK to be picking and choosing which forms of discrimination to be opposed to... [...]
    Discrimination is evil on its foundation...Or it is not. We cannot have this one both ways, it doesn't work like that."

https://dotsub.com/view/5de97c21-1363-4574-a9ee-37f492ecea3e/viewTranscript/eng

    And those blinders are on nice and tightly,
    but if you give me an open mind today, that's all I ask for, an open mind,
    I'm gonna take your blinders right off!
    My goal is simple. All I want to do is re-connect people with animals.
    Awaken some emotions and some feelings and some logic, that is been buried and suppressed, intentionally, by our society.
    And the reason why I say "re-connect" it's because each and every person in this room
    used to be a real animal rights person at one time,
    a true animal lover, and a real friend to the animal kingdom.
    And it's when we were kids!
    When we were young...When we were kids...Man!...We used to be in awe of animals.
    They used to make us laugh, and giggle and smile.
    They made us pretty happy!
    And there was a time in our lives, when we would do just about anything in the world to make THEM happy as well.
    To protect them from cruelty!
    Or to, at least, ACKNOWLEDGE the cruelty they were receiving.
    I mean, if somebody was mean to an animal in front of us when we were little,
    we would have screamed and cried.
    And that's because we all used to understand right from wrong, when it came to the treatment of animals.
    Until somebody /told/ us, and taught us differently.
    You better believe that somebody told us to ignore their suffering!
    To MOCK and excuse, their pain, and their misery.
    To make fun of their very existence.
    And this is something I want you to focus on - today, tomorrow and beyond...
    What in the hell happened along the way?!
    Who taught us to be so mean, and nasty and vicious and hateful,
    or indifferent towards animals when they used to be our friends?
    These are innocent beings, who have done nothing wrong to us.

    Because I'm pretty sure, we can all agree on at least one thing right now...
    That hatred, in its purest form, is a learned behavior.
    Racism. Sexism. Heterosexism. Antisemitism. Misogyny...
    These are all learned behaviours! When kids are 2, 3, 4 years old, playing on a playground
    they couldn't care less about the color of their friends' skin or their religious background.
    I don't think there is any doubt, that hatred, in its purest form, is learned.
    So species-ism is no different.
    That's going to be a new word to a lot of people, it's up here on the screen, below the word "vegan"
    is the word 'species', with an -ism attached to it,
    and I want to define this word as the unethical, unprincipled point of view,
    that the human species has every right to exploit, enslave and murder another species.
    And all because we believe that our species is so more special,
    so more superior than the other ones,
    that we're the only ones that count, and we're the only ones that matter.

    Now, correct me if I'm wrong,
    but that line of thinking, that thought process,
    that is the basis of all forms of discrimination.
    One group saying and thinking that they're more special than everyone else,
    and they proceed to exploit them, oppress them, denying them their right to be free.
    They treat them like property, they enslave them in many cases,
    and in many other cases they murder them with premeditation, and without penalty.

    And understand something essential about discrimination!
    It is NEVER OK to be picking and choosing which forms of discrimination to be opposed to...
    And which ones to say are evil: Racism...
    And which ones to say are okay: Speciesism.
    Discrimination is evil on its foundation...Or it is not.
    We cannot have this one both ways, it doesn't work like that.

    I want to ask you, to use some empathy right now.
    And when I say 'empathy', what I'm saying is: place yourself in the position of the animals,
    and start to view this issue from the animals' point of view.
    From the victims' point of view.
    When you examine any form of injustice, whether humans are victims or animals are victims,
    please remember the victim's point of view.
    If you are not the victim, don't examine it entirely from your point of view
    because when YOU'RE not the victim, it becomes pretty easy to rationalize and excuse cruelty,
    injustice, inequality, slavery, and even murder.
    But when you're the victim,
    things look a lot differently from that angle.

### Getting involved, one thing we need to have in common: Peace
    I'm an activist,
    root word is 'active'.
    I've been banned from 5 countries so far, and arrested 13 times,
    for random acts of kindness and compassion, on behalf of my animal brothers and sisters,
    if you want to read up about that, check out my website.
    And today, I would love to give you a chance to actually do something, and truly get involved!
    Because I understand that a lot of people want to get involved, honestly I do.
    But putting a "Coexist" bumper sticker on your car,
    wearing a "What would Jesus do?" bracelet,
    or sporting a "Peace and Love and Sunshine" t-shirt:
    That is not "getting involved"!

    I understand that we are all on a journey in life.
    We all have different likes and dislikes,
    different nationalities and religions too,
    but there is one thing that we need to have in common with each other, and that's peace!
    Genuine compassion and genuine peace for our planetary companions!
    Contrary to political and religious dogma, animals do not belong to us.
    They are not commodities! They are not property! And they are not inanimate, stupid objects, who can't think and feel!
    That Descartes' Cartesian way of looking at animals,
    like they're machines...
    It is outdated, and quite frankly, 100% insane.
    Because, if we all understand that animals use their eyes to see,
    ears to hear,
    noses to smell,
    mouths to eat,
    legs to walk,
    feathers to fly,
    fins to swim,
    genitalia to procreate,
    bowels to defecate,
    I'm always perplexed that most people don't believe that they can also use their brains to think,
    feel,
    be rational, be aware and be self-aware!
    Am I supposed to believe, that every body part of an animal functions just like it's supposed to,
    except the brain?

    Those lies are thick.
    The propaganda from the animal abusers is enormous!
    I mean, when was the last time you turned on TV and saw a commercial for shiitake mushrooms?
    People singing and dancing down the streets, having a good time eating mushrooms?
    How about alfalfa spouts?
    ...

### Andere Ausschnitte - one group thinks they are special - warum töten nicht ok ist
* 2020
    * "Yourofsky Promoting Intersectionalism", 5 min, 2002, 2014
        * ... todo ...
    * "Gary Yourofsky - Rwandan Genocide", 2 min
        * people are being taught if you don't kill them they will kill you
            => people hacking each other to death
            => mankind can be the most vicious and brutal creature on earth
        * ruanda, darfur, holocaust, slavery, apartheid, armenia, serbia, the inquisition, the crusades
            * there is an endless list of atrocities that we have committed against each other
            * one reason: **one group thinks they are more special**, more superior and more important than the other group
            * discrimination is always wrong
            * "**the moment somebody oks discrimination** is the moment somebody oks oppression, exploitation, slavery and murder"
            * ((siehe diskriminierung-speziesismus.md - Ist Speziesismus wirklich analog zu Rassismus und Sexismus zu sehen?
                * ein Aufsatz dort sagt: nein, weil letztere Ungerechtigkeiten darauf basieren, dass jemand meint, Menschen hätten eben nicht gleiche Rechte oder so))
    * "Gary Yourofsky - Condemning Sexism and Rape", 4 min
        * on PETA
        * going to jail for justice, him and people in history
            * 1.000 dollar bail for sexually harrassing a woman
            * 10.000 dollar bail for freeing rats
        * fur and rape
        * in the past: kill an animal nicely. Isn't that nature? Isn't that ok?
            * meet a woman in the bar...
            * the act of rape is evil, the act of murder is evil, you cannot do it in a nice way
            * !!! (das könnte man auch für "nutzlose" Labortiere oder Hunde im Tierheim anwenden, die keiner haben will
                => **so human wie möglich töten; warum ist das dort nicht ok?**)

Definition Diskriminierung
--------------------------
* https://de.wikipedia.org/wiki/Diskriminierung
    * "bezeichnet eine Benachteiligung oder Herabwürdigung von Gruppen oder einzelnen Personen nach Maßgabe bestimmter Wertvorstellungen oder aufgrund unreflektierter, z. T. auch unbewusster Einstellungen, Vorurteile oder emotionaler Assoziationen."
* https://www.kindersache.de/bereiche/wissen/natur-und-mensch/diskriminierung-was-bedeutet-das
    * "Das Wort Diskriminierung kommt aus dem Lateinischen und bedeutet übersetzt „Unterscheidung“. Diskriminierung beschreibt also die unterschiedliche Behandlung [...]. Die Benachteiligung [...] kann auf verschiedene Eigenschaften beruhen. Besonders häufig werden [...] aufgrund ihres Geschlechts, ihrer Hautfarbe, ihrer Herkunft, ihrer Religion oder ihres Alters diskriminiert. Hierbei gibt es meist eine Gruppe [...], die diskriminiert wird, und eine Gruppe [...], die dadurch Vorteile hat."
    * "Diskriminierung beruht meist auf Vorurteilen. Vorurteile sind Annahmen, die man von einer bestimmten [...]gruppe hat, meist sogar ohne [...] zu kennen. Dass Menschen Vorurteile haben ist ganz normal. Es ist aber wichtig zu wissen, dass diese ganz oft nicht stimmen und schon gar nicht für eine ganze Gruppe."

The Freedom Riders History (1961)
---------------------------------
* siehe -rassismus.md
    * Martin Luther King Jr.
    * Freedom Riders
    * Bernard Lafayette Jr. !
    * Selma
    * Mighty Times: The Children's March
    * Injustice anywhere...
    * Analogie von Extinction Rebellion

Die Göttliche Ordnung - Diskriminierung nach Geschlecht
-------------------------------------------------------
* Frauenrechte, Film
* Bibel-Stelle
* Gewalt an Frauen in heutiger Zeit

Mehr
----
### Intersektionalität
* https://de.wikipedia.org/wiki/Intersektionalit%C3%A4t
    * "führen zu eigenständigen Diskriminierungserfahrungen.
        So wird ein gehbehinderter Obdachloser gegebenenfalls nicht nur als Obdachloser und als Gehbehinderter diskriminiert,
        sondern er kann auch die Erfahrung machen, als gehbehinderter Obdachloser diskriminiert zu werden."
    * Ende 1960er Jahre USA: "Die besondere Situation Schwarzer Frauen war aufgrund von rassistischer Diskriminierung kaum wahrgenommen worden."

* "„Niemand kann behaupten, von Rassismus nicht betroffen zu sein“", 2020
    * https://www.deutschlandfunk.de/diskriminierung-niemand-kann-behaupten-von-rassismus-nicht.694.de.html?dram:article_id=479059
    * "Emilia Roig vom Center for Intersectional Justice appelliert an die Gesellschaft, Rassismus als kollektives Phänomen anzuerkennen. Dabei gehe es nicht nur um die Betroffenen, sondern jeder müsse das eigene Denken und Handeln kritisch hinterfragen, sagte sie im Dlf. Die Bereitschaft dazu wachse."
    * ...
    * "Wir fokussierten uns aber auf die Seite der Betroffenen, die nach ihren Rassismuserfahrungen gefragt würden. Doch man könne genauso gut fragen: Wann haben Sie sich zuletzt rassisitisch geäußert oder wann haben Sie zuletzt Rassismus perpetuiert – auch unbewusst? „Absicht ist nicht relevant. Wir machen das auch täglich, ich auch. Wir sind so geprägt von diesen Vorstellungen, von den Darstellungen und von der Hierarchie, dass es uns allen passiert und das müssen wir erst mal akzeptieren.“"
    * "also nicht nur die Perspektive der Betroffenenen beleuchten, sondern auch die Perspektive der Menschen, die von System privilegiert sind"
    * "Sie begrüßt die Diskussionen über Polizeigewalt, über strukturellen und systemischen Rassismus und sieht aktuell die Bereitschaft, „ein bisschen tiefer in die Thematik reinzugehen und zu akzeptieren und ihre Egos ein bisschen zu dezentrieren."

### Angst vor Gerechtigkeit
* 2020
    * Wenn einer Gruppe, die vorher ungerecht behandelt wurde, Gerechtigkeit zuteil werden soll,
        dann empfindet die Gruppe, die vorher davor profitiert hat, Angst etwas zu verlieren.
        Bzw. auch die Unterdrückten haben möglicherweise Angst vor Veränderung (siehe die göttliche Ordnung).
        Diese Angst ist real, sollte aber überwunden werden, da am Ende - nach einer gewissen Übergangszeit - alle gewinnen.
        * Beispiel: Sklaverei -> alle fühlen sich wohler
        * Beispiel: Frauenwahlrecht -> gerechtere Gesellschaft (frag die Frauen)
        * Beispiel: Tierleben über Geschmack stellen -> neue, reichhaltige und gesunde Speisenkreationen auf Pflanzenbasis tun sich auf

### Kleine Geschichte der Diskriminierung
* ... todo ...wen-essen.odt

### Antisemitismus
siehe diskriminierung-antisemitismus.md

### Alltagsrassismus
siehe diskriminierung-rassismus.md

### Umweltrassismus
* https://de.wikipedia.org/wiki/Klimagerechtigkeit
    * https://de.wikipedia.org/wiki/Umweltgerechtigkeit
        * https://de.wikipedia.org/wiki/Umweltrassismus
