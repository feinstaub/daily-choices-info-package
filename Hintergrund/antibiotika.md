Antibiotika-Missbrauch
======================

<!-- toc -->

- [Allgemein](#allgemein)
- [Aktuelles](#aktuelles)
  * [2019](#2019)
- [Gesundheit](#gesundheit)
- [Situation in Europa / Deutschland](#situation-in-europa--deutschland)
- [Fischzucht](#fischzucht)
- [Vegan](#vegan)

<!-- tocstop -->

Allgemein
---------
* https://de.wikipedia.org/wiki/Antibiotikaresistenz
* Tieren in der Tierhaltung wird in großen Mengen - derzeit recht unkontrolliert - Antibiotika verabreicht

Aktuelles
---------
### 2019
* "**Riskanter Einsatz in der Geflügelmast**" - https://www.tagesschau.de/investigativ/ndr/antibiotika-landwirtschaft-101.html
    * "Mastschweine erhalten weniger Antibiotika als früher,
        **bei Geflügel werden jedoch weiter für Menschen wichtige Wirkstoffe in großer Menge eingesetzt**.
        Das geht aus einem bislang internen Bericht hervor, der NDR und SZ vorliegt."
    * "Betriebe nutzen möglicherweise Gesetzeslücken - [...] Demnach werden Kälber "recht häufig auf Sammelstellen oder bei Viehhandelsunternehmen antibiotisch versorgt und dann vorbehandelt in Mastbetriebe verbracht". Dort - in den Sammelstellen und den Handelsunternehmen - wird nicht erfasst, welche und wie viele Antibiotika die Tiere bekommen. Denn sie gelten offiziell nicht als Tierhaltungsbetriebe, da die Tiere dort weniger als einen Tag lang verbleiben. Deshalb unterliegen sie auch nicht der Mitteilungspflicht für Antibiotika-Einsätze."
    * "Große Betriebe geben mehr Antibiotika - Das Landwirtschaftsministerium hat für seinen Bericht auch die Größe der Betriebe untersucht. Das Ergebnis ist hier recht eindeutig. Insgesamt werden demnach Tiere in großen Betrieben häufiger mit Antibiotika behandelt als in kleinen und mittleren - egal bei welcher Tierart."

Gesundheit
----------
* 2017: http://www.tagesschau.de/ausland/supererreger-159.html
    * Doku: ["Der unsichtbare Feind – Tödliche Supererreger aus Pharmafabriken | Die Story im Ersten"](https://www.youtube.com/watch?v=mBog5xT5Ybk), 2017, 45 min
        * in Indien, beauftragt unter anderem von deutschen Markenherstellern
        * siehe auch Risiken und Nebenwirkungen mancher [Markenhersteller](marken-multinational.md]
        * ...
    * ["Wie entstehen multiresistente Keime?"](http://www.tagesschau.de/inland/multiresistentekeime-101.html), tagesschau 2017
        * inkl. FAQ
* Was ist der Nachteil von Massentierhaltung?
    * Antibiotika-Missbrauch -> https://de.wikipedia.org/wiki/Antibiotikaresistenz, siehe [Antibiotika](antibiotika.md)

Situation in Europa / Deutschland
---------------------------------
* 2018: https://albert-schweitzer-stiftung.de/aktuell/neues-antibiotikagesetz-eu
    * "EU-Kommission, Europäischer Rat und EU-Parlament haben sich nach acht Jahren Verhandlung auf ein neues Tierarzneimittelgesetz geeinigt. Es sieht einige wichtige Regelungen für den Gebrauch von Antibiotika vor – für Deutschland ändert sich allerdings wenig. Wir fassen das Problem der Antibiotikaresistenzen zusammen und geben einen Überblick über das neue Gesetz."

Fischzucht
----------
* siehe fisch-fischer-überfischung.md
    * z. B. Doku "Schützt Fischzucht in Aquakulturen vor Überfischung der Weltmeere?", 2011

Vegan
-----
* siehe https://de.wikipedia.org/wiki/Veganismus#Antibiotikaresistenzen
