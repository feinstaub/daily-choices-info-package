Wirksamkeit des Einzelnen
=========================

<!-- toc -->

- [Zur Wirksamkeit des Einzelnen](#zur-wirksamkeit-des-einzelnen)
  * [Bücher](#bucher)
  * [Albert Schweitzer](#albert-schweitzer)
- ["Philosophie der Nachhaltigkeit - Es ist nicht egal, was du tust"](#philosophie-der-nachhaltigkeit---es-ist-nicht-egal-was-du-tust)
- [Konkretes](#konkretes)
  * [Verdrängungsmechanismen, It's not me](#verdrangungsmechanismen-its-not-me)
  * [Psychologie zur Motivation zum Handeln](#psychologie-zur-motivation-zum-handeln)
- [Menschen aus der Geschichte](#menschen-aus-der-geschichte)
  * [Eigene Überzeugungen](#eigene-uberzeugungen)
  * [Handeln](#handeln)
- [Hintergrund: Psychologie](#hintergrund-psychologie)
- [Die Verantwortung des Einzelnen](#die-verantwortung-des-einzelnen)
  * ["Ein Intellektueller zu sein, ist eine Berufung für jedermann"](#ein-intellektueller-zu-sein-ist-eine-berufung-fur-jedermann)
  * [Die Verantwortlichkeit der Intellektuellen](#die-verantwortlichkeit-der-intellektuellen)
  * ["Was nützt ein Intellektueller?"](#was-nutzt-ein-intellektueller)
  * [Ausreden](#ausreden)
- [Beispiel Menschenrechte](#beispiel-menschenrechte)
- [Inbox](#inbox)
  * ["How Much Good Can One Person Really Do?", 2020](#how-much-good-can-one-person-really-do-2020)
  * [Haltung zeigen im Sport-Doping, 2019](#haltung-zeigen-im-sport-doping-2019)
  * [Stallman / Gandhi, 2019](#stallman--gandhi-2019)
  * [2018](#2018)

<!-- tocstop -->

::wirksamkeit-des-einzelnen

Zur Wirksamkeit des Einzelnen
-----------------------------
### Bücher
* Buch: [J. Weizenbaum - Kurs auf den Eisberg](http://www.zvab.com/advancedSearch.do?title=%22Kurs+auf+den+Eisberg%22) (1988) über die Verantwortung des einzelnen
    * Elevator-Pitch-Artikel: https://ohneamazon.wordpress.com/2014/01/31/die-sogenannte-ohnmacht-des-einzelnen-ist-vielleicht-die-gefahrlichste-illusion-die-ein-mensch-haben-kann/
    * "Vermeintliche Ohnmacht des Einzelnen"
* Buch: "Selbstverbrennung" von Hans Joachim Schellnhuber
    * sehr positiv trotz überwältigender Herausforderungen
* Buch: Komm, ich erzähl dir eine Geschichte [Dec 01, 2006] Bucay, Jorge and Harrach, Stephanie von
* Buch: [A. Schweitzer - Aus meinem Leben und Denken](http://www.fischerverlage.de/buch/aus_meinem_leben_und_denken/9783596128761) (vor 1965)
    * Zitat: "Kinder erzählt man Geschichten zum Einschlafen. Erwachsenen, damit sie Aufwachen."

### Albert Schweitzer
* https://albert-schweitzer-stiftung.de/ueber-uns/menschen/albert-schweitzer/zitate
    * »Und dann lassen sie sich [die Menschen] zu leicht mutlos machen durch die Überlegung, dass der Einzelne nichts tun kann, und kommen dann dahin, wo die meisten stehen, dass sie von all dem Elend nur nichts sehen und hören wollen; sie meinen, es besteht dann weniger, weil sie so leben, als wäre es für sie nicht da. Das ist falsch und feig. Hier vermag der Einzelne viel. Ich rede nicht davon, dass eigentlich jeder Mensch Mitglied im Tierschutzverein sein soll; denn was ist der Mindestbeitrag von einer Mark im Jahr, den dieser Verein erhebt und den die meisten unter uns trotz der schlechten Zeit erschwingen können, im Vergleich zu dem, was er an Belehrung und an Einfluss Gutes leistet! Über das, was der Einzelne ausrichten kann, täuscht man sich. Er vermag mehr, als man meint.«
    * »Nur wenig von der vielen Grausamkeit, die von Menschen geübt wird, kommt wirklich auf Kosten grausamer Triebe. Das meiste davon fließt aus übernommener Gewohnheit und aus Gedankenlosigkeit. Die Grausamkeit hat also keine sehr festen, aber weit verzweigte Wurzeln. Darum macht es so viel Mühe, sie auszurotten. Aber die Zeit muss kommen, wo die von Gewöhnung und Gedankenlosigkeit geschützte Nichtmenschlichkeit der vom Denken verfochtenen Menschlichkeit erliegen wird. Arbeiten wir daran, dass sie kommt.«

"Philosophie der Nachhaltigkeit - Es ist nicht egal, was du tust"
-----------------------------------------------------------------
* https://www.cicero.de/kultur/philosophie-der-nachhaltigkeit-es-ist-nicht-egal-was-du-tust/59026, Melissa Lane, 2015
    * "Die Folgen unseres Handelns sind nie zu vernachlässigen. Vor allem Kosten und Nutzen müssen im
        Übergang zu einer ökologisch nachhaltigen Welt radikal neu gedacht werden. Plädoyer für eine Philosophie der Nachhaltigkeit"
    * ...
    * "Vernachlässigbarkeit bedeutet: Die Handlungen jedes Individuums sind für das Gesamtresultat ohne erhebliche Auswirkungen.
        Heute wissen wir, dass im Fall des nordamerikanischen Bisons oder des Dodos auf Mauritius solche Einstellungen
        tragische Konsequenzen haben können. Myriaden einzelner Tötungen führten zur Auslöschung dieser Arten."
    * "„Negligibility“, Vernachlässigbarkeit"
        * "beschreibt die Annahme, dass jeder einzelne Akteur ein so unbedeutender Mitspieler ist, dass das,
            was er individuell tut, keine Auswirkungen auf die gesamte Gesellschaft hat"
    * "Daraus folgt, dass ich tun kann, was ich will, ohne dass ich mir um die gesellschaftlichen Auswirkungen Gedanken machen muss.
        Gleichzeitig bin ich allerdings auch zu machtlos, um irgendetwas in der Gesellschaft zu bewegen.
        Indem wir unsere Handlungen als vernachlässigbar einschätzen, betrachten wir uns selbst als ohnmächtig und wirkungslos.
        Und weil wir uns so sehen, erscheinen wir als unschuldig; unsere Emissionen, mitsamt allen unseren Handlungen, sind für das wirkliche Problem irrelevant."
        * "Aus diesem Blickwinkel entsteht eine Art moralischer Arbeitsteilung: Diejenigen, die für den Großteil der Wirkungen
            verantwortlich sind (wer auch immer es sein mag), sollen auch die Verantwortung tragen.
            Wir anderen sind aus dem Schneider, entschuldigt aufgrund der Vernachlässigbarkeit unserer unerheblichen Handlungen."
    * "Vernachlässigbarkeit erkauft sich seine Unschuld durch Ohnmacht."
    * "**Wenn wir jedoch Opfer der fatalen Illusion bleiben, dass individuelle Handlungen vernachlässigbar sind**,
        **macht uns das zynisch und ignorant gegenüber Bestrebungen, die sozialen Wandel zum Ziel haben**."
    * "Mit einer größeren moralischen und politischen Vorstellungskraft lassen wir diese Trägheit hinter uns."
        * ! "Sobald mir aus ethischen Gründen wichtig ist, wie ich handle, hören meine Handlungen auf, vernachlässigbar zu sein."
    * "Wenn mir meine Handlungen überhaupt wichtig sind, dann gilt das für meine Handlungen hier und jetzt."
        * "Aber ebenso wichtig ist es, mich um meine zukünftigen Handlungen und Einstellungen zu sorgen.
            Da meine gegenwärtigen Handlungen meine zukünftigen Handlungen prägen, habe ich einen Grund, mich um meine jetzigen Handlungen zu kümmern."
    * ...
    * "Das Gute, das wir täten, wenn wir gemeinsam handelten, ist genau genommen die Summe des individuell Guten, das wir auch alleine tun können."
    * Einwände
        * Kosten
            * "Vielleicht geht es hier weniger um materielle Opfer als um eine größere Vorstellungskraft und um den Mut,
                andere Dinge ins Zentrum des Lebens zu rücken."
            * ...
        * unfair - "Warum soll ich es tun, wenn du es nicht tust?“
            * "Während ich mich bei aufgezwungenen Pflichten dagegen wehre, ungleich behandelt zu werden,
                also der passive Gegenstand einer unfairen Handlung zu sein, **will ich bei freiwilligen Handlungen nicht der Dumme sein.**"
            * ...
            * "Je mehr intrinsische Gründe wir für ein Handeln haben, Gründe, die uns vor dem Hintergrund einer
                neuen moralischen Vorstellungskraft sinnvoll erscheinen, desto weniger wird uns der Einwand der Ungleichheit berühren."
            * "Deswegen sind individuelle Handlungen weder irrational noch sinnlos, selbst angesichts der weithin herrschenden
                moralischen Trägheit. Ganz im Gegenteil: Solche Initiativen – und vor allem Initiativen zur Veränderung der
                moralischen Vorstellungskraft – stellen einen wichtigen Weg dar, auf dem wir uns in eine nachhaltigere Zukunft begeben können."

Konkretes
---------
* Frage: Wenn nicht wir und jeder einzelne, wer sonst sollte anfangen?
    * mit weniger Fliegen, weniger Fleisch/Tierprodukte, 1x/Jahr den eigenen Abgeordneten anschreiben, siehe demokratie.md
* 2018: "(nur) 59% der Befragten weigern sich, Software für ein ethisch fragwürdiges Ziel zu entwickeln."
    * https://medium.com/swlh/highlights-from-stack-overflow-annual-developer-survey-2018-7aa1053a7668
        * "56.4% of professional developers contribute to open source"
        * "48.3% of developers prefer Linux"
        * "58.5% of respondents will refuse to write a code for an Unethical Purpose"
* COPIED: Vortragsscript: [Zur Käuflichkeit der Moral - Wie politischer Konsum zu einer ethischen Ökonomie beiträgt](http://www.haraldlemke.de/texte/Lemke_Moralkaufen.pdf), 2014
* COPIED: Interview: [Kaum etwas ist so politisch wie unsere tägliche Ernährung, findet der Philosoph Harald Lemke](http://www.geo.de/GEO/natur/green-living/ernaehrung-warum-haben-sie-unser-essen-satt-herr-lemke-79973.html), geo.de-Interview, 2015
* http://www.supermarktmacht.de/uber-uns/
* siehe [Kakao](../Kakao)
* http://www.foodispower.org/
* [Faire Produktion - Gegen moderne Sklaverei](http://www.deutschlandfunk.de/faire-produktion-gegen-moderne-sklaverei.769.de.html?dram:article_id=333865), DLF 13.10.2015, Druck durch Verbraucher
* [Milchpulver](http://www.attac-netzwerk.de/ag-welthandelwto/milchpulver/hintergrund/)
* http://campact.de/
* siehe zielkonflikte.md "Wenn Sonnenblumen auf Weltreise gehen"
* https://www.fraumeike.de/2018/wollen-koennen-machen/ - "Wollen können machen"
    * netter Hinweis, "dass wir Bewohner der reichen Ersten Welt immer noch viel zu zögerlich damit sind, unser Alltags- und Konsumverhalten dem Wissen um die gravierende Ausbeutung und Umweltschädigung, die daraus entstehen, anzupassen".
    * Was soll man tun? -> klein anfangen und z. B. Ausbeuter-Firmen bei kleinen Dingen wie Schokolade konsequent ablehnen

### Verdrängungsmechanismen, It's not me
* Psychologie der Verdrängung: Cartoon https://polyp.org.uk/consumerism_cartoons/sscon8.html - It's not me
    * mehr siehe dort und wachstum.md

### Psychologie zur Motivation zum Handeln
* siehe Umweltpsychologie

Menschen aus der Geschichte
---------------------------
Personen, die durch ihr Handeln Meilensteine in der Entwicklung einleiteten:

* [Gandhi](https://de.wikipedia.org/wiki/Mohandas_Karamchand_Gandhi)
    * Zitat: "Sei du selbst die Veränderung, die du dir wünschst für diese Welt."
    * "Gandhis 10 Weisheiten um die Welt zu verändern"
        * unterschiedliche Quellen mit teilweise exakt demselben Text (http://alles-schallundrauch.blogspot.com/2009/10/gandhis-10-weisheiten-um-die-welt-zu.html, https://bewusst-vegan-froh.de/gandhis-10-weisheiten-um-die-welt-zu-veraendern-2/, http://www.goldendream.de/gandhis-zehn-weisheiten.html, wer war zuerst?)
        * inklusive Erläuterungen...
        * "Verändere dich selbst"
        * "Du hast die Kontrolle"
        * "Verzeihe und vergesse"
        * "Wenn man nicht handelt kommt man nirgends wo hin"
        * "Lebe in diesen Augenblick"
        * "Jeder ist ein Mensch"
        * "Sei beharrlich"
        * "Sehe das Gute in den Menschen und hilf ihnen"
        * "Sei einheitlich, authentisch, sei du selber"
        * "Wachse und entwickle dich weiter"
* [Nelson Mandela](https://de.wikipedia.org/wiki/Nelson_Mandela)
    * Film: [Mandela - Der lange Weg zur Freiheit](https://de.wikipedia.org/wiki/Mandela_%E2%80%93_Der_lange_Weg_zur_Freiheit), 2013
* [Malcom X](https://de.wikipedia.org/wiki/Malcolm_X)
    * todo
* [Rosa Parks](https://de.wikipedia.org/wiki/Rosa_Parks)
    * Zitat: „Sie dürfen niemals Angst vor dem haben, was Sie tun, wenn es richtig ist.“ (XR, ok?)
* [Martin Luther King](https://de.wikipedia.org/wiki/Martin_Luther_King), siehe dort
* Tim Berners-Lee: [1989: Grundlagen des WWW](https://de.wikipedia.org/wiki/Internet#Ab_1989_Kommerzialisierung_und_das_WWW)

* ["10 weltberühmten Zitate"](https://www.huffingtonpost.de/alan-fields/zitate-leben-veraendern_b_9660270.html), 2016
    * ...

### Eigene Überzeugungen
* Vereinbarkeit von eigenen Überzeugungen und wirtschaftlichen Zwängen:
    * [Der vegane Metzger: biospan vegan FFM](http://biospahn-vegan.de/de/vegan-info?coID=11)

### Handeln
* Zitat: "Man muss so handeln, als ob es möglich wäre, radikal die Welt zu verändern. Und das muss man kontinuierlich tun.“, Angela Davis, Bürgerrechtlerin und Humanwissenschaftlerin (XR, ok?)
    * geboren 1944, https://de.wikipedia.org/wiki/Angela_Davis

Hintergrund: Psychologie
------------------------
Warum sich Menschen nicht oder nur schwer ändern (lassen) wollen:

* Sachbuch: [Change of Heart](http://www.thehumaneleague.com/changeofheart/author.htm) von Nick Cooney, 2010, "What Psychology Can Teach Us about Spreading Social Change",am Beispiel Tierrechte
    * [Vortrag von Mahi Klosterhalven](https://www.youtube.com/watch?v=RtNojbjNRGo) (2012)
    * weitere Bücher von Nick Cooney:
        * Veganomics: The Surprising Science on What Motivates Vegetarians, from the Breakfast Table to the Bedroom, 2013
        * How to be Great at Doing Good: Why Results are What Count and How Smart Charity Can Change the World, 2015
    * ähnlich:
        * Buch: Living among meat eaters von Carol J. Adams, 2008
* Video: ["Playing politics does not work"](https://www.youtube.com/watch?v=tjc8x5vJRCQ), Gary Yourofsky, 2013, 4 min


Die Verantwortung des Einzelnen
-------------------------------
* siehe oben Weizenbaum, unter "Literatur zur Wirksamkeit des Einzelnen"

### "Ein Intellektueller zu sein, ist eine Berufung für jedermann"
* [Philosophische Fakultät Uni Köln](http://amp.phil-fak.uni-koeln.de/9598.html)
    * "Ein Intellektueller zu sein, ist eine Berufung für jedermann: es bedeutet, den eigenen Verstand zu gebrauchen, um Angelegenheiten voranzubringen, die für die Menschheit wichtig sind. Einige Leute sind privilegiert, mächtig und gewöhnlich konformistisch genug, um ihren Weg in die Öffentlichkeit zu nehmen. Das macht sie keineswegs intellektueller als einen Taxifahrer, der zufällig über die gleichen Dinge nachdenkt und das möglicherweise klüger und weniger oberflächlich als sie. Denn das ist eine Frage der Macht." (Noam Chomsky)
* In Kombination mit nächstem Abschnitt: jeder ist für seine Handlungen verantwortlich.

### Die Verantwortlichkeit der Intellektuellen
* Sachbuch: [Noam Chomsky - Die Verantwortlichkeit der Intellektuellen](http://www.kunstmann.de/titel-0-0/die_verantwortlichkeit_der_intellektuellen-630/), Kunstmann, 2008
    * Zentrale Schriften zur Politik, Buchtitel ist der erste Aufsatz des Buches
    * "Die Intellektuellen haben die Verantwortung, die Wahrheit zu sagen und Lügen aufzudecken."
    * [Noam Chomsky auf wikipedia](https://de.wikipedia.org/wiki/Noam_Chomsky), emeritierter Professor für Linguistik, mit Verbindung zur Informatik
    * https://chomsky.info/
    * siehe auch https://de.wikipedia.org/wiki/Intellektuelle_Verantwortung
        * "auch epistemische Verantwortung, ist ein Konzept aus der Philosophie"
        * William Kingdon Clifford
            * "Der Mathematiker und Philosoph William Kingdon Clifford definierte intellektuelle Verantwortung in Form einer kurzen Erzählung und einer Definition. In der Erzählung wird ein Schiffsinhaber beschrieben, welcher Tickets für die transatlantische Überfahrt verkauft. Er erfährt, dass das Schiff Defekte aufweisen könnte. Wissentlich, dass die Inspektion zu erheblichen Kosten und Verzögerungen bei der Abfahrt führen würde, verschiebt der Schiffsinhaber die Inspektion. Da es keinen Beweis dafür gibt, dass das Schiff Mängel aufweist, glaubt er selbst daran, dass das Schiff seetüchtig ist, und verkauft die Tickets. Als das Schiff auf hoher See untergeht, erhält er das Geld für das Schiff von der Versicherung erstattet. Nach Clifford ist der Schiffsinhaber am Tod der Matrosen und Passagiere verantwortlich, da er seinen Glauben, dass das Schiff in Ordnung sei, hinterfragen und die Inspektion durchführen hätte müssen."
            * „Es ist immer und überall und für alle falsch etwas zu glauben, ohne ausreichende Belege zu haben.“
            * „Es gibt den „privaten Glauben“ nicht. Da wir alle über unsere Glaubenssätze sprechen – manche von uns sogar viel – verbreiten sich unsere Glaubenssätze.“
            * „Der Glauben an einen Gott, dessen Existenz nicht bewiesen werden kann, ist „blinder Glaube“. Blinder Glaube bringt eine Person dazu, andere Fakten und Argumente zu ignorieren, was dazu führt, dass sie ein unerforschtes und unbedachtes Leben führt.“
                * Gegenargument von William James; und davon wieder ein Gegenargument: "Dem steht allerdings gegenüber, dass religiöser Glaube durchaus auch negative Auswirkungen, insbesondere auch auf die Umwelt und andere Menschen, hat. Insbesondere, wenn nötige Aktionen – aufgrund von Vertrauen in eine ohnehin positive Entwicklung – unterlassen werden (z. B. Philanthropie, Umweltschutz oder Impfungen)"
        * "Noam Chomsky argumentiert in seinem Essay The Responsibility of Intellectuals, dass intellektuelle Menschen die Verantwortung dafür haben, die Wahrheit zu suchen und Lügen bloß zu stellen. Ein Whistleblower folgt demnach seiner ethischen und moralischen Verantwortung."
        * Peter Van Inwagen: „Es ist immer, überall und für alle falsch, Belege zu ignorieren die für seine Glaubenssätze und Vorstellungen wichtig sind, oder diese in einer falschen Weise zu verwerfen.“

* Noam Chomsky
    * siehe auch https://www.nachdenkseiten.de
        * ["Noam Chomsky Requiem für den amerikanischen Traum Doku ORF2 am 6.11.2016"](https://vimeo.com/190552903)
    * siehe Interview mit Jung&Naiv

* Schiffsinhaber-Erzählung anwendbar z. B. auf Atomkraft und Gentechnik?

### "Was nützt ein Intellektueller?"
* Artikel über Jean Zieglers Buch "Ändere die Welt": http://www.huffingtonpost.de/alexandra-hildebrandt/was-nutzt-ein-intellektueller_b_8064732.html, 2015
    * eigentlich eine Frage von Berthold Brecht
    * Erwähnung von Roger Willemsen

### Ausreden
* aus einem Gespräch 2017
    * Der Intellektuelle und sonst Wohlhabende sollte nicht mehr Verantwortung haben als jeder andere auch.
    * Weil: der Intellektuelle ist sich den Problemen so was von bewusst und leidet darunter sehr, so dass ihm nicht noch mehr Bürden auferlegt werden sollten.
        * Tja

Beispiel Menschenrechte
-----------------------
https://www.amnesty.de/allgemein/und-was-hat-das-mit-mir-zu-tun

* "Und was hat das mit mir zu tun? - Zehn Gründe, warum dir die Menschenrechte nicht egal sein können"
* "Immer wieder wird uns die Frage gestellt, ob es überhaupt Sinn macht, sich für Menschenrechte zu engagieren. Man könne doch auch mal ein Auge zudrücken. Das sei doch Aufgabe der Politik. Es gäbe doch wichtigere Probleme… Wir sehen das anders. In diesem Text setzen wir uns mit zehn Klischees und Missverständnissen auseinander, die uns bei unserer Arbeit immer wieder begegnen."
* 1. "Warum muss man sich denn heute überhaupt noch für die Menschenrechte einsetzen?"
* 2. "Für mein Leben spielen die Menschenrechte keine Rolle."
* 3. "In Deutschland ist doch alles in Ordnung."
* 4. "Was geht mich das an, wenn in einem fernen Land Menschenrechte verletzt werden?"
* 5. "Die 'Allgemeine Erklärung der Menschenrechte' hilft mir nicht, sie ist doch nur ein Stück Papier."
* 6. "Wir können doch nicht Menschen in anderen Ländern vorschreiben, wie sie zu leben haben."
* 7. "Mir ist Sicherheit aber wichtiger als Menschenrechte."
* 8. "Menschenrechte hin oder her - wir können doch nicht alle Flüchtlinge aufnehmen."
    * "Das verlangt auch niemand. Es geht aber darum, dass alle Flüchtlinge Menschen sind und sie die gleichen Menschenrechte haben wie du und ich."
    * "2017 hielten sich von weltweit 68,5 Millionen Flüchtlingen nur gut zwei Prozent in Deutschland auf."
* 9. "Mir ist es egal, dass meine Daten gesammelt werden, ich habe nichts zu verbergen."
    * "Wenn uns beim Chatten mit dem Smartphone jemand zu nah über die Schulter schaut, empfinden wir das ganz klar als übergriffig, aber dass Firmen und Staaten uns stärker ausspähen als jemals zuvor, ist uns oft gar nicht richtig bewusst. Wir lassen zu, dass fast alle unserer täglichen Schritte detailliert verfolgt werden."
    * Detail dazu siehe demokratie.md
* 10. "Die Politik macht doch sowieso, was sie will. Es bringt nichts, sich für Menschenrechte einzusetzen."

Inbox
-----
### "How Much Good Can One Person Really Do?", 2020
* https://charleseisenstein.org/video/how-much-good-can-one-person-really-do/, 2020, 20 min
    * ...
    * "But then, some **doubt** comes into your mind. You read the headlines about climate change
        or whatever else and you’re like, None of this matters, because in the face of this
        gigantic problem, what good is it to take care of just one little creek?"
    * ...
    * "Because--isn’t one of the reasons why the world is in the state that it is because
        everybody mortgages what is really pulling at their heart for the sake of something abstract,
        for the thing that they think that they **have to do for the numbers to work out?**"
    * ...
    * "So, okay, but isn’t it true that taking care of one little creek isn’t going to change the world?
        In the **theory** of change that we’ve inherited, yes, it’s true.
        It’s true that nothing you do matters. Because you’re just one little person."
        * "And what you do, unless you have tons of money and a big platform or lots of force at your disposal, you can’t do very much, can you? That view, it’s based on a theory of change that is Newtonian. It says that something in the world changes when you push it, when you exert a force on it, and if you exert a little force, you make a tiny little change. If you exert a big force, you make a big change. So in order to be a worthwhile person, you have to leverage your force somehow. For example, through money or through your project going viral or scaling up. Then it matters, but not otherwise."
    * ...
    * "So, any act of service to a stream or a service to the soil will create a field that brings more acts of service to the soil around the world"
        * warum?
        * ...
        * "but it means that you trust the opportunities to do so when they come and if they come."
    * ...
    * ... TODO
    * ...

### Haltung zeigen im Sport-Doping, 2019
* 2019: "Danilo Hondo: - Ich muss dazu stehen" - Sportschau. 12.05.2019. 29:53 Min. Das Erste. - Der Cottbuser Ex-Radprofi Danilo Hondo hat gegenüber der ARD-Dopingredaktion sein eigenes Doping gestanden. Sehen Sie hier das komplette Interview.", https://www.sportschau.de/doping/video-danilo-hondo-ich-muss-dazu-stehen--100.html

### Stallman / Gandhi, 2019
* https://stallman.org/media.html
    * "You assist an evil system most effectively by obeying its orders and decrees. An evil system never deserves such allegiance. Allegiance to it means partaking of the evil. A good person will resist an evil system with his or her whole soul." (Mahatma Gandhi)

### 2018
* https://ohneamazon.wordpress.com, siehe amazon.md
* Sachbuch: ["Kooperation statt Konkurrenz"](https://www.christian-felber.at/buecher/koop.php), 2009
    * "Christian Felber zeigt den Weg, den die krisengeschüttelten Demokratien, dank kollektiver Mobilisierung, gemeinsam, gehen müssen, um endlich eine zivilisierte, vernunftbestimmte, soildarische Gesellschaft zu erschaffen. - Jean Ziegler"
