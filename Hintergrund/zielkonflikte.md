Zielkonflikte / daily-choices-info-package
==========================================

[zurück](../../..)

Diskussion möglicher Zielkonflikte und deren mögliche Auflösung.

<!-- toc -->

- [Bio vs. Regional](#bio-vs-regional)
- [Bio vs. Vegan?](#bio-vs-vegan)
  * [Weil die Schweiz ein Grasland ist](#weil-die-schweiz-ein-grasland-ist)
- [Mit dem Großauto zum Biomarkt?](#mit-dem-grossauto-zum-biomarkt)
- [Negative Folgen des Konsums vs. Geschmack](#negative-folgen-des-konsums-vs-geschmack)
- [Aber X (z. B. Bio) ist auch nicht immer gut](#aber-x-z-b-bio-ist-auch-nicht-immer-gut)
- [Ich blick' nicht durch beim Siegel-Dschungel](#ich-blick-nicht-durch-beim-siegel-dschungel)

<!-- tocstop -->

Bio vs. Regional
----------------
* Was ist besser: Bio, aber unbekannte Herkunft oder regional, aber konventionell?
* Betrachtung der Auswirkungen:
    * Bio vs. konventionell: konventionelle Landwirtschaft ist nicht zukunftsfähig (siehe Bio) und sollte schnellstmöglichst korrigiert werden. Die konventionelle (hoher Pestizid- und Düngeeinsatz) Landwirtschaft ist leider eine Sackgasse.
    * Bio kaufen fördert die Nachfrage und hat dadurch erhöht sich das Angebot.
    * Für Bio eintreten fördert Bewusstsein.
    * Transport: Optimierungspotential: Regionalisierung des Anbaus.
    * Längere Fahrt zum Bio-Laden? -> Optimierungspotential vorhanden (weniger oft fahren, Fahrrad fahren, E-Auto kaufen etc.)
* Fazit: im Zweifel Bio, aber nicht von Übersee.

* ["Wenn Sonnenblumen auf Weltreise gehen"](http://biobaeckerweber.de/de/rohstoffe-und-andere-grundlagen/sonnenblumenkerne-aus-china-oder-winnenden.html), 2014
    * Beispiel einer Bio-Bäckerei, die sich mit anderen Bio-Bäckereien zusammengeschlossen hat, um heimische Sonnenblumenkerne zu bekommen (gab es vorher billig nur aus China)
    * (Gegenbeispiel zu "Ohnmacht des Einzelnen"; Wille und Engagement ist aber notwendig)

Bio vs. Vegan?
--------------
* https://www.lebenlassen.de/tierleidfreie-betriebe/ -> https://www.lebenlassen.de/app/download/14074508822/2015-09_info3_tierleidfreie_landwirtschaft.pdf?t=1491140236
    * 2015
    * "Zwischen allen Stühlen - Gibt es einen „Dritten Weg“ in der Landwirtschaft – die Nutzung von Tieren und tierischen Produkten ohne Tierleid? Andreas Fendt, streitbarer Bio-Bauer im Südschwarzwald, lotet seit einiger Zeit mögliche Alternativen aus. Ein Besuch vor Ort."
    * "Andreas Fendt, der sich seit vielen Jahren vegetarisch ernährt, Gedanken darüber, wie ein würdiger Umgang mit Tieren aussehen könnte, und vor allem: Wie man Tiere nutzen kann, ohne sie zu schlachten"
    * "Gerade weil ich Vegetarier bin, interessiert mich deshalb, wie eine nicht-vegane, aber tierleidfreie Landwirtschaft funktionieren könnte."
    * "Ursprünglich Städter und zunächst als Informatiker und Berater tätig, hat sich der heute 52-Jährige im Alter von 30 Jahren umorientiert"
    * "auf 900 Meter Höhe mit 20 Ziegen und zwei Kühen, deren Mist er zwar nutzt, die er aber nicht schlachtet"
    * "Neun Hektar Land gehören zum Hof, auf den Feldern wächst vor allem Dinkel"
    * " Das Wichtigste, das die Tiere bieten, sei der Mist, alles andere nur Nebenprodukte, über die man sich freuen könne, wenn sie anfallen, die aber nicht im Mittelpunkt stehen sollten"
    * „Mein Ziel ist es, dass man die Kuh auf den Höfen wieder als eigentliche Seele des Betriebs würdigt, als das Herz, das dem Hof seinen Rhythmus gibt. Die kann man doch nicht einfach schlachten! Oder ihre Kälber weggeben – das macht doch auch etwas mit dem Wesen des Betriebs!“
    * Kasten:
        * "Noch bis vor wenigen Jahren war wohl den wenigsten Verbrauchern bewusst, wie eng verlochten die verschiedenen Bereiche der landwirtschaftlichen Tiernutzung sind."
        * Milch, Kälber, Schlachten, Spezial-Rassen, männliche Kälber, heimliche Tötung von Bullenkälbern, männliche Küken, am Ende immer Schlachtung
        * "Insofern scheint es nur konsequent, wenn Veganer den kompletten Verzicht auf tierische Nahrungsmittel propagieren"
        * bio-vegan derzeit noch wenig erforscht und nicht für alle Gebiete geeignet
    * „Mein Motto, das über diesem Hof steht, ist die Freiheit“, ..., "Und schließlich die Freiheit im Handeln. Da bin ich privilegiert, denn ich kann selbst entscheiden. Ich habe keinen Chef, ich muss es nur wollen und tun.“
    * ! "Der Tag, an dem er beschlossen hat, auf seinem Hof kein Tier mehr zu schlachten, sei für ihn, den bekennenden Buddhisten, die größte Befreiung während seines Lebens als Landwirt gewesen: „Da ging es mir gut. Die Tiere spürten das auch, der ganze Hofcharakter änderte sich, alles ist jetzt viel friedlicher. Was ist das für ein Leben mit dieser ständigen Androhung von massiver Gewalt, das wirkt ja auch auf den Menschen zurück.“"
    * ! Wirtschaftlich? -> "Ein Drittel der Betriebseinnahmen von heutigen Landwirten sind Subventionen, in der ökologischen Landwirtschaft ist der Anteil eher noch höher. Bezahlen tut das der Steuerzahler. Er bezahlt damit in der konventionellen Landwirtschaft Gülle- und Nitratbelastung, Artensterben, qualvolle Massentierhaltung. Wenn wir schon bezahlen, dann doch bitte auch das, was wir wollen"
    * "Wir müssen bei den Verbrauchern ein Bewusstsein für Qualität schaffen. Man muss das schrittweise angehen"
    * Bioland? Demeter?
    * Kasten: "Praxisbeispiel: Tierleidfreies Demeter-Gemüse"
    * ! "DAS BIOFLEISCH-DILEMMA"
        * "Bisher liegt die Nachfrage nach Bioleisch mit ein bis zwei Prozent am Gesamtmarkt deutlich hinter der nach Biomilch, die einen Marktanteil von rund sechs Prozent hat"
        * ...
        * "Die Folge ist ein kaum bekannter Missstand: Der größte Teil der neugeborenen Bullenkälber auf Bio-Betrieben wird aus wirtschaftlichen Gründen in konventionelle Mastbetriebe verkauft, wo sie in der Regel in dunklen Hallen qualvoll gemästet werden."
        * ...
        * keine offiziellen Zahlen, "aufgrund meiner Berechnungen gehe ich davon aus, dass das rund 80 Prozent der auf Biobetrieben geborenen Kälber betrifft"
        * ...
        * Milch muss teurer werden, damit es wirtschaftlich bleibt
    * BEWUSSTSEINSBILDUNG IST GEFRAGT
        * "Grundsätzlich ist die Frage, wie man eher traditionelle Landwirte mit einer neuen, urbanen Generation von bewussten Konsumenten, und darunter eben auch Veganern, zusammenbringen kann. „Veganer, die auf die Bauern schimpfen und Bauern, die sich über die Veganer lustig machen – das bringt uns doch nicht weiter“"
        * ..., "Der eine Bauer will Milch, der andere will auch Fleisch. Ich selbst erfreue ich mich an der Anmut und Schönheit der Tiere, und wenn es auch noch ein bisschen Milch gibt, reicht mir das. Aber dazu braucht man eben eine große Freiheit und die haben die Bauern heute kaum noch."
            * (Wichtig: in ländlichen Gebieten wollen die Menschen ihre Bauern unterstützen! Pauschale Diffamierung ist kontraproduktiv.)
        * "Auf der anderen Seite kritisiert Andreas aber auch die Konventionalisierung der veganen Bewegung. „Was bei der Biobewegung 30 Jahre gedauert hat, ist beim Veganismus schon nach drei Jahren zu beobachten“, sagt er."
        * "„Ich kann mich nur wundern, wenn Tierrechtler über neue Produktlinien jubeln, die zwar vegan, aber eben nicht bio sind. Dabei ist es doch so: Ein konventionelles veganes Produkt verursacht vermutlich global gesehen mehr Tierleid als ein Bio-Käse, den ich esse. Wenn vegan, dann doch bitte bio!“"
            * Kommentar: "Konventionell-Pflanzlich" ist sicher besser als "Konventionell-Fleisch", aber der Bio-Aspekt MUSS immer angesprochen werden, wenn es sich um konventionelle Produkte handelt. Denn "konventionell-pflanzlich" ist oft schlechter als "Bio-Fleisch" (wegen konventioneller Gülle auf dem Acker)
    * Kasten "Forderungen der Stiftung Lebenshof für eine tierleidfreie Landwirtschaft"
        * keine Enthornung
        * keine Anbindehaltung
        * ..., mobile Schlachtbox
        * ...

### Weil die Schweiz ein Grasland ist
* 2014: https://sentience-politics.org/de/weil-die-schweiz-ein-grasland-ist/
    * "Demnach wäre in einem Grasland wie der Schweiz für Omnivore etwa folgende Ernährung angezeigt: 6 Vegi-Tage pro Woche, fast die Hälfte der Vegi-Mahlzeiten vegan, und praktisch alle anderen Vegi-Mahlzeiten ohne Eier. Mehr Tierprodukte gibt unser Grasland nicht her."

Mit dem Großauto zum Biomarkt?
------------------------------
* Bewertung: Besser als mit demselben Großauto zum Billig-Discounter.
    * siehe auch [Autokauf](../Auto)
    * siehe auch [Louis CK about not selling his luxury car to help other people](https://www.youtube.com/watch?v=lC4FnfNKwUo), 2010
        * User-Comment: "The problem with this logic is that the more people you feed the more the population grows, which increases the number of people you need to feed. Starvation is nature's population control.﻿"
            * Reply: "How fortunate it is that your life has enough intrinsic value so as not to be part of that 'natural' equation.﻿"
            * Reply: "The bigger point is that we in the western world live in excess. We drain resources all over the world for shit no one really needs - it's just that we want it, because it's nice, new, cool, or more comfortable and luxurious."
        * Je wohlhabender (ab Mittelklasse), desto mehr Verantwortung

Negative Folgen des Konsums vs. Geschmack
-----------------------------------------
* Wie kann man die Machenschaften eines Herstellers, der ein Produkt anbietet, das den eigenen Geschmack trifft, abwägen?
    * Die Erfahrung zeigt: es gibt viele Alternativen und auch wenn nicht: der Mensch gewöhnt sich geschmacklich an vieles.
    * Frage, die man sich stellen kann: Ist das regelmäßige Geschmackserlebnis X es wert die Machenschaft Y in Kauf zu nehmen?

Aber X (z. B. Bio) ist auch nicht immer gut
-------------------------------------------
* Richtig. Aber sollte man, weil etwas "auch mal" nicht gut ist, dann immer das schlechtere wählen?

Ich blick' nicht durch beim Siegel-Dschungel
--------------------------------------------
* Das Internet hilft. Etwas Übung und Austausch mit (positiv gestimmten) anderen Menschen führt relativ schnell zum Ziel.
* Bitte beachten: auch wenn das Siegel nicht zu 100% ohne Kritik ist, dann ist es oft besser als nichts.
* Bitte beachten 2: Es gibt eine Menge Green-washing-billig-Siegel. Diese gilt es zu identifizieren.
* Übersichten
    * https://www.umweltbundesamt.de/themen/durchblick-im-siegeldschungel
        * Bewertung: Infos zum MSC-Label leider falsch, ansonsten ok.
