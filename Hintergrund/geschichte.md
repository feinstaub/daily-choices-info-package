Geschichte
==========

<!-- toc -->

- [Entwicklung der Moral](#entwicklung-der-moral)
  * [Abschaffung der Sklavenhaltung](#abschaffung-der-sklavenhaltung)
  * [Rechte von Frauen](#rechte-von-frauen)
  * [Aufklärung: Kinsey-Report 1948](#aufklarung-kinsey-report-1948)
  * [Rechte von Kindern](#rechte-von-kindern)
  * [Bedürfnisorientierung](#bedurfnisorientierung)
  * [unterschiedliche Geschwindigkeiten](#unterschiedliche-geschwindigkeiten)
- [Befreiungsbewegungen](#befreiungsbewegungen)
- [Rechte müssen erkämpft werden](#rechte-mussen-erkampft-werden)
- [Geschichte Entwicklung](#geschichte-entwicklung)
  * [Harari](#harari)
- [Fehlgeleitete Entwicklungshilfepolitik](#fehlgeleitete-entwicklungshilfepolitik)
  * [Beispiel Milchpulver](#beispiel-milchpulver)
  * [Beispiel Agrarprodukt-Export-Subventionen](#beispiel-agrarprodukt-export-subventionen)
- [Menschenrechte](#menschenrechte)
- [Kolonialismus](#kolonialismus)
- [Flüchtlinge](#fluchtlinge)
  * [Dadaab](#dadaab)
- [Verschiedenes](#verschiedenes)
  * [Wilderei für den Westen](#wilderei-fur-den-westen)
  * [Aufblasen marginaler Lösungswege - Beispiel: Globales Müllproblem](#aufblasen-marginaler-losungswege---beispiel-globales-mullproblem)
  * [Blockade sinnvoller Vorschläge - Beispiel: Auto-Verkehr](#blockade-sinnvoller-vorschlage---beispiel-auto-verkehr)
  * [Gesellschaftskritischer Humor](#gesellschaftskritischer-humor)
  * [Die Welt wird besser, nicht schlechter (aber nicht überall)](#die-welt-wird-besser-nicht-schlechter-aber-nicht-uberall)

<!-- tocstop -->

Entwicklung der Moral
---------------------
### Abschaffung der Sklavenhaltung
* ...
* Hinweis: beim Blick von außen auf eine südamerikanische Plantage damals sah das so aus, als ob die Sklaven ihre Arbeit freiwillig tun.
    * Bis heute werden gewalterfüllte Methoden angewandt (inkl. psychischer Gewalt), um Menschen gefügig zu machen. Von außen sieht man das nicht unbedingt.
* Damaliges Denken, z. B. in Form von Briefen
    * https://www.wissenschaft.de/geschichte-archaeologie/historische-briefe-britischer-sklavenhalter-veroeffentlicht/, 2015
        * ca. 1750
        * "Menschen als Ware"
        * "Ein lukratives Geschäft"
        * "Einträge wie „Dick, 25, fähiger Feldarbeiter, 40 Pfund“
            oder „Castile, 45, Köchin und Waschfrau, 60 Pfund“
            geben einen schockierenden Eindruck darin, wie sehr diese Menschen als Ware betrachtet wurden"
            * "Obwohl sich das für moderne Augen erschütternd liest, waren dies für die Beteiligten ganz normale Geschäftstransaktionen"
            * "„Es war ein alltäglicher Teil der Wirtschaft im 18. Jahrhundert.
                Wirtschafts-Magnaten machten **erhebliche Profite** von den Gütern, die durch Sklavenarbeit produziert wurden –
                **und die Kunden profitierten von den billigen Waren.**“"
        * "Ein Brief aus dem Jahr 1796 berichtet, dass einer von Perrins Gutsverwaltern plant,
            „eine Arbeitsmannschaft von 60 körperlich tüchtigen Negern“ zu erstehen, um diese auf dem Gut einzusetzen.
            In einem anderen Brief wird dargelegt, dass der Kauf billiger Sklaven zur Bestellung von Zuckerrohr dem Gut die Kosten für den Kauf von Vieh ersparen würde."
        * "Diese Briefe enthüllen neben einem **völligen Fehlen von Empathie** für ihre menschliche Ware auch die schiere **Menge an Geld**, die hier umgeschlagen wurde"
            * "Angesichts der enormen Profite durch den Sklaveneinsatz sei es kein Wunder, dass die **Antisklaverei-Bewegung lange brauchte**, um sich durchzusetzen"
            * "Die **meisten dieser Kampagnen wurden von normalen Leuten initiiert**,
                **während die Pro-Sklaverei-Lobby** signifikanten Reichtum und Einfluss besaß, und so auch das Parlament unter Druck setzen konnte", ::lobby, ::demokratie
        * "Es dauerte 20 Jahre, bis sich die Gegner der Sklaverei in Großbritannien endlich durchsetzen.
            Am 25. März 1807 beschloss das britische Parlament, den Sklavenhandel abzuschaffen."
    * https://www.sankt-georgen.de/fileadmin/user_upload/personen/Baumert/Norbert_Baumert__Ein_Freundesbrief_an_einen_Sklavenhalter_.pdf
    * https://www.welt.de/kultur/history/article10708318/Wie-Abraham-Lincoln-die-Sklavenhalter-provozierte.html
    * Film: Belle (2013)

### Rechte von Frauen
* siehe diskriminierung-sexismus.md inkl. Beschneidung

### Aufklärung: Kinsey-Report 1948
* https://de.wikipedia.org/wiki/Kinsey-Report

### Rechte von Kindern
* ...

### Bedürfnisorientierung
* ...

### unterschiedliche Geschwindigkeiten
* je nach Land und Region

Befreiungsbewegungen
--------------------
* Rassismus: Schwarze Sklaven
* Sexismus: Frauen
* Sexuelle Minderheiten
* ...

Rechte müssen erkämpft werden
-----------------------------
* Rechte müssen erkämpft werden. Das passiert nicht automatisch.
* Insbesondere individuelle Freiheit verschwindet, wenn sie nicht wertgeschätzt wird. Jeder ist aufgerufen dafür arbeiten, dass dies nicht passiert.
* ["Freedom is Worth the Inconvenience"](https://www.singularityweblog.com/richard-stallman-singularity-free-software/), 2016
    * Die Opfer, die man für Softwarefreiheit geben muss, sind in diesem Feld sogar relativ klein. Man muss nur einige kleine Unbequemlichkeiten akzeptieren.
* todo: weitere Beispiele

Geschichte Entwicklung
----------------------
### Harari
* Buch: ["Sapiens: A Brief History of Humankind"](https://en.wikipedia.org/wiki/Sapiens:_A_Brief_History_of_Humankind)
    * deutsch: "Eine kurze Geschichte der Menschheit"
    * ...
    * "Harari claims that all large-scale human cooperation systems – including religions, political structures,
        trade networks and legal institutions – owe their emergence to Sapiens' distinctive cognitive capacity for fiction.
        Accordingly, Harari reads money as a system of mutual trust and sees political and economic systems as more or less identical with religions."
    * ...
    * Gewalt gegenüber Tieren zieht sich durch die ganze Geschichte. (todo: siehe F. Schmitz blog)
    * ...

* Interview: https://www.youtube.com/watch?v=5fHKK_YFUrw - "Yuval Harari: Ein Historiker erzählt die Geschichte von morgen (SRF Sternstunde Philosophie)"
    * ... TODO
    * Wer ist die Autorität?
        * Humanisten: menschliche Gefühle
        * früher: Kirche/Gott
        * Marktwirtschaft: der Kunde
        * ...
    * ...

Fehlgeleitete Entwicklungshilfepolitik
--------------------------------------
### Beispiel Milchpulver
* Hintergrund:
    * [Milchpulver](http://www.attac-netzwerk.de/ag-welthandelwto/milchpulver/hintergrund/)
    * Mögliche praktische und konstruktive Lösung: vegan

### Beispiel Agrarprodukt-Export-Subventionen
* todo

Menschenrechte
--------------
* siehe dort inkl. Folter

Kolonialismus
-------------
siehe diskriminierung-kolonianismus.md

Flüchtlinge
-----------
### Dadaab
* https://de.wikipedia.org/wiki/Dadaab
    * Somalia, Kenia, Bürgerkrieg

Verschiedenes
-------------
### Wilderei für den Westen
* Beispiel: https://de.wikipedia.org/wiki/Nationalpark_Virunga#Wilderei_und_Naturschutz
    * "Der Dokumentarfilm Virunga (2014) beschreibt die Auseinandersetzung zwischen den Befürwortern und Schützern des Nationalparks und dem britischen Ölkonzern SOCO, der im Gebiet des Parks nach Öl sondieren will."
        * https://en.wikipedia.org/wiki/SOCO_International

### Aufblasen marginaler Lösungswege - Beispiel: Globales Müllproblem
* 2017: business-as-usual
    * http://www.tagesschau.de/ausland/raupe-plastik-101.html, 24.04.2017: "Eine kleine Raupe könnte die Lösung für das globale Müllproblem sein.
        Das Besondere an ihr: Sie hat Appetit auf Plastik. Eine italienische Forscherin hat das zufällig entdeckt - als sie die Raupe in flagranti erwischte."
        * siehe User-Comments, z. B. "Na wunderbar. Dann können wir ja weiter so wie bisher sinnlos Ressourcen verschwenden
        und unseren Müll bedenkenlos in die Natur absondern, statt von vorn herein nachhaltig und maßvoll zu leben."

### Blockade sinnvoller Vorschläge - Beispiel: Auto-Verkehr
* 2017-04-25:
    * Die EU möchte aufgrund des Abgas-Skandals (Betrugssoftware in Motorsteuergeräten) eine Regelung schaffen zur Reduzierung von Interessenskonflikten (Herstellerinteresse vs. Gemeinwohl) bei der Fahrzeugkontrolle schaffen. Die Bundesregierung blockiert dies. todo: Quelle

### Gesellschaftskritischer Humor
* Pispers: [Wählen allein hilft doch nicht weiter](https://www.youtube.com/watch?v=V0Llf7tB-uY), 2013
* Pispers: [Armut und Griechenland beim Kleinkunstpreis 2015](https://www.youtube.com/watch?v=uXdceJmKck4)
* Pispers: ["DDR als Niedriglohnsektor"](https://www.youtube.com/watch?v=CMdWSG62Sro), 2015
    * Kapitalismuserklärung mit Löwe und Gazelle
    * mehr siehe Finanzen/README.md: "[Kapitalismus als Religion](https://www.youtube.com/watch?v=YzD1l-qYqnA)"
* Louis C.K.: [What did you do to the polar bears?](https://www.youtube.com/watch?v=WrahQpIWD08), 2013

### Die Welt wird besser, nicht schlechter (aber nicht überall)
* siehe ["Die Welt wird besser, nicht schlechter"](https://www.dieoption.at/die-welt-wird-besser-nicht-schlechter/), 2018
    * ...
