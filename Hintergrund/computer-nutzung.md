Hintergrund: Computernutzung
============================

<!-- toc -->

- [Update-Politik](#update-politik)
  * [Genau hinschauen bei "as a service"-Angeboten](#genau-hinschauen-bei-as-a-service-angeboten)
- [Entwicklungen zu IT-Sicherheit](#entwicklungen-zu-it-sicherheit)
  * [2017: "Studie: Computersabotage und Erpressung nehmen deutlich zu"](#2017-studie-computersabotage-und-erpressung-nehmen-deutlich-zu)
- [Entwicklungen weg von der Privatsphäre](#entwicklungen-weg-von-der-privatsphare)
- [Möglichkeiten zu mehr Privatsphäre](#moglichkeiten-zu-mehr-privatsphare)

<!-- tocstop -->

Update-Politik
--------------
* Wichtig für
    * IT-Sicherheit
    * Weiterverwendung alter Hardware (= Umweltfreundlichkeit)

### Genau hinschauen bei "as a service"-Angeboten
* Beispiel: Windows Mobile:
    * https://www.heise.de/newsticker/meldung/Wo-Windows-as-a-Service-Kunden-beisst-3686501.html, heise.de, 18.04.2017
        * "Mit der Veröffentlichung von Windows 10 startete Microsoft sein Modell "Windows as a Service". Kunden erhalten neue Versionen automatisch und kostenlos. Was bei PCs noch ganz gut klappt, strahlt negativ auf manches Windows-Phone ab."
        * siehe auch User-Comments
    * https://www.golem.de/news/windows-10-mobile-da-waren-es-nur-noch-13-1704-127329.html
        * "Demnach wurden auf älteren Geräten mit Windows 10 Mobile bei Tests keine guten Ergebnisse mehr erzielt."
    * Alternative: LinageOS: https://lineageos.org/ ([Freie Software](../FreieSoftware))
        * Liste von Geräten: https://wiki.lineageos.org/devices/

Entwicklungen zu IT-Sicherheit
------------------------------
### 2017: "Studie: Computersabotage und Erpressung nehmen deutlich zu"
* https://www.heise.de/newsticker/meldung/Studie-Computersabotage-und-Erpressung-nehmen-deutlich-zu-3686223.html
    * Systembeschädigungen
    * Computer-Sabotage
    * Erpressung
* Mehr Aufklärung zum Gefahren-Bewusstsein erforderlich.

Entwicklungen weg von der Privatsphäre
--------------------------------------
* siehe [Privatsphäre](../Hintergrund/privatsphaere.md)

Möglichkeiten zu mehr Privatsphäre
----------------------------------
* Allgemein: [Freie Software](../FreieSoftware)
* siehe [Privatsphäre](../Hintergrund/privatsphaere.md)
