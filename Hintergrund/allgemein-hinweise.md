Hinweise
========

<!-- toc -->

- [Einleitung](#einleitung)
  * [Gewohnheiten](#gewohnheiten)
  * [Philosophie](#philosophie)
- [Hinweise](#hinweise)
- [Ethische Grundlagen](#ethische-grundlagen)
  * [Die Goldene Regel](#die-goldene-regel)
- [Was tun?](#was-tun)
  * [Kleine Schritte](#kleine-schritte)
  * [The Lifelong Activist](#the-lifelong-activist)
- [Positive Beispiele](#positive-beispiele)
- [Andere Sammlungen](#andere-sammlungen)

<!-- tocstop -->

Einleitung
----------
Informationen für Menschen mit positiver Grundhaltung, die gerne eine nachhaltige Lebensweise umsetzen möchten. Das heißt sich möglichst so zu verhalten, dass im Rahmen des eigenen Wohls das Wohl anderer (jetzt und in der Zukunft) möglichst wenig eingeschränkt wird. Ein Baustein ist Bewusstsein über die Faktenlage und daraus resultierende informierte Konsumentscheidungen.

Dieses Ziel soll dabei möglichst leicht und effizient erreicht werden. Das heißt, grundlegende Maßnahmen zur Lösung der Ursache von Problemen sind einem Aufreiben in der Optimierung von kleinen Details oder dem bloßen Lindern von Symptomen vorzuziehen. Andererseits: jede positive Handlung zählt.

* Je weniger schädlicher Konsum, desto besser.
* Wegwerfprodukte und Verschwendung (gerade beim Essen) vermeiden; selbst der fairste Konsum ist nicht besser als der Nicht-Konsum.
* Es gibt zwar oft kein absolut bestes Verhalten, daher bieten "x-ist-besser-als-y"-Bewertungen eine Orientierung.

### Gewohnheiten
Schrittweise Etablierung von kleinen, aber feinen Gewohnheitsänderungen bei täglichen Entscheidungen haben die größte positive Wirkung auf sich selber und das eigene Umfeld.

Die eigenen Gewohnheiten ändern (also sich selber ändern) ist schwer. Noch schwerer ist es aber, andere ändern zu wollen. Durch regelmäßige kritische Reflektion der Auswirkungen der eigenen Entscheidungen wird es aber relativ leicht, bei sich selber anzufangen.

### Philosophie
* siehe Hintergrund/philosophie.md

Hinweise
--------
* Ziele:
    * Informationssammlung
    * Skepsis, Zweifel und Vorbehalte gegenüber ethisch motivierten und rationalen Entscheidungen durch Information abbauen, anstelle sie mit Desinformation zu schüren
    * Etablierung einer "Muss das sein?"-Herangehensweise bei regelmäßigen Entscheidungen
    * Lösungs- und handlungsorientiert sein / vor der eigenen Haustür kehren

* Immer offen bleiben und eine gesunde Skepsis an den Tag legen (vor allem gegenüber rein geldgetriebenen Werbebotschaften). Lernen, eigenen Schlussfolgerungen zu vertrauen. Über die Themen reden und sich viel mit Menschen im eigenen Umfeld austauschen.

* Nur wer die aktuelle Situation kennt, kann informierte Enscheidungen treffen.

* Sich informieren wie die eigene Psyche funktioniert (siehe [Psychologie](../Hintergrund/psychologie.md)), um gewappnet gegenüber gezielter Beeinflussung zu sein. Andere aufklären.

* Relativierungen vermeiden; denn was ist, wenn das jeder sagt?
    * "Ach das bisschen Moorverlust. Ich kaufe ja immer nur ganz wenig Torf."

* Meinungen nicht mit Argumenten verwechseln. Auf rhetorische Tricks, die sich als Argumente tarnen, achten. Scheinargumente als solche entlarven.

* Mit der Annahme, dass die meisten (nicht alle) Menschen im Grunde "gut" sein wollen und daher offen für oben stehende Themen sind, liegt man in der Regel richtig. Man schaue sich dazu sein persönliches Umfeld an.

* Versuchen, zu viele diffuse, perspektivlose, negative Nachrichten zu vermeiden (das hat negativen Einfluss auf das Gemüt und die eigene Handlungsfähigkeit). Stattdessen konkrete, positive, lösungsorientierte Informationensquellen aufsuchen.

* Im Gegensatz zu Vor-Internet-Zeiten kann sich heute _jeder_ Mensch schnell und einfach über aktuelle Zustände und deren Auswirkungen informieren. Die meisten Probleme und deren Ursachen und Verursacher sind bekannt. Nach der Informationsbeschaffung kommt die Handlung.

* Gewahr sein, dass es vereinfachende und verkürzte Aussagen gibt, die in die Irre leiten. Mit Differenziertheit begegnen.

* Die positive Kraft des menschlichen Gestaltungswillens ist nicht zu unterschätzen.

* Nicht in jeder Lebenssituation sind Menschen handlungsfähig (wirtschaftliche Situation, Abhängigkeit von nicht wohlgesonnenen Menschen etc.). Diesen Menschen sollte man helfen.

* Wer z. B. aus Bequemlichkeitsgründen nicht selber handeln will, kann denen positiv gegenüberstehen, die es können und wollen. Menschen freuen sich über jede ehrliche, positive Unterstützung.

* Nur "bewusster" Konsum ist nicht ausreichend. Man kann auch sehr bewusst dauerhaft nachteilige Dinge konsumieren.

* Beim Durchlesen des Materials kann ein Lähmungsgefühl auftreten; dieses gilt es zu überwinden, siehe [Psychologie](../Hintergrund/psychologie.md)

* About
    * Welche Kriterien gibt es
    * Tipps zur praktischen Umsetzung
    * Stichworte:
        * ethischer Konsum
        * nachhaltige Lebensweise
        * eigene Lebensführung
        * resourcenschonend leben

Ethische Grundlagen
-------------------
### Die Goldene Regel
* Die "Goldene Regel" (Gehe mit anderen so um, wie du selber behandelt werden willst)
    * Besonders wenn das konsumierte Produkte Genuss- oder Unterhaltungszwecken dient, sind die Auswirkungen umso kritischer zu betrachten
* [Kategorischer Imperativ](https://de.wikipedia.org/wiki/Kategorischer_Imperativ)
* Zitat: "Das, was ich will, das mir zugute kommt, muss ich auch bereit sein, anderen zu geben. Das ist in der Tradition der Aufklärung" (Karl Lauterbach, SPD, 2020)

Was tun?
--------
* Sammlung von positiven und konkreten Handlungsoptionen, wenn bestimmte Problemfelder als behebenswert erkannt wurden.

### Kleine Schritte
* Selbst in kleinem Rahmen kann man aktiv werden

* Kuchen die Welt retten
    * Veganen Bio-Schoko-Kuchen mit Erdbeeren backen

* Verantwortung für die Verpflegung auf Veranstaltungen?
    * Mutig einen Caterer engagieren, der ein veganes oder bio-veganes Büffet anbieten kann.

* Abhängigkeit und Beeinflussung von und durch große IT-Konzerne/Werbung erkannt?
    * dann siehe [Freie Software](FreieSoftware)

### The Lifelong Activist
* Buch: http://lifelongactivist.com - "The Lifelong Activist: How to Change the World Without Losing Your Way (Lantern Books, 2006)"
    * referenced by stallman

Positive Beispiele
------------------
* Sammlung von positiven Beispielen, das heißt konkrete Menschen, Organisationen, Vorgehenweisen und Wegen, die konkret beschritten werden oder sich beschreiten lassen, um angesichts von hochkomplexen Zukunftsfragen den Kopf nicht in den Sand zu stecken und dadurch anderen (die möglicherweise nur das hier und jetzt interessiert) die Entscheidungen zu überlassen.

* Die positiven Beispiele ergeben nur Sinn, wenn man sich angesichts aktueller Probleme mehr in der "Denial"-Phase befindet.

* Suche nach "Positive Beispiele"

Andere Sammlungen
-----------------
* http://www.onefish.org
