Zoo und Zirkus / daily-choices-info-package
===========================================

[zurück](../../..)

<!-- toc -->

- [Zoo](#zoo)
  * [2020: Zoos sind dauerhafte Einrichtungen [...] zwecks Zurschaustellung](#2020-zoos-sind-dauerhafte-einrichtungen--zwecks-zurschaustellung)
  * [Aktiv Aufklärung vorantreiben? Leider nein.](#aktiv-aufklarung-vorantreiben-leider-nein)
  * [2020: Great Ape Project](#2020-great-ape-project)
  * [2019: Dortmunder Zoo](#2019-dortmunder-zoo)
  * [Psychopharmaka, 2017](#psychopharmaka-2017)
  * [Offene Fragen für positive Entwicklungen](#offene-fragen-fur-positive-entwicklungen)
  * [Frage: Warum gehen wir in den Zoo?](#frage-warum-gehen-wir-in-den-zoo)
  * [Cartoons](#cartoons)
  * [Elefantenqual im Hannover-Zoo, 2017](#elefantenqual-im-hannover-zoo-2017)
  * [Drogen und Psychopharmaka, 2014](#drogen-und-psychopharmaka-2014)
  * ["Individuengerecht"](#individuengerecht)
  * [Bildung: Zoobesuch fördert Umweltbewusstein?](#bildung-zoobesuch-fordert-umweltbewusstein)
  * [Zoo fördert bestimmte Denkweise](#zoo-fordert-bestimmte-denkweise)
  * [Weitere Infos Zoo](#weitere-infos-zoo)
- [Zirkus mit Tieren](#zirkus-mit-tieren)
  * [Doku über Zirkustiere 2018](#doku-uber-zirkustiere-2018)
  * [90er Jahre Amerika](#90er-jahre-amerika)
  * [Konkrete Beispiele](#konkrete-beispiele)
  * [Fragwürdige Positionen](#fragwurdige-positionen)
  * [Weitere Infos Zirkus](#weitere-infos-zirkus)
- [Menschen](#menschen)
  * [Robert Marc Lehmann](#robert-marc-lehmann)
  * [Haustiere zur Unterhaltung](#haustiere-zur-unterhaltung)
- [Filme](#filme)
  * [Blackfish](#blackfish)
- [Ethik](#ethik)

<!-- tocstop -->

Zoo
---
### 2020: Zoos sind dauerhafte Einrichtungen [...] zwecks Zurschaustellung
* https://dejure.org/gesetze/BNatSchG/42.html - Bundesnaturschutzgesetz
    * "Zoos sind dauerhafte Einrichtungen, in denen lebende Tiere wild lebender Arten
        zwecks Zurschaustellung während eines Zeitraumes von mindestens sieben Tagen im Jahr gehalten werden."
    * https://www.ndr.de/ratgeber/reise/tierparks/Pro-und-Kontra-Tierhaltung-im-Zoo,zoofeier112.html, NDR, 2015
        * ...

### Aktiv Aufklärung vorantreiben? Leider nein.
* Man könnte meinen, dass verantwortungsvolle Zoos sich dafür einsetzen, dass
    schwarze Schafe ausgedeutet werden.
    * Nur sieht man nichts davon. **Zum Beispiel auf den Webseiten Stellung beziehen für die Tiere**
    * Keine Aktion gegen Aquarien (siehe Lehmann)
    * Keine Aktion gegen Zoos, die Psychopharmaka verwenden
    * Keine Aktion gegen die eigene Currywurst
    * Keine Aktion gegen Palmöl direkt am Affenkäfig

### 2020: Great Ape Project
* siehe "Great Ape Project" -> "Lebenslänglich hinter Gittern", z. B. Facebookseite

### 2019: Dortmunder Zoo
* https://tierretter.de/tatort-dortmunder-zoo-stoerchen-werden-fluegel-gestutzt/
    *  Antwort der Bundesregierung: http://dip21.bundestag.de/dip21/btd/18/037/1803792.pdf

### Psychopharmaka, 2017
* https://www.deutschlandfunknova.de/beitrag/das-tiergespraech-psychopharmaka
    * "gaben acht Zoos in Nordrhein-Westfalen an, **Psychopharmaka bei ihren Tieren eingesetzt zu haben**."
        * "Die Zoobetreiber sprechen nur äußerst ungern über den Einsatz dieser Mittel,
            deshalb sind nur wenige Fälle ausreichend dokumentiert."
    * "Bekannt ist, dass die **Delfine im Nürnberger Zoo nahezu täglich das Beruhigungsmittel Diazepam** verabreicht bekamen - und das in immensen Dosen."
        * Update Ende 2018: "Erneut Psychopharmaka bei Delfinen im Tiergarten Nürnberg und in Duisburg" (https://www.presseportal.de/pm/111206/4142166)
    * "Und im **Zoo von London** wurden Pinguinen, die offensichtlich unter einer massiven Winterdepression litten, chemische Stimmungsaufheller verabreicht"
    * "Untersuchungen der **amerikanischen Pathologin Lynn Grinner im Zoo von San Diego** zeigen, dass Kritik berechtigt ist.
        Und dieser Zoo gilt dank der großzügigen und tierfreundlichen Gehege **als einer der vorbildlichsten Zoos** der Welt.
        Grinner obduzierte dort über einen Zeitraum von 14 Jahren alle verstorbenen Tiere.
        Dabei untersuchte sie auch die Kadaver auf **Arzneimittelrückstände**. Sie fand
        "eine erschreckend **hohe Mortalitätsrate durch die Verwendung von Anästhetika und Beruhigungsmittel**"."

### Offene Fragen für positive Entwicklungen
Zielgruppe: offene Menschen mit positiver Haltung gegenüber Verbesserungen

* Gute Arbeit der Mitarbeiter zum Wohle der Tiere. Was ist davon zu halten, im eigenen Verantwortungsbereich mit relativ geringem Aufwand noch viel mehr Tiere zu schützen, z. B. indem die Zoo-Gastronomie auf tierprodukt-frei umgestellt wird?

* Schrittweise Umstellung der Zoo-Gastronomie auf Bio und vegan: Ist das mit dem Umweltschutz-, Bildungs- und Aufklärungsziel des Zoos vereinbar?

* Tierschutz von Tieren außerhalb des Zoos: was würde es wahrgenommen, wenn man in der Kommunikation nach außen vertritt, dass man die Gäste bestmöglichst bewirten möchte, aber im Rahmen der Verantwortung für die Tiere, die ansonsten geschlachtet würden?

* Fortgeschritten: Gibt es Gedanken im Rahmen des Zoo-Selbstverständnisses, die einer schrittweisen Umstellung auf ein arten- und naturschutzfreundliches Gastro-Angebot im Weg stehen würden? (ganz unaufdringlich mit gutem Beispiel vorangehen)

* Beispiele von Gastronomie:
    * Braunschweig: https://zoo-bs.de/besucherservice/gastronomie/
        * "Pommes Frites, Currywurst, Schnitzel, Chicken Nuggets und Kartoffelpuffer"
    * Hannover: https://www.zoo-hannover.de/de/ihr-besuch/gastronomie
        * "Fischspezialitäten und Burger"
    * Reptilium Landau: https://www.reptilium-landau.de/gastronomie-im-zoo/
        * Selbstverständnis: "perfektes Freizeiterlebnis", "ganzjähriges Urlaubsflair", "ein Spaß für die ganze Familie", "Urlaub, Entspannung & Exotik"
            * Aber auch: "stellen wir das Wohlbefinden der Tiere sicher und achten in diesem Rahmen streng darauf, dass diese in einer artgerechten Umgebung heranwachsen"
        * Speisen: Salat mit Ei, Schafskäse, Hähnchen, Currywurst, Chicken Nuggets, Rindswurst, Schnitzel verschiedener Art, Pizza mit Käse und Salami, Wurstsalat, Cheeseburger, Käsespätzle, Spaghetti Bolognese, Flammkuchen
            * vegan: Beilagensalat, Pommes und Getränke
    * Osnabrück: https://www.zoo-osnabrueck.de/ihr-zoobesuch/gastronomie-zooshops/
        * "Von Burgern und Wraps über Salate und frische Backwaren bis hin zu Klassikern wie Pommes und Bratwurst ist für jeden Geschmack etwas dabei", Schnitzel, Garnelen, vom Sodexo-Team, Hot Dog, Chicken Crossis, Wrap mit Minischnitzel
            * Möglicherweise vegan: Fladenbrot mit Salat und Falafel, Cous Cous Gemüsepfanne
    * Frankfurt: https://www.zoo-frankfurt.de/ihr-zoo-besuch/gastronomiehotel/
        * Selbstverständnis:
            * "Tiere erleben - Natur bewahren"
            * "Ein Hauptanliegen des Zoo Frankfurt ist seine Verpflichtung zu Arten- und Naturschutz gemäß den Vorgaben der "Welt-Zoo-Naturschutz-Strategie" (2015)"
            * "Die Zoologische Gesellschaft Frankfurt von 1858 e.V. (ZGF) ist eine international agierende Naturschutzorganisation mit Sitz in Frankfurt."
                * "Der Bewahrung der natürlichen Lebensgrundlagen."
                * "Naturschutzbildung und -erziehung"
            * "Werden auch Sie Teil einer aktiven Gemeinschaft, die bereit ist, positive Veränderungen für die Umwelt herbeizuführen."
                * "Kaufen Sie lokale und saisonale Bioprodukte"
                * "Weniger verbrauchen"
            * "Biodiversität - Warum wir alle etwas tun müssen"
            * "Naturschutzbotschafter"
                * "gefördert durch die Allianz Umweltstiftung und die Stiftung Polytechnische Gesellschaft Frankfurt am Main."
                * "Wer Interesse an Tieren und am Naturschutz hat", ...
                    * https://naturschutzbotschafter.fzs.org/de/
            * "Fisch und Meeresfrüchte aus zertifiziert nachhaltiger Fischerei"
        * Speisen: "Brat- und Currywurst, Pommes und Erbseneintopf", Grillwagen, Waffel-Pavillon
    * Heidelberg:
        * ...
        * https://www.zooschule-heidelberg.de/kontakt

### Frage: Warum gehen wir in den Zoo?

(Zurschaustellung)

Aufgabe:

1. Schaue bei einem großen Zoo auf die Webseite und suche die Zoo-Philosophie.
    Dort steht vermutlich unter anderem geschrieben, dass den Zoo-Verantwortlichen das Wohl der Tiere wichtig ist.

2. Im nächsten Schritt schaue, was man auf dem Zoogelände den Gästen hauptsächlich zu Essen anbietet.
    Und stelle fest, dass bei der Produktion dieser Produkte das Tierwohl an letzter Stelle steht.

3. Frage doch einfach mal beim Zoo nach, warum das so ist. :)

### Cartoons
![Bild 1](https://pbs.twimg.com/media/CY0wCcsUMAMgHW3.jpg "Springfield Zoo - See your animal friends in prison")

(Bildquelle siehe Link)

![Bild 2](http://freewpzelephants.org//wp-content/uploads/2009/11/Dan-Piraro-ele-cartoon.jpg "Elephant in Zoo")

(Bildquelle siehe Link, "So random people can stare at me for 45 seconds. What're _you_ in for?")

### Elefantenqual im Hannover-Zoo, 2017
* Hannover-Zoo-Recherchen von Report Mainz
    * Elefantenhaken für Kunststückchen
    * Link: http://www.swr.de/report/zoo-hannover-tierpfleger-schlagen-junge-elefanten/-/id=233454/did=19298302/nid=233454/nuczwo/
        * Video: [Hannover-Zoo, Elefanten, Report Mainz, Das Erste"](https://www.youtube.com/watch?v=OHnuDThJ02M), 2017, 5 min, tag:class?
            * Wie bringt man Elefanten bei Männchen zu machen oder auf zwei Beinen zu balancieren?
            * Allgemeine Zoobilder vom Mai 2016, wo die Elefanten sich gegenseitig am Schwanz anfassen und eine Parade laufen; ganz wie im Zirkus
            * Malträtierung mit dem Elefantenhaken
            * Hessische Tierschutzbeauftragte: würde sowas im Zoo nicht vermuten, wegen des Anspruchs, den der Zoo stellt. Es macht sie traurig.
            * Zooverantwortliche: "Das ist ein Führen des Tieres". Drauf achten, wie das gerechtfertigt wird.
            * emotionaler Stress, ständige Bedrohung
            * Positiveres Beispiel Zoo Heidelberg
            * Kurzinterview Chef Zoo FFM

### Drogen und Psychopharmaka, 2014
* Bericht auf welt.de: ["Die Tiere in deutschen Zoos stehen unter Drogen"](https://www.welt.de/wissenschaft/umwelt/article127612535/Die-Tiere-in-deutschen-Zoos-stehen-unter-Drogen.html), 2014
    * "Angeblich leben Zootiere in Deutschland glücklich. Aber es gibt Hinweise für den systematischen Einsatz von Psychopharmaka.
        Ein Affe, der durchdreht, bekommt beispielsweise einfach Diazepam."
    * "Auch der Zürcher Zoo gibt zu, seinen Tieren hin und wieder Antidepressiva zu verabreichen, im Zoojargon die „rosarote Brille“."

### "Individuengerecht"
* Laut einem ehemaligen Zoodirektor werden Tiere im Zoo nicht "artgerecht", sondern "individuengerecht" gehalten.
    * Das heißt z. B. ein Eisbär in der freien Wildbahn wandert kilometerweit, weil er Hunger hat und sich Nahrung beschaffen muss
    * Im Zoo ist er rundum versorgt und braucht gar nicht so lange wandern.
    * Dem Eisbär fehlt also nichts. Alles super. Wer etwas nicht kennt, vermisst es auch nicht.

### Bildung: Zoobesuch fördert Umweltbewusstein?
* Leider eher nein, siehe z. B. https://de.wikipedia.org/wiki/Zoo#Kritik ::human_attitudes
    * "Eine von der amerikanischen Association of Zoos and Aquariums (AZA) im Jahr 2007 veröffentlichte und von Zoos und Aquarien vielfach zitierte Studie, die nahelegte, dass Zoos erhöhte Aufmerksamkeit und Verhaltensänderungen in der Öffentlichkeit bezüglich Arten- und Umweltschutz bewirken, wurde 2010 seitens mehrerer Wissenschaftler auf ihre Stichhaltigkeit geprüft. Die Wissenschaftler kamen zu dem Ergebnis, dass die Studie aufgrund methodologischer Mängel nur sehr geringe Aussagekraft besitze und dass es bis heute **keine Hinweise gebe, dass Zoos und Aquarien das Verhalten und die Einstellungen von Besuchern hinsichtlich Naturschutz positiv beeinflussen.**"

### Zoo fördert bestimmte Denkweise
* siehe https://de.wikipedia.org/wiki/Zoo#Kritik
    * "Dale Jamieson argumentiert in seinem Essay Against Zoos für die Abschaffung von Zoos: Demnach erforderten die Moral und „unser“ eigenes Überleben, dass „wir“ lernen, als „eine Art unter vielen“ zu leben. Zoos betonen aber einen pauschalen Unterschied zwischen Menschen und Tieren und förderten dadurch ein falsches und gefährliches Verständnis „unseres“ Platzes in der natürlichen Ordnung. Deshalb wäre es sowohl für Menschen wie Tiere besser, wenn Zoos abgeschafft würden"

### Weitere Infos Zoo
* Hervorzuhebende Organsiationen, Artikel
    * [Das Leben im Zoo](http://www.tierrechte-bw.de/index.php/zoo.html)
    * [END ZOO](http://endzoo.at/wp/)
        * [Beispiel Zoo Landau](http://endzoo.at/wp/umweltskandal-im-zoo-landau/)
        * ["Zoo-Märchen vom Zoo-Naturschutz"](http://endzoo.at/wp/zoo-marchen/vom-naturschutz/)
        * ["Die Zoo-Märchen von der Zoo-Bildung"](http://endzoo.at/wp/zoo-marchen/von-bildung/)
            * ["Das Märchen von deutscher Zoo-Transparenz"](http://endzoo.at/wp/zoos-haben-nichts-zu-verbergen/), 2013
        * ["Beispiel Bärenhaltung Innsbruck"](http://endzoo.at/wp/barenhaltung-innsbruck/)

Zirkus mit Tieren
-----------------
### Doku über Zirkustiere 2018
* Video: ["Zirkustiere - Das Leben exotischer Tiere als Entertainer im Zirkus"](https://www.youtube.com/watch?v=0G0HgxItL6k), Y-Kollektiv Dokumentation, 2018, 14 min
    * "Dressierte Wildtiere im Zirkus. In Deutschland sind sie noch erlaubt, führen Kunststücke vor und werden regelmäßig von A nach B transportiert. „Wie soll das jemals artgerecht und im Sinne der Tiere sein?“ – fragen Tierschützer. Zirkusbetreiber des Zirkus Voyage fühlen sich bedroht und missverstanden. Ihre Tiere sind ihr Kapital und das behandle man gut. Unsere Reporterin Antonia Lilly Schanze hat sich die Sorgen eines Zirkusdirektors angehört und war 40 Stunden wach, um Tierschützer bei einer geheimen Mission zu begleiten."

* https://www.stern.de/panorama/gesellschaft/tiere-im-zirkus---das-ist-sehr-schaedlich--die-leiden-massiv--7846426.html, 2018
    * "Peter Hübner ist einer der engagiertesten Tierrechtler und Gegner von Tierdressuren. Seit Jahren reist er Zirkussen hinterher und konfrontiert die Unternehmer mit Missständen. Als ehemaliger Rodeoreiter weiß er, wovon er spricht."

### 90er Jahre Amerika
* Video: [Anti-Circus TV Commercial](https://www.youtube.com/watch?v=u39A54AKjME), späte 90er Jahre, 2 min
    * Misshandlungen finden nur hinter den Kulissen statt
        * Kino-Film, der dies ebenfalls verdeutlicht: ["Wasser für die Elefanten"](https://de.wikipedia.org/wiki/Wasser_f%C3%BCr_die_Elefanten), 2011

### Konkrete Beispiele
* https://de.wikipedia.org/wiki/Zirkus_Charles_Knie#Kritik
    * Auf der eigenen Webseite schreibt der Zirkus "Kein Tier wird gezwungen etwas zu machen – die Dressurarbeit basiert auf natürlichen Verhaltensweisen der unterschiedlichsten Tiere auf Kommando."
        * Da jedes Tier einen individuellen Willen hat, wäre interessant zu wissen, was mit Tieren passiert, die bestimmte Sachen nicht machen wollen?
        * Vergleich: sogar McDonalds schreibt in seinem [Nachhaltigkeitsbericht 2015](https://www.mcdonalds.de/documents/75202/3918621/20160808_McDonalds_CR-Report_kompakt_2015_DE.pdf)
            * "Wir kümmern uns um mehr Tierwohl" (obwohl es in den Schlachthäusern weiterhin mehr als traurig aussieht)
        * Also alles in Ordnung?
    * 2012 https://www.noz.de/lokales/osnabrueck/artikel/252733/tierrechtsinitiative-protestiert-vor-dem-zirkus-knie
        * Interessanter Besucher-Kommentar ganz unten

### Fragwürdige Positionen
* [Das Märchen vom großen Leiden der Zirkustiere](https://www.welt.de/dieweltbewegen/article13746841/Das-Maerchen-vom-grossen-Leiden-der-Zirkustiere.html)
    * Verhaltensbiologe Immanuel Birmelin
    * "Die Amtstierärzte waren zufrieden, sie hatten nichts Gravierendes zu bemängeln. Wenn privaten Tierhaltern so auf den Zahn gefühlt würde, wäre ich glücklich. Warum knöpfen sich die Tierschützer nicht den Heimtiersektor vor, die Reptilienhalter oder Fans exotischer Vögel? Viele Papageien sterben einen vereinsamten Tod."
        * Amtstierärzte überprüfen nur das, was gesetzlich vorgeschrieben ist
        * Rhetorischer Trick: Begründung durch Ablenkung: nur weil es im Heimtiersektor schlimmer sei, heißt nicht, dass im Zirkus alles gut ist.
* [Aktionsbündnis „Tiere gehören zum Circus“](http://www.tiere-gehoeren-zum-circus.de)

### Weitere Infos Zirkus
* https://de.wikipedia.org/wiki/Zirkus#Kritik_an_der_Tierhaltung_in_Zirkusunternehmen

* Vor allem versteckte Aufnahmen zeigen immer wieder, dass gerade unter Zeitdruck und wirtschaftlichen Zwängen diese idyllische Idealwelt für die Tiere leider nicht funktioniert
    * https://www.youtube.com/watch?v=i4QB7w0l9GA - "Dressur der Tiere im Zirkus" (Welcher Zirkus würde öffentlich zugeben so zu arbeiten? Aber dennoch passiert es immer wieder), 2016
    * https://www.youtube.com/watch?v=ZPu78i36NKc - Zirkus Krone, PETA 2016
* Unterhaltung auf Kosten der Tiere?
    * Der Mensch hat viele andere Möglichkeiten sich unterhalten zu lassen.
* Positive Beispiele: Cirque du Soleil
    * Weltberühmter Zirkus ganz ohne Tiere: https://de.wikipedia.org/wiki/Cirque_du_Soleil

Menschen
--------
### Robert Marc Lehmann
* [Robert Marc Lehmann](http://www.robertmarclehmann.com/home/)
    * was Zoos und Aquarien für die Tiere bedeutet
    * "Wie der Verzicht auf Nutella und Plastik die Welt retten hilft" (https://www.kreiszeitung.de/lokales/verden/oyten-ort54165/verzicht-nutella-plastik-welt-retten-hilft-9593299.html)
        * "Seitdem arbeitet Lehmann gegen die Zurschaustellung von Tieren in Gefangenschaft, in Zoos und Aquarien", insbesondere Fische

### Haustiere zur Unterhaltung
* Wellensittiche: https://etnev.de/tierschutz/artgerechte-tierhaltung/wellensittiche

Filme
-----
### Blackfish
* Film: [Blackfish](https://www.youtube.com/watch?v=w2vG_Ifu4zg), 2013, 83 min, zum Thema Haie und Fisch-Shows
    * https://de.wikipedia.org/wiki/Blackfish_(Film)
    * Inhalt
        * 12 min: Wildfang: Es werden alle Orcas zusammengetrieben (auch mit Hilfe von Flugzeugen, weil die Orcas bereits schon mal gefangen wurden und die Fänger zu täuschen versuchten).
            * Dann werden nur die jungen Tiere aus der Familie entfernt.
            * Ein ehemaliger Fänger berichtet wie er sich gefühlt hat als er merkte, was er da tut. (it's like kidnapping a child from its mother)
            * Vier "unbeteiligte" Wale kamen in den Netzen um.
        * 23 min: Transfer from Sealand to SeaWorld after first accident
        * 26 min: Orcas: soziale Interaktionen, großes Gehirn, hohe Emotionalität, Selbstwahrnehmung, Bewusstsein, Intelligenz
        * 29 min: enge Käfige außerhalb der Show-Zeiten, soziale Probleme aufgrund der Enge
        * 31 min: Wal leckt mit großer Zunge an Scheibe
        * 35 min: frühere Ansagerin und Trainerin ist es mittlerweile total peinlich, dass sie regelmäßig sagte, die Tiere machten das nicht, weil sie müssen, sondern weil sie wollen. Mittlerweile Einsicht, dass die Beziehung zu den Tieren praktisch nur darauf basierten, dass sie gefüttert wurden.
        * 37 min: sehr ruppiger Umgang mit den Tieren (gelinde gesagt) (Mutter trauert um weggenommenes Kind)
        * 41 min: siehe https://de.wikipedia.org/wiki/Schwertwal#Sterblichkeit_und_Lebenserwartung
        * 43 min: whale on whale aggression ist üblich im Park (weil zusammengemixt aus unterschiedlicher Herkunft)
        * 46 min: "trainer error"
        * 48 min: trainer error, kaputter Arm; als anderer Trainer bekommt man davon nichts erzählt
        * 55 min todo
    * https://de.wikipedia.org/wiki/Blackfish_(Film)
        * Mehrere Bands und Sänger sagten ihren Auftritt nach dem Film ab.
        * Besucherzahlen sanken stark, nachdem die Leute den Film gesehen haben
            * So ein Film kann eine große Wirkung haben.
    * SeaWorld hat sich große Mühe gemacht, [möglichst alle Aussagen des Films zu widerlegen](https://seaworldcares.com/the-facts/truth-about-blackfish/), was nicht wirklich gelungen ist. [32-seitiges Dokument](http://da15bdaf715461308003-0c725c907c2d637068751776aeee5fbf.r7.cf1.rackcdn.com/adf36e5c35b842f5ae4e2322841e8933_4-4-14-updated-final-of-blacklist-list-of-inaccuracies-and-misleading-points.pdf) mit stichprobenartig - unaussagekräftigen oder ablenkenden - Details
        * Beispiel S. 5: "SeaWorld has not captured whales in nearly 34 years. The last such collection by SeaWorld took place in 1979."
            * Der Protagonist des Films, der Orca Tilikum, wurde aber dennoch später (1983) wild gefangen, in Island, siehe [wiki](https://en.wikipedia.org/wiki/Tilikum_(orca)) oder [nationalgeographic](http://news.nationalgeographic.com/2017/01/tilikum-seaworld-orca-killer-whale-dies/). Zwar nicht direkt von SeaWorld, aber er wurde nach der Schließung von https://en.wikipedia.org/wiki/Sealand_of_the_Pacific an SeaWorld verkauft.
    * ["Reps. Schiff and Huffman Discuss Amendment Directing USDA to Act to Protect Captive Orcas"](https://www.youtube.com/watch?v=qegTE0zcm9I), 2014, Abgeordnete fordern, die Gesetzgebung neusten wissenschaftlichen Erkenntnissen anzupassen, als Reaktion auf den Film
    * krass: siehe [wiki](https://de.wikipedia.org/wiki/SeaWorld#Kritik): "Im Jahr 2011 schleuste Seaworld einen Agent Provocateur bei der Tierschutzorganisation PETA ein, um zu Straftaten anzustiften. Im Februar 2016 gab Seaworld dieses Verhalten öffentlich zu", siehe z. B. [marketwatch.com](http://www.marketwatch.com/story/seaworld-admits-it-sent-spies-to-infiltrate-peta-2016-02-25), Rechtfertigung: They did it first.
* siehe auch Film: Die Bucht
    * Hintergründe zu Delfinarien

Ethik
-----
* siehe diskriminierung-speziesismus.md
