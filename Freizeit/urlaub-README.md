Urlaub / daily-choices-info-package
=====================================

[zurück](../../..)

<!-- toc -->

- [Flugreisen](#flugreisen)
  * [Erste Frage](#erste-frage)
  * [Allgemein](#allgemein)
  * [Kompensation?](#kompensation)
  * [Am Boden bleiben](#am-boden-bleiben)
- [Kreuzfahrtschiffe](#kreuzfahrtschiffe)
  * [Allgemein](#allgemein-1)
  * [Stand 2018](#stand-2018)
- [Skifahren mit Schneekanonen](#skifahren-mit-schneekanonen)
- [Golf auf dem falschen Platz](#golf-auf-dem-falschen-platz)
- [Stierkampf](#stierkampf)
- [Sonnencreme](#sonnencreme)
- [Motorrad](#motorrad)
- [Alternativen](#alternativen)

<!-- tocstop -->

Flugreisen
----------
### Erste Frage
* "Und? Habt ihr ausgeglichen?" oder "Und? Wie hast du ausgeglichen?"
* Wie oft kann man von D. nach USA fliegen (und zurück), wenn es jeder machen wollte und es gerecht zugehen soll?
    * Ein Inder alle 10 Jahre.

### Allgemein
* siehe z. B. [umweltbundesamt.de/wer-mehr-verdient-lebt-meist-umweltschaedlicher](https://www.umweltbundesamt.de/presse/pressemitteilungen/wer-mehr-verdient-lebt-meist-umweltschaedlicher), 2016
    * "**Vor allem Fernflüge**, das Auto, der Dämmstandard der Wohnung und deren Größe **und der Konsum von Fleisch** entscheiden darüber, ob jemand über oder unter dem CO2-Durchschnittsverbrauch liegt. Daher haben Menschen mit hohem Umweltbewusstsein laut Studie nicht zwangsläufig eine gute persönliche Ökobilanz. Menschen aus einfacheren Milieus, die sich selbst am wenigsten sparsam beim Ressourcenschutz einschätzen und die ein eher geringeres Umweltbewusstsein haben, belasten die Umwelt hingegen am wenigsten."
    * ["Umweltbewusste Klimasünder"](http://www.klimaretter.info/konsum/hintergrund/21707-umweltbewusste-klimasuender), 2016, ref auf Umweltbundesamt
* Also wer gut verdient, Vorbild sein und Flugreisen zum reinen Privatvergnügen aktiv meiden.

### Kompensation?
* "Natürlich wäre es am besten, gar nicht zu fliegen, aber davon sind wir weit entfernt.
    Daher ist die CO2-Kompensation eine gute Möglichkeit, etwas für den Klimaschutz zu tun" - Klimaschutz-Unternehmen Artitik
    * eine gute Möglichkeit? Das "etwas" ist das gute Gewissen
    * siehe auch https://www.geo.de/natur/nachhaltigkeit/19468-rtkl-gruene-produkte-warum-nachhaltiger-konsum-nicht-funktioniert-14

### Am Boden bleiben
https://www.ambodenbleiben.de, #SavePeopleNotPlanes, Planet over Profit, wenigstens gerechte Steuern zahlen

Kreuzfahrtschiffe
-----------------
### Allgemein
* https://www.nabu.de/umwelt-und-ressourcen/verkehr/schifffahrt/kreuzschifffahrt/index.html
* Schweröl, Schwermetalle, Schadstoffen etc
* Schlimm für Umwelt und Menschen, die in Hafenstädten leben

### Stand 2018
* ["Wie Kreuzfahrtschiffe die Natur verpesten"](https://www.ardmediathek.de/tv/UNKRAUT/Wie-Kreuzfahrtschiffe-die-Natur-verpeste/BR-Fernsehen/Video?bcastId=14913036&documentId=54244776), ARD, 2018, 6 min
    * "2,2 Millionen Deutsche machten im letzten Jahr Urlaub auf dem Schiff. Doch Kreuzfahrtschiffe verbrennen Schweröl, einen giftigen Stoff. Binnenschiffe fahren mit Diesel, doch ihre Umweltbilanz ist nicht besser. Wir begleiten die Wasserschutzpolizei bei einer Kontrolle."
    * Zahlen: "Ein Kreuzfahrtschiff stößt auf einer Strecke soviel Schadstoffe aus wie 1 Million Autos auf derselben Strecke"
    * Kritik: es gibt keine nennenswerten Umweltauflagen für Schiffe
    * nicht nur Seeschiffe (meist Schweröl), sondern auch Binnenschiffe (Diesel) problematisch
    * Wasserschutzpolizei kontrolliert ein Flusskreuzfahrtschiff
        * Schiffskläranlagen werden gern auch mal umgangen, mittels Bypass

Skifahren mit Schneekanonen
---------------------------
* ...

Golf auf dem falschen Platz
---------------------------
* siehe https://de.wikipedia.org/wiki/Golfplatz#%C3%96kologische_Aspekte, ::flächenfraß
    * Flächenverbrauch
    * Wasserverbrauch
    * Einsatz von Dünger und Pflanzenschutzmitteln
    * Artenvielfalt
    * "Am einen Ende der Skala befinden sich Golfplätze mit einer derart konsequenten ökologischen Ausrichtung, dass sie in Deutschland sogar in Naturparks genehmigt und mit Umweltpreisen ausgezeichnet wurden."
        * Welche sind das konkret?
* Es geht auch vorbildlich ohne Gift (derzeit die Ausnahme)
    * ARD-Beitrag "Ökologisch putten" (https://www.ardmediathek.de/ard/player/Y3JpZDovL2JyLmRlL3ZpZGVvLzc0MzE2NTg1LTgxMzQtNGQ5ZC1hOTFjLTNkOGI1MmMxNThhMA/), 2018

Stierkampf
----------
* https://de.wikipedia.org/wiki/Stierkampf#Argumente_gegen_den_Stierkampf
* ["Most Awesome Bull Fight Fails"](https://www.youtube.com/watch?v=Mix_mtmIvYQ), 2017, 5 min
* ["Bull Fights Gone Wrong Compilation 2016"](https://www.youtube.com/watch?v=JcqbHXp0I08), 2016
* Beitrag von Christian Nürnberger Weihnachten 2017: "Die Welt wie ein Kind sehen": "Das in der Krippe akzeptiert nicht, dass die Welt so ist und bleibt, wie sie ist"

Sonnencreme
-----------
* 2017: "Sonnencreme zerstört das Meer" - https://www.sueddeutsche.de/wissen/sonnenschutz-sonnencreme-zerstoert-das-meer-1.3571444

Motorrad
--------
* "mittendrin: Bikerhochburg Schwarzwald", 2020

Alternativen
------------
* Im Internet nach Bildern, Videos und Erfahrungsberichten von Menschen suchen, die bereits da waren, wo es hingehen sollte.
* Regionale Radtouren planen (z. B. http://radservice.radroutenplaner.hessen.de)
* https://www.outdooractive.com/de/
* Kreativ werden
