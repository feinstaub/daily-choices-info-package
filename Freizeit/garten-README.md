Garten / daily-choices-info-package
===================================

[zurück](../../..)

<!-- toc -->

- [Torf-Problematik](#torf-problematik)
  * [Lösung](#losung)
- [Grillen](#grillen)
  * [Grillkohle](#grillkohle)
  * [Grillen: Gesundheit](#grillen-gesundheit)
  * [Grillen: Alternativen](#grillen-alternativen)
- [Gartengifte schädigen Bienen](#gartengifte-schadigen-bienen)

<!-- tocstop -->

Torf-Problematik
-----------------
* https://www.bund.net/naturschutz/moore-und-torf/torffrei-gaertnern/
    * Moore sind wichtig für die Umwelt.
    * Torf wird gewonnen, indem Moore zerstört werden.
    * "von den jährlich in Deutschland zehn Millionen Kubikmeter verbrauchten Torf rund zweieinhalb Millionen an Freizeitgärtner verkauft! Um diese Nachfrage zu bedienen, kommt schon heute ein großer Teil der in Deutschland verwendeten Torfe aus den baltischen und russischen Hochmooren."

* Große Mengen an CO2 werden durch den Abbau freigesetzt.

* siehe auch https://www.vsr-gewässerschutz.de/tipps-tricks/torf/

### Lösung
* Blumenerde ohne Torf kaufen, siehe z. B. hier: https://www.bund.net/naturschutz/moore-und-torf/torffrei-gaertnern

Grillen
-------
### Grillkohle
* ["Das schmutzige Geschäft mit der Grillkohle](https://www.ardmediathek.de/tv/Reportage-Dokumentation/Das-schmutzige-Gesch%C3%A4ft-mit-der-Grillkoh/Das-Erste/Video?bcastId=799280&documentId=53736872), ARD, 03.07.2018, 45 min
    * "Sommerzeit ist Grillzeit: In keinem Land der EU wird mehr Holzkohle verbraucht als in Deutschland. Doch das Sommervergnügen hat seine Schattenseiten. Dahinter verbirgt sich ein Milliardengeschäft, das unter anderem Urwälder zerstört." und Ausbeutung lokaler Bevölkerungen
    * Zahlen: 250.000 Tonnen Holzkohleverbrauch pro Jahr in Deutschland fürs Grillen
        * benötigt, wenn in Deutschland angebaut würde, eine Waldfläche von 500.000 Fußballfeldern; nicht darstellbar
    * Großteil Import aus Ländern außerhalb der EU
        * In Nigeria, Ukraine, Paraguay werden dafür Urwälder illegal zerstört
    * Gesetzeslücke: Bei Import von Stammholz muss nachgewiesen werden, dass es legal geschlagen wurde; bei Holzkohle nicht
        * Der WWF in Belgien setzt sich dafür ein, diese zu schließen.
    * Marktstudie 2017
        * Zwei der ca. 30 Marken bieten Holzkohle oder Briketts aus Holz aus Deutschland an
        * auch Tropenholz
        * im Vergleich zum Vorjahr keine Besserung
        * auch 2 FSC-/PEFC-zertifizierte Säcke haben Tropenholz; verkauft bei N...to und L..l
    * Keine Transparenz
    * 1/3 der Kohle für D. stammt von polnischen Händlern
        * ist Drehscheibe von außereuropäischer Grillkohle für die europäischen Supermärkte
        * konkrete Firmen werden benannt
    * die FSC-Führungspersonen tun sich mit der Aufklärung und Ahndung von starken Verdachtsfällen (Holz aus Nigeria) ziemlich schwer
    * Nigeria: höchste Entwaldungsrate der Welt (50 % in den letzten 20 Jahren)
        * Regierung hat Herstellung von Holzkohle verboten, aber es wird trotzdem gemacht (wegen Armut und Abhängigkeit von Einnahmen)
        * Leute dort kochen mit Kerosin und brauchen selber keine Holzkohle
            * Problem ist also Nachfrage aus den Importländern
        * Wertschöpfung am Staat vorbei, Korruption
    * weiter 13:30 Min: ...

Lösung: Genau drauf schauen, woher die Kohle kommt. Im Zweifel (wenn z. B. nichts draufsteht) im Regal stehen lassen und bei der Geschäftsführung des Verkäufers nachfragen.

### Grillen: Gesundheit
* ["Freisetzung von Metallen aus emaillierten Grillrosten: Einige geben zu viel ab"](https://www.bfr.bund.de/cm/343/freisetzung-von-metallen-aus-emaillierten-grillrosten-einige-geben-zu-viel-ab.pdf), 2018

### Grillen: Alternativen
* Elektrogrill
* ["Eine süsse Alternative zur Bratwurst: Gegrillte FAIRTRADE-Banane"](https://www.dieoption.at/eine-suesse-alternative-zur-bratwurst-gegrillte-fairtrade-banane-gefuellt-mit-fa/), 2018

Gartengifte schädigen Bienen
----------------------------
* ["Neonikotinoide - Gartengifte schädigen Bienen weiter"](https://www.spektrum.de/news/gartengifte-schaedigen-bienen-weiter/1580204), 2018
    * "Um bestäubende Insekten zu schützen, hat die Europäische Union die Nutzung verschiedener Pestizide in der Landwirtschaft untersagt. Doch in Gärten wird fleißig weiter gesprüht."
    * ...
    * "Seit 2013 dürfen die drei Neonikotinoide Clothianidin, Imidacloprid und Thiamethoxam nur noch sehr eingeschränkt ausgebracht werden. In Gewächshäusern sind sie allerdings weiter erlaubt, ebenso wie für Privatleute bei der Bekämpfung von Blattläusen und anderen vermeintlichen Schädlingen."
    * ...
    * "Der an der Studie beteiligte Dave Goulson rät daher Gartenbesitzern zum Schutz der Bestäuber: »Pestizide im Garten sind unnötig. Ich züchte eine große Menge an Früchten, Gemüse und Blumen in meinem Garten – ohne Einsatz von Spritzmitteln. Es geht auch ohne!«"
