Haustiere / daily-choices-info-package
======================================

[zurück](../../..)

<!-- toc -->

- [Aktuelle Situation](#aktuelle-situation)
  * [2014](#2014)
  * [Entwicklungen](#entwicklungen)
- [Deutsches Tierschutzrecht](#deutsches-tierschutzrecht)
- [Ethik](#ethik)
  * [Adopt, don't shop](#adopt-dont-shop)
  * [Dilemma](#dilemma)
- [Hunde](#hunde)
  * [Veganes Hundefutter](#veganes-hundefutter)
  * [Hundeerziehung](#hundeerziehung)
  * [Hundezucht](#hundezucht)
  * [Der Mops - Nicht süß, sondern gequält](#der-mops---nicht-suss-sondern-gequalt)
- [Tierheime](#tierheime)
  * [Tierfreundlich auch zu Tieren, die wir nutzen: Gute Beispiele](#tierfreundlich-auch-zu-tieren-die-wir-nutzen-gute-beispiele)
  * [Was passiert, wenn im Tierheim kein Platz mehr ist?](#was-passiert-wenn-im-tierheim-kein-platz-mehr-ist)
- [Besitzer](#besitzer)

<!-- tocstop -->

Aktuelle Situation
------------------
### 2014
* Heimtierpopulation in Deutschland: "Nach Angaben des Zentralverbands Zoologischer Fachbetriebe Deutschlands (ZZF) wurden 2012 in Deutschland Heimtiere in mehr als einem Drittel der Haushalte gehalten: 12,3 Millionen Katzen (in 16,5 Prozent der Haushalte), 7,4 Millionen Hunde (in 13,4 Prozent der Haushalte), 7,6 Millionen Kleinsäuger (in 6,2 Prozent der Haushalte, vor allem Zwergkaninchen) und 3,7 Millionen Ziervögel (vor allem Wellensittiche). Darüber hinaus gibt es in Deutschland etwa 2,3 Millionen Aquarien und 2,6 Millionen Gartenteiche mit Zierfischbesatz sowie 800.000 Terrarien, vor allem mit Schlangen und Echsen." (https://de.wikipedia.org/wiki/Heimtier)
    * zu Kaninchen siehe auch Kaninchenfleisch
* "Rund 9,1 Milliarden Euro geben die Bundesbürger pro Jahr für Hunde, Katzen und andere Heimtiere wie Hamster oder Zierfische aus. Das geht aus einer Untersuchung der Universität Göttingen hervor. Zum Vergleich: Eine ähnliche Umsatzsumme verzeichnete mit 9,5 Milliarden Euro 2013 der deutsche Buchmarkt." (inkl. Futter, Tierarzt und Zubehör) (Quelle: [Spiegel 2014](http://www.spiegel.de/wirtschaft/unternehmen/haustiere-deutsche-geben-9-1-milliarden-fuer-tiere-aus-a-1003032.html))

### Entwicklungen
* 2019:
    * Hundekotbeutel aus Plastik => Müll
    * Tierbestattungen, z. B. http://tierbestattungen-worms.de/
* 2017: ["Evidensia-Eigentümer kauft drittgrößte britische Tierarztkette IVC"](http://www.wir-sind-tierarzt.de/2017/01/evidensia-eigentuemer-kauft-drittgroesste-britische-tierarztkette-ivc/)
* 2017: ["Mega-Deal: Mars übernimmt US-Tierklinikkette „VCA“ für 9,1 Milliarden Dollar"](http://www.wir-sind-tierarzt.de/2017/01/mega-deal-mars-uebernimmt-us-tierklinikkette-vca-fuer-91-milliarden-dollar/)
    * "Futtermittelhersteller wird Tierarztmarktführer"
* 2017: ["Mars macht Fiffi wieder mobil"](http://www.sueddeutsche.de/wirtschaft/haustiere-mars-macht-fiffi-wieder-mobil-1.3345301), sueddeutsche.de
    * "Bereits heute ist Mars der größte Produzent von Tiernahrung und Eigentümer einer anderen Kette von Kliniken für Vierbeiner."
    * "Vor allem in den USA sind die Umsätze rund ums Haustier steigend und erstaunlich krisenfest."
    * "Die "Corporatization" sei nicht immer gut für Haustier und Halter"
    * "Wellness und Schönheits-OPs als neue Trends"
    * kaum Regulierung
* 2015: ["Fällt das Kapitalbeteiligungsverbot kommen die Tierarztketten"](http://www.wir-sind-tierarzt.de/2015/04/faellt-kapitalbeteiligungsverbot-kommen-tierarztketten/)
* Menschen, die sich ihr Haustier nicht mehr leisten können:
    * http://www.tiertafel.org
    * http://www.frankfurter-tier-tafel.de/Startseite.html

Deutsches Tierschutzrecht
-------------------------
* Deutsches Tierschutzrecht: Bürgerliches Gesetzbuch (BGB), § 90a Tiere, https://www.gesetze-im-internet.de/bgb/__90a.html
    * "Tiere sind keine Sachen. Sie werden durch besondere Gesetze geschützt. Auf sie sind die für Sachen geltenden Vorschriften entsprechend anzuwenden, soweit nicht etwas anderes bestimmt ist."
        * Tiere sind also keine Sachen, aber es sollen im Zweifel die Vorschriften für Sachen angewendet werden.
* "Der Grundsatz des Tierschutzgesetzes lautet: „Niemand darf einem Tier ohne vernünftigen Grund Schmerzen, Leiden oder Schaden zufügen“ (§ 1 Satz 2)." (https://de.wikipedia.org/wiki/Tierschutzgesetz_(Deutschland))
    * Fragestellung: Ist die Produktion von Turbo-Kuh-Qual-Milch-Schokolade ein vernünftiger Grund?

Ethik
-----
### Adopt, don't shop
* "The Ethics of Pet Ownership", https://www.youtube.com/watch?v=hrwG1BHdHIk, 2019, Earthling Ed

### Dilemma
* Fragestellung: welche menschlichen Bedürfnisse wiegen es auf, Haustiere für unsere Zwecke zu züchten und sie damit konkreten oder potentiellen Qualen zu unterwerfen?
* Wen streicheln, wen essen?
* ["Tierarztpraxen: Fünfmal pro Woche im ethischen Dilemma"](http://blogs.faz.net/tierleben/2012/01/05/veterinaermedizin-fuenf-ethische-dilemmas-in-der-woche-90/), FAZ, 2012
    * "1. Das Einschläfern eines gesunden Tieres"
    * "2. finanzielle Schwierigkeiten des Tierbesitzers, die es nicht erlauben, eine notwendige Behandlung vorzunehmen"
    * "3. ein Tierbesitzer, der eine Fortsetzung der Behandlung fordert, obwohl es aus Sicht des Tierschutzes geboten erscheint, das Tier von seinem Leiden zu erlösen."
* ["Zuchtethik"](http://www.schnauzer-vom-paderquell.de/dokumente/Zuchtethik_170414.pdf)
    * aus Sicht des Züchters
* ["Hunde züchten – warum? Und falls ja, wie?"](http://www.vdh.de/news/artikel/hunde-zuechten-warum-und-falls-ja-wie/), 2013
    * "Funktion als Polizei-, Blinden- und Rettungshunde"
    * "Hund helfen gegen die Vereinsamung älterer Menschen" (auch Menschen könnten sich um ältere Menschen kümmern)
    * "Hunde sind im Allgemeinen liebenswerte Tiere"
    * "Hunde sind intelligent"
    * "Hund tragen zur Gesundheit bei"
    * "mit Hunden fühlt man sich sicherer"
    * "Über 80% sagten: „Ich mag Hunde einfach.“" (ethische Abwägung?)
    * "Wir brauchen Hunde. Weniger aus betriebswirtschaftlichen Erwägungen sondern weil sie uns und unserer Gesellschaft einfach gut tun." (was ist mit den menschlichen Nachbarn? = 2/3 der Haushalte, siehe oben)
    * ...
    * Fragestellung: Wofür brauchen junge Privatpersonen neu gezüchtete Hunde?

Hunde
-----
### Veganes Hundefutter
* https://vegan4dogs.com/der-vegane-hund/
    * inkl. Hintergrundinfos

### Hundeerziehung
* 2018 Websuche "hundeschule große hunde am halsband ziehen"
    * Leinenruck
        * https://www.pfotenlesen.de/de/tippsinfos/leinenruckneindanke
            * "In vielen Hundevereinen oder Hundeschulen wird der Leinenruck immer noch als „die Erziehungsmethode“ propagiert."
                * Gründe, warum das leider falsch ist: ...
        * http://www.ath-die-hundeschule.de/index.php?option=com_content&task=view&id=428&Itemid=266
            * ...
            * "Neben dem Kehlkopf, der natürlich auch durch eine direkte mechanische Einwirkung wie den Leinenruck verletzt werden kann", ...

### Hundezucht
* Zustände bei der Zucht, wenn nicht hingeschaut wird:
    * [Dubiose Hundezucht](http://www.peta.de/HundezuchtGerbstedt), Video, 2 min
    * [Über Hundezüchter](http://www.veganblog.de/2013/06/des-pudels-kern-der-hundezuchter/), 2013
    * http://www.peta.de/zucht
* Lösungswege:
    * Nachhaltig: Aufhören, Hunde zu Unterhaltungszwecken nachzufragen.
    * Oder: Keine _neu_ gezüchteten Hunde nicht genau bekannter Quelle kaufen, da dies oft mit großem Tierleid verbunden ist. Nur Tierheimtiere kaufen.
* Frage: Wie kann man es erreichen, dass keine neuen Hunde für den west-europäischen Markt produziert werden?

### Der Mops - Nicht süß, sondern gequält
* [„Nicht süß, sondern gequält“ – BTK über Qualzucht bei Mops & Co ](http://www.wir-sind-tierarzt.de/2016/10/btk-flyer-qualzucht-bei-mops-und-co/), 2016, wir-sind-tierarzt.de
    * "Symbol für die Qualzucht: Die Rasse der Möpse mit schweren Atemwegsproblemen (Brachyzephalie)"

Tierheime
---------
Wichtige Arbeit und Kontroversen.

### Tierfreundlich auch zu Tieren, die wir nutzen: Gute Beispiele
* Hamburger Tierschutzverein - mit Tierheim - "Tierliebe fängt beim Essen an"
    * ["Was die Milch anrichtet – SOKO Tierschutz deckt auf"](https://www.hamburger-tierschutzverein.de/tierschutz/sog-nutztiere/11183-was-die-milch-anrichtet-soko-tierschutz-deckt-auf), 2019, Mülln
    * Bild zu Ostern: https://www.hamburger-tierschutzverein.de/images/images/0988_2019-Q2/Ostern-ohne-Tierleid-1.JPG
        * "Ostern ohne Fleisch"
        * "Die Gans bleibt ganz"
        * "Mein Ei gehört mir"
* https://www.tierheim-duesseldorf.de/projekte/veggie-projekt.html
    * "Wer über das gewöhnliche Leben hinaus will, der scheut blutige Nahrung und wählt nicht den Tod zu seinem Speisemeister.“ - Joseph von Görres, 1776-1848, Gelehrter und Publizist
    * "Tierschutz ist Erziehung zur Menschlichkeit!"
* Jugendtierschutzprojekt: https://www.tierheim-duesseldorf.de/projekte/jugendtierschutz/tierethik.html
    * "Der Tierschutzverein Düsseldorf e.V. hat im Rahmen des Jugendtierschutzprojektes eine Zusammenarbeit begonnen mit der „Stiftung für Ethik im Unterricht DAS TIER UND WIR“ Bern/Schweiz. Diese bemerkenswerte Stiftung ist seit Jahren erfolgreich mit Lehre und Informationen über Tiere und ihre Lebensbedingungen in Schweizer Schulen. Dort sorgen ausgebildete Tierschutzlehrer für den fachlichen Hintergrund und bilden damit ein Fundament für modernen und effektiven Tierschutz im Bereich Kinder und Jugendliche."
* "18.10.2018: Wie ich zur Tierschutzlehrerin wurde" - https://www.tierheim-duesseldorf.de/news/neues-am-donnerstag-kopie.html
    * "Ach und übrigens: seit dieser Ausbildung bin ich Vegetarier."

* Nachholbedarf:
    * https://www.tierbefreiungsoffensive-saar.de/literatur/jens-grote/

### Was passiert, wenn im Tierheim kein Platz mehr ist?
* Deutschland vermutlich:
    * siehe z. B. https://www.tierschutzverein-muenchen.de/das-sind-wir/faq.html
    * siehe z. B. http://www.tierheim-pforzheim.de/haeufig-gestellte-fragen.html
* Frankreich und anderswo leider so etwas:
    * ["Frankreich: Gesetz erlaubt Einschläfern nach zehn Tagen"](http://www1.wdr.de/fernsehen/tiere-suchen-ein-zuhause/tierschutz-frankreich-104.html), wdr.de, 12.06.2016
        * Auffang- und Tötungsstationen speziell für Hunde
        * Es wird zwar irgendwo bedauert, aber nicht besonders schlimm gefunden.
* Die Leute sollten verstehen, dass Tiere keine Spielzeuge sind, sondern Lebewesen. Und zwar BEVOR man sich überreden lässt, eins zu kaufen!
* Beispiele:
    * http://www.warsteiner-tierfreunde.de/
        * "In vielen europäischen Ländern ist es an der Tagesordnung, überflüssige und ungewollte Tiere einfach in einer Tötungsstation zu entsorgen."
            * "Nicht einfach wegsehen, sondern mutig sein"
            * "Es wird so weitergehen, solange wir... wegschauen / nur das Angenehme in dieser Welt sehen / nicht für Tiere bremsen, anhalten oder auch nur langsamer fahren / den Nachbarn mit dem teuer gekauften Zuchthund beeindrucken wollen / Geld über Leben stellen / Tiere als Sachen sehen"
            * Anmerkung: ob damit auch gemeint ist, Tiere nicht mehr zu Zwecken menschlicher Gaumengenüsse zu missbrauchen?
    * ["Zu teuer? Gemeinde lässt Kampfhunde einschläfern"](https://www.ndr.de/nachrichten/niedersachsen/braunschweig_harz_goettingen/Zu-teuer-Gemeinde-laesst-Kampfhunde-einschlaefern,kampfhund114.html), 2016
    * ["Tierheim bestätigt, dass verhaltensgestörte Hunde in der Einrichtung mit einer Spritze getötet werden mussten."](https://www.bz-berlin.de/artikel-archiv/schweren-herzens-tierheim-toetete-hunde), 2013

Besitzer
--------
* Negative Beispiele:
    * "Die Tierheime in Nordrhein-Westfalen sind überlastet. Gerade in den Sommermonaten werden viele Katzen abgegeben, da ihre Besitzer sie nicht in der Urlaubsplanung berücksichtigen."
    http://www.ruhrnachrichten.de/nachrichten/vermischtes/aktuelles_berichte/Aufnahmestopps-Tierheimen-in-NRW-fehlt-es-an-Platz-und-an-Geld;art29854,3120702
    * 2017: Hundebesitzer lassen ihr Tier an einen Krötenzaun pinkeln, obwohl klar sein müsste, dass dieser später von freiwilligen Tierschützern wieder aufgerollt werden muss
        * Lösung bei mangelndem Wissen als Ursache: Aufklärung
    * 2017: Entsorgung der Kackbeutel
        * Hintergrund: um öffentliche Flächen sauber zu halten, wurden Kotbeutel eingeführt, mit dem der verantwortungsvolle Besitzer die Hinterlassenschaften sauber entsorgen kann
        * Story 1: die grauen Plastikbeutel wurden in einem Kröteneimer entsorgt. Die Personen, die mit den Eimern betraut sind, haben das Nachsehen.
        * Story 2: die grauen Plastikbeutel werden regelmäßig statt in einem Mülleimer direkt in der Natur entsorgt (d. h. in der Natur liegt jetzt auch noch Plastik rum)
            * Lösung: die Beutelfarbe wird von grau auf rot geändert, was aufgrund der Sichtbarkeit die Hemmschwelle der gemeinwohlschädlichen Entsorgung erhöhen könnte.
* Lösungswege (zur Diskussion):
    * Hundeführerschein?
